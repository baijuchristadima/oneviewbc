var pgList = new Vue({
    el: "#pg-list",
    name: "pgList",
    data: {
        pgList: [],
        pgSelected: {
            id: 0
        },
        showInput: false,
        phoneNum: null,
        pgDetails: null,
        showWaafiInput: false,
        accountNum: null,
        // accountHolder: '',
        // accountPsw: null,
        waafipayCountryCode: 252,
        waafipayCountryCodeUI: '',
        showHormuud: false,
        waafipayHormuud: '',
        labelSection: {}
    },
    mounted: function () {
        try {
            var paymentVisitCHeck = sessionStorage.getItem("paymentVisit");
            if (paymentVisitCHeck==="false") {
                window.location.href = "Flights/flight-booking.html"
            }
        } catch (error) {
            console.log(error)
        }
        sessionStorage.setItem("paymentVisit", false);
        // if (localStorage.User && sessionStorage.pgDetails) {
        var user = JSON.parse(localStorage.User);
        if (user.loginNode.paymentGateways != null) {
            this.pgList = user.loginNode.paymentGateways;
        }
        // }else {
        // window.location.href = '/';
        // }
        this.waafipayCountryCodeUI = '+' + this.waafipayCountryCode;
        var hormuudId = _.findWhere(this.pgList, { id: 14 });
        if (hormuudId.id == 14) {
            this.getLabels();
        }

    },
    methods: {
        proceedPayment: function () {
            var pgDetails = JSON.parse(sessionStorage.pgDetails);
            this.pgDetails = pgDetails;
            if (pgDetails) {
                var indexPg = this.pgList.map(function (e) {
                    return e.id;
                }).indexOf(this.pgSelected.id);
                if (indexPg >= 0) {
                    if (this.pgSelected.id == 15) {
                        this.showInput = true;
                        this.showWaafiInput = false;
                        this.showHormuud = false;
                    } else if (this.pgSelected.id == 17) {
                        this.showWaafiInput = true;
                        this.showInput = false;
                        this.showHormuud = false;
                    } else if (this.pgSelected.id == 14) {
                        this.showWaafiInput = false;
                        this.showInput = false;
                        this.showHormuud = true;

                    } else {
                        pgDetails.pg.currentPayGateways = [this.pgList[indexPg]];
                        var status = paymentManager(pgDetails.pg, pgDetails.retUrl, null);
                    }
                } else {
                    pgDetails.pg.currentPayGateways = [this.pgList[0]];
                    var status = paymentManager(pgDetails.pg, pgDetails.retUrl, null);
                }
            } else {
                window.location.href = '/';
            }
        },
        proceedPaymentButton: function () {

            var pgDetails = JSON.parse(sessionStorage.pgDetails);
            if (pgDetails) {
                var indexPg = this.pgList.map(function (e) {
                    return e.id;
                }).indexOf(this.pgSelected.id);
                if (this.pgSelected.id == 15) {
                    // if (this.phoneNum == undefined && this.phoneNum == '') {
                    //     alertify.alert('').set('closable', false);
                    //     return;
                    // }
                    pgDetails.pg.currentPayGateways = [this.pgList[indexPg]];
                    pgDetails.pg.phoneNum = this.phoneNum;
                    var status = paymentManager(pgDetails.pg, pgDetails.retUrl, null);
                } else if (this.pgSelected.id == 14 || this.pgSelected.id == 17) {
                    if (this.accountNum == undefined || this.accountNum == '' || this.accountNum == null) {
                        document.getElementById('mobileNumberId').focus();
                        alertify.alert('Warning', 'Mobile Number is Mandatory').set('closable', false);
                        return;
                    } else if (this.accountNum.length != 9) {
                        document.getElementById('mobileNumberId').focus();
                        alertify.alert('Warning', 'Enter a valid number!').set('closable', false);
                        return;
                    }

                    // if (this.accountPsw == undefined || this.accountPsw == '' || this.accountPsw == null) {
                    //     alertify.alert('Warning', 'Password is Mandatory').set('closable', false);
                    //     return;
                    // }
                    var paymentDetails = this.pgList[indexPg];
                    // paymentDetails.type = this.pgSelected.type;
                    pgDetails.pg.currentPayGateways = [paymentDetails];
                    pgDetails.pg.accountNum = this.waafipayCountryCode + this.accountNum;
                    // pgDetails.pg.accountHolder = this.accountHolder;
                    // pgDetails.pg.accountPsw = this.accountPsw;
                    pgDetails.pg.paymentMethod = this.waafipayHormuud;
                    var status = paymentManager(pgDetails.pg, pgDetails.retUrl, null);
                }
            } else {
                window.location.href = '/';
            }
        },
        getLabels: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var url = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Select Payment/Select Payment/Select Payment.ftl';
                axios.get(url, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    if (response) {
                        if (response.data.area_List.length > 0) {
                            var label = response.data.area_List[0].Payment_Section.component;
                            if (label[1].Mobile_Wallet_Label != null && label[2].Bank_Account_Label != null) {
                                var labels = pluck('Payment_Section', response.data.area_List);
                                self.labelSection = getAllMapData(labels[0].component);
                            } else {
                                self.labelSection.Mobile_Wallet_Label = "Mobile Wallet";
                                self.labelSection.Bank_Account_Label = "Salam Bank Account linked to MWallet";
                            }

                        }
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.labelSection.Mobile_Wallet_Label = "Mobile Wallet";
                    self.labelSection.Bank_Account_Label = "Salam Bank Account linked to MWallet";
                });
            });
        },
    }
})