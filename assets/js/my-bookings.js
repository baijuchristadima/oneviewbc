Vue.prototype.moment = moment;
var myprofile_vue = new Vue({
    el: "#div_my-bookings",
    data: {
        User: {
            "id": "",
            "loginNode": {
                "servicesList": []
            },
            "loginid": "",
            "title": {
                "id": "",
                "name": ""
            },
            "firstName": "",
            "lastName": ""
        },
        search: '',
        bookings: [],
        upcomingSeen: true,
        completedSeen: true,
        cancelledSeen: true,
        limit: 2,
        selectedService: "2",
        allBookings: [],
        hotelBookings: [],
        flightBookings: [],
        currentBookings: [],
        My_Dashboard: '',
        My_Profile: '',
        My_Companion: '',
        My_Bookings: '',
        Change_Password: '',
        Feedback: '',
        Page_SubTitle: '',
        SubTitle: '',
        Upcoming: '',
        Completed: '',
        Cancelled: '',
        Search_PlaceHolder: '',
        Booking_ID: '',
        Trip_Detail: '',
        Traveller: '',
        Booking_Date: '',
        Departure_Date: '',
        Arrival_Date: '',
        Supplier_ref: '',
        Action: '',
        Reference_ID: '',
        City: '',
        Check_in: '',
        Nights: '',
        Timelimit: '',
        Booking_Status: '',
        Flights: '',
        Hotel: '',
        Hotels: '',
        Notfoundheader: '',
        Notfoundcontent: '',
        Continue_Booking: '',
        Technical_Difficulties: '',
        ErrorTitle: '',
        key: 0
    },
    mounted() {
        if (localStorage.IsLogin == 'false') {
            window.location.href = "/";
        } else {
            this.User = JSON.parse(localStorage.User);
            this.getPagecontent();
        }
        this.selectStatus('upcoming');

    },
    computed: {
        filteredBookingsList() {
            var sorted = this.currentBookings.filter(booking => {
                return booking.referenceNumber.toLowerCase().includes(this.search.toLowerCase())
            });
            sorted.sort(function (a, b) {
                return b.referenceNumber.split("-")[1] - a.referenceNumber.split("-")[1];
            });
            return sorted;
        },
        hasAirService() {
            // const cond = (element) => element.name.toLowerCase() == 'air' && element.id === 1;
            if (this.User.loginNode.servicesList && this.User.loginNode.servicesList.length) {
                return this.User.loginNode.servicesList.some((element) => element.name.toLowerCase() == 'air' && element.id === 1);
            }
            return false;
        },
        hasHotelService() {
            if (this.User.loginNode.servicesList && this.User.loginNode.servicesList.length) {
                return this.User.loginNode.servicesList.some((element) => element.name.toLowerCase() == 'hotel' && element.id === 2);
            }
            return false;
        },
    },
    created: function () {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                'Authorization': "Bearer " + localStorage.access_token
            }
        };
        var datas = {}; //Empty request for retrieving the complete data without filtering
        var huburl = ServiceUrls.hubConnection.baseUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var requrl = ServiceUrls.hubConnection.hubServices.flights.myBookingsCriteria;
        axios.post(huburl + portno + requrl, datas, axiosConfig)
            .then((res) => {
                console.log("RESPONSE RECEIVED: ", res);
                this.flightBookings = res.data;
                this.currentBookings = res.data;
                this.bookings = res.data;
                this.selectStatus('upcoming');
            })
            .catch((err) => {
                console.log("AXIOS ERROR: ", err);
                alertify.alert(this.ErrorTitle, this.Technical_Difficulties);
            })
    },
    methods: {
        getTitle: function (title) {
            if (title != '') {
                if (title == 1) {
                    title = 'Mr';
                }
                if (title == 2) {
                    title = 'Mrs';
                }
                if (title == 3) {
                    title = 'Ms';
                }
                if (title == 4) {
                    title = 'Miss';
                }
                if (title == 5) {
                    title = 'Master';
                }
            }
            return title;
        },
        filterDivShowHide() {
            this.bookings.filter(booking => {
                var res = booking.status.toLowerCase() == "Cancelled";
            })
        },
        onLimitChange: function (event) {
            this.limit = event.target.value;
        },
        getTripdetail: function (trip) {
            return '<span v-b-tooltip.hover="" title="' + airportLocationFromAirportCode(trip.split(' ')[0]) +
                ' to ' + airportLocationFromAirportCode(trip.split(' ')[2]) + '">' + trip + '</span>';
        },
        getRefdetail: function (refNo, paxname) {
            return '<span v-b-tooltip.hover="" title="' + paxname + '">' + refNo + '</span>';
        },
        getDatedetail: function (date) {
            return '<span v-b-tooltip.hover="" title="' + moment(date).format('DD MMM YYYY HH:mm') + '">' + moment(date).format('DD MMM YYYY') + '</span>';
        },
        getBookinginfo: function (item) {
            if (this.selectedService == "2") {
                //window.localStorage.setItem('BkngRefID',item.referenceNumber);
                var bookData = {
                    BkngRefID: item.referenceNumber,
                    emailId: window.localStorage.getItem('emailId'),
                    redirectFrom: 'myBookings',
                    isMailsend: false
                };
                window.localStorage.setItem('bookData', JSON.stringify(bookData));
                try {
                    window.sessionStorage.setItem("selectCredential", JSON.stringify(item.selectCredential));
                } catch (err) {}
                window.location.href = "/Flights/flight-confirmation.html";
            } else if (this.selectedService == "3") {
                window.sessionStorage.setItem('userAction', item.referenceNumber);
                window.location.href = "/Hotels/hotel-detail.html#/hotelConfirmation";
            }
        },
        selectService: function (selectedService) {
            var serviceUrl = "";
            var method = "get";

            if (selectedService == "1" && this.allBookings.length == 0) {
                var serviceUrl = "";
            } else if (selectedService == "2" && this.flightBookings.length == 0) {
                method = "post";
                var serviceUrl = ServiceUrls.hubConnection.hubServices.flights.myBookingsCriteria;
            } else if (selectedService == "3" && this.hotelBookings.length == 0) {
                var serviceUrl = ServiceUrls.hubConnection.hubServices.hotels.allBookings;
            } else if (selectedService == "4") {
                var serviceUrl = "";
            }
            if (serviceUrl) {
                this.getBookingList(selectedService, serviceUrl, method);
            } else {
                this.currentBookings = selectedService == "2" ? this.flightBookings : selectedService == "3" ? this.hotelBookings : [];
            }
            $("ul.tabs li").removeClass('active');
            $("ul.tabs li:first").addClass('active');
            this.selectStatus('upcoming');

        },
        getBookingList(selectedService, serviceUrl, method) {
            if (!serviceUrl) {
                return;
            }

            var datas = {}; //Empty request for retrieving the complete data without filtering
            var huburl = ServiceUrls.hubConnection.baseUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;

            axios({
                method: method,
                url: huburl + portno + serviceUrl,
                data: method == "post" ? datas : undefined,
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8',
                    'Authorization': "Bearer " + localStorage.access_token
                }
            }).then((res) => {
                console.log("RESPONSE RECEIVED: ", res);
                if (selectedService == "1") {
                    this.allBookings = res.data;
                } else if (selectedService == "2") {
                    this.flightBookings = res.data;
                } else if (selectedService == "3") {
                    this.hotelBookings = res.data;
                } else if (selectedService == "4") {
                    //nothing
                }
                this.currentBookings = res.data;
                this.selectStatus('upcoming');

            }).catch((err) => {
                console.log("AXIOS ERROR: ", err);
                alertify.alert(this.ErrorTitle, this.Technical_Difficulties);
            })
        },
        selectStatus: function (selectedStatus) {
            var filteredList = [];
            if (this.selectedService == "2") {
                if (selectedStatus == "upcoming") {
                    filteredList = this.flightBookings.filter(booking => {
                        return moment(booking.departureDate).format('YYYY MM DD HH:mm') > moment(new Date).format('YYYY MM DD HH:mm') &&
                            booking.status.toLowerCase() == "ticketed";
                    });
                } else if (selectedStatus == "completed") {
                    filteredList = this.flightBookings.filter(booking => {
                        return moment(booking.departureDate).format('YYYY MM DD HH:mm') < moment(new Date).format('YYYY MM DD HH:mm') &&
                            booking.status.toLowerCase() == "ticketed";
                    });
                } else if (selectedStatus == "cancelled") {
                    filteredList = this.flightBookings.filter(booking => {
                        if (booking.status.toLowerCase() == "cancelled" || booking.status.toLowerCase() == "ticket failed") {
                            return booking.status;
                        }
                    });
                }
            }

            if (this.selectedService == "3") {
                if (selectedStatus == "upcoming") {
                    filteredList = this.hotelBookings.filter(booking => {
                        return moment(booking.checkInDate).format('YYYY MM DD') >= moment(new Date).format('YYYY MM DD') &&
                            booking.statusCode == "RR";
                    });
                } else if (selectedStatus == "completed") {
                    filteredList = this.hotelBookings.filter(booking => {
                        return moment(booking.checkInDate).format('YYYY MM DD') < moment(new Date).format('YYYY MM DD') &&
                            booking.statusCode == "RR";
                    });
                } else if (selectedStatus == "cancelled") {
                    filteredList = this.hotelBookings.filter(booking => {
                        return booking.statusCode == "XX";
                    });
                }
            }
            filteredList.sort(function (a, b) {
                return b.referenceNumber.split("-")[1] - a.referenceNumber.split("-")[1];
            });

            this.currentBookings = filteredList;
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var Agencycode = 'AGY435';
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/My Bookings/My Bookings/My Bookings.ftl';
                axios.get(aboutUsPage, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (localStorage.direction == 'rtl') {
                        $(".select2-results__options").addClass("ar_direction1");
                    } else {
                        $(".select2-results__options").removeClass("ar_direction1");
                    }
                    if (response.data.area_List.length > 0) {
                        var mainComp = response.data.area_List[0].main;
                        self.Page_SubTitle = self.pluckcom('Page_SubTitle', mainComp.component);
                        self.SubTitle = self.pluckcom('SubTitle', mainComp.component);
                        self.Upcoming = self.pluckcom('Upcoming', mainComp.component);
                        self.Completed = self.pluckcom('Completed', mainComp.component);
                        self.Cancelled = self.pluckcom('Cancelled', mainComp.component);
                        self.Search_PlaceHolder = self.pluckcom('Search_PlaceHolder', mainComp.component);
                        self.Booking_ID = self.pluckcom('Booking_ID', mainComp.component);
                        self.Trip_Detail = self.pluckcom('Trip_Detail', mainComp.component);
                        self.Traveller = self.pluckcom('Traveller', mainComp.component);
                        self.Booking_Date = self.pluckcom('Booking_Date', mainComp.component);
                        self.Departure_Date = self.pluckcom('Departure_Date', mainComp.component);
                        self.Arrival_Date = self.pluckcom('Arrival_Date', mainComp.component);
                        self.Supplier_ref = self.pluckcom('Supplier_ref', mainComp.component);
                        self.Action = self.pluckcom('Action', mainComp.component);
                        self.Reference_ID = self.pluckcom('Reference_ID', mainComp.component);
                        self.City = self.pluckcom('City', mainComp.component);
                        self.Check_in = self.pluckcom('Check_in', mainComp.component);
                        self.Nights = self.pluckcom('Nights', mainComp.component);
                        self.Timelimit = self.pluckcom('Timelimit', mainComp.component);
                        self.Booking_Status = self.pluckcom('Booking_Status', mainComp.component);
                        self.Flights = self.pluckcom('Flights', mainComp.component);
                        self.Hotel = self.pluckcom('Hotel', mainComp.component);
                        self.Hotels = self.pluckcom('Hotels', mainComp.component);
                        self.Notfoundheader = self.pluckcom('Notfoundheader', mainComp.component);
                        self.Notfoundcontent = self.pluckcom('Notfoundcontent', mainComp.component);
                        self.Continue_Booking = self.pluckcom('Continue_Booking', mainComp.component);
                        self.Technical_Difficulties = self.pluckcom('Technical_Difficulties', mainComp.component);
                        self.ErrorTitle = self.pluckcom('ErrorTitle', mainComp.component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        getmoreinfo(url) {

            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url = "/Nirvana/book-now.html?page=" + url;
            } else {
                url = "#";
            }
            return url;
        }
    },
    directives: {
        select2: {
            inserted(el) {
                $(el).on('select2:select', () => {
                    const event = new Event('change', {
                        bubbles: true,
                        cancelable: true
                    });
                    el.dispatchEvent(event);
                });

                $(el).on('select2:unselect', () => {
                    const event = new Event('change', {
                        bubbles: true,
                        cancelable: true
                    })
                    el.dispatchEvent(event)
                })
            },
        }
    }

});