var huburl = ServiceUrls.hubConnection.baseUrl;
var portno = ServiceUrls.hubConnection.ipAddress;
//var payementURLPath=
var sessionURL = huburl + portno + '/payment/';
var paymentGatwayReturnURL = huburl + portno + '/paymenturi';
var pymentGatwayID = 2; //payfort
var newtoken = "";
var provider = [];
//var accessToken = localStorage.access_token;
function tokenUpdate(callBack) {
    var reNewTokenUrl = ServiceUrls.hubConnection.hubServices.reNewToken;
    var session_url = huburl + portno + reNewTokenUrl
    var currenttoken = localStorage.access_token;
    axios.get(session_url, {
        headers: {
            'Authorization': 'Bearer ' + currenttoken
        }
    }).then(function (response) {

        newtoken = response.headers.access_token;
        callBack();

    }).catch(function (error) {
        console.log('Error on Authentication');

    });
}
//var portno = ServiceUrls.hubConnection.ipAddress;


function paymentManager(paymentDetails, returnURL, vueComponent, awsApi) {
    tokenUpdate(
        function successToken() {

            if (newtoken != null) {
                var cartId = paymentDetails.cartID;
                var isAwsApi = awsApi ? awsApi : false || false
                if (cartId == "") {
                    var bookRef = paymentDetails.insuranceBookRef ?
                        paymentDetails.bookingReference + "_" + paymentDetails.insuranceBookRef : paymentDetails.bookingReference;
                    axios
                        .get(sessionURL + bookRef, {
                            headers: {
                                Authorization: "Bearer " + newtoken
                            }
                        })
                        .then((response) => {

                            // showHideConfirmLoader(false);
                            cartId = response.data.cartId;
                            //      accessToken = response.headers.access_token;
                            pgWay(newtoken, paymentDetails, cartId, returnURL, vueComponent);

                        })
                        .catch((error) => {
                            showHideConfirmLoader(false);
                            alert("Error on getting cart ID");
                            console.error("Error on getting cart ID");
                            console.error(error);
                        });

                } else {
                    pgWay(newtoken, paymentDetails, cartId, returnURL, vueComponent, isAwsApi);
                }
                //  localStorage.access_token = accessToken;//update access Token
            }
        });

};

function pgWay(accessTken, paymentDetails, cartId, returnURL, vueComponent, isAwsApi) {
    if (accessTken) {
        // showHideConfirmLoader(false);
        //  localStorage.access_token = accessToken;//update access Token
        if (paymentDetails.totalAmount) {
            switch (paymentDetails.currentPayGateways[0].id) {
                case 1: //Telr
                    break;
                case 2: //PayFort
                    axios
                        .get(sessionURL, {
                            headers: {
                                Authorization: "Bearer " + accessTken
                            }
                        })
                        .then((response) => {

                            //  accessToken = response.headers.access_token;
                            var paymentCredentials = response.data;

                            if (paymentCredentials.length > 0) {
                                var CredentiallistTop = paymentCredentials[0].credentialList;
                                var payfortcommand = CredentiallistTop.filter((x) => {
                                    return x.key == "payfortcommand";
                                })[0].value;
                                var payfortactlink = CredentiallistTop.filter((x) => {
                                    return x.key == "payfortactlink";
                                })[0].value;
                                var payfortaccesscode = CredentiallistTop.filter((x) => {
                                    return x.key == "payfortaccesscode";
                                })[0].value;
                                var payfortlanguage = CredentiallistTop.filter((x) => {
                                    return x.key == "payfortlanguage";
                                })[0].value;
                                var payfortmerchantidentifier = CredentiallistTop.filter((x) => {
                                    return x.key == "payfortmerchantidentifier";
                                })[0].value;
                                var payfortcurrency = CredentiallistTop.filter((x) => {
                                    return x.key == "payfortcurrency";
                                })[0].value;
                                var PayfortSHARequestPhrase = CredentiallistTop.filter((x) => {
                                    return x.key == "PayfortSHARequestPhrase";
                                })[0].value;
                                var payfortCurrencyMultiplier = CredentiallistTop.filter((x) => {
                                    return x.key == "payfortCurrencyMultiplier";
                                })[0].value;
                                var awsApiUrl = CredentiallistTop.filter((x) => {
                                    return x.key == "awsApiUrl";
                                })[0];
                                awsApiUrl = awsApiUrl ? awsApiUrl.value : ""
                                //var payfortcurrency = localStorage.selectedCurrency;
                                //var PayfortSHAResponsePhrase = CredentiallistTop.filter((x) => { return x.key == "PayfortSHAResponsePhrase"; })[0].value;
                                // var integrationtype = CredentiallistTop.filter((x) => { return x.key == "integrationtype"; })[0].value;
                                // var MerchantPageUrl = CredentiallistTop.filter((x) => { return x.key == "MerchantPageUrl"; })[0].value;
                                var payfortreturl = '?paymentGwayId=' + pymentGatwayID + '&bookingAmount=' + paymentDetails.totalAmount + '&returnUrl=' + returnURL + '&cartId=' + cartId + '&token=' + accessTken;
                                // var payfortreturl = paymentGatwayReturnURL + '?paymentGwayId=' + pymentGatwayID + '&bookingAmount=' + paymentDetails.totalAmount + '&returnUrl=' + returnURL + '&cartId=' + cartId + '&token=' + accessTken;
                                if (isAwsApi && awsApiUrl) {
                                    // for Spartan Payment
                                    payfortreturl = awsApiUrl + payfortreturl
                                } else {
                                    payfortreturl = paymentGatwayReturnURL + payfortreturl
                                }


                                // var totalpayAmount = paymentDetails.totalAmount / localStorage.CurrencyMultiplier;
                                var totalpayAmount = parseFloat(paymentDetails.totalAmount);
                                totalpayAmount = totalpayAmount.toFixed(2);
                                totalpayAmount = totalpayAmount.toString().replace('.', '');
                                var signature = signatureGenerator(payfortaccesscode, payfortlanguage, payfortmerchantidentifier, cartId, payfortcommand, totalpayAmount,
                                    payfortcurrency, paymentDetails.customerEmail, payfortreturl, PayfortSHARequestPhrase);
                                var formString = outformGenerator(payfortactlink, payfortcommand, payfortaccesscode, payfortmerchantidentifier,
                                    totalpayAmount, payfortcurrency, payfortlanguage, paymentDetails.customerEmail, signature, cartId, payfortreturl);
                                vueComponent.paymentForm = formString;
                                try {
                                    $('#payformbind').html(formString);
                                } catch (err) {}
                                setTimeout(function () {
                                    $("#btnGoToPG").click();
                                }, 10);
                            }
                        })
                        .catch((error) => {
                            showHideConfirmLoader(false);
                            alert("Error on getting credentails");
                            console.error("Error on getting credentails");
                            console.error(error);
                        });
                    break;
                case 5: //test
                    break;
                case 6: //Paypal
                    break;
                case 7: //Paytm
                    break;
                case 8: //Mastercard
                    break;
                case 9: //Payfort Merchant -- no need it here already handled in travellers details
                    break;
                case 10: //Paystack

                    var request = {
                        reference: cartId,
                        amount: paymentDetails.totalAmount,
                        email: paymentDetails.customerEmail,
                        callback_url: huburl + portno + "/paystack/verify?reference=" + cartId + "&url=" + returnURL + "&token=" + localStorage.access_token
                    }

                    var pgPayStack = ServiceUrls.hubConnection.hubServices.pgPayStack;
                    axios.post(huburl + portno + pgPayStack, request, {
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.access_token
                        }
                    }).then(function (response) {

                        if (response.data.status == true) {
                            tokenUpdate(function () {
                                localStorage.access_token = newtoken;
                                window.location.href = response.data.data.authorization_url;
                            })
                        } else {
                            cancelPNR();
                            console.log('Error on Payment Gateway Hub - ' + response.data.message);
                        }
                        console.log(response);
                        return response.data;
                    }).catch(function (error) {
                        cancelPNR();
                        console.log('Error on Payment Gateway Hub - ' + error);
                        return error;
                    });

                    break;
                case 11: // SyberPay
                    var request = {
                        currency: paymentDetails.currency,
                        amount: paymentDetails.totalAmount,
                        customerRef: cartId,
                        paymentInfo: {
                            token: localStorage.access_token,
                            url: returnURL,
                            emailId: paymentDetails.customerEmail,
                            phoneNum: ""
                        }
                    }

                    var pgSyberPay = ServiceUrls.hubConnection.hubServices.pgSyberPayInitialise;
                    axios.post(huburl + portno + pgSyberPay, request, {
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.access_token
                        }
                    }).then(function (response) {

                        if (response.data.responseMessage.toLowerCase() == "approved") {
                            tokenUpdate(function () {
                                localStorage.access_token = newtoken;
                                sessionStorage.transactionId = response.data.transactionId;
                                sessionStorage.customerRef = cartId;
                                window.location.href = response.data.paymentUrl;
                            })
                        } else {
                            cancelPNR();
                            console.log('Error on Payment Gateway Hub - ' + response.data.message);
                        }
                        console.log(response);
                        return response.data;
                    }).catch(function (error) {
                        cancelPNR();
                        console.log('Error on Payment Gateway Hub - ' + error);
                        return error;
                    });
                    break;
                case 12: //Flutterwave
                    var request = {
                        reference: cartId,
                        email: paymentDetails.customerEmail,
                        amount: paymentDetails.totalAmount,
                        currency: paymentDetails.currency,
                        token: localStorage.access_token,
                        redirectUrl: returnURL
                    }

                    var pgFlutterwave = ServiceUrls.hubConnection.hubServices.pgFlutterwave;
                    axios.post(huburl + portno + pgFlutterwave, request, {
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.access_token
                        }
                    }).then(function (response) {

                        if (response.data.paymentUrl) {
                            tokenUpdate(function () {
                                localStorage.access_token = newtoken;
                                window.location.href = response.data.paymentUrl;
                            })
                        } else {
                            cancelPNR();
                            console.log('Error on Payment Gateway Hub - ' + response.data.message);
                        }
                        console.log(response);
                        return response.data;
                    }).catch(function (error) {
                        cancelPNR();
                        console.log('Error on Payment Gateway Hub - ' + error);
                        return error;
                    });
                    break;
                case 13: // EPG
                    var request = {
                        amount: paymentDetails.totalAmount,
                        currency: paymentDetails.currency,
                        cartId: cartId,
                        orderName: "PayBill",
                        redirectUrl: returnURL
                    }

                    var pgFAB = ServiceUrls.hubConnection.hubServices.pgFAB;
                    axios.post(huburl + portno + pgFAB, request, {
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.access_token
                        }
                    }).then(function (response) {

                        if (response.data.paymentUrl) {
                            tokenUpdate(function () {
                                localStorage.access_token = newtoken;
                                var formString = '<form action="' + response.data.paymentUrl + '" method="post">' +
                                    '<input type="hidden" name="TransactionID" value="' + response.data.transactionID + '"/>' +
                                    '<input id="btnGoToPG" type="submit" value="Submit"></form>';

                                try {
                                    $('#payformbind').html(formString);
                                } catch (err) {}
                                setTimeout(function () {

                                    $("#btnGoToPG").click();
                                }, 10);
                            })
                        } else {
                            cancelPNR();
                            console.log('Error on Payment Gateway Hub - ' + response.data.message);
                        }
                        console.log(response);
                        return response.data;
                    }).catch(function (error) {
                        cancelPNR();
                        console.log('Error on Payment Gateway Hub - ' + error);
                        return error;
                    });
                    break;
                    // case 14: // Waafipay
                    //     var request = {
                    //         amount: paymentDetails.totalAmount,
                    //         currency: paymentDetails.currency,
                    //         cartId: cartId,
                    //         orderName: "PayBill",
                    //         redirectUrl: returnURL,
                    //         paymentMethod: paymentDetails.currentPayGateways[0].type,

                    //     }

                    //     var pgWaafiPay = ServiceUrls.hubConnection.hubServices.pgWaafiPay;
                    //     axios.post(huburl + portno + pgWaafiPay, request, {
                    //         headers: { 'Authorization': 'Bearer ' + localStorage.access_token }
                    //     }).then(function(response) {
                    //         if (response.data.paymentUrl) {
                    //             tokenUpdate(function() {
                    //                 localStorage.access_token = newtoken;
                    //                 var formString =
                    //                     '<form method="POST" action="' + response.data.paymentUrl + '">' +
                    //                     '<input type="hidden" name="hppRequestId" id="hppRequestId" value="' + response.data.hppRequestId + '">' +
                    //                     '<input type="hidden" name="referenceId" id="referenceId" value="' + response.data.referenceId + '">' +
                    //                     '<input id="btnGoToPG" type="submit" value="Submit"></form>';

                    //                 try {
                    //                     $('#payformbind').html(formString);
                    //                 } catch (err) {}
                    //                 setTimeout(function() {

                    //                     $("#btnGoToPG").click();
                    //                 }, 10);
                    //             })
                    //         } else {
                    //             console.log('Error on Payment Gateway Hub - ' + response.data.message);
                    //         }
                    //         console.log(response);
                    //         return response.data;
                    //     }).catch(function(error) {
                    //         console.log('Error on Payment Gateway Hub - ' + error);
                    //         return error;
                    //     });
                    //     break;

                case 14: // Waafipay-Hormuud
                    var request = {
                        amount: paymentDetails.totalAmount,
                        currency: paymentDetails.currency,
                        // amount: '1.00',
                        // currency: "USD",

                        cartId: cartId,
                        orderName: "PayBill",
                        redirectUrl: returnURL,
                        accountNum: paymentDetails.accountNum,
                        paymentMethod: paymentDetails.paymentMethod
                        // accountHolder: paymentDetails.accountHolder,
                        // accountPsw: paymentDetails.accountPsw

                    }
                    var pgHormuudPay = ServiceUrls.hubConnection.hubServices.pgWaafiPayEvc;
                    axios.post(huburl + portno + pgHormuudPay, request, {
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.access_token
                        }
                    }).then(function (response) {
                        if (response.data.paymentUrl) {
                            tokenUpdate(function () {
                                localStorage.access_token = newtoken;
                                window.location.href = response.data.paymentUrl;
                            })
                        } else {
                            cancelPNR();
                            console.log('Error on Payment Gateway Hub - ' + response.data.message);
                        }
                        console.log(response);
                        return response.data;
                    }).catch(function (error) {
                        cancelPNR();
                        console.log('Error on Payment Gateway Hub - ' + error);
                        return error;
                    });
                    break;
                case 15: // eDahab
                    var request = {
                        cartId: cartId,
                        currency: paymentDetails.currency,
                        amount: paymentDetails.totalAmount,
                        // amount: "1.00",
                        redirectUrl: returnURL,
                        orderName: "PayBill",
                        phoneNum: paymentDetails.phoneNum
                    }

                    var pgEDahab = ServiceUrls.hubConnection.hubServices.pgEDahab;
                    axios.post(huburl + portno + pgEDahab, request, {
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.access_token
                        }
                    }).then(function (response) {

                        if (response.data.paymentUrl) {
                            tokenUpdate(function () {
                                localStorage.access_token = newtoken;
                                window.location.href = response.data.paymentUrl;
                            })
                        } else {
                            cancelPNR();
                            alertify.alert("Error", response.data.message);
                            console.log('Error on Payment Gateway Hub - ' + response.data.message);
                        }
                        console.log(response);
                        return response.data;
                    }).catch(function (error) {
                        cancelPNR();
                        alertify.alert("Error", error.data.message);
                        console.log('Error on Payment Gateway Hub - ' + error);
                        return error;
                    });
                    break;
                case 16: // ngenius
                    var request = {
                        amount: paymentDetails.totalAmount,
                        currency: paymentDetails.currency,
                        cartId: cartId,
                        orderName: "PayBill",
                        redirectUrl: returnURL
                    }
                    var pgNGeniusPay = ServiceUrls.hubConnection.hubServices.pgNGeniusPay;
                    axios.post(huburl + portno + pgNGeniusPay, request, {
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.access_token
                        }
                    }).then(function (response) {
                        if (response.data.paymentUrl) {
                            tokenUpdate(function () {
                                localStorage.access_token = newtoken;
                                window.location.href = response.data.paymentUrl;
                            })
                        } else {
                            cancelPNR();
                            console.log('Error on Payment Gateway Hub - ' + response.data.message);
                        }
                        console.log(response);
                        return response.data;
                    }).catch(function (error) {
                        cancelPNR();
                        console.log('Error on Payment Gateway Hub - ' + error);
                        return error;
                    });
                    break;
                case 17: // Waafipay-Telsom
                    var request = {
                        amount: paymentDetails.totalAmount,
                        currency: paymentDetails.currency,
                        // amount: '1.00',
                        // currency: "AED",

                        cartId: cartId,
                        orderName: "PayBill",
                        redirectUrl: returnURL,
                        accountNum: paymentDetails.accountNum,
                        paymentMethod: 'mwallet_account'
                        // accountHolder: paymentDetails.accountHolder,
                        // accountPsw: paymentDetails.accountPsw
                    }
                    var pgTelsomPay = ServiceUrls.hubConnection.hubServices.pgWaafiPayZaad;
                    axios.post(huburl + portno + pgTelsomPay, request, {
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.access_token
                        }
                    }).then(function (response) {
                        if (response.data.paymentUrl) {
                            tokenUpdate(function () {
                                localStorage.access_token = newtoken;
                                window.location.href = response.data.paymentUrl;
                            })
                        } else {
                            cancelPNR();
                            console.log('Error on Payment Gateway Hub - ' + response.data.message);
                        }
                        console.log(response);
                        return response.data;
                    }).catch(function (error) {
                        cancelPNR();
                        console.log('Error on Payment Gateway Hub - ' + error);
                        return error;
                    });
                    break;
                case 18: // stanbic
                    var request = {
                        amount: paymentDetails.totalAmount,
                        // amount: 0.1,
                        currency: paymentDetails.currency,
                        cartId: cartId,
                        orderName: cartId,
                        redirectUrl: returnURL
                    }

                    var stanbic = ServiceUrls.hubConnection.hubServices.stanbic;
                    axios.post(huburl + portno + stanbic, request, {
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.access_token
                        }
                    }).then(function (response) {

                        if (response.data.paymentUrl) {
                            tokenUpdate(function () {
                                localStorage.access_token = newtoken;
                                window.location.href = response.data.paymentUrl;
                            })
                        } else {
                            cancelPNR();
                            console.log('Error on Payment Gateway Hub - ' + response.data.message);
                        }
                        console.log(response);
                        return response.data;
                    }).catch(function (error) {
                        cancelPNR();
                        console.log('Error on Payment Gateway Hub - ' + error);
                        return error;
                    });
                    break;
                case 33: //PayTabs
                    if (!isAwsApi) {
                        var request = {
                            cartId: cartId,
                            amount: paymentDetails.totalAmount,
                            currency: paymentDetails.currency,
                            orderName: "PayBill",
                            redirectUrl: returnURL
                        }
                        var pgPayTabs = ServiceUrls.hubConnection.hubServices.pgPayTabs;
                        axios.post(huburl + portno + pgPayTabs, request, {
                            headers: {
                                'Authorization': 'Bearer ' + localStorage.access_token
                            }
                        }).then(function (response) {
                            if (response.data.paymentUrl) {
                                tokenUpdate(function () {
                                    localStorage.access_token = newtoken;
                                    window.location.href = response.data.paymentUrl;
                                })
                            } else {
                                cancelPNR();
                                console.log('Error on Payment Gateway Hub - ' + response.data.message);
                            }
                            console.log(response);
                            return response.data;
                        }).catch(function (error) {
                            cancelPNR();
                            console.log('Error on Payment Gateway Hub - ' + error);
                            return error;
                        });

                    } else if (isAwsApi) {
                        axios
                            .get(sessionURL, {
                                headers: {
                                    Authorization: "Bearer " + accessTken
                                }
                            })
                            .then((response) => {
                                var paymentCredentials = response.data;
                                if (paymentCredentials.length > 0) {
                                    var CredentiallistTop = paymentCredentials[0].credentialList;
                                    var awsApiUrl = CredentiallistTop.filter((x) => {
                                        return x.key == "AwsApiUrl";
                                    })[0].value;

                                    var paytabtreturl = awsApiUrl + "/submit";
                                    // vueComponent.paymentForm = formString;

                                    var request = {
                                        paymentType: "PAYMENT_GATEWAY",
                                        bookingId: cartId,
                                        amount: paymentDetails.totalAmount,
                                        currency: paymentDetails.currency,
                                        remarks: "Spartan Booking"
                                    }
                                    axios.post(paytabtreturl, request, {
                                        headers: {
                                            'Authorization': 'Bearer ' + localStorage.access_token
                                        }
                                    }).then(function (response) {
                                        if (response.status == 200) {
                                            tokenUpdate(function () {
                                                localStorage.access_token = newtoken;
                                                window.location.href = response.data.data.redirectUrl;
                                            })
                                        } else {
                                            console.log('Error on Payment Gateway Hub - ' + response.data.message);
                                        }
                                        console.log(response);
                                        return response.data;
                                    }).catch(function (error) {
                                        console.log('Error on Payment Gateway Hub - ' + error);
                                        return error;
                                    });

                                }
                            })
                            .catch((error) => {
                                showHideConfirmLoader(false);
                                alert("Error on getting credentails");
                                console.error("Error on getting credentails");
                                console.error(error);
                            });
                    }
                    break;
                default:
                    break;
            }
        }
    }
}

function signatureGenerator(payfortaccesscode, payfortlanguage, payfortmerchantidentifier, bookrefnum, payfortcommand, totpayamount, payfortcurrency, customeremail, payfortreturl, SHARequestPhrase) {
    var text = SHARequestPhrase + "access_code=" + payfortaccesscode + "amount=" + totpayamount + "command=" + payfortcommand + "currency=" + payfortcurrency + "customer_email=" + customeremail + "language=" + payfortlanguage + "merchant_identifier=" + payfortmerchantidentifier + "merchant_reference=" + bookrefnum + "return_url=" + payfortreturl + SHARequestPhrase;
    return hash = a(text);
};

function outformGenerator(payfortactlink, payfortcommand, payfortaccesscode, payfortmerchantidentifier, totpayamount, payfortcurrency, payfortlanguage, customeremail, signature, bookrefnum, payfortreturl) {

    return "<form method=\"post\" action=" + payfortactlink +
        " id=\"form1\" name=\"form1\">" +
        "<input type=\"hidden\" name=\"command\" value='" + payfortcommand + "' id=\"command\" />" +
        "<input type=\"hidden\" name=\"access_code\" value='" + payfortaccesscode + "' id=\"access_code\" />" +
        "<input type=\"hidden\" name=\"merchant_identifier\" value='" + payfortmerchantidentifier + "' id=\"merchant_identifier\" />" +
        "<input type=\"hidden\" name=\"amount\" value='" + totpayamount + "' id=\"amount\" />" +
        "<input type=\"hidden\" name=\"currency\" value='" + payfortcurrency + "' id=\"currency\" />" +
        "<input type=\"hidden\" name=\"language\" value='" + payfortlanguage + "' id=\"language\" />" +
        "<input type=\"hidden\" name=\"customer_email\" value='" + customeremail + "' id=\"customeremail\" />" +
        "<input type=\"hidden\" name=\"signature\" value='" + signature + "' id=\"signature\" />" +
        "<input type=\"hidden\" name=\"merchant_reference\" value='" + bookrefnum + "' id=\"merchant_reference\" />" +
        "<input type=\"hidden\" name=\"return_url\" value='" + payfortreturl + "' id=\"return_url\" />" +
        "<input type=\"submit\" value=\"PAY\" id=\"btnGoToPG\"  ref=\"payBtn\"  style='display:none;' /> </form>";
};

function a(b) {
    function c(a, b) {
        return a >>> b | a << 32 - b
    }
    for (var d, e, f = Math.pow, g = f(2, 32), h = "length", i = "", j = [], k = 8 * b[h], l = a.h = a.h || [], m = a.k = a.k || [], n = m[h], o = {}, p = 2; 64 > n; p++)
        if (!o[p]) {
            for (d = 0; 313 > d; d += p) o[d] = p;
            l[n] = f(p, .5) * g | 0, m[n++] = f(p, 1 / 3) * g | 0
        }
    for (b += "\x80"; b[h] % 64 - 56;) b += "\x00";
    for (d = 0; d < b[h]; d++) {
        if (e = b.charCodeAt(d), e >> 8) return;
        j[d >> 2] |= e << (3 - d) % 4 * 8
    }
    for (j[j[h]] = k / g | 0, j[j[h]] = k, e = 0; e < j[h];) {
        var q = j.slice(e, e += 16),
            r = l;
        for (l = l.slice(0, 8), d = 0; 64 > d; d++) {
            var s = q[d - 15],
                t = q[d - 2],
                u = l[0],
                v = l[4],
                w = l[7] + (c(v, 6) ^ c(v, 11) ^ c(v, 25)) + (v & l[5] ^ ~v & l[6]) + m[d] + (q[d] = 16 > d ? q[d] : q[d - 16] + (c(s, 7) ^ c(s, 18) ^ s >>> 3) + q[d - 7] + (c(t, 17) ^ c(t, 19) ^ t >>> 10) | 0),
                x = (c(u, 2) ^ c(u, 13) ^ c(u, 22)) + (u & l[1] ^ u & l[2] ^ l[1] & l[2]);
            l = [w + x | 0].concat(l), l[4] = l[4] + w | 0
        }
        for (d = 0; 8 > d; d++) l[d] = l[d] + r[d] | 0
    }
    for (d = 0; 8 > d; d++)
        for (e = 3; e + 1; e--) {
            var y = l[d] >> 8 * e & 255;
            i += (16 > y ? 0 : "") + y.toString(16)
        }
    return i
};

function PayVThree(pgId, bkRefNo, returnUrl) {
    var pgGetPostData = ServiceUrls.hubConnection.hubServices.pgGetPostData;
    var currenttoken = localStorage.access_token;
    var rq = {
        "bookingId": bkRefNo,
        "paymentGatwayId": pgId,
        "returnUrl": returnUrl
    }

    return axios.post(huburl + portno + pgGetPostData, rq, {
        headers: {
            'Authorization': 'Bearer ' + currenttoken
        }
    }).then(function (response) {
        localStorage.access_token = response.headers.access_token;
        console.log(response);
        return response.data;
    }).catch(function (error) {
        console.log('Error on Payment Gateway Hub - ' + error);
        return error;
    });
}

function GenSignature(plainStr, sigType, paymentGatwayId) {
    var pgGetSignature = ServiceUrls.hubConnection.hubServices.pgGetSignature;
    var currenttoken = localStorage.access_token;
    var rq = {
        "signature": plainStr,
        "type": sigType,
        "paymentGatwayId": paymentGatwayId
    }

    return axios.post(huburl + portno + pgGetSignature, rq, {
        headers: {
            'Authorization': 'Bearer ' + currenttoken
        }
    }).then(function (response) {
        localStorage.access_token = response.headers.access_token;
        console.log(response);
        return response.data;
    }).catch(function (error) {
        console.log('Error on Payment Gateway Hub - ' + error);
        return error;
    });
}

function cancelPNR() {
    sessionStorage.setItem("payvisit", true);
    var bookData = JSON.parse(localStorage.bookData);
    var user = JSON.parse(localStorage.User);
    var supDetails = user.loginNode.servicesList.filter(function (service) {
        return service.name.toLowerCase().includes('air')
    });
    supDetails[0].provider.forEach(function (data) {
        provider.push(data.id);
    });
    axiosConfig = {
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'Authorization': "Bearer " + localStorage.access_token
        }
    }
    var selectCred = null;
    try {
        selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"))
    } catch (err) {}
    var PostData = {
        request: {
            service: "FlightRQ",
            content: {
                command: "FlightCancelPnrRQ",
                supplierSpecific: {},
                cancelPnrRQ: {
                    bookingRefId: bookData.BkngRefID
                }
            },
            selectCredential: selectCred,
            supplierCodes: provider
        }
    };
    console.log('CancelPNRReq', JSON.stringify(PostData));
    var requrl = ServiceUrls.hubConnection.hubServices.flights.AircancelPnr;
    axios.post(huburl + portno + requrl, PostData, axiosConfig)
        .then((res) => {
            console.log("CancelPnr Response: ", res);
            localStorage.access_token = res.headers.access_token;
            console.log("Cancelled Status ", res.data.response.content.cancelPnrRS.success);
            window.location.href = '/index.html';
        })
        .catch((err) => {
            console.log("Cancelled Error: ", err);
            // showHideConfirmLoader(false);
            alertify.alert('Error', 'Server error !');

        })
}