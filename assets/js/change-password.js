﻿

var changePassword_vue = new Vue({
    el: "#div_changePassword",
    data: {
        User: JSON.parse(localStorage.User),
        postData: {
            oldPassword: '',
            newPassword: '',
            confirmPassword: ''
        },
        Required_Title: '',
        PasswordMismach_Title:'',
        alert_SrryTitle:'',
        alert_FailedTitle:'',
        error_OldPwdTitle:'',
        err_TechPrblemTitle:'',
        Succes_Msglable:'',
        alert_MessageTitle:'',
        ok_BtnLable:'',
        spTitle: '',
        chngPwd: '',
        email: '',
        crntPwd: '',
        nwPwd: '',
        rtpPwd: '',
        sbmt: ''
    },
    mounted() {
        if (localStorage.IsLogin == 'false') {
            window.location.href = "/";
        } else {
            this.getPagecontent();
            this.User = JSON.parse(localStorage.User);
            this.emailid = this.User.emailId;
        }

    },
    methods: {
        getTitle: function (title) {
            if (title != '') {
                if (title == 1) { title = 'Mr'; }
                if (title == 2) { title = 'Mrs'; }
                if (title == 3) { title = 'Ms'; }
                if (title == 4) { title = 'Miss'; }
                if (title == 5) { title = 'Master'; }
            }
            return title;
        },
        changePassword: function () {
            if (localStorage.IsLogin) {
                if (this.postData.oldPassword == "") {
                    alertify.alert(this.Required_Title, this.crntPwd).set('label', this.ok_BtnLable); 
                    return false;
                }
                if (this.postData.newPassword == "") {
                    alertify.alert(this.Required_Title, this.nwPwd).set('label', this.ok_BtnLable); 
                    return false;
                }
                if (this.postData.confirmPassword == "") {
                    alertify.alert(this.Required_Title, this.rtpPwd).set('label', this.ok_BtnLable); 
                    return false;
                }
                if (this.postData.newPassword.toLowerCase() != this.postData.confirmPassword.toLowerCase()) {
                    alertify.alert(this.Required_Title, this.PasswordMismach_Title).set('label', this.ok_BtnLable); 
                    return false;
                }
                var datas = {
                    oldPassword: this.postData.oldPassword,
                    newPassword: this.postData.newPassword
                }
                let axiosConfig = {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Authorization': "Bearer " + localStorage.access_token
                    }
                };
                var huburl = ServiceUrls.hubConnection.baseUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var requrl = ServiceUrls.hubConnection.hubServices.changePassword;
                axios.put(huburl + portno + requrl, datas, axiosConfig)
                    .then((res) => {
                        console.log("RESPONSE RECEIVED: ", res);
                        alertify.alert(this.alert_MessageTitle, this.Succes_Msglable).set('label', this.ok_BtnLable); 
                    })
                    .catch((err) => {
                        console.log("AXIOS ERROR: ", err);
                        try {
                            if (err.response.data.code == 403) { 
                                alertify.alert(this.alert_SrryTitle, this.error_OldPwdTitle).set('label', this.ok_BtnLable); 
                            } else {
                                alertify.alert(this.alert_FailedTitle, this.err_TechPrblemTitle).set('label', this.ok_BtnLable); 
                            }
                        } catch (err) {
                            alertify.alert(this.this.alert_FailedTitle, this.err_TechPrblemTitle).set('label', this.ok_BtnLable); 
                        }
                    })
            }
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = 'AGY435';
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var changePasswordPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Change Password/Change Password/Change Password.ftl';
                axios.get(changePasswordPage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (response.data.area_List.length > 0) {
                        var mainComp = response.data.area_List[0].main;
                        self.spTitle = self.pluckcom('Page_Title', mainComp.component);
                        self.Required_Title = self.pluckcom('Required_Title', mainComp.component);
                        self.PasswordMismach_Title = self.pluckcom('PasswordMismach_Title', mainComp.component);
                        self.alert_SrryTitle = self.pluckcom('alert_SrryTitle', mainComp.component);
                        self.alert_FailedTitle = self.pluckcom('alert_FailedTitle', mainComp.component);
                        self.error_OldPwdTitle = self.pluckcom('error_OldPwdTitle', mainComp.component);
                        self.err_TechPrblemTitle = self.pluckcom('err_TechPrblemTitle', mainComp.component);
                        self.ok_BtnLable = self.pluckcom('ok_BtnLable', mainComp.component);
                        self.alert_MessageTitle = self.pluckcom('alert_MessageTitle', mainComp.component);
                        self.Succes_Msglable = self.pluckcom('Succes_Msglable', mainComp.component);
                        self.chngPwd = self.pluckcom('Change_Password', mainComp.component);
                        self.email = self.pluckcom('E-mail_ID', mainComp.component);
                        self.crntPwd = self.pluckcom('Current_Password', mainComp.component);
                        self.nwPwd = self.pluckcom('New_Password', mainComp.component);
                        self.rtpPwd = self.pluckcom('Retype_Password', mainComp.component);
                        self.sbmt = self.pluckcom('Submit', mainComp.component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        }
    }
});


