

// var header = new Vue({
//     //i18n,
//     el: "#header",    
//     data: {
//         selectedLanguage: 'en',
//         content: {
//             "response": {
//               "general": {
//                 "bannerImage": "../assets/images/banner.jpg",
//                 "logo": "../assets/images/logo.png",
//                 "address": "",
//                 "agencyName": "a2zBooking",
//                 "emailID": "",
//                 "contactNo": "",
//                 "copyRight": ""
//               }
//             }
//         }
//         // selectedCurrency: Vue_localization.defaultCurrency,
//         // languageOptions: Vue_localization.languageOptions,
//         // CurrencyOptions: Vue_localization.CurrencyOptions
//     },
//     methods: {
//         onChange(value) {
//             // this.selectedCurrency=value;
//             // test.selectedCurrency=this.selectedCurrency;
//         }
//     }
// });


// var test = new Vue({
//     //i18n,
//     el: "#testDiv",
//     data: {
//         selectedLanguage: 'en',
//         selectedCurrency: header.selectedCurrency
//     }
// });



var cmsapp = new Vue({
  el: '#cmsdiv',
  name: 'cms',
  data() {
    return {
      key: '',
      content: null,
      getdata: true,
      dir: 'ltr'
    }

  },
  mounted:function() {
  
    this.getcmsdata();    


  },
  methods: {

  
    moment: function () {
      return moment();
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getcmsdata:function(){
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        self.dir = langauage == "ar" ? "rtl" : "ltr";
        var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Home/Home/Home.ftl';
        var cmsurl = huburl + portno + homecms;
        axios.get(cmsurl, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          self.getdata = false;
          var backg = self.pluck('main', self.content.area_List);
          var background = self.pluckcom('Banner', backg[0].component);
          setTimeout(function () { owlcarosl(background); }, 3000);
  
  
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
      });
    },
    
  },
  filters: {
    moment: function (date) {
      return moment(date).format('DD MMM YYYY');
    }
  }

});

function owlcarosl(imageUrl) {
  $('#main_banner').css('background-image', 'url(' + imageUrl + ')');
  $("#owl-demo-2").owlCarousel({
    items: 3,
    lazyLoad: true,
    loop: true,
    margin: 30,
    navigation: true,
    itemsDesktop: [991, 2],
    itemsDesktopSmall: [979, 2],
    itemsTablet: [768, 2],
    itemsMobile: [640, 1],
  });
  $(".owl-prev").html('<i class="fa fa-angle-left"></i>');
  $(".owl-next").html('<i class="fa fa-angle-right"></i>');
}

$(document).ready(function(){
  var active_el = (sessionStorage.active_el) ? sessionStorage.active_el : 1;
  if (active_el == 1) {
    $('.nav-tabs a[href="#flights"]').tab('show');
  }
  else if (active_el == 2) {
    $('.nav-tabs a[href="#hotels"]').tab('show');
  }
  else if (active_el == 3) {
    $('.nav-tabs a[href="#flight-hotle"]').tab('show');
  }
  else {
    $('.nav-tabs a[href="#flights"]').tab('show');
  }
  
});


