var myprofile_vue = new Vue({
    el: "#div_resetpassword",
    data: {
        newPwd: '',
        cnfmPwd: ''
    },
    methods: {
        updatePassword: function () {
            if (this.newPwd == "") {
                alert('New password required !');
                //alertify.alert('Required !', 'New password required !');
                return false;
            }
            if (this.cnfmPwd == "") {
                alert('Conform password required !');
                //alertify.alert('Required !', 'Conform password required !');
                return false;
            }
            if (this.newPwd != this.cnfmPwd) {
                alert('Password mismatch !');
                //alertify.alert('Required !', 'Password mismach !');
                return false;
            }
            var url = window.location.href;
            var token = url.split("reset-password.html?")[1];
            if (token != undefined) {
                var data = {
                    newPassword: this.newPwd
                };
                let axiosConfig = {
                    headers: {
                        "Content-Type": "application/json;charset=UTF-8",
                        Authorization: "Bearer " + token
                    }
                };
                var hubUrls = ServiceUrls;
                var hubUrl = hubUrls.hubConnection.baseUrl;
                var port = hubUrls.hubConnection.ipAddress;
                var resetPassword = hubUrls.hubConnection.hubServices.resetPassword;
                axios.put(hubUrl + port + resetPassword, data, axiosConfig).then(function (res) {
                    alert("Password changed, Please Login.")
                    window.location.href = "/";
                }).catch(function (err) {
                    if (err.response.data.message) {
                        alert(err.response.data.message);
                    } else {
                        alert("We have found some technical difficulties. Please contact admin.");
                    }
                });
            } else {
                alert("Token is invalid. Please contact admin.", function () {
                    window.location.href = "/";
                });
            }
        },

    }
});
