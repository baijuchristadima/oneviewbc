

var myprofile_vue = new Vue({
    el: "#div_add_co_travelers",
    data: {
        User: {
            "id": "",
            "loginid": "",
            "title": {
                "id": "",
                "name": ""
            },
            "firstName": "",
            "lastName": "",
            "status": "",
            "emailId": "",
            "usercontactNumber": {
                "number": "",
                "countryCode": "",
                "telephoneCode": ""
            },
            "contactnumber": "",
            "loginNode": {
                "code": "",
                "name": "",
                "currency": "",
                "city": "",
                "country": {
                    "code": "",
                    "telephonecode": ""
                },
                "url": "",
                "logo": "",
                "homepageurl": "",
                "loginurl": "",
                "address": "",
                "zip": "",
                "email": "",
                "phoneList": [
                    {
                        "type": "",
                        "nodeContactNumber": {
                            "number": ""
                        },
                        "number": ""
                    },
                    {
                        "type": "",
                        "nodeContactNumber": {
                            "number": ""
                        },
                        "number": ""
                    },
                    {
                        "type": "",
                        "nodeContactNumber": {
                            "number": ""
                        },
                        "number": ""
                    }
                ],
                "nodetype": "",
                "solutionId": "",
                "lookNfeel": {
                },
                "tenant": {},
                "site": {
                },
                "servicesList": []
            },
            "roleList": []
        },
        dob_date: { selected: '' }, dob_month: { selected: '' }, dob_year: { selected: '' },
        pExp_date: { selected: '' }, pExp_month: { selected: '' }, pExp_year: { selected: '' },
        pIsue_date: { selected: '' }, pIsue_month: { selected: '' }, pIsue_year: { selected: '' },
        dobMonths: [],
        dobDays: [],
        pExpMonths: [],
        pExpDays: [],
        pIsueMonths: [],
        pIsueDays: [],
        title: {
            selected: '',
            titles: paxTitles
        },
        firstName: '',
        lastName: '',
        passportNumber: '',
        country: {
            selected: '',
            countries: countryList
        }, 
        nationality: {
            selected: '',
            nationalities: countryList
        }
        ,coTravelerisActive:true
        , Page_Title: ''
        , First_Name: ''
        , Last_Name: ''
        , DOB: ''
        , Passport_Number: ''
        , Nationality: ''
        , Issuing_Country: ''
        , Passport_Issue_Date: ''
        , Passport_Expiry_Date: ''
        , Add_Companion: ''
        , Add_MyCompanion: ''
        , Save: ''
        , Cancel: ''
        , Submit: ''
        , Ok: ''
        , Back: ''
        , Day: ''
        , Month: ''
        , Year: ''
        , Technical_Difficulties: ''
        , Companion_Deleted: ''
        , ErrorTitle: ''
        , RequiredTitle: ''
        , MessageTitle: ''
        , FailedTitle: ''
        , Companion_Added: ''
        , Fill_FirstName: ''
        , Fill_LastName: ''
        , Select_DOB: ''
        , Fill_PassportNumber: ''
        , Select_IssuingCountry: ''
        , Select_PassportIssueDate: ''
        , Select_PassportExpiryDate: ''
        , Confirm: ''
        , Confirm_Delete: ''
    },
    computed: {

        dobYears() {
            const year = new Date().getFullYear() + 1;
            var dobYear = new Date().getFullYear() - 99;
            return Array.from({ length: year - dobYear }, (value, index) => dobYear + index);
        },
        pExpYears() {
            const year = new Date().getFullYear()
            var expYear = new Date().getFullYear() + 51;
            return Array.from({ length: expYear - year }, (value, index) => year + index);
        },
        pIsueYears() {
            const year = new Date().getFullYear()
            var IsueYear = new Date().getFullYear() - 100;
            return Array.from({ length: year - IsueYear }, (value, index) => year - index);
        }
    },
    mounted() {
        if (localStorage.IsLogin == 'false') {
            window.location.href = "/";
        } else {
            this.User = JSON.parse(localStorage.User);
            this.dobMonths = Array.from({ length: 12 }, (value, index) => 1 + index);
            this.dobDays = Array.from({ length: 31 }, (value, index) => 1 + index);
            this.pExpMonths = Array.from({ length: 12 }, (value, index) => 1 + index);
            this.pExpDays = Array.from({ length: 31 }, (value, index) => 1 + index);
            this.pIsueMonths = Array.from({ length: 12 }, (value, index) => 1 + index);
            this.pIsueDays = Array.from({ length: 31 }, (value, index) => 1 + index);
            this.title.selected = this.title.titles[0].id;
            this.country.selected = 0;
            this.nationality.selected = 0;
            this.dob_date = 0; this.dob_month = 0; this.dob_year = 0;
            this.pExp_date = 0; this.pExp_month = 0; this.pExp_year = 0;
            this.pIsue_date = 0; this.pIsue_month = 0; this.pIsue_year = 0;
            this.getPagecontent();
        }

    },
    methods: {
        getTitle: function (title) {
            if (title != '') {
                if (title == 1) { title = 'Mr'; }
                if (title == 2) { title = 'Mrs'; }
                if (title == 3) { title = 'Ms'; }
                if (title == 4) { title = 'Miss'; }
                if (title == 5) { title = 'Master'; }
            }
            return title;
        },
        onPexpChange: function (event, input) {
            var month = new Date().getMonth() + 1;
            var date = new Date().getDate();
            if (input == 'years') {
                this.pExpDays = Array.from({ length: this.getDays(this.pExp_month, this.pExp_year) }, (value, index) => 1 + index);
                if (event.target.value == new Date().getFullYear() + 50 && this.pExp_month == new Date().getMonth() + 1
                    || event.target.value == new Date().getFullYear() + 50
                    || event.target.value == new Date().getFullYear()) {
                    this.pExpMonths = Array.from({ length: 13 - month }, (value, index) => month + index);
                    if (event.target.value == new Date().getFullYear()) { date = date + 1 }
                    this.pExpDays = Array.from({ length: (this.getDays(this.pExp_month, this.pExp_year) + 1) - date }, (value, index) => date + index);
                    if (this.pExp_month != new Date().getMonth() + 1) {
                        this.pExpDays = Array.from({ length: this.getDays(this.pExp_month, this.pExp_year) }, (value, index) => 1 + index);
                    }
                }
                else {
                    this.pExpMonths = Array.from({ length: 12 }, (value, index) => 1 + index);
                    this.pExpDays = Array.from({ length: this.getDays(this.pExp_month, this.pExp_year) }, (value, index) => 1 + index);
                }
            }
            else if (input == 'months') {
                if (this.pExp_month == new Date().getMonth() + 1 && this.pExp_year == new Date().getFullYear()) {
                    this.pExpDays = Array.from({ length: (this.getDays(this.pExp_month, this.pExp_year) + 1) - date }, (value, index) => date + index);
                } else {
                    this.pExpDays = Array.from({ length: this.getDays(this.pExp_month, this.pExp_year) }, (value, index) => 1 + index);
                }

            }
            else if (input == 'days') {
                if (this.dob_date >= this.pIsue_date && this.pIsue_month == this.dob_month && this.pIsue_year == this.dob_year) {
                    this.pIsue_date = this.dob_date; this.pIsue_month = this.dob_month; this.pIsue_year = this.dob_year;
                    this.pIsueMonths = Array.from({ length: 13 - this.dob_month }, (value, index) => this.dob_month + index);
                    this.pIsueDays = Array.from({ length: (this.getDays(this.dob_month, this.dob_year) + 1) - this.dob_date }, (value, index) => this.dob_date + index);
                }

            }



        },
        onDobChange: function (event, input) {
            // var month = new Date().getMonth() + 1;
            // var date = new Date().getDate();
            // if (input == 'years') {
            //     this.dobDays = Array.from({ length: this.getDays(this.dob_month, this.dob_year) }, (value, index) => 1 + index);
            //     if (event.target.value == new Date().getFullYear() && this.dob_month == new Date().getMonth() + 1
            //         || event.target.value == new Date().getFullYear()) {
            //         this.dobMonths = Array.from({ length: month }, (value, index) => 1 + index);
            //         if (event.target.value == new Date().getFullYear()) { date = date - 1 }
            //         this.dobDays = Array.from({ length: date }, (value, index) => 1 + index);
            //     }
            //     else {
            //         if (this.dob_date >= this.pIsue_date && this.pIsue_month == this.dob_month && this.pIsue_year == this.dob_year) {
            //             this.pIsue_date = this.dob_date; this.pIsue_month = this.dob_month; this.pIsue_year = this.dob_year;
            //             this.pIsueMonths = Array.from({ length: 13 - this.dob_month }, (value, index) => this.dob_month + index);
            //             this.pIsueDays = Array.from({ length: (this.getDays(this.dob_month, this.dob_year) + 1) - this.dob_date }, (value, index) => this.dob_date + index);
            //         }
            //         else {
            //             if (this.pIsue_date.selected != '' && this.pIsue_month.selected != '' && this.pIsue_year.selected != '') {
            //                 if (this.pIsue_date != this.dob_date && this.pIsue_month != this.dob_month && this.pIsue_year != this.dob_year) {
            //                     this.pIsueMonths = Array.from({ length: 13 - this.pIsue_month }, (value, index) => this.pIsue_month + index);
            //                     this.pIsueDays = Array.from({ length: (this.getDays(this.pIsue_month, this.pIsue_year) + 1) - this.pIsue_date }, (value, index) => this.dob_date + index)
            //                 }

            //             }
            //             else {
            //                 this.dobMonths = Array.from({ length: 12 }, (value, index) => 1 + index);
            //                 this.dobDays = Array.from({ length: this.getDays(this.dob_month, this.dob_year) }, (value, index) => 1 + index);
            //                 this.pIsueMonths = Array.from({ length: 12 }, (value, index) => 1 + index);
            //                 this.pIsueDays = Array.from({ length: (this.getDays(this.pIsue_month, this.pIsue_year)) }, (value, index) => 1 + index)
            //             }

            //         }

            //     }
            //     if (this.dob_date.selected != "" && this.dob_month.selected != "" && this.dob_year.selected != ""
            //         && this.pIsue_date.selected != "" && this.pIsue_month.selected != "" && this.pIsue_year.selected != "") {
            //         if (this.dob_year > this.pIsue_year) {
            //             this.pIsue_date = this.dob_date; this.pIsue_month = this.dob_month; this.pIsue_year = this.dob_year;
            //             this.pIsueMonths = Array.from({ length: 13 - this.dob_month }, (value, index) => this.dob_month + index);
            //             this.pIsueDays = Array.from({ length: (this.getDays(this.dob_month, this.dob_year) + 1) - this.dob_date }, (value, index) => this.dob_date + index);
            //         }
            //     }
            //     if (this.dob_date < this.pIsue_date && this.pIsue_month == this.dob_month && this.pIsue_year == this.dob_year) {
            //         this.pIsue_date = this.dob_date; this.pIsue_month = this.dob_month; this.pIsue_year = this.dob_year;
            //         this.pIsueDays = Array.from({ length: (this.getDays(this.dob_month, this.dob_year) + 1) - this.dob_date }, (value, index) => this.dob_date + index);
            //     }
            // }
            // else if (input == 'months') {
            //     if (this.dob_date < this.pIsue_date && this.pIsue_month == this.dob_month && this.pIsue_year == this.dob_year) {
            //         this.pIsue_date = this.dob_date; this.pIsue_month = this.dob_month; this.pIsue_year = this.dob_year;
            //         this.pIsueDays = Array.from({ length: (this.getDays(this.dob_month, this.dob_year) + 1) - this.dob_date }, (value, index) => this.dob_date + index);
            //     }
            //     this.pIsueMonths = Array.from({ length: 12 }, (value, index) => 1 + index);
            //     this.pIsueDays = Array.from({ length: this.getDays(this.pExp_month, this.pExp_year) }, (value, index) => 1 + index);
            //     this.dobDays = Array.from({ length: this.getDays(this.dob_month, this.dob_year) }, (value, index) => 1 + index);
            //     // if(event.target.value==new Date().getMonth()+1&&this.pExp_year==new Date().getFullYear()+50){
            //     //     this.dobMonths=Array.from({length: month}, (value, index) => 1 + index);
            //     //     this.dobDays=Array.from({length: date}, (value, index) => 1 + index);
            //     // }
            //     // else{
            //     //     this.dobMonths=Array.from({length: 12}, (value, index) => 1 + index);
            //     //     this.dobDays=Array.from({length: 31}, (value, index) => 1 + index);
            //     // }
            // }
            // else if (input == 'days') {
            //     if (this.dob_date >= this.pIsue_date && this.pIsue_month == this.dob_month && this.pIsue_year == this.dob_year) {
            //         this.pIsue_date = this.dob_date; this.pIsue_month = this.dob_month; this.pIsue_year = this.dob_year;
            //         this.pIsueMonths = Array.from({ length: 13 - this.dob_month }, (value, index) => this.dob_month + index);
            //         this.pIsueDays = Array.from({ length: (this.getDays(this.dob_month, this.dob_year) + 1) - this.dob_date }, (value, index) => this.dob_date + index);
            //     }
            //     else {
            //         this.pIsueMonths = Array.from({ length: 12 }, (value, index) => 1 + index);
            //         this.pIsueDays = Array.from({ length: this.getDays(this.pExp_month, this.pExp_year) }, (value, index) => 1 + index);
            //     }

            // }


        },
        onPIsueChange: function (event, input) {
            var month = new Date().getMonth() + 1;
            var date = new Date().getDate();
            if (input == 'years') {
                this.pIsueDays = Array.from({ length: this.getDays(this.pIsue_month, this.pIsue_year) }, (value, index) => 1 + index);
                if (event.target.value == new Date().getFullYear() && this.pIsue_month == new Date().getMonth() + 1
                    || event.target.value == new Date().getFullYear()) {
                    this.pIsueMonths = Array.from({ length: month }, (value, index) => 1 + index);
                    if (event.target.value == new Date().getFullYear()) { date = date - 1 }
                    this.pIsueDays = Array.from({ length: date }, (value, index) => 1 + index);
                }
                // else if(this.dob_date==this.pIsue_date&&this.dob_month==this.pIsue_month&&this.dob_year==this.pIsue_year){
                //     else if(this.pIsue_date>this.dob_date&&this.pIsue_month>=this.dob_month&&this.pIsue_year>=this.dob_year){
                //     this.pIsue_date=this.dob_date;this.pIsue_month=this.dob_month;this.pIsue_year=this.dob_year;
                // }
                else {
                    this.pIsueMonths = Array.from({ length: 12 }, (value, index) => 1 + index);
                    this.pIsueDays = Array.from({ length: this.getDays(this.pIsue_month, this.pIsue_year) }, (value, index) => 1 + index);
                }
                if (this.pIsue_date.selected != "" && this.pIsue_month.selected != "" && this.pIsue_year.selected != ""
                    && this.dob_date.selected != "" && this.dob_month.selected != "" && this.dob_year.selected != "") {
                    if (this.pIsue_date >= this.dob_date && this.pIsue_month >= this.dob_month && this.pIsue_year >= this.dob_year) {
                        // this.pIsueMonths=Array.from({length: 12}, (value, index) => 1 + index);
                        // this.pIsueDays=Array.from({length: 31}, (value, index) => 1 + index);
                    }
                    else {
                        this.pIsue_date = this.dob_date; this.pIsue_month = this.dob_month; this.pIsue_year = this.dob_year;
                        this.pIsueMonths = Array.from({ length: 13 - this.dob_month }, (value, index) => this.dob_month + index);
                        this.pIsueDays = Array.from({ length: (this.getDays(this.pIsue_month, this.pIsue_year) + 1) - this.dob_date }, (value, index) => this.dob_date + index);
                    }

                }

            }
            else if (input == 'months') {
                this.pIsueDays = Array.from({ length: this.getDays(this.pIsue_month, this.pIsue_year) }, (value, index) => 1 + index);
                //     if(event.target.value==new Date().getMonth()+1&&this.pExp_year==new Date().getFullYear()+50){
                //         this.pIsueMonths=Array.from({length: month}, (value, index) => 1 + index);
                //         this.pIsueDays=Array.from({length: date}, (value, index) => 1 + index);
                //     }
                //     else{
                //         this.pIsueMonths=Array.from({length: 12}, (value, index) => 1 + index);
                //         this.pIsueDays=Array.from({length: 31}, (value, index) => 1 + index);
                //     }
            }


        },
        addCoTraveller: function () {
            var self = this;
            if (this.validation()) {
                var paxtype = '';
                if (this.title.selected == 1 || this.title.selected == 2 || this.title.selected == 3) { paxtype = 'ADT'; } else { paxtype = 'CHD'; };
                if (this.dob_date < 10) {
                    this.dob_date = '0' + this.dob_date
                }
                if (this.dob_month < 10) {
                    this.dob_month = '0' + this.dob_month
                }
                if (this.pIsue_date < 10) {
                    this.pIsue_date = '0' + this.pIsue_date
                }
                if (this.pIsue_month < 10) {
                    this.pIsue_month = '0' + this.pIsue_month
                }
                if (this.pExp_date < 10) {
                    this.pExp_date = '0' + this.pExp_date
                }
                if (this.pExp_month < 10) {
                    this.pExp_month = '0' + this.pExp_month
                }
                var datas = {
                    title: this.title.selected,
                    firstName: this.firstName,
                    lastName: this.lastName,
                    nationality: this.country.selected,
                    paxType: paxtype,
                    dob: this.dob_year + '-' + this.dob_month + '-' + this.dob_date,
                    passportNumber: this.passportNumber,
                    issuingCountry: this.country.selected,
                    passportIssueDate: this.pIsue_year + '-' + this.pIsue_month + '-' + this.pIsue_date,
                    passportExpDate: this.pExp_year + '-' + this.pExp_month + '-' + this.pExp_date,
                    lead: 0,
                    frequentFlyerList: [
                        {
                            frequentFlyerCode: null,
                            airLine: {
                                code: null
                            }
                        }
                    ],
                };
                let axiosConfig = {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Authorization': "Bearer " + localStorage.access_token
                    }
                };
                console.log('AddCoTravellerReq: ', JSON.stringify(datas));
                var huburl = ServiceUrls.hubConnection.baseUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var requrl = ServiceUrls.hubConnection.hubServices.addCoTraveller;
                axios.post(huburl + portno + requrl, datas, axiosConfig)
                    .then((res) => {
                        console.log("RESPONSE RECEIVED: ", res);
                        alertify.alert(self.MessageTitle, self.Companion_Added).setting({ 'closable': false, 'label':self.Ok, onclose: function () { window.location = '/co-travelers.html'; } });
                    }).catch((err) => {
                        console.log("AXIOS ERROR: ", err);
                        alertify.alert(self.FailedTitle, self.Technical_Difficulties).set('label', self.Ok);                        
                    })
            }
        },
        validation: function () {
            if (this.firstName == "") {
                alertify.alert(this.RequiredTitle, this.Fill_FirstName).set('label', this.Ok);
                return false;
            }
            if (this.lastName == "") {
                alertify.alert(this.RequiredTitle, this.Fill_LastName).set('label', this.Ok);
                return false;
            }
            if (this.dob_date==0 || this.dob_month==0 || this.dob_year==0) {
                alertify.alert(this.RequiredTitle, this.Select_DOB).set('label', this.Ok);
                return false;
            }

            if (this.passportNumber == "") {
                alertify.alert(this.RequiredTitle, this.Fill_PassportNumber).set('label', this.Ok);
                return false;
            }
            if (this.country.selected==0) {
                alertify.alert(this.RequiredTitle, this.Select_IssuingCountry).set('label', this.Ok);
                return false;
            }
            if (this.pIsue_date==0 || this.pIsue_month==0 || this.pIsue_year==0) {
                alertify.alert(this.RequiredTitle, this.Select_PassportIssueDate).set('label', this.Ok);
                return false;
            }
            if (this.pExp_date==0 || this.pExp_month==0 || this.pExp_year==0) {
                alertify.alert(this.RequiredTitle, this.Select_PassportExpiryDate).set('label', this.Ok);
                return false;
            }
            return true;
        },
        getDays: function (month, year) {
            var days = '';
            if (month != null && month.selected != '') {
                if (month == 4 || month == 6 || month == 9 || month == 11) { days = 30 }
                else if (month == 2) { if (year != '') { if (month == 2 && this.leapyear(year)) { days = 29 } else { days = 28 } } else { days = 28 } }
                else { days = 31 }
            } else { days = 31 }
            return days;
        },
        leapyear: function (year) {
            return (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0);
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var Agencycode = 'AGY435';
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/My Companion/My Companion/My Companion.ftl';
                axios.get(aboutUsPage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (response.data.area_List.length > 0) {
                        var mainComp = response.data.area_List[0].main;
                        self.My_Dashboard = self.pluckcom('My_Dashboard', mainComp.component);
                        self.Page_Title = self.pluckcom('Page_Title', mainComp.component);
                        self.First_Name = self.pluckcom('First_Name', mainComp.component);
                        self.Last_Name = self.pluckcom('Last_Name', mainComp.component);
                        self.DOB = self.pluckcom('DOB', mainComp.component);
                        self.Passport_Number = self.pluckcom('Passport_Number', mainComp.component);
                        self.Issuing_Country = self.pluckcom('Issuing_Country', mainComp.component);
                        self.Nationality = self.pluckcom('Nationality', mainComp.component);
                        self.Passport_Issue_Date = self.pluckcom('Passport_Issue_Date', mainComp.component);
                        self.Passport_Expiry_Date = self.pluckcom('Passport_Expiry_Date', mainComp.component);
                        self.Add_Companion = self.pluckcom('Add_Companion', mainComp.component);
                        self.Add_MyCompanion = self.pluckcom('Add_MyCompanion', mainComp.component);
                        self.Save = self.pluckcom('Save', mainComp.component);
                        self.Cancel = self.pluckcom('Cancel', mainComp.component);
                        self.Submit = self.pluckcom('Submit', mainComp.component);
                        self.Ok = self.pluckcom('Ok', mainComp.component);
                        self.Back = self.pluckcom('Back', mainComp.component);
                        self.Day = self.pluckcom('Day', mainComp.component);
                        self.Month = self.pluckcom('Month', mainComp.component);
                        self.Year = self.pluckcom('Year', mainComp.component);
                        self.Technical_Difficulties = self.pluckcom('Technical_Difficulties', mainComp.component);
                        self.Companion_Deleted = self.pluckcom('Companion_Deleted', mainComp.component);
                        self.ErrorTitle = self.pluckcom('ErrorTitle', mainComp.component);
                        self.RequiredTitle = self.pluckcom('RequiredTitle', mainComp.component);
                        self.MessageTitle = self.pluckcom('MessageTitle', mainComp.component);
                        self.FailedTitle = self.pluckcom('FailedTitle', mainComp.component);
                        self.Companion_Added = self.pluckcom('Companion_Added', mainComp.component);
                        self.Fill_FirstName = self.pluckcom('Fill_FirstName', mainComp.component);
                        self.Fill_LastName = self.pluckcom('Fill_LastName', mainComp.component);
                        self.Select_DOB = self.pluckcom('Select_DOB', mainComp.component);
                        self.Fill_PassportNumber = self.pluckcom('Fill_PassportNumber', mainComp.component);
                        self.Select_IssuingCountry = self.pluckcom('Select_IssuingCountry', mainComp.component);
                        self.Select_PassportIssueDate = self.pluckcom('Select_PassportIssueDate', mainComp.component);
                        self.Select_PassportExpiryDate = self.pluckcom('Select_PassportExpiryDate', mainComp.component);
                        self.Confirm = self.pluckcom('Confirm', mainComp.component);
                        self.Confirm_Delete = self.pluckcom('Confirm_Delete', mainComp.component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        getmoreinfo(url) {

            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url = "/Nirvana/book-now.html?page=" + url;
            }
            else {
                url = "#";
            }
            return url;
        }
    }

});