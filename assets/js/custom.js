$(document).ready(function() { $("#hide, #hide1, #hide2, #hide3, #hide4, #hide5, #hide6, #hide7, #hide8, #hide9, #hide10, #hide11, #hide12, #hide13").click(function() { $(".tabcontent").hide(); }); });
$("ul.nav-tabs a").click(function(e) {
    e.preventDefault();
    $(this).tab('show');
});
$("#modal_retrieve").leanModal({ top: 100, overlay: 0.6, closeButton: ".modal_close" });
$("#fare_change").leanModal({ top: 100, overlay: 0.6, closeButton: ".modal_close" });
$(function() { $('.selectpicker').selectpicker(); });
jQuery(document).ready(function($) {
    $('.myselect').select2({ minimumResultsForSearch: Infinity, 'width': '100%' });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>');
});
$(document).ready(function() {});

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 110 || charCode == 190 || charCode == 46)
        return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
var startValue = 1010;
var endValue = 500000;
var minValue = 1000;
var maxValue = 500000;
$("#slider-container").slider({
    range: true,
    min: minValue,
    max: maxValue,
    values: [startValue, endValue],
    create: function() {
        $("#amount-from").val(startValue);
        $("#amount-to").val(endValue);
    },
    slide: function(event, ui) {
        $("#amount-from").val(ui.values[0]);
        $("#amount-to").val(ui.values[1]);
        var from = $("#amount-from").val();
        var to = $("#amount-to").val();
        console.log(from + " --- " + to);
    }
});
$(document).on('click', '.number-spinner button', function() {
    var btn = $(this),
        oldValue = btn.closest('.number-spinner').find('input').val().trim(),
        newVal = 0;
    if (btn.attr('data-dir') == 'up') { newVal = parseInt(oldValue) + 1; } else { if (oldValue > 1) { newVal = parseInt(oldValue) - 1; } else { newVal = 1; } }
    btn.closest('.number-spinner').find('input').val(newVal);
});
$(document).ready(function() {
    $(window).scroll(function() { if ($(this).scrollTop() > 100) { $('.scrollToTop').fadeIn(); } else { $('.scrollToTop').fadeOut(); } });
    $('.scrollToTop').click(function() { $('html, body').animate({ scrollTop: 0 }, 800); return false; });
});
jQuery(document).ready(function($) {
    $('.myselect').select2({ minimumResultsForSearch: Infinity, 'width': '100%' });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa  fa-chevron-down"></i>');
});
jQuery(document).ready(function($) {
    $('.myselect-2').select2({ minimumResultsForSearch: Infinity, 'width': '100%' });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('');
});
$('a[href^="#select-room"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if (target.length) {
        event.preventDefault();
        $('html, body').stop().animate({ scrollTop: target.offset().top }, 1000);
    }
});
$(document).ready(function() {
    $("#showHide, #showHide2, #showHide3").hide();
    $(".showHideButton, .showHideButton2, .showHideButton3").click(function() { $("#showHide, #showHide2, #showHide3").slideToggle(500); if ($(this).text() == 'Show') { $(this).text('Advanced Search'); } else { $(this).text('Advanced Search'); } });
    $("#showHide4, #showHide5").hide();
    $(".showHideButton4, .showHideButton5").click(function() { $("#showHide4, #showHide5").slideToggle(500); if ($(this).text() == 'Show') { $(this).text('Select different check in & checkout date'); } else { $(this).text('Select different check in & checkout date'); } });
    $("#showHide6").hide();
    $(".showHideButton6").click(function() { $("#showHide6").slideToggle(500); });
    $("#showHide7").hide();
    $(".showHideButton7").click(function() { $("#showHide7").slideToggle(500); if ($(this).text() == 'Show') { $(this).text('Hide'); } else { $(this).text('Show'); } });
    $("#showHide8").hide();
    $(".showHideButton8").click(function() { $("#showHide8").slideToggle(500); if ($(this).text() == 'Show') { $(this).text('Hide'); } else { $(this).text('Show'); } });
});

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) { tabcontent[i].style.display = "none"; }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) { tablinks[i].className = tablinks[i].className.replace(" active", ""); }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
$(document).ready(function($) {
    $('.tab_content').hide();
    $('.tab_content:first').show();
    $('.tabs li').click(function(event) {
        $(this).toggleClass('active');
        $(this).siblings().removeClass('active');
        var selectTab = $(this).find('a').attr("href");
        $(selectTab).fadeIn();
    });
});
var affixElement = '.summary-detail';
$(affixElement).affix({ offset: { top: function() { return (this.top = $(affixElement).offset().top) } } });