var myprofile_vue = new Vue({
  el: "#div_CoTravellers",
  data: {
    User: {
      "id": "",
      "loginid": "",
      "title": {
        "id": "",
        "name": ""
      },
      "firstName": "",
      "lastName": "",
      "status": "",
      "emailId": ""
    },
    cotravellerList: [],
    cotraveller: {
      firstName: '',
      lastName: '',
      dob: '',
      passportNumber: '',
      issuingCountry: '',
      passportIssueDate: '',
      passportExpiryDate: '',
      nationality: '',
    },
    axiosConfig: {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': 'Bearer ' + localStorage.access_token
      }
    },
    huburl: ServiceUrls.hubConnection.baseUrl,
    portno: ServiceUrls.hubConnection.ipAddress    
    , Page_Title: ''
    , First_Name: ''
    , Last_Name: ''
    , DOB: ''
    , Passport_Number: ''
    , Issuing_Country: ''
    , Passport_Issue_Date: ''
    , Passport_Expiry_Date: ''
    , Add_Companion: ''
    , Add_MyCompanion: ''
    , Save: ''
    , Cancel: ''
    , Submit: ''
    , Ok: ''
    , Back: ''
    , Day: ''
    , Month: ''
    , Year: ''
    , Technical_Difficulties: ''
    , Companion_Deleted: ''
    , ErrorTitle: ''
    , RequiredTitle: ''
    , MessageTitle: ''
    , FailedTitle: ''
    , Companion_Added: ''
    , Fill_FirstName: ''
    , Fill_LastName: ''
    , Select_DOB: ''
    , Fill_PassportNumber: ''
    , Select_IssuingCountry: ''
    , Select_PassportIssueDate: ''
    , Select_PassportExpiryDate: ''
    , Confirm: ''
    , Confirm_Delete: ''
  },
  mounted() {
    if (localStorage.IsLogin == 'false') {
      window.location.href = "/";
    } else {
      this.User = JSON.parse(localStorage.User);
      this.getPagecontent();
    }

  },
  created: function () {
    this.getCotravellers();
  },
  methods: {
    getTitle: function (title) {
      if (title != '') {
        if (title == 1) { title = 'Mr'; }
        if (title == 2) { title = 'Mrs'; }
        if (title == 3) { title = 'Ms'; }
        if (title == 4) { title = 'Miss'; }
        if (title == 5) { title = 'Master'; }
      }
      return title;
    },
    getCotravellers: function () {
      var requrl = ServiceUrls.hubConnection.hubServices.getCoTravellers;
      axios.get(this.huburl + this.portno + requrl, this.axiosConfig)
        .then((res) => {
          console.log("RESPONSE RECEIVED: ", res);
          this.cotravellerList = res.data.filter(function(e){
            return e.lead == false;
          });

        })
        .catch((err) => {
          console.log("AXIOS ERROR: ", err);
          alertify.alert(this.ErrorTitle, this.Technical_Difficulties).set('label', this.Ok);
        })
    },
    ViewCotraveller: function (user) {
      this.cotraveller.firstName = this.getUserTitleName(user.title) + ' ' + user.firstName,
        this.cotraveller.lastName = user.lastName,
        this.cotraveller.dob = user.dob,
        this.cotraveller.passportNumber = user.passportNumber,
        this.cotraveller.issuingCountry = user.issuingCountry,
        this.cotraveller.passportIssueDate = user.passportIssueDate,
        this.cotraveller.passportExpiryDate = user.passportExpDate,
        this.cotraveller.nationality = user.nationality,
        localStorage.cotraveller = JSON.stringify(this.cotraveller);
      window.location.href = "/view-co-traveller.html";
    },
    deleteCotraveller: function (userId) {
      var self = this;
      alertify.confirm(self.Confirm, self.Confirm_Delete
        , function () {
          var requrl = ServiceUrls.hubConnection.hubServices.deleteCoTraveller;
          axios.delete(self.huburl + self.portno + requrl + '/' + userId, self.axiosConfig)
            .then((res) => {
              console.log("RESPONSE RECEIVED: ", res);
              alertify.alert(self.MessageTitle, self.Companion_Deleted).set('label', self.Ok);
              self.getCotravellers();
            })
            .catch((err) => {
              console.log("AXIOS ERROR: ", err);
              alertify.alert(self.FailedTitle, self.Technical_Difficulties).set('label', self.Ok);
            });
        }
        , function () {
        }).set({ 'closable': false, 'labels': { ok: self.Submit, cancel: self.Cancel } });
    },
    getUserName: function (Item) {
      var title = '';
      if (Item.title == 1) { title = 'Mr'; }
      if (Item.title == 2) { title = 'Mrs'; }
      if (Item.title == 3) { title = 'Ms'; }
      if (Item.title == 4) { title = 'Miss'; }
      if (Item.title == 5) { title = 'Master'; }
      return title + ' ' + Item.firstName + ' ' + Item.lastName
    },
    getUserTitleName: function (title) {
      if (title == 1) { title = 'Mr'; }
      if (title == 2) { title = 'Mrs'; }
      if (title == 3) { title = 'Ms'; }
      if (title == 4) { title = 'Miss'; }
      if (title == 5) { title = 'Master'; }
      return title;
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;

      getAgencycode(function (response) {
        var Agencycode = 'AGY435';
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        self.dir = langauage == "ar" ? "rtl" : "ltr";
        var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/My Companion/My Companion/My Companion.ftl';
        axios.get(aboutUsPage, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          self.getdata = false;
          if (response.data.area_List.length > 0) {
            var mainComp = response.data.area_List[0].main;
            self.My_Dashboard = self.pluckcom('My_Dashboard', mainComp.component);
            self.Page_Title = self.pluckcom('Page_Title', mainComp.component);
            self.First_Name = self.pluckcom('First_Name', mainComp.component);
            self.Last_Name = self.pluckcom('Last_Name', mainComp.component);
            self.DOB = self.pluckcom('DOB', mainComp.component);
            self.Passport_Number = self.pluckcom('Passport_Number', mainComp.component);
            self.Issuing_Country = self.pluckcom('Issuing_Country', mainComp.component);
            self.Passport_Issue_Date = self.pluckcom('Passport_Issue_Date', mainComp.component);
            self.Passport_Expiry_Date = self.pluckcom('Passport_Expiry_Date', mainComp.component);
            self.Add_Companion = self.pluckcom('Add_Companion', mainComp.component);
            self.Add_MyCompanion = self.pluckcom('Add_MyCompanion', mainComp.component);
            self.Save = self.pluckcom('Save', mainComp.component);
            self.Cancel = self.pluckcom('Cancel', mainComp.component);
            self.Submit = self.pluckcom('Submit', mainComp.component);
            self.Ok = self.pluckcom('Ok', mainComp.component);
            self.Back = self.pluckcom('Back', mainComp.component);
            self.Day = self.pluckcom('Day', mainComp.component);
            self.Month = self.pluckcom('Month', mainComp.component);
            self.Year = self.pluckcom('Year', mainComp.component);
            self.Technical_Difficulties = self.pluckcom('Technical_Difficulties', mainComp.component);
            self.Companion_Deleted = self.pluckcom('Companion_Deleted', mainComp.component);
            self.ErrorTitle = self.pluckcom('ErrorTitle', mainComp.component);
            self.RequiredTitle = self.pluckcom('RequiredTitle', mainComp.component);
            self.MessageTitle = self.pluckcom('MessageTitle', mainComp.component);
            self.FailedTitle = self.pluckcom('FailedTitle', mainComp.component);
            self.Companion_Added = self.pluckcom('Companion_Added', mainComp.component);
            self.Fill_FirstName = self.pluckcom('Fill_FirstName', mainComp.component);
            self.Fill_LastName = self.pluckcom('Fill_LastName', mainComp.component);
            self.Select_DOB = self.pluckcom('Select_DOB', mainComp.component);
            self.Fill_PassportNumber = self.pluckcom('Fill_PassportNumber', mainComp.component);
            self.Select_IssuingCountry = self.pluckcom('Select_IssuingCountry', mainComp.component);
            self.Select_PassportIssueDate = self.pluckcom('Select_PassportIssueDate', mainComp.component);
            self.Select_PassportExpiryDate = self.pluckcom('Select_PassportExpiryDate', mainComp.component);
            self.Confirm = self.pluckcom('Confirm', mainComp.component);
            self.Confirm_Delete = self.pluckcom('Confirm_Delete', mainComp.component);
          }
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
      });

    },
    getmoreinfo(url) {

      if (url != "") {
        url = url.split("/Template/")[1];
        url = url.split(' ').join('-');
        url = url.split('.').slice(0, -1).join('.')
        url = "/Nirvana/book-now.html?page=" + url;
      }
      else {
        url = "#";
      }
      return url;
    }
  }

});