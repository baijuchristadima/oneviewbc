var huburl = ServiceUrls.hubConnection.baseUrl;
var portno = ServiceUrls.hubConnection.ipAddress;
var pgPurchase = ServiceUrls.hubConnection.hubServices.pgPurchase;
var pgPurchaseDone = ServiceUrls.hubConnection.hubServices.pgPurchaseDone;

$(document).ready(function () {
    if (localStorage.bkRefNo) {
       
        if (getQueryStringParameterByName('fromwhere') != null) {
            //2. Purchase
            if (getQueryStringParameterByName('fromwhere') == "aftpur") {
                if (getQueryStringParameterByName('status') != null) {
                    if (getQueryStringParameterByName('status') == "02" || getQueryStringParameterByName('status') == "14") {
                        if (getQueryStringParameterByName('response_code') != null) {
                            if (getQueryStringParameterByName('response_code') == "02000" || getQueryStringParameterByName('response_code') == "14000") {
                                if (getQueryStringParameterByName('merchant_reference') != null) {
                                    Success();
                                }
                            }
                            else {
                                Failed();
                            }
                        }
                        else {
                            Failed();
                        }
                    }
                    else {
                        Failed();
                    }
                }
            }
        } else {
            //1. Merch Integration After Tokenization
            if (getQueryStringParameterByName('status') != null) {
                if (getQueryStringParameterByName('status') == "18") {
                    if (getQueryStringParameterByName('response_code') != null) {
                        if (getQueryStringParameterByName('response_code') == "18000") {
                            //Tokenization Success
                            PayFortPurchase();
                        }
                    }
                }
            }
        }


    }
});

function PayFortPurchase() {
   
    var query_stringUo = QueryStringToJSON();
    console.log(query_stringUo)

    var query_stringO = {};
    var GenKey = '';

    Object.keys(query_stringUo).sort().forEach(function (key) {
        if (key != "signature" && key != "fromwhere") {
            query_stringO[key] = query_stringUo[key];
            GenKey += key + '=' + query_stringUo[key];
        }
    });
    console.log(query_stringO);
    console.log(GenKey);
    GenSignature(GenKey, 'res', 9).then(data => {
        if (data.shaString != undefined) {
            var shaString = data.shaString;
            console.log(shaString);
            if (query_stringUo.signature == data.shaString.toLowerCase()) {
                var ipUrl = ServiceUrls.externalServiceUrl.ipAddressApi;
                fetch(ipUrl)
                .then(x => x.json())
                .then(({ ip }) => {
                    // let Ipaddress = ip;
                    // let sessionids = ip.replace(/\./g, '');
                    // getUserIP(function (ip) {
                        //Purchase
                        // var device_fingerprint = $('#device_fingerprint').val();
                        var customerIP = ip;
                       
                        var rq = {
                            "merchant_reference": localStorage.cartId,
                            "customer_email": localStorage.cartEmailId,
                            "token_name": getQueryStringParameterByName('token_name'),
                            "customer_ip": customerIP,
                            "return_url": location.origin + "/payresponse.html?fromwhere=aftpur",
                            //"device_fingerprint": device_fingerprint,
                            "paymentGatwayId": 9,
                        }
    
                        axios.post(huburl + portno + pgPurchase, rq, {
                            headers: { 'Authorization': 'Bearer ' + localStorage.access_token }
                        }).then(function (response) {
                            localStorage.access_token = response.headers.access_token;
                            console.log(response);
                            if (response.data.response_code == "20064" || response.data.status == "20") {
                                window.parent.location.href = response.data["3ds_url"];
                            } else if (response.data.status == "02" || response.data.status == "14") {
                                if (response.data.response_code == "02000" || response.data.status == "14000") {
                                    Success();
                                }
                            }
                            else {
                                sessionStorage.amountPG = response.data.amount;
                                Failed();
                            }
    
                        }).catch(function (error) {
                            console.log('Error on Payment Gateway Hub - ' + error);
                        });
                    });
                // });
            }
              

            // }
        }
    })
}

function Success() {
    console.log('Payment Gateway Success');
    var postrq = {
        "cartId": getQueryStringParameterByName('merchant_reference'),
        "serviceId": localStorage.serviceId,
        "paymentGwId": JSON.parse(localStorage.User).loginNode.paymentGateways[0].id,
        "amount": getQueryStringParameterByName('amount'),
        "isSuccess": true,
        "method": "merchent",
        "errorMsg": "no-error"
    }

    axios.post(huburl + portno + pgPurchaseDone, postrq, {
        headers: { 'Authorization': 'Bearer ' + localStorage.access_token }
    }).then(function (response) {
        localStorage.access_token = response.headers.access_token;
        if (localStorage.serviceId == 1) {
            window.parent.location.href = "/Flights/flight-confirmation.html?status=101";
        }else {
            window.parent.location.href = "/Hotels/hotel-detail.html#/hotelConfirmation?status=101";
        }
        console.log(response);
    }).catch(function (error) {
        console.log('Error on Payment Gateway Hub - ' + error);
    });
}

function Failed() {
    console.log('Payment Gateway Failed');
    var postrq = {
        "cartId": getQueryStringParameterByName('merchant_reference'),
        "serviceId": localStorage.serviceId,
        "paymentGwId": JSON.parse(localStorage.User).loginNode.paymentGateways[0].id,
        "amount": getQueryStringParameterByName('amount') || sessionStorage.amountPG,
        "isSuccess": false,
        "method": "merchent",
        "errorMsg": getQueryStringParameterByName('response_message')
    }

    axios.post(huburl + portno + pgPurchaseDone, postrq, {
        headers: { 'Authorization': 'Bearer ' + localStorage.access_token }
    }).then(function (response) {
        window.sessionStorage.removeItem('amountPG');
        localStorage.access_token = response.headers.access_token;
        if (localStorage.serviceId == 1) {
            window.parent.location.href = "/Flights/flight-confirmation.html?status=102";
        } else {
            window.parent.location.href = "/Hotels/hotel-detail.html#/hotelConfirmation?status=102";
        }
        console.log(response);
    }).catch(function (error) {
        console.log('Error on Payment Gateway Hub - ' + error);
    });
}