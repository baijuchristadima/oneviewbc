
var myprofile_vue = new Vue({
  el: "#div_view_cotraveller",
  data: {
    User: {
      "id": "",
      "loginid": "",
      "title": {
        "id": "",
        "name": ""
      },
      "firstName": "",
      "lastName": "",
      "status": "",
      "emailId": "",
      "usercontactNumber": {
        "number": "",
        "countryCode": "",
        "telephoneCode": ""
      },
      "contactnumber": "",
      "loginNode": {
        "code": "",
        "name": "",
        "currency": "",
        "city": "",
        "country": {
          "code": "",
          "telephonecode": ""
        },
        "url": "",
        "logo": "",
        "homepageurl": "",
        "loginurl": "",
        "address": "",
        "zip": "",
        "email": "",
        "phoneList": [
          {
            "type": "",
            "nodeContactNumber": {
              "number": ""
            },
            "number": ""
          },
          {
            "type": "",
            "nodeContactNumber": {
              "number": ""
            },
            "number": ""
          },
          {
            "type": "",
            "nodeContactNumber": {
              "number": ""
            },
            "number": ""
          }
        ],
        "nodetype": "",
        "solutionId": "",
        "lookNfeel": {
        },
        "tenant": {},
        "site": {
        },
        "servicesList": []
      },
      "roleList": []
    },
    cotraveller: {
      firstName: '',
      lastName: '',
      dob: '',
      passportNumber: '',
      issuingCountry: '',
      passportIssueDate: '',
      passportExpiryDate: '',
      nationality: ''
    }
    , Page_Title: ''
    , First_Name: ''
    , Last_Name: ''
    , DOB: ''
    , Passport_Number: ''
    , Nationality: ''
    , Issuing_Country: ''
    , Passport_Issue_Date: ''
    , Passport_Expiry_Date: ''
    , Add_Companion: ''
    , Add_MyCompanion: ''
    , Save: ''
    , Cancel: ''
    , Back: ''
    , Day: ''
    , Month: ''
    , Year: ''
    , Technical_Difficulties: ''
    , Companion_Deleted: ''
    , ErrorTitle: ''
    , RequiredTitle: ''
    , MessageTitle: ''
    , FailedTitle: ''
    , Companion_Added: ''
    , Fill_FirstName: ''
    , Fill_LastName: ''
    , Select_DOB: ''
    , Fill_PassportNumber: ''
    , Select_IssuingCountry: ''
    , Select_PassportIssueDate: ''
    , Select_PassportExpiryDate: ''
  },
  mounted() {
    if (localStorage.IsLogin == 'false') {
      window.location.href = "/";
    } else {
      this.User = JSON.parse(localStorage.User);
      this.cotraveller = JSON.parse(localStorage.cotraveller);
      this.getPagecontent();
    }
  },
  methods: {
    getTitle: function (title) {
      if (title != '') {
        if (title == 1) { title = 'Mr'; }
        if (title == 2) { title = 'Mrs'; }
        if (title == 3) { title = 'Ms'; }
        if (title == 4) { title = 'Miss'; }
        if (title == 5) { title = 'Master'; }
      }
      return title;
    },
    getCountryName: function (input) {
      var countries = countryList.filter(function (country) { return country.twoLetter.toUpperCase() == input.toUpperCase() });;
      { try { return countries[0].name; } catch (e) { return ""; } }
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;

      getAgencycode(function (response) {
        var Agencycode = 'AGY435';
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        self.dir = langauage == "ar" ? "rtl" : "ltr";
        var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/My Companion/My Companion/My Companion.ftl';
        axios.get(aboutUsPage, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          self.getdata = false;
          if (response.data.area_List.length > 0) {
            var mainComp = response.data.area_List[0].main;
            self.My_Dashboard = self.pluckcom('My_Dashboard', mainComp.component);
            self.Page_Title = self.pluckcom('Page_Title', mainComp.component);
            self.First_Name = self.pluckcom('First_Name', mainComp.component);
            self.Last_Name = self.pluckcom('Last_Name', mainComp.component);
            self.DOB = self.pluckcom('DOB', mainComp.component);
            self.Passport_Number = self.pluckcom('Passport_Number', mainComp.component);
            self.Issuing_Country = self.pluckcom('Issuing_Country', mainComp.component);
            self.Nationality = self.pluckcom('Nationality', mainComp.component);
            self.Passport_Issue_Date = self.pluckcom('Passport_Issue_Date', mainComp.component);
            self.Passport_Expiry_Date = self.pluckcom('Passport_Expiry_Date', mainComp.component);
            self.Add_Companion = self.pluckcom('Add_Companion', mainComp.component);
            self.Add_MyCompanion = self.pluckcom('Add_MyCompanion', mainComp.component);
            self.Save = self.pluckcom('Save', mainComp.component);
            self.Cancel = self.pluckcom('Cancel', mainComp.component);
            self.Back = self.pluckcom('Back', mainComp.component);
            self.Day = self.pluckcom('Day', mainComp.component);
            self.Month = self.pluckcom('Month', mainComp.component);
            self.Year = self.pluckcom('Year', mainComp.component);
            self.Technical_Difficulties = self.pluckcom('Technical_Difficulties', mainComp.component);
            self.Companion_Deleted = self.pluckcom('Companion_Deleted', mainComp.component);
            self.ErrorTitle = self.pluckcom('ErrorTitle', mainComp.component);
            self.RequiredTitle = self.pluckcom('RequiredTitle', mainComp.component);
            self.MessageTitle = self.pluckcom('MessageTitle', mainComp.component);
            self.FailedTitle = self.pluckcom('FailedTitle', mainComp.component);
            self.Companion_Added = self.pluckcom('Companion_Added', mainComp.component);
            self.Fill_FirstName = self.pluckcom('Fill_FirstName', mainComp.component);
            self.Fill_LastName = self.pluckcom('Fill_LastName', mainComp.component);
            self.Select_DOB = self.pluckcom('Select_DOB', mainComp.component);
            self.Fill_PassportNumber = self.pluckcom('Fill_PassportNumber', mainComp.component);
            self.Select_IssuingCountry = self.pluckcom('Select_IssuingCountry', mainComp.component);
            self.Select_PassportIssueDate = self.pluckcom('Select_PassportIssueDate', mainComp.component);
            self.Select_PassportExpiryDate = self.pluckcom('Select_PassportExpiryDate', mainComp.component);
          }
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
      });

    },
    getmoreinfo(url) {

      if (url != "") {
        url = url.split("/Template/")[1];
        url = url.split(' ').join('-');
        url = url.split('.').slice(0, -1).join('.')
        url = "/Nirvana/book-now.html?page=" + url;
      }
      else {
        url = "#";
      }
      return url;
    }
  }
});