
var myprofile_vue = new Vue({
  el: "#div_myProfile",
  data: {
    User: {
      "id": "",
      "loginid": "",
      "title": {
        "id": "",
        "name": ""
      },
      "firstName": "",
      "lastName": "",
      "status": "",
      "emailId": "",
      "usercontactNumber": {
        "number": "",
        "countryCode": "",
        "telephoneCode": ""
      },
      "userAdress": {
        "line1": "",
        "line2": "",
        "zip": "",
        "city": ""
      },
      "contactnumber": "",
      "loginNode": {
        "code": "",
        "name": "",
        "currency": "",
        "city": "",
        "country": {
          "code": "",
          "telephonecode": ""
        },
        "url": "",
        "logo": "",
        "homepageurl": "",
        "loginurl": "",
        "address": "",
        "zip": "",
        "email": "",
        "phoneList": [
          {
            "type": "",
            "nodeContactNumber": {
              "number": ""
            },
            "number": ""
          },
          {
            "type": "",
            "nodeContactNumber": {
              "number": ""
            },
            "number": ""
          },
          {
            "type": "",
            "nodeContactNumber": {
              "number": ""
            },
            "number": ""
          }
        ],
        "nodetype": "",
        "solutionId": "",
        "lookNfeel": {
        },
        "tenant": {},
        "site": {
        },
        "servicesList": []
      },
      "roleList": []
    },
    Page_Subtitle: '',
    Username: '',
    Full_name: '',
    Phone: '',
    Email: '',
    Address: '',
    Country: '',
    zip: '',
    edit: '',
    Date_Of_Birth: '',
    Nationality: '',
    Passport_Number: '',
    Expire_Date: '',
    Issue_Date: '',
    Issue_Country: '',
    Frequent_Flyer_Program: '',
    Frequent_Flyer_Number: '',
    key: 0,
    arisActive: false,
    cotravellerList: [{
      nationality: "",
      paxType: "", dob: "",
      passportNumber: "",
      issuingCountry: "",
      passportIssueDate: "",
      passportExpDate: "",
      lead: true, frequentFlyerList:
        [{ airLine: { code: "" }, frequentFlyerCode: "" }]
    }]
  },

  mounted() {
    this.getPagecontent();
    this.arisActive = localStorage.direction == 'rtl' ? true : false;
    if (localStorage.IsLogin == 'false') {
      window.location.href = "/";
    } else {
      var userData = JSON.parse(localStorage.User);
      this.User.title = userData.title;
      this.User.firstName = userData.firstName;
      this.User.lastName = userData.lastName;
      this.User.emailId = userData.emailId;
      if (userData.usercontactNumber != undefined) {
        this.User.usercontactNumber = userData.usercontactNumber;
      } else {
        this.User.usercontactNumber.number = '';
        this.User.usercontactNumber.countryCode = '';
        this.User.usercontactNumber.telephonecode = '';
      }
      if (userData.userAdress != undefined) {
        this.User.userAdress = userData.userAdress;
      } else {
        this.User.userAdress.line1 = '';
        this.User.userAdress.line2 = '';
        this.User.userAdress.zip = '';
        this.User.userAdress.city = '';
      }
    }
    this.getCotravellers();
  },
  methods: {
    getCotravellers: function () {
      var huburl = ServiceUrls.hubConnection.baseUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var requrl = ServiceUrls.hubConnection.hubServices.getCoTravellers;
      axios.get(huburl + portno + requrl, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.access_token
        }
      })
        .then((res) => {
          console.log("Travellers RESPONSE RECEIVED: ", res);
          if (res.data.length > 0) {
            this.cotravellerList = res.data.filter(function (e) {
              return e.lead == true;
            });
            if (this.cotravellerList.length==0) {
              this.cotravellerList = [{
                nationality: "",
                paxType: "", 
                dob: "",
                passportNumber: "",
                issuingCountry: "",
                passportIssueDate: "",
                passportExpDate: "",
                lead: true, frequentFlyerList:
                  [{ airLine: { code: "" }, frequentFlyerCode: "" }]
              }]
            }
          }
          else {
            console.log("No Travellers: ", res);
          }


        })
        .catch((err) => {
          console.log("AXIOS ERROR: ", err);
        })


    },
    getCountryName: function (input) {
      try {
        if (input != undefined && input != '') {
          var countries = countryList.filter(function (country) { return country.twoLetter.toUpperCase() == input.toUpperCase() });;
          { try { return countries[0].name; } catch (e) { return ""; } }
        }
      } catch (error) {	
      }
    },
    getAirLineName: function (airlineCode) {
      try {
        if(airlineCode!=undefined && airlineCode!=''){
          var airLineName = _.where(AirlinesDatas, { C: airlineCode.toUpperCase() }); if (airLineName == "") { airLineName = airlineCode; }
          else { airLineName = airLineName[0].A; }
          return airLineName;
        }
      } catch (error) {	
      }
    },
    getTitle: function (title) {
      if (title !=undefined && title != '') {
        if (title == 1) { title = 'Mr'; }
        if (title == 2) { title = 'Mrs'; }
        if (title == 3) { title = 'Ms'; }
        if (title == 4) { title = 'Miss'; }
        if (title == 5) { title = 'Master'; }
      }
      return title;
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;

      getAgencycode(function (response) {
        var Agencycode = 'AGY435';
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        self.dir = langauage == "ar" ? "rtl" : "ltr";
        var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/My Profile/My Profile/My Profile.ftl';
        axios.get(aboutUsPage, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          self.getdata = false;
          if (response.data.area_List.length > 0) {
            self.Page_Subtitle = self.pluckcom('Page_Subtitle', response.data.area_List[0].main.component);
            self.Username = self.pluckcom('Username', response.data.area_List[0].main.component);
            self.Full_name = self.pluckcom('Full_name', response.data.area_List[0].main.component);
            self.Phone = self.pluckcom('Phone', response.data.area_List[0].main.component);
            self.Email = self.pluckcom('Email', response.data.area_List[0].main.component);
            self.Address = self.pluckcom('Address', response.data.area_List[0].main.component);
            self.Country = self.pluckcom('Country', response.data.area_List[0].main.component);
            self.zip = self.pluckcom('ZIP', response.data.area_List[0].main.component);
            self.edit = self.pluckcom('Edit_Button', response.data.area_List[0].main.component);
            self.Date_Of_Birth = self.pluckcom('Date_Of_Birth', response.data.area_List[0].main.component);
            self.Nationality = self.pluckcom('Nationality', response.data.area_List[0].main.component);
            self.Passport_Number = self.pluckcom('Passport_Number', response.data.area_List[0].main.component);
            self.Expire_Date = self.pluckcom('Expire_Date', response.data.area_List[0].main.component);
            self.Issue_Date = self.pluckcom('Issue_Date', response.data.area_List[0].main.component);
            self.Issue_Country = self.pluckcom('Issue_Country', response.data.area_List[0].main.component);
            self.Frequent_Flyer_Program = self.pluckcom('Frequent_Flyer_Program', response.data.area_List[0].main.component);
            self.Frequent_Flyer_Number = self.pluckcom('Frequent_Flyer_Number', response.data.area_List[0].main.component);
          }
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
      });

    }
  },
  updated() {
    this.arisActive = localStorage.direction == 'rtl' ? true : false;
  }
});