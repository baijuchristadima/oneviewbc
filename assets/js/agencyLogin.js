var Host = window.location.hostname;

if (!localStorage.access_token) {
    $.getJSON("/Resources/HubUrls/AgencyCredentials.json", function (response) {
        let agencyPath = response.Agencies.find(o => o.Domain.toLowerCase() === Host.toLowerCase());
        if (typeof agencyPath !== "undefined") {
            var AgencyFolderName = agencyPath.AgencyFolderName;
            if (!localStorage.access_token) {
                $.getJSON('/' + AgencyFolderName + '/credentials/AgencyCredentials.json', function (json) {
                    let agency = json.Agencies.find(o => o.Domain.toLowerCase() === Host.toLowerCase());
                    if (typeof agency !== "undefined") {

                        var huburl = ServiceUrls.hubConnection.baseUrl;
                        var portno = ServiceUrls.hubConnection.ipAddress;
                        var session_url = huburl + portno + '/authenticate/' + agency.AgencyCode;
                        var encodedString = btoa(agency.UserName + ":" + agency.Password);
                        axios.get(session_url, {
                            headers: { 'Authorization': 'Basic ' + encodedString }
                        }).then(function (response) {
                            console.log(response);
                            localStorage.AgencyCode = agency.AgencyCode;
                            localStorage.AgencyFolderName = agency.AgencyFolderName;
                            localStorage.access_token = response.headers.access_token;
                            localStorage.timer = new Date();
                            localStorage.IsLogin = false;
                            localStorage.roleList = JSON.stringify(response.data.user.roleList);
                            localStorage.User = JSON.stringify(response.data.user);
                            try {
                                if (response.data.user.loginNode.currency != null) {
                                    localStorage.selectedCurrency = response.data.user.loginNode.currency
                                }
                            }
                            catch (err) { }
                            if (window.location.pathname == "/") {
                                window.location.href = "/" + agency.AgencyFolderName;
                            }

                        }).catch(function (error) {
                            console.log('Error on Authentication');
                        });

                    }
                    else {
                        console.log('Credentails Not found');
                    }

                });
            }
        }
    });
}
else {
    $.getJSON('/Resources/HubUrls/AgencyCredentials.json', function (json) {

        $.each(json.Agencies, function (i, agency) {
            if (agency.Domain.toLowerCase() == Host.toLowerCase()) {

                window.location.href = "/" + agency.AgencyFolderName;
            }

        });
    });
}

