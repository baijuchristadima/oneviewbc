var myprofile_vue = new Vue({
  el: "#div_customerProfile",
  data: {
    User: {
      "id": "",
      "loginid": "",
      "title": {
        "id": "",
        "name": ""
      },
      "firstName": "",
      "lastName": "",
      "status": "",
      "emailId": "",
      "usercontactNumber": {
        "number": "",
        "countryCode": "",
        "telephoneCode": ""
      },
      "contactnumber": "",
      "loginNode": {
        "code": "",
        "name": "",
        "currency": "",
        "city": "",
        "country": {
          "code": "",
          "telephonecode": ""
        },
        "url": "",
        "logo": "",
        "homepageurl": "",
        "loginurl": "",
        "address": "",
        "zip": "",
        "email": "",
        "phoneList": [
          {
            "type": "",
            "nodeContactNumber": {
              "number": ""
            },
            "number": ""
          },
          {
            "type": "",
            "nodeContactNumber": {
              "number": ""
            },
            "number": ""
          },
          {
            "type": "",
            "nodeContactNumber": {
              "number": ""
            },
            "number": ""
          }
        ],
        "nodetype": "",
        "solutionId": "",
        "lookNfeel": {
        },
        "tenant": {},
        "site": {
        },
        "servicesList": []
      },
      "roleList": []
    },
    bookingStatCount: {
      "bookingFailed": '',
      "cancelled": '',
      "confirmed": '',
      "fareChanged": '',
      "requested": '',
      "ticketFailed": '',
      "ticketPending": '',
      "totalBookings": '',
      "upComing": ''
    },
    hotelBookingStatCount: {
      "requested": '',
      "cancelled": '',
      "totalBookings": '',
      "upComing": ''
    },
    Page_Title: '',
    Welcome_Text: '',
    All_Trips_Text: '',
    Upcoming_Trips: '',
    Cancelled_Trips: '',
    All_Bookings_Text: '',
    Upcoming_Bookings: '',
    Cancelled_Bookings: '',
    Err_AlertTitle: '',
    Err_AlertMessage: '',
    Ok_btnLable: '',
    Flights_Label: '',
    Hotel_Label: ''
  },
  methods: {
    getTitle: function (title) {
      if (title != '') {
        if (title == 1) { title = 'Mr'; }
        if (title == 2) { title = 'Mrs'; }
        if (title == 3) { title = 'Ms'; }
        if (title == 4) { title = 'Miss'; }
        if (title == 5) { title = 'Master'; }
      }
      return title;
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;

      getAgencycode(function (response) {
        var Agencycode = 'AGY435';
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        self.dir = langauage == "ar" ? "rtl" : "ltr";
        var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/My Dashboard/My Dashboard/My Dashboard.ftl';
        axios.get(aboutUsPage, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          self.getdata = false;
          if (response.data.area_List.length > 0) {
           
            self.Page_Title = self.pluckcom('Page_Title', response.data.area_List[0].main.component);
            self.Welcome_Text = self.pluckcom('Welcome_Text', response.data.area_List[0].main.component);
            self.All_Trips_Text = self.pluckcom('All_Trips_Text', response.data.area_List[0].main.component);
            self.Upcoming_Trips = self.pluckcom('Upcoming_Trips', response.data.area_List[0].main.component);
            self.Cancelled_Trips = self.pluckcom('Cancelled_Trips', response.data.area_List[0].main.component);
            self.All_Bookings_Text = self.pluckcom('All_Bookings_Text', response.data.area_List[0].main.component);
            self.Upcoming_Bookings = self.pluckcom('Upcoming_Bookings', response.data.area_List[0].main.component);
            self.Cancelled_Bookings = self.pluckcom('Cancelled_Bookings', response.data.area_List[0].main.component);
            self.Err_AlertTitle = self.pluckcom('Err_AlertTitle', response.data.area_List[0].main.component);
            self.Err_AlertMessage = self.pluckcom('Err_AlertMessage', response.data.area_List[0].main.component);
            self.Ok_btnLable = self.pluckcom('Ok_btnLable', response.data.area_List[0].main.component);
            self.Flights_Label = self.pluckcom('Flights_Label', response.data.area_List[0].main.component);
            self.Hotel_Label = self.pluckcom('Hotel_Label', response.data.area_List[0].main.component);
          }
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
      });
    }
  },
  mounted() {
    this.getPagecontent();
    if (localStorage.IsLogin == 'false') {
      window.location.href = "/";
    } else {
      this.User = JSON.parse(localStorage.User);
    }
  },
  created: function () {

    let axiosConfig = {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': 'Bearer ' + localStorage.access_token
      }
    };
    var huburl = ServiceUrls.hubConnection.baseUrl;
    var portno = ServiceUrls.hubConnection.ipAddress;
    var requrl = ServiceUrls.hubConnection.hubServices.dashboardReports;
    
    var serviceId = 2;
    axios.get(huburl + portno + requrl + '/' + serviceId, axiosConfig)
      .then((res2) => {
        console.log("RESPONSE RECEIVED: ", res2);
        this.hotelBookingStatCount = res2.data.bookingStatCount;
      })
      .catch((err) => {
        console.log("AXIOS ERROR: ", err);
        // alertify.alert(this.Err_AlertTitle, this.Err_AlertMessage).set('label', this.Ok_btnLable);

      })
    serviceId = 1;
    axios.get(huburl + portno + requrl + '/' + serviceId, axiosConfig)
      .then((res) => {
        console.log("RESPONSE RECEIVED: ", res);
        this.bookingStatCount = res.data.bookingStatCount;
        //   localStorage.access_token = res.headers.access_token;
        // this.bookingStatCount.cancelled = this.bookingStatCount.bookingFailed + this.bookingStatCount.cancelled + this.bookingStatCount.requested + this.bookingStatCount.ticketFailed;
      })
      .catch((err) => {
        console.log("AXIOS ERROR: ", err);
        // alertify.alert(this.Err_AlertTitle, this.Err_AlertMessage).set('label', this.Ok_btnLable);

      })
  }
});