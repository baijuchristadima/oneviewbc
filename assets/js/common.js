// var cdnDomain = 'https://www.oneviewitsolutions.com';
var generalInformation = {
    systemSettings: {
        systemDateFormat: 'dd M y, D',
        calendarDisplay: 2,
        calendarDisplayInMobile: 1
    },
}
var searchResults = null;
var supplierList = [
    { id: '40', name: 'Qtech', count: 0 },
    { id: '22', name: 'Loh', count: 0 }
];
var lightColorValue = 40;
var darkColorValue = -40;

Vue.prototype.$eventHub = new Vue();

// // //RTL

// if (localStorage.direction == 'rtl') {    

//       $(".arborder").addClass("arbrder_left");
// } else {   
//      $(".arborder").removeClass("arbrder_left");
// }

// //RTL

$(document).ready(function () {


    getAgencyFolderName();
    checkLoginorNot();
    setInterval(function () { checkLoginorNot(); }, 60000);
    if (!localStorage.access_token) {
        commonlogin();
    } else {
        setColorTheme();
    }
    var path = location.pathname.split('/')[1]
    if (path == "flights") {
        sessionStorage.active_el = 1;
    } else if (path == "Hotels") {
        sessionStorage.active_el = 2;
    }


    var hName = window.location.pathname.toString().split("/")[1].toLowerCase();
    var pagePath = (window.location.href.indexOf(hName + "/demo-page") > -1) ? hName + "/demo-page" : window.location.pathname.toLowerCase();

    //  if (pagePath != '/Flights/flight-listing.html' && pagePath != '/Hotels/hotel-listing.html') {
    if (localStorage.direction == 'rtl') {
        $(".ar_direction").addClass("ar_direction1");
        $(".ajs-modal").addClass("ar_direction1");
        $(".ui-datepicker").addClass("calendar_flx");
    } else {
        $(".ar_direction").removeClass("ar_direction1");
        $(".ajs-modal").removeClass("ar_direction1");
        $(".ui-datepicker").removeClass("calendar_flx");


    }



});

function checkLoginorNot() {
    if (!localStorage.timer) {
        commonlogin();
    } else {
        var now = moment(new Date());
        var end = moment(new Date(localStorage.timer));
        var duration = moment.duration(now.diff(end));
        var timeRemaing = duration.asMinutes();
        if (timeRemaing > 7 && timeRemaing < 10) {
            var huburl = ServiceUrls.hubConnection.baseUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var reNewTokenUrl = ServiceUrls.hubConnection.hubServices.reNewToken;
            var session_url = huburl + portno + reNewTokenUrl
            var currenttoken = localStorage.access_token
            axios.get(session_url, {
                headers: { 'Authorization': 'Bearer ' + currenttoken }
            }).then(function (response) {

                localStorage.access_token = response.headers.access_token;
                localStorage.timer = new Date();
                localStorage.User = JSON.stringify(response.data.user);

            }).catch(function (error) {
                console.log('Error on Authentication');
                commonlogin();
            });
        } else if (timeRemaing > 10) {
            var Host = window.location.hostname;
            $.getJSON('/' + localStorage.AgencyFolderName + '/credentials/AgencyCredentials.json', function (json) {
                let agency = json.Agencies.find(o => o.Domain.toLowerCase() === Host.toLowerCase());
                if (typeof agency !== "undefined") {
                    var huburl = ServiceUrls.hubConnection.baseUrl;
                    var portno = ServiceUrls.hubConnection.ipAddress;
                    var session_url = huburl + portno + '/authenticate/' + agency.AgencyCode;
                    var encodedString = btoa(agency.UserName + ":" + agency.Password);
                    axios.get(session_url, {
                        headers: { 'Authorization': 'Basic ' + encodedString }
                    }).then(function (response) {
                        localStorage.access_token = response.headers.access_token;
                        localStorage.AgencyCode = agency.AgencyCode;
                        localStorage.timer = new Date();
                        localStorage.roleList = JSON.stringify(response.data.user.roleList);
                        localStorage.User = JSON.stringify(response.data.user);
                        localStorage.IsLogin = false;

                    }).catch(function (error) {
                        console.log('Error on Authentication');
                        window.location.href = "/" + agency.AgencyFolderName;
                    });

                } else {
                    console.log('Credentails Not found');

                    window.location.href = "/" + agency.AgencyFolderName;
                }

            });
        }
    }
}



/**
 * currency codes
 */
var numberFormats = {
    'AED': { currency: { style: 'currency', currency: 'AED', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'AED', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'AFN': { currency: { style: 'currency', currency: 'AFN', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'AFN', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'ALL': { currency: { style: 'currency', currency: 'ALL', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'ALL', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'AMD': { currency: { style: 'currency', currency: 'AMD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'AMD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'ANG': { currency: { style: 'currency', currency: 'ANG', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'ANG', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'AOA': { currency: { style: 'currency', currency: 'AOA', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'AOA', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'ARS': { currency: { style: 'currency', currency: 'ARS', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'ARS', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'AUD': { currency: { style: 'currency', currency: 'AUD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'AUD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'AWG': { currency: { style: 'currency', currency: 'AWG', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'AWG', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'AZN': { currency: { style: 'currency', currency: 'AZN', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'AZN', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BAM': { currency: { style: 'currency', currency: 'BAM', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BAM', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BBD': { currency: { style: 'currency', currency: 'BBD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BBD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BDT': { currency: { style: 'currency', currency: 'BDT', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BDT', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BGN': { currency: { style: 'currency', currency: 'BGN', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BGN', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BHD': { currency: { style: 'currency', currency: 'BHD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BHD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BIF': { currency: { style: 'currency', currency: 'BIF', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BIF', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BMD': { currency: { style: 'currency', currency: 'BMD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BMD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BND': { currency: { style: 'currency', currency: 'BND', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BND', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BOB': { currency: { style: 'currency', currency: 'BOB', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BOB', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BRL': { currency: { style: 'currency', currency: 'BRL', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BRL', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BSD': { currency: { style: 'currency', currency: 'BSD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BSD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BTN': { currency: { style: 'currency', currency: 'BTN', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BTN', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BWP': { currency: { style: 'currency', currency: 'BWP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BWP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BYN': { currency: { style: 'currency', currency: 'BYN', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BYN', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'BZD': { currency: { style: 'currency', currency: 'BZD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'BZD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'CAD': { currency: { style: 'currency', currency: 'CAD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'CAD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'CDF': { currency: { style: 'currency', currency: 'CDF', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'CDF', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'CHF': { currency: { style: 'currency', currency: 'CHF', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'CHF', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'CLP': { currency: { style: 'currency', currency: 'CLP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'CLP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'CNY': { currency: { style: 'currency', currency: 'CNY', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'CNY', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'COP': { currency: { style: 'currency', currency: 'COP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'COP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'CRC': { currency: { style: 'currency', currency: 'CRC', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'CRC', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'CUC': { currency: { style: 'currency', currency: 'CUC', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'CUC', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'CUP': { currency: { style: 'currency', currency: 'CUP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'CUP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'CVE': { currency: { style: 'currency', currency: 'CVE', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'CVE', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'CZK': { currency: { style: 'currency', currency: 'CZK', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'CZK', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'DJF': { currency: { style: 'currency', currency: 'DJF', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'DJF', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'DKK': { currency: { style: 'currency', currency: 'DKK', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'DKK', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'DOP': { currency: { style: 'currency', currency: 'DOP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'DOP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'DZD': { currency: { style: 'currency', currency: 'DZD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'DZD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'EGP': { currency: { style: 'currency', currency: 'EGP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'EGP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'ERN': { currency: { style: 'currency', currency: 'ERN', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'ERN', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'ETB': { currency: { style: 'currency', currency: 'ETB', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'ETB', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'EUR': { currency: { style: 'currency', currency: 'EUR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'EUR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'FJD': { currency: { style: 'currency', currency: 'FJD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'FJD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'FKP': { currency: { style: 'currency', currency: 'FKP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'FKP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'GBP': { currency: { style: 'currency', currency: 'GBP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'GBP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'GEL': { currency: { style: 'currency', currency: 'GEL', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'GEL', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'GGP': { currency: { style: 'currency', currency: 'GGP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'GGP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'GHS': { currency: { style: 'currency', currency: 'GHS', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'GHS', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'GIP': { currency: { style: 'currency', currency: 'GIP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'GIP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'GMD': { currency: { style: 'currency', currency: 'GMD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'GMD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'GNF': { currency: { style: 'currency', currency: 'GNF', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'GNF', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'GTQ': { currency: { style: 'currency', currency: 'GTQ', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'GTQ', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'GYD': { currency: { style: 'currency', currency: 'GYD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'GYD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'HKD': { currency: { style: 'currency', currency: 'HKD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'HKD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'HNL': { currency: { style: 'currency', currency: 'HNL', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'HNL', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'HRK': { currency: { style: 'currency', currency: 'HRK', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'HRK', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'HTG': { currency: { style: 'currency', currency: 'HTG', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'HTG', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'HUF': { currency: { style: 'currency', currency: 'HUF', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'HUF', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'IDR': { currency: { style: 'currency', currency: 'IDR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'IDR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'ILS': { currency: { style: 'currency', currency: 'ILS', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'ILS', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'IMP': { currency: { style: 'currency', currency: 'IMP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'IMP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'INR': { currency: { style: 'currency', currency: 'INR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'INR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'IQD': { currency: { style: 'currency', currency: 'IQD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'IQD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'IRR': { currency: { style: 'currency', currency: 'IRR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'IRR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'ISK': { currency: { style: 'currency', currency: 'ISK', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'ISK', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'JEP': { currency: { style: 'currency', currency: 'JEP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'JEP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'JMD': { currency: { style: 'currency', currency: 'JMD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'JMD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'JOD': { currency: { style: 'currency', currency: 'JOD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'JOD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'JPY': { currency: { style: 'currency', currency: 'JPY', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'JPY', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'KES': { currency: { style: 'currency', currency: 'KES', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'KES', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'KGS': { currency: { style: 'currency', currency: 'KGS', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'KGS', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'KHR': { currency: { style: 'currency', currency: 'KHR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'KHR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'KMF': { currency: { style: 'currency', currency: 'KMF', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'KMF', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'KPW': { currency: { style: 'currency', currency: 'KPW', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'KPW', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'KRW': { currency: { style: 'currency', currency: 'KRW', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'KRW', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'KWD': { currency: { style: 'currency', currency: 'KWD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'KWD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'KYD': { currency: { style: 'currency', currency: 'KYD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'KYD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'KZT': { currency: { style: 'currency', currency: 'KZT', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'KZT', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'LAK': { currency: { style: 'currency', currency: 'LAK', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'LAK', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'LBP': { currency: { style: 'currency', currency: 'LBP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'LBP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'LKR': { currency: { style: 'currency', currency: 'LKR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'LKR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'LRD': { currency: { style: 'currency', currency: 'LRD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'LRD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'LSL': { currency: { style: 'currency', currency: 'LSL', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'LSL', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'LYD': { currency: { style: 'currency', currency: 'LYD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'LYD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'MAD': { currency: { style: 'currency', currency: 'MAD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'MAD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'MDL': { currency: { style: 'currency', currency: 'MDL', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'MDL', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'MGA': { currency: { style: 'currency', currency: 'MGA', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'MGA', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'MKD': { currency: { style: 'currency', currency: 'MKD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'MKD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'MMK': { currency: { style: 'currency', currency: 'MMK', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'MMK', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'MNT': { currency: { style: 'currency', currency: 'MNT', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'MNT', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'MOP': { currency: { style: 'currency', currency: 'MOP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'MOP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'MRO': { currency: { style: 'currency', currency: 'MRO', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'MRO', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'MUR': { currency: { style: 'currency', currency: 'MUR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'MUR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'MVR': { currency: { style: 'currency', currency: 'MVR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'MVR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'MWK': { currency: { style: 'currency', currency: 'MWK', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'MWK', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'MXN': { currency: { style: 'currency', currency: 'MXN', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'MXN', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'MYR': { currency: { style: 'currency', currency: 'MYR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'MYR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'MZN': { currency: { style: 'currency', currency: 'MZN', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'MZN', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'NAD': { currency: { style: 'currency', currency: 'NAD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'NAD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'NGN': { currency: { style: 'currency', currency: 'NGN', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'NGN', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'NIO': { currency: { style: 'currency', currency: 'NIO', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'NIO', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'NOK': { currency: { style: 'currency', currency: 'NOK', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'NOK', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'NPR': { currency: { style: 'currency', currency: 'NPR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'NPR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'NZD': { currency: { style: 'currency', currency: 'NZD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'NZD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'OMR': { currency: { style: 'currency', currency: 'OMR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'OMR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'PAB': { currency: { style: 'currency', currency: 'PAB', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'PAB', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'PEN': { currency: { style: 'currency', currency: 'PEN', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'PEN', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'PGK': { currency: { style: 'currency', currency: 'PGK', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'PGK', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'PHP': { currency: { style: 'currency', currency: 'PHP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'PHP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'PKR': { currency: { style: 'currency', currency: 'PKR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'PKR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'PLN': { currency: { style: 'currency', currency: 'PLN', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'PLN', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'PYG': { currency: { style: 'currency', currency: 'PYG', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'PYG', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'QAR': { currency: { style: 'currency', currency: 'QAR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'QAR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'RON': { currency: { style: 'currency', currency: 'RON', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'RON', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'RSD': { currency: { style: 'currency', currency: 'RSD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'RSD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'RUB': { currency: { style: 'currency', currency: 'RUB', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'RUB', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'RWF': { currency: { style: 'currency', currency: 'RWF', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'RWF', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SAR': { currency: { style: 'currency', currency: 'SAR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SAR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SBD': { currency: { style: 'currency', currency: 'SBD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SBD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SCR': { currency: { style: 'currency', currency: 'SCR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SCR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SDG': { currency: { style: 'currency', currency: 'SDG', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SDG', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SEK': { currency: { style: 'currency', currency: 'SEK', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SEK', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SGD': { currency: { style: 'currency', currency: 'SGD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SGD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SHP': { currency: { style: 'currency', currency: 'SHP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SHP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SLL': { currency: { style: 'currency', currency: 'SLL', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SLL', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SOS': { currency: { style: 'currency', currency: 'SOS', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SOS', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SPL': { currency: { style: 'currency', currency: 'SPL', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SPL', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SRD': { currency: { style: 'currency', currency: 'SRD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SRD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SSP': { currency: { style: 'currency', currency: 'SSP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SSP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'STD': { currency: { style: 'currency', currency: 'STD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'STD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SVC': { currency: { style: 'currency', currency: 'SVC', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SVC', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SYP': { currency: { style: 'currency', currency: 'SYP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SYP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'SZL': { currency: { style: 'currency', currency: 'SZL', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'SZL', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'THB': { currency: { style: 'currency', currency: 'THB', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'THB', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'TJS': { currency: { style: 'currency', currency: 'TJS', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'TJS', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'TMT': { currency: { style: 'currency', currency: 'TMT', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'TMT', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'TND': { currency: { style: 'currency', currency: 'TND', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'TND', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'TOP': { currency: { style: 'currency', currency: 'TOP', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'TOP', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'TRY': { currency: { style: 'currency', currency: 'TRY', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'TRY', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'TTD': { currency: { style: 'currency', currency: 'TTD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'TTD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'TVD': { currency: { style: 'currency', currency: 'TVD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'TVD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'TWD': { currency: { style: 'currency', currency: 'TWD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'TWD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'TZS': { currency: { style: 'currency', currency: 'TZS', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'TZS', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'UAH': { currency: { style: 'currency', currency: 'UAH', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'UAH', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'UGX': { currency: { style: 'currency', currency: 'UGX', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'UGX', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'USD': { currency: { style: 'currency', currency: 'USD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'USD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'UYU': { currency: { style: 'currency', currency: 'UYU', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'UYU', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'UZS': { currency: { style: 'currency', currency: 'UZS', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'UZS', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'VEF': { currency: { style: 'currency', currency: 'VEF', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'VEF', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'VND': { currency: { style: 'currency', currency: 'VND', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'VND', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'VUV': { currency: { style: 'currency', currency: 'VUV', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'VUV', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'WST': { currency: { style: 'currency', currency: 'WST', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'WST', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'XAF': { currency: { style: 'currency', currency: 'XAF', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'XAF', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'XCD': { currency: { style: 'currency', currency: 'XCD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'XCD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'XOF': { currency: { style: 'currency', currency: 'XOF', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'XOF', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'XPF': { currency: { style: 'currency', currency: 'XPF', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'XPF', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'YER': { currency: { style: 'currency', currency: 'YER', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'YER', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'ZAR': { currency: { style: 'currency', currency: 'ZAR', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'ZAR', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'ZMW': { currency: { style: 'currency', currency: 'ZMW', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'ZMW', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'ZWD': { currency: { style: 'currency', currency: 'ZWD', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'ZWD', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
    'ZWL': { currency: { style: 'currency', currency: 'ZWL', currencyDisplay: 'code' }, currencyNoCents: { style: 'currency', currency: 'ZWL', currencyDisplay: 'code', minimumFractionDigits: 0, maximumFractionDigits: 0 } },
}
// const i18n = new VueI18n({
//   numberFormats,
//   locale: 'en'
//   //, // set locale
//   // messages, // set locale messages
// })
var Vue_localization = new Vue({
    data: {
        defaultLanguage: 'en',
        defaultCurrency: 'USD',

        languageOptions: [
            { text: 'Arabic', value: 'ae', flag: 'flag-icon flag-icon-ae', dataContent: '<span class="flag-icon flag-icon-gb"></span> English' },
            { text: 'French', value: 'fr', flag: 'flag-icon flag-icon-fr', dataContent: '<span class="flag-icon flag-icon-sa"></span> ????' }
        ],
        CurrencyOptions: [
            { text: 'AED UAE Dirham', value: 'AED', dataContent: '<span class="flag-icon flag-icon-ae"></span> AED UAE Dirham' },
            { text: 'AFN Afghanistan Afghani ', value: 'AFN', dataContent: '<span class="flag-icon flag-icon-af"></span> AFN Afghanistan Afg' },
            { text: 'ALL Albania Lek ', value: 'ALL', dataContent: '<span class="flag-icon flag-icon-al"></span> ALL Albania Lek ' },
            { text: 'AMD Armenia Dram ', value: 'AMD', dataContent: '<span class="flag-icon flag-icon-am"></span> AMD Armenia Dram ' },
            { text: 'ANG Netherlands Antilles ', value: 'ANG', dataContent: '<span class="flag-icon flag-icon-nl"></span> ANG Netherlands Ant' },
            { text: 'AOA Angola Kwanza ', value: 'AOA', dataContent: '<span class="flag-icon flag-icon-ao"></span> AOA Angola Kwanza ' },
            { text: 'ARS Argentina Peso ', value: 'ARS', dataContent: '<span class="flag-icon flag-icon-ar"></span> ARS Argentina Peso ' },
            { text: 'AUD Australia Dollar ', value: 'AUD', dataContent: '<span class="flag-icon flag-icon-au"></span> AUD Australia Dolla' },
            { text: 'AWG Aruba Guilder ', value: 'AWG', dataContent: '<span class="flag-icon flag-icon-aw"></span> AWG Aruba Guilder ' },
            { text: 'AZN Azerbaijan New Manat ', value: 'AZN', dataContent: '<span class="flag-icon flag-icon-az"></span> AZN Azerbaijan New ' },
            { text: 'BAM Bosnia and Herzegovi ', value: 'BAM', dataContent: '<span class="flag-icon flag-icon-ba"></span> BAM Bosnia and Herz' },
            { text: 'BBD Barbados Dollar ', value: 'BBD', dataContent: '<span class="flag-icon flag-icon-bb"></span> BBD Barbados Dollar' },
            { text: 'BDT Bangladesh Taka ', value: 'BDT', dataContent: '<span class="flag-icon flag-icon-bd"></span> BDT Bangladesh Taka' },
            { text: 'BGN Bulgaria Lev ', value: 'BGN', dataContent: '<span class="flag-icon flag-icon-bg"></span> BGN Bulgaria Lev ' },
            { text: 'BHD Bahrain Dinar ', value: 'BHD', dataContent: '<span class="flag-icon flag-icon-bh"></span> BHD Bahrain Dinar ' },
            { text: 'BIF Burundi Franc ', value: 'BIF', dataContent: '<span class="flag-icon flag-icon-bi"></span> BIF Burundi Franc ' },
            { text: 'BMD Bermuda Dollar ', value: 'BMD', dataContent: '<span class="flag-icon flag-icon-bm"></span> BMD Bermuda Dollar ' },
            { text: 'BND Brunei Darussalam Do ', value: 'BND', dataContent: '<span class="flag-icon flag-icon-bn"></span> BND Brunei Darussal' },
            { text: 'BOB Bolivian boliviano ', value: 'BOB', dataContent: '<span class="flag-icon flag-icon-b0"></span> BOB Bolivian bolivi' },
            { text: 'BRL Brazil Real ', value: 'BRL', dataContent: '<span class="flag-icon flag-icon-br"></span> BRL Brazil Real ' },
            { text: 'BSD Bahamas Dollar ', value: 'BSD', dataContent: '<span class="flag-icon flag-icon-bs"></span> BSD Bahamas Dollar ' },
            { text: 'BTN Bhutan Ngultrum ', value: 'BTN', dataContent: '<span class="flag-icon flag-icon-bt"></span> BTN Bhutan Ngultrum' },
            { text: 'BWP Botswana Pula ', value: 'BWP', dataContent: '<span class="flag-icon flag-icon-bw"></span> BWP Botswana Pula ' },
            { text: 'BYN Belarus Ruble ', value: 'BYN', dataContent: '<span class="flag-icon flag-icon-by"></span> BYN Belarus Ruble ' },
            { text: 'BZD Belize Dollar ', value: 'BZD', dataContent: '<span class="flag-icon flag-icon-bz"></span> BZD Belize Dollar ' },
            { text: 'CAD Canada Dollar ', value: 'CAD', dataContent: '<span class="flag-icon flag-icon-ca"></span> CAD Canada Dollar ' },
            { text: 'CDF Congo/Kinshasa Franc ', value: 'CDF', dataContent: '<span class="flag-icon flag-icon-ca"></span> CDF Congo/Kinshasa ' },
            { text: 'CHF Switzerland Franc ', value: 'CHF', dataContent: '<span class="flag-icon flag-icon-ch"></span> CHF Switzerland Fra' },
            { text: 'CLP Chile Peso ', value: 'CLP', dataContent: '<span class="flag-icon flag-icon-cl"></span> CLP Chile Peso ' },
            { text: 'CNY China Yuan Renminbi ', value: 'CNY', dataContent: '<span class="flag-icon flag-icon-cn"></span> CNY China Yuan Renm' },
            { text: 'COP Colombia Peso ', value: 'COP', dataContent: '<span class="flag-icon flag-icon-co"></span> COP Colombia Peso ' },
            { text: 'CRC Costa Rica Colon ', value: 'CRC', dataContent: '<span class="flag-icon flag-icon-cr"></span> CRC Costa Rica Colo' },
            { text: 'CUC Cuba Convertible Pes ', value: 'CUC', dataContent: '<span class="flag-icon flag-icon-cu"></span> CUC Cuba Convertibl' },
            { text: 'CUP Cuba Peso ', value: 'CUP', dataContent: '<span class="flag-icon flag-icon-cu"></span> CUP Cuba Peso ' },
            { text: 'CVE Cape Verde Escudo ', value: 'CVE', dataContent: '<span class="flag-icon flag-icon-cv"></span> CVE Cape Verde Escu' },
            { text: 'CZK Czech Republic Korun ', value: 'CZK', dataContent: '<span class="flag-icon flag-icon-cz"></span> CZK Czech Republic ' },
            { text: 'DJF Djibouti Franc ', value: 'DJF', dataContent: '<span class="flag-icon flag-icon-dj"></span> DJF Djibouti Franc ' },
            { text: 'DKK Denmark Krone ', value: 'DKK', dataContent: '<span class="flag-icon flag-icon-dk"></span> DKK Denmark Krone ' },
            { text: 'DOP Dominican Republic P ', value: 'DOP', dataContent: '<span class="flag-icon flag-icon-do"></span> DOP Dominican Repub' },
            { text: 'DZD Algeria Dinar ', value: 'DZD', dataContent: '<span class="flag-icon  flag-icon-dz"></span> DZD Algeria Dinar ' },
            { text: 'EGP Egypt Pound ', value: 'EGP', dataContent: '<span class="flag-icon flag-icon-eg"></span> EGP Egypt Pound ' },
            { text: 'ERN Eritrea Nakfa ', value: 'ERN', dataContent: '<span class="flag-icon flag-icon-er"></span> ERN Eritrea Nakfa ' },
            { text: 'ETB Ethiopia Birr ', value: 'ETB', dataContent: '<span class="flag-icon flag-icon-et"></span> ETB Ethiopia Birr ' },
            { text: 'EUR Euro Member Countrie ', value: 'EUR', dataContent: '<span class="flag-icon flag-icon-fr"></span> EUR Euro' },
            { text: 'FJD Fiji Dollar ', value: 'FJD', dataContent: '<span class="flag-icon flag-icon-fj"></span> FJD Fiji Dollar ' },
            { text: 'FKP Falkland Islands (Ma ', value: 'FKP', dataContent: '<span class="flag-icon flag-icon-fk"></span> FKP Falkland Island' },
            { text: 'GBP United Kingdom Pound ', value: 'GBP', dataContent: '<span class="flag-icon flag-icon-gb"></span> GBP Pound Sterling' },
            { text: 'GEL Georgia Lari ', value: 'GEL', dataContent: '<span class="flag-icon flag-icon-ge"></span> GEL Georgia Lari ' },
            { text: 'GGP Guernsey Pound ', value: 'GGP', dataContent: '<span class="flag-icon flag-icon-gg"></span> GGP Guernsey Pound ' },
            { text: 'GHS Ghana Cedi ', value: 'GHS', dataContent: '<span class="flag-icon flag-icon-gh"></span> GHS Ghana Cedi ' },
            { text: 'GIP Gibraltar Pound ', value: 'GIP', dataContent: '<span class="flag-icon flag-icon-gi"></span> GIP Gibraltar Pound' },
            { text: 'GMD Gambia Dalasi ', value: 'GMD', dataContent: '<span class="flag-icon flag-icon-gm"></span> GMD Gambia Dalasi ' },
            { text: 'GNF Guinea Franc ', value: 'GNF', dataContent: '<span class="flag-icon flag-icon-gn"></span> GNF Guinea Franc ' },
            { text: 'GTQ Guatemala Quetzal ', value: 'GTQ', dataContent: '<span class="flag-icon flag-icon-gt"></span> GTQ Guatemala Quetz' },
            { text: 'GYD Guyana Dollar ', value: 'GYD', dataContent: '<span class="flag-icon flag-icon-gy"></span> GYD Guyana Dollar ' },
            { text: 'HKD Hong Kong Dollar ', value: 'HKD', dataContent: '<span class="flag-icon flag-icon-hk"></span> HKD Hong Kong Dolla' },
            { text: 'HNL Honduras Lempira ', value: 'HNL', dataContent: '<span class="flag-icon flag-icon-hn"></span> HNL Honduras Lempir' },
            { text: 'HRK Croatia Kuna ', value: 'HRK', dataContent: '<span class="flag-icon flag-icon-hr"></span> HRK Croatia Kuna ' },
            { text: 'HTG Haiti Gourde ', value: 'HTG', dataContent: '<span class="flag-icon flag-icon-ht"></span> HTG Haiti Gourde ' },
            { text: 'HUF Hungary Forint ', value: 'HUF', dataContent: '<span class="flag-icon flag-icon-hu"></span> HUF Hungary Forint ' },
            { text: 'IDR Indonesia Rupiah ', value: 'IDR', dataContent: '<span class="flag-icon flag-icon-id"></span> IDR Indonesia Rupia' },
            { text: 'ILS Israel Shekel ', value: 'ILS', dataContent: '<span class="flag-icon flag-icon-il"></span> ILS Israel Shekel ' },
            { text: 'IMP Isle of Man Pound ', value: 'IMP', dataContent: '<span class="flag-icon flag-icon-im"></span> IMP Isle of Man Pou' },
            { text: 'INR India Rupee ', value: 'INR', dataContent: '<span class="flag-icon flag-icon-in"></span> INR India Rupee ' },
            { text: 'IQD Iraq Dinar ', value: 'IQD', dataContent: '<span class="flag-icon flag-icon-iq"></span> IQD Iraq Dinar ' },
            { text: 'IRR Iran Rial ', value: 'IRR', dataContent: '<span class="flag-icon flag-icon-ir"></span> IRR Iran Rial ' },
            { text: 'ISK Iceland Krona ', value: 'ISK', dataContent: '<span class="flag-icon flag-icon-is"></span> ISK Iceland Krona ' },
            { text: 'JEP Jersey Pound ', value: 'JEP', dataContent: '<span class="flag-icon flag-icon-je"></span> JEP Jersey Pound ' },
            { text: 'JMD Jamaica Dollar ', value: 'JMD', dataContent: '<span class="flag-icon flag-icon-jm"></span> JMD Jamaica Dollar ' },
            { text: 'JOD Jordan Dinar ', value: 'JOD', dataContent: '<span class="flag-icon flag-icon-jo"></span> JOD Jordan Dinar ' },
            { text: 'JPY Japan Yen ', value: 'JPY', dataContent: '<span class="flag-icon flag-icon-jp"></span> JPY Japan Yen ' },
            { text: 'KES Kenya Shilling ', value: 'KES', dataContent: '<span class="flag-icon flag-icon-ke"></span> KES Kenya Shilling ' },
            { text: 'KGS Kyrgyzstan Som ', value: 'KGS', dataContent: '<span class="flag-icon flag-icon-kg"></span> KGS Kyrgyzstan Som ' },
            { text: 'KHR Cambodia Riel ', value: 'KHR', dataContent: '<span class="flag-icon flag-icon-kh"></span> KHR Cambodia Riel ' },
            { text: 'KMF Comoros Franc ', value: 'KMF', dataContent: '<span class="flag-icon flag-icon-km"></span> KMF Comoros Franc ' },
            { text: 'KPW Korea (North) Won ', value: 'KPW', dataContent: '<span class="flag-icon flag-icon-kp"></span> KPW Korea (North) W' },
            { text: 'KRW Korea (South) Won ', value: 'KRW', dataContent: '<span class="flag-icon flag-icon-kr"></span> KRW Korea (South) W' },
            { text: 'KWD Kuwait Dinar ', value: 'KWD', dataContent: '<span class="flag-icon flag-icon-kw"></span> KWD Kuwait Dinar ' },
            { text: 'KYD Cayman Islands Dolla ', value: 'KYD', dataContent: '<span class="flag-icon flag-icon-ky"></span> KYD Cayman Islands ' },
            { text: 'KZT Kazakhstan Tenge ', value: 'KZT', dataContent: '<span class="flag-icon flag-icon-kz"></span> KZT Kazakhstan Teng' },
            { text: 'LAK Laos Kip ', value: 'LAK', dataContent: '<span class="flag-icon flag-icon-la"></span> LAK Laos Kip ' },
            { text: 'LBP Lebanon Pound ', value: 'LBP', dataContent: '<span class="flag-icon flag-icon-lb"></span> LBP Lebanon Pound ' },
            { text: 'LKR Sri Lanka Rupee ', value: 'LKR', dataContent: '<span class="flag-icon flag-icon-lk"></span> LKR Sri Lanka Rupee' },
            { text: 'LRD Liberia Dollar ', value: 'LRD', dataContent: '<span class="flag-icon flag-icon-lr"></span> LRD Liberia Dollar ' },
            { text: 'LSL Lesotho Loti ', value: 'LSL', dataContent: '<span class="flag-icon flag-icon-ls"></span> LSL Lesotho Loti ' },
            { text: 'LYD Libya Dinar ', value: 'LYD', dataContent: '<span class="flag-icon flag-icon-ly"></span> LYD Libya Dinar ' },
            { text: 'MAD Morocco Dirham ', value: 'MAD', dataContent: '<span class="flag-icon flag-icon-ma"></span> MAD Morocco Dirham ' },
            { text: 'MDL Moldova Leu ', value: 'MDL', dataContent: '<span class="flag-icon flag-icon-md"></span> MDL Moldova Leu ' },
            { text: 'MGA Madagascar Ariary ', value: 'MGA', dataContent: '<span class="flag-icon flag-icon-mg"></span> MGA Madagascar Aria' },
            { text: 'MKD Macedonia Denar ', value: 'MKD', dataContent: '<span class="flag-icon flag-icon-mk"></span> MKD Macedonia Denar' },
            { text: 'MMK Myanmar (Burma) Kyat ', value: 'MMK', dataContent: '<span class="flag-icon flag-icon-mm"></span> MMK Myanmar (Burma)' },
            { text: 'MNT Mongolia Tughrik ', value: 'MNT', dataContent: '<span class="flag-icon flag-icon-mn"></span> MNT Mongolia Tughri' },
            { text: 'MOP Macau Pataca ', value: 'MOP', dataContent: '<span class="flag-icon flag-icon-mo"></span> MOP Macau Pataca ' },
            { text: 'MRO Mauritania Ouguiya ', value: 'MRO', dataContent: '<span class="flag-icon flag-icon-mr"></span> MRO Mauritania Ougu' },
            { text: 'MUR Mauritius Rupee ', value: 'MUR', dataContent: '<span class="flag-icon flag-icon-mu"></span> MUR Mauritius Rupee' },
            { text: 'MVR Maldives (Maldive Is ', value: 'MVR', dataContent: '<span class="flag-icon flag-icon-mv"></span> MVR Maldives (Maldi' },
            { text: 'MWK Malawi Kwacha ', value: 'MWK', dataContent: '<span class="flag-icon flag-icon-mw"></span> MWK Malawi Kwacha ' },
            { text: 'MXN Mexico Peso ', value: 'MXN', dataContent: '<span class="flag-icon flag-icon-mx"></span> MXN Mexico Peso ' },
            { text: 'MYR Malaysia Ringgit ', value: 'MYR', dataContent: '<span class="flag-icon flag-icon-my"></span> MYR Malaysia Ringgi' },
            { text: 'MZN Mozambique Metical ', value: 'MZN', dataContent: '<span class="flag-icon flag-icon-mz"></span> MZN Mozambique Meti' },
            { text: 'NAD Namibia Dollar ', value: 'NAD', dataContent: '<span class="flag-icon flag-icon-na"></span> NAD Namibia Dollar ' },
            { text: 'NGN Nigeria Naira ', value: 'NGN', dataContent: '<span class="flag-icon flag-icon-ng"></span> NGN Nigeria Naira ' },
            { text: 'NIO Nicaragua Cordoba ', value: 'NIO', dataContent: '<span class="flag-icon flag-icon-ni"></span> NIO Nicaragua Cordo' },
            { text: 'NOK Norway Krone ', value: 'NOK', dataContent: '<span class="flag-icon flag-icon-no"></span> NOK Norway Krone ' },
            { text: 'NPR Nepal Rupee ', value: 'NPR', dataContent: '<span class="flag-icon flag-icon-np"></span> NPR Nepal Rupee ' },
            { text: 'NZD New Zealand Dollar ', value: 'NZD', dataContent: '<span class="flag-icon flag-icon-nz"></span> NZD New Zealand Dol' },
            { text: 'OMR Oman Rial ', value: 'OMR', dataContent: '<span class="flag-icon flag-icon-om"></span> OMR Oman Rial ' },
            { text: 'PAB Panama Balboa ', value: 'PAB', dataContent: '<span class="flag-icon flag-icon-pa"></span> PAB Panama Balboa ' },
            { text: 'PEN Peru Sol ', value: 'PEN', dataContent: '<span class="flag-icon flag-icon-pe"></span> PEN Peru Sol ' },
            { text: 'PGK Papua New Guinea Kin ', value: 'PGK', dataContent: '<span class="flag-icon flag-icon-pg"></span> PGK Papua New Guine' },
            { text: 'PHP Philippines Peso ', value: 'PHP', dataContent: '<span class="flag-icon flag-icon-ph"></span> PHP Philippines Pes' },
            { text: 'PKR Pakistan Rupee ', value: 'PKR', dataContent: '<span class="flag-icon flag-icon-pk"></span> PKR Pakistan Rupee ' },
            { text: 'PLN Poland Zloty ', value: 'PLN', dataContent: '<span class="flag-icon flag-icon-pl"></span> PLN Poland Zloty ' },
            { text: 'PYG Paraguay Guarani ', value: 'PYG', dataContent: '<span class="flag-icon flag-icon-py"></span> PYG Paraguay Guaran' },
            { text: 'QAR Qatar Riyal ', value: 'QAR', dataContent: '<span class="flag-icon flag-icon-qa"></span> QAR Qatar Riyal ' },
            { text: 'RON Romania New Leu ', value: 'RON', dataContent: '<span class="flag-icon flag-icon-ro"></span> RON Romania New Leu' },
            { text: 'RSD Serbia Dinar ', value: 'RSD', dataContent: '<span class="flag-icon flag-icon-rs"></span> RSD Serbia Dinar ' },
            { text: 'RUB Russia Ruble ', value: 'RUB', dataContent: '<span class="flag-icon flag-icon-ru"></span> RUB Russia Ruble ' },
            { text: 'RWF Rwanda Franc ', value: 'RWF', dataContent: '<span class="flag-icon flag-icon-rw"></span> RWF Rwanda Franc ' },
            { text: 'SAR Saudi Arabia Riyal ', value: 'SAR', dataContent: '<span class="flag-icon flag-icon-sa"></span> SAR Saudi Arabia Ri' },
            { text: 'SBD Solomon Islands Doll ', value: 'SBD', dataContent: '<span class="flag-icon flag-icon-sb"></span> SBD Solomon Islands' },
            { text: 'SCR Seychelles Rupee ', value: 'SCR', dataContent: '<span class="flag-icon flag-icon-sc"></span> SCR Seychelles Rupe' },
            { text: 'SDG Sudan Pound ', value: 'SDG', dataContent: '<span class="flag-icon flag-icon-sd"></span> SDG Sudan Pound ' },
            { text: 'SEK Sweden Krona ', value: 'SEK', dataContent: '<span class="flag-icon flag-icon-se"></span> SEK Sweden Krona ' },
            { text: 'SGD Singapore Dollar ', value: 'SGD', dataContent: '<span class="flag-icon flag-icon-sg"></span> SGD Singapore Dolla' },
            { text: 'SHP Saint Helena Pound ', value: 'SHP', dataContent: '<span class="flag-icon flag-icon-sh"></span> SHP Saint Helena Po' },
            { text: 'SLL Sierra Leone Leone ', value: 'SLL', dataContent: '<span class="flag-icon flag-icon-sl"></span> SLL Sierra Leone Le' },
            { text: 'SOS Somalia Shilling ', value: 'SOS', dataContent: '<span class="flag-icon flag-icon-so"></span> SOS Somalia Shillin' },
            { text: 'SPL Seborga Luigino ', value: 'SPL', dataContent: '<span class="flag-icon flag-icon-"></span> SPL Seborga Luigino' },
            { text: 'SRD Suriname Dollar ', value: 'SRD', dataContent: '<span class="flag-icon flag-icon-sr"></span> SRD Suriname Dollar' },
            { text: 'SSP South Sudanese Pound ', value: 'SSP', dataContent: '<span class="flag-icon flag-icon-ss"></span> SSP South Sudanese ' },
            { text: 'STD Sao Tome and Princip ', value: 'STD', dataContent: '<span class="flag-icon flag-icon-st"></span> STD Sao Tome and Pr' },
            { text: 'SVC El Salvador Colon ', value: 'SVC', dataContent: '<span class="flag-icon flag-icon-sv"></span> SVC El Salvador Col' },
            { text: 'SYP Syria Pound ', value: 'SYP', dataContent: '<span class="flag-icon flag-icon-sy"></span> SYP Syria Pound ' },
            { text: 'SZL Swaziland Lilangeni ', value: 'SZL', dataContent: '<span class="flag-icon flag-icon-sz"></span> SZL Swaziland Lilan' },
            { text: 'THB Thailand Baht ', value: 'THB', dataContent: '<span class="flag-icon flag-icon-th"></span> THB Thailand Baht ' },
            { text: 'TJS Tajikistan Somoni ', value: 'TJS', dataContent: '<span class="flag-icon flag-icon-tj"></span> TJS Tajikistan Somo' },
            { text: 'TMT Turkmenistan Manat ', value: 'TMT', dataContent: '<span class="flag-icon flag-icon-tm"></span> TMT Turkmenistan Ma' },
            { text: 'TND Tunisia Dinar ', value: 'TND', dataContent: '<span class="flag-icon flag-icon-tn"></span> TND Tunisia Dinar ' },
            { text: 'TOP Tonga Paanga ', value: 'TOP ', dataContent: ' < span class = "flag-icon flag-icon-to" > < /span> TOP Tonga Paanga ' },
            { text: 'TRY Turkey Lira ', value: 'TRY', dataContent: '<span class="flag-icon flag-icon-tr"></span> TRY Turkey Lira ' },
            { text: 'TTD Trinidad and Tobago ', value: 'TTD', dataContent: '<span class="flag-icon flag-icon-tt"></span> TTD Trinidad and To' },
            { text: 'TVD Tuvalu Dollar ', value: 'TVD', dataContent: '<span class="flag-icon flag-icon-tv"></span> TVD Tuvalu Dollar ' },
            { text: 'TWD Taiwan New Dollar ', value: 'TWD', dataContent: '<span class="flag-icon flag-icon-tw"></span> TWD Taiwan New Doll' },
            { text: 'TZS Tanzania Shilling ', value: 'TZS', dataContent: '<span class="flag-icon flag-icon-tz"></span> TZS Tanzania Shilli' },
            { text: 'UAH Ukraine Hryvnia ', value: 'UAH', dataContent: '<span class="flag-icon flag-icon-ua"></span> UAH Ukraine Hryvnia' },
            { text: 'UGX Uganda Shilling ', value: 'UGX', dataContent: '<span class="flag-icon flag-icon-ug"></span> UGX Uganda Shilling' },
            { text: 'USD United States Dollar ', value: 'USD', dataContent: '<span class="flag-icon flag-icon-us"></span> USD US Dollar' },
            { text: 'UYU Uruguay Peso ', value: 'UYU', dataContent: '<span class="flag-icon flag-icon-uy"></span> UYU Uruguay Peso ' },
            { text: 'UZS Uzbekistan Som ', value: 'UZS', dataContent: '<span class="flag-icon flag-icon-uz"></span> UZS Uzbekistan Som ' },
            { text: 'VEF Venezuela Bolivar ', value: 'VEF', dataContent: '<span class="flag-icon flag-icon-ve"></span> VEF Venezuela Boliv' },
            { text: 'VND Viet Nam Dong ', value: 'VND', dataContent: '<span class="flag-icon flag-icon-vn"></span> VND Viet Nam Dong ' },
            { text: 'VUV Vanuatu Vatu ', value: 'VUV', dataContent: '<span class="flag-icon flag-icon-vu"></span> VUV Vanuatu Vatu ' },
            { text: 'WST Samoa Tala ', value: 'WST', dataContent: '<span class="flag-icon flag-icon-ws"></span> WST Samoa Tala ' },
            { text: 'XAF Central Africa CFA fra ', value: 'XAF', dataContent: '<span class="flag-icon flag-icon-"></span> XAF Central AfricaC' },
            { text: 'XCD East Caribbean Dolla ', value: 'XCD', dataContent: '<span class="flag-icon flag-icon-"></span> XCD East Caribbean ' },
            { text: 'XOF West African CFA fra ', value: 'XOF', dataContent: '<span class="flag-icon flag-icon-cf"></span> XOF West African CF' },
            { text: 'XPF CFP Franc ', value: 'XPF', dataContent: '<span class="flag-icon flag-icon-pf"></span> XPF CFP Franc ' },
            { text: 'YER Yemen Rial ', value: 'YER', dataContent: '<span class="flag-icon flag-icon-ye"></span> YER Yemen Rial ' },
            { text: 'ZAR South Africa Rand ', value: 'ZAR', dataContent: '<span class="flag-icon flag-icon-za"></span> ZAR South Africa Ra' },
            { text: 'ZMW Zambia Kwacha ', value: 'ZMW', dataContent: '<span class="flag-icon flag-icon-zm"></span> ZMW Zambia Kwacha ' },
            { text: 'ZWD Zimbabwe Dollar ', value: 'ZWD', dataContent: '<span class="flag-icon flag-icon-zw"></span> ZWD Zimbabwe Dollar' },
            { text: 'ZWL Zimbabwe Dollar', value: 'ZWL', dataContent: '<span class="flag-icon flag-icon-zw"></span> ZWL Zimbabwe Dollar' }

        ],
    }
});

/**
 * web socket Connection
 */
const websocket = new Vue({
    data: {
        message: "",
        logs: [],
        status: "disconnected"
    },

    methods: {
        connect() { //Connecting to websocket server
            this.socket = new WebSocket(ServiceUrls.hubConnection.webSocketUrl);

            this.socket.onopen = () => { //get all the response here
                this.status = "connected";
                console.log("Staus:" + this.status);
                this.socket.onmessage = ({ data }) => {
                    console.log("Recieved message:");
                    console.log(data);
                    var msg = JSON.parse(data);
                    if (msg.message == 'TokenExpired') {
                        commonlogin(function () {
                            window.location.reload();
                        });
                    }

                    if (msg.response || msg.requestDetails) {
                        console.log("INN");
                        if (msg.response) {
                            serviceId = msg.response.service;
                        } else {
                            serviceId = msg.requestDetails.service;
                        }
                        // var serviceId = msg.response.service;
                        console.log(serviceId);
                        switch (serviceId) {
                            case 'FlightRS':
                                //Flight Search
                                searchResults.processFlightResult(msg);
                                break;
                            case 'HotelRS':
                                //hotel Search
                                console.log("Hotel Search!!");
                                Vue_HotelListing.fetchHotelList(msg);
                                break;
                            case 'InsuranceRS':
                            case 'InsuranceUniTrustRS':
                            case 'InsuranceWisRS':
                                //insurance Search
                                Vue_FlightBook.processInsuranceResult(msg);
                            default:
                                break;
                        }

                    }
                };

            };
        },
        disconnect() { //Disconnect connection from websocket server
            this.socket.close();
            this.status = "disconnected";
            this.logs = [];
        },
        sendMessage(msg) { //sending requests to websocket server
            var self = this;
            waitForSocketConnection(this.socket, function () {
                console.log("message sent!!!");
                self.socket.send(msg);
                console.log("Sent message:" + msg);
                self.message = "";
            });

        }
    },

});


// Make the function wait until the connection is made...
function waitForSocketConnection(socket, callback) {
    setTimeout(
        function () {
            if (socket.readyState === 1) {
                console.log("Connection is made")
                if (callback != null) {
                    callback();
                }
            } else {
                console.log("wait for connection...")
                waitForSocketConnection(socket, callback);
            }

        }, 1); // wait 1 milisecond for the connection...
}



var languagecomp = Vue.component('lang-select', {
    template: ` <ul>
  <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" id="dropdown09"
         data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></span>{{language}}
         <svg focusable="false" color="inherit" fill="currentcolor" aria-hidden="true" role="presentation" viewBox="0 0 150 150" preserveAspectRatio="xMidYMid meet" size="12" width="12" height="12" class="sc-chPdSV cKssQA"><path d="M86.8 118.6l60.1-61.5c5.5-5.7 5.5-14.8 0-20.5l-5-5.1c-5.4-5.5-14.3-5.6-19.8-.2l-.2.2L76 78.4 30.1 31.5c-5.4-5.5-14.3-5.6-19.8-.2l-.2.2-5 5.1c-5.5 5.7-5.5 14.8 0 20.5l60.1 61.5c2.8 2.9 6.8 4.4 10.8 4.1 4 .3 8-1.3 10.8-4.1z"></path></svg></a>
      <div class="nav-link dropdown-toggle dropdown-menu" aria-labelledby="dropdown09">
          <a href="#" class="dropdown-item"  v-for="item in Languages" @click.prevent="onSelected(item.code, item.lang)">{{item.lang}}</a>
          
      </div>
  </li>
</ul>`,
    data() {
        return {
            Languages: [],
            language: (localStorage.Language) ? localStorage.Language : 'English',
            languagecode: 'en'

        }
    },
    methods: {

        onSelected: function (code, lang) {
            localStorage.Languagecode = code;
            localStorage.Language = lang;
            this.languagecode = code;
            this.language = lang;
            localStorage.Languages = this.Languages;
            this.$emit('languagechange');

            //language change direction  

            var hName = window.location.pathname.toString().split("/")[1].toLowerCase();
            var pagePath = (window.location.href.indexOf(hName + "/demo-page") > -1) ? hName + "/demo-page" : window.location.pathname.toLowerCase();

            //  if (pagePath != '/Flights/flight-listing.html' && pagePath != '/Hotels/hotel-listing.html') {


            const html = document.documentElement
            if (this.languagecode == 'ar') {

                html.setAttribute('dir', 'rtl');
                html.setAttribute('lang', 'ar');
                localStorage.direction = "rtl";

                if (pagePath == '/Nirvana/') {
                    maininstance.arabic_dropdown = "dwn_icon";
                    // hotelSearch.arabic_dropdown = "dwn_icon";

                }


                //hotelRooms.arabic_dropdown= "dwn_icon";

                $(".en_dwn").addClass("dwn_icon");
                // $("#rangeSlider").addClass("direction_rtl");
                $(".arborder").addClass("arbrder_left");
                $(".ar_direction").addClass("ar_direction1");

                $(".footer_address_sec").addClass("ar_direction12");




            } else {
                html.setAttribute('dir', 'ltr');
                html.setAttribute('lang', 'en');
                localStorage.direction = "ltr";
                if (pagePath == '/Nirvana/') {
                    maininstance.arabic_dropdown = "";
                    //hotelSearch.arabic_dropdown = "";

                }


                // hotelRooms.arabic_dropdown= "";
                $(".en_dwn").removeClass("dwn_icon");
                // $("#rangeSlider").removeClass("direction_rtl");
                $(".arborder").removeClass("arbrder_left");
                $(".ar_direction").removeClass("ar_direction1");

                $(".footer_address_sec").removeClass("ar_direction12");




            }


            //}

            // if (localStorage.direction == 'rtl') {
            //     maininstance.arabicClass = "arDirection";
            // }else{
            //     maininstance.arabicClass = "";
            // }



            // vm.$forceUpdate();
            // if (window.location.pathname == "/A2Z_India/") {
            //     cmsapp.getcmsdata();

            // }
            // else if (window.location.pathname == "/Hotels/hotel-listing.html") {

            // }
            // else if (window.location.pathname == "/Hotels/hotel-detail.html") {

            // }
            // else if (window.location.href.indexOf("A2Z_India/demo-page") > -1) {
            //     holidy.getpagecontent();
            // }
            // else {
            //     window.location.reload();
            // }
            //FooterComponent.$forceUpdate();
            // footerinstance.$forceUpdate();
            footerinstance.key = Math.random();
            //  var hName = window.location.pathname.toString().split("/")[1].toLowerCase();
            //   var pagePath = (window.location.href.indexOf(hName + "/demo-page") > -1) ? hName + "/demo-page" : window.location.pathname.toLowerCase();

            switch (pagePath) {
                case "/A2Z_India/":
                    cmsapp.getcmsdata();
                    break;
                case "/SGS_Demo/":
                    cmsapp.getcmsdata();
                    break;    
                case "/Nirvana/":
                    maininstance.getPagecontent();
                    break;
                case "/Nirvana/index.html":
                    maininstance.getPagecontent();
                    break;
                case "/Flights/flight-listing.html":
                    window.location.reload();
                    break;
                case "/Flights/flight-booking.html":
                case "/Flights/flight-confirmation.html":
                case "/Hotels/hotel-listing.html":
                    window.location.reload();
                    break;
                case "/Hotels/hotel-detail.html":
                    el_hotelDetail.$children[0].getPagecontent();
                    break;
                case "/" + hName + "/demo-page/index.html":
                    holidy.getpagecontent();
                    break;
                case "/Nirvana/book-now.html":
                    Packageinfo.getPagecontent();
                    break;
                case "/" + hName + "/demo-page/packages.html":
                    holidypack.getpagecontent();
                    break;
                case "/" + hName + "/demo-page/holidaydetails.html":
                    packageView.getPageTitle();
                    break;
                case "/Nirvana/packageInfo.html":
                    packageDetailInfo.getPagecontent();
                    break;
                case "/Nirvana/package.html":
                    packagelist.getPagecontent();
                    break;
                case "/Nirvana/events.html":
                    packagelist.getPagecontent();
                    break;
                case "/Nirvana/offers.html":
                    packagelist.getPagecontent();
                    break;
                case "/Nirvana/uae-attractions.html":
                    packagelist.getPagecontent();
                    break;
                case "/Nirvana/aboutus.html":
                    packagelist.getPagecontent();
                    break;
                case "/my-profile.html":
                    myprofile_vue.getPagecontent();
                    myprofile_vue.key = Math.random();
                    break;
                case "/easy2go/package.html":
                    packagelist.getPagecontent();
                    break;
                case "/easy2go/offers.html":
                    packagelist.getPagecontent();
                    break;
                case "/easy2go/":
                    maininstance.getPagecontent();
                    break;
                case "/Biscordint/":
                    maininstance.pageContent();
                    break;
                case "/Biscordint/flight-deals.html":
                    packagelist.getPageTitle();
                    break;
                case "/Biscordint/flight-deals-details.html":
                    packageView.getPageTitle();
                    break;
                case "/Biscordint/contact-us.html":
                    contact.getPagecontent();
                    break;
                case "/Biscordint/holiday-packagesview.html":
                    Packageinfo.getPagecontent();
                    break;
                case "/Biscordint/apply-visa.html":
                    applyvisa.getPagecontent();
                    break;
                case "/Biscordint/apply-visa-form.html":
                    visa.getPagecontent();
                    break;
                case "/CompareTicket/":
                    maininstance.pageContent();
                    break;
                case "/CompareTicket/index.html":
                    maininstance.pageContent();
                    break;
                case "/spartan/":
                    maininstance.getPagecontent();
                    break;
                case "/spartan/flights.html":
                    maininstance.getPagecontent();
                    break;
                case "/spartan/hotel.html":
                    hotelList.getPagecontent();
                    break;
                case "/spartan/hotel-results.html":
                    HotelList.getLabels();
                    break;
                case "/spartan/hotel-details.html":
                    packageView.getPagedata();
                    break;
                case "/spartan/tent-accommodation.html":
                    tentList.getBanner();
                    break;
                case "/spartan/tent-result.html":
                    tentList.getLabels();
                    tentList.getPackage();
                    break;
                case "/spartan/tent-details.html":
                    packageView.getLabels();
                    packageView.getPackagedetails();
                    break;
                case "/spartan/transfers.html":
                    maininstance.getPagecontent();
                    break;
                case "/spartan/transfer-results.html":
                    maininstance.getPagecontent();
                    break;
                case "/spartan/transfer-booking.html":
                    maininstance.getPagecontent();
                    break;
                case "/spartan/visa.html":
                    visa.getPagecontent();
                    break;
                case "/spartan/tours.html":
                    packageList.getPackage();
                    break;
                case "/spartan/tour-results.html":
                    packageList.getdata();
                    break;
                case "/spartan/tour-details.html":
                    packageView.getPageDetails();
                    break;
                case "/spartan/car-rental.html":
                    packageList.getPagecontent();
                    break;
                case "/spartan/car-rental-results.html":
                    maininstance.getPagedata();
                    break;
                case "/spartan/car-rental-booking.html":
                    maininstance.getPagedata();
                    break;
                case "/spartan/self-camping.html":
                    tentList.getBanner();
                    break;
                case "/spartan/self-camping-result.html":
                    tentList.getLabels();
                    tentList.getPackage();
                    break;
                case "/spartan/self-camping-details.html":
                    packageView.getLabels();
                    packageView.getPackagedetails();
                    break;
                case "/spartan/camp-overview.html":
                    campOverview.getPagecontent();
                    break;

                case "/khartoumtkt/":
                    maininstance.pageContent();
                    maininstance.getNewsEvent();
                    break;
                case "/khartoumtkt/index.html":
                    maininstance.pageContent();
                    maininstance.getNewsEvent();
                    break;
                case "/khartoumtkt/about.html":
                    aboutPage.getData();
                    break;
                case "/khartoumtkt/promotions.html":
                    promtionPage.getData();
                    break;
                case "/khartoumtkt/contact.html":
                    contact.getContactData();
                    break;
                case "/khartoumtkt/news-event.html":
                    newsPage.getData();
                    newsPage.getNewsDetails();
                    break;
                case "/khartoumtkt/news-detail.html":
                    newsPage.getData();
                    newsPage.getNewsDetails();
                    break;
                case "/khartoumtkt/privacypolicy.html":
                    privacyPage.getData();
                    break;
                case "/khartoumtkt/terms-condition.html":
                    termsPage.getData();
                    break;

                default:
                    window.location.reload();
                    break;
            }

        }
    },
    mounted: function () {


        var hName = window.location.pathname.toString().split("/")[1].toLowerCase();
        var pagePath = (window.location.href.indexOf(hName + "/demo-page") > -1) ? hName + "/demo-page" : window.location.pathname.toLowerCase();
        //  if (pagePath != '/Flights/flight-listing.html' && pagePath != '/Hotels/hotel-listing.html') {
        if (localStorage.direction) {

            const html = document.documentElement;
            html.setAttribute('dir', localStorage.direction);
            html.setAttribute('lang', localStorage.Languagecode);


        }

        if (localStorage.direction == 'rtl') {
            $(".arborder").addClass("arbrder_left");
            $(".ar_direction").addClass("ar_direction1");

            // $(".en_dwn").addClass("dwn_icon");
        } else {
            $(".arborder").removeClass("arbrder_left");
            $(".ar_direction").removeClass("ar_direction1");

            //$(".en_dwn").removeClass("dwn_icon");

        }

        // }

        var self = this;
        getAgencycode(function (response) {
            let axiosConfig = {
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8'
                }
            };
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var AgencyCode = response;
            var requrl = '/templates?path=/B2B/AdminPanel/CMS/' + AgencyCode + '/Template/Home/Home'
            //forgot password

            axios.get(huburl + portno + requrl, axiosConfig)
                .then((response) => {
                    var languages = response.data;
                    if (languages) {
                        languages = languages.filter(function (item) {
                            return item.indexOf("B2B/AdminPanel") !== 0;
                        });

                        languages.map(function (value, key) {
                            var langauge = value.split('.')[0].split('_')[1];
                            $.getJSON('/Resources/HubUrls/languages.json', function (json) {
                                let lang = json.Languages.find(o => o.code.toLowerCase() === langauge.toLowerCase());
                                if (typeof lang !== "undefined") {
                                    self.Languages.push({ 'code': lang.code, 'lang': lang.nativeName });
                                }
                            })
                        })

                    }
                })
                .catch((err) => {


                });
        });


    }


});
var currencycomp = Vue.component('currency-select', {
    template: ` <ul v-show="showcurrncy">
  <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" id="dropdown09"
         data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-html="currencyarr.dataContent"></a>
      <div class="nav-link dropdown-toggle dropdown-menu" aria-labelledby="dropdown09">
          <a href="#" class="dropdown-item"  v-for="item in currencies" @click.prevent="onSelected(item.value, item.rate)" v-html="item.dataContent"></a>
          
      </div>
  </li>
</ul>`,
    data() {
        return {
            currencies: [],
            selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
            CurrencyOptions: [
                { text: 'AED', desc: 'UAE Dirham', value: 'AED', dataContent: '<span class="flag-icon flag-icon-ae"></span> AED' },
                { text: 'AFN', desc: 'Afghanistan Afghani ', value: 'AFN', dataContent: '<span class="flag-icon flag-icon-af"></span> AFN' },
                { text: 'ALL', desc: 'Albania Lek ', value: 'ALL', dataContent: '<span class="flag-icon flag-icon-al"></span> ALL' },
                { text: 'AMD', desc: 'Armenia Dram ', value: 'AMD', dataContent: '<span class="flag-icon flag-icon-am"></span> AMD' },
                { text: 'ANG', desc: 'Netherlands Antilles ', value: 'ANG', dataContent: '<span class="flag-icon flag-icon-nl"></span> ANG' },
                { text: 'AOA', desc: 'Angola Kwanza ', value: 'AOA', dataContent: '<span class="flag-icon flag-icon-ao"></span> AOA' },
                { text: 'ARS', desc: 'Argentina Peso ', value: 'ARS', dataContent: '<span class="flag-icon flag-icon-ar"></span> ARS' },
                { text: 'AUD', desc: 'Australia Dollar ', value: 'AUD', dataContent: '<span class="flag-icon flag-icon-au"></span> AUD' },
                { text: 'AWG', desc: 'Aruba Guilder ', value: 'AWG', dataContent: '<span class="flag-icon flag-icon-aw"></span> AWG' },
                { text: 'AZN', desc: 'Azerbaijan New Manat ', value: 'AZN', dataContent: '<span class="flag-icon flag-icon-az"></span> AZN' },
                { text: 'BAM', desc: 'Bosnia and Herzegovi', value: 'BAM', dataContent: '<span class="flag-icon flag-icon-ba"></span> BAM' },
                { text: 'BBD', desc: 'Barbados Dollar', value: 'BBD', dataContent: '<span class="flag-icon flag-icon-bb"></span> BBD' },
                { text: 'BDT', desc: 'Bangladesh Taka', value: 'BDT', dataContent: '<span class="flag-icon flag-icon-bd"></span> BDT' },
                { text: 'BGN', desc: 'Bulgaria Lev', value: 'BGN', dataContent: '<span class="flag-icon flag-icon-bg"></span> BGN' },
                { text: 'BHD', desc: 'Bahrain Dinar', value: 'BHD', dataContent: '<span class="flag-icon flag-icon-bh"></span> BHD' },
                { text: 'BIF', desc: 'Burundi Franc', value: 'BIF', dataContent: '<span class="flag-icon flag-icon-bi"></span> BIF' },
                { text: 'BMD', desc: 'Bermuda Dollar', value: 'BMD', dataContent: '<span class="flag-icon flag-icon-bm"></span> BMD' },
                { text: 'BND', desc: 'Brunei Darussalam Do', value: 'BND', dataContent: '<span class="flag-icon flag-icon-bn"></span> BND' },
                { text: 'BOB', desc: 'Bolivian boliviano', value: 'BOB', dataContent: '<span class="flag-icon flag-icon-b0"></span> BOB' },
                { text: 'BRL', desc: 'Brazil Real', value: 'BRL', dataContent: '<span class="flag-icon flag-icon-br"></span> BRL' },
                { text: 'BSD', desc: 'Bahamas Dollar', value: 'BSD', dataContent: '<span class="flag-icon flag-icon-bs"></span> BSD' },
                { text: 'BTN', desc: 'Bhutan Ngultrum', value: 'BTN', dataContent: '<span class="flag-icon flag-icon-bt"></span> BTN' },
                { text: 'BWP', desc: 'Botswana Pula', value: 'BWP', dataContent: '<span class="flag-icon flag-icon-bw"></span> BWP' },
                { text: 'BYN', desc: 'Belarus Ruble', value: 'BYN', dataContent: '<span class="flag-icon flag-icon-by"></span> BYN' },
                { text: 'BZD', desc: 'Belize Dollar', value: 'BZD', dataContent: '<span class="flag-icon flag-icon-bz"></span> BZD' },
                { text: 'CAD', desc: 'Canada Dollar', value: 'CAD', dataContent: '<span class="flag-icon flag-icon-ca"></span> CAD' },
                { text: 'CDF', desc: 'Congo/Kinshasa Franc', value: 'CDF', dataContent: '<span class="flag-icon flag-icon-ca"></span> CDF' },
                { text: 'CHF', desc: 'Switzerland Franc', value: 'CHF', dataContent: '<span class="flag-icon flag-icon-ch"></span> CHF' },
                { text: 'CLP', desc: 'Chile Peso', value: 'CLP', dataContent: '<span class="flag-icon flag-icon-cl"></span> CLP' },
                { text: 'CNY', desc: 'China Yuan Renminbi', value: 'CNY', dataContent: '<span class="flag-icon flag-icon-cn"></span> CNY' },
                { text: 'COP', desc: 'Colombia Peso', value: 'COP', dataContent: '<span class="flag-icon flag-icon-co"></span> COP' },
                { text: 'CRC', desc: 'Costa Rica Colon', value: 'CRC', dataContent: '<span class="flag-icon flag-icon-cr"></span> CRC' },
                { text: 'CUC', desc: 'Cuba Convertible Pes', value: 'CUC', dataContent: '<span class="flag-icon flag-icon-cu"></span> CUC' },
                { text: 'CUP', desc: 'Cuba Peso', value: 'CUP', dataContent: '<span class="flag-icon flag-icon-cu"></span> CUP' },
                { text: 'CVE', desc: 'Cape Verde Escudo', value: 'CVE', dataContent: '<span class="flag-icon flag-icon-cv"></span> CVE' },
                { text: 'CZK', desc: 'Czech Republic Korun', value: 'CZK', dataContent: '<span class="flag-icon flag-icon-cz"></span> CZK' },
                { text: 'DJF', desc: 'Djibouti Franc', value: 'DJF', dataContent: '<span class="flag-icon flag-icon-dj"></span> DJF' },
                { text: 'DKK', desc: 'Denmark Krone', value: 'DKK', dataContent: '<span class="flag-icon flag-icon-dk"></span> DKK' },
                { text: 'DOP', desc: 'Dominican Republic P', value: 'DOP', dataContent: '<span class="flag-icon flag-icon-do"></span> DOP' },
                { text: 'DZD', desc: 'Algeria Dinar', value: 'DZD', dataContent: '<span class="flag-icon flag-icon-dz"></span> DZD' },
                { text: 'EGP', desc: 'Egypt Pound', value: 'EGP', dataContent: '<span class="flag-icon flag-icon-eg"></span> EGP' },
                { text: 'ERN', desc: 'Eritrea Nakfa', value: 'ERN', dataContent: '<span class="flag-icon flag-icon-er"></span> ERN' },
                { text: 'ETB', desc: 'Ethiopia Birr', value: 'ETB', dataContent: '<span class="flag-icon flag-icon-et"></span> ETB' },
                { text: 'EUR', desc: 'Euro Member Countrie', value: 'EUR', dataContent: '<span class="flag-icon flag-icon-fr"></span> EUR' },
                { text: 'FJD', desc: 'Fiji Dollar', value: 'FJD', dataContent: '<span class="flag-icon flag-icon-fj"></span> FJD' },
                { text: 'FKP', desc: 'Falkland Islands (Ma', value: 'FKP', dataContent: '<span class="flag-icon flag-icon-fk"></span> FKP' },
                { text: 'GBP', desc: 'United Kingdom Pound', value: 'GBP', dataContent: '<span class="flag-icon flag-icon-gb"></span> GBP' },
                { text: 'GEL', desc: 'Georgia Lari', value: 'GEL', dataContent: '<span class="flag-icon flag-icon-ge"></span> GEL' },
                { text: 'GGP', desc: 'Guernsey Pound', value: 'GGP', dataContent: '<span class="flag-icon flag-icon-gg"></span> GGP' },
                { text: 'GHS', desc: 'Ghana Cedi', value: 'GHS', dataContent: '<span class="flag-icon flag-icon-gh"></span> GHS' },
                { text: 'GIP', desc: 'Gibraltar Pound', value: 'GIP', dataContent: '<span class="flag-icon flag-icon-gi"></span> GIP' },
                { text: 'GMD', desc: 'Gambia Dalasi', value: 'GMD', dataContent: '<span class="flag-icon flag-icon-gm"></span> GMD' },
                { text: 'GNF', desc: 'Guinea Franc', value: 'GNF', dataContent: '<span class="flag-icon flag-icon-gn"></span> GNF' },
                { text: 'GTQ', desc: 'Guatemala Quetzal', value: 'GTQ', dataContent: '<span class="flag-icon flag-icon-gt"></span> GTQ' },
                { text: 'GYD', desc: 'Guyana Dollar', value: 'GYD', dataContent: '<span class="flag-icon flag-icon-gy"></span> GYD' },
                { text: 'HKD', desc: 'Hong Kong Dollar', value: 'HKD', dataContent: '<span class="flag-icon flag-icon-hk"></span> HKD' },
                { text: 'HNL', desc: 'Honduras Lempira', value: 'HNL', dataContent: '<span class="flag-icon flag-icon-hn"></span> HNL' },
                { text: 'HRK', desc: 'Croatia Kuna', value: 'HRK', dataContent: '<span class="flag-icon flag-icon-hr"></span> HRK' },
                { text: 'HTG', desc: 'Haiti Gourde', value: 'HTG', dataContent: '<span class="flag-icon flag-icon-ht"></span> HTG' },
                { text: 'HUF', desc: 'Hungary Forint', value: 'HUF', dataContent: '<span class="flag-icon flag-icon-hu"></span> HUF' },
                { text: 'IDR', desc: 'Indonesia Rupiah', value: 'IDR', dataContent: '<span class="flag-icon flag-icon-id"></span> IDR' },
                { text: 'ILS', desc: 'Israel Shekel', value: 'ILS', dataContent: '<span class="flag-icon flag-icon-il"></span> ILS' },
                { text: 'IMP', desc: 'Isle of Man Pound', value: 'IMP', dataContent: '<span class="flag-icon flag-icon-im"></span> IMP' },
                { text: 'INR', desc: 'India Rupee', value: 'INR', dataContent: '<span class="flag-icon flag-icon-in"></span> INR' },
                { text: 'IQD', desc: 'Iraq Dinar', value: 'IQD', dataContent: '<span class="flag-icon flag-icon-iq"></span> IQD' },
                { text: 'IRR', desc: 'Iran Rial', value: 'IRR', dataContent: '<span class="flag-icon flag-icon-ir"></span> IRR' },
                { text: 'ISK', desc: 'Iceland Krona', value: 'ISK', dataContent: '<span class="flag-icon flag-icon-is"></span> ISK' },
                { text: 'JEP', desc: 'Jersey Pound', value: 'JEP', dataContent: '<span class="flag-icon flag-icon-je"></span> JEP' },
                { text: 'JMD', desc: 'Jamaica Dollar', value: 'JMD', dataContent: '<span class="flag-icon flag-icon-jm"></span> JMD' },
                { text: 'JOD', desc: 'Jordan Dinar', value: 'JOD', dataContent: '<span class="flag-icon flag-icon-jo"></span> JOD' },
                { text: 'JPY', desc: 'Japan Yen', value: 'JPY', dataContent: '<span class="flag-icon flag-icon-jp"></span> JPY' },
                { text: 'KES', desc: 'Kenya Shilling', value: 'KES', dataContent: '<span class="flag-icon flag-icon-ke"></span> KES' },
                { text: 'KGS', desc: 'Kyrgyzstan Som', value: 'KGS', dataContent: '<span class="flag-icon flag-icon-kg"></span> KGS' },
                { text: 'KHR', desc: 'Cambodia Riel', value: 'KHR', dataContent: '<span class="flag-icon flag-icon-kh"></span> KHR' },
                { text: 'KMF', desc: 'Comoros Franc', value: 'KMF', dataContent: '<span class="flag-icon flag-icon-km"></span> KMF' },
                { text: 'KPW', desc: 'Korea (North) Won', value: 'KPW', dataContent: '<span class="flag-icon flag-icon-kp"></span> KPW' },
                { text: 'KRW', desc: 'Korea (South) Won', value: 'KRW', dataContent: '<span class="flag-icon flag-icon-kr"></span> KRW' },
                { text: 'KWD', desc: 'Kuwait Dinar', value: 'KWD', dataContent: '<span class="flag-icon flag-icon-kw"></span> KWD' },
                { text: 'KYD', desc: 'Cayman Islands Dolla', value: 'KYD', dataContent: '<span class="flag-icon flag-icon-ky"></span> KYD' },
                { text: 'KZT', desc: 'Kazakhstan Tenge', value: 'KZT', dataContent: '<span class="flag-icon flag-icon-kz"></span> KZT' },
                { text: 'LAK', desc: 'Laos Kip', value: 'LAK', dataContent: '<span class="flag-icon flag-icon-la"></span> LAK' },
                { text: 'LBP', desc: 'Lebanon Pound', value: 'LBP', dataContent: '<span class="flag-icon flag-icon-lb"></span> LBP' },
                { text: 'LKR', desc: 'Sri Lanka Rupee', value: 'LKR', dataContent: '<span class="flag-icon flag-icon-lk"></span> LKR' },
                { text: 'LRD', desc: 'Liberia Dollar', value: 'LRD', dataContent: '<span class="flag-icon flag-icon-lr"></span> LRD' },
                { text: 'LSL', desc: 'Lesotho Loti', value: 'LSL', dataContent: '<span class="flag-icon flag-icon-ls"></span> LSL' },
                { text: 'LYD', desc: 'Libya Dinar', value: 'LYD', dataContent: '<span class="flag-icon flag-icon-ly"></span> LYD' },
                { text: 'MAD', desc: 'Morocco Dirham', value: 'MAD', dataContent: '<span class="flag-icon flag-icon-ma"></span> MAD' },
                { text: 'MDL', desc: 'Moldova Leu', value: 'MDL', dataContent: '<span class="flag-icon flag-icon-md"></span> MDL' },
                { text: 'MGA', desc: 'Madagascar Ariary', value: 'MGA', dataContent: '<span class="flag-icon flag-icon-mg"></span> MGA' },
                { text: 'MKD', desc: 'Macedonia Denar', value: 'MKD', dataContent: '<span class="flag-icon flag-icon-mk"></span> MKD' },
                { text: 'MMK', desc: 'Myanmar (Burma) Kyat', value: 'MMK', dataContent: '<span class="flag-icon flag-icon-mm"></span> MMK' },
                { text: 'MNT', desc: 'Mongolia Tughrik', value: 'MNT', dataContent: '<span class="flag-icon flag-icon-mn"></span> MNT' },
                { text: 'MOP', desc: 'Macau Pataca', value: 'MOP', dataContent: '<span class="flag-icon flag-icon-mo"></span> MOP' },
                { text: 'MRO', desc: 'Mauritania Ouguiya', value: 'MRO', dataContent: '<span class="flag-icon flag-icon-mr"></span> MRO' },
                { text: 'MUR', desc: 'Mauritius Rupee', value: 'MUR', dataContent: '<span class="flag-icon flag-icon-mu"></span> MUR' },
                { text: 'MVR', desc: 'Maldives (Maldive Is', value: 'MVR', dataContent: '<span class="flag-icon flag-icon-mv"></span> MVR' },
                { text: 'MWK', desc: 'Malawi Kwacha', value: 'MWK', dataContent: '<span class="flag-icon flag-icon-mw"></span> MWK' },
                { text: 'MXN', desc: 'Mexico Peso', value: 'MXN', dataContent: '<span class="flag-icon flag-icon-mx"></span> MXN' },
                { text: 'MYR', desc: 'Malaysia Ringgit', value: 'MYR', dataContent: '<span class="flag-icon flag-icon-my"></span> MYR' },
                { text: 'MZN', desc: 'Mozambique Metical', value: 'MZN', dataContent: '<span class="flag-icon flag-icon-mz"></span> MZN' },
                { text: 'NAD', desc: 'Namibia Dollar', value: 'NAD', dataContent: '<span class="flag-icon flag-icon-na"></span> NAD' },
                { text: 'NGN', desc: 'Nigeria Naira', value: 'NGN', dataContent: '<span class="flag-icon flag-icon-ng"></span> NGN' },
                { text: 'NIO', desc: 'Nicaragua Cordoba', value: 'NIO', dataContent: '<span class="flag-icon flag-icon-ni"></span> NIO' },
                { text: 'NOK', desc: 'Norway Krone', value: 'NOK', dataContent: '<span class="flag-icon flag-icon-no"></span> NOK' },
                { text: 'NPR', desc: 'Nepal Rupee', value: 'NPR', dataContent: '<span class="flag-icon flag-icon-np"></span> NPR' },
                { text: 'NZD', desc: 'New Zealand Dollar', value: 'NZD', dataContent: '<span class="flag-icon flag-icon-nz"></span> NZD' },
                { text: 'OMR', desc: 'Oman Rial', value: 'OMR', dataContent: '<span class="flag-icon flag-icon-om"></span> OMR' },
                { text: 'PAB', desc: 'Panama Balboa', value: 'PAB', dataContent: '<span class="flag-icon flag-icon-pa"></span> PAB' },
                { text: 'PEN', desc: 'Peru Sol', value: 'PEN', dataContent: '<span class="flag-icon flag-icon-pe"></span> PEN' },
                { text: 'PGK', desc: 'Papua New Guinea Kin', value: 'PGK', dataContent: '<span class="flag-icon flag-icon-pg"></span> PGK' },
                { text: 'PHP', desc: 'Philippines Peso', value: 'PHP', dataContent: '<span class="flag-icon flag-icon-ph"></span> PHP' },
                { text: 'PKR', desc: 'Pakistan Rupee', value: 'PKR', dataContent: '<span class="flag-icon flag-icon-pk"></span> PKR' },
                { text: 'PLN', desc: 'Poland Zloty', value: 'PLN', dataContent: '<span class="flag-icon flag-icon-pl"></span> PLN' },
                { text: 'PYG', desc: 'Paraguay Guarani', value: 'PYG', dataContent: '<span class="flag-icon flag-icon-py"></span> PYG' },
                { text: 'QAR', desc: 'Qatar Riyal', value: 'QAR', dataContent: '<span class="flag-icon flag-icon-qa"></span> QAR' },
                { text: 'RON', desc: 'Romania New Leu', value: 'RON', dataContent: '<span class="flag-icon flag-icon-ro"></span> RON' },
                { text: 'RSD', desc: 'Serbia Dinar', value: 'RSD', dataContent: '<span class="flag-icon flag-icon-rs"></span> RSD' },
                { text: 'RUB', desc: 'Russia Ruble', value: 'RUB', dataContent: '<span class="flag-icon flag-icon-ru"></span> RUB' },
                { text: 'RWF', desc: 'Rwanda Franc', value: 'RWF', dataContent: '<span class="flag-icon flag-icon-rw"></span> RWF' },
                { text: 'SAR', desc: 'Saudi Arabia Riyal', value: 'SAR', dataContent: '<span class="flag-icon flag-icon-sa"></span> SAR' },
                { text: 'SBD', desc: 'Solomon Islands Doll', value: 'SBD', dataContent: '<span class="flag-icon flag-icon-sb"></span> SBD' },
                { text: 'SCR', desc: 'Seychelles Rupee', value: 'SCR', dataContent: '<span class="flag-icon flag-icon-sc"></span> SCR' },
                { text: 'SDG', desc: 'Sudan Pound', value: 'SDG', dataContent: '<span class="flag-icon flag-icon-sd"></span> SDG' },
                { text: 'SEK', desc: 'Sweden Krona', value: 'SEK', dataContent: '<span class="flag-icon flag-icon-se"></span> SEK' },
                { text: 'SGD', desc: 'Singapore Dollar', value: 'SGD', dataContent: '<span class="flag-icon flag-icon-sg"></span> SGD' },
                { text: 'SHP', desc: 'Saint Helena Pound', value: 'SHP', dataContent: '<span class="flag-icon flag-icon-sh"></span> SHP' },
                { text: 'SLL', desc: 'Sierra Leone Leone', value: 'SLL', dataContent: '<span class="flag-icon flag-icon-sl"></span> SLL' },
                { text: 'SOS', desc: 'Somalia Shilling', value: 'SOS', dataContent: '<span class="flag-icon flag-icon-so"></span> SOS' },
                { text: 'SPL', desc: 'Seborga Luigino', value: 'SPL', dataContent: '<span class="flag-icon flag-icon-"></span> SPL' },
                { text: 'SRD', desc: 'Suriname Dollar', value: 'SRD', dataContent: '<span class="flag-icon flag-icon-sr"></span> SRD' },
                { text: 'SSP', desc: 'South Sudanese Pound', value: 'SSP', dataContent: '<span class="flag-icon flag-icon-ss"></span> SSP' },
                { text: 'STD', desc: 'Sao Tome and Princip', value: 'STD', dataContent: '<span class="flag-icon flag-icon-st"></span> STD' },
                { text: 'SVC', desc: 'El Salvador Colon', value: 'SVC', dataContent: '<span class="flag-icon flag-icon-sv"></span> SVC' },
                { text: 'SYP', desc: 'Syria Pound', value: 'SYP', dataContent: '<span class="flag-icon flag-icon-sy"></span> SYP' },
                { text: 'SZL', desc: 'Swaziland Lilangeni', value: 'SZL', dataContent: '<span class="flag-icon flag-icon-sz"></span> SZL' },
                { text: 'THB', desc: 'Thailand Baht', value: 'THB', dataContent: '<span class="flag-icon flag-icon-th"></span> THB' },
                { text: 'TJS', desc: 'Tajikistan Somoni', value: 'TJS', dataContent: '<span class="flag-icon flag-icon-tj"></span> TJS' },
                { text: 'TMT', desc: 'Turkmenistan Manat', value: 'TMT', dataContent: '<span class="flag-icon flag-icon-tm"></span> TMT' },
                { text: 'TND', desc: 'Tunisia Dinar', value: 'TND', dataContent: '<span class="flag-icon flag-icon-tn"></span> TND' },
                { text: 'TOP', desc: 'Tonga Paanga', value: 'TOP ', dataContent: '<span class="flag-icon flag-icon-to"></span> TOP' },
                { text: 'TRY', desc: 'Turkey Lira', value: 'TRY', dataContent: '<span class="flag-icon flag-icon-tr"></span> TRY' },
                { text: 'TTD', desc: 'Trinidad and Tobago', value: 'TTD', dataContent: '<span class="flag-icon flag-icon-tt"></span> TTD' },
                { text: 'TVD', desc: 'Tuvalu Dollar', value: 'TVD', dataContent: '<span class="flag-icon flag-icon-tv"></span> TVD' },
                { text: 'TWD', desc: 'Taiwan New Dollar', value: 'TWD', dataContent: '<span class="flag-icon flag-icon-tw"></span> TWD' },
                { text: 'TZS', desc: 'Tanzania Shilling', value: 'TZS', dataContent: '<span class="flag-icon flag-icon-tz"></span> TZS' },
                { text: 'UAH', desc: 'Ukraine Hryvnia', value: 'UAH', dataContent: '<span class="flag-icon flag-icon-ua"></span> UAH' },
                { text: 'UGX', desc: 'Uganda Shilling', value: 'UGX', dataContent: '<span class="flag-icon flag-icon-ug"></span> UGX' },
                { text: 'USD', desc: 'United States Dollar', value: 'USD', dataContent: '<span class="flag-icon flag-icon-us"></span> USD' },
                { text: 'UYU', desc: 'Uruguay Peso', value: 'UYU', dataContent: '<span class="flag-icon flag-icon-uy"></span> UYU' },
                { text: 'UZS', desc: 'Uzbekistan Som', value: 'UZS', dataContent: '<span class="flag-icon flag-icon-uz"></span> UZS' },
                { text: 'VEF', desc: 'Venezuela Bolivar', value: 'VEF', dataContent: '<span class="flag-icon flag-icon-ve"></span> VEF' },
                { text: 'VND', desc: 'Viet Nam Dong', value: 'VND', dataContent: '<span class="flag-icon flag-icon-vn"></span> VND' },
                { text: 'VUV', desc: 'Vanuatu Vatu', value: 'VUV', dataContent: '<span class="flag-icon flag-icon-vu"></span> VUV' },
                { text: 'WST', desc: 'Samoa Tala', value: 'WST', dataContent: '<span class="flag-icon flag-icon-ws"></span> WST' },
                { text: 'XAF', desc: 'Central Africa CFA fra', value: 'XAF', dataContent: '<span class="flag-icon flag-icon-"></span> XAF' },
                { text: 'XCD', desc: 'East Caribbean Dolla', value: 'XCD', dataContent: '<span class="flag-icon flag-icon-"></span> XCD' },
                { text: 'XOF', desc: 'West African CFA fra', value: 'XOF', dataContent: '<span class="flag-icon flag-icon-cf"></span> XOF' },
                { text: 'XPF', desc: 'CFP Franc', value: 'XPF', dataContent: '<span class="flag-icon flag-icon-pf"></span> XPF' },
                { text: 'YER', desc: 'Yemen Rial', value: 'YER', dataContent: '<span class="flag-icon flag-icon-ye"></span> YER' },
                { text: 'ZAR', desc: 'South Africa Rand', value: 'ZAR', dataContent: '<span class="flag-icon flag-icon-za"></span> ZAR' },
                { text: 'ZMW', desc: 'Zambia Kwacha', value: 'ZMW', dataContent: '<span class="flag-icon flag-icon-zm"></span> ZMW' },
                { text: 'ZWD', desc: 'Zimbabwe Dollar', value: 'ZWD', dataContent: '<span class="flag-icon flag-icon-zw"></span> ZWD' },
                { text: 'ZWL', desc: 'Zimbabwe Dollar', value: 'ZWL', dataContent: '<span class="flag-icon flag-icon-zw"></span> ZWL' },
            ],
            currencyarr: [],
            defaultCurrency: [],
            showcurrncy: false

        }
    },
    mounted: function () {

        var self = this;
        getAgencycode(function (response) {
            self.currencyarr = self.CurrencyOptions.find(x => x.value === self.selectedCurrency);
            let axiosConfig = {
                headers: {
                    'Content-Type': 'application/json'
                }
            };
            var huburl = ServiceUrls.hubConnection.baseUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var AgencyCode = response;
            AgencyCode = AgencyCode.replace("AGY", "");
            var requrl = '/currencyrate/' + AgencyCode;

            axios.get(huburl + portno + requrl, axiosConfig)
                .then((response) => {
                    var currency = response.data;
                    if (currency) {
                        self.defaultCurrency = self.CurrencyOptions.find(x => x.value === currency[0].toCurrencyCode.toUpperCase());
                        self.currencies.push({ text: self.defaultCurrency.text, value: self.defaultCurrency.value, rate: 1, dataContent: self.defaultCurrency.dataContent });
                        if (localStorage.getItem("selectedCurrency") === null) {
                            localStorage.selectedCurrency = currency[0].toCurrencyCode.toUpperCase();
                            localStorage.CurrencyMultiplier = 1;
                        } else if (currency.length == 1 && localStorage.CurrencyMultiplier == 1) {
                            localStorage.selectedCurrency = currency[0].toCurrencyCode.toUpperCase();
                            if (searchResults) {
                                searchResults.selectedCurrency = currency[0].toCurrencyCode.toUpperCase();
                            }
                        }
                        currency.map(function (value, key) {
                            var tempcur = self.CurrencyOptions.find(x => x.value === value.fromCurrencyCode.toUpperCase());
                            self.currencies.push({ text: tempcur.text, value: tempcur.value, rate: value.sellRate, dataContent: tempcur.dataContent });

                        });

                       localStorage.setItem("currencyList",JSON.stringify(self.currencies));
                    }

                })
                .catch((err) => {

                    console.log(err)
                });
        });


    },
    methods: {

        onSelected: function (code, rate) {
            localStorage.selectedCurrency = code;
            localStorage.CurrencyMultiplier = rate;
            this.currencyarr = this.CurrencyOptions.find(x => x.value === code);
            var hName = window.location.pathname.toString().split("/")[1];
            var pagePath = window.location.pathname;
            switch (pagePath) {
                case "/Hotels/hotel-listing.html":
                    Vue_HotelListing.selectedCurrency = code;
                    Vue_HotelListing.CurrencyMultiplier = rate;
                    var slider = document.getElementById('ageSlider');
                    slider.noUiSlider.set([parseFloat(Vue_HotelListing.lowestPrice), parseFloat(Vue_HotelListing.highestPrice)]);
                    slider.noUiSlider.updateOptions({
                        range: {
                            'min': parseFloat(Vue_HotelListing.lowestPrice),
                            'max': parseFloat(Vue_HotelListing.highestPrice)
                        }
                    });
                    slider.noUiSlider.on('update', function (values, handle) {
                        $("#minPricevalue").text(Vue_HotelListing.$n(parseFloat(values[0] / Vue_HotelListing.CurrencyMultiplier), 'currency', Vue_HotelListing.selectedCurrency));
                        $("#MaxPricevalue").text(Vue_HotelListing.$n(parseFloat(values[1] / Vue_HotelListing.CurrencyMultiplier), 'currency', Vue_HotelListing.selectedCurrency));

                        Vue_HotelListing.priceFilter(values[0], values[1]);
                    });
                    break;
                case "/Flights/flight-listing.html":
                    searchResults.selectedCurrency = code;
                    searchResults.CurrencyMultiplier = rate;
                    var slider = document.getElementById('price-slider')
                    if (searchResults.resMinPrice != searchResults.resMaxPrice) {
                        slider.noUiSlider.set([parseFloat(searchResults.resMinPrice), parseFloat(searchResults.resMaxPrice)]);
                        slider.noUiSlider.updateOptions({
                            range: {
                                'min': parseFloat(searchResults.resMinPrice),
                                'max': parseFloat(searchResults.resMaxPrice)
                            }
                        });
                        slider.noUiSlider.on('update', function (values, handle) {
                            $("#minPricevalue").text(searchResults.$n(parseFloat(values[0] / searchResults.CurrencyMultiplier), 'currency', searchResults.selectedCurrency));
                            $("#MaxPricevalue").text(searchResults.$n(parseFloat(values[1] / searchResults.CurrencyMultiplier), 'currency', searchResults.selectedCurrency));

                            searchResults.changedMinPrice = parseFloat(values[0]);
                            searchResults.changedMaxPrice = parseFloat(values[1]);
                        });
                    } else {
                        searchResults.changedMinPrice = searchResults.resMinPrice;
                        searchResults.changedMaxPrice = searchResults.resMaxPrice;
                    }
                    break;
                case "/Hotels/hotel-detail.html":
                    store.state.selectedCurrency = code;
                    store.state.currencyMultiplier = rate;
                    break;
                case "/Flights/flight-confirmation.html":
                    bookinginfo_vue.selectedCurrency = code;
                    bookinginfo_vue.CurrencyMultiplier = rate;
                    break;
                case "/Flights/flight-booking.html":
                    Vue_FlightBook.selectedCurrency = code;
                    Vue_FlightBook.CurrencyMultiplier = rate;
                    Vue_FlightFare.selectedCurrency = code;
                    Vue_FlightFare.CurrencyMultiplier = rate;
                    Vue_FareUpdate.selectedCurrency = code;
                    Vue_FareUpdate.CurrencyMultiplier = rate;
                    break;

                case "/" + hName + "/demo-page/index.html":
                    //holidays a2z
                    holidy.selectedCurrency = code;
                    holidy.CurrencyMultiplier = rate;
                    break;
                case "/" + hName + "/demo-page/packages.html":
                    holidypack.selectedCurrency = code;
                    holidypack.CurrencyMultiplier = rate;
                    break;
                case "/" + hName + "/demo-page/holidaydetails.html":
                    packageView.selectedCurrency = code;
                    packageView.CurrencyMultiplier = rate;
                    break;
                case "/Nirvana/book-now.html":
                    Packageinfo.selectedCurrency = code;
                    Packageinfo.CurrencyMultiplier = rate;
                    break;
                case "/Nirvana/package.html":
                    packagelist.selectedCurrency = code;
                    packagelist.CurrencyMultiplier = rate;
                    break;
                case "/Nirvana/events.html":
                    packagelist.selectedCurrency = code;
                    packagelist.CurrencyMultiplier = rate;
                    break;
                case "/Nirvana/packageInfo.html":
                    packageDetailInfo.selectedCurrency = code;
                    packageDetailInfo.CurrencyMultiplier = rate;
                    break;
                case "/Nirvana/offers.html":
                    packagelist.selectedCurrency = code;
                    packagelist.CurrencyMultiplier = rate;
                    break;
                case "/Nirvana/uae-attractions.html":
                    packagelist.selectedCurrency = code;
                    packagelist.CurrencyMultiplier = rate;
                    break;
                case "/easy2go/":
                    maininstance.selectedCurrency = code;
                    maininstance.CurrencyMultiplier = rate;
                    break;
                case "/easy2go/package.html":
                    packagelist.selectedCurrency = code;
                    packagelist.CurrencyMultiplier = rate;
                    break;
                case "/easy2go/offers.html":
                    packagelist.selectedCurrency = code;
                    packagelist.CurrencyMultiplier = rate;
                    break;
                case "/Biscordint/":
                    maininstance.selectedCurrency = code;
                    maininstance.CurrencyMultiplier = rate;
                    break;
                case "/Biscordint/apply-visa.html":
                    applyvisa.selectedCurrency = code;
                    applyvisa.CurrencyMultiplier = rate;
                    break;
                case "/Biscordint/flight-deals.html":
                    packagelist.selectedCurrency = code;
                    packagelist.CurrencyMultiplier = rate;
                    break;
                case "/Biscordint/flight-deals-details.html":
                    packageView.selectedCurrency = code;
                    packageView.CurrencyMultiplier = rate;
                    break;

                case "/Biscordint/holiday-packages.html":
                    packageList.selectedCurrency = code;
                    packageList.CurrencyMultiplier = rate;
                    break;
                case "/Biscordint/holiday-packagesview.html":
                    Packageinfo.selectedCurrency = code;
                    Packageinfo.CurrencyMultiplier = rate;
                    break;
                case "/Biscordint/apply-visa-form.html":
                    visa.selectedCurrency = code;
                    visa.CurrencyMultiplier = rate;
                    break;
                case "/CompareTicket/index.html":
                    maininstance.selectedCurrency = code;
                    maininstance.CurrencyMultiplier = rate;
                    break;
                case "/CompareTicket/holidays.html":
                    packageList.selectedCurrency = code;
                    packageList.CurrencyMultiplier = rate;
                    break;
                case "/CompareTicket/holiday-detail.html":
                    packageView.selectedCurrency = code;
                    packageView.CurrencyMultiplier = rate;
                    break;
                case "/flyonline/":
                    maininstance.selectedCurrency = code;
                    maininstance.CurrencyMultiplier = rate;
                    break;
                case "/flyonline/index.html":
                    maininstance.selectedCurrency = code;
                    maininstance.CurrencyMultiplier = rate;
                    break;
                case "/flyonline/package-detail.html":
                    packageView.selectedCurrency = code;
                    packageView.CurrencyMultiplier = rate;
                    break;
                case "/flyonline/package.html":
                    packageList.selectedCurrency = code;
                    packageList.CurrencyMultiplier = rate;
                    break;
                case "/Easy2GoV2/index.html":
                    maininstance.selectedCurrency = code;
                    maininstance.CurrencyMultiplier = rate;
                    break;

                case "/Easy2GoV2/":
                    maininstance.selectedCurrency = code;
                    maininstance.CurrencyMultiplier = rate;
                    break;
                case "/Easy2GoV2/package.html":
                    packagelist.selectedCurrency = code;
                    packagelist.CurrencyMultiplier = rate;
                    break;    
                case "/Easy2GoV2/packageview.html":
                    Packageinfo.selectedCurrency = code;
                    Packageinfo.CurrencyMultiplier = rate;
                    break;    
                case "/Easy2GoV2/offers.html":
                    offerelist.selectedCurrency = code;
                    offerelist.CurrencyMultiplier = rate;
                    break;    
                case "/Yone/index.html":
                    maininstance.selectedCurrency = code;
                    maininstance.CurrencyMultiplier = rate;
                    break;
                case "/Yone/":
                    maininstance.selectedCurrency = code;
                    maininstance.CurrencyMultiplier = rate;
                    break;
                case "/Yone/package.html":
                    packagelist.selectedCurrency = code;
                    packagelist.CurrencyMultiplier = rate;
                    break;    
                case "/Yone/package-detail.html":
                    packageView.selectedCurrency = code;
                    packageView.CurrencyMultiplier = rate;
                    break;    
                case "/FlyKeyV2/index.html":
                        maininstance.selectedCurrency = code;
                        maininstance.CurrencyMultiplier = rate;
                        break;
                case "/FlyKeyV2/":
                        maininstance.selectedCurrency = code;
                        maininstance.CurrencyMultiplier = rate;
                        break;
                case "/FlyKeyV2/package.html":
                        packagelist.selectedCurrency = code;
                        packagelist.CurrencyMultiplier = rate;
                        break;    
                case "/FlyKeyV2/packageview.html":
                        Packageinfo.selectedCurrency = code;
                        Packageinfo.CurrencyMultiplier = rate;
                        break;    
                case "/FlyKeyV2/offers.html":
                        offerelist.selectedCurrency = code;
                        offerelist.CurrencyMultiplier = rate;
                        break;      
                case "/TarcoAviation/index.html":
                          maininstance.selectedCurrency = code;
                            maininstance.CurrencyMultiplier = rate;
                            break;
                default:
                    try {
                        packageView.selectedCurrency = code;
                        packageView.CurrencyMultiplier = rate;
                    } catch (error) {
                    }
                    try {
                        HotelList.selectedCurrency = code;
                        HotelList.CurrencyMultiplier = rate;
                    } catch (error) {
                    }
                    break;
                // window.location.reload();
            }
        }
    },
    watch: {
        currencies: function () {
            this.currencies.length == 0 ? this.showcurrncy = false : this.currencies.length == 1 ? this.showcurrncy = false : this.showcurrncy = true;

        }

    }
});


//Common Functions
function getUrlVars() {
    var vars = [],
        hash;
    var newUrl = decodeURIComponent(window.location.href);
    var hashes = newUrl.slice(newUrl.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function login(username, password, callback) {
    var Host = window.location.hostname;
    var AgencyFolderName = localStorage.AgencyFolderName ? localStorage.AgencyFolderName : undefined;
    if (!AgencyFolderName) {
        setAgencyFolderName();
        callback(true);
    } else {
        $.getJSON('/' + AgencyFolderName + '/credentials/AgencyCredentials.json', function (json) {
            let agency = json.Agencies.find(o => o.Domain.toLowerCase() === Host.toLowerCase());
            if (typeof agency !== "undefined") {
                var huburl = ServiceUrls.hubConnection.baseUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var session_url = huburl + portno + '/authenticate/' + agency.AgencyCode;
                var encodedString = btoa(username + ":" + password);
                axios.get(session_url, {
                    headers: { 'Authorization': 'Basic ' + encodedString }
                }).then(function (response) {
                    localStorage.access_token = response.headers.access_token;
                    localStorage.timer = new Date();
                    localStorage.User = JSON.stringify(response.data.user);
                    localStorage.IsLogin = true;
                    if (typeof callback == "function") {
                        callback(true);
                    }

                }).catch(function (error) {
                    callback(false);
                });

            } else {
                console.log('Credentails Not found');
                callback(false);
            }

        });
    }

}

function registerUser(emailId, firstName, lastName, title, callback) {
    if (title != '') {
        if (title == 'Mr') { title = 1; }
        if (title == 'Mrs') { title = 2; }
        if (title == 'Ms') { title = 3; }
        if (title == 'Miss') { title = 4; }
        if (title == 'Master') { title = 5; }
    } else {
        title = 1;
    }
    var postData = {
        emailId: emailId,
        firstName: firstName,
        lastName: lastName,
        titleId: title,
        contactNumber: {
            number: "",
            countryCode: "AE",
            telephoneCode: null
        }
    };
    var Host = window.location.hostname;
    var huburl = ServiceUrls.hubConnection.baseUrl;
    var portno = ServiceUrls.hubConnection.ipAddress;
    var requrl = ServiceUrls.hubConnection.hubServices.registerUser;
    let axiosConfig = {
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'Authorization': "Bearer " + localStorage.access_token
        }
    }

    axios.post(huburl + portno + requrl, postData, axiosConfig)
        .then((res) => {
            localStorage.profileData = '';
            localStorage.access_token = res.headers.access_token;
            localStorage.timer = new Date();
            var uData = localStorage.User;
            // localStorage.User = JSON.stringify(res.data.user);
            /*login(res.data.user.loginId, res.data.user.password, function (response) {

            });*/
            $.getJSON('/Resources/HubUrls/AgencyCredentials.json', function (json) {
                let agency = json.Agencies.find(o => o.Domain.toLowerCase() === Host.toLowerCase());
                if (typeof agency !== "undefined") {
                    var fromEmail = JSON.parse(localStorage.User).loginNode.email;
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    var toEmail = JSON.parse(localStorage.User).emailId;
                    var postDataAdmin = {
                        type: "AdminResponse",
                        fromEmail: fromEmail,
                        toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                        ccEmails: null,
                        bccEmails: null,
                        primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary || "#FFF",
                        secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary || "#FFF",
                        // primaryColor: "#FFF",
                        // secondaryColor: "#FFF",
                        subject: "User Registered",
                        // logoUrl: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                        logoUrl: JSON.parse(localStorage.User).loginNode.logo,
                        agencyName: agency.AgencyFolderName,
                        year: new Date().getFullYear() + '. ' + agency.AgencyFolderName,
                        fullName: "Admin",
                        message: "The following user has registered.",
                        name: firstName + " " + lastName,
                        loginId: emailId,
                        phone: "NA",
                        email: emailId
                    };

                    var postDataRegister = {
                        type: "RegisterRequest",
                        name: firstName,
                        fromEmail: fromEmail,
                        toEmails: Array.isArray(res.data.user.emailId) ? res.data.user.emailId : [res.data.user.emailId],
                        loginId: res.data.user.emailId,
                        password: res.data.user.password,
                        logoUrl: JSON.parse(localStorage.User).loginNode.logo,
                        agencyName: agency.AgencyFolderName,
                        copyRight: new Date().getFullYear() + '. ' + agency.AgencyFolderName
                    };
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:none;background:grey !important;");

                    axios.all([
                        axios.post(emailApi, postDataAdmin, axiosConfig),
                        axios.post(emailApi, postDataRegister, axiosConfig)
                    ])
                        .then(axios.spread(function () {
                            $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:unset");
                            if (window.location.pathname.indexOf("flight-booking.html") == -1 && window.location.href.indexOf("hotelBooking") == -1) {
                                if (typeof callback == "function") {
                                    callback({ isSuccess: true, data: res });
                                } else {
                                    alertify.alert('Success', 'Email Sent Successfully.');
                                }
                            } else {
                                alertify.alert('Success', 'Email Sent Successfully.', function () {
                                    login(res.data.user.emailId, res.data.user.password, callback);
                                });
                            }
                            $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');

                        })).catch((err) => {
                            console.log("AXIOS ERROR: ", err);
                            if (err.code && err.code.message) {
                                alertify.alert('Error', err.code.message);
                            } else {
                                alertify.alert('Error', "There was an error processing your request.");
                            }

                        });
                }
            });

        })
        .catch((err) => {
            try {
                if (err.response.data.message == "The email address you have entered is already registered.") {
                    if (window.location.pathname.indexOf("flight-booking.html") == -1 && window.location.href.indexOf("hotelBooking") == -1) {
                        alert(err.response.data.message);
                    }
                } else {
                    alert(err.response.data.message);
                    console.log("RegisterUser Status: ", err);
                }
            } catch (err) {
                console.log("RegisterUser Status: ", err);
            }
            callback({ isSuccess: false, data: null });
            $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
        })
}

function commonlogin(callback) {
    var Host = window.location.hostname;
    if (!localStorage.AgencyFolderName) {
        setAgencyFolderName();
        // callback();
        // commonlogin();
    } else {
        $.getJSON('/' + localStorage.AgencyFolderName + '/credentials/AgencyCredentials.json', function (json) {
            // $.getJSON('/Resources/HubUrls/AgencyCredentials.json', function (json) {
            let agency = json.Agencies.find(o => o.Domain.toLowerCase() === Host.toLowerCase());
            if (typeof agency !== "undefined") {
                var huburl = ServiceUrls.hubConnection.baseUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var session_url = huburl + portno + '/authenticate/' + agency.AgencyCode;
                var encodedString = btoa(agency.UserName + ":" + agency.Password);
                axios.get(session_url, {
                    headers: { 'Authorization': 'Basic ' + encodedString }
                }).then(function (response) {
                    localStorage.access_token = response.headers.access_token;
                    localStorage.AgencyCode = agency.AgencyCode;
                    localStorage.timer = new Date();
                    localStorage.User = JSON.stringify(response.data.user);
                    localStorage.IsLogin = false;
                    localStorage.roleList = JSON.stringify(response.data.user.roleList);
                    setColorTheme();

                    var hName = window.location.pathname;
                    switch (hName) {

                        case '/my-profile.html':
                        case '/customer-profile.html':
                        case '/co-travelers.html':
                        case '/my-bookings.html':
                        case '/change-password.html':
                        case '/feedback.html':
                        case '/edit-my-profile.html':
                        case '/add-co-travelers.html':
                            window.location.href = "/";
                            break;

                        default:
                            break;
                    }
                    try { callback(); } catch (err) { }
                }).catch(function (error) {
                    console.log('Error on Authentication');
                });

            } else {
                console.log('Credentails Not found');
                window.location.href = "/" + agency.AgencyFolderName;
            }

        });
    }
}

function setColorTheme() {
    var authData = JSON.parse(localStorage.User);
    $('head').append('<style>:root{' +
        '--primary-color:#' + authData.loginNode.lookNfeel.primary + '; --primaryLight-color:' + shadeColor('"' + authData.loginNode.lookNfeel.primary + '"', lightColorValue) + '; --primaryDark-color:' + shadeColor('"' + authData.loginNode.lookNfeel.primary + '"', darkColorValue) + '; --secondary-color:#' + authData.loginNode.lookNfeel.secondary + '; --secondaryLight-color:' + shadeColor('"' + authData.loginNode.lookNfeel.secondary + '"', lightColorValue) + '; --secondaryDark-color:' + shadeColor('"' + authData.loginNode.lookNfeel.secondary + '"', darkColorValue) + '; --tertiary-color:#' + authData.loginNode.lookNfeel.tertiary + '; --tertiaryLight-color:' + shadeColor('"' + authData.loginNode.lookNfeel.tertiary + '"', lightColorValue) + '; --tertiaryDark-color:' + shadeColor('"' + authData.loginNode.lookNfeel.tertiary + '"', darkColorValue) + ';' +
        '}</style>');
}

function shadeColor(color, percent) {
    var R = parseInt(color.substring(1, 3), 16);
    var G = parseInt(color.substring(3, 5), 16);
    var B = parseInt(color.substring(5, 7), 16);

    R = parseInt(R * (100 + percent) / 100);
    G = parseInt(G * (100 + percent) / 100);
    B = parseInt(B * (100 + percent) / 100);

    R = (R < 255) ? R : 255;
    G = (G < 255) ? G : 255;
    B = (B < 255) ? B : 255;

    var RR = ((R.toString(16).length == 1) ? "0" + R.toString(16) : R.toString(16));
    var GG = ((G.toString(16).length == 1) ? "0" + G.toString(16) : G.toString(16));
    var BB = ((B.toString(16).length == 1) ? "0" + B.toString(16) : B.toString(16));

    return "#" + RR + GG + BB;
}

//send Mail Functions
function sendMailService(reqUrl, postData) {

    if (postData != undefined && postData.logo != undefined && localStorage.User != undefined &&
        JSON.parse(localStorage.User).loginNode.logo.includes('.s3.')) {
        postData.logo = JSON.parse(localStorage.User).loginNode.logo;
    }

    axios.post(reqUrl, postData)
        .then((res) => {
            console.log("Mail Status: ", res);
            return true;
        })
        .catch((err) => {
            console.log("Mail Status: ", err);
            return false;
        })
}

function sendMailServiceWithCallBack(reqUrl, postData, callback) {

    if (postData != undefined && postData.logo != undefined && localStorage.User != undefined &&
        JSON.parse(localStorage.User).loginNode.logo.includes('.s3.')) {
        postData.logo = JSON.parse(localStorage.User).loginNode.logo;
    }

    axios.post(reqUrl, postData)
        .then((res) => {
            callback();
            console.log("Mail Status: ", res);
            return true;
        })
        .catch((err) => {
            console.log("Mail Status: ", err);
            return false;
        })
}


function showHideConfirmLoader(show) {
    if (show) {
        $('.loading-overlay').show();
    } else {
        $('.loading-overlay').hide();
    }
}

function getAgencycode(callback) {
    if (localStorage.getItem("AgencyCode") === null) {
        var Host = window.location.hostname;
        $.getJSON('/Resources/HubUrls/AgencyCredentials.json', function (json) {
            let agency = json.Agencies.find(o => o.Domain.toLowerCase() === Host.toLowerCase());
            if (typeof agency !== "undefined") {
                localStorage.AgencyCode = agency.AgencyCode;
                localStorage.AgencyFolderName = agency.AgencyFolderName;
                callback(agency.AgencyCode);
            }
        });

    } else {
        callback(localStorage.AgencyCode);
    }
}

function getAgencyFolderName() {
    var AgencyFolderName = localStorage.AgencyFolderName;
    if (AgencyFolderName == undefined) {
        var Host = window.location.hostname;
        $.getJSON('/Resources/HubUrls/AgencyCredentials.json', function (json) {
            let agency = json.Agencies.find(o => o.Domain.toLowerCase() === Host.toLowerCase());
            if (typeof agency !== "undefined") {
                localStorage.AgencyCode = agency.AgencyCode;
                localStorage.AgencyFolderName = agency.AgencyFolderName;
                loadMasterFiles(agency.AgencyFolderName);
            }
        });
    } else {
        loadMasterFiles(AgencyFolderName);
    }
}
function setAgencyFolderName() {
    var AgencyFolderName = localStorage.AgencyFolderName ? localStorage.AgencyFolderName : undefined;
    if (AgencyFolderName == undefined) {
        var Host = window.location.hostname;
        $.getJSON('/Resources/HubUrls/AgencyCredentials.json', function (jsonCommon) {
            let agencyCommon = jsonCommon.Agencies.find(o => o.Domain.toLowerCase() === Host.toLowerCase());
            if (typeof agencyCommon !== "undefined") {
                // localStorage.AgencyCode = agencyCommon.AgencyCode;
                // localStorage.AgencyFolderName = agencyCommon.AgencyFolderName;
                localStorage.setItem('AgencyCode', agencyCommon.AgencyCode);
                localStorage.setItem('AgencyFolderName', agencyCommon.AgencyFolderName);
            }
        });
    }
}

function loadMasterFiles(aFolder) {
    var masterJsUrl = '/' + aFolder + '/master/js/masterpage.js?v=20220225000';
    var masterCssUrl = '/' + aFolder + '/master/css/master.css?v=20211201000';
    var faviconUrl = '/' + aFolder + '/website-informations/favicon/favi.png';

    loadExternalFile('masterJs', 'script', 'javascript', 'text/javascript', masterJsUrl);
    loadExternalFile('masterCss', 'link', 'stylesheet', 'text/css', masterCssUrl);
    loadExternalFile('favicon', 'link', 'shortcut icon', 'image/x-icon', faviconUrl, 'all');




    // if (aFolder.toLowerCase() == 'nirvana') {
    //     //Google Sign In Start
    //     setTimeout(function () {
    //         loadScriptOnload('https://apis.google.com/js/platform.js', function () {
    //             googleInit();
    //         });
    //         loadScriptOnload('https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3&appId=1470151406709877&autoLogAppEvents=1', function () {

    //         });
    //     }, 3000);
    // }
}

function loadExternalFile(pageId, page, pageRel, pageType, pageUrl) {
    if (!document.getElementById(pageId)) {
        var body = document.getElementsByTagName('body')[0];
        var head = document.getElementsByTagName('head')[0];
        var link = document.createElement(page);
        link.id = pageId;
        link.rel = pageRel;
        link.type = pageType;
        switch (page.toUpperCase()) {
            case 'LINK':
                link.href = pageUrl;
                head.appendChild(link);
                break;
            case 'SCRIPT':
                link.src = pageUrl;
                body.appendChild(link);
                break;
            default:
                break
        }
    }
}

function getVersion() {
    var timeStamp = moment().format('DDMMYYHHmmss');
    return timeStamp;
}

function getQueryStringParameterByName(parameterName, url) {
    if (!url) url = window.location.href;
    parameterName = parameterName.replace(/[\[\]]/g, "\\$&");
    var regularExpression =
        new RegExp("[?&]" + parameterName + "(=([^&#]*)|&|#|$)"),
        results = regularExpression.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function QueryStringToJSON() {
    var pairs = location.search.slice(1).split('&');

    var result = {};
    pairs.forEach(function (pair) {
        pair = pair.split('=');
        result[pair[0]] = decodeURIComponent(pair[1] || '');
    });

    return JSON.parse(JSON.stringify(result));
}

function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
    //compatibility for firefox and chrome
    try {
        var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
        var pc = new myPeerConnection({
            iceServers: []
        }),
            noop = function () { },
            localIPs = {},
            ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
            key;

        function iterateIP(ip) {
            if (!localIPs[ip]) {
                onNewIP(ip);
            } else {
                onNewIP("192.168.1.128");
            }
            localIPs[ip] = true;
        }

        //create a bogus data channel
        pc.createDataChannel("");

        // create offer and set local description
        pc.createOffer(function (sdp) {
            sdp.sdp.split('\n').forEach(function (line) {
                if (line.indexOf('candidate') < 0) return;
                line.match(ipRegex).forEach(iterateIP);
            });

            pc.setLocalDescription(sdp, noop, noop);
        }, noop);

        //listen for candidate events
        pc.onicecandidate = function (ice) {
            if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) {
                if (ice.candidate) iterateIP("192.168.1.128");
            } else {
                ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
            }
        };
    } catch (error) {
        onNewIP("192.168.1.128");
    }
}

var SideMenuComponent = Vue.component('sidemenuitem', {
    template: `<div class="list">
    <ul> 
    <li><a v-bind:class="{ active: myDashBoardisActive }" href="customer-profile.html"><i aria-hidden="true" class="fa fa-th-large"></i> {{My_Dashboard}}</a></li>     
    <li><a v-bind:class="{ active: myProfileisActive }" href="my-profile.html"><i class="fa fa-user" aria-hidden="true"></i> {{My_Profile}}</a></li>
    <li><a v-bind:class="{ active: coTravelerisActive }" href="co-travelers.html"><i class="fa fa-suitcase" aria-hidden="true"></i> {{My_Companion}}</a></li>
    <li><a v-bind:class="{ active: myBookingsisActive }" href="my-bookings.html"><i class="fa fa-file" aria-hidden="true"></i> {{My_Bookings}}</a></li>
    <li><a v-bind:class="{ active: changePwdPwdisActive }" href="change-password.html"><i class="fa fa-lock" aria-hidden="true"></i> {{Change_Password}}</a></li>
    <li><a v-bind:class="{ active: feedbackisActive }" href="feedback.html"><i class="fa fa-comment" aria-hidden="true"></i> {{Feedback}}</a></li>
  </ul>    
    </div>`,
    data() {
        return {
            My_Dashboard: '',
            My_Profile: '',
            My_Companion: '',
            My_Bookings: '',
            Change_Password: '',
            Feedback: '',
            getdata: true,
            myDashBoardisActive: false,
            myProfileisActive: false,
            coTravelerisActive: false,
            myBookingsisActive: false,
            changePwdPwdisActive: false,
            feedbackisActive: false,
            key: 0
        }
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getUserSideMenu: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var changePasswordPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/User SideMenu/User SideMenu/User SideMenu.ftl';
                axios.get(changePasswordPage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (response.data.area_List.length > 0) {
                        var mainComp = response.data.area_List[0].main;
                        self.My_Dashboard = self.pluckcom('My_Dashboard', mainComp.component);
                        self.My_Profile = self.pluckcom('My_Profile', mainComp.component);
                        self.My_Companion = self.pluckcom('My_Companion', mainComp.component);
                        self.My_Bookings = self.pluckcom('My_Bookings', mainComp.component);
                        self.Change_Password = self.pluckcom('Change_Password', mainComp.component);
                        self.Feedback = self.pluckcom('Feedback', mainComp.component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        setLinkActive: function () {
            if (window.location.href.indexOf('customer') > -1) { this.myDashBoardisActive = true; } else if (window.location.href.indexOf('my-profile') > -1) { this.myProfileisActive = true; } else if (window.location.href.indexOf('travel') > -1) { this.coTravelerisActive = true; } else if (window.location.href.indexOf('booking') > -1) { this.myBookingsisActive = true; } else if (window.location.href.indexOf('change') > -1) { this.changePwdPwdisActive = true; } else if (window.location.href.indexOf('feed') > -1) { this.feedbackisActive = true; }
        }
    },
    mounted: function () {
        this.getUserSideMenu();
        this.setLinkActive();
    }
});

//Google
function onSuccess(googleUser) {
    // Useful data for your client-side scripts:
    var profile = googleUser.getBasicProfile();
    console.log("ID: " + profile.getId()); // Don't send this directly to your server!
    console.log('Full Name: ' + profile.getName());
    console.log('Given Name: ' + profile.getGivenName());
    console.log('Family Name: ' + profile.getFamilyName());
    console.log("Image URL: " + profile.getImageUrl());
    console.log("Email: " + profile.getEmail());

    // The ID token you need to pass to your backend:
    var id_token = googleUser.getAuthResponse().id_token;
    console.log("ID Token: " + id_token);
}

function onFailure(error) {
    console.log(error);
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}

function loadScriptOnload(url, callback) {
    var script = document.createElement("script")
    script.type = "text/javascript";
    if (script.readyState) { // only required for IE <9
        script.onreadystatechange = function () {
            if (script.readyState === "loaded" || script.readyState === "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else { //Others
        script.onload = function () {
            callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

//Facebook

function checkLoginState() {
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
}

function statusChangeCallback(response) {
    console.log(response);
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        getFbUserInfo();
    } else {
        // The person is not logged into your app or we are unable to tell.
        console.log('Please log into this app.');
    }
}

function getFbUserInfo() {
    FB.api('/me', { fields: 'name, email, first_name, last_name' }, function (response) {
        console.log(response);
    });
}

function signOutFb() {
    FB.logout(function (response) {
        // Person is now logged out
    });
}
$(document).ready(function () {
    $(".filterview").click(function () {
        $(".filter").toggle();
        $(".sorting").hide();
    });
    $(".sorting_sec").click(function () {
        $(".sorting").toggle();
        $(".filter").hide();
    });
});

 function getAllMapData(contentArry) {
    var tempDataObject = {};
    if (contentArry != undefined) {
        contentArry.map(function (item) {
            let allKeys = Object.keys(item)
            for (let j = 0; j < allKeys.length; j++) {
                let key = allKeys[j];
                let value = item[key];
                if (key != 'name' && key != 'type') {
                    if (value == undefined || value == null) {
                        value = "";
                    }
                    tempDataObject[key] = value;
                }
            }
        });
    }
    return tempDataObject;
}

function pluck(key, contentArry) {
    var Temparry = [];
    contentArry.map(function (item) {
        if (item[key] != undefined) {
            Temparry.push(item[key]);
        }
    });
    return Temparry;
}

function pluckcom(key, contentArry) {
    var Temparry = [];
    contentArry.map(function (item) {
        if (item[key] != undefined) {
            Temparry = item[key];
        }
    });
    return Temparry;
}

function getValidationMsgByCode (code) {
    if (sessionStorage.validationItems !== undefined) {
      var validationList = JSON.parse(sessionStorage.validationItems);
      for (let validationItem of validationList.Validation_List) {
        if (code === validationItem.Code) {
          return validationItem.Message;
        }
      }
    }
}

async function cmsRequestData(callMethod, urlParam, data, headerVal) {
    var huburl = ServiceUrls.hubConnection.cmsUrl;
    var portno = ServiceUrls.hubConnection.ipAddress;
    const url = huburl + portno + "/" + urlParam;
    if (data != null) {
        data = JSON.stringify(data);
    }
    const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: { 'Content-Type': 'application/json' },
        body: data, // body data type must match "Content-Type" header
    });
    try {
        const myJson = await response.json();
        return myJson;
    } catch (error) {
        return object;
    }
}