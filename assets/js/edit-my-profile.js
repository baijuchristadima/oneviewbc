var myprofile_vue = new Vue({
  el: "#div_editmyProfile",
  data: {
    User: {
      "id": "",
      "loginid": "",
      "title": {
        "id": "",
        "name": ""
      },
      "firstName": "",
      "lastName": "",
      "status": "",
      "emailId": "",
      "usercontactNumber": {
        "number": "",
        "countryCode": "",
        "telephoneCode": ""
      },
      "userAdress": {
        "line1": "",
        "line2": "",
        "zip": "",
        "city": ""
      },
      "contactnumber": "",
      "loginNode": {
        "code": "",
        "name": "",
        "currency": "",
        "city": "",
        "country": {
          "code": "",
          "telephonecode": ""
        },
        "url": "",
        "logo": "",
        "homepageurl": "",
        "loginurl": "",
        "address": "",
        "zip": "",
        "email": "",
        "phoneList": [
          {
            "type": "",
            "nodeContactNumber": {
              "number": ""
            },
            "number": ""
          },
          {
            "type": "",
            "nodeContactNumber": {
              "number": ""
            },
            "number": ""
          },
          {
            "type": "",
            "nodeContactNumber": {
              "number": ""
            },
            "number": ""
          }
        ],
        "nodetype": "",
        "solutionId": "",
        "lookNfeel": {
        },
        "tenant": {},
        "site": {
        },
        "servicesList": []
      },
      "roleList": []
    },
    title: {
      selected: '',
      titles: paxTitles
    },
    country: {
      selected: '',
      countries: countryList
    },
    Page_Subtitle: '',
    Username: '',
    Full_name: '',
    Phone: '',
    Email: '',
    Address: '',
    Country: '',
    zip: '',
    save: '',
    edit: '',
    Required_Fields: '',
    Required: '',
    Required1: '',
    Required2: '',
    Required3: '',
    Required4: '',
    Required5: '',
    Required6: '',
    Required7: '',
    Alert_MessageTile: '',
    Alert_FailedTitle: '',
    Alert_FailedMessage: '',
    Success_Message: '',
    OK_BtnLabel: '',
    AirlinesDatas: AirlinesDatas,
    Title: "",
    First_Name: "",
    Last_Name: "",
    Passport_Number: "",
    Nationality: "",
    Date_Of_Birth: "",
    Issue_Country: "",
    Issue_Date: "",
    Expire_Date: "",
    Frequent_Flyer_Program: "",
    Frequent_Flyer_Number: "",
    Frequent_Flyer: "",
    frequentFlyerDetails: {
      firstName: "",
      lastName: "",
      nationality: "",
      paxType: "ADT",
      title: 1,
      passportIssueDate: "",
      passportExpDate: "",
      issuingCountry: "",
      dob: "",
      lead: 1,
      passportNumber: "",
      frequentFlyerList: 
        {
          frequentFlyerCode: null,
          airLine: {
            code: null
          }
      }
    },
    cotravelerDetails: []
  },

  mounted() {
    this.getPagecontent();
    if (localStorage.IsLogin == 'false') {
      window.location.href = "/";
    } else {
      var userData = JSON.parse(localStorage.User);
      this.title.selected = userData.title.id;
      this.User.title = userData.title;
      this.User.firstName = userData.firstName;
      this.User.lastName = userData.lastName;
      this.User.emailId = userData.emailId;
      this.country.selected = userData.loginNode.country.code;

      var vm = this;
      var dateFormat = 'dd/mm/yy';
      var noOfMonths = 1;
      $(".calendar").datepicker({
        numberOfMonths: parseInt(noOfMonths),
        changeMonth: true,
        changeYear: true,
        showButtonPanel: false,
        dateFormat: dateFormat,
        beforeShow: function () {
          vm.getMinDate(this);
          vm.getMaxDate(this);
        }
        /*,onSelect: function (date) {
            var startDate = $(this).datepicker('getDate');
            sDate = startDate.getTime();
        }*/
      });
      if (userData.usercontactNumber != undefined) {
        this.User.usercontactNumber = userData.usercontactNumber;
      } else {
        this.User.usercontactNumber.number = '';
        this.User.usercontactNumber.countryCode = '';
        this.User.usercontactNumber.telephonecode = '';
      }
      if (userData.userAdress != undefined) {
        this.User.userAdress = userData.userAdress;
      } else {
        this.User.userAdress.line1 = '';
        this.User.userAdress.line2 = '';
        this.User.userAdress.zip = '';
        this.User.userAdress.city = '';
      }
      var huburl = ServiceUrls.hubConnection.baseUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var addCoTraveller = ServiceUrls.hubConnection.hubServices.addCoTraveller;
      var session_url = huburl + portno + addCoTraveller;
      axios.get(session_url, {
        headers: { 'Authorization': 'Bearer ' + localStorage.access_token }
      }).then(function (response) {
        vm.cotravelerDetails = response.data;
        var traveler = vm.cotravelerDetails;
        var leadTraveler = traveler.filter(function (e) {
          return e.lead == true;
        });
        if (leadTraveler.length>0) {
          vm.frequentFlyerDetails.nationality = leadTraveler[0].nationality;
          vm.frequentFlyerDetails.nationality = leadTraveler[0].nationality;
          vm.frequentFlyerDetails.issuingCountry = leadTraveler[0].issuingCountry;
          vm.frequentFlyerDetails.passportNumber = leadTraveler[0].passportNumber;
          try {
            vm.frequentFlyerDetails.frequentFlyerList.frequentFlyerCode = leadTraveler[0].frequentFlyerList[0].frequentFlyerCode;
            vm.frequentFlyerDetails.frequentFlyerList.airLine.code = leadTraveler[0].frequentFlyerList[0].airLine.code;
          } catch (error) {
          }
          $("#txtDob").val(moment(leadTraveler[0].dob, 'YYYY-MM-DD').format('DD/MM/YYYY'));
          $("#txtPassportIssue").val(moment(leadTraveler[0].passportIssueDate, 'YYYY-MM-DD').format('DD/MM/YYYY'));
          $("#txtExpireDate").val(moment(leadTraveler[0].passportExpDate, 'YYYY-MM-DD').format('DD/MM/YYYY'));
        }else {
          vm.frequentFlyerDetails.nationality = userData.loginNode.country.code;
          vm.frequentFlyerDetails.issuingCountry = userData.loginNode.country.code;
        }
      }).catch(function (error) {
        console.log(error);
      });

      if (window.location.search =="?edit-profile=true") {
        alertify.alert("Info", "Please input your personal travel information.");
      }
    }
  },
  methods: {
    getTitle: function (title) {
      if (title != '') {
        if (title == 1) { title = 'Mr'; }
        if (title == 2) { title = 'Mrs'; }
        if (title == 3) { title = 'Ms'; }
        if (title == 4) { title = 'Miss'; }
        if (title == 5) { title = 'Master'; }
      }
      return title;
    },
    getCountryName: function (input) {
      if (input != '') {
        var countries = countryList.filter(function (country) { return country.twoLetter.toUpperCase() == input.toUpperCase() });
        { try { return countries[0].name; } catch (e) { return ""; } }
      }
    },
    updateProfile: function () {
      var self = this;
      if (this.validation()) {
        var address2 = '';
        if (this.User.userAdress.line2 != undefined) {
          address2 = this.User.userAdress.line2;
        }
        var datasProfile = {
          title: this.getTitleName(this.title.selected),
          firstName: this.User.firstName,
          lastName: this.User.lastName,
          emailId: this.User.emailId,
          contactNumber: {
            number: this.User.usercontactNumber.number,
            countryCode: this.country.selected
          },
          // cityCode: this.User.userAdress.city,
          cityCode: '1230145',
          addressLine1: this.User.userAdress.line1,
          addressLine2: address2,
          zip: this.User.userAdress.zip
        };

        var traveler = self.cotravelerDetails;
        var leadTraveler = traveler.filter(function(e){
         return e.lead == true;
        });

        var datasCoTraveler = {
          id: leadTraveler.length == 0  ? undefined : leadTraveler[0].id,
          firstName: this.User.firstName,
          lastName: this.User.lastName,
          nationality: this.frequentFlyerDetails.nationality,
          paxType: "ADT",
          title: this.getTitleName(this.title.selected) || 1,
          passportIssueDate: moment($("#txtPassportIssue").val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
          passportExpDate: moment($("#txtExpireDate").val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
          issuingCountry: this.frequentFlyerDetails.issuingCountry,
          dob: moment($("#txtDob").val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
          lead: 1,
          passportNumber: this.frequentFlyerDetails.passportNumber,
          frequentFlyerList: [
            {
              frequentFlyerCode: this.frequentFlyerDetails.frequentFlyerList.frequentFlyerCode,
              airLine: {
                code: this.frequentFlyerDetails.frequentFlyerList.airLine.code
              }
            }
          ]
        };

        let axiosConfig = {
          headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'Authorization': 'Bearer ' + localStorage.access_token
          }
        };
        var huburl = ServiceUrls.hubConnection.baseUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var updateProfile = ServiceUrls.hubConnection.hubServices.updateProfile;
        var addCoTraveller = ServiceUrls.hubConnection.hubServices.addCoTraveller;

        axios.all([
          axios.put(huburl + portno + updateProfile, datasProfile, axiosConfig),
          axios.post(huburl + portno + addCoTraveller, datasCoTraveler, axiosConfig)
        ])
          .then(axios.spread(function (profile, coTraveler) {
            alertify.confirm(self.Alert_MessageTile, self.Success_Message, function () {
              self.updateUserinfo();
            }
              , function () { }).set('label', self.OK_BtnLabel);
          })).catch((err) => {
            console.log("AXIOS ERROR: ", err);
            alertify.alert(self.Alert_FailedTitle, self.Alert_FailedMessage).set('label', self.OK_BtnLabel);

          });
      }

    },
    validation: function () {
      if (this.title.selected == "") {
        alertify.alert(this.Required, this.Required1).set('label', this.OK_BtnLabel);
        return false;
      }
      if (this.User.firstName == "") {
        alertify.alert(this.Required, this.Required2).set('label', this.OK_BtnLabel);
        return false;
      }
      if (this.User.lastName == "") {
        alertify.alert(this.Required, this.Required3).set('label', this.OK_BtnLabel);
        return false;
      }
      if (this.User.usercontactNumber.number == "") {
        alertify.alert(this.Required, this.Required4).set('label', this.OK_BtnLabel);
        return false;
      }
      if (this.User.emailId == "") {
        alertify.alert(this.Required, this.Required5).set('label', this.OK_BtnLabel);
        return false;
      }
      if (this.country.selected == "") {
        alertify.alert(this.Required, this.Required6).set('label', this.OK_BtnLabel);
        return false;
      }
      // if (this.User.userAdress.zip == undefined || this.User.userAdress.zip == "") {
      //     alertify.alert(this.Required, this.Required7).set('label', this.OK_BtnLabel);
      //   return false;
      // }
   
    
      if (isNullorEmptyToBlank($('#txtDob').val()) == '') {
        alertify.alert(this.Required, "Date of birth is required.").set('label', this.OK_BtnLabel);
        toggleClassShow(('#txtDob'), 'border_danger');
         return false;
      }
      if (isNullorEmptyToBlank($('#txtPassport').val()) == '') {
        alertify.alert(this.Required, "Passport number is required.").set('label', this.OK_BtnLabel);
        toggleClassShow(('#txtPassport'), 'border_danger');
         return false;
      }
      var txtPn = $('#txtPassport').val();
      if (txtPn.length > 20) {
        alertify.alert("Alert", "Maximum 20 characters only.").set('label', Vue_FlightBook.Ok);
        toggleClassShow(('#txtPassport'), 'border_danger');
         return false;
      }
      if (isNullorEmptyToBlank($('#txtExpireDate').val()) == '') {
        alertify.alert(this.Required, "Passport expiry date is required.").set('label', this.OK_BtnLabel);
        toggleClassShow(('#txtExpireDate'), 'border_danger');
         return false;
      }
      if (isNullorEmptyToBlank($('#txtPassportIssue').val()) == '') {
        alertify.alert(this.Required, "Passport issue date is required.").set('label', this.OK_BtnLabel);
        toggleClassShow(('#txtPassportIssue'), 'border_danger');
        return false;
      }
      toggleClassHide(('.border_danger'), 'border_danger');
      return true;
    },
    getTitleName: function (title) {
      if (title == 'Mr' || title == 1) { title = 1; }
      if (title == 'Miss' || title == 2) { title = 2; }
      if (title == 'Mrs' || title == 3) { title = 3; }
      if (title == 'Ms' || title == 4) { title = 4; }
      if (title == 'Master' || title == 5) { title = 5; }
      return title;
    },
    updateUserinfo: function () {
      var huburl = ServiceUrls.hubConnection.baseUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var reNewTokenUrl = ServiceUrls.hubConnection.hubServices.reNewToken;
      var session_url = huburl + portno + reNewTokenUrl
      var currenttoken = localStorage.access_token
      axios.get(session_url, {
        headers: { 'Authorization': 'Bearer ' + currenttoken }
      }).then(function (response) {
        localStorage.access_token = response.headers.access_token;
        localStorage.timer = new Date();
        localStorage.User = JSON.stringify(response.data.user);
        window.location.href = "/my-profile.html";
      }).catch(function (error) {
        console.log('Error on Authentication');
      });
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;

      getAgencycode(function (response) {
        var Agencycode = 'AGY435';
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        self.dir = langauage == "ar" ? "rtl" : "ltr";
        var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/My Profile/My Profile/My Profile.ftl';
        axios.get(aboutUsPage, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          self.getdata = false;
          if (response.data.area_List.length > 0) {
            self.Page_Subtitle = self.pluckcom('Page_Subtitle', response.data.area_List[0].main.component);
            self.Username = self.pluckcom('Username', response.data.area_List[0].main.component);
            self.Full_name = self.pluckcom('Full_name', response.data.area_List[0].main.component);
            self.Phone = self.pluckcom('Phone', response.data.area_List[0].main.component);
            self.Email = self.pluckcom('Email', response.data.area_List[0].main.component);
            self.Address = self.pluckcom('Address', response.data.area_List[0].main.component);
            self.Country = self.pluckcom('Country', response.data.area_List[0].main.component);
            self.zip = self.pluckcom('ZIP', response.data.area_List[0].main.component);
            self.edit = self.pluckcom('Edit_Button', response.data.area_List[0].main.component);
            self.save = self.pluckcom('Save_Button', response.data.area_List[0].main.component);
            self.Required_Fields = self.pluckcom('Required_Fields', response.data.area_List[0].main.component);


            self.Title = self.pluckcom('Title', response.data.area_List[0].main.component);
            self.First_Name = self.pluckcom('First_Name', response.data.area_List[0].main.component);
            self.Last_Name = self.pluckcom('Last_Name', response.data.area_List[0].main.component);
            self.Passport_Number = self.pluckcom('Passport_Number', response.data.area_List[0].main.component);
            self.Nationality = self.pluckcom('Nationality', response.data.area_List[0].main.component);
            self.Date_Of_Birth = self.pluckcom('Date_Of_Birth', response.data.area_List[0].main.component);
            self.Issue_Country = self.pluckcom('Issue_Country', response.data.area_List[0].main.component);
            self.Issue_Date = self.pluckcom('Issue_Date', response.data.area_List[0].main.component);
            self.Expire_Date = self.pluckcom('Expire_Date', response.data.area_List[0].main.component);
            self.Frequent_Flyer_Program = self.pluckcom('Frequent_Flyer_Program', response.data.area_List[0].main.component);
            self.Frequent_Flyer_Number = self.pluckcom('Frequent_Flyer_Number', response.data.area_List[0].main.component);
            self.Frequent_Flyer = self.pluckcom('Frequent_Flyer', response.data.area_List[0].main.component);


            self.Required = self.pluckcom('Required', response.data.area_List[0].main.component);
            self.Required1 = self.pluckcom('Title_Validation', response.data.area_List[0].main.component);
            self.Required2 = self.pluckcom('First_Name_Validation', response.data.area_List[0].main.component);
            self.Required3 = self.pluckcom('Last_Name_Validation', response.data.area_List[0].main.component);
            self.Required4 = self.pluckcom('Number_Validation', response.data.area_List[0].main.component);
            self.Required5 = self.pluckcom('Email_Validation', response.data.area_List[0].main.component);
            self.Required6 = self.pluckcom('Country_Validation', response.data.area_List[0].main.component);
            self.Required7 = self.pluckcom('Zip_Validation', response.data.area_List[0].main.component);
            self.Alert_MessageTile = self.pluckcom('Alert_MessageTile', response.data.area_List[0].main.component);
            self.Alert_FailedTitle = self.pluckcom('Alert_FailedTitle', response.data.area_List[0].main.component);
            self.Alert_FailedMessage = self.pluckcom('Alert_FailedMessage', response.data.area_List[0].main.component);
            self.Success_Message = self.pluckcom('Success_Message', response.data.area_List[0].main.component);
            self.OK_BtnLabel = self.pluckcom('OK_BtnLabel', response.data.area_List[0].main.component);
          }
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
      });

    },
    getMaxDate: function (dateObject) {
      var id = $(dateObject).attr("id");
      var endDate = new Date();
      switch (id) {
        case 'txtDob':
          adddays = -1;
          endDate.setDate(endDate.getDate() + adddays);
          addyears = -12;
          endDate.setFullYear(endDate.getFullYear() + addyears);
          $(dateObject).datepicker('option', 'yearRange', "-100:+0");
          $(dateObject).datepicker('option', 'maxDate', new Date(endDate));
          break;
        case 'txtExpireDate':
          break;
        case 'txtPassportIssue':
          adddays = -1;
          endDate.setDate(endDate.getDate() + adddays);
          $(dateObject).datepicker('option', 'maxDate', endDate);
          break;
      }
      return endDate;
    },
    getMinDate: function (dateObject) {
      var id = $(dateObject).attr("id");
      var startDate = new Date();
      switch (id) {
        case 'txtDob':
          addyears = -120;
          startDate.setFullYear(startDate.getFullYear() + addyears);
          break;
        case 'txtExpireDate':
          adddays = 1;
          startDate.setDate(startDate.getDate() + adddays);
          break;
        case 'txtPassportIssue':
          if (isNullorUndefined($("#txtDob").val())) {
            addyears = -100;
            startDate.setFullYear(startDate.getFullYear() + addyears);
          } else {
            startDate = moment($("#txtDob").val(), 'DD/MM/YYYY')._d;
          }
        break;
      }
      $(dateObject).datepicker('option', 'minDate', startDate);
      return startDate;
    },
  }
});

function isNullorEmpty(value) {
  var status = false;
  if (value == null || value == undefined || value == "undefined") { status = true; }
  if (!status && $.trim(value) == '') { status = true; }
  return status;
}

function isNullorEmptyToBlank(value, optval) {
  return isNullorEmpty(value) ? (isNullorEmpty(optval) ? '' : optval) : value;
}

function toggleClassShow(obj, className) {
  // $('html, body').animate({
  //     scrollTop: $(obj).offset().top
  // }, 20);
  $(obj).addClass(className);
}

function isNullorUndefined(value) {
  var status = false;
  if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) { status = true; }
  return status;
}

function toggleClassHide(obj, className) {
  //setTimeout(function () {
  $(obj).removeClass(className);
  //}, 4000);
}