var myprofile_vue = new Vue({
  el: "#div_feedback",
  data: {
    feedback: '',
    User: {
      "title": {
        "id": "",
        "name": ""
      },
      "firstName": "",
      "lastName": "",
      "status": "",
      "emailId": "",
      "usercontactNumber": {
        "number": "",
        "countryCode": "",
        "telephoneCode": ""
      }

    },
    spTitle: '',
    titleDesc: '',
    frstName: '',
    lstName: '',
    email: '',
    mobNo: '',
    fdBck: '',
    btnSnd: '',
    Required_Title:'',
    Err_Title:'',
    Err_Mob_Title:'',
    Succes_Msglable:'',
    Alert_MsgTitle:'',
    Alert_FailedTitle:'',
    Err_TechPrblem:'',
    Ok_BtnLable:''
  },
  mounted() {
    if (localStorage.IsLogin == 'false') {
      window.location.href = "/";
    } else {
      this.getPagecontent();
      var userData = JSON.parse(localStorage.User);
      this.User.firstName = userData.firstName;
      this.User.lastName = userData.lastName;
      this.User.emailId = userData.emailId;
      this.User.title = userData.title;
      if (userData.usercontactNumber != undefined) {
        this.User.usercontactNumber = userData.usercontactNumber;
      } else {
        this.User.usercontactNumber.number = '';
        this.User.usercontactNumber.countryCode = '';
        this.User.usercontactNumber.telephonecode = '';
      }
    }
  },
  methods: {
    getTitle: function (title) {
      if (title != '') {
        if (title == 1) { title = 'Mr'; }
        if (title == 2) { title = 'Mrs'; }
        if (title == 3) { title = 'Ms'; }
        if (title == 4) { title = 'Miss'; }
        if (title == 5) { title = 'Master'; }
      }
      return title;
    },
    sendFeedback: function () {
      if (this.User.usercontactNumber.number == "") {
        alertify.alert(this.Required_Title, this.Err_Mob_Title).set('label', this.Ok_BtnLable); 
        return false;
      }
      if (this.feedback == "") {
        alertify.alert(this.Required_Title, this.Err_Title).set('label', this.Ok_BtnLable); 
        return false;
      }      
      var datas = {
        feedBack: this.feedback,
      };
      let axiosConfig = {
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          'Authorization': 'Bearer ' + localStorage.access_token
        }
      };
      var huburl = ServiceUrls.hubConnection.baseUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var requrl = ServiceUrls.hubConnection.hubServices.feedback;
      axios.post(huburl + portno + requrl, datas, axiosConfig)
        .then((res) => {
          console.log("RESPONSE RECEIVED: ", res);
          alertify.alert(this.Alert_MsgTitle, this.Succes_Msglable).set('label', this.Ok_BtnLable); 
          this.feedback = '';
        })
        .catch((err) => {
          console.log("AXIOS ERROR: ", err);
          alertify.alert(this.Alert_FailedTitle, this.Err_TechPrblem).set('label', this.Ok_BtnLable); 

        })
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = 'AGY435';
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        self.dir = langauage == "ar" ? "rtl" : "ltr";
        var feedbackPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Feedback/Feedback/Feedback.ftl';
        axios.get(feedbackPage, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          self.getdata = false;
          if (response.data.area_List.length > 0) {
            var mainComp = response.data.area_List[0].main;
            self.spTitle = self.pluckcom('PageTitle', mainComp.component);
            self.titleDesc = self.pluckcom('PageSubHeading', mainComp.component);
            self.frstName = self.pluckcom('First_Name', mainComp.component);
            self.lstName = self.pluckcom('Last_Name', mainComp.component);
            self.email = self.pluckcom('E-mail_ID', mainComp.component);
            self.mobNo = self.pluckcom('Mobile_Number', mainComp.component);
            self.fdBck = self.pluckcom('Write_your_feedback', mainComp.component);
            self.Succes_Msglable = self.pluckcom('Succes_Msglable', mainComp.component);
            self.Alert_MsgTitle = self.pluckcom('Alert_MsgTitle', mainComp.component);
            self.Required_Title = self.pluckcom('Required_Title', mainComp.component);
            self.Alert_FailedTitle = self.pluckcom('Alert_FailedTitle', mainComp.component);
            self.Err_Title = self.pluckcom('Err_Title', mainComp.component);
            self.Err_Mob_Title = self.pluckcom('Err_Mob_Title', mainComp.component);
            self.Err_TechPrblem = self.pluckcom('Err_TechPrblem', mainComp.component);
            self.Ok_BtnLable = self.pluckcom('Ok_BtnLable', mainComp.component);
            self.btnSnd = self.pluckcom('Send_Btnlable', mainComp.component);
          }
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
      });

    }   
  }
});