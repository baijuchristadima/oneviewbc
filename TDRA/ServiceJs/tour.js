const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var packageList = new Vue({
    i18n,
    el: '#tours',
    name: 'tours',
    data: {
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        Banner: {
            Banner_Image: ''
        },
        LocationList: [],
        cityName: '',
        ToDate: '',
        Adults: '',
        mainSection:{}
    },
    computed: {
        Uniquelocation() {
            return _.uniqBy(this.LocationList, 'property')
        }
    },
    methods: {
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#to2").datepicker({
                // minDate: "0d",
                // maxDate: "360d",
                minDate: new Date('11/20/2021'),
                maxDate: new Date('12/08/2021'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
            });
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPackage: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var Language = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = commonPath + Agencycode + '/Template/Home Page/Home Page/Home Page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': Language
                    }
                }).then(function (res) {
                    console.log(res);

                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var main = pluck('Index_Section', self.content);
                        self.mainSection = getAllMapData(main[0].component);
                        self.Banner.Banner_Image = self.mainSection.Banner_Image_1920_x_700px;
                        setTimeout(() => {
                            initSelect2();
                        }, 100);
                          
                    }

                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });
                
                var transferUrl = huburl + portno + commonPath + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';
                axios.get(transferUrl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': Language
                    }
                }).then(function (res) {
                    var packageData = res.data;
                    self.Packages = packageData.Values;
                   
                    self.Packages.forEach(function (item, index) {
                        if (item.Location) {
                            self.LocationList.push(item.Location.toUpperCase().trim());
                        }
                    });

                    self.LocationList = _.uniq(_.sortBy(self.LocationList));
                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });

            });
        },
        SearchPackage: function () {
            if (this.cityName == null || this.cityName == "" || this.cityName.trim() == "") {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M011')).set('closable', false);
                return false;
            } else if (this.ToDate == null || this.ToDate == "" || this.ToDate.trim() == "") {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M030')).set('closable', false);
                return false;
            } else if (this.Adults == null || this.Adults == "" || this.Adults.trim() == "") {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M031')).set('closable', false);
                return false;
            } else {
                var searchCriteria =
                    "city=" + this.cityName +
                    "&date=" + this.ToDate +
                    "&adult=" + this.Adults + "&";
                searchCriteria = searchCriteria.split(' ').join('-');
                var uri = "/TDRA/tour-results.html?" + searchCriteria;
                window.location.href = uri;
            }
        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {

                if (type == 'city') {
                    this.cityName = event.target.value;
                } else if (type == 'to2') {
                    this.ToDate = event.target.value;
                } else if (type == 'adult') {
                    this.Adults = event.target.value;
                }
            }
        },

    },
    mounted: function () {
        localStorage.removeItem("backUrl");

        sessionStorage.active_e = 4;
        this.getPackage();
        this.setCalender();
        var vm = this;
        this.$nextTick(function () {
            $('#city').on("change", function (e) {
                vm.dropdownChange(e, 'city')
            });
            $('#to2').on("change", function (e) {
                vm.dropdownChange(e, 'to2')
            });
            $('#adult').on("change", function (e) {
                vm.dropdownChange(e, 'adult')
            });
        })
    },
});
jQuery(document).ready(function ($) {
    initSelect2();
});
function initSelect2() {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
}



