const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var campOverview = new Vue({
  i18n,
  el: '#campoverview',
  name: 'campoverview',
  data: {
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
    CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    LabelSection: {},
    camOverview: {},
    isData: false
  },
  methods: {
    getPagecontent: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        // self.dir = langauage == "ar" ? "rtl" : "ltr";
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Camp Overview/Camp Overview/Camp Overview.ftl';

        axios.get(pageurl, {
          headers: {
            'content-type': 'text/html',
            'Accept': 'text/html',
            'Accept-Language': langauage
          }
        }).then(function (response) {
          if (response.data.area_List.length > 0) {
            var camp = pluck('Camp_Overview', response.data.area_List);
            self.camOverview = getAllMapData(camp[0].component);
            setTimeout(() => {
              $('.fotorama').fotorama();
            }, 100);
          }
          self.stopLoader();

        }).catch(function (error) {
          self.stopLoader();

          console.log('Error:' + error);
        });
      });
    },
    stopLoader: function () {
        $('#preloader').delay(50).fadeOut(250);
    },
  },

  mounted: function () {
    this.getPagecontent();
    sessionStorage.active_e = 10;
  },
})