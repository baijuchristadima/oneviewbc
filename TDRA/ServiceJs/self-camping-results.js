const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var tentList = new Vue({
    i18n,
    el: '#selfcamping',
    name: 'selfcamping',
    data: {
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        Hotels: [],
        campFull: [],
        filteredCamp: [],


        cityName: 'ABU DHABI',
        fromDate: '',
        toDate: '',
        roomCount: 0,
        rooms: [],
        nights: 0,
        set: null,
        occupancyId: '',
        locationCamp: [],
        OccupancyHotels: [],
        pageLoad: true,
        showOccupancy: false,
        labelsSection:{},
        bannerSection:{},
        Banner:{
            Banner_Image:''
        },
        labels:{},
        selectedCampPrice:[],
        grandTotalPrice:0,
        LocationList: [],
    },
    methods: {
        addRoom: function () {
            var self = this;
            if (self.roomCount < 5) {
                self.roomCount++;
                let type = _.first(self.OccupancyHotels).Occupancy_ID;
                self.rooms.push(Number(type));
            }
        },
        removeRoom: function () {
            var self = this;
            if (self.roomCount > 1) {
                self.rooms = self.rooms.slice(0, self.rooms.length - 1);
                self.roomCount--;
            }
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getOccupancy: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = commonPath + Agencycode + '/Template/Tent Configuration/Tent Configuration/Tent Configuration.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (res) {
                    if (res.data.area_List.length) {
                        var content = res.data.area_List;
                        var val = self.pluck('Configurations', content);
                        var tempOccupancyHotels = self.pluckcom('Room_Occupancy_List', val[0].component);
                        self.OccupancyHotels = tempOccupancyHotels.splice(0,2)
                        self.cityName = "ABU DHABI";
                    }

                }).catch(function (error) {
                    console.log('Error', error);
                });
            });
        },
        noOfnightsFind: function (date1, date2) {
            if (date1 && date2) {
                var start = moment(date1, "DD/MM/YYYY");
                var end = moment(date2, "DD/MM/YYYY");
                var nights = end.diff(start, 'days')
                return nights;
            }
        },
        getRoomPrice: function(price, TID, RID) {
            var self = this;
            var newPrice = price;
            if (TID != "SC3" && self.nights > 1) {
                newPrice = parseFloat(price) + parseFloat((parseInt(self.nights) - 1)*parseInt(RID.replace( /^\D+/g, '')));
            }
            // return self.nights * price;
            return newPrice;
        },
        getPackage: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var campurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/List Of Self Camping/List Of Self Camping/List Of Self Camping.ftl';
                axios.get(campurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    var campData = response.data;
                    try {
                        self.cityName = getQueryStringValue('city').trim();
                        self.fromDate = getQueryStringValue('cin').trim();
                        self.toDate = getQueryStringValue('cout').trim();
                        self.roomCount = parseInt(getQueryStringValue('rooms').trim());
                        self.rooms = JSON.parse(getQueryStringValue('roomtype'));
                        self.nights = self.noOfnightsFind(self.fromDate, self.toDate);
                        self.set = getQueryStringValue('set').trim();

                        if (campData != undefined && campData.Values != undefined) {
                            self.campFull = campData.Values.filter(function(e){return e.ID == self.set});
                            self.campFull.forEach(function (item, index) {
                                self.locationCamp.length=0;
                                self.locationCamp.push(item.Location.toUpperCase().trim());
                            });
                            self.locationCamp = _.uniq(_.sortBy(self.locationCamp));
                            var cond = self.rooms;
                            var availableTentList = [];
                            for(let userRw of cond){
                                let campTypeList =[];
                                self.campFull.forEach(function (campItem) {
                                    campItem.Tent_Type_List.forEach(function (tType) {
                                        let tentType = {
                                            name:tType.Type_Name,
                                            tentList : []
                                        };

                                        for(let tRow of tType.Room_Details){

                                            if(Number(tRow.Occupancy_ID) == userRw){
                                                var price = self.getRoomPrice(tRow.Price, campItem.ID, tRow.ID);
                                                Object.assign(tRow, {totalPrice: price, tId: tType.ID});
                                                tentType.tentList.push(tRow)
                                            }
                                        }
                                        if(tentType.tentList.length>0){
                                            campTypeList.push(tentType);
                                        }
                                    });
                                     
                                });
                                availableTentList.push(campTypeList);
                            }
                            self.filteredCamp = availableTentList;
                            for(let index in availableTentList){
                                self.selectedCampPrice.push(availableTentList[index][0].tentList[0]);
                            }
                            self.pageLoad = false;
                        }
                        $("#from1").val(self.fromDate);
                        $("#from2").val(self.toDate);
                    } catch (error2) {
                        console.log('Error', error2);
                        self.pageLoad = false;
                    }
                }).catch(function (error3) {
                    console.log('Error', error3);
                    self.pageLoad = false;
                });
            });
        },
        getDbDataTableValue: async function (extraFilter, sortField) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/cms/data/search/byQuery";
            let agencyCode = JSON.parse(localStorage.User).loginNode.code;
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            if (requestObject != null) {
                requestObject = JSON.stringify(requestObject);
            }
            try {
                let allDBData = await axios({
                    url: url,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    data: requestObject
                })
                return allDBData;
            } catch (error) {
                console.log(error);
                return error.response.data.code;
            }
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            let decodeUrl = decodeURIComponent(window.location.href);
            var hashes = decodeUrl.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        getmoreinfo(tent, index) {
                var self = this;
                try {
                    var url = tent[0].More_details_page_link;
                    // var park = _.first(hotel.Room_Details).Including_Park_Access;
                    // if (hotel.Including_Park_Access) {
                    //     park = hotel.Room_Details[index != undefined ? index : hotel.Room_Details.length - 1].Including_Park_Access
                    // }
                    var url_string = window.location.href
                    var url1 = new URL(url_string);
                    var c = url1.searchParams.get("set");
                    var urlParam =
                        "&city=" + self.cityName +
                        "&cin=" + self.fromDate +
                        "&cout=" + self.toDate +
                        "&rooms=" + self.roomCount +
                        "&nights=" + self.nights +
                        "&grandTotal="+self.grandTotalPrice+
                        "&roomTypes=" + JSON.stringify(self.rooms)+
                        "&set=" + c +
                        "&selectedTents="+JSON.stringify(self.selectedCampPrice);
                    if (url != null) {
                        if (url != "") {
                            url = url.split("/Template/")[1];
                            url = url.split(' ').join('-');
                            url = url.split('.').slice(0, -1).join('.')
                            url = "/TDRA/self-camping-details.html?page=" + url + urlParam;
                            window.location.href = url;
                        } else {
                            url = "#";
                        }
                    } else {
                        url = "#";
                    }
                    return url;
                } catch (error) {
                    console.log(error);
                }
        },
        dateChecker: function () {
            let dateObj1 = document.getElementById("from1");
            let dateValue1 = dateObj1.value;
            let dateObj2 = document.getElementById("from2");
            let dateValue2 = dateObj2.value;
            if (dateValue2 !== undefined || dateValue2 !== '') {
                // document.getElementById("to1").value = '';
                $('#from2').val('').trigger('change');
                $("#from2").val("");
                return false;
            }
        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                if (type == 'city') {
                    this.cityName = event.target.value;
                } else if (type == 'from1') {
                    this.fromDate = event.target.value;
                } else if (type == 'from2') {
                    this.toDate = event.target.value;
                    this.nights = this.noOfnightsFind(this.fromDate, this.toDate);
                }
                // else if (type == 'occupancy') {
                //     this.occupancyId = event.target.value;
                // }
            }
        },
        modifySearch: function () {
            var self = this;
            
            if (self.cityName == undefined || self.cityName == '' || self.cityName == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M011')).set('closable', false);
                return ;
            } else if (self.fromDate == undefined || self.fromDate == '' || self.fromDate == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M006')).set('closable', false);
                return ;
            } else if (self.toDate == undefined || self.toDate == '' || self.toDate == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M007')).set('closable', false);
                return ;
            }
            //  else if (self.occupancyId == undefined || self.occupancyId == '' || self.occupancyId == null) {
            //     alertify.alert('Alert', 'Please select Occupancy.').set('closable', false);
            //     return false;
            // }
            else {
                var url_string = window.location.href
                var url = new URL(url_string);
                var c = url.searchParams.get("set");
                self.pageLoad = true;
                var searchCriteria =
                "&city=" + self.cityName +
                "&cin=" + self.fromDate +
                "&cout=" + self.toDate +
                "&rooms=" + self.roomCount +
                "&set=" + c +
                "&roomtype=" + JSON.stringify(self.rooms);
                // searchCriteria = searchCriteria.split(' ').join('-');
                var uri = "/TDRA/self-camping-result.html?" + searchCriteria;
                window.location.href = uri;
                // try {
                //     var filter = [];
                //     self.rooms.forEach(element => {
                //         filter.push({
                //             "Occupancy_ID": element,
                //         })
                //     });

                //     var uniqFilter = _.uniqWith(filter, _.isEqual);
                //     var cond = _.map(uniqFilter, filter1 => filter1.Occupancy_ID);

                //     var hotelList = [];
                //     self.hotelFull.forEach(function (hotel) {
                //         if (hotel.Status && hotel.Location.toLowerCase().trim() == self.cityName.toLowerCase()) {
                //             let {
                //                 Room_Details,
                //                 ...hotelWithoutRoomsDetails
                //             } = hotel;
                //             var hotelSelected = _.filter((hotel.Room_Details), room1 => _.indexOf(cond, Number(room1.Occupancy_ID)) !== -1);
                //             if (hotelSelected.length) {
                //                 var hotelWithOut = hotelSelected.filter((element) => (!element.Including_Park_Access));
                //                 var hotelWith = hotelSelected.filter((element) => (element.Including_Park_Access));
                //                 if (hotelWithOut.length) {
                //                     var priceList = [];
                //                     self.rooms.forEach(element => {
                //                         hotelWithOut.forEach(element2 => {
                //                             if (element == element2.Occupancy_ID) {
                //                                 priceList.push(element2.Price)
                //                             }
                //                         });
                //                     });
                //                     var price = _.reduce(priceList, function (sum, n) {
                //                         return sum + Number(n);
                //                     }, 0);
                //                     price = (self.nights * price);
                //                     var Including_Park_Access = false;
                //                     hotelList.push({
                //                         ...hotelWithoutRoomsDetails,
                //                         price,
                //                         Including_Park_Access,
                //                         ...{
                //                             'Room_Details': hotelWithOut
                //                         }
                //                     });
                //                 }
                //                 if (hotelWith.length) {
                //                     var priceList2 = [];
                //                     self.rooms.forEach(element => {
                //                         hotelWith.forEach(element2 => {
                //                             if (element == element2.Occupancy_ID) {
                //                                 priceList2.push(element2.Price)
                //                             }
                //                         });
                //                     });

                //                     var price = _.reduce(priceList2, function (sum, n) {
                //                         return sum + Number(n);
                //                     }, 0);
                //                     var Including_Park_Access = true;
                //                     price = (self.nights * price);
                //                     hotelList.push({
                //                         ...hotelWithoutRoomsDetails,
                //                         price,
                //                         Including_Park_Access,
                //                         ...{
                //                             'Room_Details': hotelWith
                //                         },
                //                     });
                //                 }
                //             }
                //         }
                //     });

                //     var filteredHotels = _.sortBy(hotelList, [function (o) {
                //         return o.price;
                //     }]);
                //     filteredHotelsWithPrice = filteredHotels.filter(hotel => (Number(hotel.Minimum_Number_of_Nights) != NaN && self.nights >= Number(hotel.Minimum_Number_of_Nights)));

                //     var hits = 0;
                //     var bookings = [];
                //     var hotelsWithAvailableRooms = [];
                //     var filterValue = "type='Hotel Rooms'";
                //     self.getDbDataTableValue(filterValue, "ingest_timestamp").then(function (response) {
                //         if (response != undefined && response.data != undefined && response.status == 200) {
                //             hits = response.data.count;
                //             bookings = response.data.data;
                //             if (hits > 0) {
                //                 filteredHotelsWithPrice.forEach(hotel => {
                //                     var available = bookings.find(ele => (hotel.Hotel_ID.toLowerCase() == ele.keyword1.toLowerCase()));
                //                     if (available) {
                //                         var availableRooms = Number(hotel.Total_Rooms) - Number(available.number1)
                //                         if (Number(availableRooms) >= Number(self.roomCount)) {
                //                             hotelsWithAvailableRooms.push(hotel)
                //                         }
                //                     } else {
                //                         hotelsWithAvailableRooms.push(hotel)
                //                     }
                //                 });
                //                 self.Hotels = hotelsWithAvailableRooms;
                //                 self.pageLoad = false;
                //             }
                //             // else {
                //             //     self.Hotels = filteredHotelsWithPrice;
                //             // }
                //         } else if (response == 404) {
                //             self.Hotels = filteredHotelsWithPrice;
                //             self.pageLoad = false;
                //         }
                //     }).catch(function (error) {
                //         console.log(error);
                //         self.pageLoad = false;
                //     });

                // } catch (error) {
                //     console.log('Error', error);
                //     self.pageLoad = false;
                // }
            }
        },
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#from1").datepicker({
                // minDate: "0d",
                // maxDate: "360d",
                minDate: new Date('12/02/2021'),
                maxDate: new Date('12/05/2021'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
            });

            $("#from2").datepicker({
                minDate: new Date('12/02/2021'),
                maxDate: new Date('12/05/2021'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
                beforeShow: function (event, ui) {
                    var url_string = window.location.href
                    var url = new URL(url_string);
                    var c = url.searchParams.get("set");
                    var selectedDate = $('#from1').datepicker('getDate', '+1d');
                    // selectedDate.setDate(selectedDate.getDate()  + ((c == "SC3" || c == "SC2") ? 1 : 2));
                    selectedDate.setDate(selectedDate.getDate()  + 1);
                    $("#from2").datepicker("option", "minDate", selectedDate);
                    // if (c != 'SC3' && c != 'SC2') {
                    //     var maxDateTemp = $('#from1').datepicker('getDate', '+1d');
                    //     maxDateTemp.setDate(maxDateTemp.getDate() + 7);
                    //     if(maxDateTemp > new Date('12/05/2021')){
                    //         $("#from2").datepicker("option", "maxDate", new Date('12/05/2021'));
                    //     } else {
                    //         $("#from2").datepicker("option", "maxDate", maxDateTemp);
                    //     }
                    // }
                }
            });
        },
        getLabels: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var Language = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var hotelcms = huburl + portno + commonPath + Agencycode + '/Template/Self Camping Result/Self Camping Result/Self Camping Result.ftl';
                var labelsurl = huburl + portno + commonPath + Agencycode + '/Template/Home Page/Home Page/Home Page.ftl';
                // var cmsurl = huburl + portno + hotelcms;
                axios.get(hotelcms, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': Language
                    }
                }).then(function (res) {
                    console.log(res);

                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var banner = pluck('Banner_Section', self.content);
                        self.bannerSection = getAllMapData(banner[0].component);
                        var main = pluck('Main_Section', self.content);
                        self.labelsSection = getAllMapData(main[0].component);
                    }
                    self.getOccupancy();
                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });
                axios.get(labelsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': Language
                    }
                }).then(function (res) {
                    console.log(res);
                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var main = pluck('Index_Section', self.content);
                        self.labels = getAllMapData(main[0].component);
                        self.getRoomData();
                    }

                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });

                var transferUrl = huburl + portno + commonPath + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';
                axios.get(transferUrl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': Language
                    }
                }).then(function (res) {
                    var packageData = res.data;
                    self.Packages = packageData.Values;
                    self.Packages.forEach(function (item, index) {
                        if (item.Location) {
                            self.LocationList.push(item.Location.toUpperCase().trim());
                        }
                    });
                    self.LocationList = _.uniq(_.sortBy(self.LocationList));
                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });

            });
        },
        getRoomData:function(){

            var self = this;
            var occupancySummary = 0;
            for (var index = 0; index < self.rooms.length; index++) {
                if (self.rooms[index] == 3) {
                    occupancySummary += 3;
                }
                else {
                    occupancySummary += self.rooms[index];
                }
                
            }
            self.occupancySummary = occupancySummary + (occupancySummary > 1 ? " " + this.labels.Persons_Label : " " + this.labels.Person_Label);

        },
        // getWeather: async function () {
        //     var weatCity = this.city;
        //     axios.get('https://api.openweathermap.org/data/2.5/weather?lat=' + JSON.parse(window.sessionStorage.getItem("cityLocation")).lat + '&lon=' + JSON.parse(window.sessionStorage.getItem("cityLocation")).lon + '&units=metric&appid=7c0ff0999be50513159abbf786d93a0a', {
        //         headers: {
        //             'content-type': 'application/json'
        //         }
        //     }).then(function (response) {
        //         console.log(response);
        //         callback(response);
        //     });
        // },
    },
    computed: {
        noOfnights() {
            try {
                return this.nights > 1 ? this.nights + ' '+ this.labels.Nights_Label : this.nights == 1 ? '1 ' + this.labels.Night_Label: this.labels.Number_Of_Nights_Label || this.labels.Number_Of_Nights_Label;
                // }
            } catch (error) {
                return this.labels.Number_Of_Nights_Label
            }
        }
    },
    mounted: function () {
        // var historyPage = localStorage.getItem("backUrl") ? localStorage.getItem("backUrl") : null;
        // if (historyPage) {
        //     localStorage.removeItem("backUrl");
        //     window.location.href = historyPage;
        // }
        this.getLabels();
        this.getOccupancy();
        this.getPackage();
        this.setCalender();
        sessionStorage.active_e = 2;
        var vm = this;
        this.$nextTick(function () {
            $('#city').on("change", function (e) {
                vm.dropdownChange(e, 'city');
            });
            $('#from1').on("change", function (e) {
                vm.dropdownChange(e, 'from1')
            });
            $('#from2').on("change", function (e) {
                vm.dropdownChange(e, 'from2')
            });
            // $('#occupancy').on("change", function (e) {
            //     vm.dropdownChange(e, 'occupancy')
            // });
        })
    },
    watch: {
        rooms: function () {
            this.getRoomData();
            
        },
        selectedCampPrice:function(){
            let totalPrice = 0;
            this.selectedCampPrice.forEach(function (row) {
            totalPrice += parseFloat(row.totalPrice);
            });
            this.grandTotalPrice = totalPrice;
        }
        
    }
});

jQuery(document).ready(function ($) {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}