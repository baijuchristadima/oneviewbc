
var HeaderComponent = Vue.component('headeritem', {
    template: `<div><div  v-if="getdata"></div>
    <div class="ar_direction" v-else v-for="item in pluck('Header',content.area_List)">
    <section class="top-bar">
        <div class="container">
            <div class="social-media-top">
                <ul>
                    <li><a :href="Branch.Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a :href="Branch.Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a :href="Branch.Linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    <li><a :href="Branch.Instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>

            <div class="top-right">
            
                <div class="sl-nav">
               <currency-select></currency-select>          
                     <lang-select @languagechange="getpagecontent"></lang-select>
                </div>
                <div class="retrive_booking_sec" data-toggle="modal" data-target="#myModal">{{pluckcom('Retrieve_booking_label',item.component)}}</div>
                <ul id="sign-bt-area" class="login-reg  js-signin-modal-trigger"  v-show="!userlogined">
                    <li><a href="javascript:void(0);"  data-signin="login" class="cd-main-nav__item cd-main-nav__item--signin"><i class="fa fa-lock"></i> {{pluckcom('Sign_in_lable',item.component)}}</a></li>
                    <!--li><a href="javascript:void(0);" data-signin="signup" data-type="signup"><i class="fa  fa-user-plus"></i> Register</a></li-->
                </ul>
                <!--iflogin-->
        <label id="bind-login-info" for="profile2" class="profile-dropdown" v-show="userlogined">
            <input type="checkbox" id="profile2">
            <img v-if="[2,3,4].indexOf(userinfo.title?userinfo.title.id:1)<0" src="/assets/images/user.png">
            <img v-else src="/assets/images/user_lady.png">
            <span>{{userinfo.firstName+' '+userinfo.lastName }}</span>
            <label for="profile2"><i class="fa fa-angle-down" aria-hidden="true"></i></label>
            <ul>
                <li><a href="/customer-profile.html"><i class="fa fa-th-large" aria-hidden="true"></i>{{pluckcom('My_dashboard_label',item.component)}}</a></li>
                <li><a href="/my-profile.html"><i class="fa fa-user" aria-hidden="true"></i>{{pluckcom('My_profile_label',item.component)}}</a></li>
                <li><a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i>{{pluckcom('My_booking_label',item.component)}}</a></li>
                <li><a href="#" v-on:click.prevent="logout"><i class="fa fa-power-off" aria-hidden="true"></i>{{pluckcom('Logout_label',item.component)}}</a></li>
            </ul>
        </label>
        <!--ifnotlogin-->
                <!--view booking popup-->
        <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog  modal-smsp">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">View Bookings</h4>
            </div>
            <div class="modal-body">
            <div class="user_login">
            <div>
            <p><label>Enter your email address</label></p>
            <div class="validation_sec">
            <input v-model="bEmail" type="text" id="txtretrivebooking" name="text" placeholder="name@example.com" class=""> 
            <span v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveEmailErormsg }"  class="cd-signin-modal__error sp_validation">Email Required!</span>
            </div> 
            <p></p> 
            <p><label>Enter your ID number</label></p>
            <div class="validation_sec">
            <input v-model="bRef" type="text" id="text" name="text" placeholder="Example: AGY509-8509" class=""> 
            <span  v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveBkngRefErormsg }"   class="cd-signin-modal__error sp_validation">Booking Reference Id Required!</span>
            </div> 
            <p></p>
            <div class="viewradio_sec">
            <label class="radio_view">Flight
                <input v-model="bService" value="F" type="radio" checked="checked" name="radio">
                <span class="checkmark"></span>
            </label>
            <label class="radio_view">Hotel
                <input v-model="bService" value="H" type="radio" name="radio">
                <span class="checkmark"></span>
            </label>
            </div> 
            <button v-on:click="retrieveBooking" type="submit" id="retrieve-booking" class="btn-blue">Continue</button></div></div>
            </div>            
          </div>
          
        </div>
      </div>
            </div>

        </div>
        <!--popup area-->
        <div class="cd-signin-modal js-signin-modal"> <!-- this is the entire modal form, including the background -->
		<div class="cd-signin-modal__container"> <!-- this is the container wrapper -->
			<ul class="cd-signin-modal__switcher js-signin-modal-switcher js-signin-modal-trigger">
				<li><a href="#0" data-signin="login" data-type="login">{{pluckcom('Sign_in_lable',item.component)}}</a></li>
				<li><a href="#0" data-signin="signup" data-type="signup">{{pluckcom('Register_label',item.component)}}</a></li>
			</ul>

			<div class="cd-signin-modal__block js-signin-modal-block" data-type="login"> <!-- log in form -->
                <div class="cd-signin-modal__form">
                <div class="connect">
                <a href="#"><img src="https://ovit-fileupload.s3.amazonaws.com/B2B/AdminPanel/CMS/AGY435/Images/24042019052933.jpg" alt=""></a>
                <a href="#"><img src="https://ovit-fileupload.s3.amazonaws.com/B2B/AdminPanel/CMS/AGY435/Images/24042019052949.jpg" alt=""></a>
                </div>
					<p class="cd-signin-modal__fieldset">
                        <label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" 
                        for="signin-email">{{pluckcom('signinemail_label',item.component)}}</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signin-email" type="email" :placeholder="pluckcom('signinemail_label',item.component)" v-model="username" >
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': usererrormsg.empty||usererrormsg.invalid }">{{usererrormsg.empty?'Please enter email!':(usererrormsg.invalid?'Email seems incorrect!':'')}}</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <label class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace" 
                        for="signin-password">{{pluckcom('signinpassword_label',item.component)}}</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signin-password" type="password"  :placeholder="pluckcom('signinpassword_label',item.component)"  v-model="password" >
						<a v-on:click="showhidepassword" class="cd-signin-modal__hide-password js-hide-password changeShowTxtCls">{{pluckcom('showpassword_label',item.component)}}</a>
                        <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': psserrormsg }">
                        Please enter password!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width" type="submit"
                        v-on:click="loginaction" :value="pluckcom('loginbutton_label',item.component)">
                    </p>
                    <div id="myGoogleButton"></div>
                    <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false"
        data-use-continue-as="false" onlogin="checkLoginState();"></div>
                    <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="reset">{{pluckcom('forgotpassword_label',item.component)}}</a></p>
				</div>
				
				
			</div> <!-- cd-signin-modal__block -->

			<div class="cd-signin-modal__block js-signin-modal-block" data-type="signup"> <!-- sign up form -->
                <div class="cd-signin-modal__form">
                <div class="connect">
                <a href="#"><img src="https://ovit-fileupload.s3.amazonaws.com/B2B/AdminPanel/CMS/AGY435/Images/24042019052933.jpg" alt=""></a>
                <a href="#"><img src="https://ovit-fileupload.s3.amazonaws.com/B2B/AdminPanel/CMS/AGY435/Images/24042019052949.jpg" alt=""></a>
                </div>
                    <p class="cd-signin-modal__fieldset">
                        <label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">Title</label>                        
                        <select v-model="registerUserData.title" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border down_select" id="signup-title">
                            <option selected>Mr</option>
                            <option selected>Ms</option>
                            <option>Mrs</option>
                        </select>
                        <span class="cd-signin-modal__error">Title seems incorrect!</span>
                    </p>
					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">First Name</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signup-firstname" type="text" :placeholder="pluckcom('signupfirstname_label',item.component)" v-model="registerUserData.firstName">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userFirstNameErormsg }" >Plese enter first name!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">Last Name</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signup-lastname" type="text" :placeholder="pluckcom('signuplastname_label',item.component)" v-model="registerUserData.lastName">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userLastNameErrormsg }">Plese enter last name!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" for="signup-email">Email</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signup-email" type="email" :placeholder="pluckcom('signupemail_label',item.component)" v-model="registerUserData.emailId">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userEmailErormsg.empty||userEmailErormsg.invalid }">{{userEmailErormsg.empty?'Please enter email!':(userEmailErormsg.invalid?'Email seems incorrect!':'')}}</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding" type="submit" 
                        :value="pluckcom('createaccountbutton_label',item.component)" v-on:click="registerUser">
                    </p>
                    <div id="myGoogleButtonReg"></div>
                    <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false"
        data-use-continue-as="false" onlogin="checkLoginState();"></div>                    
				</div>
			</div> <!-- cd-signin-modal__block -->

			<div class="cd-signin-modal__block js-signin-modal-block" data-type="reset"> <!-- reset password form -->
				<p class="cd-signin-modal__message">{{pluckcom('paswordlostmessage_label',item.component)}}</p>

				<div class="cd-signin-modal__form">
					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" for="reset-email">E-mail</label>
						<input v-model="emailId" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" id="reset-email" type="email" :placeholder="pluckcom('forgotpassemail_label',item.component)">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userforgotErrormsg.empty||userforgotErrormsg.invalid }">{{userforgotErrormsg.empty?'Please enter email!':(userforgotErrormsg.invalid?'Email seems incorrect!':'')}}</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding" type="submit" 
                        :value="pluckcom('resetpassbutton_label',item.component)" v-on:click="forgotPassword">
                    </p>
                    <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="login">{{pluckcom('backtologinlink_label',item.component)}}</a></p>
				</div>

				
			</div> <!-- cd-signin-modal__block -->
			<a href="#0" class="cd-signin-modal__close js-close">Close</a>
		</div> <!-- cd-signin-modal__container -->
	</div> 
        <!--popup area close-->
    </section>
    <div class="container">
        <div class="row flx_sec">
           <div class="col-md-12 col-xs-12">
                <!--<div class="top-email">
                    <ul>
                        <li><i class="fa fa-phone"></i> <a :href="'tel:'+Branch.Phone">{{Branch.Phone}}</a></li>
                        <li><i class="fa fa-envelope"></i> <a :href="'mailto:'+Branch.Email">{{Branch.Email}}</a></li>
                    </ul>
                </div>-->
                <div class="menu-nav">
                <nav class="navbar navbar-default" id="nav_bar">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-brand" v-for="logo in pluckcom('Logo',item.component)" href="/"><img :src="logo.Image" alt="logo"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-center">
                        <li><a href="/">{{Pagelink.home}}</a></li>
                        <li><a href="/easy2go/package.html">{{Pagelink.package}}</a></li>
                        <li><a href="/easy2go/offers.html">{{Pagelink.offer}}</a></li>
                        <li><a href="/easy2go/contactus.html">{{Pagelink.contact}}</a></li>
                    </ul>
                </div>

            </nav>
            </div>

            </div>
        </div>
    </div>
    <!--<section class="menu-nav">
        <div class="container">
           
        </div>
    </section>-->
</div>
    </div>
</div>`,
    data() {
        return {
            username: '',
            password: '',
            emailId: '',
            retrieveEmailId: '',
            retrieveBookRefid: '',
            usererrormsg: { empty: false, invalid: false },
            psserrormsg: false,
            userFirstNameErormsg: false,
            userLastNameErrormsg: false,
            userEmailErormsg: { empty: false, invalid: false },
            userPasswordErormsg: false,
            userVerPasswordErormsg: false,
            userPwdMisMatcherrormsg: false,
            userTerms: false,
            userforgotErrormsg: { empty: false, invalid: false },
            retrieveBkngRefErormsg: false,
            retrieveEmailErormsg: false,
            userlogined: this.checklogin(),
            userinfo: [],
            registerUserData: {
                firstName: '',
                lastName: '',
                emailId: '',
                password: '',
                verifyPassword: '',
                terms: '',
                title: 'Mr'
            },
            Languages: [],
            language: 'en',
            content: null,
            getdata: true,
            active_el: (sessionStorage.active_el) ? sessionStorage.active_el : 1,
            bEmail: '',
            bRef: '',
            bService: 'F',
            show: 'show',
            hide: 'hide',
            Branch: {
                Address: '',
                Email: '',
                Phone: '',
                Facebook: '',
                Twitter: '',
                Linkedin: '',
                Instagram: ''

            },
            Pagelink: {
                home: '',
                package: '',
                offer: '',
                contact: ''
            }
        }
    },
    methods: {
        loginaction: function() {

            if (!this.username.trim()) {
                this.usererrormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.username.trim())) {
                this.usererrormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.usererrormsg = { empty: false, invalid: false };
            }
            if (!this.password) {
                this.psserrormsg = true;
                return false;

            } else {
                this.psserrormsg = false;
                var self = this;
                login(this.username, this.password, function(response) {
                    if (response == false) {
                        self.userlogined = false;
                        alert("Invalid username or password.");
                    } else {
                        self.userlogined = true;
                        self.userinfo = JSON.parse(localStorage.User);
                        $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                        try {
                            self.$eventHub.$emit('logged-in', { userName: self.username, password: self.password });
                            signArea.headerLogin({ userName: self.username, password: self.password })
                        } catch (error) {

                        }
                    }
                });

            }



        },
        validEmail: function(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        checklogin: function() {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);
                return true;
            } else {
                this.userlogined = false;
                return false;
            }
        },
        logout: function() {
            this.userlogined = false;
            this.userinfo = [];
            localStorage.profileUpdated = false;
            try {
                this.$eventHub.$emit('logged-out');
                signArea.logout()
            } catch (error) {

            }
            commonlogin();
            // signOut();
            // signOutFb();
        },
        registerUser: function() {
            if (this.registerUserData.firstName.trim() == "") {
                this.userFirstNameErormsg = true;
                return false;
            } else {
                this.userFirstNameErormsg = false;
            }
            if (this.registerUserData.lastName.trim() == "") {
                this.userLastNameErrormsg = true;
                return false;
            } else {
                this.userLastNameErrormsg = false;
            }
            if (this.registerUserData.emailId.trim() == "") {
                this.userEmailErormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.registerUserData.emailId.trim())) {
                this.userEmailErormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.userEmailErormsg = { empty: false, invalid: false };
            }
            var vm = this;
            registerUser(this.registerUserData.emailId, this.registerUserData.firstName, this.registerUserData.lastName, this.registerUserData.title, function(response) {
                if (response.isSuccess == true) {
                    vm.username = response.data.data.user.loginId;
                    vm.password = response.data.data.user.password;
                    login(vm.username, vm.password, function (response) {
                        if (response != false) {
                            $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                            window.location.href = "/edit-my-profile.html?edit-profile=true";
                        }
                    });
                }

            });
        },

        forgotPassword: function() {
            if (this.emailId.trim() == "") {
                this.userforgotErrormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.emailId.trim())) {
                this.userforgotErrormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.userforgotErrormsg = { empty: false, invalid: false };
            }

            var datas = {
                emailId: this.emailId,
                agencyCode: localStorage.AgencyCode,
                logoUrl: window.location.origin + "/" + localStorage.AgencyFolderName + "/website-informations/logo/logo.png",
                websiteUrl: window.location.origin,
                resetUri: window.location.origin + "/" + localStorage.AgencyFolderName + "/reset-password.html"
            };
            $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:none;background:grey !important;");

            var huburl = ServiceUrls.hubConnection.baseUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var requrl = ServiceUrls.hubConnection.hubServices.forgotPasswordUrl;
            axios.post(huburl + portno + requrl, datas)
                .then((response) => {
                    if (response.data != "") {
                        alert(response.data);
                    } else {
                        alert("Error in forgot password. Please contact admin.");
                    }
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:#006ace !important");

                })
                .catch((err) => {
                    console.log("FORGOT PASSWORD  ERROR: ", err);
                    if (err.response.data.message == 'No User is registered with this emailId.') {
                        alert(err.response.data.message);
                    } else {
                        alert('We have found some technical difficulties. Please contact admin!');
                    }
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:#006ace !important");

                })

        },

        retrieveBooking: function() {
            if (this.bEmail == "") {
                //alert('Email required !');
                this.retrieveEmailErormsg = true;
                return false;

            } else if (!this.validEmail(this.bEmail)) {
                //alert('Invalid Email !');
                this.retrieveEmailErormsg = true;
                return false;

            } else {
                this.retrieveEmailErormsg = false;
            }
            if (this.bRef == "") {
                this.retrieveBkngRefErormsg = true;
                return false;

            } else {
                this.retrieveBkngRefErormsg = false;
            }
            if (!this.retrieveBkngRefErormsg && !this.retrieveEmailErormsg && !this.retrieveEmailErormsg) {
                switch (this.bService) {
                    case 'F':
                        this.retBookFlight();
                        break;
                    case 'H':
                        var vm = this;
                        var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;

                        axios({
                            method: "get",
                            url: hubUrl + "/hotelBook/bookingbyref/" + vm.bRef + ":" + vm.bEmail,
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer ' + localStorage.access_token
                            }
                        }).then(response => {
                            window.sessionStorage.setItem('userAction', vm.bRef);
                            window.location.href = "/Hotels/hotel-detail.html#/hotelConfirmation";

                        }).catch(error => {
                            alertify.alert('Error!', 'Booking details not found!');
                        });
                        break;
                    default:
                        break;
                }
            }

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        activate: function(el) {
            sessionStorage.active_el = el;
            this.active_el = el;
        },
        getpagecontent: function() {
            var self = this;
            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Master page/Master page/Master page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    self.content = response.data;
                    console.log(self.content);
                    self.getdata = false;
                    window.localStorage.setItem("cmsContent", JSON.stringify(response.data));
                    var header = self.pluck('Header', self.content.area_List);
                    var footer = self.pluck('Footer', self.content.area_List);
                    document.title = self.pluck('Page_Title', header[0].component);
                    self.show = self.pluckcom('showpassword_label', header[0].component);
                    self.hide = self.pluckcom('hidepassword_label', header[0].component);
                    self.Branch.Address = self.pluckcom('Branch_address', footer[0].component);
                    self.Branch.Email = self.pluckcom('Header_email_address', header[0].component);
                    self.Branch.Phone = self.pluckcom('Header_phonenumber', header[0].component);
                    self.Branch.Facebook = self.pluckcom('Facebook_link', footer[0].component);
                    self.Branch.Twitter = self.pluckcom('Twitter_link', footer[0].component);
                    self.Branch.Linkedin = self.pluckcom('LinkedIn_link', footer[0].component);
                    self.Branch.Instagram = self.pluckcom('Instagram_link', footer[0].component);
                    self.Pagelink.home = self.pluckcom('Home_menu_lable', header[0].component);
                    self.Pagelink.package = self.pluckcom('Packages_menu_title', header[0].component);
                    self.Pagelink.offer = self.pluckcom('Offer_menu_title', header[0].component);
                    self.Pagelink.contact = self.pluckcom('Contact_us_menu_title', header[0].component);
                    Vue.nextTick(function() {
                        (

                            function() {
                                jqurufunctions();
                                self.active_el = (sessionStorage.active_el) ? sessionStorage.active_el : 1;
                                //Login/Signup modal window - by CodyHouse.co
                                function ModalSignin(element) {
                                    this.element = element;
                                    this.blocks = this.element.getElementsByClassName('js-signin-modal-block');
                                    this.switchers = this.element.getElementsByClassName('js-signin-modal-switcher')[0].getElementsByTagName('a');
                                    this.triggers = document.getElementsByClassName('js-signin-modal-trigger');
                                    this.hidePassword = this.element.getElementsByClassName('js-hide-password');
                                    this.init();
                                };

                                ModalSignin.prototype.init = function() {
                                    var self1 = this;
                                    //open modal/switch form
                                    for (var i = 0; i < this.triggers.length; i++) {
                                        (function(i) {
                                            self1.triggers[i].addEventListener('click', function(event) {
                                                if (event.target.hasAttribute('data-signin')) {
                                                    event.preventDefault();
                                                    self1.showSigninForm(event.target.getAttribute('data-signin'));
                                                }
                                            });
                                        })(i);
                                    }

                                    //close modal
                                    this.element.addEventListener('click', function(event) {
                                        if (hasClass(event.target, 'js-signin-modal') || hasClass(event.target, 'js-close')) {
                                            event.preventDefault();
                                            removeClass(self1.element, 'cd-signin-modal--is-visible');
                                        }
                                    });
                                    //close modal when clicking the esc keyboard button
                                    document.addEventListener('keydown', function(event) {
                                        (event.which == '27') && removeClass(self1.element, 'cd-signin-modal--is-visible');
                                    });

                                    // //hide/show password
                                    // for (var i = 0; i < this.hidePassword.length; i++) {
                                    //     (function(i) {
                                    //         self1.hidePassword[i].addEventListener('click', function(event) {
                                    //             self1.togglePassword(self1.hidePassword[i]);
                                    //         });
                                    //     })(i);
                                    // }

                                    //IMPORTANT - REMOVE THIS - it's just to show/hide error messages in the demo

                                };

                                // ModalSignin.prototype.togglePassword = function(target) {
                                //     var password = target.previousElementSibling;
                                //     ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text'): password.setAttribute('type', 'password');
                                //     target.textContent = ('Hide' == target.textContent) ? 'Show' : 'Hide';
                                //     putCursorAtEnd(password);
                                // }

                                ModalSignin.prototype.showSigninForm = function(type) {
                                    // show modal if not visible
                                    !hasClass(this.element, 'cd-signin-modal--is-visible') && addClass(this.element, 'cd-signin-modal--is-visible');
                                    // show selected form
                                    for (var i = 0; i < this.blocks.length; i++) {
                                        this.blocks[i].getAttribute('data-type') == type ? addClass(this.blocks[i], 'cd-signin-modal__block--is-selected') : removeClass(this.blocks[i], 'cd-signin-modal__block--is-selected');
                                    }
                                    //update switcher appearance
                                    var switcherType = (type == 'signup') ? 'signup' : 'login';
                                    for (var i = 0; i < this.switchers.length; i++) {
                                        this.switchers[i].getAttribute('data-type') == switcherType ? addClass(this.switchers[i], 'cd-selected') : removeClass(this.switchers[i], 'cd-selected');
                                    }
                                };

                                ModalSignin.prototype.toggleError = function(input, bool) {
                                    // used to show error messages in the form
                                    toggleClass(input, 'cd-signin-modal__input--has-error', bool);
                                    toggleClass(input.nextElementSibling, 'cd-signin-modal__error--is-visible', bool);
                                }

                                var signinModal = document.getElementsByClassName("js-signin-modal")[0];
                                if (signinModal) {
                                    new ModalSignin(signinModal);
                                }

                                // toggle main navigation on mobile
                                var mainNav = document.getElementsByClassName('js-main-nav')[0];
                                if (mainNav) {
                                    mainNav.addEventListener('click', function(event) {
                                        if (hasClass(event.target, 'js-main-nav')) {
                                            var navList = mainNav.getElementsByTagName('ul')[0];
                                            toggleClass(navList, 'cd-main-nav__list--is-visible', !hasClass(navList, 'cd-main-nav__list--is-visible'));
                                        }
                                    });
                                }

                                //class manipulations - needed if classList is not supported
                                function hasClass(el, className) {
                                    if (el.classList) return el.classList.contains(className);
                                    else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
                                }

                                function addClass(el, className) {
                                    var classList = className.split(' ');
                                    if (el.classList) el.classList.add(classList[0]);
                                    else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
                                    if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
                                }

                                function removeClass(el, className) {
                                    var classList = className.split(' ');
                                    if (el.classList) el.classList.remove(classList[0]);
                                    else if (hasClass(el, classList[0])) {
                                        var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
                                        el.className = el.className.replace(reg, ' ');
                                    }
                                    if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
                                }

                                function toggleClass(el, className, bool) {
                                    if (bool) addClass(el, className);
                                    else removeClass(el, className);
                                }
                                // $("#modal_retrieve").leanModal({
                                //     top: 100,
                                //     overlay: 0.6,
                                //     closeButton: ".modal_close"
                                // });
                                //credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
                                function putCursorAtEnd(el) {
                                    if (el.setSelectionRange) {
                                        var len = el.value.length * 2;
                                        el.focus();
                                        el.setSelectionRange(len, len);
                                    } else {
                                        el.value = el.value;
                                    }
                                };
                            })();
                    }.bind(self));


                }).catch(function(error) {
                    console.log('Error');
                    this.content = [];
                });
            });
        },
        // retrieveBooking: function () {
        //     switch (this.bService) {
        //         case 'F':
        //             this.retBookFlight();
        //             break;
        //         case 'H':
        //             //for Kelvin
        //             break;
        //         default:
        //             break;
        //     }
        // },
        retBookFlight: function() {
            if (this.bEmail == "") {
                alert('Email required !');
                return false;
            } else if (!this.validEmail(this.bEmail)) {
                alert('Invalid Email !');
                return false;
            } else {
                this.retrieveEmailErormsg = false;
            }
            if (this.bRef == "") {
                this.retrieveBkngRefErormsg = true;
                return false;
            } else {
                this.retrieveBkngRefErormsg = false;
            }
            var bookData = {
                BkngRefID: this.bRef,
                emailId: this.bEmail,
                redirectFrom: 'retrievebooking',
                isMailsend: false
            };
            localStorage.bookData = JSON.stringify(bookData);
            window.location.href = '/Flights/flight-confirmation.html';
        },
        showhidepassword: function(event) {
            var password = event.target.previousElementSibling;
            ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text'): password.setAttribute('type', 'password');
            event.target.textContent = (this.hide == event.target.textContent) ? this.show : this.hide;
        },
    },
    mounted: function() {

        document.onreadystatechange = () => {
            if (document.readyState == "complete") {
                if (localStorage.direction == 'rtl') {
                    $(".arborder").addClass("arbrder_left");
                    $(".ar_direction").addClass("ar_direction1");
                    $(".ajs-modal").addClass("ar_direction1");
                    $(".multiSec").addClass("multiSec1");
                } else {
                    $(".arborder").removeClass("arbrder_left");
                    $(".ar_direction").removeClass("ar_direction1");
                    $(".ajs-modal").removeClass("ar_direction1");
                    $(".multiSec").removeClass("multiSec1");
                }
            }
        }


        if (localStorage.IsLogin == "true") {
            this.userlogined = true;
            this.userinfo = JSON.parse(localStorage.User);

        } else {
            this.userlogined = false;

        }
        this.getpagecontent();

    },
    created: function() {

    },
    watch: {
        updatelogin: function() {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);

            } else {
                this.userlogined = false;

            }

        },
        bRef: function() {
            this.retrieveBkngRefErormsg = false;
        }

    }
});
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});

Vue.prototype.$eventHub = new Vue({
    data: {}
});

var mapComponent = Vue.component('mapcomponent', {
    template: `<div v-if="getdata"></div><div class="contact_head" id="contact" v-else>
    
    <div class="contact1">
 
        <div class="container" style="width: 100%;">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 country" v-for="item in pluck('main',content.area_List)">
                    <div class="row">
                        <h3>{{pluck('Branch_Name',item.component)[0].toUpperCase()}}</h3>
                    </div>
                    <div class="row uae">
                        <div class="col-md-3 col-sm-6 col-xs-3" @click="changeCurrentLocation(brnch)"  v-for="(brnch, index) in pluckcom('Uae_branches',item.component)">
                            <img class="" :src="brnch.icon">
                            <p>{{brnch.Branch_Name}}</p>
                        </div>
                        
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 country" v-for="item in pluck('Global_branches',content.area_List)">
                    <div class="row">
                        <h3>{{pluck('Branch_Name',item.component)[0].toUpperCase()}}</h3>
                    </div>
                        <div class="row uae">
                            <div class="col-md-3 col-sm-6 col-xs-3"   @click="changeCurrentLocation(brnch)" v-for="(brnch, index) in pluckcom('Global_branch',item.component)">
                            <img class="" :src="brnch.icon">
                            <p>{{brnch.Branch_Name}}</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="map_sec" v-if="showMap">
        <iframe
            :src="'https://www.google.com/maps/embed/v1/place?q='+currentLocationDetails.Latitude + ',' + currentLocationDetails.Longitude +'&zoom=14&key=AIzaSyBoOq8WuZ48t8uO4_XK_ACLJyIsOLX0F58'"
            width="100%" height="450" frameborder="0" style="border:0" allowfullscreen="">
        </iframe>
    </div>
</div>`,
    data() {
        return {
            content: null,
            getdata: true,
            currentLocationDetails: {},
            showMap: false,
            contactTitle: ''
        }

    },
    methods: {
        changeCurrentLocation: function(brnch) {
            this.currentLocationDetails = brnch;
            this.$eventHub.$emit('select-location', brnch);
        },
        getpagecontent: function() {
            var self = this;
            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact us/Contact us/Contact us.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    self.content = response.data;
                    self.getdata = false;
                    self.contactTitle = response.data.area_List[0].Common_Datas.component[0].Title;
                    var uaeComponents = self.pluck('main', self.content.area_List);
                    var uaeBranch = self.pluckcom('Uae_branches', uaeComponents[0].component);
                    self.currentLocationDetails = uaeBranch[0];
                    self.$eventHub.$emit('select-location', self.currentLocationDetails);
                }).catch(function(error) {
                    console.log('Error');
                    this.content = [];
                });

            });
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
    },
    created: function() {
        this.getpagecontent();
        if (window.location.pathname == "/Nirvana/") {
            this.showMap = true;
        }
    }
});

var FooterComponent = Vue.component('footeritem', {
    template: `<div>
    <div class="container ar_direction">
            <div class="row flx_sec">
                <div class="col-md-3 col-sm-3">
                    <div class="foot-logo">
                        <a href="#"><img :src="Branch.logo" alt="logo"></a>
                        <p>{{Branch.Address}}</p>
                    </div>

                    <ul class="foot-social">
                        <li><a :href="Branch.Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a :href="Branch.Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a :href="Branch.Linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                        <li><a :href="Branch.Instagram" target="_blank"> <i class="fa fa-instagram"></i></a></li>

                    </ul>

                </div>

               <div class="col-md-6 col-sm-5">
                    <!--<div class="footer-newsletter">
                        <h3>{{Newsletter.title}}</h3>
                        <input type="text" class="form-control" v-model="newsltrname" id="nestext" name="text" :placeholder="Newsletter.name">
                        <input type="email" id="email" class="form-control"  v-model="newsltremail" name="text" :placeholder="Newsletter.email">
                        <button type="submit" v-on:click="sendnewsletter">{{Newsletter.button}}</button>

                    </div>-->
                    <div class="footer-detail">
                        <ul class="foot-links">
                        <h4>Site Links</h4>
                        <!--<li><a href="/">{{Pagelink.home}}</a></li>
                        <li><a href="/easy2go/package.html">{{Pagelink.package}} </a></li>
                        <li><a href="/easy2go/offers.html">{{Pagelink.offer}} </a></li>-->
                            <li><a href="/easy2go/contactus.html"> {{Pagelink.contact}}</a></li>
                            <li><a href="/easy2go/privacypolicy.html">{{Pagelink.privacyPolicy}} </a></li>
                            <li><a href="/easy2go/terms-and-conditions.html">{{Pagelink.termsConditions}}</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-3 col-sm-4">
                    <div class="foot-contact">
                        <ul>
                            <li><i class="fa fa-map-marker"></i>
                                <p>{{Branch.Address}} </p>
                            </li>
                            <li><i class="fa fa-envelope"></i> <a :href="'mailto:'+Branch.Email">{{Branch.Email}} </a></li>
                            <li><i class="fa fa-phone"></i><a :href="'tel:'+Branch.Phone">{{Branch.Phone}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--<div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-detail">
                        
                        <ul class="foot-links">
                           <li><a href="/">{{Pagelink.home}}</a></li>
                           <li><a href="/easy2go/package.html">{{Pagelink.package}} </a></li>
                           <li><a href="/easy2go/offers.html">{{Pagelink.offer}} </a></li>
                            <li><a href="/easy2go/contactus.html"> {{Pagelink.contact}}</a></li>
                            <li><a href="/easy2go/privacypolicy.html">{{Pagelink.privacyPolicy}} </a></li>
                            <li><a href="/easy2go/terms-and-conditions.html">{{Pagelink.termsConditions}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>-->
        </div>
        <section class="all-reserved ar_direction">
        <div class="container">
            <div class="reserved">
                <p>{{Branch.copyright}}</p>
            </div>
            <div class="Powered">
                <p>Powered by:</p>
                <a href="http://www.oneviewit.com/" target="_blank"><img src="/easy2go/images/oneview_logo.png" alt="logo"></a>
            </div>
        </div>
    </section></div>`,
    data() {
        return {
            content: null,
            getdata: true,
            newsltrname: '',
            newsltremail: '',
            cntusername: '',
            cntemail: '',
            cntcontact: '',
            cntsubject: '',
            cntmessage: '',
            currentLocationDetails: {},
            textDirection: localStorage.direction,
            key: 0,
            Branch: {
                Address: '',
                Email: '',
                Phone: '',
                Facebook: '',
                Twitter: '',
                Linkedin: '',
                Instagram: '',
                copyright: '',
                logo: '',
            },
            Newsletter: {
                title: '',
                email: '',
                name: '',
                button: ''
            },
            Pagelink: {
                home: '',
                package: '',
                offer: '',
                contact: '',
                privacyPolicy: '',
                termsConditions: ''
            }
        }

    },
    props: {
        newKey: String
    },
    methods: {
        selectLocation: function(brnch) {
            this.currentLocationDetails = brnch;
        },
        getpagecontent: function() {


            var self = this;
            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Master page/Master page/Master page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    self.content = response.data;
                    self.getdata = false;
                    self.key = Math.random();
                    self.$eventHub.$on('select-location', self.selectLocation);
                    var header = self.pluck('Header', self.content.area_List);
                    var footer = self.pluck('Footer', self.content.area_List);
                    self.Branch.Address = self.pluckcom('Branch_address', footer[0].component);
                    self.Branch.Email = self.pluckcom('Header_email_address', header[0].component);
                    self.Branch.Phone = self.pluckcom('Header_phonenumber', header[0].component);
                    self.Branch.Facebook = self.pluckcom('Facebook_link', footer[0].component);
                    self.Branch.Twitter = self.pluckcom('Twitter_link', footer[0].component);
                    self.Branch.Linkedin = self.pluckcom('LinkedIn_link', footer[0].component);
                    self.Branch.Instagram = self.pluckcom('Instagram_link', footer[0].component);
                    self.Branch.copyright = self.pluckcom('Copyright_info', footer[0].component);
                    self.Branch.logo = self.pluckcom('Branch_footer_logo', footer[0].component);
                    self.Newsletter.title = self.pluckcom('Newsletter_heading', footer[0].component);
                    self.Newsletter.email = self.pluckcom('Email_placeholder', footer[0].component);
                    self.Newsletter.name = self.pluckcom('Name_placeholder', footer[0].component);
                    self.Newsletter.button = self.pluckcom('Newsletter_button_label', footer[0].component);
                    self.Pagelink.home = self.pluckcom('Home_menu_lable', header[0].component);
                    self.Pagelink.package = self.pluckcom('Packages_menu_title', header[0].component);
                    self.Pagelink.offer = self.pluckcom('Offer_menu_title', header[0].component);
                    self.Pagelink.contact = self.pluckcom('Contact_us_menu_title', header[0].component);
                    self.Pagelink.privacyPolicy = self.pluckcom('Privacy_Policy_Menu_Title', header[0].component);
                    self.Pagelink.termsConditions = self.pluckcom('Terms_Condition_Menu_Title', header[0].component);

                }).catch(function(error) {
                    console.log('Error');
                    this.content = [];
                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);

                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        sendnewsletter: async function() {


            if (!this.newsltrname) {
                alertify.alert('Alert', 'Name required.');
                return false;
            }
            if (!this.newsltremail) {
                alertify.alert('Alert', 'Email Id required.');
                return false;;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.');
                return false;
            } else {

                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                var filterValue = "type='Subscribe Details' AND keyword2='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                    return false;
                } else {
                    var frommail = JSON.parse(localStorage.User).loginNode.email;
                    var custmail = {
                        type: "UserAddedRequest",
                        fromEmail: frommail,
                        toEmail: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                        logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                        agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                        agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                        personName: this.newsltrname,
                        primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                        secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                    };



                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = { type: "Subscribe Details", keyword1: this.newsltrname, keyword2: this.newsltremail, keyword3: "Subscribe Newsletter", date1: requestedDate, nodeCode: agencyCode };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        var emailApi = ServiceUrls.emailServices.emailApi;
                        sendMailService(emailApi, custmail);
                        alertify.alert('Newsletter', 'Thank you for subscribing !');
                    } catch (e) {

                    }
                }

            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        sendcontactus: async function() {
            if (!this.cntusername) {
                alertify.alert('Alert', 'Name required.').set('closable', false);

                return false;
            }
            if (!this.cntemail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.cntcontact) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            if (!this.cntsubject) {
                alertify.alert('Alert', 'Subject required.').set('closable', false);
                return false;
            }

            if (!this.cntmessage) {
                alertify.alert('Alert', 'Message required.').set('closable', false);
                return false;
            } else {
                var fromEmail = JSON.parse(localStorage.User).loginNode.email;
                var toEmail = JSON.parse(localStorage.User).loginNode.email;
                var postData = {
                    type: "AdminContactUsRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.cntusername,
                    emailId: this.cntemail,
                    contact: this.cntcontact,
                    message: this.cntmessage,

                };
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.cntusername,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };


                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Contact Us",
                    keyword1: this.cntusername,
                    keyword2: this.cntemail,
                    keyword3: this.cntcontact,
                    keyword4: this.cntsubject,
                    text1: this.cntmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, postData);
                    sendMailService(emailApi, custmail);
                    this.cntemail = '';
                    this.cntusername = '';
                    this.cntcontact = '';
                    this.cntmessage = '';
                    this.cntsubject = '';
                    alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
                } catch (e) {

                }



            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {
            var allDBData = [];
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        }
    },
    mounted: function() {
        // document.onreadystatechange = () => {
        //  if (document.readyState == "complete") {
        if (localStorage.direction == 'rtl') {
            // $(".arborder").addClass("arbrder_left");
            $(".ar_direction").addClass("ar_direction1");
            // $(".ajs-modal").addClass("ar_direction1");
        } else {
            //   $(".arborder").removeClass("arbrder_left");
            $(".ar_direction").removeClass("ar_direction1");
            //    $(".ajs-modal").removeClass("ar_direction1");
        }
        // }
        // }

        this.getpagecontent();

    },
    created: function() {
        this.$eventHub.$on('select-location', this.selectLocation);
    },
    beforeDestroy: function() {
        this.$eventHub.$off('select-location');
    }
});

var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});







/**
 * jquery.dlmenu.js v1.0.1
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Codrops
 * http://www.codrops.com
 */
;
(function($, window, undefined) {

    'use strict';

    // global
    var Modernizr = window.Modernizr,
        $body = $('body');

    $.DLMenu = function(options, element) {
        this.$el = $(element);
        this._init(options);
    };

    // the options
    $.DLMenu.defaults = {
        // classes for the animation effects
        animationClasses: { classin: 'dl-animate-in-1', classout: 'dl-animate-out-1' },
        // callback: click a link that has a sub menu
        // el is the link element (li); name is the level name
        onLevelClick: function(el, name) { return false; },
        // callback: click a link that does not have a sub menu
        // el is the link element (li); ev is the event obj
        onLinkClick: function(el, ev) { return false; }
    };

    $.DLMenu.prototype = {
        _init: function(options) {

            // options
            this.options = $.extend(true, {}, $.DLMenu.defaults, options);
            // cache some elements and initialize some variables
            this._config();

            var animEndEventNames = {
                    'WebkitAnimation': 'webkitAnimationEnd',
                    'OAnimation': 'oAnimationEnd',
                    'msAnimation': 'MSAnimationEnd',
                    'animation': 'animationend'
                },
                transEndEventNames = {
                    'WebkitTransition': 'webkitTransitionEnd',
                    'MozTransition': 'transitionend',
                    'OTransition': 'oTransitionEnd',
                    'msTransition': 'MSTransitionEnd',
                    'transition': 'transitionend'
                };
            // animation end event name
            this.animEndEventName = animEndEventNames[Modernizr.prefixed('animation')] + '.dlmenu';
            // transition end event name
            this.transEndEventName = transEndEventNames[Modernizr.prefixed('transition')] + '.dlmenu',
                // support for css animations and css transitions
                this.supportAnimations = Modernizr.cssanimations,
                this.supportTransitions = Modernizr.csstransitions;

            this._initEvents();

        },
        _config: function() {
            this.open = false;
            this.$trigger = this.$el.children('.dl-trigger');
            this.$menu = this.$el.children('ul.dl-menu');
            this.$menuitems = this.$menu.find('li:not(.dl-back)');
            this.$el.find('ul.dl-submenu').prepend('<li class="dl-back"><a href="#">back</a></li>');
            this.$back = this.$menu.find('li.dl-back');
            $(".dl-trigger").unbind('click');
        },
        _initEvents: function() {

            var self = this;

            this.$trigger.on('click.dlmenu', function() {

                if (self.open) {
                    self._closeMenu();
                } else {
                    self._openMenu();
                }
                return false;

            });

            this.$menuitems.on('click.dlmenu', function(event) {

                event.stopPropagation();

                var $item = $(this),
                    $submenu = $item.children('ul.dl-submenu');

                if ($submenu.length > 0) {

                    var $flyin = $submenu.clone().css('opacity', 0).insertAfter(self.$menu),
                        onAnimationEndFn = function() {
                            self.$menu.off(self.animEndEventName).removeClass(self.options.animationClasses.classout).addClass('dl-subview');
                            $item.addClass('dl-subviewopen').parents('.dl-subviewopen:first').removeClass('dl-subviewopen').addClass('dl-subview');
                            $flyin.remove();
                        };

                    setTimeout(function() {
                        $flyin.addClass(self.options.animationClasses.classin);
                        self.$menu.addClass(self.options.animationClasses.classout);
                        if (self.supportAnimations) {
                            self.$menu.on(self.animEndEventName, onAnimationEndFn);
                        } else {
                            onAnimationEndFn.call();
                        }

                        self.options.onLevelClick($item, $item.children('a:first').text());
                    });

                    return false;

                } else {
                    self.options.onLinkClick($item, event);
                }

            });

            this.$back.on('click.dlmenu', function(event) {

                var $this = $(this),
                    $submenu = $this.parents('ul.dl-submenu:first'),
                    $item = $submenu.parent(),

                    $flyin = $submenu.clone().insertAfter(self.$menu);

                var onAnimationEndFn = function() {
                    self.$menu.off(self.animEndEventName).removeClass(self.options.animationClasses.classin);
                    $flyin.remove();
                };

                setTimeout(function() {
                    $flyin.addClass(self.options.animationClasses.classout);
                    self.$menu.addClass(self.options.animationClasses.classin);
                    if (self.supportAnimations) {
                        self.$menu.on(self.animEndEventName, onAnimationEndFn);
                    } else {
                        onAnimationEndFn.call();
                    }

                    $item.removeClass('dl-subviewopen');

                    var $subview = $this.parents('.dl-subview:first');
                    if ($subview.is('li')) {
                        $subview.addClass('dl-subviewopen');
                    }
                    $subview.removeClass('dl-subview');
                });

                return false;

            });

        },
        closeMenu: function() {
            if (this.open) {
                this._closeMenu();
            }
        },
        _closeMenu: function() {
            var self = this,
                onTransitionEndFn = function() {
                    self.$menu.off(self.transEndEventName);
                    self._resetMenu();
                };

            this.$menu.removeClass('dl-menuopen');
            this.$menu.addClass('dl-menu-toggle');
            this.$trigger.removeClass('dl-active');

            if (this.supportTransitions) {
                this.$menu.on(this.transEndEventName, onTransitionEndFn);
            } else {
                onTransitionEndFn.call();
            }

            this.open = false;
        },
        openMenu: function() {
            if (!this.open) {
                this._openMenu();
            }
        },
        _openMenu: function() {
            var self = this;
            // clicking somewhere else makes the menu close
            $body.off('click').on('click.dlmenu', function() {
                self._closeMenu();
            });
            this.$menu.addClass('dl-menuopen dl-menu-toggle').on(this.transEndEventName, function() {
                $(this).removeClass('dl-menu-toggle');
            });
            this.$trigger.addClass('dl-active');
            this.open = true;
        },
        // resets the menu to its original state (first level of options)
        _resetMenu: function() {
            this.$menu.removeClass('dl-subview');
            this.$menuitems.removeClass('dl-subview dl-subviewopen');
        }
    };

    var logError = function(message) {
        if (window.console) {
            window.console.error(message);
        }
    };

    $.fn.dlmenu = function(options) {
        if (typeof options === 'string') {
            var args = Array.prototype.slice.call(arguments, 1);
            this.each(function() {
                var instance = $.data(this, 'dlmenu');
                if (!instance) {
                    logError("cannot call methods on dlmenu prior to initialization; " +
                        "attempted to call method '" + options + "'");
                    return;
                }
                if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
                    logError("no such method '" + options + "' for dlmenu instance");
                    return;
                }
                instance[options].apply(instance, args);
            });
        } else {
            this.each(function() {
                var instance = $.data(this, 'dlmenu');
                if (instance) {
                    instance._init();
                } else {
                    instance = $.data(this, 'dlmenu', new $.DLMenu(options, this));
                }
            });
        }
        return this;
    };

})(jQuery, window);







// $(document).ready(function(){


function jqurufunctions() {
    $('.nav ul li a[href^="#"]').on('click', function(event) {
        var target = $(this.getAttribute('href'));
        if (target.length) {
            event.preventDefault();

            $('html, body').stop().animate({
                scrollTop: target.offset().top - 0
            }, 1200);
        }
    });
    $("#advanceSearch").unbind('click');
    $('#advanceSearch').click(function() {

        $('.ad_view').toggle();
    });
    $('#dl-menu').dlmenu({
        animationClasses: { classin: 'dl-animate-in-3', classout: 'dl-animate-out-3' }
    });
}
$(function() {
    var selectedClass = "";
    $(".filter").click(function() {
        selectedClass = $(this).attr("data-rel");
        $("#gallery").fadeTo(100, 0.1);
        $("#gallery div").not("." + selectedClass).fadeOut().removeClass('animation');
        setTimeout(function() {
            $("." + selectedClass).fadeIn().addClass('animation');
            $("#gallery").fadeTo(300, 1);
        }, 300);
    });
});
// });

function searchArray(nameKey, myArray, tagName) {
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i][tagName] === nameKey) {
            return myArray[i];
        }
    }
}

//Google
function googleInit() {
    console.log('google inited');
    gapi.load('auth2', () => {
        gapi.auth2.init({ client_id: '509151388330-lsap2aj7ace202lmav6l16eflekel2ih.apps.googleusercontent.com' }).then(() => {
            // DO NOT ATTEMPT TO RENDER BUTTON UNTIL THE 'Init' PROMISE RETURNS
            renderButton();
            myGoogleButtonReg();
        });
    })
}

function renderButton() {
    gapi.signin2.render('myGoogleButton', {
        'scope': 'profile email',
        'width': 240,
        'height': 40,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });
}

function myGoogleButtonReg() {
    gapi.signin2.render('myGoogleButtonReg', {
        'scope': 'profile email',
        'width': 240,
        'height': 40,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });
}

function changeShowTxt() {
    var langauagee = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
    if (langauagee == "ar") {
        if ($('.changeShowTxtCls').text() == 'Show') {
           
            $('.changeShowTxtCls').text('تبين');
        }
    } else {
        if ($('.changeShowTxtCls').text() == 'تبين') {
           
            $('.changeShowTxtCls').text('Show');
        }
    }
}