const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var packagelist = new Vue({
    i18n,
    el: '#packagepage',
    name: 'packagepage',
    data: {
        content: null,
        getdata: false,
        packages: null,
        getpackage: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        spPackages: null,
        spTitle: '',
        pkgPgBnr: '',
        homebreadcrumb: '',
        offertag:'',
        moredetails:'',
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                 
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var packagepage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Packages/Packages/Packages.ftl';
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';
                var generalPageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/General Details/General Details/General Details.ftl';

                axios.get(generalPageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = true;
                    if (response.data.area_List.length > 1 && response.data.area_List[1].Packages_Page_Details!=undefined) {
                        self.pkgPgBnr = self.pluckcom('Banner_Image', response.data.area_List[1].Packages_Page_Details.component);
                        self.spTitle = self.pluckcom('Title', response.data.area_List[1].Packages_Page_Details.component);
                    }
                    if (response.data.area_List.length > 4 && response.data.area_List[4].Common_Details!=undefined) {
                        self.homebreadcrumb = self.pluckcom('Home_Breadcrump_Label', response.data.area_List[4].Common_Details.component);
                        self.moredetails = self.pluckcom('View_Button_Name', response.data.area_List[4].Common_Details.component);
                        self.offertag = self.pluckcom('Special_Offer_Tag', response.data.area_List[4].Common_Details.component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
                axios.get(topackageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    var packageData = response.data;
                    let holidayaPackageListTemp=[];
                    if(packageData!=undefined&&packageData.Values!=undefined){
                        holidayaPackageListTemp = packageData.Values.filter(function (el) {
                            return el.Status == true &&
                                (new Date(el.To_Date.replace(/-/g, "/"))).getTime() >= Date.now() &&
                                    el.Holiday_Package == true
                        });
                    }
                    self.packages =holidayaPackageListTemp;
                    self.getpackage = true;
                    /*var Result = response.data;
                    if (Result != null || Result.area_List.length > 0) {
                        self.getpackage = true;
                        Result = self.pluck('main', Result.area_List);
                        Result = self.pluckcom('Package_list', Result[0].component);
                        Result = Result.filter(function (el) {
                            return el.To_Date && (new Date(el.To_Date)).getTime() >= Date.now()
                        });
                        self.packages = Result.filter(function (el) {
                            return el.Status == true &&
                                el.Holiday_Packages == true
                        });
                       
                    }*/

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });

            });

        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/easy2go/packageview.html?page=" + url + "&from=pkg";
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;
        }
    },
    mounted: function () {
        this.getPagecontent();
    },
});