const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var aboutus = new Vue({
    i18n,
    el: '#contactuspage',
    name: 'contactuspage',
    data: {
        content: null,
        getdata: false,
        offerdata: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        pageContent: {
            Title: '',
            Home: '',
            Background: '',
            Subtitle: '',
            Description: '',
            mapurl: '',
            NamePlaceholder: '',
            EmailPlaceholder: '',
            PhonePlaceholder: '',
            SubjectPlaceholder: '',
            MessagePlaceholder: '',
            Button: ''
        },
        branch: {
            Address: '',
            Phone: '',
            Email: '',
            Facebook: '',
            Twitter: '',
            Linkedin: '',
            Instagtram: '',
            Addresslabel: '',
            Phonelabel: '',
            Emaillabel: ''
        },
        offerContent: {
            title: '',
            Subtitle: '',
            description: '',
            link: '',
            button: ''
        },
        cntusername: '',
        cntemail: '',
        cntcontact: '',
        cntmessage: '',
        cntsubject:''
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact us/Contact us/Contact us.ftl';

                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var pagecontent = self.pluck('Contact_us', self.content.area_List);
                    if (pagecontent.length > 0) {
                        self.pageContent.Title = self.pluckcom('Title', pagecontent[0].component);
                        self.pageContent.Home = self.pluckcom('Home_breadcrumb_label', pagecontent[0].component);
                        self.pageContent.Background = self.pluckcom('Background_image', pagecontent[0].component);
                        self.pageContent.Subtitle = self.pluckcom('Subtitle', pagecontent[0].component);
                        self.pageContent.Description = self.pluckcom('Description', pagecontent[0].component);
                        self.pageContent.mapurl = self.pluckcom('Google_map_embed_url', pagecontent[0].component);
                        self.pageContent.NamePlaceholder = self.pluckcom('Name_placeholder', pagecontent[0].component);
                        self.pageContent.EmailPlaceholder = self.pluckcom('Email_placeholder', pagecontent[0].component);
                        self.pageContent.PhonePlaceholder = self.pluckcom('Phone_placeholder', pagecontent[0].component);
                        self.pageContent.SubjectPlaceholder = self.pluckcom('Subject_placeholder', pagecontent[0].component);
                        self.pageContent.MessagePlaceholder = self.pluckcom('Message_placeholder', pagecontent[0].component);
                        self.pageContent.Button = self.pluckcom('Button_label', pagecontent[0].component);
                    }
                    var branch = self.pluck('Branch', self.content.area_List);
                    if (branch.length > 0) {
                        self.branch.Address = self.pluckcom('Address', branch[0].component);
                        self.branch.Phone = self.pluckcom('Phone_number', branch[0].component);
                        self.branch.Email = self.pluckcom('Email_address', branch[0].component);
                        self.branch.Facebook = self.pluckcom('Facebook_link', branch[0].component);
                        self.branch.Twitter = self.pluckcom('Twitter_link', branch[0].component);
                        self.branch.Linkedin = self.pluckcom('Linkedin_link', branch[0].component);
                        self.branch.Instagtram = self.pluckcom('Instagtram_link', branch[0].component);
                        self.branch.Addresslabel = self.pluckcom('Address_label', branch[0].component);
                        self.branch.Phonelabel = self.pluckcom('Phone_label', branch[0].component);
                        self.branch.Emaillabel = self.pluckcom('Email_label', branch[0].component);
                    }
                    var Offerdata = self.pluck('Offer_section', self.content.area_List);
                    if (Offerdata.length > 0) {
                        self.offerdata = self.pluckcom('Status', Offerdata[0].component);
                        self.offerContent.title = self.pluckcom('Title', Offerdata[0].component);
                        self.offerContent.Subtitle = self.pluckcom('Subtitle', Offerdata[0].component);
                        self.offerContent.description = self.pluckcom('Description', Offerdata[0].component);
                        self.offerContent.link = self.pluckcom('Button_link', Offerdata[0].component);
                        self.offerContent.button = self.pluckcom('Button_label', Offerdata[0].component);


                    }

                    self.getdata = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });


            });

        },
        sendcontactus: async function () {
            if (!this.cntusername) {
                alertify.alert('Alert', 'Name required.').set('closable', false);

                return false;
            }
            if (!this.cntemail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.cntcontact) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            if (!this.cntsubject) {
                alertify.alert('Alert', 'Subject required.').set('closable', false);
                return false;
            }

            if (!this.cntmessage) {
                alertify.alert('Alert', 'Message required.').set('closable', false);
                return false;
            } else {
                var toEmail = JSON.parse(localStorage.User).loginNode.email;
                var frommail=JSON.parse(localStorage.User).loginNode.email;   
                var postData = {
                    type: "AdminContactUsRequest",
                    fromEmail:frommail,
                    toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl +JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.cntusername,
                    emailId: this.cntemail,
                    contact: this.cntcontact,
                    message: this.cntmessage,

                };
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl +JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.cntusername,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };


                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Contact Us",
                    keyword1: this.cntusername,
                    keyword2: this.cntemail,
                    keyword3: this.cntcontact,
                    keyword4: this.cntsubject,
                    text1: this.cntmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, postData);
                    sendMailService(emailApi, custmail);
                    this.cntemail = '';
                    this.cntusername = '';
                    this.cntcontact = '';
                    this.cntmessage = '';
                    this.cntsubject = '';
                    alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
                } catch (e) {

                }



            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        }
    },
    mounted: function () {
        this.getPagecontent();
    },
});