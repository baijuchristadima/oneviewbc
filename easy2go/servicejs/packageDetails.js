const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var Packageinfo = new Vue({
    i18n,
    el: '#Packageinfo',
    name: 'Packageinfo',
    data: {
        content: null,
        getdata: true,
        pagecontent: null,
        getpagecontent: false,
        gallery: null,
        galleryon: false,
        packagetabs: null,
        locationTabName:"",
        galleryTabName:"",
        latitude: '',
        longitude: '',
        packagename: '',
        refNum: '',
        emailConfiguration:{},
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        username: '',
        userfname: '',
        userlname: '',
        Emailid: '',
        contactno: '',
        bookdate: '',
        adtcount: 1,
        chdcount: 0,
        infcount: 0,
        message: '',
        hasCity: false,
        Title: '',
        Backgroundimage: '',
        Home_breadcrumb: '',
        Price: '',
        SpecialOffer: '',
        Booktabletitle: '',
        Bookbutton: '',
        FirstName: '',
        LastName: '',
        Email: '',
        Phone: '',
        Bookdate: '',
        Adult: '',
        Child: '',
        Infant: '',
        Messagelabel: '',
        getmapurl: false,
        Package: {
            Title: '',
            Applicable: '',
            City: '',
            Destination: '',
            From: '',
            To: '',
            Price: '',
            Days: '',
            Nights: '',
            Short_description: '',
            Offer_Packages: false,
            Image: '',
            mapurl: null,
        },
        Agencynode: ''
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                self.Agencynode = Agencycode;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var packageurl = getQueryStringValue('page');
                if (packageurl != "") {
                    packageurl = packageurl.split('-').join(' ');
                    //  langauage = packageurl.split('_')[1];
                    packageurl = packageurl.split('_')[0];
                    var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
                    var pageinfourl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/General Details/General Details/General Details.ftl';
                    axios.get(topackageurl, {
                        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                    }).then(async function (response) {
                        //self.content = response.data;
                        let Package_InfoTemp={};
                        let Booking_InfoTemp={};
                        let Detail_PageTemp={};
                        let Package_SettingTemp={};
                        if(response!=undefined&&response.data!=undefined&&response.data.area_List!=undefined){
                            let areaList=response.data.area_List;
                            if(areaList.length>0 && areaList[0].Package_Info!=undefined){
                                Package_InfoTemp = await self.getAllMapData(areaList[0].Package_Info.component);
                            }
                            if(areaList.length>1 && areaList[1].Booking_Info!=undefined){
                                Booking_InfoTemp = await self.getAllMapData(areaList[1].Booking_Info.component);
                            }
                            if(areaList.length>2 && areaList[2].Detail_Page!=undefined){
                                Detail_PageTemp = await self.getAllMapData(areaList[2].Detail_Page.component);
                            }
                            if(areaList.length>3 && areaList[3].Package_Setting!=undefined){
                                Package_SettingTemp = await self.getAllMapData(areaList[3].Package_Setting.component);
                            }

                        }
                        self.content ={
                            Package_Info:Package_InfoTemp,
                            Booking_Info:Booking_InfoTemp,
                            Detail_Page:Detail_PageTemp,
                            Package_Setting:Package_SettingTemp
                        };
                        self.packagetabs = Detail_PageTemp.Package_Overview;
                        //self.Title = Package_InfoTemp.Name;
                        self.packagename= Package_InfoTemp.Name;
                        self.Package.Applicable =Package_InfoTemp.Applicable_Title;
                        self.Package.City =Booking_InfoTemp.City;
                        self.Package.Destination=Booking_InfoTemp.Destination;
                        self.Package.From =Booking_InfoTemp.From;
                        self.Package.To =Booking_InfoTemp.To_Date;
                        self.Package.Price =Booking_InfoTemp.Price;
                        self.Package.Days =Booking_InfoTemp.Days;
                        self.Package.Nights =Booking_InfoTemp.Nights;
                        self.Package.Short_description =Detail_PageTemp.Short_Description;
                        self.Package.Short_description =Detail_PageTemp.Short_Description;
                        self.Package.mapurl= Detail_PageTemp.Google_Map_Embed_URL;
                        self.gallery = Detail_PageTemp.Gallery;
                        self.Package.Image =Detail_PageTemp.Image;
                        self.Package.Offer_Packages =Package_SettingTemp.Offer_Package;

                        if (self.Package.mapurl != "") {
                            self.getmapurl = true;
                        }
                        if (self.gallery != "") {
                            self.galleryon = true;
                        }

                        //console.log(Detail_PageTemp);
                        //var Result = self.pluck('main', self.content.area_List);
                        //self.packagetabs = self.pluckcom('Package_tabs', Result[0].component);
                        //self.packagename = self.pluckcom('Title', Result[0].component);
                        //self.Package.Title = self.pluckcom('Title', Result[0].component);
                        //self.Package.Applicable = self.pluckcom('Applicable_title', Result[0].component);
                        //self.Package.City = self.pluckcom('City', Result[0].component);
                        //self.Package.Destination = self.pluckcom('Destination', Result[0].component);
                        //self.Package.From = self.pluckcom('From_Date', Result[0].component);
                        //self.Package.To = self.pluckcom('To_Date', Result[0].component);
                        //self.Package.Price = self.pluckcom('Price', Result[0].component);
                        //self.Package.Days = self.pluckcom('Days', Result[0].component);
                        //self.Package.Nights = self.pluckcom('Nights', Result[0].component);
                        //self.Package.Short_description = self.pluckcom('Short_description', Result[0].component);
                        //self.Package.Offer_Packages = self.pluckcom('Offer_Packages', Result[0].component);
                        //self.Package.Image = self.pluckcom('Image', Result[0].component);
                        
                        self.getdata = false;
                        self.setclaneder();
                    }).catch(function (error) {
                        window.location.href="/easy2go/package.html";
                        self.content = [];
                        self.getdata = true;
                    });
                    axios.get(pageinfourl, {
                        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                    }).then(async function (response) {

                        let bookFormData={};
                        let packageInfoData={};
                        let commonInfoData={};
                        var emailConfigurationTempData={};
                        if(response!=undefined&&response.data!=undefined&&response.data.area_List!=undefined){
                            let areaList=response.data.area_List;
                            if(areaList.length>0 && areaList[0].Offer_Page_Details!=undefined){
                                let titleDataTemp = await self.getAllMapData(areaList[0].Offer_Page_Details.component);
                                emailConfigurationTempData.offer=titleDataTemp;
                            }
                            if(areaList.length>1 && areaList[1].Packages_Page_Details!=undefined){
                                let titleDataTemp = await self.getAllMapData(areaList[1].Packages_Page_Details.component);
                                emailConfigurationTempData.package=titleDataTemp;
                            }

                            if(areaList.length>2 && areaList[2].Package_Info_Page!=undefined){
                                packageInfoData = await self.getAllMapData(areaList[2].Package_Info_Page.component);
                            }
                            if(areaList.length>3 && areaList[3].Book_Form_Details!=undefined){
                                bookFormData = await self.getAllMapData(areaList[3].Book_Form_Details.component);
                            }
                            if(areaList.length>4 && areaList[4].Common_Details!=undefined){
                                commonInfoData = await self.getAllMapData(areaList[4].Common_Details.component);
                            }
                        }
                        self.emailConfiguration=emailConfigurationTempData;
                        self.Title =packageInfoData.Title;
                        self.Backgroundimage =packageInfoData.Banner_Image;
                        self.Home_breadcrumb =commonInfoData.Home_Breadcrump_Label;
                        self.Price =commonInfoData.Price_Label;
                        self.SpecialOffer =commonInfoData.Special_Offer_Tag;
                        self.locationTabName=packageInfoData.Location_Tab_Name;
                        self.galleryTabName=packageInfoData.Gallery_Tab_Name;
                        self.Booktabletitle =bookFormData.Title;
                        self.Bookbutton =bookFormData.Book_Button_Name;
                        self.FirstName =bookFormData.First_Name_Place_Holder;
                        self.LastName =bookFormData.Last_Name_Place_Holder;
                        self.Email =bookFormData.Email_Place_Holder;
                        self.Phone =bookFormData.Phone_Place_Holder;
                        self.Bookdate =bookFormData.Date_Place_Holder;
                        self.Adult =bookFormData.Adult_Place_Holder;
                        self.Child =bookFormData.Child_Place_Holder;
                        self.Infant =bookFormData.Infant_Place_Holder;
                        self.Messagelabel =bookFormData.Message_Place_Holder;
                        self.getpagecontent = true
                        self.pagecontent = response.data;
                        //var pageinfo = self.pluck('main', self.pagecontent.area_List);
                       // self.Title = self.pluckcom('Title', pageinfo[0].component);
                        //self.Backgroundimage = self.pluckcom('Background_image', pageinfo[0].component);
                        //self.Home_breadcrumb = self.pluckcom('Home_breadcrumb_label', pageinfo[0].component);
                        //self.Price = self.pluckcom('Price_label', pageinfo[0].component);
                        //self.SpecialOffer = self.pluckcom('Special_offer_tag', pageinfo[0].component);
                        //self.Booktabletitle = self.pluckcom('Book_table_title', pageinfo[0].component);
                        //self.Bookbutton = self.pluckcom('Book_button_label', pageinfo[0].component);
                        //self.FirstName = self.pluckcom('First_Name', pageinfo[0].component);
                        //self.LastName = self.pluckcom('Last_Name', pageinfo[0].component);
                        //self.Email = self.pluckcom('Email', pageinfo[0].component);
                        //self.Phone = self.pluckcom('Phone', pageinfo[0].component);
                        //self.Bookdate = self.pluckcom('Book_date', pageinfo[0].component);
                        //self.Adult = self.pluckcom('Adult', pageinfo[0].component);
                        //self.Child = self.pluckcom('Child', pageinfo[0].component);
                        //self.Infant = self.pluckcom('Infant', pageinfo[0].component);
                        //self.Messagelabel = self.pluckcom('Message', pageinfo[0].component);

                    }).catch(function (error) {
                        console.log('Error');
                        self.pagecontent = [];
                    });

                }


            });
        },async getAllMapData(contentArry){
            var tempDataObject = {};
            if(contentArry!=undefined){
                contentArry.map(function (item) {
                    let allKeys=Object.keys(item)
                    for(let j=0;j<allKeys.length;j++){
                        let key=allKeys[j];
                        let value= item[key];
                        
                        if(key!='name' && key!='type'){
                            if(value==undefined||value==null){
                                value="";
                            }
                            tempDataObject[key]=value;
                        }
                    }
                });
            }
            return tempDataObject;
        },async getEmailObject(configureObject){
            let returnObject=undefined;
            if(configureObject!=undefined){
                returnObject=configureObject;
            }
            return returnObject;
        },
        async bookpackage () {
            if (!this.userfname) {
                alertify.alert('Alert', 'First name required.').set('closable', false);
                return false;
            }
            if (!this.userlname) {
                alertify.alert('Alert', 'Last name required.').set('closable', false);
                return false;
            }
            if (!this.Emailid) {
                alertify.alert('Alert', 'Email id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.Emailid.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.contactno) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.contactno.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            this.bookdate = $('#traveldate').val();
            if (!this.bookdate) {
                alertify.alert('Alert', 'pick a date.').set('closable', false);
                return false;
            }
            if (!this.message) {
                alertify.alert('Alert', 'comment required.').set('closable', false);
                return false;
            }
            else {
                this.username = this.userfname + " " + this.userlname;
                var fromPage = getQueryStringValue('from');
                var fromEmail = JSON.parse(localStorage.User).loginNode.email;
                var toEmail = { To_Email: '' };

                let configureObject=undefined;
                switch (fromPage.toLocaleLowerCase()) {
                    case 'pkg':
                        configureObject=await this.getEmailObject(this.emailConfiguration.package);
                        break;
                    case 'offer':
                        configureObject=await this.getEmailObject(this.emailConfiguration.offer);
                    break;
                    default:
                        configureObject={To_Email:toEmail};
                        break;
                }
                let agencyAddress="";
                var cc=[];
                let cusMessage="Thank you for the request, your request is being reviewed our sales team will soon revert with the status.";
                if(configureObject!=undefined && configureObject.To_Email!=undefined){
                    toEmail=configureObject.To_Email;
                }
                if(configureObject!=undefined && configureObject.CC!=undefined){
                    cc=configureObject.CC;
                }
                if(configureObject!=undefined && configureObject.Customer_Message!=undefined){
                    cusMessage=configureObject.Customer_Message;
                }
                
                if(JSON.parse(localStorage.User)!=undefined&&JSON.parse(localStorage.User).loginNode!=undefined&&
                    JSON.parse(localStorage.User).loginNode.address!=undefined){
                    agencyAddress=JSON.parse(localStorage.User).loginNode.address;
                }
                var postData = {
                    type: "PackageBookingRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(toEmail.To_Email) ? toEmail.To_Email : [toEmail.To_Email],
                    ccEmails:cc,
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + '.xhtml?ln=logo',
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: agencyAddress || "",
                    packegeName: this.packagename,
                    personName: this.username,
                    emailAddress: this.Emailid,
                    contact: this.contactno,
                    departureDate: this.bookdate,
                    adults: this.adtcount,
                    child2to5: this.chdcount,
                    child6to11: "0",
                    infants: this.infcount,
                    message: this.message,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                var custmail = {
                    type: "ThankYouRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(this.Emailid) ? this.Emailid : [this.Emailid],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + '.xhtml?ln=logo',
                    agencyName: JSON.parse(localStorage.User).loginNode.name,
                    agencyAddress: agencyAddress || "",
                    personName: this.username,
                    message:cusMessage,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                var traveldate = $('#traveldate').val() == "" ? "" : $('#traveldate').datepicker('getDate');
                let toDateVal = await moment(traveldate).format('YYYY-MM-DDThh:mm:ss');
                let requestedDate = await moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let requestObject = {
                    type: "Package Booking",
                    nodeCode: this.Agencynode,
                    keyword1: this.packagename,
                    keyword2: this.userfname + " " + this.userlname,
                    keyword3: this.Emailid,
                    keyword4: this.contactno,
                    date1: toDateVal,
                    date2: requestedDate,
                    number1: this.adtcount,
                    number2: this.chdcount,
                    number3: this.infcount,
                    text1:this.message
                };
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var cmsURL = huburl + portno + '/cms/data';      
                
                let responseObject = await this.cmsRequestData("POST", "cms/data", requestObject, null);
                var emailApi = ServiceUrls.emailServices.emailApi;
                sendMailService(emailApi, postData);
                sendMailService(emailApi, custmail);
                alertify.alert('Book package', 'Thank you for booking !');
            }



        },
        setclaneder: function () {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            $("#traveldate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    // $(this).parents('.txt_animation').addClass('focused');
                }
            });
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        }

    },
    mounted: function () {

        this.getPagecontent();
        var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
        $("#traveldate").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: 1,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            onSelect: function (selectedDate) {
                // $(this).parents('.txt_animation').addClass('focused');
            }
        });


    },
    updated: function () {
        var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
        var self = this;
        if (!this.bookdate) {
            $("#traveldate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    self.bookdate = selectedDate;
                }
            });
        }
    }
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}


