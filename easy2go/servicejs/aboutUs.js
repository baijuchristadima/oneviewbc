const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var aboutus = new Vue({
    i18n,
    el: '#aboutuspage',
    name: 'aboutuspage',
    data: {
        content: null,
        getdata: false,
        offerdata: false,       
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        aboutUsContent: {
            Title: '',
            Home: '',
            Subtitle1: '',
            Description1: '',
            Subtitle2: '',
            Description2: '',
            Image: '',
            Subtitle3: '',
            Description3: '',
            Background: ''
        },
        offerContent: {
            title: '',
            Subtitle: '',
            description: '',
            link: '',
            button: ''
        }
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                 
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/About Us page/About Us page/About Us page.ftl';

                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(async function (response) {
                    self.content = response.data;
                    var Aboutcontent = self.pluck('About_us', self.content.area_List);
                    if (Aboutcontent.length > 0) {
                        self.aboutUsContent.Title = self.pluckcom('Title', Aboutcontent[0].component);
                        self.aboutUsContent.Home = self.pluckcom('Home_breadcrumb_label', Aboutcontent[0].component);
                        self.aboutUsContent.Subtitle1 = self.pluckcom('Subtitle_1', Aboutcontent[0].component);
                        self.aboutUsContent.Description1 = self.pluckcom('Description1', Aboutcontent[0].component);
                        self.aboutUsContent.Subtitle2 = self.pluckcom('Subtitle2', Aboutcontent[0].component);
                        self.aboutUsContent.Description2 = self.pluckcom('Description2', Aboutcontent[0].component);
                        self.aboutUsContent.Image = self.pluckcom('Image', Aboutcontent[0].component);
                        self.aboutUsContent.Subtitle3 = self.pluckcom('Subtitle3', Aboutcontent[0].component);
                        self.aboutUsContent.Description3 = self.pluckcom('Description3', Aboutcontent[0].component);
                        self.aboutUsContent.Background = self.pluckcom('Background_image', Aboutcontent[0].component);
                    }
                    var Offerdata = self.pluck('Offer_section', self.content.area_List);
                    if (Offerdata.length > 0) {
                        self.offerdata=self.pluckcom('Status', Offerdata[0].component);
                        self.offerContent.title = self.pluckcom('Title', Offerdata[0].component);
                        self.offerContent.Subtitle = self.pluckcom('Subtitle', Offerdata[0].component);
                        self.offerContent.description = self.pluckcom('Description', Offerdata[0].component);
                        self.offerContent.link = self.pluckcom('Button_link', Offerdata[0].component);
                        self.offerContent.button = self.pluckcom('Button_label', Offerdata[0].component);


                    }
                   
                    self.getdata = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });


            });

        },async getAllMapData(contentArry){
            var tempDataObject = {};
            if(contentArry!=undefined){
                contentArry.map(function (item) {
                    let allKeys=Object.keys(item)
                    for(let j=0;j<allKeys.length;j++){
                        let key=allKeys[j];
                        let value= item[key];
                        
                        if(key!='name' && key!='type'){
                            if(value==undefined||value==null){
                                value="";
                            }
                            tempDataObject[key]=value;
                        }
                    }
                });
            }
            return tempDataObject;
        }
    },
    mounted: function () {
        this.getPagecontent();
    },
});