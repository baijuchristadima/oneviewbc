generalInformation = {
    systemSettings: {
        calendarDisplay: 1,

    },
};
const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var flightserchfromComponent = Vue.component('flightserch', {
    data() {
        return {
            access_token: '',
            KeywordSearch: '',
            resultItemsarr: [],
            autoCompleteProgress: false,
            highlightIndex: 0
        }
    },
    props: {
        itemText: String,
        itemId: String,
        placeHolderText: String,
        returnValue: Boolean,
        id: { type: String, default: '', required: false },
        leg: Number,
        //KeywordSearch:String
    },

    template: `<div class="autocomplete">
        <input type="text" :placeholder="placeHolderText" :id="id" :data-aircode="KeywordSearch" autocomplete="off" 
        v-model="KeywordSearch" class="cont_txt01" 
        :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !autoCompleteProgress  }" 
        @input="onSelectedAutoCompleteEvent(KeywordSearch)"
            @keydown.down="down"
            @keydown.up="up"
            @keydown.tab="autoCompleteProgress=false"
            @keydown.esc="autoCompleteProgress=false"
            @keydown.enter="onSelected(resultItemsarr[highlightIndex])"/>
        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>        
        <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length>0">
            <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item)">
                {{ item.label }}
            </li>
        </ul>
    </div>`,


    methods: {
        up: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex < this.resultItemsarr.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
            if (this.$refs.options[this.highlightIndex]) {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            }

        },
        onSelectedAutoCompleteEvent: _.debounce(async function (keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
              this.autoCompleteProgress = true;
              self.resultItemsarr = await getFlightResultUsingElasticSearch(keywordEntered);

            } else {
                this.autoCompleteProgress = false;
                this.resultItemsarr = [];

            }
        }, 100),
        onSelected: function (item) {
            this.KeywordSearch = item.label;
            this.resultItemsarr = [];
            var targetWhenClicked = $(event.target).parent().parent().find("input").attr("id");
            if (maininstance.triptype != 'M') {
                if (event.target.id == "Departurefrom" || targetWhenClicked == "Departurefrom") {
                    $('#DepartureTo').focus();
                } else if (event.target.id == "DepartureTo" || targetWhenClicked == "DepartureTo") {
                    $('#deptDate01').focus();
                }
            } else {
                if (event.target.id == "Departurefrom" || targetWhenClicked == "Departurefrom") {
                    $('#DepartureTo').focus();
                } else if (event.target.id == "DepartureTo" || targetWhenClicked == "DepartureTo") {
                    $('#deptDate01').focus();
                } else if (event.target.id == "DeparturefromLeg2" || targetWhenClicked == "DeparturefromLeg2") {
                    $('#ArrivalfromLeg2').focus();
                } else if (event.target.id == "ArrivalfromLeg2" || targetWhenClicked == "ArrivalfromLeg2") {
                    $('#txtLeg2Date').focus();
                } else if (event.target.id == "DeparturefromLeg3" || targetWhenClicked == "DeparturefromLeg3") {
                    $('#ArrivalfromLeg3').focus();
                } else if (event.target.id == "ArrivalfromLeg3" || targetWhenClicked == "ArrivalfromLeg3") {
                    $('#txtLeg3Date').focus();
                } else if (event.target.id == "DeparturefromLeg4" || targetWhenClicked == "DeparturefromLeg4") {
                    $('#ArrivalfromLeg4').focus();
                } else if (event.target.id == "ArrivalfromLeg4" || targetWhenClicked == "ArrivalfromLeg4") {
                    $('#txtLeg4Date').focus();
                } else if (event.target.id == "DeparturefromLeg5" || targetWhenClicked == "DeparturefromLeg5") {
                    $('#ArrivalfromLeg5').focus();
                } else if (event.target.id == "ArrivalfromLeg5" || targetWhenClicked == "ArrivalfromLeg5") {
                    $('#txtLeg5Date').focus();
                } else if (event.target.id == "DeparturefromLeg6" || targetWhenClicked == "DeparturefromLeg6") {
                    $('#ArrivalfromLeg6').focus();
                } else if (event.target.id == "ArrivalfromLeg6" || targetWhenClicked == "ArrivalfromLeg6") {
                    $('#txtLeg6Date').focus();
                }

            }

            this.$emit('air-search-completed', item.code, item.label, this.leg);


        }
    }


});

var maininstance = new Vue({
    i18n,
    el: '#indexmain',
    name: 'indexmain',
    data: {
        content: {
            area_List: []
        },
        getdata: true,
        getservice: false,
        triptype: "O",
        placeholderfrom: "Enter Airport or City",
        placeholderTo: "Enter Airport or City",
        placeholderDepartureDate: "DD/MM/YYYY",
        placeholderReturnDate: "DD/MM/YYYY",
        Oneway: "",
        Round_trip: "",
        Multicity: "",
        Advance_search_label: "",
        preferred_airline_placeholder: "",
        CityFrom: '',
        CityTo: '',
        validationMessage: '',
        adtcount: 1,
        chdcount: 0,
        infcount: 0,
        preferAirline: '',
        returnValue: true,
        legcount: 1,
        legs: [],
        cityList: [],
        adt: "adult",
        adults: [

            { 'value': 1, 'text': '1 Adult' },
            { 'value': 2, 'text': '2 Adults' },
            { 'value': 3, 'text': '3 Adults' },
            { 'value': 4, 'text': '4 Adults' },
            { 'value': 5, 'text': '5 Adults' },
            { 'value': 6, 'text': '6 Adults' },
            { 'value': 7, 'text': '7 Adults' },
            { 'value': 8, 'text': '8 Adults' },
            { 'value': 9, 'text': '9 Adults' }
        ],
        children: [
            { 'value': 0, 'text': 'Children (2-11 yrs)' },
            { 'value': 1, 'text': '1 Child' },
            { 'value': 2, 'text': '2 Children' },
            { 'value': 3, 'text': '3 Children' },
            { 'value': 4, 'text': '4 Children' },
            { 'value': 5, 'text': '5 Children' },
            { 'value': 6, 'text': '6 Children' },
            { 'value': 7, 'text': '7 Children' },
            { 'value': 8, 'text': '8 Children' }

        ],
        infants: [
            { 'value': 0, 'text': 'Infants (0-1 yr)' },
            { 'value': 1, 'text': '1 Infant' }

        ],
        cabinclass: [{ 'value': 'Y', 'text': 'Economy' },
        { 'value': 'C', 'text': 'Business' },
        { 'value': 'F', 'text': 'First' }
        ],
        selected_cabin: 'Y',
        selected_adult: 1,
        selected_child: 0,
        selected_infant: 0,
        totalAllowdPax: 9,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        Banner:{Banner_Image:''},
        Packages:[]

    },
    methods: {
        Departurefrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (AirportCode == this.CityTo) {
                    this.returnValue = false;
                    this.validationMessage = "Departure and arrival airports should not be same !";
                    var self = this;
                    this.CityFrom = '';
                    setTimeout(function () {
                        self.validationMessage = '';
                    }, 1500);
                } else {
                    this.returnValue = true;
                    this.CityFrom = AirportCode;
                }
            } else {
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    this.cityList[index].from = AirportCode
                } else {
                    this.cityList.push({
                        id: leg,
                        from: AirportCode,
                        to: ''

                    });
                }
            }


        },
        Arrivalfrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (this.CityFrom == AirportCode) {
                    this.returnValue = false;
                    this.validationMessage = "Departure and arrival airports should not be same !";
                    this.CityTo = '';
                    var self = this;
                    setTimeout(function () {
                        self.validationMessage = '';
                    }, 1500);
                } else {
                    this.returnValue = true;
                    this.CityTo = AirportCode;
                }
            } else {
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    this.cityList[index].to = AirportCode
                } else {
                    this.cityList.push({
                        id: leg,
                        from: '',
                        to: AirportCode

                    });
                }
            }
        },
        SearchFlight: function () {

            var Departuredate = $('#deptDate01').val() == "" ? "" : $('#deptDate01').datepicker('getDate');
            if (!this.CityFrom) {
                this.validationMessage = "Please fill origin !";
                var self = this;
                setTimeout(function () {
                    self.validationMessage = '';
                }, 1500);
                return false;
            }
            if (!this.CityTo) {
                this.validationMessage = "Please fill destination !";
                var self = this;
                setTimeout(function () {
                    self.validationMessage = '';
                }, 1500);
                return false;
            }
            if (!Departuredate) {
                this.validationMessage = "Please choose departure date !";
                var self = this;
                setTimeout(function () {
                    self.validationMessage = '';
                }, 1500);
                return false;
            }
            var sec1TravelDate = moment(Departuredate).format('DD|MM|YYYY');

            var sectors = this.CityFrom + '-' + this.CityTo + '-' + sec1TravelDate;
            if (this.triptype == 'R') {
                var ArrivalDate = $('#retDate').val() == "" ? "" : $('#retDate').datepicker('getDate');
                if (!isNullorUndefined(ArrivalDate)) {
                    ArrivalDate = moment(ArrivalDate).format('DD|MM|YYYY');
                    if (!ArrivalDate) {
                        this.validationMessage = "Please choose return date !";
                        var self = this;
                        setTimeout(function () {
                            self.validationMessage = '';
                        }, 1500);
                        return false;
                    } else {
                        sectors += '/' + this.CityTo + '-' + this.CityFrom + '-' + ArrivalDate;
                    }
                } else {
                    this.validationMessage = "Please choose return date !";
                    var self = this;
                    setTimeout(function () {
                        self.validationMessage = '';
                    }, 1500);
                    return false;
                }
            }
            if (this.triptype == 'M') {
                for (var legValue = 1; legValue <= this.legcount; legValue++) {
                    var temDeparturedate = $('#txtLeg' + (legValue + 1) + 'Date').val() == "" ? "" : $('#txtLeg' + (legValue + 1) + 'Date').datepicker('getDate');
                    if (temDeparturedate != "" && this.cityList[legValue - 1].from != "" && this.cityList[legValue - 1].to != "") {
                        var departureFrom = this.cityList[legValue - 1].from;
                        var arrivalTo = this.cityList[legValue - 1].to;
                        var travelDate = moment(temDeparturedate).format('DD|MM|YYYY');
                        sectors += '/' + departureFrom + '-' + arrivalTo + '-' + travelDate;
                    } else {
                        this.validationMessage = "Please fill the Trip " + (legValue + 1) + "   fields !";
                        var self = this;
                        setTimeout(function () {
                            self.validationMessage = '';
                        }, 1500);
                        return false;
                    }
                }
            }
            if ($('#checkbox-list-6').is(':checked')) {
                var directFlight = "DF";
            } else {
                var directFlight = "AF";
            }
            var adult = this.selected_adult;
            var child = this.selected_child;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            var searchUrl = '/Flights/flight-listing.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-all-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
            // searchUrl = searchUrl.toLocaleLowerCase();
            window.location.href = searchUrl;
        },
        AddNewLeg() {
            if (this.legcount <= 5) {
                ++this.legcount;
                var legno = 1;
                if (this.cityList.length > 0) {
                    legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                    legno = legno + 1;
                }

                this.cityList.push({
                    id: legno,
                    from: '',
                    to: ''
                });

            }
            console.log(this.cityList);
        },
        DeleteLeg(leg) {
            var legs = this.legcount;
            if (legs > 1) {
                --this.legcount;
                var legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    this.cityList.splice(index, 1);

                }


            }
        },
        setCalender() {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = generalInformation.systemSettings.calendarDisplay;
            $("#deptDate01").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);

                }
            });

            $("#retDate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) { }
            });

            $("#txtLeg2Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg3Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg2Date").val();
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg4Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg3Date").val();
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg5Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg4Date").val();
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg6Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg5Date").val();
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) { }
            });


        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },

        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = commonPath + Agencycode + '/Template/Home Page/Home Page/Home Page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (res) {
                    console.log(res);

                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var Banner = self.pluck('Index_Section', self.content);
                        self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
                    }

                });

            });
        },
        getPackage: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';
                axios.get(topackageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    var packageData = response.data;
                    let PackageListTemp = [];
                    if (packageData != undefined && packageData.Values != undefined) {
                        PackageListTemp = packageData.Values.filter(function (el) {
                            return el.Status == true
                        });
                    }
                    self.Packages = PackageListTemp;
                }).catch(function (error) {
                    console.log('Error');
                });
            });
        },
        onKeyUpAutoCompleteEvent: _.debounce(function (event) {
            var self = this;
            let searchResult = [];

            let keywordEntered = "";
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                keywordEntered = event.target.value;
            }

            if (keywordEntered.length > 2) {


                for (let i = 0; i < this.allPackages.length; i++) {
                    let Destination = this.allPackages[i].Destination;
                    if ((this.allPackages[i].Holiday_Package == true || this.allPackages[i].Special_Package == true) &&
                        this.allPackages[i].Status == true &&
                        Destination != undefined &&
                        Destination.toLowerCase().includes(keywordEntered.toLowerCase())) {
                        searchResult.push(this.allPackages[i]);
                    }

                }


            }
            var resArr = [];
            searchResult.filter(function (item) {
                var i = resArr.findIndex(function (x) { return x.Destination.trim() == item.Destination.trim() });
                if (i <= -1) {
                    resArr.push(item);
                }
                return null;
            });

            this.packageSearchResult = resArr;

        }, 100),
        onAdultChange: function () {
            alert(this.selected);
            //this.disp = this.selected;
        },
        onCheck: function (e) {
            var currid = $(e.target).attr('id');
             
            console.log(currid);
            var selectedDate = $("#deptDate01").val();
            $("#retDate").datepicker("option", "minDate", selectedDate);
            $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
        },
        getmoreinfo(url, type) {
            if (type == undefined || type == '') {
                type = "pkg";
            }
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.');
                    url = url.split('/')[url.split('/').length - 1];
                    url = "/Nirvana/packageInfo.html?page=" + url + "&from=" + type;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;
        },
    },
    mounted: function () {
        this.setCalender();
        this.getPagecontent();
        this.getPackage();
        sessionStorage.active_e=1;
    },
    updated: function () {
        var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
        var numberofmonths = generalInformation.systemSettings.calendarDisplay;
        $("#deptDate01").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            onSelect: function (selectedDate) {
                $("#retDate").datepicker("option", "minDate", selectedDate);
                $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);

            }
        });

        $("#retDate").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#deptDate01").val();
                $("#retDate").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) { }
        });

        $("#txtLeg2Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#deptDate01").val();
                $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) {
                $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#txtLeg3Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#txtLeg2Date").val();
                $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) {
                $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#txtLeg4Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#txtLeg3Date").val();
                $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) {
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#txtLeg5Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#txtLeg4Date").val();
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) {
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#txtLeg6Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#txtLeg5Date").val();
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) { }
        });

    }
});


function isNullorUndefined(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) { status = true; }
    return status;
}

function onCabinChange(e){
    maininstance.selected_cabin = $('#cabinValue option:selected').val();
}

function onPaxChange(e) {
    var currentadult = parseInt($('#adultdrp option:selected').val());
    var currentchild = parseInt($('#childdrp option:selected').val());
    var currentinfant = parseInt($('#infantdrp option:selected').val());
    var currenttotal = parseInt(currentadult) + parseInt(currentchild);
    var child = 9 - currentadult;
    maininstance.selected_adult=currentadult;
    maininstance.children = [
        { 'value': 0, 'text': 'Children (2-11 yrs)' }
    ];
    for (i = 1; i <= child; i++) {
        maininstance.children.push({ 'value': i, 'text': i + ' Child' + (i > 1 ? 'ren' : '') });
    }
    if (currentchild > child) {
        currentchild = child;
    }
    currentchild = (currentchild < 0) ? 0 : currentchild;
    maininstance.selected_child = currentchild;
    var infant = parseInt(currentadult);
    if (infant + parseInt(currentadult) + parseInt(currentchild) > 9) {
        infant = 9 - (parseInt(currentadult) + parseInt(currentchild));
    }
    infant = ((parseInt(infant) < 0) ? 0 : infant);
    if (infant == 0) maininstance.selected_infant = 0;

    // var infant = (((currenttotal > 2 && currenttotal < 7) && (currentadult > 3)) ? 3 : ((currenttotal > 6) ? (9 - currenttotal) : currentadult));
    maininstance.infants = [
        { 'value': 0, 'text': 'Infants (0-1 yr)' }
    ];
    for (i = 1; i <= infant; i++) {
        maininstance.infants.push({ 'value': i, 'text': i + ' Infant' + (i > 1 ? 's' : '') });
    }
    if (currentinfant > infant) {
        currentinfant = infant;
    }
    currentinfant = (currentinfant < 0) ? 0 : currentinfant;
    maininstance.selected_infant = currentinfant;
}


jQuery(document).ready(function ($) {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
});