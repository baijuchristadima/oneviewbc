var hotelList = new Vue({
    el: '#hotels',
    name: 'hotels',
    data: {
        cityName: '',
        occupancyType: '',
        locationHotels: [],
        OccupancyHotels: [],
        Banner:{Banner_Image:''},
        fromDate: '',
        toDate: '',
        labels: {
            Adult_Label: "Adult",
            Child_Label: "Child",
            Infant_Label: "Infant",
            Traveler_Placeholder: "Guest",
            City_Placeholder: "City",
            Room_Label: "Room",
            Child_Age_Label: "(2-12 yrs)",
            Adult_Age_Label: "(12+ yrs)",
        }
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getBanner: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = commonPath + Agencycode + '/Template/Home Page/Home Page/Home Page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (res) {
                    console.log(res);

                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var Banner = self.pluck('Index_Section', self.content);
                        self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
                    }

                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });
            });
        },

        getPackage: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var tohotelurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Hotel/Hotel/Hotel.ftl';
                axios.get(tohotelurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    var packageData = response.data;
                    let PackageListTemp = [];
                    if (packageData != undefined && packageData.Values != undefined) {
                        PackageListTemp = packageData.Values.filter(function (el) {
                            return el.Status == true
                        });

                        PackageListTemp.forEach(function (item, index) {
                            self.locationHotels.push(item.Location.toUpperCase().trim());
                            self.OccupancyHotels.push(item.Room_Occupancy.toUpperCase().trim());
                        });
                        self.$nextTick(function () {

                            self.locationHotels = _.uniq(_.sortBy(self.locationHotels));
                            self.OccupancyHotels = _.uniq(_.sortBy(self.OccupancyHotels));
                        });

                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });
        },
        dateChecker: function () {
            let dateObj1 = document.getElementById("from1");
            let dateValue1 = dateObj1.value;
            let dateObj2 = document.getElementById("to1");
            let dateValue2 = dateObj2.value;
            if (dateValue2 !== undefined || dateValue2 !== '') {
                // document.getElementById("to1").value = '';
                $('#to1').val('').trigger('change');
                $("#to1").val("");
                return false;
            }
        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {

                if (type == 'from1') {
                    this.fromDate = event.target.value;
                } else if (type == 'to1') {
                    this.toDate = event.target.value;
                } else if (type == 'city') {
                    this.cityName = event.target.value;
                } else if (type == 'occupancy') {
                    this.occupancyType = event.target.value;
                }
            }
        },
        searchHotels: function () {
            if (this.cityName == null || this.cityName.trim() == "") {
                alertify.alert('Alert', 'Please select a city.');
            } else if (this.occupancyType == null || this.occupancyType.trim() == "") {
                alertify.alert('Alert', 'Please select occupancy.');
            } else if (this.fromDate == null || this.fromDate.trim() == "") {
                alertify.alert('Alert', 'Please select Check In Date.');
            } else if (this.toDate == null || this.toDate.trim() == "") {
                alertify.alert('Alert', 'Please select Check Out Date.');
            } else {
                var searchCriteria =
                    "&city=" + this.cityName +
                    "&cin=" + this.fromDate +
                    "&cout=" + this.toDate +
                    "&occupancy=" + this.occupancyType + "&";
                searchCriteria = searchCriteria.split(' ').join('-');
                var uri = "/ICA/hotel-results.html?" + searchCriteria;
                window.location.href = uri;
            }

        },
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#from1").datepicker({
                // minDate: "0d",
                // maxDate: "360d",
                minDate: new Date('11/15/2020'),
                maxDate: new Date('11/20/2020'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {direction: "up"},
                showButtonPanel: false,
                dateFormat: dateFormat,
            });

            $("#to1").datepicker({
                minDate: new Date('11/15/2020'),
                maxDate: new Date('11/20/2020'),
                numberOfMonths: 1,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {direction: "up"},
                showButtonPanel: false,
                dateFormat: dateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#from1").val();
                    $("#to1").datepicker("option", "minDate", selectedDate);
                }
            });
        }

    },

    mounted: function () {
        localStorage.removeItem("backUrl");

        this.getBanner();
        this.setCalender();
        this.getPackage();
        sessionStorage.active_e=2;
        var vm = this;
        this.$nextTick(function () {
            $('#from1').on("change", function (e) {
                vm.dropdownChange(e, 'from1')
            });
            $('#to1').on("change", function (e) {
                vm.dropdownChange(e, 'to1')
            });
            $('#city').on("change", function (e) {
                vm.dropdownChange(e, 'city')
            });
            $('#occupancy').on("change", function (e) {
                vm.dropdownChange(e, 'occupancy')
            });
        })
    },
});
jQuery(document).ready(function ($) {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
});
