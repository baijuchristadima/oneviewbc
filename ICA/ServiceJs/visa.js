var visa = new Vue({
  el: '#visapage',
  name: 'visapage',
  data: {
    visaDetails: {
      Banner_Image: '',
      Title: '',
      Price: '',
      Document_Requirement_Title: '',
      Document_Requirements: '',
      Description_Title: '',
      Description: ''
    },
    name: '',
    phone: '',
    mail: '',
    visaType: '',
    file: null,
    file2: null,
    fileLink: '',
    fileLink2: '',
    fileLink3: '',
    customFile: '',
    passport: '',
    uploadflag: false,
    uploadimg: false,
    terms:'',
    visaSection:{},
    LabelSection:{}
  },
  methods: {
    getPagecontent: function () {
      var self = this;

      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        // self.dir = langauage == "ar" ? "rtl" : "ltr";
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Visa Page/Visa Page/Visa Page.ftl';

        axios.get(pageurl, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          if (response.data.area_List.length > 0) {
            var visa = pluck('Visa_Section', response.data.area_List);
            self.visaSection = getAllMapData(visa[0].component);

            var label = pluck('Page_Labels', response.data.area_List);
            self.LabelSection = getAllMapData(label[0].component);
  

            self.isLoading = false;
        }
          // var visaDetails = self.pluck('Visa_Section', self.content.area_List);
          // if (visaDetails.length > 0) {
          //   self.visaDetails.Banner_Image = self.pluckcom('Banner_Image', visaDetails[0].component);
          //   self.visaDetails.Title = self.pluckcom('Title', visaDetails[0].component);
          //   self.visaDetails.Price = self.pluckcom('Price', visaDetails[0].component);
          //   self.visaDetails.Document_Requirement_Title = self.pluckcom('Document_Requirement_Title', visaDetails[0].component);
          //   self.visaDetails.Document_Requirements = self.pluckcom('Document_Requirements', visaDetails[0].component);
          //   self.visaDetails.Description_Title = self.pluckcom('Description_Title', visaDetails[0].component);
          //   self.visaDetails.Description = self.pluckcom('Description', visaDetails[0].component);
          // }

          // self.getdata = true;

        }).catch(function (error) {
          console.log('Error');
          self.aboutUsContent = [];
        });
      });
    },
    sendvisa: function () {
      let fileObj1 = document.getElementById("photo");
      let fileImg = fileObj1.value;
      let fileObj2 = document.getElementById("passport");
      let filePDF = fileObj2.value;
      let fileObj3 = document.getElementById("national-id");
      let fileID = fileObj3.value;
      if (!this.visaType) {
        alertify.alert('Alert', 'Please select visa type to proceed.').set('closable', false);

        return false;
      }
      if (!this.name) {
        alertify.alert('Alert', 'Name required.').set('closable', false);

        return false;
      }
      if (this.mail == null) {
        alertify.alert('Alert', 'Email Required.').set('closable', false);
        return false;
      }
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.mail.match(emailPat);
      if (matchArray == null) {
        alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
        return false;
      }
      if (this.phone == null) {
        alertify.alert('Alert', 'Mobile number required.').set('closable', false);
        return false;
      }
      var phoneno = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
      // var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
      var matchphone = this.phone.match(phoneno);
      if (matchphone == null) {
        alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
        return false;
      }
      if (this.phone.length < 8) {
        alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
        return false;
      }
      // if (!this.message) {
      //   alertify.alert('Alert', 'Message required.').set('closable', false);
      //   return false;
      // }
      if (filePDF == undefined || filePDF == '') {
        alertify.alert('Alert', 'Upload Passport Copy.').set('closable', false);
        return false;
      }
      if (fileImg == undefined || fileImg == '') {
        alertify.alert('Alert', 'Upload Passport Photo.').set('closable', false);
        return false;
      }
      
      if (fileID == undefined || fileID == '') {
        alertify.alert('Alert', 'Upload National ID Details.').set('closable', false);
        return false;
      }
      if (!this.fileLink) {
        alertify.alert('Alert', 'Please Wait Image is uploading. Or Choose Another File').set('closable', false);
        return false;
      }
      if (!this.fileLink2) {
        alertify.alert('Alert', 'Please Wait Passport is uploading.Or Choose Another File').set('closable', false);
        return false;
      }
      if (!this.fileLink3) {
        alertify.alert('Alert', 'Please Wait National ID is uploading.Or Choose Another File').set('closable', false);
        return false;
      }
      if (!this.terms) {
        alertify.alert('Alert', 'Please Agree Terms and Condtions').set('closable', false);
        return false;
      }
      else {
        var fromEmail = JSON.parse(localStorage.User).loginNode.email;
        var toEmail = JSON.parse(localStorage.User).emailId;
        var postData = {
          type: "VisaAdmin",
          fromEmail:fromEmail,
          toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
          ccEmails: [],
          bccEmails: [],
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary,
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.name,
          emailAddress: this.mail,
          contact: this.phone,
          departureDate:moment(String(new Date())).format("YYYY-MM-DD"),
          message: ""
        };
        var frommail = JSON.parse(localStorage.User).loginNode.email;
        var custmail = {
          type: "UserAddedRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(this.mail) ? this.mail : [this.mail],
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.name,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        var sendingRequest = {
          type: "Visa Request",
          keyword1: this.name,
          keyword2: this.mail,
          keyword3: this.phone,
          keyword4: this.visaType,
          text11: this.fileLink,
          text12: this.fileLink2,
          date1: requestedDate,
          text13: this.fileLink3,
          nodeCode: agencyCode
        };
        let responseObject = this.cmsRequestData("POST", "cms/data", sendingRequest, null);
        try {
          let insertID = Number(responseObject);
          var emailApi = ServiceUrls.emailServices.emailApi;
          sendMailService(emailApi, postData);
          sendMailService(emailApi, custmail);
          this.mail = '';
          this.name = '';
          this.phone = '';
          this.message = '';
          // $('#customFile').val('').trigger('change');
          // $('#customFile2').val('').trigger('change');


          $(".custom-file-input").siblings(".custom-file-label").removeClass("selected").html("Choose Your Photo");
          $(".custom-file-input2").siblings(".custom-file-label2").removeClass("selected").html("Choose Your Passport");


          // document.getElementById("customFile").value = null;
          // document.getElementById("customFile2").value = null;
          alertify.alert('Success', 'Thank you for contacting us.We shall get back to you.');
          setTimeout(function () { window.location.reload() }, 1000);
        } catch (e) {

        }
      }
    },
    cmsRequestData: function (callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      return fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: { 'Content-Type': 'application/json' },
        body: data, // body data type must match "Content-Type" header
      }).then(function (response) {
        return response.json();
      });
    },
    handleFilePhoto() {
      var self = this;
      file_temp = self.$refs.file.files[0];
      var ext = file_temp.name.split('.')[1];
      if (ext == "jpg" || ext == "png" || ext == "PNG" || ext == "JPEG" || ext == "jpeg") {
        self.file = self.$refs.file.files[0];
        if (self.file !== undefined) {

          // var fileName = $(this).val().split("\\").pop();
          $(".custom-file-input").siblings(".custom-file-label").addClass("selected").html(self.file.name);


          self.submitPhoto()
        }
      } else {
        alertify.alert("Warning", "Please select valid File");
        // $("#resume").val(null);
        self.file = null;
      }
      // console.log('>>>> 1st element in files array >>>> ', this.file);
    },
    submitPhoto() {
      var self = this;
      self.uploadimg = true;
      if (self.file != null && self.file != undefined && !jQuery.isEmptyObject(self.file)) {
        $.getJSON('credentials/AgencyCredentials.json', function (json) {
          let Password = json.file_upload_ICA[0].Password;
          // var Password = "6Yhn7Ujm^";
          var LoggedUser = window.localStorage.getItem("AgencyCode");
          // var LoggedUser = "AGY20483";
          // var UserName=LoggedUser.loginNode.code;	
          var encodedString = btoa(LoggedUser + ":" + Password);
          var formData = new FormData();
          formData.append('file', self.file);
          // console.log('>> formData >> ', formData);
          var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "visa"
          // You should have a server side REST API 
          axios.post(url,
            formData, {
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'multipart/form-data',
              'Authorization': 'Basic ' + encodedString
            }
          }
          ).then(function (repo) {
            console.log('SUCCESS!!', repo.data.message);
            self.fileLink = repo.data.message;
            if (self.fileLink) {
              self.uploadimg = false;
            }
            // alertify.alert("Success", "Image Successfully uploaded.");
            self.file = null;
          })
            .catch(function () {
              self.uploadimg = false;
              console.log('FAILURE!!');
              self.fileLink = null;
            });
        });
      } else {
        alertify.alert("warning", "please select the file")
      }
    },
    handleFilePassport() {
      var self = this;
      file_temp = self.$refs.file2.files[0];
      var ext = file_temp.name.split('.')[1];
      if (ext == "pdf" || ext == "docx" || ext == "doc" || ext == "jpg" || ext == "png" || ext == "PNG" || ext == "JPEG" || ext == "jpeg") {
        self.file2 = self.$refs.file2.files[0];
        if (self.file2) {
          // var fileName = $(this).val().split("\\").pop();
          $(".custom-file-input2").siblings(".custom-file-label2").addClass("selected").html(self.file2.name);
          self.submitPassport()
        }
      } else {
        alertify.alert("Warning", "Please select valid File");
        // $("#resume").val(null);
        self.file2 = null;
      }
      // console.log('>>>> 1st element in files array >>>> ', this.file);
    },
    submitPassport() {
      var self = this;
      self.uploadflag = true;
      if (self.file2 != null && self.file2 != undefined && !jQuery.isEmptyObject(self.file2)) {
        $.getJSON('credentials/AgencyCredentials.json', function (json) {
          let Password = json.file_upload_ICA[0].Password;
          // var Password = "6Yhn7Ujm^";
          var LoggedUser = window.localStorage.getItem("AgencyCode");
          // var LoggedUser = "AGY20483";
          // var UserName=LoggedUser.loginNode.code;	
          var encodedString = btoa(LoggedUser + ":" + Password);
          var formData = new FormData();
          formData.append('file', self.file2);
          // console.log('>> formData >> ', formData);
          var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "passport"
          // You should have a server side REST API 
          axios.post(url,
            formData, {
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'multipart/form-data',
              'Authorization': 'Basic ' + encodedString
            }
          }
          ).then(function (repo) {
            console.log('SUCCESS!!', repo.data.message);
            self.fileLink2 = repo.data.message;
            if (self.fileLink2) {
              self.uploadflag = false;
            }
            // alertify.alert("Success", "Passport Successfully uploaded.Now Submit");
            self.file2 = null;
          })
            .catch(function () {
              self.uploadflag = false;
              console.log('FAILURE!!');
              self.fileLink2 = null;
            });
        });
      } else {
        alertify.alert("Warning", "please select the file")
      }
    },
    handleFileId() {
      var self = this;
      file_temp = self.$refs.file3.files[0];
      var ext = file_temp.name.split('.')[1];
      if (ext == "jpg" || ext == "png" || ext == "PNG" || ext == "JPEG" || ext == "jpeg" || ext == "pdf" || ext == "docx" || ext == "doc") {
        self.file3 = self.$refs.file3.files[0];
        if (self.file !== undefined) {

          // var fileName = $(this).val().split("\\").pop();
          $(".custom-file-input").siblings(".custom-file-label").addClass("selected").html(self.file3.name);


          self.submitId()
        }
      } else {
        alertify.alert("Warning", "Please select valid File");
        // $("#resume").val(null);
        self.file = null;
      }
      // console.log('>>>> 1st element in files array >>>> ', this.file);
    },
    submitId() {
      var self = this;
      self.uploadimg = true;
      if (self.file3 != null && self.file3 != undefined && !jQuery.isEmptyObject(self.file3)) {
        $.getJSON('credentials/AgencyCredentials.json', function (json) {
          let Password = json.file_upload_ICA[0].Password;
          // var Password = "6Yhn7Ujm^";
          var LoggedUser = window.localStorage.getItem("AgencyCode");
          // var LoggedUser = "AGY20483";
          // var UserName=LoggedUser.loginNode.code;	
          var encodedString = btoa(LoggedUser + ":" + Password);
          var formData = new FormData();
          formData.append('file', self.file3);
          // console.log('>> formData >> ', formData);
          var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "Idcard"
          // You should have a server side REST API 
          axios.post(url,
            formData, {
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'multipart/form-data',
              'Authorization': 'Basic ' + encodedString
            }
          }
          ).then(function (repo) {
            console.log('SUCCESS!!', repo.data.message);
            self.fileLink3 = repo.data.message;
            if (self.fileLink3) {
              self.uploadimg = false;
            }
            // alertify.alert("Success", "Image Successfully uploaded.");
            self.file3 = null;
          })
            .catch(function () {
              self.uploadimg = false;
              console.log('FAILURE!!');
              self.fileLink3 = null;
            });
        });
      } else {
        alertify.alert("warning", "please select the file")
      }
    },
    dropdownChange: function (event, type) {
      if (event != undefined && event.target != undefined && event.target.value != undefined) {

        if (type == 'VisaType') {
          this.visaType = event.target.value;

        }

      }
    }
  },

  mounted: function () {
    this.getPagecontent();
    sessionStorage.active_e = 5;
    var vm = this;
    this.$nextTick(function () {
      $('#Visaselect').on("change", function (e) {
        vm.dropdownChange(e, 'VisaType')
      });
    })

  },
})