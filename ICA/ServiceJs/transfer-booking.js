var maininstance = new Vue({
    el: '#transfer-booking',
    name: 'transfer-booking',
    data: {
        content: {
            area_List: []
        },
        Transfer: {
            Description_Title: null,
            Destination_From: null
        },
        Traveler_Details: {
            Terms_and_Conditions: null
        },
        pageURLLink: null,
        travellerInfo: {
            firstName: null,
            lastName: null,
            email: null,
            contactNumber: null
        },
        siteData: null,
        searchCriteria: {},
        checked: false,
        // historyData: '',
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                packageurl = getQueryStringValue('page');
                if (packageurl != "") {
                    packageurl = packageurl.split('-').join(' ');
                    packageurl = packageurl.split('_')[0];
                    self.pageURLLink = packageurl;
                    var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
                    var siteData = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Transfer Booking Page/Transfer Booking Page/Transfer Booking Page.ftl';
                    var searchData = self.getSearchData();
                    // self.historyData = searchData;
                    if (searchData != "" || searchData != null) {
                        searchData.hour = searchData.hour.trim();
                        searchData.date = searchData.date.trim();
                        searchData.min = searchData.min.trim();
                        searchData.guest = searchData.guest.trim();
                        self.searchCriteria = Object.assign({}, searchData);
                        localStorage.setItem("backUrl",self.yourCallBackFunction(self.searchCriteria));
                        axios.get(topackageurl, {
                            headers: {
                                'content-type': 'text/html',
                                'Accept': 'text/html',
                                'Accept-Language': langauage
                            }
                        }).then(function (response) {
                            self.content = response.data;
                            var pagecontent = self.pluck('Transfers_Details_Section', self.content.area_List);
                            if (pagecontent.length > 0) {
                                self.Transfer.Destination_From = self.pluckcom('Destination_From', pagecontent[0].component);
                                self.Transfer.Destination_To = self.pluckcom('Destination_To', pagecontent[0].component);
                                self.Transfer.Vehicle_Name = self.pluckcom('Vehicle_Name', pagecontent[0].component);
                                self.Transfer.Description = self.pluckcom('Description', pagecontent[0].component);
                                self.Transfer.Cancellation_Policy = self.pluckcom('Cancellation_Policy', pagecontent[0].component);
                                self.Transfer.Availability_Status = self.pluckcom('Availability_Status', pagecontent[0].component);
                                self.Transfer.Price = self.pluckcom('Price', pagecontent[0].component);
                                self.Transfer.Extra_Hour_Rate = self.pluckcom('Extra_Hour_Rate', pagecontent[0].component);
                                self.Transfer.Full_Day_Rent = self.pluckcom('Full_Day_Rent', pagecontent[0].component);
                                self.Transfer.City = self.pluckcom('City', pagecontent[0].component);
                                self.Transfer.Max_Number_of_Passengers = self.pluckcom('_Max_Number_of_Passengers', pagecontent[0].component);
                                self.Transfer.Rate_Basis = self.pluckcom('Rate_Basis', pagecontent[0].component);
                                self.Transfer.Vehicle_Image = self.pluckcom('Vehicle_Image', pagecontent[0].component);

                            }
                        }).catch(function (error) {
                            console.log("2", error)
                            window.location.href = "/ICA/transfer-booking.html";
                            self.content = [];
                        });
                    }

                    axios.get(siteData, {
                        headers: {
                            'content-type': 'text/html',
                            'Accept': 'text/html',
                            'Accept-Language': langauage
                        }
                    }).then(function (res) {
                        self.siteData = res.data;
                        var siteData = self.pluck('Transfer_Details_Section', self.siteData.area_List);
                        if (siteData.length > 0) {
                            self.Transfer.Description_Title = self.pluckcom('Description_Title', siteData[0].component);
                            self.Transfer.Cancellation_Policy_Title = self.pluckcom('Cancellation_Policy_Title', siteData[0].component);
                        }
                        var Traveler = self.pluck('Traveler_Details_Section', self.siteData.area_List);
                        if (Traveler.length > 0) {
                            self.Traveler_Details.Terms_and_Conditions = self.pluckcom('Terms_and_Conditions', Traveler[0].component);
                            self.Traveler_Details.Description = self.pluckcom('Description', Traveler[0].component);
                        }

                    }).catch(function (error) {
                        console.log("1", error)
                        window.location.href = "/ICA/transfer-booking.html";
                        self.siteData = [];
                    });


                }
            });
        },
        bookingDeatils: function () {

            if (!this.travellerInfo.firstName) {
                alertify.alert('Alert', 'First Name required.').set('closable', false);
                return false;
            }
            if (!this.travellerInfo.lastName) {
                alertify.alert('Alert', 'Last Name required.').set('closable', false);
                return false;
            }
            if (this.travellerInfo.email == null) {
                alertify.alert('Alert', 'Email Required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.travellerInfo.email.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (this.travellerInfo.contactNumber == null) {
                alertify.alert('Alert', 'Contact number required.').set('closable', false);
                return false;
            }
            if (!this.travellerInfo.contactNumber) {
                alertify.alert('Alert', 'Contact number required.').set('closable', false);
                return false;
            }
            if (this.travellerInfo.contactNumber.length < 8) {
                alertify.alert('Alert', 'Enter valid Contact number.').set('closable', false);
                return false;
            }
            if (this.checked == false) {
                alertify.alert('Alert', 'Accept cancellation policy and payment terms.').set('closable', false);
                return false;
            }
            else {
                var fromEmail = JSON.parse(localStorage.User).loginNode.email;
                var toEmail = JSON.parse(localStorage.User).emailId;
                var postData = {
                    type: "PackageBookingRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                    ccEmails: [],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    packegeName: this.Transfer.Vehicle_Name,
                    personName: this.travellerInfo.firstName,
                    emailAddress: this.travellerInfo.email,
                    contact: this.travellerInfo.contactNumber,
                    departureDate: moment(this.searchCriteria.date, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'),
                    adults: this.searchCriteria.guest,
                    child2to5: "NA",
                    child6to11: "NA",
                    infants: "NA",
                    message: "NA",
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                var frommail = JSON.parse(localStorage.User).loginNode.email;
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.travellerInfo.email) ? this.travellerInfo.email : [this.travellerInfo.email],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.travellerInfo.firstName,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                var bookDate = moment(this.searchCriteria.date, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
                var sendingRequest = {
                    type: "Transfer Booking",
                    keyword1: this.travellerInfo.firstName,
                    keyword2: this.travellerInfo.lastName,
                    keyword3: this.travellerInfo.email,
                    keyword11: this.travellerInfo.contactNumber,
                    keyword4: this.Transfer.Vehicle_Name,
                    keyword5: this.Transfer.City,
                    keyword6: this.Transfer.Destination_From,
                    keyword7: this.Transfer.Destination_To,
                    date1: bookDate,
                    keyword9: this.searchCriteria.guest,
                    keyword10: this.searchCriteria.hour + ':' + this.searchCriteria.min,
                    amount1: this.Transfer.Price,
                    date2: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = this.cmsRequestData("POST", "cms/data", sendingRequest, null);
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, postData);
                    sendMailService(emailApi, custmail);
                    this.travellerInfo.email = '';
                    this.travellerInfo.firstName = '';
                    this.travellerInfo.contactNumber = '';
                    this.travellerInfo.lastName = '';
                    alertify.alert('Success', 'Thank you for contacting us.We shall get back to you.');
                    // setTimeout(function () { window.location.reload() }, 3000);
                } catch (e) {
                    console.log(e);
                }
            }
        },
        cmsRequestData: function (callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            return fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            }).then(function (response) {
                return response.json();
            });
        },
        getSearchData: function () {
            var urldata = this.getUrlVars();
            // urldata = decodeURIComponent(urldata);
            return urldata;
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        yourCallBackFunction: function (data) {
            if (data != null) {
                if (data != "") {
                    var customSearch =
                        "&date=" + data.date +
                        "&hour=" + (parseInt(data.hour) + 1) +
                        "&min=" + (parseInt(data.min) + 1) +
                        "&guest=" + data.guest +
                        "&city=" + data.city +
                        "&from=" + data.from +
                        "&to=" + data.to + "&";

                    url = "/ICA/transfer-results.html?page=" + customSearch;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;
        }
    },
    mounted: function () {
        localStorage.removeItem("backUrl");
        this.getPagecontent();
        sessionStorage.active_e=3;
    },
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}