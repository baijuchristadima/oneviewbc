var packageView = new Vue({
    el: '#Hoteldetails',
    name: 'Hoteldetails',
    data: {
        maincontent: {
            Hotel_Name: '',
            Rate: '',
        },
        details: {
            Informations: '',
            Hotel_Image_Banner: '',
            Amenities: '',
        },
        Bookingdetails: {
            inDate: '',
            outDate: '',
            nights: '',
            instructions: '',
        },
        Roomdetails: {
            Room_Details: '',
            Total_Rooms: '',
            Instructions: '',
        },
        name: '',
        email: '',
        indate: '',
        outdate: '',
        roomtotal: '',
        norooms: 1,
        Rate: 0

    },
    mounted() {
        localStorage.removeItem("backUrl");

        this.getPackagedetails();
        this.setCalender();
        sessionStorage.active_e = 2;
        var vm = this;
        this.$nextTick(function () {
            $('#roomno').on("change", function (e) {
                vm.dropdownChange(e, 'roomno')
            });
        })

    },
    methods: {
        dateFormat: function (d) {
            return moment(d).format('DD-MMM-YYYY');
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPackagedetails: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var hotelurl = getQueryStringValue('page');
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                if (hotelurl != "") {
                    hotelurl = hotelurl.split('-').join(' ');
                    hotelurl = hotelurl.split('_')[0];
                    var tohotelurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + hotelurl + '.ftl';
                    self.pageURLLink = hotelurl;
                    axios.get(tohotelurl, {
                        headers: {
                            'content-type': 'text/html',
                            'Accept': 'text/html',
                            'Accept-Language': langauage
                        }
                    }).then(function (response) {

                        var searchData = self.getSearchData();
                        if (searchData != "" || searchData != null) {
                            var fromDate = searchData.cin.trim();
                            var toDate = searchData.cout.trim();
                            var searchCriteria = Object.assign({}, searchData);
                            localStorage.setItem("backUrl", self.yourCallBackFunction(searchCriteria));
                            $("#from1").val(fromDate);
                            $("#from2").val(toDate);

                            var maincontent = self.pluck('Hotel_Details', response.data.area_List);
                            if (maincontent.length > 0) {
                                self.maincontent.Hotel_Name = self.pluckcom('Hotel_Name', maincontent[0].component);
                                self.maincontent.Rate = self.pluckcom('Rate', maincontent[0].component);
                                self.checkPrice();
                            }
                            var details = self.pluck('Hotel_Details_Section', response.data.area_List);
                            if (details.length > 0) {
                                self.details.Informations = self.pluckcom('Informations', details[0].component);
                                self.details.Hotel_Image_Banner = self.pluckcom('Hotel_Image_Banner', details[0].component);
                                self.details.Amenities = self.pluckcom('Amenities', details[0].component);
                            }
                            var Bookingdetails = self.pluck('Booking_Details_Section', response.data.area_List);
                            if (Bookingdetails.length > 0) {
                                self.Bookingdetails.inDate = self.pluckcom('Main_Check_In_Date', Bookingdetails[0].component);
                                self.Bookingdetails.outDate = self.pluckcom('Main_Check_Out_Date', Bookingdetails[0].component);
                                self.Bookingdetails.nights = self.pluckcom('Number_of_Nights', Bookingdetails[0].component);
                                self.Bookingdetails.instructions = self.pluckcom('Instructions', Bookingdetails[0].component);

                            }
                            var Roomdetails = self.pluck('Room_Details_Section', response.data.area_List);
                            if (Roomdetails.length > 0) {
                                self.Roomdetails.Room_Details = self.pluckcom('Room_Details', Roomdetails[0].component);
                                self.Roomdetails.Total_Rooms = self.pluckcom('Total_Rooms', Roomdetails[0].component);
                                self.Roomdetails.Instructions = self.pluckcom('Instructions', Roomdetails[0].component);

                            }
                        }

                    })
                }
            });
        },
        yourCallBackFunction: function (data) {
            if (data != null) {
                if (data != "") {
                    var customSearch =
                        "&city=" + data.city +
                        "&cin=" + data.cin +
                        "&cout=" + data.cout +
                        "&occupancy=" + data.occupancy + "&";


                    url = "/ICA/hotel-results.html?page=" + customSearch;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;
        },
        checkPrice: function () {
            this.roomtotal = this.norooms * this.maincontent.Rate;
        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {

                if (type == 'roomno') {
                    this.norooms = event.target.value;
                    this.checkPrice();
                }
            }
        },
        checkReservation: function () {
            let dateObj1 = document.getElementById("from1");
            let dateValue1 = dateObj1.value;
            let dateObj2 = document.getElementById("from2");
            let dateValue2 = dateObj2.value;
            let room = document.getElementById("roomno");
            let roomValue = room.value;
            // let yourPrice = roomValue*
            if (!this.name) {
                alertify.alert('Alert', 'Name required.').set('closable', false);
                return false;
            }
            if (!this.email) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.email.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (dateValue1 == undefined || dateValue1 == '') {
                alertify.alert('Alert', 'Select Check In date').set('closable', false);
                return false;
            }
            if (dateValue2 == undefined || dateValue2 == '') {
                alertify.alert('Alert', 'Select Check Out Date').set('closable', false);
                return false;
            }
            if (roomValue == undefined || roomValue == '') {
                alertify.alert('Alert', 'Please Select Rooms.').set('closable', false);
                return false;
            } else {
                var fromEmail = JSON.parse(localStorage.User).loginNode.email;
                var toEmail = JSON.parse(localStorage.User).emailId;
                //let dateValue1 = moment(String(dateValue)).format('YYYY-MM-DDThh:mm:ss');
                var postData = {
                    type: "PackageBookingRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                    ccEmails: [],
                    logo:  JSON.parse(localStorage.User).loginNode.logo,
                    // logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    packegeName: this.maincontent.Hotel_Name,
                    personName: this.name,
                    emailAddress: this.email,
                    contact: "NA",
                    departureDate: dateValue1,
                    adults: "NA",
                    child2to5: "NA",
                    child6to11: "NA",
                    infants: "NA",
                    message: "this.message",
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                var frommail = JSON.parse(localStorage.User).loginNode.email;
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.email) ? this.email : [this.email],
                    // logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    logo:  JSON.parse(localStorage.User).loginNode.logo,
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.name,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                // var bookDate = moment(this.searchCriteria.date, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');

                dateValue1 = moment(dateValue1,'DD/MM/YYYY').format('YYYY-MM-DDThh:mm:ss');
                dateValue2 = moment(dateValue2,'DD/MM/YYYY').format('YYYY-MM-DDThh:mm:ss');
                var sendingRequest = {
                    type: "Hotel Booking",
                    keyword1: this.name,
                    keyword2: this.email,
                    keyword3: roomValue,
                    keyword4: this.maincontent.Hotel_Name,
                    date1: dateValue1,
                    date2: dateValue2,
                    amount1: this.roomtotal,
                    nodeCode: agencyCode
                };
                let responseObject = this.cmsRequestData("POST", "cms/data", sendingRequest, null);
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, postData);
                    sendMailService(emailApi, custmail);
                    this.name = '';
                    this.email = '';
                    document.getElementById("from1").value = '';
                    document.getElementById("from2").value = '';
                    $('#roomno').val('').trigger('change');
                    alertify.alert('Success', 'Thank you for contacting us.We shall get back to you.');
                    $("#from1").val("");
                    $("#from2").val("");
                } catch (e) {

                }
            }
        },
        getSearchData: function () {
            var urldata = this.getUrlVars();
            return urldata;
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        cmsRequestData: function (callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            return fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json'
                },
                body: data, // body data type must match "Content-Type" header
            }).then(function (response) {
                return response.json();
            });
        },
        dateChecker: function () {
            let dateObj1 = document.getElementById("from1");
            let dateValue1 = dateObj1.value;
            let dateObj2 = document.getElementById("from2");
            let dateValue2 = dateObj2.value;
            if (dateValue2 !== undefined || dateValue2 !== '') {
                // document.getElementById("to1").value = '';
                $('#from2').val('').trigger('change');
                $("#from2").val("");
                return false;
            }
        },
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#from1").datepicker({
                // minDate: "0d",
                // maxDate: "360d",
                minDate: new Date('11/15/2020'),
                maxDate: new Date('11/20/2020'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: { direction: "up" },
                showButtonPanel: false,
                dateFormat: dateFormat,
            });

            $("#from2").datepicker({
                minDate: new Date('11/15/2020'),
                maxDate: new Date('11/20/2020'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: { direction: "up" },
                showButtonPanel: false,
                dateFormat: dateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#from1").val();
                    $("#from2").datepicker("option", "minDate", selectedDate);
                }
            });
        }
    }
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
jQuery(document).ready(function ($) {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
});