var HotelList = new Vue({
    el: '#Hotels',
    name: 'Hotels',
    data: {
        Hotels: [],
        hotelFull: [],
        cityName: '',
        fromDate: '',
        toDate: '',
        occupancyName: '',
        locationHotels: [],
        OccupancyHotels: [],
        pageLoad: true,

    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPackage: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var tohotelurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Hotel/Hotel/Hotel.ftl';
                axios.get(tohotelurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    var packageData = response.data;
                    let PackageListTemp = [];
                    var searchData = self.getSearchData();

                    if (searchData != "" || searchData != null) {
                        self.cityName = searchData.city.split('-').join(' ');
                        self.fromDate = searchData.cin.trim();
                        self.toDate = searchData.cout.trim();
                        self.occupancyName = searchData.occupancy.trim();

                        if (packageData != undefined && packageData.Values != undefined) {
                            self.hotelFull = packageData.Values;
                            self.hotelFull.forEach(function (item, index) {
                                self.locationHotels.push(item.Location.toUpperCase().trim());
                                self.OccupancyHotels.push(item.Room_Occupancy.toUpperCase().trim());
                            });
                            self.locationHotels = _.uniq(_.sortBy(self.locationHotels));
                            self.OccupancyHotels = _.uniq(_.sortBy(self.OccupancyHotels));


                            PackageListTemp = packageData.Values.filter(function (x) {
                                return x.Status == true &&
                                    x.Location.toLowerCase().trim() == self.cityName.toLowerCase() &&
                                    x.Room_Occupancy.toLowerCase().trim() == self.occupancyName.toLowerCase()
                            });

                            self.Hotels = PackageListTemp;

                        }
                        $("#from1").val(self.fromDate);
                        $("#from2").val(self.toDate);
                        // $("#city").val(self.city);
                        $("#city").val(self.cityName);
                        $("#occupancy").val(self.occupancyName);
                        if (!self.Hotels.length) {
                            self.pageLoad = false;
                        } else { self.pageLoad = true; }
                    }
                }).catch(function (error) {
                    console.log('Error', error);
                    self.content = [];
                    self.pageLoad;
                });
            });
        },
        getSearchData: function () {
            var urldata = this.getUrlVars();
            // urldata = decodeURIComponent(urldata);
            return urldata;
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        getmoreinfo(url) {
            var customSearch =
                "&city=" + this.cityName.split(' ').join('-') +
                "&cin=" + this.fromDate +
                "&cout=" + this.toDate +
                "&occupancy=" + this.occupancyName + "&";
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/ICA/hotel-details.html?page=" + url + customSearch;
                    window.location.href = url;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;
        },
        dateChecker: function () {
            let dateObj1 = document.getElementById("from1");
            let dateValue1 = dateObj1.value;
            let dateObj2 = document.getElementById("from2");
            let dateValue2 = dateObj2.value;
            if (dateValue2 !== undefined || dateValue2 !== '') {
                // document.getElementById("to1").value = '';
                $('#from2').val('').trigger('change');
                $("#from2").val("");
                return false;
            }
        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                if (type == 'city') {
                    this.cityName = event.target.value;
                } else if (type == 'from1') {
                    this.fromDate = event.target.value;
                } else if (type == 'from2') {
                    this.toDate = event.target.value;
                } else if (type == 'occupancy') {
                    this.occupancyName = event.target.value;
                }
            }
        },
        modifySearch: function () {
            var self = this;
            if (self.cityName == undefined || self.cityName == '' || self.cityName == null) {
                alertify.alert('Alert', 'Please select a city.').set('closable', false);
                return false;
            } else if (self.fromDate == undefined || self.fromDate == '' || self.fromDate == null) {
                alertify.alert('Alert', 'Please select Check In Date.').set('closable', false);
                return false;
            } else if (self.toDate == undefined || self.toDate == '' || self.toDate == null) {
                alertify.alert('Alert', 'Please select Check Out Date.').set('closable', false);
                return false;
            } else if (self.occupancyName == undefined || self.occupancyName == '' || self.occupancyName == null) {
                alertify.alert('Alert', 'Please select Occupancy.').set('closable', false);
                return false;
            } else {
                var PackagesTemp = [];
                PackagesTemp = self.hotelFull;
                PackagesTemp = PackagesTemp.filter(function (x) {
                    return x.Status == true &&
                        x.Location.toLowerCase().trim() == self.cityName.toLowerCase() &&
                        x.Room_Occupancy.toLowerCase().trim() == self.occupancyName.toLowerCase()
                });
                self.$nextTick(function () {
                    self.Hotels = PackagesTemp;
                    if (!self.Hotels.length) {
                        self.pageLoad = false;
                    } else {
                        self.pageLoad = true;
                    }
                });
            }


        },
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#from1").datepicker({
                // minDate: "0d",
                // maxDate: "360d",
                minDate: new Date('11/15/2020'),
                maxDate: new Date('11/20/2020'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: { direction: "up" },
                showButtonPanel: false,
                dateFormat: dateFormat,
            });

            $("#from2").datepicker({
                minDate: new Date('11/15/2020'),
                maxDate: new Date('11/20/2020'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: { direction: "up" },
                showButtonPanel: false,
                dateFormat: dateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#from1").val();
                    $("#from2").datepicker("option", "minDate", selectedDate);
                }
            });
        }
    },

    mounted: function () {
        var historyPage = localStorage.getItem("backUrl") ? localStorage.getItem("backUrl") : null;
        if (historyPage) {
            localStorage.removeItem("backUrl");
            window.location.href = historyPage;
        }

        this.getPackage();
        this.setCalender();
        sessionStorage.active_e = 2;
        var vm = this;
        this.$nextTick(function () {
            $('#city').on("change", function (e) {
                vm.dropdownChange(e, 'city');
            });
            $('#from1').on("change", function (e) {
                vm.dropdownChange(e, 'from1')
            });
            $('#from2').on("change", function (e) {
                vm.dropdownChange(e, 'from2')
            });
            $('#occupancy').on("change", function (e) {
                vm.dropdownChange(e, 'occupancy')
            });
        })
    },
});

jQuery(document).ready(function ($) {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}