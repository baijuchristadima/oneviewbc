var packageView = new Vue({
  el: '#Packagedetails',
  name: 'Packagedetails',
  data: {
    maincontent: {
      Image: '',
      Title: '',
      Hours: '',
      Minutes: '',
      Description: '',
      Price: '',
      ChildPrice: ''
    },
    Inclusions: {},
    Terms: {},
    Gallery: {},
    pageURLLink: '',
    // Booking form
    name: '',
    email: '',
    phone: '',
    message: '',
    adult: '',
    child: 0,
    adulttotal: '',
    childtotal: '',
    grandtotal: '',
    searchCriteria: {}

  },
  mounted() {
    localStorage.removeItem("backUrl");

    this.getPackagedetails();
    this.setCalender();
    var vm=this;
    this.$nextTick(function () {
      $('#from').on("change", function (e) {
        vm.dropdownChange(e, 'from')
      });
    })
    sessionStorage.active_e = 4;
  },
  methods: {
    dropdownChange: function (event, type) {
      if (event != undefined && event.target != undefined && event.target.value != undefined) {
        if (type == 'from') {
          this.searchCriteria.date = event.target.value;
        }
      }
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPackagedetails: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var packageurl = getQueryStringValue('page');
        var Language = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        if (packageurl != "") {
          packageurl = packageurl.split('-').join(' ');
          packageurl = packageurl.split('_')[0];
          var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
          var searchData = self.getSearchData();
          if (searchData != "" || searchData != null) {
            searchData.date = searchData.date.trim();
            searchData.adult = searchData.adult;
            self.searchCriteria = Object.assign({}, searchData);
            localStorage.setItem("backUrl", self.yourCallBackFunction(self.searchCriteria));
            self.pageURLLink = packageurl;
            axios.get(topackageurl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': Language }
            }).then(function (response) {
              var maincontent = self.pluck('Information_Area', response.data.area_List);
              if (maincontent.length > 0) {
                self.maincontent.Title = self.pluckcom('Package_Title', maincontent[0].component);
                self.maincontent.Image = self.pluckcom('Package_Image', maincontent[0].component);
                self.maincontent.Hours = self.pluckcom('Hours', maincontent[0].component);
                self.maincontent.Minutes = self.pluckcom('Minutes', maincontent[0].component);
                self.maincontent.Description = self.pluckcom('Description', maincontent[0].component);
                self.maincontent.Price = self.pluckcom('Price_Of_Adult', maincontent[0].component);
                self.maincontent.ChildPrice = self.pluckcom('Price_Of_Child', maincontent[0].component);
              }

              var Inclusions = self.pluck('Inclusions_Area', response.data.area_List);
              if (Inclusions.length > 0) {
                self.Inclusions.Inclusion = self.pluckcom('Inclusion', Inclusions[0].component);
              }

              var Terms = self.pluck('Terms_And_Conditions_Area', response.data.area_List);
              if (Terms.length > 0) {
                self.Terms.Term = self.pluckcom('Terms_And_conditions', Terms[0].component);
              }
              var Gallery = self.pluck('Gallery_Area', response.data.area_List);
              if (Gallery.length > 0) {
                self.Gallery.Images = self.pluckcom('Images', Gallery[0].component);
              }
              self.calc();
            })
          }
        }
      });
    },
    getSearchData: function () {
      var urldata = this.getUrlVars();
      // urldata = decodeURIComponent(urldata);
      return urldata;
    },
    getUrlVars: function () {
      var vars = [],
        hash;
      var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
      for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars[hash[0]] = hash[1];
      }
      return vars;
    },
    sendbooking: function () {
      let dateObj = document.getElementById("from");
      let dateValue = dateObj.value;
      if (!this.name) {
        alertify.alert('Alert', 'Name required.').set('closable', false);

        return false;
      }
      if (!this.email) {
        alertify.alert('Alert', 'Email Id required.').set('closable', false);
        return false;
      }
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.email.match(emailPat);
      if (matchArray == null) {
        alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
        return false;
      }
      if (!this.phone) {
        alertify.alert('Alert', 'Mobile number required.').set('closable', false);
        return false;
      }
      if (this.phone.length < 8) {
        alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
        return false;
      }
      if (dateValue == undefined || dateValue == '') {
        alertify.alert('Alert', 'Please select date').set('closable', false);
        return false;
      }

      if (!this.message) {
        alertify.alert('Alert', 'Message required.').set('closable', false);
        return false;
      } else {
        var fromEmail = JSON.parse(localStorage.User).loginNode.email;
        var toEmail = JSON.parse(localStorage.User).emailId;
        var postData = {
          type: "PackageBookingRequest",
          fromEmail: fromEmail,
          toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
          ccEmails: [],
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          packegeName: this.maincontent.Title,
          personName: this.name,
          emailAddress: this.email,
          contact: this.phone,
          departureDate: dateValue,
          adults: this.searchCriteria.adult,
          child2to5: this.child,
          child6to11: "NA",
          infants: "NA",
          message: this.message,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        var fromEmail = JSON.parse(localStorage.User).loginNode.email;
        var custmail = {
          type: "UserAddedRequest",
          fromEmail: fromEmail,
          toEmails: Array.isArray(this.email) ? this.email : [this.email],
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.name,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        dateValue = moment(this.searchCriteria.date, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
          type: "Tour Booking",
          keyword1: this.name,
          keyword2: this.email,
          keyword3: this.maincontent.Title,
          keyword5: this.phone,
          date1: dateValue,
          number2: this.searchCriteria.adult,
          number3: this.child,
          text1: this.message,
          nodeCode: agencyCode,
          date2: requestedDate
        };
        let responseObject = this.cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
          let insertID = Number(responseObject);
          var emailApi = ServiceUrls.emailServices.emailApi;
          sendMailService(emailApi, postData);
          sendMailService(emailApi, custmail);
          this.name = '';
          this.email = '';
          this.phone = '';
          this.message = '';
          this.searchCriteria.date = "";
          this.searchCriteria.adult = "1";
          $('#from').val('');
          this.child = "0";
          this.calc();
          alertify.alert('Success', 'Thank you for Booking.We shall get back to you.');
        } catch (e) {

        }
      }
    },
    cmsRequestData: function (callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      return fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: { 'Content-Type': 'application/json' },
        body: data, // body data type must match "Content-Type" header
      }).then(function (response) {
        return response.json();
      });
    },
    calc: function () {
      var vm = this;
      vm.adulttotal = vm.searchCriteria.adult * vm.maincontent.Price;
      vm.childtotal = vm.child * vm.maincontent.ChildPrice;
      vm.grandtotal = vm.adulttotal + vm.childtotal;
    },
    setCalender() {
      var dateFormat = "dd/mm/yy"
      $("#from").datepicker({
        minDate: "0d",
        maxDate: "360d",
        numberOfMonths: 1,
        changeMonth: true,
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>",
        duration: "fast",
        showAnim: "slide",
        showOptions: { direction: "up" },
        showButtonPanel: false,
        dateFormat: dateFormat,
      });
    },
    yourCallBackFunction: function (data) {
      if (data != null) {
        if (data != "") {
          var customSearch =
            "&city=" + data.city +
            "&date=" + data.date +
            "&adult=" + data.adult + "&";

          url = "/ICA/tour-results.html?page=" + customSearch;
        } else {
          url = "#";
        }
      } else {
        url = "#";
      }
      return url;
    },

  }
})
function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}