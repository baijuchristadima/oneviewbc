var packageList = new Vue({
    el: '#transfer',
    name: 'transfer',
    data: {
        cityName: '',
        transferDate: '',
        from: '',
        to: '',
        pickUpH: '',
        pickUpM: '',
        guest: 1,
        Banner: { Banner_Image: '' },
        cityList: [],
        filteredListFromTo: [],
        numberPassenger: 6,
    },
    methods: {

        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = commonPath + Agencycode + '/Template/Home Page/Home Page/Home Page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (res) {
                    console.log(res);

                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var Banner = self.pluck('Index_Section', self.content);
                        self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
                    }

                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });

                var transferUrl = huburl + portno + commonPath + Agencycode + '/Master Table/List Of Transfers/List Of Transfers/List Of Transfers.ftl';
                axios.get(transferUrl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (res) {
                    var packageData = res.data;
                    self.Packages = packageData.Values;
                    var maxPax = [];
                    self.Packages.forEach(function (item, index) {

                        if (item.City) {
                            self.cityList.push(item.City.toUpperCase().trim());
                        }
                        if (!isNaN(item._Max_Number_of_Passengers.trim())) {
                            maxPax.push(Number((item._Max_Number_of_Passengers.trim())));
                        }
                    });
                    var tempCity = [];
                    // self.cityList.forEach(function (item, index) {
                    //     tempCity.push(item)
                    // });
                    tempCity = _.uniq(_.sortBy(self.cityList));
                    self.cityList = tempCity;
                    self.numberPassenger = _.max(maxPax);
                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });

            });
        },
        searchTransfer: function () {
            let cityValue = document.getElementById("city");
            let cityVal = cityValue.value;
            let tDate = document.getElementById("from2");
            let transfDate = tDate.value;
            let pickLocation = document.getElementById("from");
            let Pickup = pickLocation.value;
            let dropLocation = document.getElementById("to");
            let drop = dropLocation.value;
            let guestData = document.getElementById("guestId");
            let guestDat = guestData.value;
            let pickupTime = document.getElementById("pickuphour");
            let pickupTim = pickupTime.value;
            let pickupM = document.getElementById("pickupmin");
            let pickupMIN = pickupM.value;
            if (cityVal == undefined || cityVal == '') {
                alertify.alert('Alert', 'Please select a city.').set('closable', false);
                return false;
            }
            else if (transfDate == undefined || transfDate == '') {
                alertify.alert('Alert', 'Please select a Transfer Date.').set('closable', false);
                return false;
            }
            else if (Pickup == undefined || Pickup == '') {
                alertify.alert('Alert', 'Please Choose a Pick Up Location.').set('closable', false);
                return false;
            }
            else if (drop == undefined || drop == '') {
                alertify.alert('Alert', 'Please Choose a Drop Location.').set('closable', false);
                return false;
            }
            else if (guestDat == undefined || guestDat == '') {
                alertify.alert('Alert', 'Please Choose No of Passengers.').set('closable', false);
                return false;
            }
            else if (pickupTim == undefined || pickupTim == '') {
                alertify.alert('Alert', 'Please Choose Pick Up Time(H).').set('closable', false);
                return false;
            }
            else if (pickupMIN == undefined || pickupMIN == '') {
                alertify.alert('Alert', 'Please Choose Pick Up Time(M).').set('closable', false);
                return false;
            } else if (Pickup == drop) {
                alertify.alert('Alert', 'Pick Up and Drop Off Locations should not be same!').set('closable', false);
                return false;
            } else {
                // $('#city').val('');
                var searchCriteria =
                    "city=" + this.cityName +
                    "&date=" + this.transferDate +
                    "&hour=" + this.pickUpH +
                    "&min=" + this.pickUpM +
                    "&from=" + this.from +
                    "&to=" + this.to +
                    "&guest=" + this.guest + "&";
                searchCriteria = searchCriteria.split(' ').join('-');
                var uri = "/ICA/transfer-results.html?" + searchCriteria;

                //    

                window.location.href = uri;
            }
        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {

                if (type == 'city') {
                    this.cityName = event.target.value;
                    this.fromTo();
                } else if (type == 'from') {
                    this.from = event.target.value;
                } else if (type == 'to') {
                    this.to = event.target.value;
                } else if (type == 'pickuphour') {
                    this.pickUpH = event.target.value;
                } else if (type == 'pickupmin') {
                    this.pickUpM = event.target.value;
                } else if (type == 'guest') {
                    this.guest = event.target.value;
                } else if (type == 'from2') {
                    this.transferDate = event.target.value;
                }
            }
        },
        fromTo: function () {
            var self = this;
            self.filteredListFromTo = [];
            $('#from').val('').trigger('change');
            $('#to').val('').trigger('change');
            var result = {
                'city': '',
                'from': [],
                'to': []
            };
            var filteredListFromToTemp = self.Packages.filter(Transfer => (Transfer.City.toLowerCase()).trim().includes((self.cityName.toLowerCase()).trim()));
            result.city = self.cityName;
             filteredListFromToTemp.forEach(function (item, index) {
                result.from.push(item.Destination_From.toUpperCase().trim());
                result.to.push(item.Destination_To.toUpperCase().trim());
            });
            self.$nextTick(function () {
                result.from = _.uniq(result.from);
                result.to = _.uniq(result.to);
                self.filteredListFromTo = result;
            });
        },
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#from2").datepicker({
                // minDate: "0d",
                // maxDate: "360d",
                minDate: new Date('11/15/2020'),
                maxDate: new Date('11/20/2020'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: { direction: "up" },
                showButtonPanel: false,
                dateFormat: dateFormat,
            });
        }


    },

    mounted: function () {

        localStorage.removeItem("backUrl");

        $('#city').val('');
        $('#from2').val('');
        $('#from').val('');
        $('#to').val('');
        // $('#guestId').val('');
        $('#pickuphour').val('');
        $('#pickupmin').val('');
        this.getPagecontent();
        this.setCalender();
        sessionStorage.active_e = 3;
        var vm = this;
        vm.$nextTick(function () {
            $('#city').on("change", function (e) {
                vm.dropdownChange(e, 'city')
            });
            $('#from').on("change", function (e) {
                vm.dropdownChange(e, 'from')
            });
            $('#to').on("change", function (e) {
                vm.dropdownChange(e, 'to')
            });
            $('#pickuphour').on("change", function (e) {
                vm.dropdownChange(e, 'pickuphour')
            });
            $('#pickupmin').on("change", function (e) {
                vm.dropdownChange(e, 'pickupmin')
            });
            $('#guestId').on("change", function (e) {
                vm.dropdownChange(e, 'guest')
            });
            $('#from2').on("change", function (e) {
                vm.dropdownChange(e, 'from2')
            });
        })


    },
});

jQuery(document).ready(function ($) {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
});