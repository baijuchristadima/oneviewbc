var packageList = new Vue({
    el: '#Packages',
    name: 'Packages',
    components: {
        VueSlider: window['vue-slider-component']
    },
    data: {
        Packages: [],
        search: '',
        Locationsearch: '',
        lowest: null,
        highest: null,
        uniqueLocation: [],
        searchCriteria: [],
        date: '',
        adult: '',
        filteredList: [],
        pageLoad: true,


    },
    methods: {
        dropdown: function () {
            this.filteredList = this.Packages.filter(Package => Package.Location.toLowerCase().includes(this.Locationsearch.toLowerCase()));
            this.searchCriteriaFun();
            this.slider();
            if (!this.filteredList.length) {
                this.pageLoad = false;
            } else { this.pageLoad = true; }

        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                if (type == 'city') {
                    this.Locationsearch = event.target.value;
                    this.dropdown();
                }
            }
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPackage: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var Language = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';

                var searchData = self.getSearchData();
                if (searchData != "" || searchData != null) {
                    self.searchCriteria = searchData;
                    var city, date, adult;
                    city = searchData.city.split('-').join(' ');
                    self.Locationsearch = city;
                    date = searchData.date.split('-').join(' ');
                    self.date = date;
                    adult = searchData.adult.split('-').join(' ');
                    self.adult = adult;

                    axios.get(topackageurl, {
                        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': Language }
                    }).then(function (response) {
                        var packageData = [];
                        var packageData = response.data;
                        if (packageData != undefined && packageData.Values != undefined) {
                            self.Packages = packageData.Values;
                            self.Packages = _.sortBy(self.Packages, 'Package_Title');
                            self.filteredList = self.Packages.filter(function (x) {
                                return x.Location.toLowerCase().includes(city.toLowerCase());
                            });
                            self.Packages.forEach(function (item, index) {
                                if (item.Location) {
                                    self.uniqueLocation.push(item.Location.toUpperCase().trim());
                                }
                            });
                            self.uniqueLocation = _.uniq(_.sortBy(self.uniqueLocation));
                        }
                        self.slider();
                        self.$watch(function (vm) { return vm.search },
                            function () {
                               

                                self.filteredList = self.Packages.filter(function (Package) {
                                    return Package.Package_Title.toLowerCase().includes(self.search.toLowerCase())
                                });
                                self.slider();
                            })
                        if (!self.filteredList.length) {
                            self.pageLoad = false;
                        } else { self.pageLoad = true; }
                    }).catch(function (error) {
                        console.log('Error', error);
                        self.content = [];
                        self.pageLoad = false;
                    });
                }
            });
        },

        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/ICA/tour-details.html?page=" + url + "&city=" + this.Locationsearch.split(' ').join('-') + "&date=" + this.date + "&adult=" + this.adult + "&";
                    console.log(url);
                    window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;

        },
        getSearchData: function () {
            var urldata = this.getUrlVars();
            return urldata;
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        slider: function () {
            var vm = this;
            var slider = document.getElementById("slider-range");
            var lowestPrice = Math.min.apply(Math, vm.filteredList.map(function (o) { return o.Price_Of_Adult; }))
            var highestPrice = Math.max.apply(Math, vm.filteredList.map(function (o) { return o.Price_Of_Adult; }))
            vm.lowest = lowestPrice == Number.POSITIVE_INFINITY ? 0 : lowestPrice;
            vm.highest = highestPrice == Number.NEGATIVE_INFINITY ? 1 : highestPrice;
            if (vm.highest == vm.lowest) {
                vm.lowest = parseFloat(vm.lowest) - 1;
            }
            if (vm.lowest && vm.highest) {
                if (slider.noUiSlider) {
                    slider.noUiSlider.updateOptions({
                        start: [parseFloat(vm.lowest), parseFloat(vm.highest)],
                        range: { 'min': parseFloat(vm.lowest), 'max': parseFloat(vm.highest) }
                    });
                } else {
                    noUiSlider.create(slider, {
                        start: [parseFloat(vm.lowest), parseFloat(vm.highest)],
                        connect: true,
                        range: { 'min': [parseFloat(vm.lowest)], 'max': [parseFloat(vm.highest)] }
                    })
                    slider.noUiSlider.on('update', function (values) {
                        vm.lowest = values[0];
                        vm.highest = values[1];
                    });
                }
            }
            vm.$watch(function (vm) { return vm.lowest, vm.highest, Date.now(); },
                function () {
                    vm.filteredList = vm.Packages.filter(Package => Package.Location.toLowerCase().includes(vm.Locationsearch.toLowerCase())).filter(function (e) {
                        return parseFloat(e.Price_Of_Adult) >= parseFloat(vm.lowest)
                            && parseFloat(e.Price_Of_Adult) <= parseFloat(vm.highest);
                    }).filter(function (Package) {
                        return Package.Package_Title.toLowerCase().includes(vm.search.toLowerCase())
                    });
                    if (!vm.filteredList.length) {
                        vm.pageLoad = false;
                    } else { vm.pageLoad = true; }
                })
        },
        sliderchange: function () {
            // var minVal = 0;
            // var maxVal = 2000;
            var vm = this;
            var options = {
                range: true,
                min: vm.lowest,
                max: vm.highest,
                values: [vm.lowest, vm.highest],
                slide: function (event, ui) {
                    var min = ui.values[0],
                        max = ui.values[1];
                    $("#amount").val("AED " + min + " -AED " + max);

                }
            }, min, max;
            $("#slider-range").slider(options);
            min = $("#slider-range").slider("values", 0);
            max = $("#slider-range").slider("values", 1);
            $("#amount").val("AED " + min + " - AED " + max);
        },
        searchCriteriaFun: function () {
            if (this.search != "") {
                this.filteredList = this.Packages.filter(Package => Package.Location.toLowerCase().includes(this.Locationsearch.toLowerCase())).
                    filter(Package => Package.Package_Title.toLowerCase().includes(this.search.toLowerCase()));
            }
            else {
                this.filteredList = this.Packages.filter(Package => Package.Location.toLowerCase().includes(this.Locationsearch.toLowerCase()));
            }
            if (!this.filteredList.length) {
                this.pageLoad = false;
            } else { this.pageLoad = true; }
        },
        clear(e) {
            this.search = "";
            this.slider();
        }

    },

    mounted: function () {
        var historyPage = localStorage.getItem("backUrl") ? localStorage.getItem("backUrl") : null;
        if (historyPage) {
            localStorage.removeItem("backUrl");
            window.location.href = historyPage;
        }

        sessionStorage.active_e = 4;
        this.getPackage();
        var self = this;
        self.$nextTick(function () {
            $('#city').on("change", function (e) {
                self.dropdownChange(e, 'city');
            });
        });
    },
    watch: {
        search: function () {
            this.searchCriteriaFun();
        }
    }
});

jQuery(document).ready(function ($) {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}