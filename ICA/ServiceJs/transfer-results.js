var maininstance = new Vue({
    el: '#transfer-results',
    name: 'transfer-results',
    data: {
        Packages: [],
        cityList: [],
        PackagesFull: [],
        resultFrom: [],
        resultTo: [],

        cityName: '',
        transferDate: '',
        fromL: '',
        toL: '',
        pickUpH: '',
        pickUpM: '',
        guest: '',
        numberPassenger: 6,
        pageLoad: true,
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var transferUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/List Of Transfers/List Of Transfers/List Of Transfers.ftl';
                var searchData = self.getSearchData();

                if (searchData != "" || searchData != null) {
                    searchData.city = searchData.city.split('-').join(' ');
                    searchData.from = searchData.from.split('-').join(' ');
                    searchData.to = searchData.to.split('-').join(' ');
                    searchData.guest = searchData.guest.trim();

                    self.cityName = searchData.city;

                    // self.fromL = searchData.from;
                    // self.toL = searchData.to;

                    self.pickUpH = searchData.hour;
                    self.pickUpM = searchData.min;
                    self.transferDate = searchData.date;
                    self.guest = searchData.guest;

                    axios.get(transferUrl, {
                        headers: {
                            'content-type': 'text/html',
                            'Accept': 'text/html',
                            'Accept-Language': langauage
                        }
                    }).then(function (res) {
                        var PackagesTemp = [];
                        var packageData = res.data;
                        self.PackagesFull = packageData.Values;
                        self.fromTo();
                        self.fromL = searchData.from;
                        self.toL = searchData.to;
                        PackagesTemp = packageData.Values;


                        $("#from2").val(self.transferDate);

                        PackagesTemp = PackagesTemp.filter(function (x) {
                            return x.City.toLowerCase() == self.cityName.toLowerCase() &&
                                x.Destination_From.toLowerCase() == self.fromL.toLowerCase() &&
                                x.Destination_To.toLowerCase() == self.toL.toLowerCase() &&
                                x._Max_Number_of_Passengers.trim() >= Number(self.guest.trim());
                        });
                        self.Packages = PackagesTemp;

                        self.Packages = PackagesTemp;
                        if (!self.Packages.length) {
                            self.pageLoad = false;
                        } else { self.pageLoad = true; }

                    }).catch(function (error) {
                        console.log('Error', error);
                        self.content = [];
                        self.pageLoad = false;
                    });
                }


            });
        },
        getmoreinfo(url) {
            var customSearch =
                "&date=" + this.transferDate +
                "&hour=" + ('0' + (this.pickUpH - 1)).slice(-2) +
                "&min=" + ('0' + (this.pickUpM - 1)).slice(-2) +
                "&guest=" + this.guest +
                "&city=" + this.cityName.split(' ').join('-') +
                "&from=" + this.fromL.split(' ').join('-') +
                "&to=" + this.toL.split(' ').join('-') + "&";

            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/ICA/transfer-booking.html?page=" + url + customSearch;
                    window.location.href = url;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;
        },
        getSearchData: function () {
            var urldata = this.getUrlVars();
            return urldata;
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        dropdownChange: function (event, type) {
            var self = this;
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                if (type == 'cityD') {
                    self.cityName = event.target.value;
                    // $('#tFrom').val('');
                    // $('#tTo').val('');
                    self.fromTo();
                } else if (type == 'fromD') {
                    self.fromL = event.target.value;
                } else if (type == 'toD') {
                    self.toL = event.target.value;
                } else if (type == 'guestIdD') {
                    self.guest = event.target.value;
                } else if (type == 'from2D') {
                    self.transferDate = event.target.value;
                } else if (type == 'pickuphourD') {
                    self.pickUpH = event.target.value;
                } else if (type == 'pickupminD') {
                    self.pickUpM = event.target.value;
                }
            }
        },
        modifySearch: function () {
            var self = this;
            self.setCalender();

            if (self.cityName == undefined || self.cityName == '' || self.cityName == null) {
                alertify.alert('Alert', 'Please select a city.').set('closable', false);
                return false;
            } else if (self.transferDate == undefined || self.transferDate == '' || self.transferDate == null) {
                alertify.alert('Alert', 'Please select a Transfer Date.').set('closable', false);
                return false;
            } else if (self.fromL == undefined || self.fromL == '' || self.fromL == null) {
                alertify.alert('Alert', 'Please Choose a Pick Up Location.').set('closable', false);
                return false;
            } else if (self.toL == undefined || self.toL == '' || self.toL == null) {
                alertify.alert('Alert', 'Please Choose a Drop Location.').set('closable', false);
                return false;
            } else if (self.guest == undefined || self.guest == '' || self.guest == null) {
                alertify.alert('Alert', 'Please Choose No of Travellers.').set('closable', false);
                return false;
            } else if (self.pickUpH == undefined || self.pickUpH == '' || self.pickUpH == null) {
                alertify.alert('Alert', 'Please Choose Pick Up Time(H).').set('closable', false);
                return false;
            } else if (self.pickUpM == undefined || self.pickUpM == '' || self.pickUpM == null) {
                alertify.alert('Alert', 'Please Choose Pick Up Time(M).').set('closable', false);
                return false;
            } else if (self.fromL == self.toL) {
                alertify.alert('Alert', 'Pick Up and Drop Off Locations should not be same!').set('closable', false);
                return false;
            } else {
                var PackagesTemp = [];
                PackagesTemp = self.PackagesFull;
                PackagesTemp = PackagesTemp.filter(function (x) {
                    return x.City.toLowerCase() == self.cityName.toLowerCase() &&
                        x.Destination_From.toLowerCase() == self.fromL.toLowerCase() &&
                        x.Destination_To.toLowerCase() == self.toL.toLowerCase() &&
                        x._Max_Number_of_Passengers.trim() >= Number(self.guest.trim());
                });
                self.$nextTick(function () {
                    self.Packages = PackagesTemp;
                    if (!self.Packages.length) {
                        self.pageLoad = false;
                    } else { self.pageLoad = true; }
                });
            }

        },
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#from2").datepicker({
                minDate: new Date('11/15/2020'),
                maxDate: new Date('11/20/2020'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: { direction: "up" },
                showButtonPanel: false,
                dateFormat: dateFormat,
            });
        },
        fromTo: function () {
            var self = this;
            // $('#from').val('');
            // $('#to').val('');

            $('#tFrom').val('').trigger('change');
            $('#tTo').val('').trigger('change');
            self.resultFrom = [];
            self.resultTo = [];
            self.cityList = [];
            self.fromL = '';
            self.toL = '';

            var fromTemp = [];
            var toTemp = [];
            var maxPax = [];
            var cityFull = [];
            var filteredListFromToTemp = [];
            self.PackagesFull.forEach(function (item, index) {
                cityFull.push(item.City.toUpperCase().trim());
                // maxPax.push(item._Max_Number_of_Passengers.trim());
                if (!isNaN(item._Max_Number_of_Passengers.trim())) {
                    maxPax.push(Number((item._Max_Number_of_Passengers.trim())));
                }

            });
            filteredListFromToTemp = self.PackagesFull.filter(Transfer => (Transfer.City.toLowerCase()).trim() == (self.cityName.toLowerCase()).trim());
            filteredListFromToTemp.forEach(function (item, index) {
                fromTemp.push(item.Destination_From.toUpperCase().trim());
                toTemp.push(item.Destination_To.toUpperCase().trim());
            });
            self.numberPassenger = _.max(maxPax);
            // self.resultFrom = fromTemp;
            // self.resultTo = toTemp;


            self.$nextTick(function () {
                self.cityList = _.uniq(_.sortBy(cityFull));
                self.resultFrom = _.uniq(_.sortBy(fromTemp));
                self.resultTo = _.uniq(_.sortBy(toTemp));
            });
        },
    },
    mounted: function () {
        var vm = this;
        var historyPage = localStorage.getItem("backUrl") ? localStorage.getItem("backUrl") : null;
        if (historyPage) {
            localStorage.removeItem("backUrl");
            window.location.href = historyPage;
        }

        vm.getPagecontent();
        vm.setCalender();
        sessionStorage.active_e = 3;
        // vm.$nextTick(function () {
        $('#city').on("change", function (e) {
            vm.dropdownChange(e, 'cityD');
        });
        $('#tFrom').on("change", function (e) {
            vm.dropdownChange(e, 'fromD')
        });
        $('#tTo').on("change", function (e) {
            vm.dropdownChange(e, 'toD')
        });
        $('#pickuphour').on("change", function (e) {
            vm.dropdownChange(e, 'pickuphourD')
        });
        $('#pickupmin').on("change", function (e) {
            vm.dropdownChange(e, 'pickupminD')
        });
        $('#guestId').on("change", function (e) {
            vm.dropdownChange(e, 'guestIdD')
        });
        $('#from2').on("change", function (e) {
            vm.dropdownChange(e, 'from2D')
            // });
        })
    }
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
jQuery(document).ready(function ($) {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
});