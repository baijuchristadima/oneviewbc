const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var packageView = new Vue({
  i18n,

  el: '#Holidaydetails',
  name: 'Holidaydetails',
  data: {
    content: null,
    packagecontent: {
      Image: "",
      Days: "",
      Night: "",
      Price: "",
      Package_Description: "",
      Itinerary: "",
      Post_Button_Label: "",
      ChildPrice: "",
      Description: ""
    },
    getpackage: false,
    getdata: false,
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
    CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    pagecontent: {
      Breadcrum1: '',
      Breadcrumb2: '',
    },
    review: {},
    Traveller_Limit_Count: '',
    //booking form
    cntusername: '',
    cntemail: '',
    cntcontact: '',
    cntdate: '',
    cntperson: 1,
    child: 0,
    CoupenCode: '',
    grandtotal: '',
    CoupenInfo: {
      Amount: "",
      ApplyCoupen: false,
      discountpercent: false,
      CoupenCode: "",
      newPrice: "",
      DisPercent: "",
      Disamount: ""
    },
    Travellercount: {},
    Coupon_Success_Message: '',

    //review form
    username: '',
    email: '',
    contact: '',
    comment: '',
    overitems: null,
    banner: {
      Banner_Image: '',
      Banner_Title: ''
    },
    allReview: [],
    pageURLLink: '',
    pagebanner: {
      Breadcrumb1: "",
      Breadcrumb3: "",
      Inner_Banner_Title: ""

    },
    pagelabels: {},
    alerts: {},
    alerttypes: {
      Success: "",
      warning: "",
      Booking: "",
      Review: ""
    },
    destination: '',
    allFlightInventory: [],
    allHotelInventory: [],
    allMappedFlights: [],
    allMappedHotels: [],
    allDepartureList: [],
    allAirlineList: [],
    allStarList: [],
    allHotelList: [],
    allNightList: [],
    selectedDeparture: {},
    selectedNight: {},
    selectedStar: {},
    selectedHotel: {},
    selectedAirLine: {},
    totalFareAmount: '0',
    Flight_Label: 'Flight Details',
    Hotel_Label: 'Hotel Details',
    Check_In_Label: 'Check In',
    Check_Out_Label: 'Check Out',
    Room_Type_Label: 'Room Type',
    isrtl: false,
    sessionids: '',
    ipAddres: '',
    isLoading: false,
    fullPage: true,
    selectedHot: {
      hotelid: []
    },
    show: false,
    rooms: [],
    Child_Limit_Count: {},
    Transportation_Count_Limit: {},
    Activity_Count_Limit: {},
    ChildCount: [],
    ActivityCount: [],
    TransportationCount: [],
    totalTransportationCount: null,
    totalActivityCount: null,
    Childtotal: '0',
    adultTotal: '',
    packagedet: {},
    selectedActivities: [],
    childRoomAmount: 0,
    totalTransferAmount: '',
    totalActivityAmount: '',
    packageAmount: 0,
    adultAmount: 0,

    hotelrooms: [],

    hotelRate: []
  },
  mounted: function () {
    this.getPageTitle();
    // this.sessionid();
    // this.setip();
    this.recreateDatePicker();
    var self = this;
    // this.getBanner();
    // this.viewReview();
    var dateFormat = "dd/mm/yy";
    from = $("#from").datepicker({
      minDate: "0d",
      maxDate: "360d",
      numberOfMonths: 1,
      dateFormat: dateFormat,
      showOn: "both",
      buttonText: "<i class='fa fa-calendar'></i>",
      duration: "fast",
      showAnim: "slide",
      showOptions: {
        direction: "up"
      },
      onSelect: function (selectedDate, date) {
        let dateObj = document.getElementById("from");
        dateObj.value = selectedDate;
        $("#from").datepicker("setDate", selectedDate);
        self.constructDefaultValues(self.destination, selectedDate);
        if (self.packagecontent != undefined && self.packagecontent.ItineraryCopy != undefined) {
          self.generateDefaultHotelAndRooms(selectedDate);
        }

      }
    });

  },
  methods: {
    recreateDatePicker: function () {
      var systemDateFormat = generalInformation.systemSettings.systemDateFormat;

      var self = this;
      Vue.nextTick(function () {
        $("#traveldate").datepicker({
          minDate: "0d",
          maxDate: "360d",
          numberOfMonths: 1,
          changeMonth: true,
          autoclose: true,
          showButtonPanel: false,
          // onSelect: function (selectedDate) {
          //     // $(this).parents('.txt_animation').addClass('focused');
          // }
        });



      }.bind(self));
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    onChange(event) {
      console.log(event.target.value)
    },
    defaultValues: function () {
      this.allFlightInventory = [];
      this.allHotelInventory = [];
      this.allMappedFlights = [];
      this.allMappedHotels = [];
      this.allDepartureList = [];
      this.allAirlineList = [];
      this.allStarList = [];
      this.allHotelList = [];
      this.allNightList = [];
      this.selectedDeparture = {};
      this.selectedNight = {};
      this.selectedStar = {};
      this.selectedHotel = {};
      this.selectedAirLine = {};
    },
    getPageTitle: function () {
      var self = this;
      self.isLoading = true;
      self.defaultValues();
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        if (langauage == 'ar') {
          self.isrtl = true;
        } else {
          self.isrtl = false;
        }
        // banner and labels
        // var flightInventory = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Inventory - Flight/Inventory - Flight/Inventory - Flight.ftl';
        // var hotelInventory = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Inventory - Hotel/Inventory - Hotel/Inventory - Hotel.ftl';
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Packages/Packages/Packages.ftl';
        axios.get(pageurl, {
          headers: {
            'content-type': 'text/html',
            'Accept': 'text/html',
            'Accept-Language': langauage
          }
        }).then(function (response) {
          self.content = response.data;

          var maincontent = self.pluck('Common_Area', response.data.area_List);
          if (maincontent.length > 0) {
            // self.packagecontent = maincontent[0].component;
            self.Traveller_Limit_Count = self.pluckcom('Traveller_Limit_Count', maincontent[0].component);
            self.Coupon_Success_Message = self.pluckcom('Coupon_Success_Message', maincontent[0].component);
            self.alerttypes.Success = self.pluckcom('Success_alert_Type', maincontent[0].component);
            self.alerttypes.warning = self.pluckcom('Warning_Alert_Type', maincontent[0].component);
            self.alerttypes.Booking = self.pluckcom('Booking_Alert_Type', maincontent[0].component);
            self.alerttypes.Review = self.pluckcom('Review_Alert_Type', maincontent[0].component);
            self.Child_Limit_Count = self.pluckcom('Child_Limit_Count', maincontent[0].component);
            self.Transportation_Count_Limit = self.pluckcom('Transportation_Count_Limit', maincontent[0].component);
            self.Activity_Count_Limit = self.pluckcom('Activity_Count_Limit', maincontent[0].component);
            self.Travellercounts();
          }

          var pagecontent = self.pluck('Banner_Area', self.content.area_List);
          if (pagecontent.length > 0) {
            self.pagebanner.Breadcrumb1 = self.pluckcom('Breadcrumb1', pagecontent[0].component);
            self.pagebanner.Breadcrumb3 = self.pluckcom('Breadcrumb3', pagecontent[0].component);
            self.pagebanner.Inner_Banner_Title = self.pluckcom('Inner_Banner_Title', pagecontent[0].component);
          }
          var pagecontent = self.pluck('Labels', self.content.area_List);
          if (pagecontent.length > 0) {
            self.pagelabels.Book_Now_Label = self.pluckcom('Book_Now_Label', pagecontent[0].component);
            self.pagelabels.Day_Label = self.pluckcom('Day_Label', pagecontent[0].component);
            self.pagelabels.Night_Label = self.pluckcom('Night_Label', pagecontent[0].component);
            self.pagelabels.Review_Label = self.pluckcom('Review_Label', pagecontent[0].component);
            self.pagelabels.Per_Person_Label = self.pluckcom('Per_Person_Label', pagecontent[0].component);
            self.pagelabels.Name_Placeholder = self.pluckcom('Name_Placeholder', pagecontent[0].component);
            self.pagelabels.Email_Label = self.pluckcom('Email_Label', pagecontent[0].component);
            self.pagelabels.Email_Placeholder = self.pluckcom('Email_Placeholder', pagecontent[0].component);
            self.pagelabels.Phone_number_Label = self.pluckcom('Phone_number_Label', pagecontent[0].component);
            self.pagelabels.Phone_Number_Placeholder = self.pluckcom('Phone_Number_Placeholder', pagecontent[0].component);
            self.pagelabels.Tour_Date_Label = self.pluckcom('Tour_Date_Label', pagecontent[0].component);
            self.pagelabels.Tour_Date_Placeholder = self.pluckcom('Tour_Date_Placeholder', pagecontent[0].component);
            self.pagelabels.Number_of_Person_Label = self.pluckcom('Number_of_Person_Label', pagecontent[0].component);
            self.pagelabels.Number_of_Person_Placeholder = self.pluckcom('Number_of_Person_Placeholder', pagecontent[0].component);
            self.pagelabels.Book_Now_Label = self.pluckcom('Book_Now_Label', pagecontent[0].component);
            self.pagelabels.Write_Review_Label = self.pluckcom('Write_Review_Label', pagecontent[0].component);
            self.pagelabels.Email_Label = self.pluckcom('Email_Label', pagecontent[0].component);
            self.pagelabels.Phone_number_Label = self.pluckcom('Phone_number_Label', pagecontent[0].component);
            self.pagelabels.Comment_Label = self.pluckcom('Comment_Label', pagecontent[0].component);
            self.pagelabels.Post_Button_Label = self.pluckcom('Post_Button_Label', pagecontent[0].component);
            self.pagelabels.Name_Label = self.pluckcom('Name_Label', pagecontent[0].component);
            self.pagelabels.Table_Type_Label = self.pluckcom('Table_Type_Label', pagecontent[0].component);
            self.pagelabels.Table_Quantity_Label = self.pluckcom('Table_Quantity_Label', pagecontent[0].component);
            self.pagelabels.Table_Subtotal_Label = self.pluckcom('Table_Subtotal_Label', pagecontent[0].component);
            self.pagelabels.Table_Adult_Label = self.pluckcom('Table_Adult_Label', pagecontent[0].component);
            self.pagelabels.Total_Label = self.pluckcom('Total_Label', pagecontent[0].component);
            self.pagelabels.Promo_Code_Label = self.pluckcom('Promo_Code_Label', pagecontent[0].component);
            self.pagelabels.Grand_Total_Label = self.pluckcom('Grand_Total_Label', pagecontent[0].component);
            self.pagelabels.Apply_Now_Label = self.pluckcom('Apply_Now_Label', pagecontent[0].component);
            self.pagelabels.Rating_Label = self.pluckcom('Rating_Label', pagecontent[0].component);
            self.pagelabels.Coupon_Code_Placeholder = self.pluckcom('Coupon_Code_Placeholder', pagecontent[0].component);
            self.pagelabels.Discount_Percent_Label = self.pluckcom('Discount_Percent_Label', pagecontent[0].component);
            self.pagelabels.Departure_From_Label = self.pluckcom('Departure_From_Label', pagecontent[0].component);
            self.pagelabels.Flight_Option_Label = self.pluckcom('Flight_Option_Label', pagecontent[0].component);
            self.pagelabels.Hotel_Option_Label = self.pluckcom('Hotel_Option_Label', pagecontent[0].component);
            self.pagelabels.Hotel_Label = self.pluckcom('Hotel_Label', pagecontent[0].component);
            self.pagelabels.Night_Option_Label = self.pluckcom('Night_Option_Label', pagecontent[0].component);
            self.Flight_Label = self.pluckcom('Flight_Details_Label', pagecontent[0].component);
            self.Hotel_Label = self.pluckcom('Hotel_Details_Label', pagecontent[0].component);
            self.Check_In_Label = self.pluckcom('Check_In_Label', pagecontent[0].component);
            self.Check_Out_Label = self.pluckcom('Check_Out_Label', pagecontent[0].component);
            self.Room_Type_Label = self.pluckcom('Room_Type_Label', pagecontent[0].component);
            self.pagelabels.ChildLabel = self.pluckcom('Child_Label', pagecontent[0].component);
            self.pagelabels.TransportationNeeded = self.pluckcom('Transportation_Needed', pagecontent[0].component);
            self.pagelabels.TransferDetailsLabel = self.pluckcom('Transfer_Details_Label', pagecontent[0].component);
            self.pagelabels.PriceLabel = self.pluckcom('Price_Label', pagecontent[0].component);
            self.pagelabels.StarLabel = self.pluckcom('Star_Label', pagecontent[0].component);
            self.pagelabels.AllLabel = self.pluckcom('All_Label', pagecontent[0].component);
            self.pagelabels.ActivityOptionalLabel = self.pluckcom('Activity_Optional_Label', pagecontent[0].component);
            self.pagelabels.ActivityLabel = self.pluckcom('Activity_Label', pagecontent[0].component);

          }

          var pagecontent = self.pluck('Alert_Messages', self.content.area_List);
          if (pagecontent.length > 0) {
            self.alerts.Name_Alert_Message = self.pluckcom('Name_Alert_Message', pagecontent[0].component);
            self.alerts.Email_Alert_Message = self.pluckcom('Email_Alert_Message', pagecontent[0].component);
            self.alerts.Phone_Number_Alert_Message = self.pluckcom('Phone_Number_Alert_Message', pagecontent[0].component);
            self.alerts.Tour_Date_Alert_Message = self.pluckcom('Tour_Date_Alert_Message', pagecontent[0].component);
            self.alerts.Coupon_Error_Alert_Message = self.pluckcom('Coupon_Error_Alert_Message', pagecontent[0].component);
            self.alerts.Rating_Alert_Message = self.pluckcom('Rating_Alert_Message', pagecontent[0].component);
            self.alerts.Comment_Alert_Message = self.pluckcom('Comment_Alert_Message', pagecontent[0].component);
            self.alerts.Enter_Coupon_Message = self.pluckcom('Enter_Coupon_Message', pagecontent[0].component);
            self.alerts.Incorrect_Mail_Alert_Message = self.pluckcom('Incorrect_Mail_Alert_Message', pagecontent[0].component);
            self.alerts.Valid_Phone_Number_Message = self.pluckcom('Valid_Phone_Number_Message', pagecontent[0].component);
            self.alerts.Success_Booking_Message = self.pluckcom('Success_Booking_Message', pagecontent[0].component);
            self.alerts.Review_Thanks_Message = self.pluckcom('Review_Thanks_Message', pagecontent[0].component);
          }
          self.isLoading = false;
          var packageurl = getQueryStringValue('page');
          var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
          if (packageurl != "") {
            packageurl = packageurl.split('-').join(' ');
            packageurl = packageurl.split('_')[0];
            var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
            self.pageURLLink = packageurl;
            axios.get(topackageurl, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': langauage
              }
            }).then(function (response) {

              var maincontent = self.pluck('Package', response.data.area_List);
              if (maincontent.length > 0) {
                self.packagecontent.Title = self.pluckcom('Title', maincontent[0].component);
                self.packagecontent.Image = self.pluckcom('Image', maincontent[0].component);
                self.packagecontent.Days = self.pluckcom('Days', maincontent[0].component);
                self.packagecontent.Night = self.pluckcom('Night', maincontent[0].component);
                self.packagecontent.Price = self.pluckcom('Price', maincontent[0].component);
                self.packagecontent.ChildPrice = self.pluckcom('Child_Price', maincontent[0].component);
                self.packagecontent.Basic_Price = self.pluckcom('Basic_Price', maincontent[0].component);
                self.packagecontent.Package_Description = self.pluckcom('Package_Description', maincontent[0].component);
                self.packagecontent.Itinerary = self.pluckcom('Itinerary', maincontent[0].component);
                self.packagecontent.ItineraryCopy = self.pluckcom('Itinerary', maincontent[0].component);
                self.packagecontent.Post_Button_Label = self.pluckcom('Post_Button_Label', maincontent[0].component);
                self.packagecontent.Description = self.pluckcom('Description', maincontent[0].component);
                var destination = self.pluckcom('Destination', maincontent[0].component);

                var datevalue = moment(new Date().setDate(new Date().getDate() + 1)).format('DD/MM/YYYY');
                self.constructDefaultValues(destination, datevalue);
                if (self.packagecontent.Itinerary != undefined && self.packagecontent.Itinerary.length > 0) {
                  self.generateDefaultHotelAndRooms(datevalue);
                }
              }
            })
          }
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
      });
    },
    generateDefaultHotelAndRooms(searchDate) {
      var self = this;
      var tempNextCheckInDate;
      var temNextRoomDate = "";
      self.packagecontent.Itinerary = JSON.parse(JSON.stringify(self.packagecontent.ItineraryCopy));
      self.packagecontent.Itinerary.forEach(function (arrayItem, index) {

        var newSearchDate = index == 0 ? searchDate : temNextRoomDate;
        temNextRoomDate = moment(newSearchDate, 'DD/MM/YYYY').add(arrayItem.No_Of_Days, 'days').format('DD/MM/YYYY');

        if (arrayItem.Hotels != undefined && arrayItem.Hotels != null) {


          var allMappedHotelsTemp = arrayItem.Hotels.filter(function (Hotel) {
            var fromDate = moment(Hotel.From_Date).format('DD/MM/YYYY');
            var toDate = moment(Hotel.To_Date).format('DD/MM/YYYY');
            var tempFromDate = new Date(moment(fromDate, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
            var tempToDate = new Date(moment(toDate, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
            var tempSearchDate = new Date(moment(newSearchDate, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));

            var availableRoomList = self.checkRoomAvailability(Hotel.Price, newSearchDate);
            var roomStatus = availableRoomList.length > 0 ? true : false;
            Hotel.Price = availableRoomList;

            return Hotel.Active == true && tempFromDate <= tempSearchDate && tempToDate >= tempSearchDate && roomStatus;
          });

          arrayItem.currentHotelList = (allMappedHotelsTemp != undefined && allMappedHotelsTemp.length > 0) ? allMappedHotelsTemp : [];
          arrayItem.Hotels = (allMappedHotelsTemp != undefined && allMappedHotelsTemp.length > 0) ? allMappedHotelsTemp : [];
          arrayItem.tempSelectedHotelRoomList = [];
          arrayItem.selectedHotel = arrayItem.Hotels.length > 0 ? arrayItem.Hotels[0] : {};
          arrayItem.selectedHotel.hotelRoomList = arrayItem.selectedHotel.Price != undefined ? arrayItem.selectedHotel.Price : [];
          //arrayItem.selectedHotel.hotelSelectedRoom = arrayItem.selectedHotel.hotelRoomList.length > 0 ? arrayItem.selectedHotel.hotelRoomList[0] : {};
          arrayItem.selectedHotel.hotelRoomList.forEach(roomRow => {
            roomRow.selectedRoomUnit = 1;
            roomRow.isRoomSelected = false;
          });
          // arrayItem.hotelRoomList = self.generateHotelRoomList(arrayItem.currentHotelList);
          arrayItem.hotelRatingList = self.generateHotelRatingList(arrayItem.currentHotelList);
          arrayItem.selectedStar = 'All';
        } else {
          arrayItem.Hotels = [];
        }
        if (arrayItem.Transfer != undefined && arrayItem.Transfer != null) {
          arrayItem.isTransportationEnabled = true;
          var allMappedTransferTemp = arrayItem.Transfer.filter(function (transferObj) {
            var fromDate = moment(transferObj.From_Date).format('DD/MM/YYYY');
            var toDate = moment(transferObj.To_Date).format('DD/MM/YYYY');
            var tempFromDate = new Date(moment(fromDate, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
            var tempToDate = new Date(moment(toDate, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
            var tempSearchDate = new Date(moment(newSearchDate, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
            var availableVehicleList = self.checkRoomAvailability(transferObj.Price, newSearchDate);
            var roomStatus = availableVehicleList.length > 0 ? true : false;
            transferObj.Price = availableVehicleList;

            return transferObj.Active == true && tempFromDate <= tempSearchDate && tempToDate >= tempSearchDate && roomStatus;
          });
          arrayItem.currentTransferList = (allMappedTransferTemp != undefined && allMappedTransferTemp.length > 0) ? allMappedTransferTemp : [];
          arrayItem.Transfer = (allMappedTransferTemp != undefined && allMappedTransferTemp.length > 0) ? allMappedTransferTemp : [];
          arrayItem.selectedClass = arrayItem.Transfer.length > 0 ? arrayItem.Transfer[0] : {};
          arrayItem.selectedClass.transferVehicleList = arrayItem.selectedClass.Price != undefined ? arrayItem.selectedClass.Price : [];
          arrayItem.selectedClass.transferSelectedVehicle = arrayItem.selectedClass.transferVehicleList.length > 0 ? arrayItem.selectedClass.transferVehicleList[0] : {};
          arrayItem.selectedClass.transferSelectedVehicle.noOfUnit = 1;
          arrayItem.transferTypeList = self.generateTransferTypeList(arrayItem.currentTransferList);
          arrayItem.selectedTransferType = 'All';

        } else {
          arrayItem.Transfer = [];
        }
        if (arrayItem.Activity != undefined && arrayItem.Activity != null) {
          arrayItem.isActivityEnabled = true;
          var allMappedActivityTemp = arrayItem.Activity.filter(function (activityObj) {
            var fromDate = moment(activityObj.From_Date).format('DD/MM/YYYY');
            var toDate = moment(activityObj.To_Date).format('DD/MM/YYYY');
            var tempFromDate = new Date(moment(fromDate, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
            var tempToDate = new Date(moment(toDate, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
            var tempSearchDate = new Date(moment(newSearchDate, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
            var availableActivtyList = self.checkRoomAvailability(activityObj.Price, newSearchDate);
            var activityStatus = availableActivtyList.length > 0 ? true : false;
            activityObj.Price = availableActivtyList;

            return activityObj.Active == true && tempFromDate <= tempSearchDate && tempToDate >= tempSearchDate && activityStatus;
          });
          arrayItem.currentActivityList = (allMappedActivityTemp != undefined && allMappedActivityTemp.length > 0) ? allMappedActivityTemp : [];
          arrayItem.Activity = (allMappedActivityTemp != undefined && allMappedActivityTemp.length > 0) ? allMappedActivityTemp : [];
          arrayItem.tempSelectedActivityList = [];
          arrayItem.selectedActivityCategory = arrayItem.Activity.length > 0 ? arrayItem.Activity[0] : {};
          arrayItem.selectedActivityCategory.activityList = arrayItem.selectedActivityCategory.Price != undefined ? arrayItem.selectedActivityCategory.Price : [];
          arrayItem.selectedActivityCategory.activityList.forEach(activityRow => {
            activityRow.selectedActivityUnit = 1;
            activityRow.isActivitySelected = false;
          });
          //arrayItem.selectedActivityCategory.activitySelected = arrayItem.selectedActivityCategory.activityList.length > 0 ? arrayItem.selectedActivityCategory.activityList[0] : {};

          arrayItem.activityTypeList = self.generateActivityTypeList(arrayItem.currentActivityList);
          arrayItem.selectedActivityType = 'All';

        } else {
          arrayItem.Activity = [];
        }
        if (index == 0) {
          arrayItem.checkIn = searchDate;
        } else {
          arrayItem.checkIn = tempNextCheckInDate;
        }
        var newDate = moment(arrayItem.checkIn, 'DD/MM/YYYY').add(arrayItem.No_Of_Days, 'days').format('DD/MM/YYYY');
        arrayItem.checkOut = newDate;
        tempNextCheckInDate = arrayItem.checkOut;

      });
      self.refreshAndCalculate();

    },
    checkRoomAvailability(roomList, searchDate) {
      if (roomList != undefined && roomList.length > 0) {
        var availableRoomList = roomList.filter(function (room) {
          var fromDate = moment(room.From_Date).format('DD/MM/YYYY');
          var toDate = moment(room.To_Date).format('DD/MM/YYYY');
          var tempFromDate = new Date(moment(fromDate, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
          var tempToDate = new Date(moment(toDate, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
          var tempSearchDate = new Date(moment(searchDate, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));

          return tempFromDate <= tempSearchDate && tempToDate >= tempSearchDate
        });
        return availableRoomList;
      }
    },
    // generateHotelRoomList: function (room) {
    //   var roomLists = [];
    //   if (room != undefined && room.length > 0) {
    //     room.forEach(function (listItem) {
    //       if (roomLists.length >= 1) {
    //         var isExist = false;
    //         for (let roomItem of roomLists) {
    //           if (roomItem == listItem.hotelRoomList) {
    //             isExist = true;
    //             break;
    //           }
    //         }
    //         if (!isExist) {
    //           roomLists.push(listItem.hotelRoomList);
    //         }
    //       } else {
    //         roomLists.push(listItem.hotelRoomList);
    //       }
    //     });
    //   }
    //   return roomLists;
    // },
    generateHotelRatingList: function (hotelList) {
      var starRatingList = [];
      if (hotelList != undefined && hotelList.length > 0) {
        hotelList.forEach(function (listItem) {
          if (starRatingList.length >= 1) {
            var isExist = false;
            for (let starRatingItem of starRatingList) {
              if (starRatingItem == listItem.Star_Rating) {
                isExist = true;
                break;
              }
            }
            if (!isExist) {
              starRatingList.push(listItem.Star_Rating);
            }
          } else {
            starRatingList.push(listItem.Star_Rating);
          }
        });
      }
      if (starRatingList.length > 0) {
        starRatingList = this.sortedArray(starRatingList);
      }

      starRatingList.unshift("All");
      return starRatingList;
    },
    generateTransferTypeList: function (transferList) {
      var vehicleTypeList = [];
      if (transferList != undefined && transferList.length > 0) {
        transferList.forEach(function (listItem) {
          if (vehicleTypeList.length >= 1) {
            var isExist = false;
            for (let starRatingItem of vehicleTypeList) {
              if (starRatingItem == listItem.Type) {
                isExist = true;
                break;
              }
            }
            if (!isExist) {
              vehicleTypeList.push(listItem.Type);
            }
          } else {
            vehicleTypeList.push(listItem.Type);
          }
        });
      }
      if (vehicleTypeList.length > 0) {
        vehicleTypeList = this.sortedArray(vehicleTypeList);
      }

      vehicleTypeList.unshift("All");
      return vehicleTypeList;
    },
    generateActivityTypeList: function (activityLis) {
      var activityTypeList = [];
      if (activityLis != undefined && activityLis.length > 0) {
        activityLis.forEach(function (listItem) {
          if (activityTypeList.length >= 1) {
            var isExist = false;
            for (let typeItem of activityTypeList) {
              if (typeItem == listItem.Type) {
                isExist = true;
                break;
              }
            }
            if (!isExist) {
              activityTypeList.push(listItem.Type);
            }
          } else {
            activityTypeList.push(listItem.Type);
          }
        });
      }
      if (activityTypeList.length > 0) {
        activityTypeList = this.sortedArray(activityTypeList);
      }

      activityTypeList.unshift("All");
      return activityTypeList;
    },
    sortedArray: function (list) {
      function compare(a, b) {
        if (a < b)
          return -1;
        if (a > b)
          return 1;
        return 0;
      }

      return list.sort(compare);
    },
    hotelChangeEvent: function (selectedHotel) {
      var self = this;
      if (selectedHotel != undefined) {
        selectedHotel.hotelRoomList = [];
        selectedHotel.hotelRoomList = selectedHotel.Price;
        selectedHotel.hotelSelectedRoom = selectedHotel.Price[0];
        for (var i in selectedHotel.hotelRoomList) {
          if (selectedHotel.hotelRoomList[i].selectedRoomUnit == undefined) {
            selectedHotel.hotelRoomList[i].selectedRoomUnit = 1;
          }
        }
      }
      self.refreshAndCalculate();
    },
    changeRoomUnit: function (selectedRoomUnit, item, room) {
      if (item) {
        for (var i in item) {
          if (item[i].roomName == room.Name) {
            item[i].unitOfRoom = selectedRoomUnit;
          }
        }
        this.refreshAndCalculate();
      }
    },
    hotelRoomChange: function (room, temp, hotelName) {
      var self = this;
      var objRoom = {
        hotelName: hotelName,
        roomName: room.Name,
        price: room.Recommend_Amount,
        selected: room.isRoomSelected,
        unitOfRoom: room.selectedRoomUnit,
        Id: room.Room_ID
      };
      if (room.isRoomSelected == false) {
        for (var i in temp) {
          if (temp[i].roomName == room.Name && temp[i].hotelName == hotelName) {
            temp.splice(i, 1);
            break;
          }
        }
      } else
        temp.push(objRoom);
      self.refreshAndCalculate();
    },
    removeRoom: function (room, temp, roomList) {
      var self = this;
      roomList.forEach(hotel => {
        for (i in hotel.Price) {
          if (hotel.Price[i].isRoomSelected == true && room.Id == hotel.Price[i].Room_ID) {
            hotel.Price[i].isRoomSelected = false;
          }
        }

      });
      for (var i in temp) {
        if (temp[i].roomName == room.roomName && temp[i].hotelName == room.hotelName) {
          temp.splice(i, 1);
        }
      }
      // self.hotelRate -= parseInt(room.price);
      self.refreshAndCalculate();
    },
    removeActivity: function (activity, temp) {
      var self = this;
      for (var i in temp) {
        if (temp[i].Name == activity.Name) {
          temp.splice(i, 1);
        }
      }
      if (activity.isActivitySelected == true) {
        activity.isActivitySelected = false;
      }
      self.refreshAndCalculate();
    },
    activitySelection: function (activity, tempActivity) {
      var self = this;
      const index = tempActivity.indexOf(activity);
      if (index > -1) {
        tempActivity.splice(index, 1);
      } else {
        tempActivity.push(activity);
      }
      self.refreshAndCalculate();
    },
    travelClassChange: function (selectedTravelClass) {
      var self = this;
      if (selectedTravelClass != undefined) {
        selectedTravelClass.transferVehicleList = [];
        selectedTravelClass.transferVehicleList = selectedTravelClass.Price;
        selectedTravelClass.transferSelectedVehicle = selectedTravelClass.Price[0];
        if (selectedTravelClass.transferSelectedVehicle.noOfUnit == undefined) {
          selectedTravelClass.transferSelectedVehicle.noOfUnit = 1;
        }
      }

      self.refreshAndCalculate();
    },
    // changeActivityCategory: function (selectedCategory) {
    //   var self = this;
    //   self.refreshAndCalculate();
    // },
    changeActivityCategory: function (selectedCategory) {
      var self = this;
      if (selectedCategory != undefined) {
        selectedCategory.activityList = [];
        selectedCategory.activityList = selectedCategory.Price;
        for (var i in selectedCategory.activityList) {
          if (selectedCategory.activityList[i].selectedActivityUnit == undefined) {
            selectedCategory.activityList[i].selectedActivityUnit = 1;
          }
        }
      }
      self.refreshAndCalculate();
    },

    hotelChangeRoom: function () {
      var self = this;
      self.refreshAndCalculate();
    },
    vehicleChange: function (item) {
      var self = this;
      if (item.noOfUnit == undefined) {
        item.noOfUnit = 1;
      }
      self.refreshAndCalculate();
    },
    changeTransportationNeed: function (item) {

      var self = this;
      item.isTransportationEnabled = !item.isTransportationEnabled;
      self.refreshAndCalculate();
    },
    changeActivityNeed: function (item) {
      var self = this;
      item.isActivityEnabled = !item.isActivityEnabled;
      self.refreshAndCalculate();
    },
    refreshAndCalculate() {
      var self = this;
      self.packagecontent.Itinerary = [...self.packagecontent.Itinerary];
      self.calculateTotalAmount();
    },
    calculateTotalAmount: function () {

      var self = this;
      var totalAdultRoomAmount = 0;
      var totalChildRoomAmount = 0;
      var totalSelectedTransferVehicleCount = 0;
      var totalSelectedActivityCount = 0;
      var totalTransferAmount = 0;
      var totalActivityAmount = 0;
      var totalSelectedHotelCount = 0;
      var packageAmount = this.packagecontent.Price != undefined ? Number(this.packagecontent.Price) : 0;
      self.packagecontent.Itinerary.forEach(function (arrayItem, index) {
        if (arrayItem.tempSelectedHotelRoomList != undefined && arrayItem.tempSelectedHotelRoomList != null) {
          for (var i in arrayItem.tempSelectedHotelRoomList) {
            if (arrayItem.tempSelectedHotelRoomList[i] != undefined) {
              if (arrayItem.tempSelectedHotelRoomList[i].selected) {
                totalAdultRoomAmount += Number(arrayItem.tempSelectedHotelRoomList[i].price * arrayItem.tempSelectedHotelRoomList[i].unitOfRoom);
                //totalSelectedHotelCount += roomItem.selectedRoomUnit;
              }
            }
          }
          // if (arrayItem.selectedHotel.hotelRoomList != undefined) {

          //   arrayItem.selectedHotel.hotelRoomList.forEach(function (roomItem) {
          //     if (roomItem.isRoomSelected) {
          //       totalAdultRoomAmount += Number(roomItem.Recommend_Amount * roomItem.selectedRoomUnit);
          //       totalSelectedHotelCount += roomItem.selectedRoomUnit;
          //     } 
          //   });

          // }
        }
        if (arrayItem.selectedHotel != undefined && arrayItem.selectedHotel != null) {
          // if (arrayItem.selectedHotel.hotelRoomList != undefined) {

          //   arrayItem.selectedHotel.hotelRoomList.forEach(function (roomItem) {
          //     if (roomItem.isRoomSelected) {
          //       totalAdultRoomAmount += Number(roomItem.Recommend_Amount * roomItem.selectedRoomUnit);
          //       totalSelectedHotelCount += roomItem.selectedRoomUnit;
          //     } 
          //   });



          // }
          if (arrayItem.selectedHotel.hotelSelectedRoom != undefined && arrayItem.selectedHotel.hotelSelectedRoom.Child_Price != undefined &&
            arrayItem.selectedHotel.hotelSelectedRoom.Child_Price != null) {
            //totalChildRoomAmount += (Number(arrayItem.selectedHotel.hotelSelectedRoom.Child_Price) * self.child) ;
            totalChildRoomAmount += Number(arrayItem.selectedHotel.hotelSelectedRoom.Child_Price);
          }
        }
        if (arrayItem.selectedClass != undefined && arrayItem.selectedClass != null && arrayItem.isTransportationEnabled) {
          if (arrayItem.selectedClass.transferSelectedVehicle != undefined && arrayItem.selectedClass.transferSelectedVehicle.Recommend_Amount != undefined &&
            arrayItem.selectedClass.transferSelectedVehicle.Recommend_Amount != null) {
            totalTransferAmount += (Number(arrayItem.selectedClass.transferSelectedVehicle.Recommend_Amount) * arrayItem.selectedClass.transferSelectedVehicle.noOfUnit);
            totalSelectedTransferVehicleCount += arrayItem.selectedClass.transferSelectedVehicle.noOfUnit;
          }
        }
        if (arrayItem.selectedActivityCategory != undefined && arrayItem.selectedActivityCategory != null && arrayItem.isActivityEnabled) {
          if (arrayItem.selectedActivityCategory.activityList != undefined && arrayItem.selectedActivityCategory.activityList.length > 0) {
            self.packagecontent.Itinerary.forEach(function (activityItem) {
              if (activityItem.tempSelectedActivityList != undefined && activityItem.tempSelectedActivityList != null) {
                
                for (var i in activityItem.tempSelectedActivityList) {
                  if (activityItem.tempSelectedActivityList[i] != undefined) {
                    if (activityItem.tempSelectedActivityList[i].isActivitySelected) {
                      totalActivityAmount += Number(activityItem.tempSelectedActivityList[i].Recommend_Amount != null ? (activityItem.tempSelectedActivityList[i].Recommend_Amount * activityItem.tempSelectedActivityList[i].selectedActivityUnit) : 0);
                      totalSelectedActivityCount += activityItem.tempSelectedActivityList[i].selectedActivityUnit;
                      // totalAdultRoomAmount += Number(arrayItem.tempSelectedHotelRoomList[i].price * arrayItem.tempSelectedHotelRoomList[i].unitOfRoom);
                    }
                  }
                }
              
              }
              // if (activityItem.isActivitySelected) {
              //   totalActivityAmount += Number(activityItem.Recommend_Amount != null ? (activityItem.Recommend_Amount * activityItem.selectedActivityUnit) : 0);
              //   totalSelectedActivityCount += activityItem.selectedActivityUnit;
              // }
            });

          }
        }

      });
      self.totalTransportationCount = totalSelectedTransferVehicleCount;
      self.totalActivityCount = totalSelectedActivityCount;
      self.totalTransferAmount = totalTransferAmount;
      self.totalActivityAmount = totalActivityAmount;
      self.childRoomAmount = totalChildRoomAmount;
      self.adultAmount = totalAdultRoomAmount;
      self.packageAmount = packageAmount;
      self.totalFareAmount = totalAdultRoomAmount + packageAmount;
      self.calc();

    },
    getUserPackageData: function () {
      var self = this;
      var selectedHotelTransferDetails = [];
      self.packagecontent.Itinerary.forEach(function (arrayItem, index) {

        var Itinerary = {};
        Itinerary.itineraryOrder = index + 1;
        if (arrayItem.selectedHotel != undefined && arrayItem.selectedHotel != null) {
          // if (arrayItem.selectedHotel.hotelSelectedRoom != undefined && arrayItem.selectedHotel.hotelSelectedRoom.Name != undefined) {
          //   arrayItem.selectedHotel.hotelSelectedRoom.selectedDateFrom=arrayItem.checkIn;
          //   arrayItem.selectedHotel.hotelSelectedRoom.selectedDateTo=arrayItem.checkOut;
          //   Itinerary.selectedHotel = arrayItem.selectedHotel.hotelSelectedRoom;
          // }

          if (arrayItem.selectedHotel.hotelRoomList.length > 0) {
            Itinerary.selectedRoomList = arrayItem.tempSelectedHotelRoomList.filter(function (room) {
              room.selectedDateFrom = arrayItem.checkIn;
              room.selectedDateTo = arrayItem.checkOut;
              return room.selected == true;
            });
          }


        }
        if (arrayItem.selectedClass != undefined && arrayItem.selectedClass != null && arrayItem.isTransportationEnabled) {
          if (arrayItem.selectedClass.transferSelectedVehicle != undefined && arrayItem.selectedClass.transferSelectedVehicle.Name != undefined) {
            arrayItem.selectedClass.transferSelectedVehicle.selectedDateFrom = arrayItem.checkIn;
            arrayItem.selectedClass.transferSelectedVehicle.selectedDateTo = arrayItem.checkOut;
            Itinerary.selectedTransfer = arrayItem.selectedClass.transferSelectedVehicle;
          }
        }
        if (arrayItem.selectedActivityCategory != undefined && arrayItem.selectedActivityCategory.activityList != undefined &&
          arrayItem.selectedActivityCategory.activityList != null && arrayItem.isActivityEnabled) {
          if (arrayItem.selectedActivityCategory.activityList.length > 0) {
            Itinerary.selectedActivityList = arrayItem.selectedActivityCategory.activityList.filter(function (activity) {
              activity.selectedDateFrom = arrayItem.checkIn;
              activity.selectedDateTo = arrayItem.checkOut;
              return activity.isActivitySelected == true;
            });
          }
        }
        if (Itinerary != {}) {
          selectedHotelTransferDetails.push(Itinerary);
        }
      });
      return selectedHotelTransferDetails;
    },
    changeCount: function () {
      this.refreshAndCalculate();
    },
    setDefaultTime: function (date) {
      date.setHours(0);
      date.setMinutes(0);
      date.setSeconds(0);
    },
    getUIDate: function (date) {
      return moment(date, 'YYYY-MM-DD HH:mm:ss').format('DD MMM,HH:mm');
    },

    constructDefaultValues: function (destination, datevalue) {
      var self = this;
      self.destination = destination;
      let dateObj = document.getElementById("from");
      dateObj.value = datevalue;
      var tourDate = new Date(moment(dateObj.value, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
      self.setDefaultTime(tourDate);
      // if (self.allMappedHotels != undefined && destination != undefined) {
      //   self.allMappedHotels = self.allMappedHotels.filter(function (flight) {
      //     var fromdate = new Date(moment(flight.From_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
      //     var toDate = new Date(moment(flight.To_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
      //     self.setDefaultTime(fromdate);
      //     self.setDefaultTime(toDate);
      //     return flight.Destination == destination && flight.Active == true && tourDate.getTime() >= fromdate.getTime() && toDate.getTime() >= tourDate.getTime();
      //   });
      // }

      //   // this.allMappedFlights.forEach(element => {
      //   //   var isthere=false;
      //   //   var sourceTime="";

      //   //   for(var k=0;k<allDeparturePlace.length;k++){
      //   //     if(allDeparturePlace[k].Name==element.Source){
      //   //       isthere=true;
      //   //       break;
      //   //     }
      //   //   }
      //   //   if(isthere==false){
      //   //     allDeparturePlace.push({Name:element.Source});
      //   //   }
      //   // });
      //   // self.allMappedHotels.forEach(element => {
      //   //   var isthere = false;
      //   //   for (var k = 0; k < allStarPlace.length; k++) {
      //   //     if (allStarPlace[k].Id == element.Star_Rating) {
      //   //       isthere = true;
      //   //       break;
      //   //     }
      //   //   }
      //   //   if (isthere == false) {
      //   //     allStarPlace.push({ Name: element.Star_Rating + " Star", Id: element.Star_Rating });
      //   //   }
      //   // });
      //   // if(allDeparturePlace.length>0){
      //   //   this.selectedDeparture=allDeparturePlace[0];
      //   // }
      //   allStarPlace.sort(function (a, b) {
      //     return Number(a.Id) - Number(b.Id);
      //   });

      //   allStarPlace = allStarPlace.sort(function (a, b) { return a.Id < b.Id });
      //   if (allStarPlace.length > 0) {
      //     self.selectedStar = allStarPlace[0];
      //   }
      //   // this.departureChangeEvent();
      //   self.startRatingChangeEvent();
      //   // this.allDepartureList=allDeparturePlace;
      //   self.allStarList = allStarPlace;


      // },
      // startRatingChangeEvent: function (item) {
      //   var allHotelListTemp = [];
      //   var selectedHotelTemp;
      //   var self = this;
      //   self.allMappedHotels.forEach(element => {

      //     if (element.Star_Rating != undefined    ) {
      //       var isthere = false;
      //       for (var k = 0; k < allHotelListTemp.length; k++) {
      //         if (allHotelListTemp[k].Hotel_Name == element.Hotel_Name) {
      //           isthere = true;
      //           break;
      //         }
      //       }
      //       if (isthere == false) {
      //         allHotelListTemp.push(element);
      //       }
      //     }
      //   });
      //   if (allHotelListTemp.length > 0) {
      //     selectedHotelTemp = allHotelListTemp[0];

      //   }
      //   self.selectedHotel = selectedHotelTemp;
      //   self.hotelChangeEvent();
      //   self.allHotelList = allHotelListTemp;

    },
    startRatingChangeEvent: function (item) {
      if (item != undefined) {
        if (item.selectedStar == 'All') {
          item.Hotels = item.currentHotelList;
        } else {
          var ratingFilterHotelList = item.currentHotelList.filter(function (Hotel) {
            return Hotel.Star_Rating == item.selectedStar;
          });
          item.Hotels = ratingFilterHotelList;
          if (item.Hotels != undefined && item.Hotels.length > 0) {
            item.selectedHotel = item.Hotels[0];
            item.selectedHotel.hotelRoomList = item.selectedHotel.Price != undefined ? item.selectedHotel.Price : [];
          }
        }
      }
      this.refreshAndCalculate();
    },

    transferTypeChange: function (item) {
      if (item != undefined) {
        if (item.selectedTransferType == 'All') {
          item.Transfer = item.currentTransferList;
        } else {
          var typeFilterTrasferList = item.currentTransferList.filter(function (transfer) {
            return transfer.Type == item.selectedTransferType;
          });
          item.Transfer = typeFilterTrasferList;
          if (item.Transfer != undefined && item.Transfer.length > 0) {
            item.selectedClass = item.Transfer[0];
            item.selectedClass.transferVehicleList = item.selectedClass.Price != undefined ? item.selectedClass.Price : [];
            item.selectedClass.transferSelectedVehicle = item.selectedClass.transferVehicleList.length > 0 ? item.selectedClass.transferVehicleList[0] : {};
            if (item.selectedClass.transferSelectedVehicle.noOfUnit == undefined) {
              item.selectedClass.transferSelectedVehicle.noOfUnit = 1;
            }

          }
        }
      }
      this.refreshAndCalculate();
    },
    changeActivityType: function (item) {
      if (item != undefined) {
        if (item.selectedActivityType == 'All') {
          item.Activity = item.currentActivityList;
          if (item.Activity != undefined && item.Activity.length > 0) {
            item.selectedActivityCategory = item.Activity[0];
            item.selectedActivityCategory.activityList = item.selectedActivityCategory.Price != undefined ? item.selectedActivityCategory.Price : [];
            item.selectedActivityCategory.activityList.forEach(activityRow => {
              if (activityRow.selectedActivityUnit == undefined) {
                activityRow.selectedActivityUnit = 1;
                activityRow.isActivitySelected = false;
              }
            });

          }
        } else {
          var typeFilterActivityList = item.currentActivityList.filter(function (activity) {
            return activity.Type == item.selectedActivityType;
          });
          item.Activity = typeFilterActivityList;
          if (item.Activity != undefined && item.Activity.length > 0) {
            item.selectedActivityCategory = item.Activity[0];
            item.selectedActivityCategory.activityList = item.selectedActivityCategory.Price != undefined ? item.selectedActivityCategory.Price : [];
            item.selectedActivityCategory.activityList.forEach(activityRow => {
              if (activityRow.selectedActivityUnit == undefined) {
                activityRow.selectedActivityUnit = 1;
                activityRow.isActivitySelected = false;
              }
            });

          }
        }
      }
      this.refreshAndCalculate();
    },
    // hotelChangeEvent: function () {
    //   var allNightListTemp = [];
    //   var self = this;
    //   var selectedNightTemp = {};
    //   let dateObj = document.getElementById("from").value;
    //   var tourDate = new Date(moment(dateObj, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
    //   self.setDefaultTime(tourDate);
    //   if (this.selectedHotel != undefined && this.selectedHotel.Price != undefined) {
    //     for (var i = 0; i < this.selectedHotel.Price.length; i++) {
    //       var element = this.selectedHotel.Price[i];
    //       var fromdate = new Date(moment(element.From_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
    //       var toDate = new Date(moment(element.To_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
    //       self.setDefaultTime(fromdate);
    //       self.setDefaultTime(toDate);
    //       if (tourDate.getTime() >= fromdate.getTime() && toDate.getTime() >= tourDate.getTime()) {
    //         allNightListTemp.push(element);
    //       }
    //     }

    //   }
    //   if (allNightListTemp.length > 0) {
    //     selectedNightTemp = allNightListTemp[0];

    //   }


    //   this.selectedNight = selectedNightTemp;
    //   this.allNightList = allNightListTemp;
    //   this.nightChangeEvent();


    // },

    hotelCheckinDateChangeEvent: function () {
      let dateObj = document.getElementById("from").value;
      var checkinDate = new Date(moment(dateObj, 'DD/MM/YYYY').format('YYYY-MM-DD'));
      if (this.selectedAirLine != undefined && this.selectedAirLine.data != undefined && this.selectedAirLine.data.Flight_Details != undefined && this.selectedAirLine.data.Flight_Details.length > 0) {
        var flight = this.selectedAirLine.data.Flight_Details[this.selectedAirLine.data.Flight_Details.length - 1];
        checkinDate = new Date(flight.Destination_Date);
      }
      var checkin = new Date(moment(checkinDate, 'YYYY-MM-DD').format('DD-MMMM-YYYY'));
      var checkOut = new Date(moment(checkinDate, 'YYYY-MM-DD').format('DD-MMMM-YYYY'));
      checkOut.setDate(checkOut.getDate() + Number(this.selectedNight.Nights));
      if (this.selectedHotel != undefined) {
        this.selectedHotel.checkin = moment(checkin).format('DD MMMM YYYY');;
        this.selectedHotel.checkOut = moment(checkOut).format('DD MMMM YYYY');
      }

    },
    nightChangeEvent: function () {
      var self = this;
      this.calc();
      this.hotelCheckinDateChangeEvent();
      this.$nextTick(function () {
        self.callOwl();
      }.bind(self));
    },
    callOwl: function () {

      $("#htl-in-item").owlCarousel({
        autoplay: true,
        autoPlay: 8000,
        autoplayHoverPause: true,
        stopOnHover: false,
        items: 1,
        margin: 10,
        lazyLoad: true,
        navigation: true,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [991, 1],
        itemsTablet: [600, 1]
      });

      $(".owl-prev").html('<i class="fa  fa-angle-left"></i>');
      $(".owl-next").html('<i class="fa  fa-angle-right"></i>');

    },
    departureChangeEvent: function () {
      var allAirlinesPlace = [];
      var selectedAirLineTemp = {};




      this.allMappedFlights.forEach(element => {
        if (element.Flight_Details != undefined && element.Flight_Details.length > 0 && element.Source == this.selectedDeparture.Name) {
          let dateObj = document.getElementById("from").value;
          var tourDate = new Date(moment(dateObj, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
          var isthere = false;
          var sourceTime = "";
          if (element.Flight_Details.length > 0) {
            var fromdate = new Date(moment(element.Flight_Details[0].Source_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
            sourceTime = moment(fromdate).format('HH:mm');
          }
          if (sourceTime != undefined && sourceTime != '') {
            sourceTime = "(" + sourceTime + ")";
          }

          for (let i = 0; i < element.Flight_Details.length; i++) {
            var flightObj = element.Flight_Details[i];
            if (i == 0) {
              var fromdate = new Date(moment(flightObj.Source_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
              var toDate = new Date(moment(flightObj.Destination_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
              flightObj.Source_Date = moment(tourDate, 'YYYY-MM-DD[T]HH:mm:ss').format('YYYY-MM-DD') + " " + flightObj.Source_Date.split(' ')[1];
              var diffDays = toDate.getDate() - fromdate.getDate();
              tourDate.setDate(tourDate.getDate() + diffDays);
              flightObj.Destination_DateORG = flightObj.Destination_Date;
              flightObj.Destination_Date = moment(tourDate, 'YYYY-MM-DD[T]HH:mm:ss').format('YYYY-MM-DD') + " " + flightObj.Destination_Date.split(' ')[1];
            } else {
              var prevFlight = element.Flight_Details[i - 1];
              var fromdateSRC = new Date(moment(prevFlight.Destination_DateORG).format('YYYY-MM-DD[T]HH:mm:ss'));
              var toDateSRC = new Date(moment(flightObj.Source_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
              var diffDays = toDateSRC.getDate() - fromdateSRC.getDate();
              tourDate.setDate(tourDate.getDate() + diffDays);

              var fromdate = new Date(moment(flightObj.Source_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
              var toDate = new Date(moment(flightObj.Destination_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
              diffDays = toDate.getDate() - fromdate.getDate();
              flightObj.Source_Date = moment(tourDate, 'YYYY-MM-DD[T]HH:mm:ss').format('YYYY-MM-DD') + " " + flightObj.Source_Date.split(' ')[1];
              tourDate.setDate(tourDate.getDate() + diffDays);
              flightObj.Destination_DateORG = flightObj.Destination_Date;
              flightObj.Destination_Date = moment(tourDate, 'YYYY-MM-DD[T]HH:mm:ss').format('YYYY-MM-DD') + " " + flightObj.Destination_Date.split(' ')[1];
            }
          }
          var flightOption = element.Flight_Details[0];
          for (var k = 0; k < allAirlinesPlace.length; k++) {
            if (allAirlinesPlace[k].Name == flightOption.Air_Line_Name + sourceTime) {
              isthere = true;
              break;
            }
          }


          if (isthere == false) {
            allAirlinesPlace.push({
              Name: flightOption.Air_Line_Name + sourceTime,
              data: element
            });
          }
        }



      });
      if (allAirlinesPlace.length > 0) {
        selectedAirLineTemp = allAirlinesPlace[0];

      }
      this.selectedAirLine = selectedAirLineTemp;
      this.allAirlineList = allAirlinesPlace;
      this.flightChangeEvent();
    },
    flightChangeEvent: function () {
      this.calc();
      this.hotelCheckinDateChangeEvent();
    },
    getAmount: function (amount) {
      amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
      return amount;
    },
    sendbooking: async function () {
      let dateObj = document.getElementById("from");
      let dateValue = dateObj.value;
      datevalue = moment(dateValue, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
      // dateValue = moment(String(dateValue)).format('YYYY-MM-DDThh:mm:ss');
      if (!this.cntusername) {
        alertify.alert(this.alerttypes.warning, this.alerts.Name_Alert_Message).set('closable', false);

        return false;
      }
      if (!this.cntemail) {
        alertify.alert(this.alerttypes.warning, this.alerts.Email_Alert_Message).set('closable', false);
        return false;
      }
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.cntemail.match(emailPat);
      if (matchArray == null) {
        alertify.alert(this.alerttypes.warning, this.alerts.Incorrect_Mail_Alert_Message).set('closable', false);
        return false;
      }
      if (!this.cntcontact) {
        alertify.alert(this.alerttypes.warning, this.alerts.Phone_Number_Alert_Message).set('closable', false);
        return false;
      }
      if (this.cntcontact.length < 8) {
        alertify.alert(this.alerttypes.warning, this.alerts.Valid_Phone_Number_Message).set('closable', false);
        return false;
      }
      if (dateValue == undefined || dateValue == '') {
        alertify.alert(this.alerttypes.warning, this.alerts.Tour_Date_Alert_Message).set('closable', false);
        return false;
      } else {
        this.isLoading = true;
        var frommail = JSON.parse(localStorage.User).loginNode.email;
        var custmail = {
          type: "UserAddedRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.cntusername,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;

        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');

        var originalPrice = this.grandtotal;
        if (this.CoupenInfo.ApplyCoupen == true) {
          originalPrice = this.CoupenInfo.newPrice;
        }
        var couponprice = 0.00;
        if (this.CoupenInfo.ApplyCoupen == true) {
          couponprice = this.CoupenInfo.Amount;
        }
        var cocode = "N/A";
        if (this.CoupenInfo.ApplyCoupen == true) {
          cocode = this.CoupenInfo.CoupenCode;
        }
        var percent = 0.00;
        var percentamt = 0.00;
        if (this.CoupenInfo.discountpercent == true) {
          percent = this.CoupenInfo.DisPercent;
          percentamt = this.CoupenInfo.Disamount;
        }
        packageDetails = this.getUserPackageData();
        console.log("details", packageDetails);
        let insertContactData = {
          type: "Booking",
          keyword1: this.cntusername,
          keyword2: this.cntemail,
          keyword4: this.cntcontact,
          keyword5: this.packagecontent.Title,
          keyword6: this.selectedCurrency,
          keyword7: this.packagecontent.Days,
          keyword8: this.packagecontent.Night,
          number2: this.cntperson,
          date1: datevalue,
          nodeCode: agencyCode,
          date2: requestedDate,
          amount1: couponprice,
          amount2: originalPrice,
          amount3: this.grandtotal,
          nested1: packageDetails,
          number1: this.child,
          amount4: this.childRoomAmount,
          amount5: this.totalTransferAmount,
          number3: this.totalTransportationCount,
          amount6: this.totalActivityAmount,
          number4: this.totalActivityCount,
          amount7: this.packageAmount,
          amount8: this.adultAmount
          //keyword3: cocode,

        };
        let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
          let insertID = Number(responseObject);
          var emailApi = ServiceUrls.emailServices.emailApi;
          sendMailService(emailApi, custmail);
          this.cntemail = '';
          this.cntusername = '';
          this.cntcontact = '';
          this.cntdate = '';
          this.CoupenInfo.ApplyCoupen = false;
          this.CoupenInfo.Amount = "";
          this.CoupenInfo.discountpercent = false;
          this.CoupenInfo.CoupenCode = "";
          this.CoupenInfo.newPrice = "";
          this.CoupenCode = "";
          dateObj.value = null;
          $('#travel').val($('#travel option:first-child').val()).trigger('change');
          this.isLoading = false;
          alertify.alert('Booking', this.alerts.Success_Booking_Message).set('onok', function (closeEvent) {
            window.location.href = "packages.html"
          });
        } catch (e) {

        }



      }
    },
    calculateFlightAndHotelAmount: function () {
      var flightRecommendAmount = 0;
      var flightActualAmount = 0;
      var hotelRecommendAmount = 0;
      var hotelActualAmount = 0;
      if (this.selectedAirLine.data != undefined && this.selectedAirLine.data.Recommend_Amount != undefined && this.selectedAirLine.data.Actual_Amount != undefined) {
        flightRecommendAmount = this.selectedAirLine.data.Recommend_Amount;
        flightActualAmount = this.selectedAirLine.data.Actual_Amount;
      }
      if (this.selectedNight != undefined && this.selectedNight.Recommend_Amount != undefined && this.selectedNight.Actual_Amount != undefined) {
        hotelRecommendAmount = this.selectedNight.Recommend_Amount;
        hotelActualAmount = this.selectedNight.Actual_Amount;
      }
      var totalRecommendAmount = Number(flightRecommendAmount) + Number(hotelRecommendAmount);
      var totalActualAmount = Number(flightActualAmount) + Number(hotelActualAmount);
      if (this.packagecontent.Basic_Price != undefined && this.packagecontent.Basic_Price != '') {
        this.totalFareAmount = Number(this.packagecontent.Basic_Price) + Number(totalActualAmount);
      } else {
        this.totalFareAmount = this.packagecontent.Price;
      }
      if (this.allNightList.length == 0) {
        this.totalFareAmount = this.packagecontent.Price;
      }
    },
    calc: function () {
      // this.calculateFlightAndHotelAmount();
      // this.grandtotal = this.cntperson * this.totalFareAmount;
      this.adultTotal = this.cntperson * this.totalFareAmount;
      this.Childtotal = this.child * (Number(this.packagecontent.ChildPrice) + this.childRoomAmount);
      this.grandtotal = this.adultTotal + this.Childtotal + this.totalTransferAmount + this.totalActivityAmount;
      this.CoupenInfo.ApplyCoupen = false;
      this.CoupenInfo.Amount = "";
      this.CoupenInfo.discountpercent = false;
      this.CoupenInfo.CoupenCode = "";
      this.CoupenInfo.newPrice = "";
      this.CoupenCode = "";
      // alert("hai");
    },

    sendReview: async function () {
      let ratings = this.getUserRating();
      console.log(ratings);
      ratings = Number(ratings);
      if (ratings == 0) {
        alertify.alert(this.alerttypes.warning, this.alerts.Rating_Alert_Message);
        return;
      }
      if (!this.username) {
        alertify.alert(this.alerttypes.warning, this.alerts.Name_Alert_Message).set('closable', false);

        return false;
      }
      if (!this.email) {
        alertify.alert(this.alerttypes.warning, this.alerts.Email_Alert_Message).set('closable', false);
        return false;
      }
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.email.match(emailPat);
      if (matchArray == null) {
        alertify.alert(this.alerttypes.warning, this.alerts.Incorrect_Mail_Alert_Message).set('closable', false);
        return false;
      }
      if (this.contact.length < 8) {
        alertify.alert(this.alerttypes.warning, this.alerts.Valid_Phone_Number_Message).set('closable', false);
        return false;
      }
      if (!this.contact) {
        alertify.alert(this.alerttypes.warning, this.alerts.Phone_Number_Alert_Message).set('closable', false);
        return false;
      }

      if (!this.comment) {
        alertify.alert(this.alerttypes.warning, this.alerts.Comment_Alert_Message).set('closable', false);
        return false;
      } else {
        this.isLoading = true;
        var frommail = JSON.parse(localStorage.User).loginNode.email;
        var custmail = {
          type: "UserAddedRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(this.email) ? this.email : [this.email],
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.username,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
          type: "Reviews",
          keyword1: this.username,
          keyword2: this.email,
          keyword3: this.contact,
          keyword4: this.pageURLLink,
          text1: this.comment,
          keyword5: this.packagecontent.Title,
          number1: ratings,
          nodeCode: agencyCode,
          date1: requestedDate,
        };
        let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
          let insertID = Number(responseObject);
          var emailApi = ServiceUrls.emailServices.emailApi;
          sendMailService(emailApi, custmail);
          this.email = '';
          this.username = '';
          this.contact = '';
          this.comment = '';
          this.isLoading = false;
          alertify.alert('Review', this.alerts.Review_Thanks_Message);
        } catch (e) {

        }



      }
    },
    viewReview: async function () {
      var self = this;
      let allReview = [];
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
      let requestObject = {
        from: 0,
        size: 100,
        type: "Reviews",
        nodeCode: agencyCode,
        orderBy: "desc"
      };
      let responseObject = await this.cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
        if (responseObject != undefined && responseObject.data != undefined) {
          allResult = JSON.parse(JSON.stringify(responseObject)).data;
          for (let i = 0; i < allResult.length; i++) {
            if (allResult[i].keyword4 == self.pageURLLink) {
              let object = {
                Name: allResult[i].keyword1,
                Date: moment(String(allResult[i].date1), "YYYY-MM-DDThh:mm:ss").format('MMM DD,YYYY'),
                comment: allResult[i].text1,
                Ratings: allResult[i].number1,
              };
              allReview.push(object);
            }
          }
          self.allReview = allReview;
        }

      });
    },
    userRating: function (num) {
      var ulList = document.getElementById("ratingID");
      let m = 0;
      for (let i = 0; i < ulList.childNodes.length; i++) {
        let childNode = ulList.childNodes[i];
        if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
          m = m + 1;
          for (let k = 0; k < childNode.childNodes.length; k++) {
            let style = childNode.childNodes[k].style.color;
            if ((m) < Number(num)) {
              childNode.childNodes[k].style = "color: rgb(239, 158, 8)";
            } else if ((m) == Number(num)) {
              if (style.trim() == "rgb(239, 158, 8)") {
                childNode.childNodes[k].style = "color: a9a9a9;";
              } else {
                childNode.childNodes[k].style = "color: rgb(239, 158, 8);";
              }
            } else {
              childNode.childNodes[k].style = "color: a9a9a9";
            }
          }
        }
      }
    },
    getUserRating: function () {
      var ulList = document.getElementById("ratingID");
      let m = 0;
      for (let i = 0; i < ulList.childNodes.length; i++) {
        let childNode = ulList.childNodes[i];
        if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
          let style = "";
          for (let k = 0; k < childNode.childNodes.length; k++) {
            style = childNode.childNodes[k].style.color;
            if (style != undefined && style != '') {
              break;
            }
          }
          if (style.trim() == "a9a9a9") {
            break;
          } else if (style.trim() == "rgb(239, 158, 8)") {
            m = m + 1;
          }
        }
      }
      return m;
    },

    async cmsRequestData(callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
        },
        body: data, // body data type must match "Content-Type" header
      });
      try {
        const myJson = await response.json();
        return myJson;
      } catch (error) {
        return object;
      }
    },
    // setip: function () {
    //   var self = this;
    //   $.getJSON("https://api.ipify.org?format=json", function (data) {
    //     $("#ipaddress").html(data.ip);
    //     self.ipAddres = data.ip;
    //     self.sendip();
    //   })
    // },
    // sessionid: function () {
    //   var GUID = function () {
    //     var S4 = function () {
    //       return (
    //         Math.floor(
    //           Math.random() * 0x10000 /* 65536 */
    //         ).toString(16)
    //       );
    //     };
    //     return (
    //       S4() + S4() + "-" +
    //       S4() + "-" +
    //       S4() + "-" +
    //       S4() + "-" +
    //       S4() + S4() + S4()
    //     );
    //   };
    //   if (!window.name.match(/^G/)) {
    //     window.name = "G" + GUID();
    //   }
    //   //window.name = window.name.replace(/^GUID-/, "");
    //   this.sessionids = window.name;
    // },
    // sendip: async function () {

    //   let agencyCode = JSON.parse(localStorage.User).loginNode.code;
    //   let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
    //   let insertContactData = {
    //     type: "Visit",
    //     keyword1:this.sessionids,
    //     ip1: this.ipAddres,
    //     keyword2:"Package Details",
    //     keyword3:this.packagecontent.Title,
    //     date1: requestedDate,
    //     nodeCode: agencyCode
    //   };
    //   let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
    //   try {
    //     let insertID = Number(responseObject);
    //   } catch (e) {

    //   }

    // },
    getGlobalcoupencode: async function () {
      var self = this;
      var CoupenCode = self.CoupenCode;
      var packamt = self.grandtotal;
      if (CoupenCode == "") {
        alertify.alert(self.alerttypes.warning, self.alerts.Enter_Coupon_Message);
      } else {
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;

        let dateObj = document.getElementById("from");
        let datevalue = "CURDATE()"; //moment(dateObj.value, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
        if (datevalue == undefined || datevalue == '') {
          datevalue = "CURDATE()";
        }
        var query = "select * from cms_forms_data  where  type='Coupon code-All Packages' AND nodeCode='#NodeCode#' AND amount2<=#priceAmount# AND  amount3>=#priceAmount# AND keyword3='True' AND keyword1='#Code#' AND date2 >= #TravelDate# AND date1<=#TravelDate# ";
        query = query.replace("#NodeCode#", agencyCode).replace("#priceAmount#", packamt).replace("#priceAmount#", packamt).replace("#Code#", CoupenCode).replace("#TravelDate#", datevalue).replace("#TravelDate#", datevalue);
        let postData = {
          from: 0,
          sortField: 'nodeCode',
          orderBy: 'asc',
          query: query
        };

        let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", postData, null);

        if (responseObject != undefined && responseObject.data != undefined && responseObject.data.length > 0 && (responseObject.data[0].amount1 != undefined || responseObject.data[0].amount4 != undefined)) {
          var Result = responseObject.data[0];
          self.CoupenInfo.Amount = Result.amount1;
          self.CoupenInfo.DisPercent = Result.amount4;
          self.CoupenInfo.ApplyCoupen = true;
          self.CoupenInfo.CoupenCode = CoupenCode;
          var cuurntPrice = self.grandtotal;
          if (self.CoupenInfo.DisPercent != undefined && self.CoupenInfo.DisPercent != '') {
            self.CoupenInfo.discountpercent = true;
            self.CoupenInfo.Disamount = cuurntPrice * (self.CoupenInfo.DisPercent / 100);
            var percentageamount = self.CoupenInfo.Disamount;
            self.CoupenInfo.newPrice = parseFloat(cuurntPrice) - (parseFloat(self.CoupenInfo.Amount) + percentageamount);
          } else {
            self.CoupenInfo.discountpercent = false;
            self.CoupenInfo.newPrice = parseFloat(cuurntPrice) - (parseFloat(self.CoupenInfo.Amount));
          }
        } else {
          alertify.alert(self.alerttypes.warning, self.alerts.Coupon_Error_Alert_Message);
          self.CoupenInfo.ApplyCoupen = false;
          self.CoupenInfo.Amount = "";
          self.CoupenInfo.discountpercent = false;
          self.CoupenInfo.CoupenCode = "";
          self.CoupenInfo.newPrice = "";
          self.CoupenInfo.Disamount = "";
        }




        /*var query = {
          _source: [
            "keyword1",
            "date1",
            "date2",
            "amount1",
            "keyword3",
            "amount2",
            "amount3",
            "amount4"
          ],
          query: {
            bool: {
              filter: [
                {
                  match_phrase: {
                    keyword1: {
                      query: CoupenCode
                    }
                  }
                },
                {
                  range: {
                    date1: {
                      lte: "now"
                    }
                  }
                },
                {
                  range: {
                    date2: {
                      gte: "now"
                    }
                  }
                },
                {
                  range: {
                    amount2: {
                      lte: packamt
                    }
                  }
                },
                {
                  range: {
                    amount3: {
                      gte: packamt
                    }
                  }
                },
                {
                  match_phrase: {
                    type: {
                      query: "Coupon code-All Packages"
                    }
                  }
                },
                {
                  match_phrase: {
                    nodeCode: {
                      query: agencyCode
                    }
                  }
                },
                {
                  match_phrase: {
                    keyword3: {
                      query: "True"
                    }
                  }
                }
              ]
            }
          }

        };
        var client = new elasticsearch.Client({
          host: [{
            host: ServiceUrls.elasticSearch.elasticsearchHost,
            auth: "a2z:agy435",
            protocol: ServiceUrls.elasticSearch.protocol,
            port: ServiceUrls.elasticSearch.port,
            requestTimeout: 60000
          }],
          log: 'trace'
        });
        client.search({ index: 'cms_forms_data', size: 5, pretty: true, filter_path: 'hits.hits._source', body: query }).then(function (resp) {
          if (isEmpty(resp)) {
            alertify.alert(self.alerttypes.warning, self.alerts.Coupon_Error_Alert_Message);
            self.CoupenInfo.ApplyCoupen = false;
            self.CoupenInfo.Amount = "";
            self.CoupenInfo.discountpercent = false;
            self.CoupenInfo.CoupenCode = "";
            self.CoupenInfo.newPrice = "";
            self.CoupenInfo.Disamount = "";
          }
          else {
            console.log("coupencodeGlobal", resp);
            var Result = resp.hits.hits;
            if (Result.length > 0) {
              Result = Result.find((element) => {
                return (element._source.amount1 != null || element._source.amount1 != null);
              });
              console.log(Result);
              self.CoupenInfo.Amount = Result._source.amount1;
              self.CoupenInfo.DisPercent = Result._source.amount4;
              self.CoupenInfo.ApplyCoupen = true;
              self.CoupenInfo.CoupenCode = CoupenCode;
              var cuurntPrice = self.grandtotal;
              // self.CoupenInfo.Disamount=cuurntPrice*(self.CoupenInfo.DisPercent/100);
              // var percentageamount=self.CoupenInfo.Disamount;
              self.CoupenInfo.newPrice = parseFloat(cuurntPrice) - (parseFloat(self.CoupenInfo.Amount));

              if (self.CoupenInfo.DisPercent != null || self.CoupenInfo.DisPercent != null) {
                self.CoupenInfo.discountpercent = true;
                var disResult = resp.hits.hits;
                if (disResult.length > 0) {
                  disResult = disResult.find((element) => {
                    return (element._source.amount1 != null || element._source.amount1 != null || element._source.amount4 != null || element._source.amount4 != null);
                  });

                  self.CoupenInfo.Amount = disResult._source.amount1;
                  self.CoupenInfo.DisPercent = disResult._source.amount4;
                  self.CoupenInfo.ApplyCoupen = true;
                  self.CoupenInfo.CoupenCode = CoupenCode;
                  var cuurntPrice = self.grandtotal;
                  self.CoupenInfo.Disamount = cuurntPrice * (self.CoupenInfo.DisPercent / 100);
                  var percentageamount = self.CoupenInfo.Disamount;
                  self.CoupenInfo.newPrice = parseFloat(cuurntPrice) - (parseFloat(self.CoupenInfo.Amount) + percentageamount);
                }
              }

            }
          }

        });*/

      }
    },
    clearCoupon: function () {
      var self = this;
      self.CoupenInfo.ApplyCoupen = false;
      self.CoupenInfo.discountpercent = false;
      self.CoupenInfo.Amount = "";
      self.CoupenInfo.DisPercent = "";
      self.CoupenInfo.CoupenCode = "";
      self.CoupenInfo.newPrice = "";
      self.CoupenCode = "";
    },

    Travellercounts: function () {
      var arrayLength = this.Traveller_Limit_Count;
      var childArrayLength = this.Child_Limit_Count;
      var TransportationArrayLength = this.Transportation_Count_Limit;
      var ActivityArrayLength = this.Activity_Count_Limit;
      var count = [];
      var childCount = [];
      var TransportationCount = [];
      var ActivityCount = []
      for (var i = 1; i <= arrayLength; i++) {
        count.push(i);
      }
      this.Travellercount = count;

      for (var i = 0; i <= childArrayLength; i++) {
        childCount.push(i);
      }
      this.ChildCount = childCount;

      for (var i = 1; i <= TransportationArrayLength; i++) {
        TransportationCount.push(i);
      }
      this.TransportationCount = TransportationCount;

      for (var i = 1; i <= ActivityArrayLength; i++) {
        ActivityCount.push(i);
      }
      this.ActivityCount = ActivityCount;

    },


    // getHotels:function(){
    //   var self = this;
    //   allMappedHotelsTemp = self.packagecontent.Itinerary[0].Hotels.filter(function(Hotel) {
    //     return Hotel.Active == true
    //   });
    //   self.allMappedHotel = allMappedHotelsTemp;
    //   this.startRatingChangeEvents();
    // },
  },
  // startRatingChangeEvents:function(){
  //   var allHotelListTemp=[];
  //   var selectedHotelTemp;
  //   var self=this;
  //   this.allMappedHotel.forEach(element => {

  //     if(element.Star_Rating!=undefined&&element.Star_Rating==this.selectedStar.Id){
  //       var isthere=false;
  //       for(var k=0;k<allHotelListTemp.length;k++){
  //         if(allHotelListTemp[k].Hotel_Name==element.Hotel_Name){
  //           isthere=true;
  //           break;
  //         }
  //       }
  //       if(isthere==false){
  //         allHotelListTemp.push(element);
  //       }
  //     }
  //   });
  //   if(allHotelListTemp.length>0){
  //     selectedHotelTemp=allHotelListTemp[0];

  //   }
  //   this.selectedHotel=selectedHotelTemp;
  //   this.hotelChangeEvent();
  //   this.allHotelList=allHotelListTemp;

  // },
})

function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

function responsivetab() {
  //Horizontal Tab
  $('#parentHorizontalTab').easyResponsiveTabs({
    type: 'default', //Types: default, vertical, accordion
    width: 'auto', //auto or any width like 600px
    fit: true, // 100% fit in a container
    tabidentify: 'hor_1', // The tab groups identifier
    activate: function (event) { // Callback function if tab is switched
      var $tab = $(this);
      var $info = $('#nested-tabInfo');
      var $name = $('span', $info);
      $name.text($tab.text());
      $info.show();
    }
  });

  // Child Tab
  $('#ChildVerticalTab_1').easyResponsiveTabs({
    type: 'vertical',
    width: 'auto',
    fit: true,
    tabidentify: 'ver_1', // The tab groups identifier
    activetab_bg: '#fff', // background color for active tabs in this group
    inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
    active_border_color: '#c1c1c1', // border color for active tabs heads in this group
    active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
  });

  //Vertical Tab
  $('#parentVerticalTab').easyResponsiveTabs({
    type: 'vertical', //Types: default, vertical, accordion
    width: 'auto', //auto or any width like 600px
    fit: true, // 100% fit in a container
    closed: 'accordion', // Start closed if in accordion view
    tabidentify: 'hor_1', // The tab groups identifier
    activate: function (event) { // Callback function if tab is switched
      var $tab = $(this);
      var $info = $('#nested-tabInfo2');
      var $name = $('span', $info);
      $name.text($tab.text());
      $info.show();
    }
  });
}

function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
}