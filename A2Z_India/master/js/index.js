// var header = new Vue({
//     //i18n,
//     el: "#header",    
//     data: {
//         selectedLanguage: 'en',
//         content: {
//             "response": {
//               "general": {
//                 "bannerImage": "../assets/images/banner.jpg",
//                 "logo": "../assets/images/logo.png",
//                 "address": "",
//                 "agencyName": "a2zBooking",
//                 "emailID": "",
//                 "contactNo": "",
//                 "copyRight": ""
//               }
//             }
//         }
//         // selectedCurrency: Vue_localization.defaultCurrency,
//         // languageOptions: Vue_localization.languageOptions,
//         // CurrencyOptions: Vue_localization.CurrencyOptions
//     },
//     methods: {
//         onChange(value) {
//             // this.selectedCurrency=value;
//             // test.selectedCurrency=this.selectedCurrency;
//         }
//     }
// });


// var test = new Vue({
//     //i18n,
//     el: "#testDiv",
//     data: {
//         selectedLanguage: 'en',
//         selectedCurrency: header.selectedCurrency
//     }
// });

var cmsapp = new Vue({
  el: '#cmsdiv',
  name: 'cms',
  data() {
    return {
      key: '',
      content: null,
      getdata: true,
      dir: 'ltr',
      ipAddres: '',
      sessionids: '',
      Socialmedia: {},
      isLoading: false,
      fullPage: true,
    }
  },
  mounted: function () {
    // this.setip();
    this.getcmsdata();


  },
  methods: {


    moment: function () {
      return moment();
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getcmsdata: function () {
      var self = this;
      self.isLoading = true;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        self.dir = langauage == "ar" ? "rtl" : "ltr";
        var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Home/Home/Home.ftl';
        var cmsurl = huburl + portno + homecms;
        axios.get(cmsurl, {
          headers: {
            'content-type': 'text/html',
            'Accept': 'text/html',
            'Accept-Language': langauage
          }
        }).then(function (response) {
          self.content = response.data;
          self.getdata = false;
          var backg = self.pluck('main', self.content.area_List);
          var background = self.pluckcom('Banner', backg[0].component);
          setTimeout(function () {
            owlcarosl(background);
          }, 100);
          var Socialmedia = self.pluck('Social_Media_Aggregation', self.content.area_List);
          self.Socialmedia.Title = self.pluckcom('Title', Socialmedia[0].component);
          self.Socialmedia.Icon = self.pluckcom('Icon', Socialmedia[0].component);
          self.isLoading = false;
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
          self.isLoading = false;
        });
      });
    },
    // setip: function () {
    //   var self = this;
    //   $.getJSON("https://api.ipify.org?format=json", function (data) {
    //     $("#ipaddress").html(data.ip);
    //     self.ipAddres = data.ip;
    //     self.sessionids = data.ip.replace(/\./g, '');
    //     self.sendip();
    //   })
    // },
    // sessionid: function () {
    //   var GUID = function () {
    //     var S4 = function () {
    //       return (
    //         Math.floor(
    //           Math.random() * 0x10000 /* 65536 */
    //         ).toString(16)
    //       );
    //     };
    //     return (
    //       S4() + S4() + "-" +
    //       S4() + "-" +
    //       S4() + "-" +
    //       S4() + "-" +
    //       S4() + S4() + S4()
    //     );
    //   };
    //   if (!window.name.match(/^G/)) {
    //     window.name = "G" + GUID();
    //   }
    //   window.name = window.name.replace(/^GUID-/, "");
    //   str.
    //     this.sessionids = window.name;
    // },
    // sendip: async function () {
    //   let agencyCode = JSON.parse(localStorage.User).loginNode.code;
    //   let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
    //   let insertContactData = {
    //     type: "Visit",
    //     keyword1: this.sessionids,
    //     ip1: this.ipAddres,
    //     keyword2: "Master",
    //     keyword3: "Home Page",
    //     date1: requestedDate,
    //     nodeCode: agencyCode
    //   };
    //   let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
    //   try {
    //     let insertID = Number(responseObject);

    //   } catch (e) {

    //   }

    // },



    async cmsRequestData(callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
        },
        body: data, // body data type must match "Content-Type" header
      });
      try {
        const myJson = await response.json();
        return myJson;
      } catch (error) {
        return object;
      }
    },
  },
  filters: {
    moment: function (date) {
      return moment(date).format('DD MMM YYYY');
    }
  }

});

function owlcarosl(imageUrl) {
  $('#main_banner').css('background-image', 'url(' + imageUrl + ')');
  $("#owl-demo-2").owlCarousel({
    items: 3,
    lazyLoad: true,
    loop: true,
    margin: 30,
    navigation: true,
    itemsDesktop: [991, 2],
    itemsDesktopSmall: [979, 2],
    itemsTablet: [768, 2],
    itemsMobile: [640, 1],
  });
  $(".owl-prev").html('<i class="fa fa-angle-left"></i>');
  $(".owl-next").html('<i class="fa fa-angle-right"></i>');
}

$(document).ready(function () {
  var active_el = (sessionStorage.active_el) ? sessionStorage.active_el : 1;
  if (active_el == 1) {
    $('.nav-tabs a[href="#flights"]').tab('show');
  } else if (active_el == 2) {
    $('.nav-tabs a[href="#hotels"]').tab('show');
  } else if (active_el == 3) {
    $('.nav-tabs a[href="#flight-hotle"]').tab('show');
  } else {
    $('.nav-tabs a[href="#flights"]').tab('show');
  }

});
