var HeaderComponent = Vue.component('headeritem', {
    template: `
    <div>
          <div class="container relative">
              <div class="row">
                  
                  
                  <div class="col-md-6">
                      
                      <ul class="top-contact">
                          <li><i class="fa fa-phone"></i> <a :href="'tel:'+HeaderContent.Phone_Number">{{HeaderContent.Phone_Number}}</a></li>
                          <li><i class="fa fa-envelope"></i> <a :href="'mailto:'+HeaderContent.Email">{{HeaderContent.Email}}</a></li>
                      </ul>
                  </div>
                  
                  <div class="col-md-6 col-sm-12">
                      <div class="header-right">

                          <div class="log-in"><li id="sign-bt-area" class="login-reg  js-signin-modal-trigger" v-show="!userlogined">
                          <a href="javascript:void(0);" data-signin="login" ><i class="fa fa-user"></i> 
                          <span data-signin="login">{{HeaderContent.Login_Or_Register_Label}}</span></a></li>
                          </div>
                         
                          <!--iflogin-->
                          <label id="bind-login-info" for="profile2" class="profile-dropdown" v-show="userlogined">
                              <input type="checkbox" id="profile2">
                              <img src="/assets/images/user.png">
                              <span>{{userinfo.firstName+' '+userinfo.lastName }}</span>
                              <label for="profile2"><i class="fa fa-angle-down" aria-hidden="true"></i></label>
                              <ul>
                                  <li><a href="/customer-profile.html"><i class="fa fa-th-large" aria-hidden="true"></i>Dashboard</a></li>
                                  <li><a href="/my-profile.html"><i class="fa fa-user" aria-hidden="true"></i>My Profile</a></li>
                                  <li><a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i>My Booking</a></li>
                                  <li><a href="#" v-on:click.prevent="logout"><i class="fa fa-power-off" aria-hidden="true"></i>Log Out</a></li>
                              </ul>
                          </label>
                          <!--ifnotlogin-->
                          <div class="currency language">
                          <currency-select></currency-select>              
                          </div>
                          <div class="currency agent-login">
                             <a href="https://b2b.compare-ticket.com">Agent Login</a>
                          </div>
                      </div> 
                  </div>
              </div>
          </div>
          <div class="header-bottom">
          <div class="container">
              <div class="nav-logo">
              <div class="row">
                  
                      
                      <div class="col-md-3 col-sm-3">
                           <div class="logo">
                               <a href="/CompareTicket/index.html"><img :src="HeaderContent.Logo" alt="logo"></a>
                           </div>
                       </div>
                      
                      
                      <div class="col-md-6 col-sm-6">
                      <nav id="navbar-main" class="navbar navbar-default">
               
                 <div class="navbar-header">
                   <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                   </div>
                 <div class="collapse navbar-collapse navbar-right" id="myNavbar" >
                   <ul class="nav navbar-nav">
                     <li @click="activate(0)" :class="{ active : active_e == 0 }"><a href="/CompareTicket/index.html">{{Menu.Home_Label}}</a></li>
                     <li @click="activate(1)" :class="{ active : active_e == 1 }"><a href="/CompareTicket/about.html">{{Menu.About_Us_Label}}</a></li>
                     <li @click="activate(2)" :class="{ active : active_e == 2 }"><a href="/CompareTicket/holidays.html">{{Menu.Holidays_Label}}</a></li>
                     <li @click="activate(3)" :class="{ active : active_e == 3 }"><a href="/CompareTicket/contact.html">{{Menu.Contact_Us_Label}}</a></li>
                     <li id="sign-bt-area" class="login-reg  js-signin-modal-trigger" v-show="!userlogined">
                          <a href="javascript:void(0);" data-signin="login" ><i class="fa fa-user"></i> 
                          <span data-signin="login">{{HeaderContent.Login_Or_Register_Label}}</span></a></li>
                    <li id="compare-ticket-currency">   <currency-select></currency-select></li>
                    <li id="agent-login"><a href="http://b2b.compareticket.org/Login/CompareTicket/">Agent Login</a></li>
                   </ul>
                 </div>
              
         </nav>
                           </div>
                      
                      <div class="col-md-3 col-sm-3">
                           <div class="buttons">
                               <a class="btn" href="#" data-toggle="modal" data-target="#myModal">{{Menu.View_Booking_Label}}</a>
                           </div>
                      </div>
                      </div>
                  </div>
              </div>
          </div>
          <!--popup area-->
          <div class="cd-signin-modal js-signin-modal"> <!-- this is the entire modal form, including the background -->
          <div class="cd-signin-modal__container"> <!-- this is the container wrapper -->
              <ul class="cd-signin-modal__switcher js-signin-modal-switcher js-signin-modal-trigger">
                  <li><a href="#0" data-signin="login" data-type="login">{{LoginData.Sign_In_Label}}</a></li>
                  <li><a href="#0" data-signin="signup" data-type="signup">{{LoginData.Register_Label}}</a></li>
              </ul>
  
              <div class="cd-signin-modal__block js-signin-modal-block" data-type="login"> <!-- log in form -->
                  <div class="cd-signin-modal__form">
                      <p class="cd-signin-modal__fieldset">
                          <label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" 
                          for="signin-email">{{LoginData.Email_PlaceHolder}}</label>
                          <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                          id="signin-email" type="email" :placeholder="LoginData.Email_PlaceHolder" v-model="username" >
                          <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': usererrormsg.empty||usererrormsg.invalid }">{{usererrormsg.empty?'Please enter email!':(usererrormsg.invalid?'Email seems incorrect!':'')}}</span>
                      </p>
  
                      <p class="cd-signin-modal__fieldset">
                          <label class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace" 
                          for="signin-password">{{LoginData.Password_PlaceHolder}}</label>
                          <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                          id="signin-password" type="password"  :placeholder="LoginData.Password_PlaceHolder"  v-model="password" >
                          <a v-on:click="showhidepassword" class="cd-signin-modal__hide-password js-hide-password changeShowTxtCls">{{LoginData.Show_Label}}</a>
                          <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': psserrormsg }">
                          Please enter password!</span>
                      </p>
  
                      <p class="cd-signin-modal__fieldset">
                          <input class="cd-signin-modal__input cd-signin-modal__input--full-width" type="submit"
                          v-on:click="loginaction" v-bind:value="LoginData.Login_Button_Label">
                      </p>
                      <div id="myGoogleButton"></div>
                      <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false"
          data-use-continue-as="false" onlogin="checkLoginState();"></div>
                      <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="reset">{{LoginData.Forgot_Label}}</a></p>
                  </div>
                  
                  
              </div> <!-- cd-signin-modal__block -->
  
              <div class="cd-signin-modal__block js-signin-modal-block" data-type="signup"> <!-- sign up form -->
                  <div class="cd-signin-modal__form">
                      <p class="cd-signin-modal__fieldset">
                          <label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">Title</label>                        
                          <select v-model="registerUserData.title" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border down_select" id="signup-title">
                              <option selected>{{LoginData.Mr_Label}}</option>
                              <option selected>{{LoginData.Ms_Label}}</option>
                              <option>{{LoginData.Mrs_Label}}</option>
                          </select>
                          <span class="cd-signin-modal__error">Title seems incorrect!</span>
                      </p>
                      <p class="cd-signin-modal__fieldset">
                          <label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">First Name</label>
                          <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                          id="signup-firstname" type="text" :placeholder="LoginData.First_Name_Label" v-model="registerUserData.firstName">
                          <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userFirstNameErormsg }" >Plese enter first name!</span>
                      </p>
  
                      <p class="cd-signin-modal__fieldset">
                          <label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">Last Name</label>
                          <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                          id="signup-lastname" type="text" :placeholder="LoginData.Last_Name_Label" v-model="registerUserData.lastName">
                          <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userLastNameErrormsg }">Plese enter last name!</span>
                      </p>
  
                      <p class="cd-signin-modal__fieldset">
                          <label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" for="signup-email">Email</label>
                          <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                          id="signup-email" type="email" :placeholder="LoginData.Email_PlaceHolder" v-model="registerUserData.emailId">
                          <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userEmailErormsg.empty||userEmailErormsg.invalid }">{{userEmailErormsg.empty?'Please enter email!':(userEmailErormsg.invalid?'Email seems incorrect!':'')}}</span>
                      </p>
  
                      <p class="cd-signin-modal__fieldset">
                          <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding" type="submit" 
                          v-bind:value="LoginData.Create_Account_Label" v-on:click="registerUser">
                      </p>
                      <div id="myGoogleButtonReg"></div>
                      <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false"
          data-use-continue-as="false" onlogin="checkLoginState();"></div>                    
                  </div>
              </div> <!-- cd-signin-modal__block -->
  
              <div class="cd-signin-modal__block js-signin-modal-block" data-type="reset"> <!-- reset password form -->
                  <p class="cd-signin-modal__message">{{LoginData.Forgot_your_password_Content}}</p>
  
                  <div class="cd-signin-modal__form">
                      <p class="cd-signin-modal__fieldset">
                          <label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" for="reset-email">{{LoginData.Email_PlaceHolder}}</label>
                          <input v-model="emailId" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" id="reset-email" type="email" :placeholder="LoginData.Email_PlaceHolder">
                          <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userforgotErrormsg.empty||userforgotErrormsg.invalid }">{{userforgotErrormsg.empty?'Please enter email!':(userforgotErrormsg.invalid?'Email seems incorrect!':'')}}</span>
                      </p>
  
                      <p class="cd-signin-modal__fieldset">
                          <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding" type="submit" 
                          v-bind:value="LoginData.Reset_Password_Label" v-on:click="forgotPassword">
                      </p>
                      <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="login">{{LoginData.Login_Button_Label}}</a></p>
                  </div>
  
                  
              </div> <!-- cd-signin-modal__block -->
              <a href="#0" class="cd-signin-modal__close js-close">Close</a>
          </div> <!-- cd-signin-modal__container -->
      </div> 
          <!--popup area close-->
          <!--view booking popup-->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog  modal-smsp">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{BookingData.View_Booking_Title}}</h4>
      </div>
      <div class="modal-body">
      <div class="user_login">
      <div>
      <p><label>{{BookingData.Enter_your_email_address_Label}}</label></p>
      <div class="validation_sec">
      <input v-model="bEmail" type="text" id="txtretrivebooking" name="text" placeholder="name@example.com" class=""> 
      <span v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveEmailErormsg }"  class="cd-signin-modal__error sp_validation">Email Required!</span>
      </div> 
      <p></p> 
      <p><label>{{BookingData.Enter_your_ID_number_Label}}</label></p>
      <div class="validation_sec">
      <input v-model="bRef" type="text" id="text" name="text" placeholder="Example: AGY509-8509" class=""> 
      <span  v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveBkngRefErormsg }"   class="cd-signin-modal__error sp_validation">Booking Reference Id Required!</span>
      </div> 
      <p></p>
      <div class="viewradio_sec">
      <label class="radio_view">{{BookingData.Flight_Label}}
          <input v-model="bService" value="F" type="radio" checked="checked" name="radio">
          <span class="checkmark"></span>
      </label>
      <label class="radio_view">{{BookingData.Hotel_Label}}
          <input v-model="bService" value="H" type="radio" name="radio">
          <span class="checkmark"></span>
      </label>
      </div>
      <div class="btn-submit"><button v-on:click="retrieveBooking" type="submit" id="retrieve-booking" class="btn-blue">{{BookingData.Continue_Label}}</button></div> 
      </div></div>
      </div>            
    </div>
    
  </div>
</div>
      
          </div>`,
    data() {
        return {
            // content: null,
            HeaderContent: '',
            Menu: '',
            BookingData: '',
            LoginData: '',
            username: '',
            password: '',
            emailId: '',
            retrieveEmailId: '',
            retrieveBookRefid: '',
            usererrormsg: { empty: false, invalid: false },
            psserrormsg: false,
            userFirstNameErormsg: false,
            userLastNameErrormsg: false,
            userEmailErormsg: { empty: false, invalid: false },
            userPasswordErormsg: false,
            userVerPasswordErormsg: false,
            userPwdMisMatcherrormsg: false,
            userTerms: false,
            userforgotErrormsg: { empty: false, invalid: false },
            retrieveBkngRefErormsg: false,
            retrieveEmailErormsg: false,
            userlogined: this.checklogin(),
            userinfo: [],
            registerUserData: {
                firstName: '',
                lastName: '',
                emailId: '',
                password: '',
                verifyPassword: '',
                terms: '',
                title: 'Mr'
            },
            Languages: [],
            language: 'en',
            content: null,
            getdata: true,
            active_e: (sessionStorage.active_e) ? sessionStorage.active_e : 0,
            bEmail: '',
            bRef: '',
            bService: 'F',
            show: 'show',
            hide: 'hide',
        }
    },
    methods: {
        activate: function (el) {
            sessionStorage.active_e = el;
            this.active_el = el;
            if (el == 1 || el == 2) {
                maininstance.actvetab = el;
            }
            else {
                maininstance.actvetab = 0;
            }



        },
        headerData: function () {
            var self = this;

            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Header/Header/Header.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    if (response.data.area_List.length) {
                        var Headers = self.pluck('Header', self.content.area_List);
                        if (Headers != undefined) {
                            var HeaderTemp = self.getAllMapData(Headers[0].component);
                            self.HeaderContent = HeaderTemp;
                        }
                        var Menu = self.pluck('Menu', self.content.area_List);
                        if (Menu != undefined) {
                            var MenuTemp = self.getAllMapData(Menu[0].component);
                            self.Menu = MenuTemp;
                        }

                        var Login = self.pluck('Login_Section', self.content.area_List);
                        if (Login != undefined) {
                            var LoginTemp = self.getAllMapData(Login[0].component);
                            self.LoginData = LoginTemp;
                        }

                        var Booking = self.pluck('View_Booking_Section', self.content.area_List);
                        if (Booking != undefined) {
                            var BookingTemp = self.getAllMapData(Booking[0].component);
                            self.BookingData = BookingTemp;
                        }
                        self.getdata = false;
                        Vue.nextTick(function () {
                            (

                                function () {
                                    self.active_el = (sessionStorage.active_el) ? sessionStorage.active_el : 0;
                                    //Login/Signup modal window - by CodyHouse.co
                                    function ModalSignin(element) {
                                        this.element = element;
                                        this.blocks = this.element.getElementsByClassName('js-signin-modal-block');
                                        this.switchers = this.element.getElementsByClassName('js-signin-modal-switcher')[0].getElementsByTagName('a');
                                        this.triggers = document.getElementsByClassName('js-signin-modal-trigger');
                                        this.hidePassword = this.element.getElementsByClassName('js-hide-password');
                                        this.init();
                                    };

                                    ModalSignin.prototype.init = function () {
                                        var self1 = this;
                                        //open modal/switch form
                                        for (var i = 0; i < this.triggers.length; i++) {
                                            (function (i) {
                                                self1.triggers[i].addEventListener('click', function (event) {
                                                    if (event.target.hasAttribute('data-signin')) {
                                                        event.preventDefault();
                                                        self1.showSigninForm(event.target.getAttribute('data-signin'));
                                                    }
                                                });
                                            })(i);
                                        }

                                        //close modal
                                        this.element.addEventListener('click', function (event) {
                                            if (hasClass(event.target, 'js-signin-modal') || hasClass(event.target, 'js-close')) {
                                                event.preventDefault();
                                                removeClass(self1.element, 'cd-signin-modal--is-visible');
                                            }
                                        });
                                        //close modal when clicking the esc keyboard button
                                        document.addEventListener('keydown', function (event) {
                                            (event.which == '27') && removeClass(self1.element, 'cd-signin-modal--is-visible');
                                        });

                                        // //hide/show password
                                        // for (var i = 0; i < this.hidePassword.length; i++) {
                                        //     (function(i) {
                                        //         self1.hidePassword[i].addEventListener('click', function(event) {
                                        //             self1.togglePassword(self1.hidePassword[i]);
                                        //         });
                                        //     })(i);
                                        // }

                                        //IMPORTANT - REMOVE THIS - it's just to show/hide error messages in the demo

                                    };

                                    // ModalSignin.prototype.togglePassword = function(target) {
                                    //     var password = target.previousElementSibling;
                                    //     ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text'): password.setAttribute('type', 'password');
                                    //     target.textContent = ('Hide' == target.textContent) ? 'Show' : 'Hide';
                                    //     putCursorAtEnd(password);
                                    // }

                                    ModalSignin.prototype.showSigninForm = function (type) {
                                        // show modal if not visible
                                        !hasClass(this.element, 'cd-signin-modal--is-visible') && addClass(this.element, 'cd-signin-modal--is-visible');
                                        // show selected form
                                        for (var i = 0; i < this.blocks.length; i++) {
                                            this.blocks[i].getAttribute('data-type') == type ? addClass(this.blocks[i], 'cd-signin-modal__block--is-selected') : removeClass(this.blocks[i], 'cd-signin-modal__block--is-selected');
                                        }
                                        //update switcher appearance
                                        var switcherType = (type == 'signup') ? 'signup' : 'login';
                                        for (var i = 0; i < this.switchers.length; i++) {
                                            this.switchers[i].getAttribute('data-type') == switcherType ? addClass(this.switchers[i], 'cd-selected') : removeClass(this.switchers[i], 'cd-selected');
                                        }
                                    };

                                    ModalSignin.prototype.toggleError = function (input, bool) {
                                        // used to show error messages in the form
                                        toggleClass(input, 'cd-signin-modal__input--has-error', bool);
                                        toggleClass(input.nextElementSibling, 'cd-signin-modal__error--is-visible', bool);
                                    }

                                    var signinModal = document.getElementsByClassName("js-signin-modal")[0];
                                   
                                    if (signinModal) {
                                        new ModalSignin(signinModal);
                                    }

                                    // toggle main navigation on mobile
                                    var mainNav = document.getElementsByClassName('js-main-nav')[0];
                                    if (mainNav) {
                                        mainNav.addEventListener('click', function (event) {
                                            if (hasClass(event.target, 'js-main-nav')) {
                                                var navList = mainNav.getElementsByTagName('ul')[0];
                                                toggleClass(navList, 'cd-main-nav__list--is-visible', !hasClass(navList, 'cd-main-nav__list--is-visible'));
                                            }
                                        });
                                    }

                                    //class manipulations - needed if classList is not supported
                                    function hasClass(el, className) {
                                        if (el.classList) return el.classList.contains(className);
                                        else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
                                    }

                                    function addClass(el, className) {
                                        var classList = className.split(' ');
                                        if (el.classList) el.classList.add(classList[0]);
                                        else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
                                        if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
                                    }

                                    function removeClass(el, className) {
                                        var classList = className.split(' ');
                                        if (el.classList) el.classList.remove(classList[0]);
                                        else if (hasClass(el, classList[0])) {
                                            var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
                                            el.className = el.className.replace(reg, ' ');
                                        }
                                        if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
                                    }

                                    function toggleClass(el, className, bool) {
                                        if (bool) addClass(el, className);
                                        else removeClass(el, className);
                                    }
                                    // $("#modal_retrieve").leanModal({
                                    //     top: 100,
                                    //     overlay: 0.6,
                                    //     closeButton: ".modal_close"
                                    // });
                                    //credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
                                    function putCursorAtEnd(el) {
                                        if (el.setSelectionRange) {
                                            var len = el.value.length * 2;
                                            el.focus();
                                            el.setSelectionRange(len, len);
                                        } else {
                                            el.value = el.value;
                                        }
                                    };
                                })();
                        }.bind(self));
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.menus = [];
                    self.header = [];

                });
            });
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        loginaction: function () {

            if (!this.username.trim()) {
                this.usererrormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.username.trim())) {
                this.usererrormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.usererrormsg = { empty: false, invalid: false };
            }
            if (!this.password) {
                this.psserrormsg = true;
                return false;

            } else {
                this.psserrormsg = false;
                var self = this;
                login(this.username, this.password, function (response) {
                    if (response == false) {
                        self.userlogined = false;
                        alert("Invalid username or password.");
                    } else {
                        self.userlogined = true;
                        self.userinfo = JSON.parse(localStorage.User);
                        $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                        try {
                            self.$eventHub.$emit('logged-in', { userName: self.username, password: self.password });
                            signArea.headerLogin({ userName: self.username, password: self.password })
                        } catch (error) {

                        }
                    }
                });

            }



        },
        validEmail: function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        checklogin: function () {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);
                return true;
            } else {
                this.userlogined = false;
                return false;
            }
        },
        logout: function () {
            this.userlogined = false;
            this.userinfo = [];
            localStorage.profileUpdated = false;
            try {
                this.$eventHub.$emit('logged-out');
                signArea.logout()
            } catch (error) {

            }
            commonlogin();
            // signOut();
            // signOutFb();
        },
        registerUser: function () {
            if (this.registerUserData.firstName.trim() == "") {
                this.userFirstNameErormsg = true;
                return false;
            } else {
                this.userFirstNameErormsg = false;
            }
            if (this.registerUserData.lastName.trim() == "") {
                this.userLastNameErrormsg = true;
                return false;
            } else {
                this.userLastNameErrormsg = false;
            }
            if (this.registerUserData.emailId.trim() == "") {
                this.userEmailErormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.registerUserData.emailId.trim())) {
                this.userEmailErormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.userEmailErormsg = { empty: false, invalid: false };
            }
            var vm = this;
            registerUser(this.registerUserData.emailId, this.registerUserData.firstName, this.registerUserData.lastName, this.registerUserData.title, function (response) {
                if (response.isSuccess == true) {
                    vm.username = response.data.data.user.loginId;
                    vm.password = response.data.data.user.password;
                    login(vm.username, vm.password, function (response) {
                        if (response != false) {
                            $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                            window.location.href = "/edit-my-profile.html?edit-profile=true";
                        }
                    });
                }

            });
        },

        forgotPassword: function () {
            if (this.emailId.trim() == "") {
                this.userforgotErrormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.emailId.trim())) {
                this.userforgotErrormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.userforgotErrormsg = { empty: false, invalid: false };
            }

            var datas = {
                emailId: this.emailId,
                agencyCode: localStorage.AgencyCode,
                logoUrl: window.location.origin + "/" + localStorage.AgencyFolderName + "/website-informations/logo/logo.png",
                websiteUrl: window.location.origin,
                resetUri: window.location.origin + "/" + localStorage.AgencyFolderName + "/reset-password.html"
            };
            $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:none;background:grey !important;");
            
            var huburl = ServiceUrls.hubConnection.baseUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var requrl = ServiceUrls.hubConnection.hubServices.forgotPasswordUrl;
            axios.post(huburl + portno + requrl, datas)
                .then((response) => {
                    if (response.data != "") {
                        alert(response.data);
                    } else {
                        alert("Error in forgot password. Please contact admin.");
                    }
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:var(--primary-color) !important");

                })
                .catch((err) => {
                    console.log("FORGOT PASSWORD  ERROR: ", err);
                    if (err.response.data.message == 'No User is registered with this emailId.') {
                        alert(err.response.data.message);
                    } else {
                        alert('We have found some technical difficulties. Please contact admin!');
                    }
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:var(--primary-color) !important");

                })

        },

        retrieveBooking: function () {
            if (this.bEmail == "") {
                //alert('Email required !');
                this.retrieveEmailErormsg = true;
                return false;

            } else if (!this.validEmail(this.bEmail)) {
                //alert('Invalid Email !');
                this.retrieveEmailErormsg = true;
                return false;

            } else {
                this.retrieveEmailErormsg = false;
            }
            if (this.bRef == "") {
                this.retrieveBkngRefErormsg = true;
                return false;

            } else {
                this.retrieveBkngRefErormsg = false;
            }
            if (!this.retrieveBkngRefErormsg && !this.retrieveEmailErormsg && !this.retrieveEmailErormsg) {
                switch (this.bService) {
                    case 'F':
                        this.retBookFlight();
                        break;
                    case 'H':
                        var vm = this;
                        var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;

                        axios({
                            method: "get",
                            url: hubUrl + "/hotelBook/bookingbyref/" + vm.bRef + ":" + vm.bEmail,
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer ' + localStorage.access_token
                            }
                        }).then(response => {
                            window.sessionStorage.setItem('userAction', vm.bRef);
                            window.location.href = "/Hotels/hotel-detail.html#/hotelConfirmation";

                        }).catch(error => {
                            alertify.alert('Error!', 'Booking details not found!');
                        });
                        break;
                    default:
                        break;
                }
            }

        },
        retBookFlight: function () {
            if (this.bEmail == "") {
                alert('Email required !');
                return false;
            } else if (!this.validEmail(this.bEmail)) {
                alert('Invalid Email !');
                return false;
            } else {
                this.retrieveEmailErormsg = false;
            }
            if (this.bRef == "") {
                this.retrieveBkngRefErormsg = true;
                return false;
            } else {
                this.retrieveBkngRefErormsg = false;
            }
            var bookData = {
                BkngRefID: this.bRef,
                emailId: this.bEmail,
                redirectFrom: 'retrievebooking',
                isMailsend: false
            };
            localStorage.bookData = JSON.stringify(bookData);
            window.location.href = '/Flights/flight-confirmation.html';
        },
        showhidepassword: function (event) {
            var password = event.target.previousElementSibling;
            ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text') : password.setAttribute('type', 'password');
            event.target.textContent = (this.hide == event.target.textContent) ? this.show : this.hide;
        },
    },
    mounted: function () {
        if (localStorage.IsLogin == "true") {
            this.userlogined = true;
            this.userinfo = JSON.parse(localStorage.User);

        } else {
            this.userlogined = false;

        }
        this.headerData();
    },
    watch: {
        updatelogin: function () {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);

            } else {
                this.userlogined = false;

            }

        },
        bRef: function () {
            this.retrieveBkngRefErormsg = false;
        }

    }
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true,
        }

    },

});
Vue.component('partners', {
    template: `    
    <section class="partners">
    <ul id="partners">
        <li class="item" v-for="items in logo">
            <div class="partnersLogos">
                <img :src="items.Partner_logo">
            </div>
        </li>
    </ul>
</section>
    `,
    data() {
        return {
            // content: null,
            logo: null,
        }
    },
    methods: {
        partnersLogo: function () {
            var self = this;

            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Footer/Footer/Footer.ftl';

                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    if (response.data.area_List.length) {
                        var partnerComponent = self.pluck('Partners', self.content.area_List);
                        var logoArray = self.pluckcom('logo', partnerComponent[0].component);
                        self.logo = logoArray;
                        setTimeout(function () { partners() }, 1000);
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.logo = [];
                });
            });
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },

    },
    mounted: function () {
        this.partnersLogo();
        $(".pageload").hide();

    },

})

Vue.component('footeritem', {
    props: {
        item: Number
    },
    template: `
    <div class="container">
    <div class="row">
        
        <div class="col-md-4">
            <div class="footer-logo">
                <a class="f-logo" href="index.html">
                <img :src="FooterContent.Logo" alt="footer-logo">
                    </a>
                
                <ul>
                    <li><i class="fa fa-phone"></i><a :href="'tel:'+ContactData.Phone_Number">{{ContactData.Phone_Number}}</a></li>

                    <li><i class="fa fa-envelope-o"></i><a :href="'mailto:'+ContactData.Email">{{ContactData.Email}}</a></li>
                    
                    <li><i class="fa fa-map-marker"></i><a>{{ContactData.Address}}</a></li>
                </ul>
            </div>
        </div>
        
        <div class="col-md-8">
            
            
            <div class="newsletter">
                <h3>{{FooterContent.Newsletter_Title}}</h3>
                <div class="row">
                     <div class="col-md-6">
                    <p>{{FooterContent.Newsletter_Description}}</p>
                </div>
                     <div class="col-md-6">
                    <input type="email" id="text" v-model="newsltremail" :placeholder="FooterContent.Newsletter_Placeholder">
                    <button type="submit" v-on:click="sendnewsletter">{{FooterContent.Button_Label}}</button>
                </div>
                </div>
            </div>
            
            
            <div class="link-media">
                <div class="row">
                 <div class="col-md-6 col-sm-6 col-xs-8">
                    <ul>
                        <li><a href="/CompareTicket/index.html"">{{Menu.Home_Label}}</a></li>
                        <li><a href="/CompareTicket/about.html">{{Menu.About_Us_Label}}</a></li>
                        <li><a href="/CompareTicket/holidays.html">{{Menu.Holidays_Label}}</a></li>
                        <li><a href="/CompareTicket/contact.html">{{Menu.Contact_Us_Label}}</a></li>
                        <li><a href="/CompareTicket/privacypolicy.html">Privacy Policy</a></li>
                        <li><a href="/CompareTicket/terms-condition.html">Terms of Use</a></li>
                    </ul>
                </div>
                    
                    <div class="col-md-6 col-sm-6 col-xs-4">
                        <ul class="f-media">
                        <li><a :href="SocialContent.Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            
                            <li><a :href="SocialContent.Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            
                            <li><a :href="SocialContent.LinkedIn" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            
                            <li><a :href="SocialContent.Instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                    
                    
                    </div>
            </div>
            
            <div class="copy-right">
                <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-7">
                    <p>{{FooterContent.Copyright_Content}}</p>
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-5 ">
                    <div class="powerd-by">
                    <p>Powered by:</p>
                  <a href="http://www.oneviewit.com/" target="_blank">  <img src="images/oneview_logo.png" alt="logo"></a>
                        
                        </div>
                </div>
                    
                    </div>
            </div>
            
        </div>
        
    </div>
</div>
   `,
    data() {
        return {
            // content: null,
            // contentH:null,
            FooterContent: '',
            SocialContent: '',
            Menu: '',
            ContactData: { Address: '' },
            Newsletter: {
                Title: '',
                Placeholder: '',
                Email_placeholder: '',
                Button_Label: '',
            },
            newsltremail: null,

        }
    },
    methods: {
        footerData: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                // var contacturl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                var Headerurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Header/Header/Header.ftl';
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Footer/Footer/Footer.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    if (response.data.area_List.length) {
                        var Footers = self.pluck('Footer', self.content.area_List);
                        if (Footers != undefined) {
                            var FootersTemp = self.getAllMapData(Footers[0].component);
                            self.FooterContent = FootersTemp;
                        }

                        var Social = self.pluck('Social_Media_Link', self.content.area_List);
                        if (Social != undefined) {
                            var SocialsTemp = self.getAllMapData(Social[0].component);
                            self.SocialContent = SocialsTemp;
                        }
                        var ContactData = self.pluck('Contact_Us_Area', self.content.area_List);
                        if (ContactData != undefined) {
                            self.ContactData.Address = self.pluckcom('Address', ContactData[0].component);
                            self.ContactData.Email = self.pluckcom('Email', ContactData[0].component);
                            self.ContactData.Phone_Number = self.pluckcom('Phone_Number', ContactData[0].component);
                        }
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.footer = [];
                });
                axios.get(Headerurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    if (response.data.area_List.length) {
                        var Menu = self.pluck('Menu', self.content.area_List);
                        if (Menu != undefined) {
                            var MenuTemp = self.getAllMapData(Menu[0].component);
                            self.Menu = MenuTemp;
                        }
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.footer = [];
                });
                // axios.get(contacturl, {
                //     headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                // }).then(function (response) {
                //     self.content = response.data;
                //     if (response.data.area_List.length) {
                //         var ContactData = self.pluck('Contact_Us_Section', self.content.area_List);
                //         if (ContactData != undefined) {
                //             self.ContactData.Address = self.pluckcom('Address', ContactData[0].component);
                //             self.ContactData.Email = self.pluckcom('Email', ContactData[0].component);
                //             self.ContactData.Phone_Number = self.pluckcom('Phone_Number', ContactData[0].component);
                //         }
                //     }
                // }).catch(function (error) {
                //     console.log('Error');
                //     self.aboutUsContent = [];
                // });
            });
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },


        // Form Post


        sendnewsletter: async function () {

            if (!this.newsltremail) {
                alertify.alert('Alert', 'Email Id required.');
                return false;;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.');
                return false;
            } else {

                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                var filterValue = "type='Newsletter' AND keyword1='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                    return false;
                } else {
                    var frommail = JSON.parse(localStorage.User).loginNode.email;
                    var custmail = {
                        type: "UserAddedRequest",
                        fromEmail: frommail,
                        toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                        logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                        agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                        agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                        personName: this.newsltremail.split("@")[0],
                        primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                        secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                    };



                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = { type: "Newsletter", date1: requestedDate, keyword1: this.newsltremail, keyword2: "Subscribe Newsletter", nodeCode: agencyCode };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        var emailApi = ServiceUrls.emailServices.emailApi;
                        sendMailService(emailApi, custmail);
                        alertify.alert('Newsletter', 'Thank you for subscribing !');
                        this.newsltremail = ""
                    } catch (e) {

                    }
                }

            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {

            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {

                const myJson = await response.json();
                return myJson;
            } catch (error) {

                return object;
            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {

            var allDBData = [];
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        }




    },
    mounted: function () {
        this.footerData();
    },

})

function partners() {
    $("#partners").owlCarousel({
        items: 6,
        itemsCustom: false,
        itemsDesktop: [1199, 6],
        itemsDesktopSmall: [991, 4],
        itemsTablet: [768, 4],
        itemsTabletSmall: [600, 2],
        itemsMobile: [479, 2],
        singleItem: false,
        itemsScaleUp: false,

        //Autoplay
        autoPlay: true,
        stopOnHover: true,

        // Navigation
        navigation: false,
        navigationText: ['<img src="images/left-arrow.png">', '<img src="images/right-arrow.png">'],
        rewindNav: true,
        scrollPerPage: false,

        //Pagination
        pagination: false,
        paginationNumbers: false,

        // Responsive 
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
    });
}
var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true,
            mobileView: false,
        }

    },
    mounted: function () {
        this.mobileView = this.checkMobileOrNot();

    },
    methods: {
        checkMobileOrNot: function () {
            if (navigator.userAgent.match(/Android/i) ||
                navigator.userAgent.match(/webOS/i) ||
                navigator.userAgent.match(/iPhone/i) ||
                navigator.userAgent.match(/iPad/i) ||
                navigator.userAgent.match(/iPod/i) ||
                navigator.userAgent.match(/BlackBerry/i) ||
                navigator.userAgent.match(/Windows Phone/i)) {
                return true;
            }
            else { return false; }
        },
    }
});
function searchArray(nameKey, myArray, tagName) {
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i][tagName] === nameKey) {
            return myArray[i];
        }
    }
}