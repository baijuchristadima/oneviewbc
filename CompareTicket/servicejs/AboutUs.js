var termsnconditions_vue = new Vue({
    el: "#AboutUs",
    data: {
        Page_Title:'',
        Terms_Body:'',
        Background_image: '',
        Home_label:'',
        BannerSection:'',
        AboutUsSection:'',
        BookSection:'',
        Main_Area:{
            Terms:'',
        },
    },
    mounted() {
        this.getPagecontent();
        sessionStorage.active_e=1;
    },
    methods: {
      pluck(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
          if (item[key] != undefined) {
            Temparry.push(item[key]);
          }
        });
        return Temparry;
      },
      pluckcom(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
          if (item[key] != undefined) {
            Temparry = item[key];
          }
        });
        return Temparry;
      },
      getPagecontent: function () {
        var self = this;
        getAgencycode(function (response) {
            var Agencycode = response;
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            // self.dir = langauage == "ar" ? "rtl" : "ltr";
            var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/About Us/About Us/About Us.ftl';
    
            axios.get(pageurl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
                self.content = response.data;
                if (response.data.area_List.length) {
                    var bannerDetails = self.pluck('Banner_Section', self.content.area_List);
                    if (bannerDetails != undefined) {
                        var bannerDetailsTemp = self.getAllMapData(bannerDetails[0].component);
                        self.BannerSection = bannerDetailsTemp;
                    }
                    var AboutUsData = self.pluck('About_Us_Section', self.content.area_List);
                    if (AboutUsData != undefined) {
                        var AboutUsDataTemp = self.getAllMapData(AboutUsData[0].component);
                        self.AboutUsSection = AboutUsDataTemp;
                    }
                    var Book = self.pluck('Book_Section', self.content.area_List);
                    if (Book != undefined) {
                        var BookDataTemp = self.getAllMapData(Book[0].component);
                        self.BookSection = BookDataTemp;
                    }
                }
            }).catch(function (error) {
              console.log('Error');
              self.aboutUsContent = [];
            });
          });
  
      },
      getAllMapData: function (contentArry) {
        var tempDataObject = {};
        if (contentArry != undefined) {
            contentArry.map(function (item) {
                let allKeys = Object.keys(item)
                for (let j = 0; j < allKeys.length; j++) {
                    let key = allKeys[j];
                    let value = item[key];
                    if (key != 'name' && key != 'type') {
                        if (value == undefined || value == null) {
                            value = "";
                        }
                        tempDataObject[key] = value;
                    }
                }
            });
        }
        return tempDataObject;
    }   
    }
  });