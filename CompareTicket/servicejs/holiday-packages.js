const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var packageList = new Vue({
    i18n,
    el: '#holidayPackages',
    name: 'holidayPackages',
    data: {
        content: null,
        getdata: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'AED',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        pageContent: {
            Title: '',
            Breadcrumbpath1: '',
            Breadcrumb2: '',
            Common_image: '',
        },
        packages: [],
        getpackage: false,
      
        search: '',
        daysearch: '',
        sortBy: '',
        Pricesearch: '',
        allReview:[],
        BannerSection:'',
        packageDatas:'',
        BookSection:'',
        totalNews: 0,
      currentPage: 1,
      currentPages: 1,
      fromPage: 1,
      totalpage: 1,
      constructedNumbers: [],
      Package: [],
      pageLimit: 3,
      paginationLimit: 1,
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getBanner: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Holiday Package/Holiday Package/Holiday Package.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var bannerDetails = self.pluck('Banner_Section', self.content.area_List);
                    if (bannerDetails != undefined) {
                        var bannerDetailsTemp = self.getAllMapData(bannerDetails[0].component);
                        self.BannerSection = bannerDetailsTemp;
                    }

                    var packageHeaders = self.pluck('Package_Section', self.content.area_List);
                    if (packageHeaders != undefined) {
                        var packageTemp = self.getAllMapData(packageHeaders[0].component);
                        self.packageDatas = packageTemp;
                    }

                    var Book = self.pluck('Book_Section', self.content.area_List);
                    if (Book != undefined) {
                        var BookDataTemp = self.getAllMapData(Book[0].component);
                        self.BookSection = BookDataTemp;
                    }

                })
            });
        },

        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Package List/Package List/Package List.ftl';
                axios.get(topackageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    var packageData = response.data;
                    let holidayaPackageListTemp = [];
                    if (packageData != undefined && packageData.Values != undefined) {
                        holidayaPackageListTemp = packageData.Values.filter(function (el) {
                            return el.Status == true
                        });
                    }
                    self.packages = holidayaPackageListTemp;
                    self.setTotalPackageCount();
                    self.getpackage = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/CompareTicket/holiday-detail.html?page=" + url;
                    console.log(url);
                    window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;

        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        setTotalPackageCount: function () {
            if (this.packages != undefined && this.packages != undefined) {
              this.totalNews = Number(this.packages.length);
              this.totalpage = Math.ceil(this.totalNews / this.pageLimit);
              this.currentPage = 1;
              this.fromPage = 1;
              if (Number(this.totalNews) < 6) {
                // this.pageLimit=3;
              }
      
              this.constructAllPagianationLink();
      
            }
          },
          constructAllPagianationLink: function () {
            let limit = this.paginationLimit;
            this.constructedNumbers = [];
            for (let i = Number(this.fromPage); i <= (Number(this.totalpage) + limit); i++) {
              if (i <= Number(this.totalpage)) {
                this.constructedNumbers.push(i);
      
              }
      
            }
            this.currentPage = this.constructedNumbers;
            this.setNewsItems();
          },
          prevNextEvent: function (type) {
      
            let limit = this.paginationLimit;
            if (type == 'Previous') {
              if (this.currentPages > this.totalpage) {
                this.currentPages = this.currentPages - 1;
              }
      
              // this.currentPage = event.target.innerText;
              // this.currentPage=parseInt(this.currentPage);
              this.fromPage = this.currentPages;
              if (Number(this.fromPage) != 1) {
                // this.fromPage = Number(this.fromPage) - limit;
                this.currentPages = Number(this.currentPages) - 1;
                this.setNewsItems();
      
                // this.constructAllPagianationLink();
      
              }
      
            } else if (type == 'Next') {
              if (this.currentPages == 'undefined' || this.currentPages == '') {
                // this.currentPages = sessionStorage.getItem("currentpage");
              }
              if (this.currentPages <= this.totalpage) {
                let limit = this.paginationLimit;
                this.fromPage = this.currentPages;
                var totalP = (this.totalpage) + 1;
                if (Number(this.fromPage) != totalP) {
                  var count = this.currentPages + limit;
                  if (Number(count) <= Number(this.totalpage)) {
                    this.selectNewEvent(this.currentPages);
                  }
      
                }
              }
            }
          },
          selectNewEvent: function (ev) {
            let limit = 1;
            console.log(ev);
            this.currentPages = this.currentPage[ev];
      
            this.setNewsItems();
          },
          selected: function (ev) {
            let limit = 1;
            console.log(ev);
            this.currentPages = this.currentPage[ev];
      
            this.setNewsItems();
          },
          setNewsItems: function () {
            if (this.packages != undefined && this.packages != undefined) {
              let start = 0;
              let end = Number(start) + Number(this.pageLimit);
              if (Number(this.currentPages) == 1) {
              } else {
                var limit = this.totalpage;
                start = (Number(this.currentPages) + Number(this.pageLimit)) - 2;
                end = Number(start) + Number(this.pageLimit);
              }
              this.Package = this.packages.slice(start, end);
            }
          },
        viewReview: async function () {
            var self=this;
            
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestObject = { from: 0, size: 100, type: "Reviews", nodeCode: agencyCode, orderBy: "desc" };
               let responseObject = await this.cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
                  if (responseObject != undefined && responseObject.data != undefined) {
                    self.allReview = JSON.parse(JSON.stringify(responseObject)).data;                   
                   
                }
                
                });
          },
          async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
         getrating:function(url){
             var self=this;
            url = url.split("/Template/")[1];
            url = url.split(' ').join('-');
            url = url.split('.').slice(0, -1).join('.');
            url=url.split('_')[0];
            let reviewrate=[];
            reviewrate =self.allReview.filter(r=>r.keyword4==url);
            var sum = 0;
             if (reviewrate.length > 0) {
                 $.each(reviewrate, function () {
                     sum += this.number1?this.number1:0;
                 })
                 sum=sum/reviewrate.length;
             }
           console.log(sum);
             return sum;
         },
        getAmount: function (amount) {
            amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
            return amount;
        }

    },
    mounted: function () {
        this.viewReview();
        this.getPagecontent();
        this.getBanner();
        sessionStorage.active_e=2;
    },

});
function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}