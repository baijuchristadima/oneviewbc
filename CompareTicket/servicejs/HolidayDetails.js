const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var packageView = new Vue({
  i18n,
  el: '#package-detail',
  name: 'package-detail',
  data: {
    // content: null,
    packagecontent: null,
    getpackage: false,
    getdata: false,
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'AED',
    CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    Package: { Package_Name: '' },
    Description: { Description: '' },
    review: {},
    //booking form
    addBookingForm: {
      fname: '',
      lname: '',
      femail: '',
      fphone: '',
      fpack: '',
      fdate: '',
      fadult: 1,
      fchild: 0,
      finfants: 0,
      fmessage: '',
      banner: {}
    },
    BannerSection:'',   
    BookSection:'',
    FormList:'',

    PackageListt:{},
    BriefDetailsList:'',
    AboutSectionList:'',
    ItineraryList:'',
    LocationList:'',
    //review form
    Name: '',
    Email: '',
    contact: '',
    Review: '',
    overitems: null,
    pageURLLink: '',
    reviewAvailable: false,
    allReview: [],
    avgrating: '',
    ratingcount: '',
  },
  mounted() {

    this.getBanner();
    this.getPageTitle();
    this.viewReview();
    sessionStorage.active_e=2;
    var vm = this;
    this.$nextTick(function () {
      $('#Adultmyselect').on("change", function (e) {
        vm.dropdownChange(e, 'Adult')
      });
      $('#Infantmyselect').on("change", function (e) {
        vm.dropdownChange(e, 'Infant')
      });
      $('#Childmyselect').on("change", function (e) {
        vm.dropdownChange(e, 'Child')
      });
      $('#from').change(function () {
        vm.addBookingForm.fdate = $('#from').val();
      });
    })

  },
  methods: {

    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getBanner: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Holiday Package/Holiday Package/Holiday Package.ftl';
        axios.get(pageurl, {
            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
            self.content = response.data;
            var bannerDetails = self.pluck('Banner_Section', self.content.area_List);
            if (bannerDetails != undefined) {
                var bannerDetailsTemp = self.getAllMapData(bannerDetails[0].component);
                self.BannerSection = bannerDetailsTemp;
            }

            var FormSection = self.pluck('Form_Section', self.content.area_List);
            if (FormSection != undefined) {
                var FormSectionTemp = self.getAllMapData(FormSection[0].component);
                self.FormList = FormSectionTemp;
            }

            var Book = self.pluck('Book_Section', self.content.area_List);
            if (Book != undefined) {
                var BookDataTemp = self.getAllMapData(Book[0].component);
                self.BookSection = BookDataTemp;
            }
        })
    });
    },
    getAllMapData: function (contentArry) {
      var tempDataObject = {};
      if (contentArry != undefined) {
        contentArry.map(function (item) {
          let allKeys = Object.keys(item)
          for (let j = 0; j < allKeys.length; j++) {
            let key = allKeys[j];
            let value = item[key];
            if (key != 'name' && key != 'type') {
              if (value == undefined || value == null) {
                value = "";
              }
              tempDataObject[key] = value;
            }
          }
        });
      }
      return tempDataObject;
    },

    getPageTitle: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var packageurl = getQueryStringValue('page');
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        if (packageurl != "") {
          packageurl = packageurl.split('-').join(' ');
          packageurl = packageurl.split('_')[0];
          var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
          self.pageURLLink = packageurl;
          axios.get(topackageurl, {
            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
          }).then(function (response) {
            self.content = response.data;
            var PackageDetails = self.pluck('Package_Details', self.content.area_List);
            if (PackageDetails != undefined) {
                var packageDetailsTemp = self.getAllMapData(PackageDetails[0].component);
                self.PackageListt = packageDetailsTemp;
            }  
            
            var BriefDetails = self.pluck('Package_Brief_Details', self.content.area_List);
            if (BriefDetails != undefined) {
                var BriefDetailsTemp = self.getAllMapData(BriefDetails[0].component);
                self.BriefDetailsList = BriefDetailsTemp;
            } 

            var AboutSection = self.pluck('About_Package_Section', self.content.area_List);
            if (AboutSection != undefined) {
                var AboutSectionTemp = self.getAllMapData(AboutSection[0].component);
                self.AboutSectionList = AboutSectionTemp;
            } 

            var Itinerary = self.pluck('Itinerary_Section', self.content.area_List);
            if (Itinerary != undefined) {
                var ItineraryTemp = self.getAllMapData(Itinerary[0].component);
                self.ItineraryList = ItineraryTemp;
            }

            var Location = self.pluck('Location_Section', self.content.area_List);
            if (Location != undefined) {
                var LocationTemp = self.getAllMapData(Location[0].component);
                self.LocationList = LocationTemp;
            }

          })
        }
      });
    },
    addbooking: async function () {
      let dateObj = document.getElementById("from");
      let dateValue = dateObj.value;
      // datevalue = moment(dateValue, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
      dateValue = moment(String(dateValue)).format('YYYY-MM-DDThh:mm:ss');
      let self = this;
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.addBookingForm.femail.match(emailPat);
      if (this.addBookingForm.fname == undefined || this.addBookingForm.fname == '') {
        alertify.alert('Alert', 'First name required.').set('closable', false);
        return;
      }
      else if (this.addBookingForm.femail == undefined || this.addBookingForm.femail == '') {
        alertify.alert('Alert', 'Email Id required.').set('closable', false);
        return;

      }
      else if (matchArray == null) {
        alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
        return false;
      } else if (this.addBookingForm.fphone == undefined || this.addBookingForm.fphone == '') {
        alertify.alert('Alert', 'Mobile number required.').set('closable', false);
        return;
      }
      else if (dateValue == undefined || dateValue == '') {
        alertify.alert('Alert', 'Select Date.').set('closable', false);
        return;

      }

      else if (this.addBookingForm.fadult == undefined || this.addBookingForm.fadult == '' || this.addBookingForm.fadult == 'AL') {
        alertify.alert('Alert', 'Please select adult.').set('closable', false);
        return;
      } else {
        var toEmail = JSON.parse(localStorage.User).emailId;
        var frommail = JSON.parse(localStorage.User).loginNode.email;
        var postData = postData = {
          type: "PackageBookingRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
          ccEmails: [],
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          packegeName: this.PackageListt.Package_Name,
          personName: this.addBookingForm.fname,
          emailAddress: this.addBookingForm.femail,
          contact: this.addBookingForm.fphone,
          departureDate: dateValue,
          adults: this.addBookingForm.fadult,
          child2to5: "NA",
          child6to11: this.addBookingForm.fchild,
          infants: this.addBookingForm.finfants,
          message: this.addBookingForm.fmessage,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        var custmail = {
          type: "UserAddedRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(this.addBookingForm.femail) ? this.addBookingForm.femail : [this.addBookingForm.femail],
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.addBookingForm.fname,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        dateValue = moment(String(dateValue)).format('YYYY-MM-DDThh:mm:ss');
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
          type: "Package Booking",
          keyword1: this.addBookingForm.fname,
          keyword2: this.addBookingForm.lname,
          keyword3: this.addBookingForm.femail,
          text1: this.addBookingForm.fphone,
          keyword8: this.PackageListt.Package_Name,        
          keyword4: this.addBookingForm.fadult,
          keyword6: this.addBookingForm.fchild,
          keyword5: this.addBookingForm.finfants,
          keyword7: this.addBookingForm.fmessage,
          date1: requestedDate,
          date2: dateValue,  
          nodeCode: agencyCode
        };
        let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
          let insertID = Number(responseObject);
          var emailApi = ServiceUrls.emailServices.emailApi;

          sendMailService(emailApi, postData);
          sendMailService(emailApi, custmail);
          alertify.alert('Booking', 'Thank you for Booking.We shall get back to you.');
          self.addBookingForm.fname = "";
          self.addBookingForm.lname = "";
          self.addBookingForm.femail = "";
          self.addBookingForm.fphone = "";
          dateObj.value = null;
          self.addBookingForm.fadult = "";
          self.addBookingForm.fchild = "";
          self.addBookingForm.finfants = "";
          self.addBookingForm.fmessage = "";
        } catch (e) {

        }



      }
    } ,
    addReviews: async function () {
      let ratings = this.getUserRating();
      console.log(ratings);
      ratings = Number(ratings);
      if (ratings == 0) {
        alertify.alert('Alert', 'Rating required');
        return;
      }
      var self = this;
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.Email.match(emailPat);
      if (this.Name == undefined || this.Name == '') {
        alertify.alert('Alert', 'Name required.').set('closable', false);
        return;
      } else if (this.Email == undefined || this.Email == '') {
        alertify.alert('Alert', 'Email Id required.').set('closable', false);
        return;

      } else if (matchArray == null) {
        alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
        return false;
      } else if (this.Review == undefined || this.Review == '') {
        alertify.alert('Alert', 'Review required.').set('closable', false);
        return;
      } else {
        var frommail = JSON.parse(localStorage.User).loginNode.email;
        var custmail = {
          type: "UserAddedRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(this.Email) ? this.Email : [this.Email],
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.Name,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
          type: "Booking Review",
          keyword4: self.pageURLLink,
          keyword1: self.PackageListt.Package_Name,
          keyword2: self.Name,
          keyword3: self.Email,
          text1: self.Review,
          number1: ratings,
          date1: requestedDate,
          nodeCode: agencyCode
        };
        let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
            let insertID = Number(responseObject);
          var emailApi = ServiceUrls.emailServices.emailApi;
          sendMailService(emailApi, custmail);
            this.email = '';
            this.username = '';
            this.contact = '';
            this.comment = '';
            this.viewReview();
            alertify.alert('Review', 'Thank you for Review.');
        } catch (e) {

        }



      }
    },
    userRating: function (num) {
      var ulList = document.getElementById("ratingID");
      let m = 0;
      for (let i = 0; i < ulList.childNodes.length; i++) {
        let childNode = ulList.childNodes[i];
        if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
          m = m + 1;
          for (let k = 0; k < childNode.childNodes.length; k++) {
            let style = childNode.childNodes[k].style.color;
            if ((m) < Number(num)) {
              childNode.childNodes[k].style = "color: rgb(239, 158, 8)";
            } else if ((m) == Number(num)) {
              if (style.trim() == "rgb(239, 158, 8)") {
                childNode.childNodes[k].style = "color: a9a9a9;";
              } else {
                childNode.childNodes[k].style = "color: rgb(239, 158, 8);";
              }
            } else {
              childNode.childNodes[k].style = "color: a9a9a9";
            }
          }
        }
      }
    },
    getUserRating: function () {
      var ulList = document.getElementById("ratingID");
      let m = 0;
      for (let i = 0; i < ulList.childNodes.length; i++) {
        let childNode = ulList.childNodes[i];
        if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
          let style = "";
          for (let k = 0; k < childNode.childNodes.length; k++) {
            style = childNode.childNodes[k].style.color;
            if (style != undefined && style != '') {
              break;
            }
          }
          if (style.trim() == "a9a9a9") {
            break;
          } else if (style.trim() == "rgb(239, 158, 8)") {
            m = m + 1;
          }
        }
      }
      return m;
    },
    viewReview: async function () {
      var self = this;
      let allReview = [];
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
      let requestObject = { from: 0, size: 100, type: "Booking Review", nodeCode: agencyCode, orderBy: "desc" };
      let responseObject = await this.cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
        if (responseObject != undefined && responseObject.data != undefined) {
          allResult = JSON.parse(JSON.stringify(responseObject)).data;
          for (let i = 0; i < allResult.length; i++) {
            if (allResult[i].keyword4 == self.pageURLLink) {
              let object = {
                Name: allResult[i].keyword2,
                Date: moment(String(allResult[i].date1), "YYYY-MM-DDThh:mm:ss").format('MMM DD,YYYY'),
                comment: allResult[i].text1,
                Ratings: allResult[i].number1,
              };
              allReview.push(object);
            }
          }
          self.allReview = allReview;



        }
        self.ratingcount = allReview.length;
        allratingcount = self.ratingcount;
        var avgratingtemp = 0;
        for (let i = 0; i < self.ratingcount; i++) {

          avgratingtemp = avgratingtemp + allReview[i].Ratings;
        }
        self.avgrating = ((avgratingtemp) / allratingcount).toFixed(2);
        if (self.avgrating > 0) {
          self.reviewAvailable = true;
        }
      });
    },

    async cmsRequestData(callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: { 'Content-Type': 'application/json' },
        body: data, // body data type must match "Content-Type" header
      });
      try {
        const myJson = await response.json();
        return myJson;
      } catch (error) {
        return object;
      }
    },
    getAmount: function (amount) {
      amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
      return amount;
  },
    dropdownChange: function (event, type) {
      let dateObj = document.getElementById("from");
      if (event != undefined && event.target != undefined && event.target.value != undefined) {

        if (type == 'Adult') {
          this.addBookingForm.fadult = event.target.value;

        } else if (type == 'Infant') {
          this.addBookingForm.finfants = event.target.value;
        } else if (type == 'Child') {
          this.addBookingForm.fchild = event.target.value;
        }
      }
      let dateValue = dateObj.value;
      setTimeout(function () {
        let dateObjNew = document.getElementById("from");
        dateObjNew.value = dateValue;
      }, 100);
    },
  }
})
function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
