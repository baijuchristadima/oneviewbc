var terms = new Vue({
    el: "#terms",
    data: {
        BannerSection:{},
        termsSection:{},
        BookSection:{}
    },
    mounted() {
        this.getPagecontent();
        this.getbooking();
    },
    methods: {
      getPagecontent: function () {
        var self = this;
        getAgencycode(function (response) {
            var Agencycode = response;
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Terms And Condition/Terms And Condition/Terms And Condition.ftl';
            axios.get(pageurl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
                self.content = response.data;
                if (response.data.area_List.length) {
                    var bannerDetails = pluck('Banner_Section', self.content.area_List);
                    if (bannerDetails != undefined) {
                        var bannerDetailsTemp = getAllMapData(bannerDetails[0].component);
                        self.BannerSection = bannerDetailsTemp;
                    }
                    var termsData = pluck('Main_Content', self.content.area_List);
                    if (termsData != undefined) {
                        var termsDataTemp = getAllMapData(termsData[0].component);
                        self.termsSection = termsDataTemp;
                    }
                }
            }).catch(function (error) {
              console.log('Error');
            });
          });
  
      },
      getbooking: function () {
        var self = this;
        getAgencycode(function (response) {
            var Agencycode = response;
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            // self.dir = langauage == "ar" ? "rtl" : "ltr";
            var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/About Us/About Us/About Us.ftl';
    
            axios.get(pageurl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
                self.content = response.data;
                if (response.data.area_List.length) {
                    var Book = pluck('Book_Section', self.content.area_List);
                    if (Book != undefined) {
                        var BookDataTemp = getAllMapData(Book[0].component);
                        self.BookSection = BookDataTemp;
                    }
                }
            }).catch(function (error) {
              console.log('Error');
              self.aboutUsContent = [];
            });
          });
  
      },  
    }
  });