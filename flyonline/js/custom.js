(function($) { 

        
  /* Language JS
  ============================================================== */
  $(function(){
      $('.selectpicker').selectpicker();
  });
  
      
      
  /*--Back-to-top--*/ 
      $(document).ready(function(){
    
    //Check to see if the window is top if not then display button
    $(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
        $('.scrollToTop').fadeIn();
      } else {
        $('.scrollToTop').fadeOut();
      }
    });
    
    //Click event to scroll to top
    $('.scrollToTop').click(function(){
      $('html, body').animate({scrollTop : 0},800);
      return false;
    });
    
  });
  
      
      
  
   
      
      
  /*--nav-dropdown-hover--*/
  $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).slideDown(300);
      }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).slideUp(300);
      });
      
      
      
  /*--select-form-js--*/    
  $(document).ready(function(){
    $(function() {
      "use strict";
      
      $( "#departure_date" ).datepicker({ minDate: -0, maxDate: "+3M" });
      $( "#return_date" ).datepicker({ minDate: -0, maxDate: "+3M" });
      $( "#check_out" ).datepicker({ minDate: -0, maxDate: "+3M" });
      $( "#check_in" ).datepicker({ minDate: -0, maxDate: "+3M" });
      $( "#package_start" ).datepicker({ minDate: -0, maxDate: "+3M" });
      $( "#car_start" ).datepicker({ minDate: -0, maxDate: "+3M" });
      $( "#car_end" ).datepicker({ minDate: -0, maxDate: "+3M" });
      $( "#cruise_start" ).datepicker({ minDate: -0, maxDate: "+3M" });
      $( "#adult_count" ).spinner({
        min: 1
      });
      $( "#child_count" ).spinner( {
        min: 1
      });
      $( "#hotel_adult_count" ).spinner( {
        min: 1
      });
      $( "#hotel_child_count" ).spinner( {
        min: 1
      });
      $('.selectpicker').selectpicker({
        style: 'custom-select-button'
      });
  });
  });
      
      /*---select-box-js---*/
      
      jQuery(document).ready(function($){
      $('.myselect').select2({minimumResultsForSearch: Infinity,'width':'100%'});
      $('b[role="presentation"]').hide();
      $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
  });
        jQuery(document).ready(function($){
      $('.myselect-2').select2({minimumResultsForSearch: Infinity,'width':'100%'});
      $('b[role="presentation"]').hide();
      $('.select2-selection__arrow').append('');
  });
  
      
  /*--date-picker--*/
       $( function() {
      var dateFormat = "mm/dd/yy",
        from = $( "#from-1,#from-2,#from-3,#from-4,#from-5,#from-6,#from-7,#from-8,#from-9,#from-10" )
          .datepicker({
            minDate: "dateToday",
            numberOfMonths: 1,
            showOn: "both",
            buttonText: " ",
            duration: "fast",
            showAnim: "slide", 
            showOptions: {direction: "up"} 
          })
          .on( "change", function() {
            to.datepicker( "option", "minDate", getDate( this ) );
          }),
        to = $( "#to-1,#to-2,#to-3,#to-4,#to-5,#to-6,#to-7,#to-8,#to-9,#to-10" ).datepicker({
          minDate: "dateToday",
            numberOfMonths: 1,
            showOn: "both",
            buttonText: " ",
            duration: "fast",
            showAnim: "slide", 
            showOptions: {direction: "up"} 
        })
        .on( "change", function() {
          from.datepicker( "option", "maxDate", getDate( this ) );
        });
   
      function getDate( element ) {
        var date;
        try {
          date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
          date = null;
        }
   
        return date;
      }
    } );
      
      
      
      
      
      
      
      
      
  // /*--------Offer-Slider---------*/  
  //      $(document).ready(function() {
  
  //       $("#owl-demo-3").owlCarousel({
  //         autoplay:true,
  //         autoPlay : 8000,
  //         autoplayHoverPause:true, 
  //         stopOnHover : false,  
  //         items : 3,
  //         margin:10,  
  //         lazyLoad : true,
  //         navigation : true,
  //         itemsDesktop : [1199, 3],
  //         itemsDesktopSmall : [991,2],
  //           itemsTablet : [600, 1]
  //       });
  
  //          $( ".owl-prev").html('<i class="fa  fa-angle-left"></i>');
  //          $( ".owl-next").html('<i class="fa  fa-angle-right"></i>');
  //     });
      
      
  //     /*--------Holiday-Slider---------*/  
  //      $(document).ready(function() {
  
  //       $("#owl-demo-4").owlCarousel({
  //         autoplay:true,
  //         autoPlay : 8000,
  //         autoplayHoverPause:true, 
  //         stopOnHover : false,  
  //         items : 2,
  //         margin:10,  
  //         lazyLoad : true,
  //         navigation : true,
  //         itemsDesktop : [1199, 2],
  //         itemsDesktopSmall : [991,2],
  //           itemsTablet : [600, 1]
  //       });
  
  //          $( ".owl-prev").html('<i class="fa  fa-angle-left"></i>');
  //          $( ".owl-next").html('<i class="fa  fa-angle-right"></i>');
  //     });
      
  
  
  
  })(jQuery);
  