var contact = new Vue({
    el: '#contact',
    name: 'contact',
    data: {
        content: {},
        contents: {},
        bannerDetails: {
            Image: '',
            Title: '',
            Breadcrumb: '',
        },
        cntusername: '',
        cntemail: '',
        cntcontact: '',
        cntmessage: '',
        cntsubject: '',
        socialmedia: {
            Short_Description: '',
            items: ''
        },
        addressDetails: {
            Phone_Number: '',
            Helpline_Phone_number: '',
            Email: '',
            Address: '',
        },
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            var responseData;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var url = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact Page/Contact Page/Contact Page.ftl';
                axios.get(url, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (res) {
                    responseData = res.data
                    if (responseData.area_List) {
                        var mainData = self.pluck('Main_Area', responseData.area_List);
                        self.contents = self.getAllMapData(mainData[0].component);

                        var bannerDetails = self.pluck('Banner_Area', responseData.area_List);
                        if (bannerDetails.length > 0) {
                            self.bannerDetails.Image = self.pluckcom('Banner_Image', bannerDetails[0].component);
                            self.bannerDetails.Title = self.pluckcom('Banner_Title', bannerDetails[0].component);
                            self.bannerDetails.Breadcrumb = self.pluckcom('Breadcrumb', bannerDetails[0].component);

                        }
                    } else {
                        console.log('Error');
                        self.content = [];
                    }
                }).catch(function (error) {
                    console.log('Error', error);
                    self.content = [];
                });


            });

        },
        getAddresscontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Header And Footer Page/Header And Footer Page/Header And Footer Page.ftl';

                axios.get(pageurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    self.content = response.data;
                    var addressDetails = self.pluck('Header_Area', self.content.area_List);
                    if (addressDetails.length > 0) {
                        self.addressDetails.Phone_Number = self.pluckcom('Phone_Number', addressDetails[0].component);
                        self.addressDetails.Helpline_Phone_number = self.pluckcom('Helpline_Phone_number', addressDetails[0].component);
                        self.addressDetails.Email = self.pluckcom('Email', addressDetails[0].component);
                        self.addressDetails.Address = self.pluckcom('Address', addressDetails[0].component);
                    }

                    var socialmedia = self.pluck('Footer_Area', self.content.area_List);
                    if (socialmedia.length > 0) {
                        self.socialmedia.Short_Description = self.pluckcom('Short_Description', socialmedia[0].component);

                        self.socialmedia.items = self.pluckcom('Social_media_Links', socialmedia[0].component);
                        // self.socialmedia.Twitter_Link = self.pluckcom('Twitter_Link', socialmedia[0].component);
                        // self.socialmedia.Instagram_Link = self.pluckcom('Instagram_Link', socialmedia[0].component);
                        // self.socialmedia.LinkedIn_Link = self.pluckcom('LinkedIn_Link_', socialmedia[0].component);

                    }

                    // self.getdata = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.aboutUsContent = [];
                });
            });
        },
        getmoreinfo(url) {
            var customSearch =
                "&date=" + this.transferDate +
                "&hour=" + ('0' + (this.pickUpH - 1)).slice(-2) +
                "&min=" + ('0' + (this.pickUpM - 1)).slice(-2) +
                "&guest=" + this.guest +
                "&city=" + this.cityName.split(' ').join('-') +
                "&from=" + this.fromL.split(' ').join('-') +
                "&to=" + this.toL.split(' ').join('-') + "&";

            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/ICA/transfer-booking.html?page=" + url + customSearch;
                    window.location.href = url;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;
        },
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#from2").datepicker({
                minDate: new Date('11/15/2020'),
                maxDate: new Date('11/20/2020'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
            });
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },

        sendcontactus: async function () {
            if (!this.cntusername) {
                alertify.alert('Alert', 'Name required.').set('closable', false);

                return false;
            }
            if (!this.cntemail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.cntcontact) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            if (!this.cntsubject) {
                alertify.alert('Alert', 'Subject required.').set('closable', false);
                return false;
            }

            if (!this.cntmessage) {
                alertify.alert('Alert', 'Message required.').set('closable', false);
                return false;
            } else {
                var frommail = JSON.parse(localStorage.User).loginNode.email;
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                    logo: JSON.parse(localStorage.User).loginNode.logo,
                    // logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.cntusername,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Contact Us",
                    keyword1: this.cntusername,
                    keyword2: this.cntemail,
                    text1: this.cntcontact,
                    keyword3: this.cntsubject,
                    keyword4: this.cntmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, custmail);
                    this.cntemail = '';
                    this.cntusername = '';
                    this.cntcontact = '';
                    this.cntmessage = '';
                    this.cntsubject = '';
                    alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
                } catch (e) {

                }



            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json'
                },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        }

    },
    mounted: function () {
        var vm = this;
        vm.getPagecontent();
        vm.getAddresscontent();
        sessionStorage.active_e = 3;
    }
});