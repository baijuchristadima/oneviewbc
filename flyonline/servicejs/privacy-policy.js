var termsnconditions_vue = new Vue({
    el: "#privacy",
    data: {
        Page_Title:'',
        Terms_Body:'',
        Background_image: '',
        Home_label:'',
        bannerDetails:{
            Image:'',
            Title:'',
            Breadcrumb:'',
        },
        Main_Area:{
            Privacy:'',
        },
    },
    mounted() {
        this.getPagecontent();
    },
    methods: {
      pluck(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
          if (item[key] != undefined) {
            Temparry.push(item[key]);
          }
        });
        return Temparry;
      },
      pluckcom(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
          if (item[key] != undefined) {
            Temparry = item[key];
          }
        });
        return Temparry;
      },
      getPagecontent: function () {
        var self = this;
        getAgencycode(function (response) {
            var Agencycode = response;
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            // self.dir = langauage == "ar" ? "rtl" : "ltr";
            var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Privacy Policy Page/Privacy Policy Page/Privacy Policy Page.ftl';
    
            axios.get(pageurl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
              self.content = response.data;
              var bannerDetails = self.pluck('Banner_Area', self.content.area_List);
              if (bannerDetails.length > 0) {
                self.bannerDetails.Image = self.pluckcom('Banner_Image', bannerDetails[0].component);
                self.bannerDetails.Title = self.pluckcom('Banner_Title', bannerDetails[0].component);
                self.bannerDetails.Breadcrumb = self.pluckcom('Breadcrumb', bannerDetails[0].component);
                
              }

              var Main_Area = self.pluck('Main_Area', self.content.area_List);
              if (Main_Area.length > 0) {
                self.Main_Area.Privacy = self.pluckcom('Privacy', Main_Area[0].component);
                             
              }
    
              // self.getdata = true;
    
            }).catch(function (error) {
              console.log('Error');
              self.aboutUsContent = [];
            });
          });
  
      }   
    }
  });