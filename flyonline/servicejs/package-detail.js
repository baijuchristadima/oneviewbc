const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var packageView = new Vue({
  i18n,
  el: '#package-detail',
  name: 'package-detail',
  data: {
    // content: null,
    packagecontent: null,
    getpackage: false,
    getdata: false,
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'AED',
    CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    Package: {
      Package_Name: ''
    },
    Description: {
      Description: ''
    },
    review: {},
    Itinerary: {},
    Location: {},
    //booking form
    addBookingForm: {
      fname: '',
      lname: '',
      femail: '',
      fphone: '',
      fpack: '',
      fdate: '',
      fadult: 1,
      fchild: 0,
      finfants: 0,
      fmessage: '',
      banner: {}
    },

    //review form
    Name: '',
    Email: '',
    contact: '',
    Review: '',
    overitems: null,
    banner: {
      Banner_Image: '',
      Banner_Title: ''
    },
    socialMedia: '',
    pageURLLink: '',
    reviewAvailable: false,
    allReview: [],
    avgrating: '',
    ratingcount: '',
  },
  mounted() {

    this.getBanner();
    this.getPageTitle();
    this.viewReview();
    var vm = this;
    this.$nextTick(function () {
      $('#Adultmyselect').on("change", function (e) {
        vm.dropdownChange(e, 'Adult')
      });
      $('#Infantmyselect').on("change", function (e) {
        vm.dropdownChange(e, 'Infant')
      });
      $('#Childmyselect').on("change", function (e) {
        vm.dropdownChange(e, 'Child')
      });
      $('#from').change(function () {
        vm.addBookingForm.fdate = $('#from').val();
      });



      var dateFormat = "mm/dd/yy",
        from = $("#from,#from-2,#from-3,#from-4,#from-5,#from-6,#from-7,#from-8,#from-9,#from-10")
        .datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: " ",
          duration: "fast",
          showAnim: "slide",
          showOptions: {
            direction: "up"
          }
        })
        .on("change", function () {
          to.datepicker("option", "minDate", getDate(this));
        }),
        to = $("#to-1,#to-2,#to-3,#to-4,#to-5,#to-6,#to-7,#to-8,#to-9,#to-10").datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: " ",
          duration: "fast",
          showAnim: "slide",
          showOptions: {
            direction: "up"
          }
        })
        .on("change", function () {
          from.datepicker("option", "maxDate", getDate(this));
        });

      function getDate(element) {
        var date;
        try {
          date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
          date = null;
        }

        return date;
      }
    })

  },
  methods: {

    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getBanner: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Packages Page Banner/Packages Page Banner/Packages Page Banner.ftl';
        axios.get(pageurl, {
          headers: {
            'content-type': 'text/html',
            'Accept': 'text/html',
            'Accept-Language': langauage
          }
        }).then(function (response) {
          var content = response.data;
          if (content.area_List.length) {
            var banner = self.pluck('Package_Detail_Page_Area', content.area_List);
            self.banner = self.getAllMapData(banner[0].component);
            self.socialMedia = self.pluckcom('Social_media_Share', banner[0].component);
          }
        })
      });
    },
    navigateSocialPage(url) {
      if (url.includes("?")) {
        var fullUrl = url + "=" + encodeURIComponent(window.location.href);
        return fullUrl
      } else {
        return url
      }
    },
    getAllMapData: function (contentArry) {
      var tempDataObject = {};
      if (contentArry != undefined) {
        contentArry.map(function (item) {
          let allKeys = Object.keys(item)
          for (let j = 0; j < allKeys.length; j++) {
            let key = allKeys[j];
            let value = item[key];
            if (key != 'name' && key != 'type') {
              if (value == undefined || value == null) {
                value = "";
              }
              tempDataObject[key] = value;
            }
          }
        });
      }
      return tempDataObject;
    },

    getPageTitle: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var packageurl = getQueryStringValue('page');
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        if (packageurl != "") {
          packageurl = packageurl.split('-').join(' ');
          packageurl = packageurl.split('_')[0];
          var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
          self.pageURLLink = packageurl;
          axios.get(topackageurl, {
            headers: {
              'content-type': 'text/html',
              'Accept': 'text/html',
              'Accept-Language': langauage
            }
          }).then(function (response) {
            var pagecontent = self.pluck('Package_Area', response.data.area_List);
            if (pagecontent.length > 0) {
              self.Package.Package_Name = self.pluckcom('Package_Name', pagecontent[0].component);
              self.Package.Package_Image = self.pluckcom('Package_Image', pagecontent[0].component);
              self.Package.No_of_Days = self.pluckcom('Number_Of_Days', pagecontent[0].component);
              self.Package.No_of_Nights = self.pluckcom('Number_Of_Nights', pagecontent[0].component);
              self.Package.Validity = self.pluckcom('Validity', pagecontent[0].component);
              self.Package.Price = self.pluckcom('Price_Per_Person', pagecontent[0].component);
              self.Package.Destination = self.pluckcom('Destination', pagecontent[0].component);
              self.Package.Holiday_Type = self.pluckcom('Holiday_Type', pagecontent[0].component);
            }
            var Description = self.pluck('Description_Tab', response.data.area_List);
            self.Description.Description = self.pluckcom('Description', Description[0].component);
            self.Description.Departure = self.pluckcom('Departure', Description[0].component);
            self.Description.Bottom_Description = self.pluckcom('Bottom_Description', Description[0].component);
            self.Description.Return_Time = self.pluckcom('Return_Time', Description[0].component);
            self.Description.Departure_Time = self.pluckcom('Departure_Time', Description[0].component);
            self.Description.Included = self.pluckcom('Included', Description[0].component);
            self.Description.Not_Included = self.pluckcom('Not_Included', Description[0].component);

            if (response.data.area_List[2].Itinerary_Tab != undefined) {
              var titleDataTemp = self.getAllMapData(response.data.area_List[2].Itinerary_Tab.component);
              self.Itinerary = titleDataTemp;
            }
            if (response.data.area_List[3].Location_Tab != undefined) {
              var titleDataTemp = self.getAllMapData(response.data.area_List[3].Location_Tab.component);
              self.Location = titleDataTemp;
            }

          })
        }
      });
    },
    addbooking: async function () {
      let dateObj = document.getElementById("from");
      let dateValue = dateObj.value;
      // datevalue = moment(dateValue, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
      dateValue = moment(String(dateValue)).format('YYYY-MM-DDThh:mm:ss');
      let self = this;
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.addBookingForm.femail.match(emailPat);
      if (this.addBookingForm.fname == undefined || this.addBookingForm.fname == '') {
        alertify.alert('Alert', 'First name required.').set('closable', false);
        return;
      } else if (this.addBookingForm.femail == undefined || this.addBookingForm.femail == '') {
        alertify.alert('Alert', 'Email Id required.').set('closable', false);
        return;

      } else if (matchArray == null) {
        alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
        return false;
      } else if (this.addBookingForm.fphone == undefined || this.addBookingForm.fphone == '') {
        alertify.alert('Alert', 'Mobile number required.').set('closable', false);
        return;
      } else if (dateValue == undefined || dateValue == '') {
        alertify.alert('Alert', 'Select Date.').set('closable', false);
        return;

      } else if (this.addBookingForm.fadult == undefined || this.addBookingForm.fadult == '' || this.addBookingForm.fadult == 'AL') {
        alertify.alert('Alert', 'Please select adult.').set('closable', false);
        return;
      } else {
        var toEmail = JSON.parse(localStorage.User).emailId;
        var frommail = JSON.parse(localStorage.User).loginNode.email;
        var postData = postData = {
          type: "PackageBookingRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
          ccEmails: [],
          logo: JSON.parse(localStorage.User).loginNode.logo,
          // logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          packegeName: this.Package.Package_Name,
          personName: this.addBookingForm.fname,
          emailAddress: this.addBookingForm.femail,
          contact: this.addBookingForm.fphone,
          departureDate: dateValue,
          adults: this.addBookingForm.fadult,
          child2to5: "NA",
          child6to11: this.addBookingForm.fchild,
          infants: this.addBookingForm.finfants,
          message: this.addBookingForm.fmessage,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        var custmail = {
          type: "UserAddedRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(this.addBookingForm.femail) ? this.addBookingForm.femail : [this.addBookingForm.femail],
          logo: JSON.parse(localStorage.User).loginNode.logo,
          // logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.addBookingForm.fname,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        dateValue = moment(String(dateValue)).format('YYYY-MM-DDThh:mm:ss');
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
          type: "Package Booking",
          keyword1: this.addBookingForm.fname,
          keyword6: this.addBookingForm.lname,
          keyword3: this.addBookingForm.femail,
          keyword4: this.addBookingForm.fphone,
          keyword5: this.Package.Package_Name,
          date1: dateValue,
          number1: this.addBookingForm.fadult,
          number2: this.addBookingForm.fchild,
          number3: this.addBookingForm.finfants,
          text1: this.addBookingForm.fmessage,
          nodeCode: agencyCode
        };
        let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
          let insertID = Number(responseObject);
          var emailApi = ServiceUrls.emailServices.emailApi;
          sendMailService(emailApi, postData);
          sendMailService(emailApi, custmail);
          alertify.alert('Booking', 'Thank you for Booking.We shall get back to you.');
          self.addBookingForm.fname = "";
          self.addBookingForm.lname = "";
          self.addBookingForm.femail = "";
          self.addBookingForm.fphone = "";
          dateObj.value = null;
          self.addBookingForm.fadult = "";
          self.addBookingForm.fchild = "";
          self.addBookingForm.finfants = "";
          self.addBookingForm.fmessage = "";
        } catch (e) {

        }



      }
    },
    addReviews: async function () {
      let ratings = this.getUserRating();
      console.log(ratings);
      ratings = Number(ratings);
      if (ratings == 0) {
        alertify.alert('Alert', 'Rating required');
        return;
      }
      var self = this;
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.Email.match(emailPat);
      if (this.Name == undefined || this.Name == '') {
        alertify.alert('Alert', 'Name required.').set('closable', false);
        return;
      } else if (this.Email == undefined || this.Email == '') {
        alertify.alert('Alert', 'Email Id required.').set('closable', false);
        return;

      } else if (matchArray == null) {
        alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
        return false;
      } else if (this.Review == undefined || this.Review == '') {
        alertify.alert('Alert', 'Review required.').set('closable', false);
        return;
      } else {
        var frommail = JSON.parse(localStorage.User).loginNode.email;
        var custmail = {
          type: "UserAddedRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(this.Email) ? this.Email : [this.Email],
          logo: JSON.parse(localStorage.User).loginNode.logo,
          // logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.Name,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
          type: "Booking Review",
          keyword4: self.pageURLLink,
          keyword1: self.Package.Package_Name,
          keyword2: self.Name,
          keyword3: self.Email,
          text1: self.Review,
          number1: ratings,
          date1: requestedDate,
          nodeCode: agencyCode
        };
        let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
          var emailApi = ServiceUrls.emailServices.emailApi;
          sendMailService(emailApi, custmail);
          self.Name = "";
          self.Email = "";
          self.Review = "";
          alertify.alert('Review', 'Thank you for Review.');
          setTimeout(function () {
            self.viewReview();
          }, 3000);
        } catch (e) {

        }



      }
    },
    userRating: function (num) {
      var ulList = document.getElementById("ratingID");
      let m = 0;
      for (let i = 0; i < ulList.childNodes.length; i++) {
        let childNode = ulList.childNodes[i];
        if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
          m = m + 1;
          for (let k = 0; k < childNode.childNodes.length; k++) {
            let style = childNode.childNodes[k].style.color;
            if ((m) < Number(num)) {
              childNode.childNodes[k].style = "color: rgb(239, 158, 8)";
            } else if ((m) == Number(num)) {
              if (style.trim() == "rgb(239, 158, 8)") {
                childNode.childNodes[k].style = "color: a9a9a9;";
              } else {
                childNode.childNodes[k].style = "color: rgb(239, 158, 8);";
              }
            } else {
              childNode.childNodes[k].style = "color: a9a9a9";
            }
          }
        }
      }
    },
    getUserRating: function () {
      var ulList = document.getElementById("ratingID");
      let m = 0;
      for (let i = 0; i < ulList.childNodes.length; i++) {
        let childNode = ulList.childNodes[i];
        if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
          let style = "";
          for (let k = 0; k < childNode.childNodes.length; k++) {
            style = childNode.childNodes[k].style.color;
            if (style != undefined && style != '') {
              break;
            }
          }
          if (style.trim() == "a9a9a9") {
            break;
          } else if (style.trim() == "rgb(239, 158, 8)") {
            m = m + 1;
          }
        }
      }
      return m;
    },
    viewReview: async function () {
      var self = this;
      let allReview = [];
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
      let requestObject = {
        from: 0,
        size: 100,
        type: "Booking Review",
        nodeCode: agencyCode,
        orderBy: "desc"
      };
      let responseObject = await this.cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
        if (responseObject != undefined && responseObject.data != undefined) {
          allResult = JSON.parse(JSON.stringify(responseObject)).data;
          for (let i = 0; i < allResult.length; i++) {
            if (allResult[i].keyword4 == self.pageURLLink) {
              let object = {
                Name: allResult[i].keyword2,
                Date: moment(String(allResult[i].date1), "YYYY-MM-DDThh:mm:ss").format('MMM DD,YYYY'),
                comment: allResult[i].text1,
                Ratings: allResult[i].number1,
              };
              allReview.push(object);
            }
          }
          self.allReview = allReview;



        }
        self.ratingcount = allReview.length;
        allratingcount = self.ratingcount;
        var avgratingtemp = 0;
        for (let i = 0; i < self.ratingcount; i++) {

          avgratingtemp = avgratingtemp + allReview[i].Ratings;
        }
        self.avgrating = ((avgratingtemp) / allratingcount).toFixed(2);
        if (self.avgrating > 0) {
          self.reviewAvailable = true;
        }
        if (isNaN(self.avgrating)) {
          self.avgrating = 0;
        }
      });
    },

    async cmsRequestData(callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
        },
        body: data, // body data type must match "Content-Type" header
      });
      try {
        const myJson = await response.json();
        return myJson;
      } catch (error) {
        return object;
      }
    },

    dropdownChange: function (event, type) {
      let dateObj = document.getElementById("from");
      if (event != undefined && event.target != undefined && event.target.value != undefined) {

        if (type == 'Adult') {
          this.addBookingForm.fadult = event.target.value;

        } else if (type == 'Infant') {
          this.addBookingForm.finfants = event.target.value;
        } else if (type == 'Child') {
          this.addBookingForm.fchild = event.target.value;
        }
      }
      let dateValue = dateObj.value;
      setTimeout(function () {
        let dateObjNew = document.getElementById("from");
        dateObjNew.value = dateValue;
      }, 100);
    },
  }
})

function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}