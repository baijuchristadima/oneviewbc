var about = new Vue({
    el: '#about',
    name: 'about',
    data: {
      pageContents:null,
      bannerContents: null,
      bannerImage:null,
      bannerName:null,
      breadcrumb:null,
      bodyContentImage:null,
      contentTitle:null,
      contentDescription:null,
      missionAndVisionArray:[]
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var language = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var url = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/About Us Page/About Us Page/About Us Page.ftl';


                axios.get(url, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': language
                    }
                }).then(function (res) {
                    self.pageContents = res.data;
                    if(self.pageContents!=''&& self.pageContents!==null)
                    {
                        self.bannerContents = self.pluck('Banner_Area',self.pageContents.area_List);
                        if(self.bannerContents.length>0)
                        {
                            self.bannerImage = self.pluckcom('Banner_Image',self.bannerContents[0].component);
                            self.bannerName = self.pluckcom('Banner_Title',self.bannerContents[0].component);
                            self.breadcrumb = self.pluckcom('Breadcrumb',self.bannerContents[0].component);
                        }
                        var mainContents = self.pluck('About_Us_Area',self.pageContents.area_List);
                        if(mainContents.length>0)
                        {
                            self.bodyContentImage= self.pluckcom('Image',mainContents[0].component);
                            self.contentTitle= self.pluckcom('Title',mainContents[0].component);
                            self.contentDescription= self.pluckcom('Description',mainContents[0].component);
                        }
                        var listContents = self.pluck('Mission_And__Vision_Area',self.pageContents.area_List);
                        if(listContents.length>0)
                        {
                            self.missionAndVisionArray= self.pluckcom('Visions_And_Missions',listContents[0].component);
                        }
                    }
                }).catch(function (error) {
                    console.log('Error', error);
                    self.content = [];
                });


            });
        },
        getmoreinfo(url) {
            var customSearch =
                "&date=" + this.transferDate +
                "&hour=" + ('0' + (this.pickUpH - 1)).slice(-2) +
                "&min=" + ('0' + (this.pickUpM - 1)).slice(-2) +
                "&guest=" + this.guest +
                "&city=" + this.cityName.split(' ').join('-') +
                "&from=" + this.fromL.split(' ').join('-') +
                "&to=" + this.toL.split(' ').join('-') + "&";

            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/ICA/transfer-booking.html?page=" + url + customSearch;
                    window.location.href = url;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;
        },
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#from2").datepicker({
                minDate: new Date('11/15/2020'),
                maxDate: new Date('11/20/2020'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: { direction: "up" },
                showButtonPanel: false,
                dateFormat: dateFormat,
            });
        },

    },
    mounted: function () {
        var vm = this;
        vm.getPagecontent();
        sessionStorage.active_e = 3;
    }
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}