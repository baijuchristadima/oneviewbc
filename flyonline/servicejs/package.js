const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var packageList = new Vue({
    i18n,
    el: '#Packages',
    name: 'Packages',
    data: {
        content: null,
        getdata: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'AED',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        pageContent: {
            Title: '',
            Breadcrumbpath1: '',
            Breadcrumb2: '',
            Common_image: '',
        },
        packages: [],
        getpackage: false,
        banner: {},
        search: '',
        daysearch: '',
        sortBy: '',
        Pricesearch: '',
        allReview: [],
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Package List/Package List/Package List.ftl';
                axios.get(topackageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    var packageData = response.data;
                    let holidayaPackageListTemp = [];
                    if (packageData != undefined && packageData.Values != undefined) {
                        holidayaPackageListTemp = packageData.Values.filter(function (el) {
                            return el.Availability_Status == true
                        });
                    }
                    self.packages = holidayaPackageListTemp;
                    self.getpackage = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "package-detail.html?page=" + url;
                    console.log(url);
                    window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;

        },
        getBanner: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Packages Page Banner/Packages Page Banner/Packages Page Banner.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    var content = response.data;
                    if (content.area_List.length) {
                        var banner = self.pluck('Package_Page_Area', content.area_List);
                        self.banner = self.getAllMapData(banner[0].component);
                    }
                })
            });
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },

    },
    mounted: function () {
        this.getBanner();
        this.getPagecontent();
    },

});
function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}