$(document).ready(function() {

    owluaejs();
    owltourjs();
    owloffer();
    configureIndexPageSetUp();
    owlpartner();
  //  eventinfo();
});

function configureIndexPageSetUp() {
    // $('#advanceSearch').click(function () {
    //     $('.ad_view').toggle();
    // });
}

function owlpartner() {
    var owl = $("#owl-demo");
    owl.owlCarousel({
        autoPlay: false,
        items: 4, //10 items above 1000px browser width
        itemsDesktop: [1000, 4], //5 items between 1000px and 901px
        itemsDesktopSmall: [900, 3], // 3 items betweem 900px and 601px
        itemsTablet: [600, 2], //2 items between 600 and 0;
        itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
        pagination: false
    });
    $(".next").click(function() {
        owl.trigger('owl.next');
    })
    $(".prev").click(function() {
        owl.trigger('owl.prev');
    });
}

function owluaejs() {
    $("#owl-uae-attractions").owlCarousel({
        items: 3,
        itemsCustom: false,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: [600, 1],
        itemsMobile: [479, 1],
        singleItem: false,
        itemsScaleUp: false,

        //Autoplay
        autoPlay: false,
        stopOnHover: true,

        // Navigation
        navigation: true,
        navigationText: ['<img src="images/left-arrow.png">', '<img src="images/right-arrow.png">'],
        rewindNav: true,
        scrollPerPage: false,

        //Pagination
        pagination: true,
        paginationNumbers: false,

        // Responsive 
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
    });
}

function owltourjs() {
    $("#owl-tour-offers").owlCarousel({
        items: 3,
        itemsCustom: false,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: [600, 1],
        itemsMobile: [479, 1],
        singleItem: false,
        itemsScaleUp: false,

        //Autoplay
        autoPlay: false,
        stopOnHover: true,

        // Navigation
        navigation: true,
        navigationText: ['<img class="olnextleft" src="images/left-arrow.png">', '<img class="olnextright" src="images/right-arrow.png">'],
        rewindNav: true,
        scrollPerPage: false,

        //Pagination
        pagination: false,
        paginationNumbers: false,

        // Responsive 
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
    });
}

function owloffer() {
    $("#owl-hotel-offers").owlCarousel({
        items: 3,
        itemsCustom: false,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: [600, 1],
        itemsMobile: [479, 1],
        singleItem: false,
        itemsScaleUp: false,

        //Autoplay
        autoPlay: false,
        stopOnHover: true,

        // Navigation
        navigation: true,
        navigationText: ['<img src="images/left-arrow.png">', '<img src="images/right-arrow.png">'],
        rewindNav: true,
        scrollPerPage: false,

        //Pagination
        pagination: true,
        paginationNumbers: false,

        // Responsive 
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
    });

}

function eventinfo() {
    $("#eventinfo").owlCarousel({
        items: 3,
        itemsCustom: false,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: [600, 1],
        itemsMobile: [479, 1],
        singleItem: false,
        itemsScaleUp: false,

        //Autoplay
        autoPlay: true,
        stopOnHover: true,

        // Navigation
        navigation: false,
        navigationText: ['<img src="images/left-arrow.png">', '<img src="images/right-arrow.png">'],
        rewindNav: true,
        scrollPerPage: false,

        //Pagination
        pagination: true,
        paginationNumbers: false,

        // Responsive 
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
    });

}