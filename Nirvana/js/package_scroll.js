$(document).ready(function(){

    owlgallery();
     
});

    function owlgallery() {
        var gallery = $("#owl-tour-offersnew");
        gallery.owlCarousel({
            items: 4,
            itemsCustom: false,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [991, 2],
            itemsTablet: [768, 2],
            itemsTabletSmall: [600, 1],
            itemsMobile: [479, 1],
            singleItem: false,
            itemsScaleUp: false,
    
            //Autoplay
            autoPlay: true,
            stopOnHover: true,
    
            // Navigation
            navigation: true,
            navigationText: ['<img class="olnextleft" src="images/left-arrow.png">', '<img class="olnextright" src="images/right-arrow.png">'],
            rewindNav: true,
            scrollPerPage: false,
    
            //Pagination
            pagination: false,
            paginationNumbers: false,
    
            // Responsive 
            responsive: true,
            responsiveRefreshRate: 200,
            responsiveBaseWidth: window,
        });
    }
