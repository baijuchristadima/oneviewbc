$(document).ready(function(){
    awards_pic();
    // $(".awards-btn").click(function(){
    //     $(".awards_pic_scroll").slideToggle("slow");
    // });

});
function awards_pic() {
        $(".awards_pic").owlCarousel({
            items: 3,
            itemsCustom: false,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [991, 2],
            itemsTablet: [768, 2],
            itemsTabletSmall: [600, 1],
            itemsMobile: [479, 1],
            singleItem: false,
            itemsScaleUp: false,
    
            //Autoplay
            autoPlay: true,
            stopOnHover: true,
    
            // Navigation
            navigation: true,
            navigationText: ['<img style="width:3px"  src="images/left-arrow.png">', '<img style="width:3px"  src="images/right-arrow.png">'],
            rewindNav: true,
            scrollPerPage: false,
    
            //Pagination
            pagination: false,
            paginationNumbers: false,
    
            // Responsive 
            responsive: true,
            responsiveRefreshRate: 200,
            responsiveBaseWidth: window,
        });
}