$(document).ready(function () {
    $('.retrive_booking_sec').click(function () {
        $('.retrive_list').slideToggle(300);
    });
    $('.nav ul li a[href^="#"]').on('click', function (event) {
        var target = $(this.getAttribute('href'));
        if (target.length) {
            event.preventDefault();

            $('html, body').stop().animate({
                scrollTop: target.offset().top - 0
            }, 1200);
        }
    });

    $('#advanceSearch').click(function () {
        $('.ad_view').toggle();
    });

    try { autocompleteAirlines(); } catch (err) { }
});

function autocompleteAirlines() {
    $.each(AirlinesDatas, function (index, item) {
        item['label'] = item.A + '(' + item.C + ')';
    });
    $.widget('ui.autocomplete', $.ui.autocomplete, {
        _renderMenu: function (ul, items) {
            var that = this;
            ul.attr('class', 'autocomplete-results');
            $.each(items, function (index, item) {
                that._renderItemData(ul, item);
            });
            $.each(ul.children(), function (index, li) {
                $(li).attr('class', 'autocomplete-result');
            }); 
        },
        _resizeMenu: function () {
            var ul = this.menu.element;
            ul.outerWidth(Math.min(
                // Firefox wraps long text (possibly a rounding bug)
                // so we add 1px to avoid the wrapping (#7513)
                ul.width('').outerWidth(),
                this.element.outerWidth()
            ));
        }
    });
    var widgetInst = $('.preferairline').autocomplete({
        source: AirlinesDatas, minLength: 2, position: { my: 'left top', at: 'left bottom' },
        change: function (event, ui) { if (!ui.item) { $(this).val(''); $(this).prop('placeholder', 'No Match Found'); } },
        select: function (event, ui) {
            if (ui.item.id != 0) {
                if (ui.item.value.indexOf('No Match Found') < 0) {
                    this.value = ui.item.label;
                    $(this).data('val', ui.item.C);
                    maininstance.preferAirline = ui.item.C;
                }
            } return false;
        },
        focus: function () { return false; }
    });
    $.ui.autocomplete.filter = function (array, term) {
        var matcher = new RegExp($.ui.autocomplete.escapeRegex(term), "i");
        var matcher1 = new RegExp("^" + "A", "i");
        var arraylist = array;
        var arraylist2 = array;
        var list1 = $.grep(arraylist, function (value) {
            return ((matcher1.test(value.T) && matcher.test(value.I)));
        });
        var list2 = $.grep(arraylist2, function (value) {
            return ((matcher1.test(value.T) && matcher.test(value.label)));
        });
        var list3 = list1.concat(list2);

        function removeDuplicates(arr) {
            let unique_array = []
            for (let i = 0; i < arr.length; i++) {
                if (unique_array.indexOf(arr[i]) == -1) {
                    unique_array.push(arr[i])
                }
            }
            return unique_array
        }
        var list4 = removeDuplicates(list3);
        return (list4);
        //return ($.grep(array, function (value) { return ((matcher1.test(value.T) && (matcher.test(value.C) || matcher.test(value.label)))); }));
    };
}