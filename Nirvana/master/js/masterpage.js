var HeaderComponent = Vue.component('headeritem', {
    template: `<div><div  v-if="getdata"></div>
    <div class="container flx_sec ar_direction" v-else v-for="item in pluck('Header',content.area_List)">
        <div class="logo" v-for="logo in pluckcom('Logo',item.component)"><a href="/"><img :src="logo.Image" alt="logo"></a></div>
        <div class="grow_sec1"></div>
        <div class="header-right">
        <div class="header_top arborder">
        <div class="contact_number"><i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;<span dir="ltr"><a :href="'tel:'+pluckcom('Header_phonenumber',item.component)">{{pluckcom('Header_phonenumber',item.component)}}</a></span></div>
        <div class="retrive_booking">
        <div class="retrive_booking_sec" data-toggle="modal" data-target="#myModal">{{pluckcom('Retrieve_booking_label',item.component)}}</div>       
        </div>
        <!--view booking popup-->
        <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog  modal-smsp">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">View Bookings</h4>
            </div>
            <div class="modal-body">
            <div class="user_login">
            <div>
            <p><label>Enter your email address</label></p>
            <div class="validation_sec">
            <input v-model="bEmail" type="text" id="txtretrivebooking" name="text" placeholder="name@example.com" class=""> 
            <span v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveEmailErormsg }"  class="cd-signin-modal__error sp_validation">Email Required!</span>
            </div> 
            <p></p> 
            <p><label>Enter your ID number</label></p>
            <div class="validation_sec">
            <input v-model="bRef" type="text" id="text" name="text" placeholder="Example: AGY509-8509" class=""> 
            <span  v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveBkngRefErormsg }"   class="cd-signin-modal__error sp_validation">Booking Reference Id Required!</span>
            </div> 
            <p></p>
            <div class="viewradio_sec">
            <label class="radio_view">Flight
                <input v-model="bService" value="F" type="radio" checked="checked" name="radio">
                <span class="checkmark"></span>
            </label>
            <label class="radio_view">Hotel
                <input v-model="bService" value="H" type="radio" name="radio">
                <span class="checkmark"></span>
            </label>
            </div> 
            <button v-on:click="retrieveBooking" type="submit" id="retrieve-booking" class="btn-blue">Continue</button></div></div>
            </div>            
          </div>
          
        </div>
      </div>
        <!--view booking popup close-->
        <div class="currency lag">
            <currency-select></currency-select>  
        </div>
        <div class="lang lag" v-if="false">
            <lang-select @languagechange="getpagecontent"></lang-select>
        </div>
        <div class="homepage_link"><a href="/">{{pluckcom('Home_menu_lable',item.component)}}</a></div>

        <!--iflogin-->
        <label id="bind-login-info" for="profile2" class="profile-dropdown" v-show="userlogined">
            <input type="checkbox" id="profile2">
            <img v-if="[2,3,4].indexOf(userinfo.title?userinfo.title.id:1)<0" src="/assets/images/user.png">
            <img v-else src="/assets/images/user_lady.png">
            <span>{{userinfo.firstName=='Guest'?userinfo.emailId:userinfo.firstName+' '+userinfo.lastName }}</span>
            <label for="profile2"><i class="fa fa-angle-down" aria-hidden="true"></i></label>
            <ul>
                <li><a href="/customer-profile.html"><i class="fa fa-th-large" aria-hidden="true"></i>{{pluckcom('My_dashboard_label',item.component)}}</a></li>
                <li><a href="/my-profile.html"><i class="fa fa-user" aria-hidden="true"></i>{{pluckcom('My_profile_label',item.component)}}</a></li>
                <li><a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i>{{pluckcom('My_booking_label',item.component)}}</a></li>
                <li><a href="#" v-on:click.prevent="logout"><i class="fa fa-power-off" aria-hidden="true"></i>{{pluckcom('Logout_label',item.component)}}</a></li>
            </ul>
        </label>
        <!--ifnotlogin-->
        <div id="sign-bt-area" class="cd-main-nav js-main-nav" v-show="!userlogined" >
        <ul class="cd-main-nav__list js-signin-modal-trigger">
            <!-- inser more links here -->
            <li><a onclick="changeShowTxt()" class="cd-main-nav__item cd-main-nav__item--signin" href="javascript:void(0);" data-signin="login">{{pluckcom('Sign_in_lable',item.component)}}</a>
            </li>
        </ul>
        </div>


        <!--popup area-->
        <div class="cd-signin-modal js-signin-modal"> <!-- this is the entire modal form, including the background -->
		<div class="cd-signin-modal__container"> <!-- this is the container wrapper -->
			<ul class="cd-signin-modal__switcher js-signin-modal-switcher js-signin-modal-trigger">
				<li><a href="#0" data-signin="login" data-type="login">{{pluckcom('Sign_in_lable',item.component)}}</a></li>
				<li><a href="#0" data-signin="signup" data-type="signup">{{pluckcom('Register_label',item.component)}}</a></li>
			</ul>

			<div class="cd-signin-modal__block js-signin-modal-block" data-type="login"> <!-- log in form -->
                <div class="cd-signin-modal__form">
                <div class="connect">
                <a href="#"><img src="https://ovit-fileupload.s3.amazonaws.com/B2B/AdminPanel/CMS/AGY435/Images/24042019052933.jpg" alt=""></a>
                <a href="#"><img src="https://ovit-fileupload.s3.amazonaws.com/B2B/AdminPanel/CMS/AGY435/Images/24042019052949.jpg" alt=""></a>
                </div>
					<p class="cd-signin-modal__fieldset">
                        <label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" 
                        for="signin-email">{{pluckcom('signinemail_label',item.component)}}</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signin-email" type="email" :placeholder="pluckcom('signinemail_label',item.component)" v-model="username" >
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': usererrormsg.empty||usererrormsg.invalid }">{{usererrormsg.empty?'Please enter email!':(usererrormsg.invalid?'Email seems incorrect!':'')}}</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <label class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace" 
                        for="signin-password">{{pluckcom('signinpassword_label',item.component)}}</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signin-password" type="password"  :placeholder="pluckcom('signinpassword_label',item.component)"  v-model="password" >
						<a v-on:click="showhidepassword" class="cd-signin-modal__hide-password js-hide-password changeShowTxtCls">{{pluckcom('showpassword_label',item.component)}}</a>
                        <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': psserrormsg }">
                        Please enter password!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width" type="submit"
                        v-on:click="loginaction" :value="pluckcom('loginbutton_label',item.component)">
                    </p>
                    <div id="myGoogleButton"></div>
                    <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false"
        data-use-continue-as="false" onlogin="checkLoginState();"></div>
                    <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="reset">{{pluckcom('forgotpassword_label',item.component)}}</a></p>
				</div>
				
				
			</div> <!-- cd-signin-modal__block -->

			<div class="cd-signin-modal__block js-signin-modal-block" data-type="signup"> <!-- sign up form -->
                <div class="cd-signin-modal__form">
                <div class="connect">
                <a href="#"><img src="https://ovit-fileupload.s3.amazonaws.com/B2B/AdminPanel/CMS/AGY435/Images/24042019052933.jpg" alt=""></a>
                <a href="#"><img src="https://ovit-fileupload.s3.amazonaws.com/B2B/AdminPanel/CMS/AGY435/Images/24042019052949.jpg" alt=""></a>
                </div>
                    <p class="cd-signin-modal__fieldset">
                        <label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">Title</label>                        
                        <select v-model="registerUserData.title" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border down_select" id="signup-title">
                            <option selected>Mr</option>
                            <option selected>Ms</option>
                            <option>Mrs</option>
                        </select>
                        <span class="cd-signin-modal__error">Title seems incorrect!</span>
                    </p>
					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">First Name</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signup-firstname" type="text" :placeholder="pluckcom('signupfirstname_label',item.component)" v-model="registerUserData.firstName">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userFirstNameErormsg }" >Plese enter first name!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">Last Name</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signup-lastname" type="text" :placeholder="pluckcom('signuplastname_label',item.component)" v-model="registerUserData.lastName">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userLastNameErrormsg }">Plese enter last name!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" for="signup-email">Email</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signup-email" type="email" :placeholder="pluckcom('signupemail_label',item.component)" v-model="registerUserData.emailId">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userEmailErormsg.empty||userEmailErormsg.invalid }">{{userEmailErormsg.empty?'Please enter email!':(userEmailErormsg.invalid?'Email seems incorrect!':'')}}</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding" type="submit" 
                        :value="pluckcom('createaccountbutton_label',item.component)" v-on:click="registerUser">
                    </p>
                    <div id="myGoogleButtonReg"></div>
                    <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false"
        data-use-continue-as="false" onlogin="checkLoginState();"></div>                    
				</div>
			</div> <!-- cd-signin-modal__block -->

			<div class="cd-signin-modal__block js-signin-modal-block" data-type="reset"> <!-- reset password form -->
				<p class="cd-signin-modal__message">{{pluckcom('paswordlostmessage_label',item.component)}}</p>

				<div class="cd-signin-modal__form">
					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" for="reset-email">E-mail</label>
						<input v-model="emailId" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" id="reset-email" type="email" :placeholder="pluckcom('forgotpassemail_label',item.component)">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userforgotErrormsg.empty||userforgotErrormsg.invalid }">{{userforgotErrormsg.empty?'Please enter email!':(userforgotErrormsg.invalid?'Email seems incorrect!':'')}}</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding" type="submit" 
                        :value="pluckcom('resetpassbutton_label',item.component)" v-on:click="forgotPassword">
                    </p>
                    <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="login">{{pluckcom('backtologinlink_label',item.component)}}</a></p>
				</div>

				
			</div> <!-- cd-signin-modal__block -->
			<a href="#0" class="cd-signin-modal__close js-close">Close</a>
		</div> <!-- cd-signin-modal__container -->
	</div> 
        <!--popup area close-->
        </div>
        <div class="nav">
        <ul>
        <li><a href="/Nirvana/aboutus.html"><svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46668" version="1.1" id="svg8" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" sodipodi:docname="About_Us_Icon.svg"> <defs id="defs2"> <clipPath clipPathUnits="userSpaceOnUse" id="clipPath6266"> <rect style="fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.72308987;stroke-opacity:1" id="rect6268" width="235.73741" height="82.482758" x="-308.72379" y="-6.8863735" /> </clipPath> <clipPath clipPathUnits="userSpaceOnUse" id="clipPath6274"> <ellipse style="fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.80294245;stroke-opacity:1" id="ellipse6276" cx="-192.50114" cy="17.866276" rx="64.168037" ry="64.273895" /> </clipPath> <clipPath clipPathUnits="userSpaceOnUse" id="clipPath6284"> <rect style="fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.72308987;stroke-opacity:1" id="rect6286" width="177.67633" height="117.21391" x="-267.78333" y="-14.123268" /> </clipPath> <clipPath clipPathUnits="userSpaceOnUse" id="clipPath6290"> <ellipse style="fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.79538357;stroke-opacity:1" id="ellipse6292" cx="-193.01503" cy="28.365179" rx="66.930298" ry="60.109688" /> </clipPath> </defs> <sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.53740115" inkscape:cx="155.77825" inkscape:cy="310.99283" inkscape:document-units="mm" inkscape:current-layer="layer2" showgrid="false" units="px" showborder="true" inkscape:window-width="1366" inkscape:window-height="705" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" inkscape:snap-smooth-nodes="false" showguides="false" inkscape:pagecheckerboard="true" /> <metadata id="metadata5"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:groupmode="layer" id="layer2" inkscape:label="Inner Layer" style="display:inline"> <g id="g826"> <path inkscape:connector-curvature="0" id="path6169" d="M 67.624966,21.570191 C 50.079683,21.593611 34.121997,33.809605 26.651305,49.684934 35.541276,35.634474 51.000981,29.520625 67.627668,29.498224 84.14445,29.499324 99.528527,35.482067 108.46628,49.371652 100.92601,33.66149 85.050942,21.587089 67.624966,21.570191 Z M 41.409519,47.560126 c -1.056269,-0.03934 -0.421925,0.427362 1.474607,2.160595 2.583537,2.361098 3.665746,3.61332 4.459603,5.159771 l 0.514493,1.001977 v 12.474737 c 0,13.589044 -0.03504,13.008239 0.906097,14.956045 0.705872,1.460891 2.968648,3.283317 5.323168,4.287429 0.439987,0.187641 0.943737,0.325832 1.119459,0.306535 0.374722,-0.04114 0.367918,0.394024 0.108025,-7.227864 -0.189582,-5.557975 -0.241699,-15.586175 -0.08575,-16.490068 0.06785,-0.393228 0.180615,-0.571424 0.357849,-0.565806 0.142261,0.0045 1.062999,0.951015 2.046488,2.10388 2.977584,3.490354 9.59872,10.537002 12.054774,12.829206 6.164043,5.752857 11.419265,8.621974 15.797324,8.625505 3.279975,0.0026 5.367435,-1.270032 6.86732,-4.187507 1.775973,-3.454526 2.28875,-8.285835 2.20448,-20.760617 l -0.05199,-7.732231 -0.514489,-1.000625 c -0.623612,-1.213114 -1.988995,-2.593049 -3.587943,-3.626426 -1.346427,-0.870167 -3.156466,-1.777575 -3.290181,-1.649478 -0.05016,0.04806 -0.0067,0.724156 0.09587,1.502288 0.471864,3.575377 0.538501,5.168514 0.457099,10.868467 -0.05956,4.168692 -0.151628,6.224676 -0.320712,7.151571 -0.575357,3.154054 -1.232738,3.875538 -3.51907,3.862066 -2.125944,-0.01254 -3.374787,-0.602741 -6.813296,-3.221313 -2.481531,-1.889791 -4.99933,-3.920727 -8.38716,-6.766037 C 60.674082,54.944061 55.991857,51.556338 52.650685,50.065742 50.082596,48.920032 47.866129,48.357931 43.938087,47.855858 42.718876,47.700021 41.889636,47.578005 41.409514,47.560126 Z M 27.000375,85.781729 C 34.47107,101.65707 50.428756,113.87309 67.974037,113.89648 85.400011,113.87958 101.27508,101.80518 108.81536,86.095016 99.877596,99.984603 84.493522,105.96734 67.976736,105.96845 51.350051,105.94604 35.890349,99.832191 27.000375,85.781729 Z" style="stroke-width:0.15635954" /> <g inkscape:label="Layer 1" id="layer1" transform="matrix(1.0281448,0,0,1.0281447,-1.4400028,-170.00676)"> <path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="M 67.27977,165.35295 A 65.879319,65.879319 0 0 0 1.4005815,231.23213 65.879319,65.879319 0 0 0 67.27977,297.11131 65.879319,65.879319 0 0 0 133.15894,231.23213 65.879319,65.879319 0 0 0 67.27977,165.35295 Z m 0,5.06739 A 60.81168,60.81168 0 0 1 128.09155,231.23213 60.81168,60.81168 0 0 1 67.27977,292.04392 60.81168,60.81168 0 0 1 6.4679725,231.23213 60.81168,60.81168 0 0 1 67.27977,170.42034 Z" id="path2255" inkscape:connector-curvature="0" /> </g> </g> </g> </svg><p>{{pluckcom('About_us_menu_title',item.component)}}</p></a></li>
        <li><a href="/Nirvana/services.html"><svg version="1.1" id="svg3730" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:osb="http://www.openswatchbook.org/uri/2009/osb" xmlns:dc="http://purl.org/dc/elements/1.1/" sodipodi:docname="Arab.svg" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" enable-background="new 0 0 512 512" xml:space="preserve"> <sodipodi:namedview pagecolor="#ffffff" id="base" borderopacity="1.0" bordercolor="#666666" inkscape:cx="99.376519" inkscape:zoom="0.70710678" showgrid="false" inkscape:cy="126.53654" showguides="true" units="px" inkscape:guide-bbox="true" inkscape:snap-global="false" inkscape:pagecheckerboard="true" inkscape:window-maximized="1" inkscape:window-y="-8" inkscape:current-layer="layer2" inkscape:pageshadow="2" inkscape:pageopacity="0.0" inkscape:window-x="-8" inkscape:window-height="705" inkscape:document-units="mm" inkscape:window-width="1366"> </sodipodi:namedview> <g id="layer1" transform="translate(0,-161.5333)" inkscape:label="Layer 1" inkscape:groupmode="layer"> </g> <g id="layer3" inkscape:label="outer" inkscape:groupmode="layer"> </g> <g> <path d="M257.3,0C116.9,0,2.6,114.8,2.6,255.9s114.3,255.9,254.7,255.9C397.7,511.7,512,397,512,255.9 S397.7,0,257.3,0z M345.1,468c-0.5-15-0.9-30.2-1.4-34.4c-0.9-8,1.3-14.9,6.9-20.8c16-16.5,32-33.2,48-49.8c2.6-2.7,5-5.4,6.6-8.8 c10.8-23.3,21.6-46.5,32.5-69.8c4-8.6,7.6-17,7.4-26.9c-0.2-10.1,0-20.3,0.6-30.5c0.8-14.2,1.2-28.4,1.3-42.6 c0.1-10.2-6.4-17.3-15.8-18.1c-8.6-0.8-15.9,5-17.8,14.7c-3.2,16.3-5.9,32.7-9,49c-0.6,3.1,0.4,4.7,3,6.2 c13.2,7.9,14.8,20.5,5.2,32.7c-21.7,27.5-45.8,53.1-64.6,82.8c-1,1.6-2.1,4.3-4.5,2.7c-2.2-1.4-0.8-3.7,0.3-5.4 c5.3-8,10.1-16.3,16.2-23.8c16.5-20.3,33.1-40.6,49.6-60.9c5.7-7,5.2-15.1-0.8-20.9c-5.7-5.5-13.9-5.8-20.5-0.1 c-5.7,4.9-10.8,10.5-16.4,15.6c-12.2,11.1-21.9,25-37.9,31.6c-5.8,2.4-11.1,5.8-16.6,8.7c-5.4,2.9-10.2,6.6-13.7,11.8 c-8.6,12.9-17.2,25.8-25.7,38.7c-3.7,5.5-5.3,11.5-5.7,18.2c-1.1,17-0.7,34-0.8,47c0,13.1,0,47.9,0,70.2c-4.7,0.3-9.3,0.5-14.1,0.5 c-4.7,0-9.3-0.2-14-0.5c0-20.7,0-55.7,0-66.2c0,0-0.1,0-0.1,0c0-12.6-0.1-25.1,0-37.7c0.2-15.2-2.8-29.1-12.7-41.3 c-6.4-7.8-11.5-16.7-17-25.2c-4.8-7.6-11-13.2-19.2-17c-17.6-8-33.4-18.4-46.1-33.4c-2.8-3.4-6.2-6.3-9.4-9.4 c-4.7-4.4-9.2-9.1-14.3-12.9c-6.5-4.7-14.7-3.9-19.2,1.2c-5.4,6-5.4,14.8-0.2,21.2c17,20.8,34.2,41.6,50.9,62.7 c5.6,7,10.1,15,15.1,22.5c1,1.6,2,3.6-0.1,4.9c-1.9,1.2-3-0.7-3.9-2c-2-2.9-3.9-5.9-5.9-8.7c-4.2-6.1-8.2-12.3-12.8-18 c-15.6-19.4-31.5-38.5-47.2-57.9c-9.3-11.6-6.8-24.7,6.1-32.3c2.2-1.3,3.3-2.4,2.8-5.2c-3.1-16.9-6.1-33.8-9.2-50.7 c-1-5.2-4-9.1-8.7-11.4c-12.6-6.3-25.2,2.9-24.7,18c0.8,24.4,1.5,48.7,2.1,73.1c0.2,6.7,1.6,13,4.5,19.2 c10.9,23,21.7,46.1,32.3,69.3c3.2,7,7.2,13.1,12.6,18.6c14.8,14.9,29.3,30.1,43.9,45.2c5.8,6,8.9,13,7.9,21.6 c-0.5,4.4-0.9,20.1-1.4,34.9c-82.7-34.6-141-116.7-141-212.2c0-126.7,102.6-229.8,228.7-229.8C383.4,26.1,486,129.2,486,255.9 C486,351.3,427.8,433.3,345.1,468z"/> <path d="M287.3,230.5c0.2-6.7,0.6-13.3,0.8-20c0.1-1.9,0.7-3.5,1.9-4.9c4.2-5,6.5-10.8,6.5-17.5 c0-6.1,0-12.2,0-18.3c0.1-10.2,0.4-20.4-0.9-30.5c-0.3-2.6-1.5-4.4-3.9-5.2c-5.1-1.8-10.3-3.4-15.4-5.2c-1.8-0.6-3-0.3-4.2,1.2 c-3.9,5.3-7.9,10.6-11.9,15.8c-2.2,2.9-3.5,2.9-5.7,0c-4.1-5.4-8.1-10.7-12.2-16.1c-0.9-1.3-2-1.6-3.5-1.1 c-5.3,1.8-10.6,3.6-15.9,5.4c-2,0.7-3.4,2.1-3.7,4.4c-0.4,4.6-1.1,9.1-1.1,13.8c0,11.7,0.1,23.4,0,35c-0.1,7.1,2.2,13.2,6.7,18.6 c1.1,1.3,1.6,2.6,1.6,4.2c0.3,6.8,0.6,13.6,0.8,20.4c0.3,9.1,6.8,16.6,15.8,17.2c9.5,0.7,19,0.7,28.5,0 C280.5,247.1,287.1,239.5,287.3,230.5z"/> <path d="M231.5,94.8c-0.1,13.8,12,26.1,25.7,26.2c13.5,0.1,25.7-12,25.8-25.7c0.1-14-11.5-25.9-25.5-26 C243.6,69.2,231.7,80.9,231.5,94.8z"/> <path d="M183.4,233.2c-19.1-19.1-31.1-45.5-31-74.6c0.1-58,47.1-105.2,104.8-105.3V41.8c-0.1,0-0.2,0-0.3,0 c-64.2,0.4-116.1,52.9-115.9,117.3c0.1,33.3,14.2,63.2,36.6,84.4l-4.1,7.2l49,13.2l-35.8-36L183.4,233.2z"/> <path d="M337.2,74l4.1-7.1l-49-13.2l35.8,36l3.1-5.4c19.1,19.1,31.1,45.4,31,74.5c-0.1,58-47.3,105.3-104.9,105.3 v11.5c0.3,0,0.6,0,0.9,0c63.7-0.4,115.8-53.2,115.5-117.1C373.5,125.2,359.5,95.2,337.2,74z"/> </g> </svg><p>{{pluckcom('Services_menu_Icon',item.component)}}</p></a></li>
        <li><a href="/Nirvana/travel-guide.html"><svg xmlns:osb="http://www.openswatchbook.org/uri/2009/osb" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46666" version="1.1" id="svg5506" style="enable-background:new" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" sodipodi:docname="travel_guide.svg"> <defs id="defs5500"> <linearGradient osb:paint="solid" id="linearGradient6884"> <stop id="stop6882" offset="0" style="stop-color:#f10000;stop-opacity:1;" /> </linearGradient> <linearGradient osb:paint="solid" id="linearGradient6864"> <stop id="stop6862" offset="0" style="stop-color:#e50000;stop-opacity:1;" /> </linearGradient> </defs> <sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.9609375" inkscape:cx="256" inkscape:cy="256" inkscape:document-units="px" inkscape:current-layer="layer2" showgrid="false" showborder="true" inkscape:snap-grids="false" inkscape:snap-to-guides="false" units="px" inkscape:window-width="1366" inkscape:window-height="705" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" inkscape:snap-others="false" inkscape:object-nodes="false" inkscape:snap-nodes="false" inkscape:snap-global="false" inkscape:pagecheckerboard="true" /> <metadata id="metadata5503"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Outer Border" inkscape:groupmode="layer" id="layer1" transform="translate(0,-161.53334)" style="opacity:1"> <path style="stroke-width:2.92485166" d="m 3631.011,-429.20229 c 2.7825,2.39641 5.4307,4.97742 8.5142,6.96031 0.6611,0.42511 -0.1271,-1.77932 -0.7725,-2.22779 -2.4753,-1.71971 -5.089,-3.25371 -7.7417,-4.73252 z" id="path6681" inkscape:connector-curvature="0" /> <path style="stroke-width:2.92485166" d="m 1113.9983,-375.41848 a 1273.4841,1209.5962 0 0 0 -0.3163,5.99858 1273.4841,1209.5962 0 0 0 0.4548,12.19359 c -0.078,-6.12291 -0.1259,-12.20205 -0.1385,-18.19217 z" id="path6303" inkscape:connector-curvature="0" /> </g> <g inkscape:groupmode="layer" id="layer2" inkscape:label="Lens" style="opacity:1" transform="translate(0,-376.53335)"> <path style="opacity:1;fill-opacity:1;stroke-width:0.00834122;enable-background:new" d="M 67.648286,376.53335 A 67.648439,67.733392 0 0 0 -3.3920189e-6,444.26668 67.648439,67.733392 0 0 0 67.648286,512.00001 67.648439,67.733392 0 0 0 119.08487,488.1415 l -0.94707,-0.86733 a 21.636547,21.340013 0 0 1 -8.28056,2.98428 62.444715,62.523133 0 0 1 -42.208954,16.53135 62.444715,62.523133 0 0 1 -62.4448129,-62.52312 62.444715,62.523133 0 0 1 62.4448129,-62.52311 62.444715,62.523133 0 0 1 62.444814,62.52311 62.444715,62.523133 0 0 1 -2.77309,18.41361 21.636547,21.340013 0 0 1 1.03688,6.49738 21.636547,21.340013 0 0 1 -1.51118,7.71675 67.648439,67.733392 0 0 0 8.45087,-32.62774 67.648439,67.733392 0 0 0 -67.648294,-67.73333 z m 0.947589,10.4632 c -31.377515,9e-5 -56.813607,25.35242 -56.813477,56.62603 7e-5,31.27346 25.436101,56.62594 56.813477,56.62602 12.430546,5e-5 23.92704,-3.98167 33.280675,-10.73127 a 21.077885,21.299547 0 0 1 -9.411364,-4.67668 c -7.236016,4.17749 -15.465486,6.40017 -23.869311,6.41499 -2.427886,-0.0199 -4.850557,-0.22438 -7.247294,-0.61125 -0.185615,-0.43854 -0.473667,-0.81706 -1.143195,-1.5323 -0.228823,-0.24444 -0.467254,-0.56757 -0.750949,-0.64358 0.203384,-0.36784 0.407301,-0.7352 0.625017,-1.08846 1.68712,-2.73737 2.89793,-3.09275 1.066811,-7.02102 -0.228923,-3.28475 -0.04004,-3.98264 -1.680988,-4.26518 -0.498515,-0.086 -0.791821,-0.13465 -1.013652,-0.13612 -0.488077,-0.003 -0.630564,0.22146 -1.855434,0.78649 -0.149053,0.19459 -0.449731,0.30849 -0.447472,0.58413 3.59e-4,0.0398 0.02378,0.0544 0.05884,0.0527 -1.590929,0.88272 -3.262199,1.61354 -4.622332,3.04686 -0.237509,0.34026 -0.451613,0.64465 -0.651852,0.91844 -0.471727,0.0671 -0.945897,0.11474 -1.423447,0.1356 -2.45827,0.10736 -3.370512,2.38725 -2.762767,3.91679 -0.06302,0.14867 -0.123936,0.29846 -0.18322,0.44957 -4.923042,-2.5556 -9.362251,-5.94636 -13.117591,-10.02041 -0.04384,-0.19277 -0.08319,-0.37973 -0.129028,-0.56796 -0.185578,-0.76433 -0.337024,-1.66895 -0.337024,-2.01055 0,-0.53177 0.04873,-0.66972 0.33857,-0.96016 0.227544,-0.22797 0.455709,-0.339 0.695208,-0.339 0.325203,0 0.356585,-0.0333 0.355086,-0.3776 -0.002,-0.568 -0.267715,-1.45083 -0.685401,-2.27758 -0.205691,-0.40709 -0.446806,-1.08618 -0.536242,-1.50882 -0.210095,-0.99149 -0.211342,-3.03089 -0.0021,-3.81822 0.178453,-0.67155 0.577412,-1.42355 0.832493,-1.56985 0.291634,-0.16724 0.503217,-0.89252 0.577532,-1.97717 0.06692,-0.97578 0.05223,-1.08897 -0.177543,-1.37844 -0.319778,-0.40267 -2.23994,-1.69347 -2.650253,-1.78159 -1.068627,-0.2294 -1.643828,-0.73737 -1.643828,-1.45145 0,-0.24295 -0.05243,-0.47375 -0.116127,-0.5132 -0.06362,-0.0393 -0.11561,-0.18734 -0.11561,-0.32805 0,-0.43466 -1.024771,-1.84179 -3.199399,-4.394 -1.188296,-1.39471 -1.300369,-1.68309 -1.16384,-2.99157 0.0798,-0.76558 -0.08518,-1.9705 -0.33754,-2.45908 -0.09508,-0.18423 -0.477297,-0.6713 -0.84901,-1.0822 -1.055413,-1.16665 -1.169518,-1.49686 -1.169518,-3.39056 v -1.59279 l -0.726174,-0.70095 c -0.313775,-0.30338 -0.743299,-0.63735 -1.147841,-0.91479 0.903594,-6.85102 3.291733,-13.42415 6.999042,-19.26319 0.292844,-0.12012 0.584764,-0.23118 0.80256,-0.3025 0.425275,-0.13925 0.830729,-0.32191 0.900621,-0.40628 0.06991,-0.0843 0.30183,-0.51263 0.515601,-0.9513 0.665981,-1.36652 1.139567,-1.60985 3.023918,-1.55315 3.0169,0.0907 3.185983,0.062 3.185983,-0.54293 0,-0.35638 0.196086,-0.73195 0.380892,-0.7312 0.0774,4.4e-4 0.522502,0.26121 0.989394,0.57996 l 0.848494,0.57943 h 1.019843 c 0.785962,0 1.054466,-0.0414 1.171069,-0.18202 0.08319,-0.10062 0.191114,-0.58384 0.239992,-1.07386 0.226226,-2.2691 0.4483,-3.31247 0.913525,-4.28864 0.251823,-0.52839 0.692546,-1.2124 0.979586,-1.51978 0.506274,-0.54225 0.79579,-0.70867 1.936467,-1.11298 1.124045,-0.39834 1.477026,-0.91483 1.608732,-2.35581 0.136988,-1.49892 0.669824,-2.35144 1.584474,-2.5347 0.257307,-0.0517 0.418977,6.2e-4 0.675595,0.22322 0.290224,0.25023 0.336386,0.3696 0.327216,0.85221 -0.007,0.35528 -0.147994,0.88001 -0.384506,1.43111 -0.471966,1.09978 -0.711797,2.1103 -0.599208,2.52531 l 0.08671,0.31814 1.09107,-0.008 c 1.333413,-0.0122 1.994432,-0.19517 3.974603,-1.10567 1.850406,-0.85079 2.532364,-1.05397 3.39346,-1.00919 l 0.673532,0.0355 0.0351,0.86524 c 0.01908,0.47586 0.09558,0.93928 0.170318,1.02952 0.195945,0.23653 1.412407,0.11616 2.033495,-0.20131 1.195948,-0.61118 1.92392,-1.60756 2.156847,-2.95141 0.15983,-0.92197 0.536012,-1.79623 0.924364,-2.14876 0.171174,-0.15532 0.643871,-0.27765 1.631441,-0.42245 2.383883,-0.3495 4.142863,-0.976 5.096126,-1.81445 0.884506,-0.77798 1.312329,-2.02535 0.940876,-2.74488 -0.09528,-0.18456 -0.152291,-0.38116 -0.174963,-0.57631 l 5.18e-4,-10e-4 c 3.888012,0.14093 7.744284,0.75433 11.483051,1.82697 0.678359,0.81388 1.155521,1.50675 1.245387,1.84104 0.171234,0.63701 -0.08959,1.40114 -0.853652,2.5008 -0.536759,0.77249 -0.65263,1.03382 -0.767465,1.73622 -0.202795,1.24073 -0.303485,1.35038 -1.244872,1.34819 -0.42862,-6.5e-4 -0.857169,-0.0514 -0.952749,-0.11161 -0.130416,-0.0825 -0.188279,-0.42245 -0.231735,-1.36227 l -0.05781,-1.25171 -0.433538,-0.21226 c -0.238199,-0.11665 -0.889971,-0.25794 -1.448218,-0.31449 -0.558289,-0.0563 -1.327604,-0.18876 -1.709893,-0.29363 -1.351271,-0.3707 -2.415043,-0.06 -3.652547,1.06708 -0.811481,0.73905 -1.386859,1.13021 -1.850276,1.25691 -0.659281,0.18029 -1.594711,0.5885 -2.045365,0.89341 -0.671446,0.45429 -0.966683,1.11116 -0.966683,2.14823 0,0.89222 0.213327,1.35547 0.83817,1.81914 0.429152,0.31849 1.543446,0.40063 2.027818,0.14969 0.44585,-0.23097 0.536181,-0.13378 0.72153,0.77136 0.170236,0.83156 0.446315,1.06338 1.06268,0.89288 0.872394,-0.24129 1.192788,-0.98165 1.161778,-2.68229 -0.02457,-1.36027 0.128346,-1.91133 0.727721,-2.62493 0.395983,-0.47137 0.655026,-0.47503 1.042038,-0.0146 0.690353,0.82188 0.703468,0.88631 0.680755,3.46775 -0.02407,2.75751 -0.0777,3.24083 -0.393795,3.58091 -0.210964,0.22687 -0.350101,0.2493 -1.562796,0.2493 -1.168054,0 -1.399324,-0.0338 -1.878662,-0.27433 -0.66854,-0.33526 -1.488431,-0.44699 -2.371551,-0.32284 -0.626003,0.0879 -0.705741,0.13721 -1.312998,0.80944 -0.409314,0.45307 -0.738128,0.7167 -0.897008,0.71816 -0.138038,6.8e-4 -0.511839,0.15647 -0.830431,0.34579 -0.318589,0.18932 -0.631475,0.34506 -0.695207,0.34578 -0.06362,6.8e-4 -0.272322,-0.12387 -0.463471,-0.27694 -0.316152,-0.2532 -0.352477,-0.35567 -0.405149,-1.13123 -0.05583,-0.82121 -0.06971,-0.85356 -0.368509,-0.88819 -0.445218,-0.0517 -0.544344,-0.2279 -0.732366,-1.29916 -0.283286,-1.61409 -0.729266,-2.20623 -1.467319,-1.94849 -0.31796,0.11092 -1.911783,1.88618 -3.018242,3.36135 -0.454739,0.60625 -0.573765,1.1901 -0.298831,1.46554 0.220293,0.22068 0.842319,0.16766 1.077133,-0.0923 0.115335,-0.12763 0.246673,-0.2326 0.291606,-0.2326 0.04504,0 0.112547,0.30054 0.150189,0.66757 0.108743,1.05797 0.589479,1.28239 1.532864,0.71556 0.421308,-0.25317 1.202096,-0.3951 1.609765,-0.29259 0.376156,0.0947 0.687766,0.56234 0.586822,0.88089 -0.175248,0.55317 -0.499809,0.69782 -1.563315,0.69782 -0.621479,0 -1.049915,0.0522 -1.136486,0.13925 -0.16509,0.16533 -0.187936,1.5056 -0.02838,1.66529 0.203243,0.20362 0.675146,1.61373 0.716885,2.14146 l 0.04129,0.52206 -0.679207,0.0349 c -0.483733,0.0247 -0.735113,-0.0152 -0.873269,-0.1403 -0.266875,-0.24191 -0.989033,-0.53249 -1.322804,-0.53249 -0.205022,0 -0.366012,0.14501 -0.626563,0.56483 -0.270771,0.43639 -0.470306,0.61144 -0.874816,0.76615 l -0.524372,0.20027 -0.250318,1.00397 c -0.137628,0.55223 -0.328799,1.1259 -0.42528,1.27465 -0.09648,0.1488 -0.147482,0.34287 -0.113544,0.43131 0.03386,0.0883 0.493939,0.37725 1.022424,0.6415 1.358323,0.67913 1.481768,0.81422 1.481768,1.61887 0,0.5394 -0.05883,0.74074 -0.327732,1.11141 -0.516482,0.71206 -1.541438,1.60194 -3.848158,3.34205 -1.36798,1.03189 -2.575883,2.05956 -3.354234,2.85388 -0.66906,0.68274 -1.464845,1.40911 -1.768213,1.61418 -0.744234,0.50315 -1.112354,0.91821 -1.245386,1.40452 -0.06112,0.22337 -0.158995,1.50301 -0.217801,2.84345 -0.0779,1.77565 -0.156638,2.55412 -0.291089,2.86848 -0.392368,0.91744 -0.209424,2.8223 0.471728,4.90668 1.633228,4.99761 3.779534,6.95201 7.627157,6.94539 1.446331,-9.8e-4 2.281933,-0.12583 4.749811,-0.69991 0.955788,-0.22235 2.166257,-0.4335 2.69051,-0.46886 l 0.953265,-0.0641 0.437148,0.45687 c 0.24035,0.2513 0.593357,0.71548 0.784496,1.03161 0.419932,0.69436 0.787317,0.84916 2.039172,0.85846 0.99425,0.007 1.561891,0.1633 1.728473,0.47513 0.06242,0.11674 0.113543,0.5324 0.113543,0.92313 0,0.58576 -0.06951,0.84128 -0.396892,1.45928 -0.316322,0.59706 -0.377987,0.81532 -0.303992,1.07385 0.123646,0.43182 0.437566,1.04397 0.755077,1.4718 0.142101,0.1915 0.322833,0.54601 0.402055,0.78805 0.0792,0.24204 0.746518,1.3388 1.482283,2.43717 1.603225,2.39338 2.113469,3.40556 2.200199,4.36375 0.05903,0.65209 0.03935,0.72466 -0.320507,1.19486 -0.210645,0.27525 -0.564625,0.69411 -0.787075,0.93095 -0.379264,0.40372 -0.414166,0.50812 -0.562051,1.6825 -0.243632,1.93441 -0.213822,2.0291 1.061134,3.3723 l 1.083843,1.14166 0.03871,1.44467 0.03922,1.44467 0.456247,0.73329 c 0.439498,0.70708 0.456983,0.77806 0.520245,1.99438 0.06182,1.19068 0.08469,1.28105 0.400504,1.61105 0.29442,0.30764 0.511944,0.41358 0.939846,0.45792 0.06382,0.005 0.45498,-0.0325 0.869139,-0.0866 0.527228,-0.0688 1.176414,-0.29532 2.165621,-0.75624 1.018879,-0.47476 1.768005,-0.73401 2.68638,-0.92887 0.700422,-0.14861 1.456902,-0.38332 1.680989,-0.52206 0.17596,-0.10892 0.576842,-0.48065 0.981651,-0.90071 l 0.112516,0.26756 c -0.03865,-0.0901 -0.07412,-0.17928 -0.111483,-0.26912 0.110282,-0.11445 0.222003,-0.23139 0.32825,-0.34891 1.123607,-1.24263 1.88354,-1.75025 2.848958,-1.90155 0.004,-6.5e-4 0.0085,-10e-4 0.01238,-0.002 a 21.077885,21.299547 0 0 1 -2.615672,-10.22954 21.077885,21.299547 0 0 1 18.707115,-21.13815 c 0.0327,-0.10526 0.0651,-0.21021 0.0893,-0.30771 0.23152,-0.9319 0.22108,-1.88237 -0.0222,-2.03297 -0.22129,-0.13697 -1.86461,0.43429 -2.68638,0.93408 -1.611877,0.98031 -2.310068,1.08644 -3.323783,0.50538 -1.414391,-0.81071 -4.062973,-4.0214 -7.298905,-8.85007 -1.500465,-2.23899 -2.41911,-4.18903 -2.11659,-4.49205 0.194896,-0.19525 0.512089,-0.0351 1.79092,0.90331 0.657084,0.48215 1.501126,1.032 1.875564,1.22198 1.419695,0.72034 2.350829,1.85821 4.422594,5.40423 1.936166,3.31389 2.521551,3.91353 3.38417,3.4667 0.88647,-0.4592 2.40572,-2.53716 4.54852,-6.22254 0.64941,-1.11687 1.28883,-2.18204 1.42087,-2.3678 0.14265,-0.20069 0.26053,-0.58607 0.29057,-0.94869 0.049,-0.59127 0.0344,-0.62815 -0.4743,-1.17138 -0.5454,-0.58267 -1.54358,-1.24884 -2.0588,-1.37375 -0.26362,-0.064 -0.3234,-0.0159 -0.51766,0.41567 -0.27092,0.60182 -0.60353,1.03579 -0.79378,1.03579 -0.24073,0 -2.808742,-1.41852 -3.539003,-1.95475 -0.810191,-0.59493 -1.231672,-1.17278 -1.336741,-1.83166 -0.06802,-0.4273 -0.04285,-0.50532 0.236381,-0.72546 0.256778,-0.20238 0.471233,-0.24617 1.193775,-0.24617 0.72504,0 1.177708,0.0939 2.561998,0.53249 0.92476,0.29309 2.09007,0.68487 2.58935,0.87046 0.49923,0.18556 1.06202,0.33744 1.25106,0.33744 0.18899,0 0.70853,-0.18919 1.15455,-0.42037 0.6978,-0.36154 0.89141,-0.41261 1.38577,-0.36508 0.68927,0.0664 1.00683,0.3228 1.10088,0.88871 0.21661,1.30393 0.38827,1.64497 1.22629,2.43248 0.92412,0.8685 1.21296,1.31499 1.70318,2.6291 0.61734,1.65483 1.95492,4.77632 2.49749,5.82877 0.60097,1.16559 1.54696,2.28178 1.87969,2.2176 0.0607,-0.012 0.10657,-0.0518 0.14813,-0.14551 -0.0201,2.04357 -0.17289,4.08361 -0.45574,6.10727 a 21.077885,21.299547 0 0 1 7.87902,7.2119 c 1.04552,-4.29528 1.60356,-8.78065 1.60357,-13.39636 1.3e-4,-31.27376 -25.43684,-56.62615 -56.814509,-56.62603 z m 12.833729,36.6843 1.236096,0.0396 1.236098,0.0386 0.747849,0.64254 c 0.411474,0.35338 0.877073,0.72388 1.034298,0.82299 0.379662,0.23927 0.565661,0.59806 0.565661,1.09107 0,0.59365 -0.519088,1.0769 -1.272739,1.18495 -0.30414,0.0435 -0.656577,0.14761 -0.782948,0.23156 -0.622028,0.41326 -1.170551,1.58915 -1.170551,2.50915 0,0.41493 0.05463,0.57875 0.318444,0.94868 0.358101,0.50226 0.68866,0.64133 1.144227,0.48191 0.283594,-0.0992 0.319111,-0.0836 0.414441,0.1815 0.05723,0.15921 0.127239,0.78575 0.156383,1.39252 0.05243,1.08797 0.04854,1.11723 -0.283864,2.1112 -0.185306,0.55434 -0.407824,1.1029 -0.494953,1.21885 -0.154047,0.20496 -0.177775,0.20213 -0.863462,-0.0991 -0.387743,-0.17034 -0.916365,-0.45679 -1.17468,-0.63681 -0.340001,-0.23695 -0.729111,-0.37616 -1.410026,-0.50433 -1.146969,-0.21593 -1.319345,-0.14434 -1.650539,0.68844 -0.187664,0.47185 -0.232224,0.76935 -0.262185,1.7602 -0.01968,0.65587 -0.07651,1.25704 -0.126449,1.33515 -0.110931,0.17324 -0.892172,0.18922 -1.025006,0.0209 -0.05263,-0.0666 -0.396562,-0.21925 -0.76385,-0.33848 -1.012209,-0.32858 -2.451075,-1.18001 -2.975407,-1.76126 -0.534257,-0.59223 -0.788686,-0.67419 -1.592216,-0.51111 -0.328137,0.0666 -0.656892,0.0917 -0.730819,0.0558 -0.384768,-0.18709 -0.734588,-2.30214 -0.53418,-3.22992 0.122147,-0.56569 0.06252,-0.68114 -0.353021,-0.68114 -0.338976,0 -0.966686,0.36111 -0.966686,0.55597 0,0.10859 -1.068849,0.99235 -1.29958,1.07438 -0.09858,0.0352 -0.950022,0.0755 -1.892078,0.0908 l -1.297,0.0219 c -0.432145,-0.33251 -0.873793,-0.66756 -1.327964,-1.00501 l -0.0046,-0.74216 -0.0031,-0.74163 0.395859,-0.0219 c 0.662018,-0.0362 0.914183,-0.30538 1.55196,-1.65433 0.41376,-0.87507 0.674422,-1.30024 0.915588,-1.49318 0.185489,-0.14835 0.4923,-0.53693 0.681273,-0.86315 0.352718,-0.60895 0.989933,-1.20124 1.815694,-1.68772 l 0.468634,-0.27537 0.244638,0.22844 c 0.134573,0.1259 0.408186,0.53126 0.607984,0.9007 0.392577,0.72592 0.484417,0.78613 1.653118,1.08585 0.405771,0.10407 0.657198,0.24234 0.828882,0.45531 0.228252,0.28314 0.246187,0.3724 0.246187,1.21519 0,1.14429 0.0794,1.40595 0.507859,1.66894 0.439556,0.26984 1.429471,0.30133 1.555054,0.0501 0.04684,-0.0937 0.08611,-0.63621 0.08671,-1.20633 6.19e-4,-0.76604 0.03496,-1.07233 0.131092,-1.17347 0.178975,-0.18821 0.446688,-0.0576 0.717918,0.351 0.192798,0.29035 0.227607,0.45671 0.227607,1.07803 0,0.63432 0.03096,0.77328 0.226574,1.02118 0.186245,0.23603 0.278188,0.27325 0.515599,0.20862 0.158831,-0.0431 0.353708,-0.12184 0.433539,-0.17472 0.263638,-0.17475 0.447988,-0.85619 0.427342,-1.58027 -0.01458,-0.50629 0.01947,-0.73377 0.133674,-0.89341 0.198481,-0.27796 0.739292,-0.38571 1.101906,-0.21957 0.512776,0.23499 0.6662,-0.18502 0.83456,-2.29427 0.114965,-1.44048 0.290116,-2.05153 0.734949,-2.56077 z m 25.550816,25.83934 v 5.2e-4 a 19.745815,19.721118 0 0 0 -19.745549,19.72111 19.745815,19.721118 0 0 0 19.745549,19.72111 19.745815,19.721118 0 0 0 10.41985,-2.99731 l 9.36079,8.68109 c 2.01285,1.86676 5.23512,1.84956 7.2251,-0.0391 1.98999,-1.88822 1.97153,-4.91121 -0.0412,-6.77797 l -9.72464,-9.01905 a 19.745815,19.721118 0 0 0 2.50625,-9.56927 19.745815,19.721118 0 0 0 -19.74606,-19.72111 z m 0,7.61557 a 12.121193,12.106032 0 0 1 12.12149,12.10606 12.121193,12.106032 0 0 1 -12.12149,12.10606 12.121193,12.106032 0 0 1 -12.120976,-12.10606 12.121193,12.106032 0 0 1 12.120976,-12.10606 z m -51.745743,31.68062 c -0.003,0.18118 5.31e-4,0.34789 0.0093,0.5085 -0.125331,-0.0314 -0.250661,-0.063 -0.375734,-0.0954 0.124426,-0.1335 0.246623,-0.27097 0.366443,-0.41306 z m 3.963252,0.11317 c 0.232866,0.37299 0.602516,0.81408 -0.133159,0.72703 -0.153797,-0.0184 0.04136,-0.40081 0.06193,-0.60133 0.02377,-0.042 0.04745,-0.0837 0.07122,-0.1257 z" id="path6853-0" inkscape:connector-curvature="0" /> </g> </svg><p>{{pluckcom('Travel_guides_menu_title_',item.component)}}</p></a></li>
        <li><a href="/Nirvana/events.html"><svg xmlns:osb="http://www.openswatchbook.org/uri/2009/osb" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46667" version="1.1" id="svg8" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" sodipodi:docname="Events_Logo.svg"> <defs id="defs2"> <linearGradient osb:paint="solid" id="linearGradient6884"> <stop id="stop6882" offset="0" style="stop-color:#f10000;stop-opacity:1;" /> </linearGradient> <linearGradient osb:paint="solid" id="linearGradient6864"> <stop id="stop6862" offset="0" style="stop-color:#e50000;stop-opacity:1;" /> </linearGradient> </defs> <sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.65738834" inkscape:cx="-119.9205" inkscape:cy="256.10316" inkscape:document-units="mm" inkscape:current-layer="layer2" showgrid="false" showborder="true" units="px" inkscape:object-nodes="false" inkscape:object-paths="false" inkscape:snap-intersection-paths="true" inkscape:snap-smooth-nodes="true" inkscape:snap-nodes="true" inkscape:window-width="1366" inkscape:window-height="705" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" showguides="true" inkscape:guide-bbox="true" inkscape:pagecheckerboard="true" /> <metadata id="metadata5"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Outer Border" inkscape:groupmode="layer" id="layer1" transform="translate(-15.083317,-131.65091)" style="display:inline;opacity:1"> <rect y="204.36469" x="128.77872" height="10.021408" width="0" id="rect4702" style="opacity:0.328;stroke-width:0.26458332" /> </g> <g inkscape:groupmode="layer" id="layer2" inkscape:label="Square" style="display:inline" transform="translate(-15.083317,29.882414)"> <g id="g1392"> <g style="display:inline;opacity:1;" inkscape:label="Layer 1" id="layer1-7" transform="translate(23.141937,-185.98952)"> <g transform="matrix(1.0281448,0,0,1.0281447,-9.4986227,-13.899648)" id="layer1-4" inkscape:label="Layer 1"> <path inkscape:connector-curvature="0" id="path2255" d="M 67.27977,165.35295 A 65.879319,65.879319 0 0 0 1.4005815,231.23213 65.879319,65.879319 0 0 0 67.27977,297.11131 65.879319,65.879319 0 0 0 133.15894,231.23213 65.879319,65.879319 0 0 0 67.27977,165.35295 Z m 0,5.06739 A 60.81168,60.81168 0 0 1 128.09155,231.23213 60.81168,60.81168 0 0 1 67.27977,292.04392 60.81168,60.81168 0 0 1 6.4679725,231.23213 60.81168,60.81168 0 0 1 67.27977,170.42034 Z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /> </g> </g> <path inkscape:connector-curvature="0" id="rect862" d="m 54.42565,-1.0412447 c -1.83652,0 -3.3156,1.47821103 -3.3156,3.314731 v 4.3963412 h -8.9185 c -3.20424,-3.3e-6 -5.78374,2.5794851 -5.78374,5.7837425 v 58.505772 c 0,3.204261 2.5795,5.783741 5.78374,5.783741 h 81.25021 c 3.20425,0 5.78373,-2.57948 5.78373,-5.783741 V 12.45357 c 0,-3.2042574 -2.57948,-5.7837425 -5.78373,-5.7837425 h -8.97441 V 2.5687873 c 0,-1.83651797 -1.4782,-3.31472997 -3.31473,-3.31472997 -1.83653,0 -3.31561,1.478212 -3.31561,3.31472997 V 6.6698275 H 57.74038 V 2.2734863 c 0,-1.83651997 -1.47821,-3.314731 -3.31473,-3.314731 z M 44.54174,21.242759 h 75.78186 l 0.25774,26.04955 h 0.17735 v 16.551807 c -0.23672,3.014678 -2.21636,5.421536 -4.73532,5.821308 h -13.3096 v 0.193961 l -53.00423,0.06291 c -2.57958,-0.233128 -4.64759,-2.172856 -4.99131,-4.651456 z m 20.992709,3.763798 c -0.546678,0 -0.986377,0.74333 -0.986377,1.666978 v 3.931545 c 0,0.923645 0.439699,1.666975 0.986377,1.666975 h 7.43412 c 0.546679,0 0.986379,-0.74333 0.986379,-1.666975 v -3.931545 c 0,-0.923648 -0.4397,-1.666978 -0.986379,-1.666978 z m 27.447436,0.147645 c -0.546678,0 -0.986382,0.743332 -0.986382,1.666974 v 3.930674 c 0,0.923643 0.439704,1.667848 0.986382,1.667848 H 100.416 c 0.54668,0 0.98726,-0.744205 0.98726,-1.667848 v -3.930674 c 0,-0.923642 -0.44058,-1.666974 -0.98726,-1.666974 z m 12.986335,0 c -0.54668,0 -0.98638,0.743327 -0.98638,1.666974 v 3.930674 c 0,0.923643 0.4397,1.667848 0.98638,1.667848 h 7.43412 c 0.54667,0 0.98636,-0.744205 0.98636,-1.667848 v -3.930674 c 0,-0.923647 -0.43969,-1.666974 -0.98636,-1.666974 z m -26.867312,0.167749 c -0.546679,0 -0.986383,0.74333 -0.986383,1.666976 v 3.931546 c 0,0.923647 0.439704,1.666975 0.986383,1.666975 h 7.434114 c 0.546679,0 0.987256,-0.743328 0.987256,-1.666975 v -3.931546 c 0,-0.923646 -0.440577,-1.666976 -0.987256,-1.666976 z M 51.83782,35.742296 c -0.54668,0 -0.98638,0.743314 -0.98638,1.666977 v 3.931545 c 0,0.923646 0.4397,1.666975 0.98638,1.666975 h 7.434114 c 0.546682,0 0.987259,-0.743329 0.987259,-1.666975 v -3.931545 c 0,-0.923663 -0.440577,-1.666977 -0.987259,-1.666977 z m 13.696629,0.10834 c -0.546678,0 -0.986377,0.743313 -0.986377,1.666976 v 3.931546 c 0,0.923645 0.439699,1.666976 0.986377,1.666976 h 7.43412 c 0.546679,0 0.986379,-0.743331 0.986379,-1.666976 v -3.931546 c 0,-0.923663 -0.4397,-1.666976 -0.986379,-1.666976 z m 27.447436,0.147644 c -0.546678,0 -0.986382,0.74333 -0.986382,1.666977 v 3.931545 c 0,0.923646 0.439704,1.666977 0.986382,1.666977 H 100.416 c 0.54668,0 0.98726,-0.743331 0.98726,-1.666977 v -3.931545 c 0,-0.923647 -0.44058,-1.666977 -0.98726,-1.666977 z m 12.986335,0 c -0.54668,0 -0.98638,0.74333 -0.98638,1.666977 v 3.931545 c 0,0.923646 0.4397,1.666977 0.98638,1.666977 h 7.43412 c 0.54667,0 0.98636,-0.743331 0.98636,-1.666977 v -3.931545 c 0,-0.923647 -0.43969,-1.666977 -0.98636,-1.666977 z m -26.867312,0.167748 c -0.546679,0 -0.986383,0.744205 -0.986383,1.66785 v 3.930672 c 0,0.923645 0.439704,1.667849 0.986383,1.667849 h 7.434114 c 0.546679,0 0.987256,-0.744204 0.987256,-1.667849 v -3.930672 c 0,-0.923645 -0.440577,-1.66785 -0.987256,-1.66785 z M 51.83782,46.586372 c -0.54668,0 -0.98638,0.743332 -0.98638,1.666978 v 3.931546 c 0,0.923643 0.4397,1.666973 0.98638,1.666973 h 7.434114 c 0.546682,0 0.987259,-0.74333 0.987259,-1.666973 V 48.25335 c 0,-0.923646 -0.440577,-1.666978 -0.987259,-1.666978 z m 13.696629,0.108339 c -0.546678,0 -0.986377,0.743332 -0.986377,1.666975 v 3.931549 c 0,0.923647 0.439699,1.666975 0.986377,1.666975 h 7.43412 c 0.546679,0 0.986379,-0.743328 0.986379,-1.666975 v -3.931547 c 0,-0.923644 -0.4397,-1.666973 -0.986379,-1.666973 z m 27.447436,0.147645 c -0.546678,0 -0.986382,0.743332 -0.986382,1.666975 v 3.931546 c 0,0.923648 0.439704,1.666978 0.986382,1.666978 H 100.416 c 0.54668,0 0.98726,-0.74333 0.98726,-1.666978 v -3.931542 c 0,-0.923646 -0.44058,-1.666975 -0.98726,-1.666975 z m 12.986335,0 c -0.54668,0 -0.98638,0.743314 -0.98638,1.666975 v 3.931546 c 0,0.923648 0.4397,1.666978 0.98638,1.666978 h 7.43412 c 0.54667,0 0.98636,-0.74333 0.98636,-1.666978 v -3.931542 c 0,-0.923664 -0.43969,-1.666975 -0.98636,-1.666975 z m -26.867312,0.168628 c -0.546679,0 -0.986383,0.743328 -0.986383,1.666975 v 3.93067 c 0,0.923652 0.439704,1.667854 0.986383,1.667854 h 7.434114 c 0.546679,0 0.987256,-0.744202 0.987256,-1.667854 v -3.930667 c 0,-0.923646 -0.440577,-1.666973 -0.987256,-1.666973 z M 51.44991,57.398127 c -0.54668,0 -0.98638,0.743315 -0.98638,1.666978 v 3.931545 c 0,0.923645 0.4397,1.666975 0.98638,1.666975 h 7.434114 c 0.546678,0 0.986382,-0.74333 0.986382,-1.666975 v -3.931542 c 0,-0.923663 -0.439704,-1.666978 -0.986382,-1.666978 z m 13.566451,0.315397 c -0.546677,0 -0.986383,0.743328 -0.986383,1.66698 v 3.931544 c 0,0.923645 0.439706,1.666975 0.986383,1.666975 h 7.434116 c 0.546677,0 0.986382,-0.74333 0.986382,-1.666975 v -3.93154 c 0,-0.923649 -0.439705,-1.666979 -0.986382,-1.666979 z" style="opacity:1;fill-opacity:1;stroke-width:0.44732258" /> </g> </g> <g inkscape:groupmode="layer" id="layer3" inkscape:label="Small Square" style="display:inline" transform="translate(-15.083317,29.882414)" /> </svg><p>{{pluckcom('Events_menu_title',item.component)}}</p></a></li>
        <li><a href="/Nirvana/package.html"><svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46667" version="1.1" id="svg8" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" sodipodi:docname="travel_final.svg"> <defs id="defs2"> <clipPath clipPathUnits="userSpaceOnUse" id="clipPath51"> <rect style="fill-rule:evenodd" id="rect53" width="499.8577" height="227.66798" x="1.0118577" y="99.162056" /> </clipPath> </defs> <sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.9296875" inkscape:cx="256" inkscape:cy="256" inkscape:document-units="mm" inkscape:current-layer="layer1" showgrid="false" units="px" inkscape:object-nodes="true" showguides="true" inkscape:guide-bbox="true" inkscape:window-width="1366" inkscape:window-height="705" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" inkscape:pagecheckerboard="true" inkscape:snap-nodes="false" inkscape:snap-global="false" /> <metadata id="metadata5"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(0,-161.53331)"> <g id="g332"> <g inkscape:label="Layer 1" id="layer1-4" transform="matrix(1.0281448,0,0,1.0281447,-0.87081532,-7.9042566)"> <path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="M 67.27977,165.35295 A 65.879319,65.879319 0 0 0 1.4005815,231.23213 65.879319,65.879319 0 0 0 67.27977,297.11131 65.879319,65.879319 0 0 0 133.15894,231.23213 65.879319,65.879319 0 0 0 67.27977,165.35295 Z m 0,5.06739 A 60.81168,60.81168 0 0 1 128.09155,231.23213 60.81168,60.81168 0 0 1 67.27977,292.04392 60.81168,60.81168 0 0 1 6.4679725,231.23213 60.81168,60.81168 0 0 1 67.27977,170.42034 Z" id="path2255" inkscape:connector-curvature="0" /> </g> <path inkscape:connector-curvature="0" id="rect892" d="m 25.160057,205.20891 1.970354,4.10538 8.723695,-4.18689 -1.970354,-4.10538 z" style="stroke-width:0.26458332" /> <path inkscape:transform-center-y="-1.4229692" inkscape:transform-center-x="-3.9843138" style="stroke-width:0.26458332" d="m 10.777633,218.19516 3.22213,3.21781 6.837665,-6.84684 -3.22213,-3.21781 z" id="path898" inkscape:connector-curvature="0" /> <path inkscape:connector-curvature="0" id="path900" d="m 102.63388,206.74955 1.97035,-4.10538 8.7237,4.18689 -1.97035,4.10538 z" style="stroke-width:0.26458332" /> <path style="stroke-width:0.23867022" d="m 117.03215,216.70592 2.64582,-3.52482 5.71527,5.93765 -2.64582,3.52482 z" id="path902" inkscape:connector-curvature="0" /> <path sodipodi:nodetypes="cccccccsscccscsccccccccccc" inkscape:connector-curvature="0" id="path68" d="m 61.488706,219.9099 c -0.282942,-0.26853 -0.280154,-0.2836 0.532027,-2.85729 1.302494,-7.01015 1.633452,-12.1821 0.923637,-13.10683 -0.411489,-0.53614 -0.168253,-0.43641 -4.435752,-0.37303 -4.993995,0.0741 -5.072246,0.0977 -7.147636,2.15184 -1.563263,1.54716 -2.227665,1.94382 -2.682326,1.60133 -0.888006,-0.66895 -1.177125,-2.55644 -1.172871,-7.65691 0.0038,-3.94843 0.143346,-5.48912 0.566912,-6.23657 0.556192,-0.98147 1.167344,-0.71864 3.624063,1.55861 2.177829,2.01873 2.964818,2.48379 4.558997,2.69405 3.613583,0.47665 5.907299,0.1264 6.89736,-1.23898 0.237362,-2.34546 0.418652,-3.1006 0.154661,-5.9631 -0.213647,-4.13064 -0.179589,-4.80783 -0.698767,-8.73054 -0.252302,-1.90629 1.417962,-1.65509 3.537639,0.65203 1.252812,1.36356 2.345,2.99813 4.642641,6.94813 2.864522,4.92454 4.029811,6.37124 5.689878,7.06381 0.862902,0.36001 2.575421,0.42112 5.429171,0.19378 3.032956,-0.24183 5.016272,0.4187 5.880132,1.9579 0.405467,0.7225 0.446326,0.92651 0.40072,2.0007 -0.05749,1.35437 -0.315259,1.81491 -1.269438,2.26671 -0.89047,0.4217 -1.93788,0.54811 -5.273431,0.6364 -4.833791,0.12782 -5.588073,0.31682 -7.035237,1.76176 -1.023044,1.02146 -1.98806,2.51731 -4.428876,6.86503 -3.176237,5.65782 -4.09782,6.9371 -5.417193,7.51989 -0.868443,0.38363 -2.981162,0.57151 -3.276311,0.29146 z" style="stroke-width:0.15312754" /> <path inkscape:connector-curvature="0" id="path307" d="m -88.508683,210.76804 c 0,0 -1.138375,11.66835 0,12.80672 1.138375,1.13838 34.720448,10.81457 40.412326,7.96863 5.691875,-2.84594 25.898038,-25.61344 18.498597,-26.75182 -7.399438,-1.13837 -43.82745,-1.70756 -43.82745,-1.70756 z" style="fill:none;stroke-width:0.26458332" /> <g transform="translate(0,-1.0583333)" id="g318"> <path style="stroke-width:0.32753712" d="M 67.702188,228.23687 A 46.204662,42.632331 0.68660245 0 0 21.62763,270.52609 l 6.367551,-0.005 a 39.707746,36.673705 0.67417453 0 1 39.586255,-36.20104 39.707746,36.673705 0.67417453 0 1 39.805544,36.13897 l 6.64466,-0.005 A 46.204662,42.632331 0.68660245 0 0 67.702188,228.23671 Z" id="path128" inkscape:connector-curvature="0" /> <path style="fill-opacity:1;stroke-width:0.30711973" d="m 54.619746,280.11627 c -3.611411,-0.58053 -5.961211,-1.33636 -11.260774,-3.6221 l -4.430012,-1.93013 0.247077,-2.03072 c 0.186835,-2.84058 0.143206,-3.90005 0.0813,-6.11524 l -0.01406,-3.83427 -0.710859,-0.73727 c -2.842089,-2.32311 -5.682036,-3.26068 -7.679536,-2.82546 -1.060033,0.20488 -1.191322,1.69273 -1.893338,3.98241 -0.533399,1.56373 -0.525305,1.9526 -0.702025,1.9331 -1.323612,-0.14596 -2.332303,-1.11775 -2.524724,-1.71037 -0.194019,-0.32232 -0.169784,-1.10677 0.0064,-2.51379 1.477058,-6.23652 1.469251,-8.48498 8.547516,-16.66896 2.557196,-2.95665 2.525997,-2.9742 8.518018,-7.19794 4.101208,-1.9272 4.212674,-2.03046 8.633197,-3.37056 l 8.54893,-1.10217 7.582608,-0.29188 c 7.697567,-0.0503 7.844977,-0.11022 17.098138,1.674 9.264728,4.48676 10.759893,4.49627 15.199911,9.0269 4.535587,4.01453 4.648097,5.66798 6.954927,9.88053 1.8195,5.47461 1.2333,3.45272 1.89444,5.80287 -0.0964,1.69505 -0.26165,2.24854 -0.82011,2.74512 -1.0856,0.96523 -1.56936,0.52847 -2.17639,-1.96501 -1.27637,-5.24267 -4.28101,-10.10724 -8.662474,-14.02455 -4.025085,-3.59869 -10.702995,-7.56439 -14.6094,-8.67585 -1.017898,-0.28959 -2.51027,-0.7482 -3.316402,-1.01907 -2.105481,-0.70753 -18.203157,-1.0087 -18.816096,-0.35204 -0.644922,0.69093 -2.435851,7.17076 -2.478597,8.9679 -0.03596,1.51078 0.05559,1.73552 1.10079,2.70343 1.498394,1.38757 2.269039,1.71696 5.322916,2.27517 2.728808,0.4988 4.495098,1.12405 5.146361,1.82175 0.232741,0.24932 0.503583,1.87227 0.642625,3.85072 0.29975,4.26504 0.86422,5.36143 4.708754,9.14616 1.50959,1.48611 2.744702,2.88734 2.744702,3.11387 0,0.51488 -1.485979,1.85953 -2.312868,2.09289 -0.340169,0.096 -0.618501,0.29153 -0.618501,0.43451 0,0.14299 -0.435465,0.701 -0.967709,1.23998 -1.826599,1.84967 -3.087086,4.75546 -3.760553,8.66912 l -0.186879,1.08597 -6.255123,-0.0262 c -3.819108,-0.0162 -7.239452,-0.18425 -8.782647,-0.4323 z" id="path878" inkscape:connector-curvature="0" sodipodi:nodetypes="cscccccccsccscccccccccsscccccccccscscscccc" /> <path style="stroke-width:0.26458332" d="m 39.255195,271.38212 -0.609235,5.84375 c 0,0 7.786646,9.50576 33.03091,7.60052 25.244255,-1.90523 -1.276995,-14.07191 -1.276995,-14.07191 z" id="path311" inkscape:connector-curvature="0" /> </g> </g> </g> </svg><p>{{pluckcom('Packages_menu_title',item.component)}}</p></a></li>
        <li><a href="/Nirvana/offers.html"><svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" version="1.1" id="svg2" width="512" height="512" viewBox="0 0 512 512" sodipodi:docname="Offer White New.svg" inkscape:version="0.92.4 (5da689c313, 2019-01-14)"><metadata id="metadata8"><rdf:RDF><cc:Work rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /><dc:title></dc:title></cc:Work></rdf:RDF></metadata><defs id="defs6" /><sodipodi:namedview borderopacity="1" objecttolerance="10" gridtolerance="10" guidetolerance="10" inkscape:pageopacity="0" inkscape:pageshadow="2" inkscape:window-width="1366" inkscape:window-height="705" id="namedview4" showgrid="false" inkscape:zoom="0.65186406" inkscape:cx="58.092532" inkscape:cy="242.71671" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" inkscape:current-layer="layer2" inkscape:pagecheckerboard="true" /><g inkscape:groupmode="layer" id="layer5" inkscape:label="Inner 3" /><g inkscape:groupmode="layer" id="layer4" inkscape:label="inner 2" /><g inkscape:groupmode="layer" id="layer3" inkscape:label="Inner" /><g inkscape:groupmode="layer" id="layer2" inkscape:label="Outer" style="display:inline"><g id="g1294"><g inkscape:label="Layer 1" id="layer1" transform="matrix(3.8872253,0,0,3.8859945,-5.4222514,-642.23081)"><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="M 67.27977,165.35295 A 65.879319,65.879319 0 0 0 1.4005815,231.23213 65.879319,65.879319 0 0 0 67.27977,297.11131 65.879319,65.879319 0 0 0 133.15894,231.23213 65.879319,65.879319 0 0 0 67.27977,165.35295 Z m 0,5.06739 A 60.81168,60.81168 0 0 1 128.09155,231.23213 60.81168,60.81168 0 0 1 67.27977,292.04392 60.81168,60.81168 0 0 1 6.4679725,231.23213 60.81168,60.81168 0 0 1 67.27977,170.42034 Z" id="path2255" inkscape:connector-curvature="0" /></g><g transform="translate(-7.9129803,-21.176722)" id="g1279"><g id="g158" transform="rotate(6.3275467,306.73838,399.25664)"><g transform="matrix(0.52441485,-0.12559829,0.12893762,0.52543277,569.74728,63.382243)" id="g28-2"><path inkscape:connector-curvature="0" id="path38-0" d="m -845.11814,502.94756 c -3.22842,-1.29151 -6.82013,-4.66785 -8.59552,-8.08011 -2.11099,-4.05726 -2.1054,-317.55829 0.006,-321.59932 1.89416,-3.6257 6.45575,-7.83374 9.90438,-9.13672 1.89028,-0.7142 25.59343,-1.06362 72.15105,-1.06362 h 69.33595 l 0.36214,-17.25 c 0.33834,-16.11655 0.53442,-17.61927 2.98411,-22.87001 4.73051,-10.13948 12.90984,-17.26759 22.85165,-19.91472 3.29636,-0.87769 23.08957,-1.19018 74,-1.16829 68.06578,0.0293 69.61514,0.0731 75.07935,2.12446 10.26291,3.85284 18.66316,13.15314 21.41183,23.70603 1.01597,3.90055 1.50882,10.62382 1.50882,20.58263 v 14.7899 h 69.84272 69.84271 l 4.40729,2.32502 c 2.74756,1.44945 5.2484,3.80312 6.64083,6.25 l 2.23355,3.92498 0.0164,158.60212 0.0165,158.60212 -2.25,3.30594 c -1.2375,1.81826 -3.52048,4.24731 -5.07328,5.39788 l -2.82328,2.09194 -240.67672,0.18984 c -158.54992,0.12506 -241.52963,-0.15136 -243.17621,-0.81007 z m 321.99949,-355.42943 c 0,-17.4708 -0.55639,-19.48481 -6.67128,-24.14864 l -3.67339,-2.8017 h -69.7545 c -69.21012,0 -69.77654,0.0163 -72.57767,2.09195 -1.55274,1.15057 -3.83566,3.57962 -5.07316,5.39789 -2.09968,3.08506 -2.25,4.36502 -2.25,19.15805 v 15.85211 h 80 80 z" /><path inkscape:connector-curvature="0" id="path36-6" d="m -845.61865,501.66855 c -3.76091,-1.66421 -5.99586,-4.14995 -7.96186,-8.85524 -1.36172,-3.25904 -1.5375,-21.37737 -1.53256,-157.9634 0.004,-103.04223 0.3486,-155.51725 1.03843,-158.00107 1.12625,-4.05526 4.93282,-9.11624 8.31385,-11.05359 1.61405,-0.92486 19.64521,-1.35072 73.14214,-1.72746 l 71,-0.5 0.5,-17.52591 c 0.49478,-17.34284 0.53433,-17.59561 3.78675,-24.19736 3.92854,-7.97416 10.0873,-13.76721 18.06616,-16.99338 l 5.64709,-2.28335 h 71 71 l 5.5,2.59017 c 6.73466,3.17161 11.17797,6.83886 14.87255,12.27495 4.80431,7.06889 5.4458,10.10334 6.05354,28.63488 l 0.57391,17.5 71,0.5 c 53.49693,0.37674 71.52808,0.8026 73.14214,1.72746 3.44073,1.97156 7.22297,7.05788 8.34394,11.22086 0.70265,2.60941 0.9538,54.11656 0.7763,159.20453 -0.25893,153.30686 -0.28865,155.39175 -2.26238,158.74262 -1.1,1.86751 -3.27048,4.34251 -4.82328,5.5 l -2.82328,2.10453 -240.67672,0.21414 c -198.21706,0.17636 -241.20597,-0.0201 -243.67672,-1.11338 z m 323.30502,-352.60076 c 0.17482,-8.31083 -0.15058,-16.06105 -0.76236,-18.15725 -1.29938,-4.45222 -6.65964,-9.36476 -11.76533,-10.78262 -5.13962,-1.42728 -131.36148,-1.44219 -136.49628,-0.0161 -4.76697,1.32392 -9.67796,5.46501 -11.41702,9.62719 -1.43045,3.42353 -2.02422,32.33529 -0.69145,33.66805 0.36992,0.36992 36.70742,0.55742 80.75,0.41667 l 80.07742,-0.25592 z" /><path inkscape:connector-curvature="0" id="path34-6" d="m -843.11865,501.90031 c -1.925,-0.54753 -4.5705,-1.97134 -5.87888,-3.16402 -5.3827,-4.90668 -5.12112,3.50445 -5.12112,-164.6685 0,-170.72531 -0.40192,-159.53091 5.94213,-165.5 l 3.18846,-3 71.10602,-0.26974 c 49.98144,-0.1896 71.50076,-0.59733 72.43471,-1.37244 1.00571,-0.83467 1.32868,-5.07459 1.32868,-17.44275 0,-13.98087 0.29357,-17.12469 2.03335,-21.77465 2.86169,-7.64855 8.73368,-13.9081 16.74826,-17.85367 l 6.67634,-3.28675 h 71.52102 71.52103 l 6.30048,2.90922 c 7.73281,3.57059 14.27642,10.50764 17.14577,18.17667 1.76392,4.7145 2.05375,7.79508 2.05375,21.82918 0,12.36816 0.32297,16.60808 1.32868,17.44275 0.93396,0.77511 22.45601,1.18284 72.44483,1.37244 l 71.11613,0.26974 3.58144,3.47162 c 1.96979,1.90938 4.03976,5.05938 4.59993,7 0.67767,2.34765 0.91986,55.95823 0.72374,160.20516 l -0.29475,156.67679 -2.10453,2.82321 c -1.15749,1.55277 -3.63249,3.72323 -5.5,4.82322 -3.36767,1.98363 -5.34373,2.00135 -241.39547,2.16402 -155.44333,0.10712 -239.21407,-0.18131 -241.5,-0.8315 z m 320.8,-338.03252 c 0.8217,-0.8217 1.2,-5.71825 1.2,-15.53211 0,-15.26701 -0.90809,-19.39789 -5.29773,-24.09925 -4.99175,-5.34623 -2.35626,-5.16864 -76.70227,-5.16864 -74.34601,0 -71.71052,-0.17759 -76.70227,5.16864 -4.38964,4.70136 -5.29773,8.83224 -5.29773,24.09925 0,9.81386 0.3783,14.71041 1.2,15.53211 1.73785,1.73785 159.86215,1.73785 161.6,0 z" /><path inkscape:connector-curvature="0" id="path32-2" d="m -843.78574,500.97468 c -3.4447,-1.00185 -8.02087,-5.7711 -8.86011,-9.23396 -0.28965,-1.19511 -0.40201,-72.90248 -0.24971,-159.34971 0.27655,-156.9737 0.27957,-157.18036 2.3387,-159.93822 1.13398,-1.51878 3.30421,-3.68923 4.82272,-4.82321 2.71983,-2.03109 3.82452,-2.0659 74.18822,-2.33748 55.4664,-0.21409 71.44704,-0.54927 71.51575,-1.5 0.0487,-0.67337 0.23703,-8.2402 0.41861,-16.81518 0.18157,-8.57498 0.82287,-17.42078 1.42511,-19.65733 2.55658,-9.49443 11.77404,-18.8992 21.29343,-21.72616 7.6918,-2.28422 139.85694,-2.28422 147.54874,0 9.49768,2.82051 18.73668,12.2311 21.27961,21.67483 0.59464,2.20832 1.10631,10.60412 1.13704,18.65733 0.0307,8.05321 0.27123,15.64319 0.53442,16.86662 l 0.47854,2.22442 71.32528,0.27558 c 70.26077,0.27147 71.3665,0.30635 74.08623,2.33737 1.51851,1.13398 3.68874,3.30443 4.82272,4.82322 2.05953,2.7584 2.06207,2.93487 2.31331,160.93821 0.24711,155.4026 0.21726,158.23284 -1.70226,161.37245 -1.07458,1.75761 -3.19294,4.00761 -4.70746,5 -2.63296,1.72523 -13.2336,1.81152 -241.79774,1.96833 -143.28676,0.0983 -240.31274,-0.20498 -242.21115,-0.75711 z M -520.72564,163.486 c 1.02085,-2.66028 0.66321,-27.67559 -0.45314,-31.69554 -1.30526,-4.7002 -6.36809,-10.58581 -10.64213,-12.37162 -4.76197,-1.98968 -137.83351,-1.98968 -142.59548,0 -4.27404,1.78581 -9.33687,7.67142 -10.64213,12.37162 -1.11635,4.01995 -1.47399,29.03526 -0.45314,31.69554 0.54665,1.42454 8.73734,1.58179 82.39301,1.58179 73.65567,0 81.84636,-0.15725 82.39301,-1.58179 z" /><path inkscape:connector-curvature="0" id="path30-3" d="m -844.99716,499.35788 c -1.85817,-0.9154 -4.21899,-3.04266 -5.24626,-4.72723 -1.8028,-2.95635 -1.86775,-8.53993 -1.86775,-160.56286 0,-154.35294 0.0389,-157.56385 1.94852,-160.69567 4.39344,-7.20545 -0.54977,-6.74839 78.99916,-7.30433 l 71.54484,-0.5 0.52487,-18.07154 c 0.52181,-17.96627 0.54414,-18.1107 3.83384,-24.79302 3.9209,-7.96447 10.57405,-13.79557 18.38812,-16.1161 7.655,-2.27329 139.85134,-2.27329 147.50634,0 7.81407,2.32053 14.46722,8.15163 18.38812,16.1161 3.2897,6.68232 3.31203,6.82675 3.83384,24.79302 l 0.52487,18.07154 71.54472,0.5 c 79.54878,0.55594 74.60586,0.0989 78.99928,7.30433 1.90959,3.13182 1.94852,6.34273 1.94852,160.69567 0,156.21224 -0.0165,157.52706 -2.01815,160.81018 -4.0664,6.66969 15.07587,6.19211 -247.19225,6.16705 -220.21715,-0.021 -238.53826,-0.14895 -241.66063,-1.68714 z m 324.96079,-335.44383 c 0.78142,-1.46009 1.02074,-7.06286 0.7431,-17.39647 -0.39801,-14.8138 -0.49278,-15.37715 -3.36746,-20.01863 -1.92948,-3.11535 -4.49029,-5.58518 -7.3652,-7.10352 l -4.40729,-2.32764 h -68.68543 -68.68543 l -4.40729,2.32764 c -2.87491,1.51834 -5.43572,3.98817 -7.3652,7.10352 -2.87468,4.64148 -2.96945,5.20483 -3.36746,20.01863 -0.27764,10.33362 -0.0383,15.93638 0.7431,17.39648 l 1.15265,2.15373 h 81.92963 81.92963 z" /></g><path style="stroke-width:0.19473971" d="m 311.93552,192.48934 c 0.36412,0.11529 0.86309,0.4599 1.10882,0.76579 0.43125,0.53683 0.6533,1.68154 6.38531,32.91781 5.81987,31.71501 5.93404,32.37628 5.71339,33.09458 -0.50763,1.65261 0.35197,1.36506 -13.73752,4.59543 l -12.67183,2.90533 0.58829,3.73374 c 0.58485,3.71198 0.58634,3.74253 0.25476,5.24442 -0.39519,1.79006 -1.35547,3.2488 -2.65404,4.0317 -1.27213,0.76696 -24.72122,5.94543 -26.16479,5.7782 -1.47356,-0.1707 -2.87356,-1.1082 -3.86935,-2.59107 -0.83549,-1.24416 -0.8449,-1.27296 -1.61487,-4.94406 l -0.77449,-3.69261 -12.70951,2.69985 c -14.13139,3.00191 -13.23737,2.90219 -14.28836,1.59378 -0.45681,-0.56869 -0.58478,-1.22691 -6.40465,-32.94192 -5.88997,-32.09705 -5.93662,-32.36785 -5.70535,-33.12085 0.46982,-1.52971 -2.90765,-0.68173 43.61462,-10.95031 39.06308,-8.62216 42.31772,-9.31356 42.92957,-9.11981 z m -44.99392,81.65344 c -0.0836,0.33062 0.0853,1.4912 0.52412,3.60358 0.62915,3.02821 0.66721,3.14025 1.35213,3.98133 0.45971,0.56453 1.00708,0.97169 1.57428,1.17105 l 0.86953,0.30562 12.18347,-2.69059 12.18348,-2.69059 0.69401,-0.6509 c 0.4527,-0.4246 0.81381,-1.03239 1.0386,-1.74808 0.33491,-1.0663 0.33048,-1.18576 -0.15748,-4.24516 -0.34038,-2.13413 -0.59408,-3.27596 -0.78774,-3.54536 l -0.28566,-0.39737 -14.53275,3.20939 -14.53274,3.2094 z" id="path30-3-6" inkscape:connector-curvature="0" /></g><path inkscape:connector-curvature="0" style="stroke-width:0.59603173" d="m 265.09734,75.506604 c -0.93957,-0.0021 -1.90039,0.11814 -2.79407,0.361668 l -2.86607,0.781744 -30.13889,28.333644 -30.13887,28.33365 -6.14066,-6.40369 c -4.13432,-4.31193 -7.13803,-7.02328 -9.19573,-8.29996 -5.56704,-3.45407 -13.05132,-4.07123 -19.07945,-1.57595 -3.20951,1.32855 -3.89625,1.93735 -33.28019,29.53775 -21.97798,20.64401 -30.39088,28.80811 -31.44896,30.52541 -3.191167,5.17936 -3.760537,11.58376 -1.592529,17.893 1.122706,3.26725 1.662089,3.9986 8.206729,11.11395 l 7.00454,7.61513 -29.920344,28.12857 c -20.090565,18.88762 -30.173498,28.65513 -30.692706,29.73121 -0.947235,1.9632 -1.168471,5.63599 -0.480659,7.97428 0.766486,2.60622 130.910459,138.34726 133.505719,139.24756 2.18269,0.75718 5.13555,0.76097 7.06484,0.0104 0.46136,-0.17947 9.29678,-8.30746 24.86221,-22.82663 -16.96584,8.832 -26.63281,13.78006 -27.0773,13.82559 -1.86919,0.19143 -4.44757,-0.52218 -6.16874,-1.70741 -2.04651,-1.40926 -82.47777,-151.14019 -82.50882,-153.59882 -0.0279,-2.2059 1.06598,-5.35656 2.37441,-6.84228 0.71718,-0.81436 11.91823,-6.91578 34.09565,-18.57207 l 33.02717,-17.36045 -4.25167,-8.3296 c -3.9738,-7.78206 -4.26586,-8.54706 -4.44613,-11.66816 -0.34811,-6.027 1.71912,-11.48004 5.77567,-15.23338 1.345,-1.24446 10.69329,-6.34859 34.94975,-19.08419 32.43019,-17.02711 33.17828,-17.39484 36.30743,-17.78313 5.87718,-0.7293 12.26464,1.60852 16.28148,5.96048 1.4847,1.60857 3.44398,4.69732 5.99909,9.4537 l 3.79572,7.06284 33.26891,-17.48476 24.0201,-12.62506 -53.05834,-55.32822 -2.59267,-0.793439 c -0.80834,-0.247279 -1.72705,-0.371345 -2.66662,-0.373417 z m -91.22706,50.933106 c 2.92742,0.12867 4.7412,1.70842 10.63413,7.85452 l 6.45606,6.73271 -34.52207,32.45428 -34.52207,32.45428 -6.57952,-6.86305 c -6.14118,-6.40505 -6.60799,-7.02132 -6.98268,-9.20888 -0.22082,-1.28929 -0.24477,-3.26754 -0.0524,-4.39562 0.34706,-2.03509 0.58604,-2.27201 30.45161,-30.34933 l 30.09883,-28.29761 2.74879,-0.27694 c 0.85778,-0.0865 1.59373,-0.13404 2.2693,-0.10436 z m 67.37397,22.721 -33.22733,17.46442 c -32.96778,17.32761 -33.23395,17.47699 -34.03572,19.16962 -0.44443,0.93827 -0.90781,2.67015 -1.03078,3.8484 -0.20865,1.99913 0.0474,2.6483 3.84279,9.71354 l 4.06716,7.5704 38.10744,-20.028 38.10749,-20.02994 -3.98904,-7.42565 c -4.48244,-8.34416 -5.2646,-9.16744 -9.37397,-9.86397 z m 127.59321,30.20278 14.05939,26.1673 12.63122,23.50639 6.63119,-6.24389 0.34888,-2.05182 c 0.19242,-1.12811 0.17039,-3.10431 -0.0505,-4.39362 l -0.40123,-2.34576 z" id="path38-0-6-7" /><path style="opacity:1;fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:2.51415491;stroke-opacity:1" d="m 153.37425,374.56221 a 29.487607,50.927267 50.206338 0 0 -14.95933,41.87304 29.487607,50.927267 50.206338 0 0 52.56843,1.68637 29.487607,50.927267 50.206338 0 0 10.27878,-7.82507 l -3.15503,-2.35607 a 20.198386,39.450089 64.736938 0 1 -4.26592,2.38712 20.198386,39.450089 64.736938 0 1 -45.89564,0.75892 20.198386,39.450089 64.736938 0 1 14.918,-29.44542 z" id="path1260" inkscape:connector-curvature="0" /></g></g></g></svg><p>{{pluckcom('Offer_menu_title',item.component)}}</p></a></li>
        
        <!--<li><a href="#"><img class="img-fluid" src="/Nirvana/images/icons/our-partners-icon.png"><p>our partners</p></a></li>-->
        <!--<li><a href="/Nirvana/awards.html"><svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46667" version="1.1" id="svg1699" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" sodipodi:docname="award_final.svg"><defs id="defs1693" /><sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="1.0117187" inkscape:cx="81.050181" inkscape:cy="256" inkscape:document-units="mm" inkscape:current-layer="g6121" showgrid="false" units="px" showguides="true" inkscape:guide-bbox="true" inkscape:window-width="1366" inkscape:window-height="705" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" inkscape:pagecheckerboard="true" /><metadata id="metadata1696"><rdf:RDF><cc:Work rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /><dc:title></dc:title></cc:Work></rdf:RDF></metadata><g inkscape:label="Border" inkscape:groupmode="layer" id="layer1" transform="translate(0,-161.53332)" /><g transform="translate(0,-161.53332)" id="g6020" inkscape:groupmode="layer" inkscape:label="Border copy" /><g inkscape:groupmode="layer" id="layer3" inkscape:label="Star" style="opacity:1" /><g inkscape:groupmode="layer" id="layer4" inkscape:label="Leaves" style="display:inline" /><g style="display:inline" inkscape:label="Leaves copy" id="g6121" inkscape:groupmode="layer"><g id="g6179" transform="matrix(1.0281447,0,0,1.0281447,-2.0333159,-1.7793499)"><path inkscape:connector-curvature="0" id="path2255" d="M 67.856841,163.26396 A 65.879318,65.879318 0 0 0 1.9776577,229.14314 65.879318,65.879318 0 0 0 67.856841,295.02233 65.879318,65.879318 0 0 0 133.73602,229.14314 65.879318,65.879318 0 0 0 67.856841,163.26396 Z m 0,5.06739 a 60.811679,60.811679 0 0 1 60.811789,60.81179 60.811679,60.811679 0 0 1 -60.811789,60.8118 60.811679,60.811679 0 0 1 -60.8117924,-60.8118 60.811679,60.811679 0 0 1 60.8117924,-60.81179 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" transform="translate(0,-161.53332)" /><path inkscape:connector-curvature="0" id="path6022" d="m 34.953471,193.26883 a 48.595592,48.595592 0 0 0 -17.500224,37.28661 48.595592,48.595592 0 0 0 34.568434,46.48296 h 0.10232 v -2.72955 A 45.7293,45.7293 0 0 1 20.31974,230.82623 45.7293,45.7293 0 0 1 36.712538,195.80666 v -2.53783 z m 60.471766,0 v 2.52956 a 45.7293,45.7293 0 0 1 16.353083,35.02784 45.7293,45.7293 0 0 1 -32.325756,43.70586 v 2.50631 h 0.730188 A 48.595592,48.595592 0 0 0 114.64431,230.55544 48.595592,48.595592 0 0 0 97.198257,193.26883 Z" style="opacity:1;fill-opacity:1;stroke:none;stroke-width:2.09375501;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" transform="translate(0,-161.53332)" /><path transform="matrix(0.71554811,0,0,0.69633923,19.302004,20.530454)" inkscape:transform-center-y="-3.2928064" inkscape:transform-center-x="-0.12318555" d="M 97.348917,114.11569 67.40613,97.955864 37.119028,113.46077 43.235105,79.989822 19.129838,55.976354 52.852568,51.449999 68.241796,21.103958 82.967513,51.77746 116.58385,57.036043 91.962109,80.519665 Z" inkscape:randomized="0" inkscape:rounded="0" inkscape:flatsided="false" sodipodi:arg2="1.5816696" sodipodi:arg1="0.9533511" sodipodi:r2="25.618816" sodipodi:r1="51.237633" sodipodi:cy="72.338562" sodipodi:cx="67.684685" sodipodi:sides="5" id="path5964" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" sodipodi:type="star" /><g id="g6099" style="display:inline;"><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 40.775554,114.54435 c -3.126113,-0.7203 -6.949163,-3.02955 -8.731844,-5.27433 -0.615899,-0.77556 -0.605299,-1.06131 0.0484,-1.30481 0.663058,-0.24699 2.62215,-0.58249 3.420294,-0.58575 1.768287,-0.007 5.819133,1.42775 9.24071,3.27342 0.898014,0.48441 1.650345,0.8564 1.671846,0.82665 0.0215,-0.0297 -0.267653,-0.88935 -0.642566,-1.91026 -1.012749,-2.75775 -1.259675,-3.86025 -1.267103,-5.65749 -0.0058,-1.40349 0.159517,-2.58017 0.362506,-2.58017 0.04888,0 0.368905,0.42345 0.711164,0.94098 1.498927,2.26656 2.752932,6.35801 2.87482,9.37971 0.05911,1.46535 -0.04999,1.87135 -0.601341,2.23779 -0.888213,0.59035 -5.511767,1.01718 -7.08689,0.65426 z" id="path860" inkscape:connector-curvature="0" /><path inkscape:connector-curvature="0" id="path5993" d="m 25.431516,102.87336 c -2.481155,-1.37502 -5.990743,-4.882146 -5.852599,-5.848481 0.06157,-0.430645 1.748012,-0.614613 3.368369,-0.36744 1.557452,0.237614 4.384334,1.682254 7.197231,3.678151 1.052911,0.74709 1.971103,1.31534 2.040427,1.26277 0.06932,-0.0526 -0.07155,-1.14174 -0.313066,-2.420391 -0.308772,-1.634756 -0.38769,-2.914827 -0.265874,-4.312561 0.24649,-2.82828 0.580965,-2.898379 1.385132,-0.290305 0.839014,2.721099 1.062967,4.841695 0.761463,7.210217 -0.239129,1.87854 -0.246544,1.89456 -0.990444,2.13814 -0.487628,0.15967 -1.690572,0.13473 -3.442855,-0.0714 -2.250204,-0.26467 -2.88986,-0.42574 -3.887784,-0.97873 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 17.913737,90.196178 c -1.958503,-1.909671 -4.307633,-6.001804 -3.917415,-6.824049 0.1739,-0.366432 1.806679,-0.06438 3.261177,0.603285 1.398018,0.641783 3.662312,2.708934 5.764851,5.263051 0.787016,0.956044 1.495778,1.715635 1.575029,1.687979 0.07925,-0.02765 0.240646,-1.036356 0.358669,-2.241551 0.150881,-1.540847 0.421913,-2.702399 0.913112,-3.913263 0.993927,-2.450159 1.326821,-2.420181 1.378584,0.124133 0.054,2.654576 -0.307488,4.604576 -1.229103,6.630182 -0.730958,1.606563 -0.742237,1.618773 -1.506257,1.630165 -0.500817,0.0075 -1.623377,-0.347023 -3.212794,-1.014565 -2.041059,-0.857229 -2.598119,-1.177339 -3.385846,-1.945367 z" id="path5995" inkscape:connector-curvature="0" /><path inkscape:connector-curvature="0" id="path5997" d="m 15.036926,78.373003 c -1.521321,-1.936373 -3.144744,-5.906349 -2.707223,-6.620343 0.19498,-0.31819 1.607447,0.116521 2.821143,0.868257 1.166565,0.722581 2.941137,2.832642 4.51802,5.372292 0.590253,0.95063 1.133147,1.714022 1.206436,1.696426 0.07329,-0.01759 0.328968,-0.924299 0.568181,-2.014896 0.305831,-1.394331 0.675703,-2.430161 1.246094,-3.489711 1.154171,-2.143975 1.445681,-2.084246 1.20719,0.247385 -0.248818,2.432634 -0.786923,4.180653 -1.829602,5.943432 -0.826977,1.398107 -0.838331,1.408176 -1.516329,1.34442 -0.444429,-0.0418 -1.399113,-0.474925 -2.732329,-1.239641 -1.712055,-0.982015 -2.169695,-1.32881 -2.781589,-2.107583 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 14.62871,65.942556 c -0.873514,-2.230168 -1.27155,-6.382869 -0.663767,-6.925053 0.270858,-0.241624 1.464316,0.559514 2.383712,1.600126 0.883684,1.000237 1.944318,3.463215 2.700317,6.270801 0.282983,1.050923 0.574394,1.914185 0.647583,1.918361 0.0732,0.0041 0.565695,-0.768497 1.09446,-1.717049 0.676026,-1.212726 1.311106,-2.073616 2.139584,-2.900337 1.676406,-1.672849 1.931128,-1.535436 1.054854,0.569066 -0.914243,2.195691 -1.90581,3.67264 -3.371438,5.021762 -1.162423,1.070028 -1.175823,1.076218 -1.789351,0.82662 -0.402167,-0.16362 -1.169738,-0.834843 -2.196801,-1.921078 -1.318907,-1.394894 -1.647804,-1.846262 -1.999159,-2.743208 z" id="path6008" inkscape:connector-curvature="0" /><path inkscape:connector-curvature="0" id="path6010" d="m 17.050108,53.532041 c -0.412313,-2.153907 -0.07426,-5.958607 0.563733,-6.344727 0.284322,-0.172075 1.22425,0.749173 1.87733,1.840021 0.627706,1.048517 1.169414,3.44336 1.379126,6.097311 0.0785,0.993415 0.196054,1.819325 0.261234,1.835355 0.06517,0.01608 0.63806,-0.596887 1.273071,-1.36204 0.811864,-0.978246 1.527868,-1.646668 2.41223,-2.2519 1.789485,-1.224674 1.995725,-1.058271 0.854066,0.689129 -1.191111,1.823096 -2.331306,2.986271 -3.876781,3.954892 -1.225752,0.768242 -1.238851,0.771569 -1.749251,0.444019 -0.334567,-0.214712 -0.912924,-0.947608 -1.655268,-2.097575 -0.953286,-1.476739 -1.173649,-1.938177 -1.339511,-2.804459 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 21.579037,44.015664 c 0.06035,-1.922575 1.079636,-5.114055 1.69976,-5.322121 0.276357,-0.09272 0.904068,0.876077 1.253735,1.935002 0.336071,1.017827 0.340237,3.171436 0.01055,5.483114 -0.123401,0.865297 -0.181257,1.594723 -0.128541,1.620949 0.05271,0.02623 0.660616,-0.388443 1.350903,-0.921485 0.882532,-0.681496 1.623585,-1.116206 2.496602,-1.464537 1.766532,-0.704841 1.911122,-0.522853 0.598757,0.753664 -1.369212,1.331816 -2.568237,2.108588 -4.0768,2.641097 -1.196481,0.422348 -1.20833,0.422682 -1.582325,0.04442 -0.245152,-0.247954 -0.59954,-0.986181 -1.014263,-2.112831 -0.532569,-1.446794 -0.632641,-1.884004 -0.608396,-2.657251 z" id="path6012" inkscape:connector-curvature="0" /><path inkscape:connector-curvature="0" id="path6014" d="m 28.545103,34.63887 c 0.571825,-1.84726 2.319783,-4.665485 2.914898,-4.699679 0.265212,-0.01524 0.548995,1.093358 0.566803,2.214217 0.01709,1.077351 -0.561077,3.165979 -1.472079,5.317656 -0.341002,0.805406 -0.588324,1.49682 -0.549602,1.536476 0.03872,0.03965 0.679032,-0.198099 1.422913,-0.528345 0.951053,-0.422222 1.712487,-0.643441 2.565268,-0.745293 1.725583,-0.206095 1.802073,0.0094 0.3168,0.892215 -1.549674,0.921135 -2.801507,1.350231 -4.256353,1.458955 -1.153875,0.08624 -1.164263,0.08336 -1.387091,-0.384303 -0.146063,-0.306555 -0.254606,-1.117836 -0.310654,-2.321912 -0.07197,-1.546222 -0.04083,-1.997039 0.189131,-2.740004 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 34.940405,31.888019 c 1.424889,-1.307304 4.35497,-2.860485 4.88621,-2.590088 0.236748,0.120501 -0.07687,1.221032 -0.626385,2.198105 -0.528211,0.939133 -2.080224,2.451713 -3.951486,3.851005 -0.700442,0.523777 -1.262521,0.996311 -1.249063,1.050078 l -1.785018,0.403336 c 0.02834,-0.338389 0.623065,-1.572962 1.181496,-2.641182 0.717116,-1.371761 0.971218,-1.745441 1.544284,-2.271251 z" id="path6024" inkscape:connector-curvature="0" sodipodi:nodetypes="cccsccccc" /></g><g transform="translate(-0.52916663,2.1166667)" id="g6141"><path inkscape:connector-curvature="0" id="path6101" d="m 91.728036,112.33907 c 3.126113,-0.7203 6.949163,-3.02955 8.731844,-5.27433 0.6159,-0.77556 0.6053,-1.06131 -0.0484,-1.30481 -0.663058,-0.24699 -2.62215,-0.58249 -3.420294,-0.58575 -1.768287,-0.007 -5.819133,1.42775 -9.24071,3.27342 -0.898014,0.48441 -1.650345,0.8564 -1.671846,0.82665 -0.0215,-0.0297 0.267653,-0.88935 0.642566,-1.91026 1.012749,-2.75775 1.259675,-3.86025 1.267103,-5.65749 0.0058,-1.40349 -0.159517,-2.580167 -0.362506,-2.580167 -0.04888,0 -0.368905,0.42345 -0.711164,0.940977 -1.498927,2.26656 -2.752932,6.35801 -2.87482,9.37971 -0.05911,1.46535 0.04999,1.87135 0.601341,2.23779 0.888213,0.59035 5.511767,1.01718 7.08689,0.65426 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 107.07207,100.66808 c 2.48116,-1.375017 5.99075,-4.882143 5.8526,-5.848478 -0.0616,-0.430645 -1.74801,-0.614613 -3.36837,-0.36744 -1.55745,0.237614 -4.38433,1.682254 -7.19723,3.678151 -1.05291,0.74709 -1.9711,1.31534 -2.04042,1.26277 -0.0693,-0.0526 0.0715,-1.14174 0.31306,-2.420391 0.30877,-1.634756 0.38769,-2.914827 0.26588,-4.312561 -0.24649,-2.82828 -0.58097,-2.898379 -1.385136,-0.290305 -0.839014,2.721099 -1.062967,4.841695 -0.761463,7.210217 0.239129,1.878537 0.246544,1.894557 0.990444,2.138137 0.487625,0.15967 1.690575,0.13473 3.442855,-0.0714 2.2502,-0.26467 2.88986,-0.42574 3.88778,-0.97873 z" id="path6103" inkscape:connector-curvature="0" /><path inkscape:connector-curvature="0" id="path6105" d="m 114.58985,87.990901 c 1.95851,-1.909671 4.30764,-6.001804 3.91742,-6.824049 -0.1739,-0.366432 -1.80668,-0.06438 -3.26118,0.603285 -1.39802,0.641783 -3.66231,2.708934 -5.76485,5.263051 -0.78702,0.956044 -1.49578,1.715635 -1.57503,1.687979 -0.0793,-0.02765 -0.24064,-1.036356 -0.35867,-2.241551 -0.15088,-1.540847 -0.42191,-2.702399 -0.91311,-3.913263 -0.99393,-2.450159 -1.32682,-2.420181 -1.37858,0.124133 -0.054,2.654576 0.30748,4.604576 1.2291,6.630182 0.73096,1.606563 0.74224,1.618773 1.50626,1.630165 0.50081,0.0075 1.62337,-0.347023 3.21279,-1.014565 2.04106,-0.857229 2.59812,-1.177339 3.38585,-1.945367 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 117.46666,76.167726 c 1.52133,-1.936373 3.14475,-5.906349 2.70723,-6.620343 -0.19498,-0.31819 -1.60745,0.116521 -2.82115,0.868257 -1.16656,0.722581 -2.94113,2.832642 -4.51802,5.372292 -0.59025,0.95063 -1.13314,1.714022 -1.20643,1.696426 -0.0733,-0.01759 -0.32897,-0.924299 -0.56818,-2.014896 -0.30583,-1.394331 -0.67571,-2.430161 -1.2461,-3.489711 -1.15417,-2.143975 -1.44568,-2.084246 -1.20719,0.247385 0.24882,2.432634 0.78693,4.180653 1.82961,5.943432 0.82697,1.398107 0.83833,1.408176 1.51632,1.34442 0.44443,-0.0418 1.39912,-0.474925 2.73233,-1.239641 1.71206,-0.982015 2.1697,-1.32881 2.78159,-2.107583 z" id="path6107" inkscape:connector-curvature="0" /><path inkscape:connector-curvature="0" id="path6109" d="m 117.87488,63.737279 c 0.87351,-2.230168 1.27155,-6.382869 0.66377,-6.925053 -0.27086,-0.241624 -1.46432,0.559514 -2.38372,1.600126 -0.88368,1.000237 -1.94431,3.463215 -2.70031,6.270801 -0.28298,1.050923 -0.5744,1.914185 -0.64758,1.918361 -0.0732,0.0041 -0.5657,-0.768497 -1.09446,-1.717049 -0.67603,-1.212726 -1.31111,-2.073616 -2.13959,-2.900337 -1.6764,-1.672849 -1.93113,-1.535436 -1.05485,0.569066 0.91424,2.195691 1.90581,3.67264 3.37144,5.021762 1.16242,1.070028 1.17582,1.076218 1.78935,0.82662 0.40216,-0.16362 1.16973,-0.834843 2.1968,-1.921078 1.3189,-1.394894 1.6478,-1.846262 1.99916,-2.743208 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 115.45348,51.326764 c 0.41232,-2.153907 0.0743,-5.958607 -0.56373,-6.344727 -0.28432,-0.172075 -1.22425,0.749173 -1.87733,1.840021 -0.62771,1.048517 -1.16941,3.44336 -1.37913,6.097311 -0.0785,0.993415 -0.19605,1.819325 -0.26123,1.835355 -0.0652,0.01608 -0.63806,-0.596887 -1.27307,-1.36204 -0.81187,-0.978246 -1.52787,-1.646668 -2.41223,-2.2519 -1.78949,-1.224674 -1.99573,-1.058271 -0.85407,0.689129 1.19111,1.823096 2.33131,2.986271 3.87678,3.954892 1.22576,0.768242 1.23885,0.771569 1.74925,0.444019 0.33457,-0.214712 0.91293,-0.947608 1.65527,-2.097575 0.95329,-1.476739 1.17365,-1.938177 1.33951,-2.804459 z" id="path6111" inkscape:connector-curvature="0" /><path inkscape:connector-curvature="0" id="path6113" d="m 110.92455,41.810387 c -0.0603,-1.922575 -1.07963,-5.114055 -1.69976,-5.322121 -0.27635,-0.09272 -0.90406,0.876077 -1.25373,1.935002 -0.33607,1.017827 -0.34024,3.171436 -0.0105,5.483114 0.1234,0.865297 0.18126,1.594723 0.12854,1.620949 -0.0527,0.02623 -0.66062,-0.388443 -1.3509,-0.921485 -0.88254,-0.681496 -1.62359,-1.116206 -2.49661,-1.464537 -1.76653,-0.704841 -1.91112,-0.522853 -0.59875,0.753664 1.36921,1.331816 2.56823,2.108588 4.0768,2.641097 1.19648,0.422348 1.20833,0.422682 1.58232,0.04442 0.24515,-0.247954 0.59954,-0.986181 1.01427,-2.112831 0.53256,-1.446794 0.63264,-1.884004 0.60839,-2.657251 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 103.95849,32.433593 c -0.57183,-1.84726 -2.31979,-4.665485 -2.9149,-4.699679 -0.26521,-0.01524 -0.549,1.093358 -0.5668,2.214217 -0.0171,1.077351 0.56107,3.165979 1.47208,5.317656 0.341,0.805406 0.58832,1.49682 0.5496,1.536476 -0.0387,0.03965 -0.67903,-0.198099 -1.42292,-0.528345 -0.95105,-0.422222 -1.712483,-0.643441 -2.565264,-0.745293 -1.725583,-0.206095 -1.802073,0.0094 -0.3168,0.892215 1.549674,0.921135 2.801504,1.350231 4.256354,1.458955 1.15387,0.08624 1.16426,0.08336 1.38709,-0.384303 0.14606,-0.306555 0.25461,-1.117836 0.31065,-2.321912 0.072,-1.546222 0.0408,-1.997039 -0.18913,-2.740004 z" id="path6115" inkscape:connector-curvature="0" /><path sodipodi:nodetypes="cccsccccc" inkscape:connector-curvature="0" id="path6117" d="m 97.563185,29.682742 c -1.424889,-1.307304 -4.35497,-2.860485 -4.88621,-2.590088 -0.236748,0.120501 0.07687,1.221032 0.626385,2.198105 0.528211,0.939133 2.080224,2.451713 3.951486,3.851005 0.700442,0.523777 1.262521,0.996311 1.249063,1.050078 l 1.785021,0.403336 c -0.0283,-0.338389 -0.623068,-1.572962 -1.181499,-2.641182 -0.717116,-1.371761 -0.971218,-1.745441 -1.544284,-2.271251 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /></g></g></g></svg><p>{{pluckcom('Awards_menu_title',item.component)}}</p></a></li>-->
        <li><a href="/Nirvana/gallery-list.html"><svg xmlns:osb="http://www.openswatchbook.org/uri/2009/osb" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46667" version="1.1" id="svg8" inkscape:version="0.92.3 (2405546, 2018-03-11)"> <defs id="defs2"> <linearGradient id="linearGradient6884" osb:paint="solid"> <stop style="stop-color:#f10000;stop-opacity:1;" offset="0" id="stop6882" /> </linearGradient> <linearGradient id="linearGradient6864" osb:paint="solid"> <stop style="stop-color:#e50000;stop-opacity:1;" offset="0" id="stop6862" /> </linearGradient> </defs> <sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.49497475" inkscape:cx="399.62837" inkscape:cy="-20.003769" inkscape:document-units="mm" inkscape:current-layer="g843" showgrid="false" units="px" showguides="true" inkscape:guide-bbox="true" inkscape:window-width="1920" inkscape:window-height="1017" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1"> <sodipodi:guide position="78.241068,130.11831" orientation="0,1" id="guide845" inkscape:locked="false" /> <sodipodi:guide position="5.4806545,62.838543" orientation="1,0" id="guide847" inkscape:locked="false" /> <sodipodi:guide position="70.681547,5.3861607" orientation="0,1" id="guide849" inkscape:locked="false" /> <sodipodi:guide position="130.54353,67.019903" orientation="1,0" id="guide851" inkscape:locked="false" /> </sodipodi:namedview> <metadata id="metadata5"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(0,-161.53332)"> <g style="enable-background:new" id="g843" transform="translate(0.42039757,160.49011)"> <path style="opacity:1;fill-opacity:1;stroke:none;stroke-width:0.9653374;stroke-opacity:1" d="M 256.31055 0.01171875 A 255.80007 255.99442 0 0 0 0.51171875 256.00586 A 255.80007 255.99442 0 0 0 256.31055 512 A 255.80007 255.99442 0 0 0 512.11133 256.00586 A 255.80007 255.99442 0 0 0 256.31055 0.01171875 z M 257.05273 20.214844 A 236.33929 236.33929 0 0 1 493.39258 256.55273 A 236.33929 236.33929 0 0 1 257.05273 492.89258 A 236.33929 236.33929 0 0 1 20.714844 256.55273 A 236.33929 236.33929 0 0 1 257.05273 20.214844 z " transform="matrix(0.26458333,0,0,0.26458333,-0.42039757,1.04321)" id="path857" /> <path style="opacity:1;fill-rule:evenodd;stroke-width:0.31607604" d="m 29.191599,33.484221 c -6.119621,0 -11.046038,4.8107 -11.046038,10.786747 v 48.773263 c 0,5.976047 4.926417,10.787379 11.046038,10.787379 h 74.946281 c 6.11962,0 11.04666,-4.811332 11.04666,-10.787379 V 44.270968 c 0,-5.976047 -4.92704,-10.786747 -11.04666,-10.786747 z m -2.081927,7.003451 h 78.628498 c 1.15297,0 2.08131,0.947134 2.08131,2.124234 V 94.81769 c 0,1.177098 -0.92834,2.124862 -2.08131,2.124862 H 27.109672 c -1.152969,0 -2.08131,-0.947764 -2.08131,-2.124862 V 42.611906 c 0,-1.1771 0.928341,-2.124234 2.08131,-2.124234 z" id="rect871" inkscape:connector-curvature="0" /> <path style="opacity:1;fill-opacity:1;stroke:none;stroke-width:0.32026237;stroke-opacity:1" d="m 56.01806,48.103691 a 5.7632579,5.883865 0 0 0 -5.763528,5.884139 5.7632579,5.883865 0 0 0 5.763528,5.883507 5.7632579,5.883865 0 0 0 5.763527,-5.883507 5.7632579,5.883865 0 0 0 -5.763527,-5.884139 z m 18.665533,8.246011 c -1.000316,0.02917 -1.61577,1.070016 -1.61577,1.070016 L 61.060991,78.17727 56.148063,70.158154 55.618142,69.351061 c 0,0 -0.660579,-1.072696 -1.5811,-1.246352 -0.920521,-0.173656 -1.508051,0.73062 -1.508051,0.73062 l -8.065843,14.11372 c 0,0 -0.820788,1.791244 -0.608544,2.614682 0.212254,0.823436 1.197277,1.030198 1.197277,1.030198 0,0 44.625433,0.02043 45.005659,0 0.38021,-0.02044 1.090604,-0.183819 1.540859,-1.011238 0.450258,-0.827418 -0.241434,-2.128024 -0.241434,-2.128024 L 76.629946,57.664943 c 0,0 -0.620801,-1.184446 -1.741442,-1.307024 -0.07003,-0.0077 -0.138222,-0.01015 -0.204911,-0.0082 z" id="path904" inkscape:connector-curvature="0" /> </g> </g> </svg><p>{{pluckcom('Gallery_menu',item.component)}}</p></a></li>
        <li><a href="/Nirvana/blog.html"><svg xmlns:osb="http://www.openswatchbook.org/uri/2009/osb" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46667" version="1.1" id="svg8" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" sodipodi:docname="blog.svg"> <defs id="defs2"> <linearGradient osb:paint="solid" id="linearGradient6884"> <stop id="stop6882" offset="0" style="stop-color:#f10000;stop-opacity:1;" /> </linearGradient> <linearGradient osb:paint="solid" id="linearGradient6864"> <stop id="stop6862" offset="0" style="stop-color:#e50000;stop-opacity:1;" /> </linearGradient> </defs> <sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.9609375" inkscape:cx="256" inkscape:cy="256" inkscape:document-units="mm" inkscape:current-layer="layer2" showgrid="false" showguides="false" inkscape:guide-bbox="true" units="px" inkscape:lockguides="true" inkscape:window-width="1366" inkscape:window-height="705" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" inkscape:snap-others="false" inkscape:object-nodes="false" inkscape:snap-nodes="false" inkscape:snap-global="false" inkscape:pagecheckerboard="true"> <sodipodi:guide position="-222.70963,215.23058" orientation="1,0" id="guide3731" inkscape:locked="true" /> </sodipodi:namedview> <metadata id="metadata5"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(0,-161.53332)" style="display:inline" /> <g inkscape:groupmode="layer" id="layer2" inkscape:label="s" style="display:inline;opacity:1"> <g id="g4073" transform="matrix(1.0428068,0,0,1.077978,-10.398381,6.8098933)"> <path d="m -76.706692,49.596344 a 14.36668,13.748156 0 0 1 -7.18334,11.906252 14.36668,13.748156 0 0 1 -14.36668,0 14.36668,13.748156 0 0 1 -7.183338,-11.906253 l 14.366678,1e-6 z" sodipodi:end="3.1415927" sodipodi:start="0" sodipodi:ry="13.748156" sodipodi:rx="14.36668" sodipodi:cy="49.596344" sodipodi:cx="-91.073372" sodipodi:type="arc" id="path3723" style="display:inline;stroke-width:0.10764731" transform="matrix(-0.72231588,-0.69156328,0.72706926,-0.68656412,0,0)" /> <path inkscape:connector-curvature="0" id="path2255" d="M 69.791146,-6.0890897 A 62.815645,64.963908 85.312558 0 0 10.177947,61.825175 62.815645,64.963908 85.312558 0 0 80.057737,119.12174 62.815645,64.963908 85.312558 0 0 139.67093,51.207474 62.815645,64.963908 85.312558 0 0 69.791146,-6.0890897 Z m 0.394851,4.8155735 A 57.983673,59.966685 85.312558 0 1 134.69066,51.615828 57.983673,59.966685 85.312558 0 1 79.662886,114.30616 57.983673,59.966685 85.312558 0 1 15.158211,61.416821 57.983673,59.966685 85.312558 0 1 70.185997,-1.2735162 Z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /> <path id="path3713" transform="scale(0.26458333)" d="m 339.53516,77.193359 -157.2168,171.673831 -0.002,-0.002 -0.002,0.006 -1.70507,1.86133 0.73437,0.70703 -23.10937,61.16602 -18.80079,50.88476 70.08789,-20.88867 50.53711,-15.41406 0.70313,0.67578 1.13086,-1.23437 3.42383,-1.04493 -1.33203,-1.24023 155.70312,-170.02148 z m 23.10546,43.244141 16.44727,15.57617 -146.97656,153.83399 -16.44727,-15.57422 z" style="stroke-width:0.4254483" inkscape:connector-curvature="0" /> </g> </g> </svg><p>{{pluckcom('Blog_MenuTitle',item.component)}}</p></a></li>
        <li><a href="/Nirvana/csr.html"><svg xmlns:osb="http://www.openswatchbook.org/uri/2009/osb" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46667" version="1.1" id="svg1000" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" sodipodi:docname="csr-final.svg"> <defs id="defs994"> <inkscape:perspective id="perspective23" inkscape:persp3d-origin="67.73333 : 45.155557 : 1" inkscape:vp_z="135.46666 : 67.733335 : 1" inkscape:vp_y="0 : 1000 : 0" inkscape:vp_x="0 : 67.733335 : 1" sodipodi:type="inkscape:persp3d" /> <linearGradient id="linearGradient6884" osb:paint="solid"> <stop style="stop-color:#f10000;stop-opacity:1;" offset="0" id="stop6882" /> </linearGradient> <linearGradient id="linearGradient6864" osb:paint="solid"> <stop style="stop-color:#e50000;stop-opacity:1;" offset="0" id="stop6862" /> </linearGradient> </defs> <sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.9296875" inkscape:cx="256" inkscape:cy="256" inkscape:document-units="mm" inkscape:current-layer="layer1" showgrid="false" units="px" showguides="true" inkscape:guide-bbox="true" inkscape:pagecheckerboard="true" inkscape:window-width="1366" inkscape:window-height="705" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" /> <metadata id="metadata997"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(0,-161.53332)"> <g inkscape:label="icons" id="layer1-9" transform="translate(-217.76658,-139.42923)" /> <g id="layer3" inkscape:label="image" transform="translate(-217.76658,22.104086)" /> <g id="layer2" inkscape:label="main" transform="translate(-217.76658,22.104086)" /> <g id="g1662"> <path style="opacity:1;stroke-width:0.77719426" d="m 48.862077,195.1728 c -5.295233,0.0211 -9.570284,4.51348 -9.548716,10.03523 0.02118,5.5213 4.330196,9.98023 9.624945,9.95999 5.294233,-0.0211 9.568043,-4.51141 9.547172,-10.03215 -0.02118,-5.52121 -4.328683,-9.98249 -9.623401,-9.96307 z m 38.680302,-0.14547 c -5.295234,0.0211 -9.570285,4.51348 -9.548716,10.03523 0.02118,5.52129 4.330195,9.98022 9.624949,9.95996 5.294741,-0.0211 9.569576,-4.51082 9.548736,-10.03212 -0.02119,-5.52179 -4.329733,-9.98327 -9.624969,-9.96307 z M 28.714698,208.40874 A 13.241498,12.698227 89.749623 0 0 16.067685,221.69712 13.241498,12.698227 89.749623 0 0 28.81567,234.89147 13.241498,12.698227 89.749623 0 0 41.462714,221.6015 13.241498,12.698227 89.749623 0 0 28.714698,208.40874 Z m 39.350894,-0.14816 A 13.241498,12.698227 89.749623 0 0 55.418578,221.54894 13.241498,12.698227 89.749623 0 0 68.166565,234.74326 13.241498,12.698227 89.749623 0 0 80.815151,221.4533 13.241498,12.698227 89.749623 0 0 68.065592,208.26058 Z m 39.352468,-0.14818 a 13.241498,12.698227 89.749623 0 0 -12.647043,13.28836 13.241498,12.698227 89.749623 0 0 12.748023,13.19436 13.241498,12.698227 89.749623 0 0 12.64703,-13.28995 13.241498,12.698227 89.749623 0 0 -12.74801,-13.19277 z m -65.626901,6.97538 c -0.126146,0.23052 -0.250463,0.46742 -0.374032,0.70961 1.303496,1.75172 2.015078,3.90287 2.025786,6.11963 0.01512,4.57456 -1.412877,6.01317 -4.216186,8.12967 l 1.928898,2.03048 c 4.564444,7.75742 7.472173,22.13715 7.660201,37.88395 l 0.232919,-2.7e-4 c 0.06987,-15.89573 2.9214,-30.40636 7.493831,-38.13982 l 1.199832,-1.82015 c -2.087674,-1.44101 -3.67388,-3.36068 -3.693481,-8.12316 -0.0091,-2.37466 0.786659,-4.6754 2.248086,-6.49604 -0.02722,-0.0502 -0.05535,-0.10181 -0.08349,-0.15208 -1.993539,1.63722 -4.496051,2.53981 -7.082412,2.55214 -2.692901,0.009 -5.298803,-0.94835 -7.340015,-2.69351 z m 38.6803,-0.14577 c -0.09861,0.18123 -0.196316,0.36572 -0.293415,0.55403 1.374671,1.77806 2.12951,3.99104 2.138886,6.27453 0.01213,4.65345 -1.708047,6.70852 -3.557632,8.64898 l 1.268801,1.51112 c 4.564444,7.75745 7.472175,22.13715 7.660201,37.88397 l 0.234437,-2.5e-4 c 0.06982,-15.89572 2.921395,-30.40637 7.493826,-38.13979 l 1.018002,-1.95153 c -2.517095,-2.44368 -3.458292,-6.14208 -3.51323,-7.99182 -0.0066,-2.309 0.749041,-4.54985 2.139256,-6.3465 -0.0553,-0.10129 -0.111262,-0.20196 -0.166974,-0.30085 -1.993536,1.63718 -4.496079,2.53977 -7.082409,2.55212 -2.692905,0.009 -5.298809,-0.94833 -7.34002,-2.69352 z m -61.12123,19.84191 a 59.033696,19.540911 89.781196 0 0 -5.879979,15.35656 57.251259,58.197098 89.868059 0 0 35.047865,34.39037 59.033696,19.540911 89.781196 0 0 -10.06513,-49.55791 14.835553,14.740534 0 0 1 -9.380258,3.37928 14.835553,14.740534 0 0 1 -9.722498,-3.5683 z m 39.350894,-0.14817 a 59.033696,19.540911 89.781196 0 0 -9.868388,50.00726 57.251259,58.197098 89.868059 0 0 19.536739,3.31853 57.251259,58.197098 89.868059 0 0 19.503135,-3.44996 59.033696,19.540911 89.781196 0 0 -10.06873,-49.68677 14.835553,14.740534 0 0 1 -9.380258,3.37923 14.835553,14.740534 0 0 1 -9.722498,-3.56829 z m 39.352471,-0.14817 a 59.033696,19.540911 89.781196 0 0 -9.867147,49.92025 57.251259,58.197098 89.868059 0 0 34.778243,-34.6718 59.033696,19.540911 89.781196 0 0 -5.80834,-15.05941 14.835553,14.740534 0 0 1 -9.38184,3.37929 14.835553,14.740534 0 0 1 -9.720916,-3.56833 z" id="path4599-1-3-2" inkscape:connector-curvature="0" /> <g inkscape:label="Layer 1" id="layer1-1"> <g transform="matrix(1.0281448,0,0,1.0281447,-1.4400028,-8.4734356)" id="layer1-4" inkscape:label="Layer 1"> <path inkscape:connector-curvature="0" id="path2255" d="M 67.27977,165.35295 A 65.879319,65.879319 0 0 0 1.4005815,231.23213 65.879319,65.879319 0 0 0 67.27977,297.11131 65.879319,65.879319 0 0 0 133.15894,231.23213 65.879319,65.879319 0 0 0 67.27977,165.35295 Z m 0,5.06739 A 60.81168,60.81168 0 0 1 128.09155,231.23213 60.81168,60.81168 0 0 1 67.27977,292.04392 60.81168,60.81168 0 0 1 6.4679725,231.23213 60.81168,60.81168 0 0 1 67.27977,170.42034 Z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /> </g> </g> </g> </g> <g inkscape:groupmode="layer" id="layer4" inkscape:label="Layer 2" /> </svg><p>{{pluckcom('Community_Services_Title',item.component)}}</p></a></li>
        <li><a href="/Nirvana/contactus.html"><svg width="512" height="512" version="1.1" viewBox="0 0 135.47 135.47" xmlns="http://www.w3.org/2000/svg" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
        <defs>
        <clipPath id="a">
        <ellipse cx="200.24" cy="211.56" rx="125.59" ry="127.8" fill="#999" fill-opacity=".81768" fill-rule="evenodd"/>
        </clipPath>
        </defs>
        <metadata>
        <rdf:RDF>
        <cc:Work rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"/>
        <dc:title/>
        </cc:Work>
        </rdf:RDF>
        </metadata>
        <g>
        <g transform="matrix(1.0281 0 0 1.0281 -1.44 -170.01)">
        <path d="m67.28 165.35a65.879 65.879 0 0 0-65.879 65.879 65.879 65.879 0 0 0 65.879 65.879 65.879 65.879 0 0 0 65.879-65.879 65.879 65.879 0 0 0-65.879-65.879zm0 5.0674a60.812 60.812 0 0 1 60.812 60.812 60.812 60.812 0 0 1-60.812 60.812 60.812 60.812 0 0 1-60.812-60.812 60.812 60.812 0 0 1 60.812-60.812z" stroke-width="0"/>
        </g>
        <g transform="matrix(.50393 0 0 .50393 -33.173 -38.879)">
        <path d="m208.32 33.498c-21.496-0.04264-37.986 2.6379-58.721 9.5469-24.795 8.2621-47.022 21.714-67.01 40.555l-3.0098 2.8379 1.0898 1.1855 1.0898 1.1875-1.1719-1.0762-1.1719-1.0742-2.8574 2.9941c-3.0977 3.2481-6.0323 6.5798-8.8438 9.9785 0.97424-0.42199 1.9618-0.64833 2.9199-0.65039 4.3429-0.0093 8.0553 4.2979 6.9238 12.949-1.0005 7.6494-0.58972 5.9225-1.8906 11.68 4.7663-6.8644 10.623-14.067 15.779-19.326 1.7882-1.8239 2.6272-2.9041 2.4629-3.1699-0.16927-0.27389-0.11959-0.32356 0.1543-0.15429 0.26575 0.16424 1.3466-0.67218 3.1699-2.4551 5.0794-4.9669 9.5784-8.7059 16.811-13.969 23.51-17.108 51.285-27.32 80.994-29.777 4.5181-0.37373 9.0397-0.55401 13.549-0.54297 31.564 0.07726 62.603 9.5028 88.852 27.166 9.0359 6.0803 15.245 11.161 23.318 19.084 23.755 23.315 39.338 52.624 45.441 85.465 2.1565 11.604 3.0646 25.2 2.4473 36.639-1.0088 18.693-4.4718 34.854-11.109 51.84-2.134 5.4612-7.8892 17.179-11.002 22.4-14.006 23.492-33.283 42.77-56.775 56.775-4.9875 2.9734-16.868 8.8464-21.76 10.758-17.421 6.8063-33.274 10.262-52 11.336-22.134 1.2692-46.041-2.6634-67.359-11.082-4.8927-1.9321-16.231-7.4084-20.801-10.047-5.6114-3.2402-13.845-8.7406-17.76-11.863-27.177-21.677-44.898-46.806-55.143-78.197-6.4581-19.788-8.9757-41.501-7.2324-62.398 1.8387-22.042 7.664-42.316 17.67-61.416-0.52348 0.70003-1.0553 1.3933-1.6035 2.0742-0.37536 0.46625-1.1175 0.57599-1.4277 1.0879-1.0949 1.8065-1.8036 3.8201-2.7051 5.7305-2.5063 3.9836-5.0132 7.9676-7.5195 11.951-1.9117 3.1315-3.7842 6.2868-5.7344 9.3945-1.0009 1.5951-1.8937 3.2814-3.1172 4.7129-0.09191 0.10754-0.18403 0.24421-0.27539 0.36328-1.3264 6.995-4.3661 12.161-9.0762 18.504-1.0656 1.435-2.3575 2.6866-3.5078 4.0547-0.18119 0.21549-0.48768 0.97485-0.48242 0.69336 0.06029-3.2291 0.43176-6.4456 0.64844-9.668-0.76193-0.44585-1.5235-0.8919-2.2852-1.3379-0.48863 3.3-0.89589 6.6496-1.2012 10.037 2.1618 3.8953 3.1147 6.8377-0.42774 5.9062-0.72337 12.443-0.23409 25.157 1.5176 37.111 1.5702 10.716 3.9218 20.493 7.541 31.359 15.151 45.488 47.44 82.818 90.521 104.65 22.237 11.269 46.478 17.741 71.84 19.182 6.4933 0.36877 19.203 0.13367 26.24-0.48633 14.342-1.2637 26.853-3.8915 41.279-8.6699 15.367-5.0902 29.129-11.778 42.561-20.682 46.79-31.015 76.414-81.246 81.006-137.35 0.51036-6.2278 0.51244-21.589 4e-3 -28.32-3.0972-40.987-20.559-80.448-48.852-110.4-4.2803-4.5315-4.6657-4.9099-9.9336-9.7656-19.429-17.908-44.843-32.365-69.824-39.719-18.261-5.3751-33.24-7.5502-52.24-7.5879zm84.16 30.193c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.55859 0.49214 0.55859 0.55859 0 0.26337-0.26164 0.09554-0.67968-0.4375-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm2.5586 2.5586c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.56055 0.4941 0.56055 0.56055 0 0.26337-0.2636 0.09359-0.68164-0.43945-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm2.5606 2.5605c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.56055 0.4941 0.56055 0.56055 0 0.26337-0.2636 0.09359-0.68164-0.43945-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm2.5606 2.5605c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.55859 0.4941 0.55859 0.56055 0 0.26337-0.26165 0.09359-0.67969-0.43945-0.19902-0.25377-0.29581-0.38387-0.27929-0.40039zm2.5605 2.5605c0.0165-0.01652 0.14467 0.08028 0.39844 0.2793 0.308 0.24155 0.56055 0.49214 0.56055 0.55859 0 0.26337-0.26165 0.09359-0.67969-0.43945-0.19902-0.25377-0.29581-0.38192-0.2793-0.39844zm2.5586 2.5586c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.56054 0.49415 0.56054 0.56055 0 0.26338-0.2636 0.09359-0.68164-0.43945-0.19902-0.25377-0.29581-0.38387-0.27929-0.40039zm2.5605 2.5605c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.56055 0.49405 0.56055 0.56055 0 0.26338-0.2636 0.09359-0.68164-0.43945-0.19902-0.25377-0.29581-0.38387-0.2793-0.40039zm2.5606 2.5605c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.55859 0.49209 0.5586 0.55859 0 0.26338-0.26165 0.09554-0.67969-0.4375-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm2.5606 2.5586c0.0165-0.01652 0.14466 0.08028 0.39844 0.2793 0.30799 0.24155 0.56054 0.49405 0.56054 0.56055 0 0.26338-0.2636 0.09359-0.68164-0.43945-0.19902-0.25377-0.29386-0.38387-0.27734-0.40039zm2.5586 2.5605c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.56055 0.49405 0.56055 0.56055 0 0.26338-0.2636 0.09359-0.68164-0.43945-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm21.121 0.64062c0.0165 0.01652-0.0803 0.14662-0.27929 0.40039-0.41805 0.53308-0.68164 0.70283-0.68164 0.43945 0-0.06645 0.25254-0.319 0.56054-0.56055 0.25378-0.19902 0.38388-0.29582 0.40039-0.2793zm-254.71 1.5996c0.088 0 1.8837 1.7278 3.9902 3.8398s3.7579 3.8398 3.6699 3.8398-1.8837-1.7278-3.9902-3.8398-3.7579-3.8398-3.6699-3.8398zm236.15 0.32031c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.55859 0.49405 0.55859 0.56055 0 0.26338-0.26165 0.09359-0.67968-0.43945-0.19903-0.25377-0.29582-0.38387-0.2793-0.40039zm16.641 0c0.0165 0.01652-0.0803 0.14662-0.27929 0.40039-0.41805 0.53308-0.68164 0.70283-0.68164 0.43945 0-0.06645 0.25254-0.319 0.56054-0.56055 0.25378-0.19902 0.38388-0.29581 0.40039-0.2793zm-14.08 2.5605c0.0165-0.01652 0.14467 0.08028 0.39844 0.2793 0.308 0.24155 0.56055 0.49219 0.56055 0.55859 0 0.26338-0.26164 0.09359-0.67969-0.43945-0.19901-0.25377-0.29581-0.38192-0.2793-0.39844zm11.52 0c0.0165 0.01652-0.0803 0.14467-0.2793 0.39844-0.41805 0.53308-0.68164 0.70283-0.68164 0.43945 0-0.06645 0.25255-0.31704 0.56055-0.55859 0.25377-0.19902 0.38387-0.29581 0.40039-0.2793zm-8.9609 2.5586c0.0165-0.01652 0.14661 0.08028 0.40039 0.2793 0.308 0.24155 0.56055 0.49415 0.56055 0.56055 0 0.26338-0.26359 0.09359-0.68164-0.43945-0.19901-0.25377-0.29582-0.38387-0.2793-0.40039zm6.4004 0c0.0165 0.01652-0.0803 0.14662-0.2793 0.40039-0.41805 0.53308-0.67968 0.70283-0.67969 0.43945 1e-5 -0.06645 0.2506-0.319 0.5586-0.56055 0.25377-0.19902 0.38387-0.29581 0.40039-0.2793zm-238.46 2.5684c0.04562 0.01329 0.01495 0.11868-0.11328 0.32617-0.11354 0.18371-0.03301 0.54449 0.17969 0.80078 0.21244 0.25629 0.30623 0.46484 0.20703 0.46484-0.09916 0-0.27444-0.15381-0.39062-0.3418-0.11619-0.18803-0.44233-0.25406-0.72266-0.14648-0.40952 0.15716-0.39339 0.0683 0.08203-0.45703 0.394-0.43534 0.68178-0.66864 0.75781-0.64648zm235.26 0.66406c0.1059 0 0.21084 0.05263 0.31445 0.15625 0.20724 0.20724 0.10001 0.31445-0.31445 0.31445-0.41442 0-0.52365-0.10721-0.31641-0.31445 0.10362-0.10362 0.21051-0.15625 0.31641-0.15625zm-1.9199 1.8887c0.0165 0.01652-0.0803 0.14662-0.2793 0.40039-0.41805 0.53308-0.68164 0.70088-0.68164 0.4375 0-0.0665 0.25255-0.31704 0.56055-0.55859 0.25377-0.19902 0.38387-0.29581 0.40039-0.2793zm3.8398 0c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.5586 0.49209 0.5586 0.55859 0 0.26338-0.26164 0.0955-0.67969-0.4375-0.19901-0.25377-0.29581-0.38387-0.2793-0.40039zm2.5606 2.5606c0.0165-0.0165 0.14466 0.0783 0.39844 0.27734 0.308 0.24155 0.56054 0.49405 0.56054 0.56055 0 0.26338-0.26163 0.0936-0.67968-0.43946-0.19901-0.25377-0.29582-0.38192-0.2793-0.39843zm2.5586 2.5586c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24155 0.56055 0.49404 0.56055 0.56054 0 0.26338-0.26359 0.0936-0.68164-0.43945-0.19901-0.25377-0.29581-0.38387-0.2793-0.40039zm-109.7 1.8613-0.49024 1.709c-0.27007 0.94035-0.59013 2.1419-0.71093 2.6699-0.1208 0.52801-0.99643 3.814-1.9453 7.3027-0.94888 3.4887-1.6419 6.4773-1.541 6.6406 0.10091 0.16327 0.3815 0.29688 0.62304 0.29688 1.0565 0 10.937 3.4389 14.834 5.1621 7.6982 3.4045 15.801 8.3102 21.805 13.201 3.3973 2.7676 9.5976 8.8311 12.307 12.037 2.553 3.0213 5.9594 7.8197 7.6875 10.83 0.62174 1.083 1.614 2.8134 2.207 3.8457 1.6996 2.9584 4.3097 8.8117 5.8203 13.053 2.3736 6.6639 3.723 12.337 4.8457 20.371 0.73973 5.2939 0.72543 17.438-0.0273 22.619-1.0562 7.2691-1.0563 6.7186 0 6.7324 0.50851 7e-3 4.3074 0.64041 8.4434 1.4082 8.7306 1.6207 9.1721 1.6635 9.4141 0.9004 0.31809-1.0033 1.1082-6.7188 1.4922-10.803 0.56765-6.0375 0.21626-19.624-0.65234-25.26-3.2308-20.965-11.211-39.224-24.152-55.264-2.981-3.6947-11.799-12.334-16.021-15.697-7.4744-5.9536-16.829-11.596-25.84-15.582-4.3332-1.9171-14.644-5.5491-16.928-5.9629zm112.26 0.69922c0.0165-0.0165 0.14662 0.0803 0.40039 0.27929 0.308 0.24155 0.56055 0.49415 0.56055 0.56055 0 0.26337-0.26359 0.0936-0.68164-0.43945-0.19901-0.25377-0.29582-0.38387-0.2793-0.40039zm-275.33 1.502c-9.3116 12.838-16.785 26.738-22.514 41.893 2.1587-3.4304 4.3181-6.8606 6.4766-10.291 2.5138-3.6256 4.7345-7.4728 7.541-10.877 0.24454-0.29661 0.62812-0.71414 1.041-1.1348 0.2684-0.75653 0.5552-1.5132 0.86328-2.2188 0.31384-0.71874 0.75638-1.3755 1.1348-2.0625 0.88962-1.4068 1.6435-2.9084 2.6699-4.2188 0.21163-0.27018 0.51577-0.68936 0.83789-1.1191 0.58125-2.4735 0.47269-2.1917 1.207-6.752 0.18544-1.1515 0.43865-2.2205 0.74219-3.2188zm277.89 1.0586c0.0165-0.0165 0.14661 0.0803 0.40039 0.2793 0.308 0.24155 0.55859 0.49208 0.55859 0.5586 0 0.26336-0.26164 0.0936-0.67969-0.43946-0.19901-0.25377-0.29581-0.38192-0.27929-0.39844zm2.5586 2.5586c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24154 0.56055 0.49404 0.56055 0.56054 0 0.26337-0.26359 0.0936-0.68164-0.43945-0.19901-0.25377-0.29582-0.38387-0.2793-0.40039zm2.5606 2.5605c0.0165-0.0165 0.14661 0.0803 0.40039 0.2793 0.308 0.24155 0.56054 0.49405 0.56054 0.56055 0 0.26337-0.26359 0.0936-0.68164-0.43945-0.19901-0.25377-0.29581-0.38387-0.27929-0.4004zm-120 0.96094c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24155 0.56054 0.49219 0.56054 0.55859 0 0.26337-0.2636 0.0936-0.68164-0.43945-0.19902-0.25377-0.29581-0.38192-0.27929-0.39844zm122.56 1.5996c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.30802 0.24155 0.5586 0.49209 0.5586 0.55859 0 0.26337-0.26164 0.0955-0.67969-0.4375-0.19901-0.25377-0.29581-0.38387-0.2793-0.40039zm-120 0.95899c0.0165-0.0165 0.14663 0.0803 0.40039 0.27929 0.308 0.24155 0.5586 0.49405 0.5586 0.56055 0 0.26337-0.26165 0.0936-0.67969-0.43945-0.19902-0.25377-0.29581-0.38388-0.2793-0.40039zm122.56 1.6016c0.0165-0.0165 0.14466 0.0783 0.39844 0.27734 0.308 0.24155 0.56055 0.49405 0.56055 0.56055 0 0.26337-0.26164 0.0936-0.67969-0.43945-0.19901-0.25377-0.29582-0.38192-0.2793-0.39844zm-120 0.95898c0.0165-0.0165 0.14467 0.0803 0.39844 0.2793 0.308 0.24155 0.56055 0.49405 0.56055 0.56055 0 0.26337-0.26165 0.0936-0.67969-0.43946-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm122.56 1.5996c0.0165-0.0165 0.14661 0.0803 0.40039 0.2793 0.308 0.24155 0.56054 0.49415 0.56054 0.56055 0 0.26337-0.26359 0.0936-0.68164-0.43946-0.19901-0.25377-0.29581-0.38387-0.27929-0.40039zm-120 0.96094c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24155 0.56054 0.49209 0.56054 0.55859 0 0.26337-0.2636 0.0955-0.68164-0.4375-0.19902-0.25377-0.29581-0.38387-0.27929-0.40039zm-92.699 1.0977c-2.7932-1.2e-4 -5.5418 0.2581-7.4531 0.77343-8.1417 2.1952-13.646 6.1531-18.137 13.043-2.487 3.8154-4.7339 9.091-5.7109 13.406-0.74013 3.269 0.24872 11.935 2.1172 18.559 2.1005 7.4461 8.5411 23.607 14.238 35.723 18.739 39.85 45.056 77.446 70.846 101.21 3.08 2.8384 6.6078 5.9677 7.8398 6.9551 5.135 4.1155 11.982 7.8836 15.84 8.7168 2.9133 0.62913 11.39 0.64598 14.641 0.0293 5.2242-0.99102 9.0611-2.6674 12.912-5.6426 5.6817-4.3895 9.8419-11.095 11.557-18.627 0.93735-4.117 0.9538-10.728 0.0371-14.926-3.2711-14.979-14.641-26.389-29.387-29.486-3.1293-0.65734-10.044-0.50199-13.545 0.30274-4.5666 1.0495-10.019 3.8099-13.197 6.6816l-1.168 1.0547-3.3359-3.5977c-4.6866-5.0514-7.4521-8.2497-11.199-12.957-1.7859-2.2436-3.4069-4.1851-3.6016-4.3144-0.1947-0.1293-0.23606-0.23724-0.0937-0.24024 0.14227-3e-3 -0.37548-0.83175-1.1504-1.8438-4.8315-6.3096-12.072-18.153-16.773-27.439-3.3393-6.5957-8.416-18.233-8.416-19.291 0-0.14472 0.68098-0.59249 1.5117-0.99609 8.3864-4.0744 14.567-11.821 17.098-21.43 0.8378-3.1808 1.1386-9.8742 0.60742-13.518-0.21542-1.4776-0.82908-4.0546-1.3633-5.7266-4.1057-12.85-14.273-22.511-26.992-25.648-2.09-0.51551-4.9275-0.77332-7.7207-0.77343zm215.26 0.50195c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24155 0.56055 0.49404 0.56055 0.56054 0 0.26337-0.26359 0.0936-0.68164-0.43945-0.19901-0.25377-0.29581-0.38387-0.2793-0.40039zm-120 0.96094c0.0165-0.0165 0.14662 0.0783 0.40039 0.27734 0.308 0.24155 0.56055 0.49405 0.56055 0.56055 0 0.26337-0.2636 0.0936-0.68164-0.43946-0.19902-0.25377-0.29582-0.38192-0.2793-0.39843zm122.56 1.5996c0.0165-0.0165 0.14661 0.0803 0.40039 0.27929 0.308 0.24155 0.55859 0.4921 0.55859 0.5586 0 0.26337-0.26163 0.0955-0.67968-0.4375-0.19901-0.25377-0.29582-0.38388-0.2793-0.40039zm-120 0.95898c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24155 0.55859 0.49414 0.55859 0.56054 0 0.26337-0.26164 0.0936-0.67968-0.43945-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm-27.328 8.0547c-0.43009-0.0103-0.51921 0.21089-0.68164 0.95703-0.12806 0.58829-1.1495 4.4519-2.2695 8.5879-1.12 4.136-2.0332 7.736-2.0312 8 3e-3 0.33606 1.5613 0.99073 5.1973 2.1816 2.8563 0.93556 6.5604 2.3566 8.2324 3.1582 18.32 8.7825 31.095 24.252 36.154 43.781 2.2016 8.4977 2.6624 18.719 1.2461 27.607-0.20404 1.2805-0.30386 2.3954-0.22266 2.4766 0.12997 0.12996 14.23 2.7584 16.742 3.1211 0.528 0.0762 1.1299 0.1611 1.3359 0.1875 0.20602 0.0264 0.45737-0.34803 0.56055-0.83203 0.62866-2.949 1.1882-8.3226 1.3574-13.041 0.57917-16.148-3.4789-32.274-11.668-46.375-10.368-17.853-27.865-31.662-48.545-38.311-2.112-0.67901-4.2932-1.3198-4.8477-1.4238-0.2344-0.044-0.41718-0.0727-0.56055-0.0762zm-175.56 27.473c-0.14266 0.47999-0.29599 0.95122-0.43555 1.4336-0.65074 2.2493-1.0592 3.7234-1.4492 5.2812 2.2693-0.33511 4.9302-0.17552 7.4629-0.17774 1.643-0.80492 2.9373-1.9344 3.5762-3.6738 1.1596-3.1573-4.6275-2.6806-9.1543-2.8633zm163.71 0.35157c-0.11832 0.12893-3.3058 11.815-4.1797 15.322-0.28501 1.144-0.58853 2.318-0.67383 2.6094-0.11956 0.40835 0.13295 0.57821 1.1035 0.74218 2.2817 0.38551 9.4249 3.5247 12.801 5.625 3.7851 2.3549 8.489 6.6118 10.863 9.832 3.4175 4.6352 6.1553 11.009 7.3125 17.023 0.73125 3.8006 0.75357 15.679 0.0293 15.686-0.41359 4e-3 -0.26093 0.95895 0.16015 1.002 0.22 0.0225 0.68707 0.0845 1.0391 0.13672 1.1111 0.16469 13.702 2.4177 15.041 2.6914 2.3627 0.48312 2.2882 0.53744 2.707-1.9883 0.59256-3.5732 0.79138-13.19 0.34766-16.799-1.1584-9.422-4.6171-19.036-9.5254-26.48-6.4502-9.7828-16.66-17.961-28.09-22.5-3.4853-1.384-8.7573-3.0965-8.9355-2.9023zm95.176 19.482c0.0165-0.0165 0.14662 0.0783 0.40039 0.27734 0.308 0.24155 0.55859 0.49405 0.55859 0.56055 0 0.26337-0.26164 0.0936-0.67968-0.43945-0.19902-0.25377-0.29582-0.38192-0.2793-0.39844zm-169.62 2.2383c0.088 0-0.98502 1.1526-2.3848 2.5606s-2.6171 2.5605-2.7051 2.5605 0.98503-1.1525 2.3848-2.5605 2.6171-2.5606 2.7051-2.5606zm172.17 0.32031c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24155 0.56055 0.49405 0.56055 0.56055 0 0.26337-0.2636 0.0936-0.68164-0.43946-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm2.5606 2.5606c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24155 0.56055 0.49404 0.56055 0.56054 0 0.26337-0.2636 0.0936-0.68164-0.43945-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm-65.92 2.5606c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.67969 0.70087-0.67969 0.4375 0-0.0665 0.2506-0.31705 0.5586-0.5586 0.25377-0.19902 0.38387-0.29581 0.40039-0.27929zm68.48 0c0.0165-0.0165 0.14662 0.0803 0.40039 0.27929 0.308 0.24155 0.55859 0.4921 0.55859 0.5586 0 0.26337-0.26165 0.0955-0.67969-0.4375-0.19902-0.25377-0.29581-0.38388-0.27929-0.40039zm-71.041 2.5586c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.67968 0.70282-0.67968 0.43945 0-0.0665 0.25254-0.31899 0.56054-0.56054 0.25377-0.19902 0.38192-0.29582 0.39844-0.2793zm73.602 0c0.0165-0.0165 0.14467 0.0803 0.39844 0.2793 0.308 0.24155 0.56055 0.49414 0.56055 0.56054 0 0.26337-0.26165 0.0936-0.67969-0.43945-0.19902-0.25377-0.29581-0.38387-0.2793-0.40039zm-76.16 2.5606c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.68164 0.70282-0.68164 0.43945 0-0.0664 0.25255-0.319 0.56055-0.56055 0.25377-0.19902 0.38387-0.29581 0.40039-0.27929zm78.719 0c0.0165-0.0165 0.14662 0.0803 0.40039 0.27929 0.308 0.24155 0.56054 0.49405 0.56054 0.56055 0 0.26337-0.2636 0.0936-0.68164-0.43945-0.19902-0.25377-0.29581-0.38388-0.27929-0.40039zm-81.279 2.5605c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.68164 0.70283-0.68164 0.43946 0-0.0665 0.25255-0.319 0.56055-0.56055 0.25377-0.19902 0.38387-0.29581 0.40039-0.2793zm-43.52 43.52c0.0165 0.0165-0.0803 0.14663-0.2793 0.4004-0.41804 0.53308-0.68164 0.70282-0.68164 0.43945 0-0.0665 0.25255-0.319 0.56055-0.56055 0.25377-0.19902 0.38387-0.29581 0.40039-0.2793zm-2.5606 2.5606c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41802 0.53308-0.67968 0.70087-0.67968 0.4375 0-0.0665 0.25059-0.31704 0.55859-0.55859 0.25377-0.19902 0.38387-0.29582 0.40039-0.2793zm-2.5605 2.5606c0.0165 0.0165-0.0803 0.14467-0.2793 0.39844-0.41804 0.53308-0.67969 0.70282-0.67969 0.43945 0-0.0665 0.25255-0.319 0.56055-0.56055 0.25377-0.19902 0.38192-0.29386 0.39844-0.27734zm-119.84 50.08 17.201 17.117c9.46 9.4154 17.199 17.157 17.199 17.201 0 0.2248-1.3688-1.1312-17.281-17.119zm302.24 3.6797c0.0165 0.0165-0.0803 0.14467-0.2793 0.39844-0.24157 0.308-0.49415 0.56055-0.56055 0.56055-0.26339 0-0.0936-0.26165 0.43946-0.67969 0.25376-0.19902 0.38387-0.29581 0.40039-0.2793zm-117.44 0.95899c0.0165 0.0165-0.0803 0.14662-0.27929 0.40039-0.41804 0.53308-0.68164 0.70282-0.68164 0.43945-2e-5 -0.0665 0.25254-0.31899 0.56054-0.56054 0.25377-0.19902 0.38387-0.29582 0.40039-0.2793zm114.88 1.5996c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41805 0.53308-0.67969 0.70282-0.67969 0.43945 0-0.0664 0.25255-0.31899 0.56055-0.56054 0.25378-0.19903 0.38192-0.29582 0.39844-0.2793zm-117.44 0.96094c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.67969 0.70282-0.67969 0.43945 0-0.0664 0.2506-0.319 0.5586-0.56055 0.25375-0.19902 0.38387-0.29581 0.40039-0.27929zm114.88 1.5996c0.0165 0.0165-0.0803 0.14663-0.27929 0.4004-0.41805 0.53308-0.68164 0.70282-0.68164 0.43945 0-0.0665 0.25254-0.319 0.56054-0.56055 0.25378-0.19902 0.38389-0.2958 0.40039-0.2793zm-117.44 0.96094c0.0165 0.0165-0.0803 0.14467-0.2793 0.39844-0.41804 0.53308-0.67969 0.70282-0.67969 0.43945 0-0.0665 0.25255-0.31704 0.56055-0.55859 0.25377-0.19902 0.38192-0.29582 0.39844-0.2793zm114.88 1.5996c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41805 0.53308-0.68164 0.70087-0.68164 0.4375 0-0.0665 0.25255-0.31704 0.56055-0.55859 0.25377-0.19902 0.38387-0.29582 0.40039-0.2793zm-117.44 0.95899c0.0165 0.0165-0.0803 0.14662-0.27929 0.40039-0.41804 0.53308-0.68164 0.70282-0.68164 0.43945 0-0.0664 0.25254-0.319 0.56054-0.56055 0.25377-0.19902 0.38388-0.29581 0.40039-0.27929zm114.88 1.6016c0.0165 0.0165-0.0803 0.14467-0.2793 0.39844-0.41805 0.53308-0.67968 0.70282-0.67968 0.43945 0-0.0664 0.25059-0.319 0.55859-0.56055 0.25378-0.19902 0.38388-0.29386 0.40039-0.27734zm-117.44 0.95898c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.68164 0.70283-0.68164 0.43946 0-0.0664 0.25255-0.319 0.56055-0.56055 0.25377-0.19902 0.38387-0.29581 0.40039-0.2793zm114.88 1.5996c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41805 0.53308-0.68164 0.70283-0.68164 0.43946 0-0.0665 0.25255-0.319 0.56055-0.56055 0.25377-0.19902 0.38387-0.29582 0.40039-0.2793zm-117.44 0.96094c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.67968 0.70087-0.67968 0.4375 0-0.0665 0.25059-0.31704 0.55859-0.55859 0.25377-0.19902 0.38387-0.29582 0.40039-0.2793zm114.88 1.5996c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41805 0.53308-0.68164 0.70282-0.68164 0.43945 0-0.0665 0.25255-0.31899 0.56055-0.56054 0.25378-0.19902 0.38387-0.29582 0.40039-0.2793zm-117.44 0.96094c0.0165 0.0165-0.0803 0.14466-0.2793 0.39843-0.41804 0.53308-0.68164 0.70283-0.68164 0.43946 0-0.0664 0.25255-0.319 0.56055-0.56055 0.25377-0.19902 0.38387-0.29386 0.40039-0.27734zm114.88 1.5996c0.0165 0.0165-0.0803 0.14662-0.27929 0.40039-0.41805 0.53308-0.67969 0.70087-0.67969 0.4375 0-0.0664 0.25059-0.31705 0.55859-0.5586 0.25378-0.19902 0.38388-0.29581 0.40039-0.27929zm-117.44 0.95898c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.68164 0.70282-0.68164 0.43945 0-0.0664 0.25255-0.31899 0.56055-0.56054 0.25377-0.19902 0.38387-0.29582 0.40039-0.2793zm114.88 1.5996c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.24157 0.308-0.49219 0.56055-0.55859 0.56055-0.26339 0-0.0936-0.2636 0.43945-0.68164 0.25377-0.19902 0.38192-0.29582 0.39844-0.2793zm-2.5586 2.5606c0.0165 0.0165-0.0803 0.14662-0.27929 0.40039-0.24157 0.308-0.49415 0.56054-0.56055 0.56054-0.26339 0-0.0936-0.2636 0.43945-0.68164 0.25378-0.19902 0.38388-0.29581 0.40039-0.27929zm-2.5605 2.5605c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41805 0.53297-0.68164 0.70285-0.68164 0.43946 0-0.0664 0.25255-0.31898 0.56055-0.56055 0.25377-0.19901 0.38387-0.29581 0.40039-0.2793zm-10.08 1.7598 1.6797 1.5879c0.924 0.87338 1.6797 1.6291 1.6797 1.6797 0 0.23878-0.2959-0.0268-1.7715-1.5879zm-228 0.80078c0.01652 0.0165-0.08028 0.14466-0.2793 0.39844-0.41804 0.53297-0.68164 0.70284-0.68164 0.43945 0-0.0664 0.25255-0.31702 0.56055-0.55859 0.25377-0.19901 0.38387-0.29582 0.40039-0.2793zm235.52 0c0.0165 0.0165-0.0803 0.14466-0.2793 0.39844-0.41805 0.53297-0.67969 0.70284-0.67969 0.43945 0-0.0664 0.2506-0.31702 0.5586-0.55859 0.25377-0.19901 0.38387-0.29582 0.40039-0.2793zm-238.13 2.5586c0.088 0-0.17648 0.36079-0.58984 0.80079s-0.82411 0.80078-0.91211 0.80078 0.17844-0.36078 0.5918-0.80078 0.82216-0.80079 0.91016-0.80079zm235.52 0c0.088 0-0.17846 0.36079-0.5918 0.80079s-0.82411 0.80078-0.91211 0.80078 0.17846-0.36078 0.5918-0.80078 0.82411-0.80079 0.91211-0.80079zm-235.31 1.7598 15.6 15.52c8.58 8.5353 15.602 15.555 15.602 15.6 0 0.22499-1.2569-1.0174-15.682-15.518zm235.52 0 4.0801 3.9961c2.244 2.1973 4.0801 4.0334 4.0801 4.0801 0 0.22989-0.45293-0.2062-4.1641-3.9961zm-238.24 0.80078c0.01652 0.0165-0.08028 0.14662-0.2793 0.40039-0.24155 0.308-0.49415 0.56055-0.56055 0.56055-0.26337 0-0.09359-0.26359 0.43945-0.68164 0.25377-0.19901 0.38387-0.29581 0.40039-0.2793zm235.52 0c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.24157 0.308-0.49414 0.56055-0.56055 0.56055-0.26339 0-0.0936-0.26359 0.43946-0.68164 0.25377-0.19901 0.38387-0.29581 0.40039-0.2793zm-238.08 2.5606c0.01652 0.0165-0.08028 0.14661-0.2793 0.40039-0.41804 0.53297-0.68164 0.70089-0.68164 0.4375 0-0.0664 0.25255-0.31702 0.56055-0.5586 0.25377-0.199 0.38387-0.29581 0.40039-0.27929zm235.52 0c0.0165 0.0165-0.0803 0.14661-0.2793 0.40039-0.24155 0.308-0.49414 0.55859-0.56054 0.55859-0.26339 0-0.0936-0.26164 0.43945-0.67969 0.25378-0.199 0.38388-0.29581 0.40039-0.27929zm-238.08 2.5586c0.01652 0.0165-0.08028 0.14662-0.2793 0.40039-0.24155 0.308-0.49415 0.56055-0.56055 0.56055-0.26337 0-0.09359-0.26359 0.43945-0.68164 0.25377-0.19901 0.38387-0.29581 0.40039-0.2793zm235.52 0c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.24155 0.308-0.4922 0.56055-0.5586 0.56055-0.26337 0-0.0936-0.26359 0.43946-0.68164 0.25377-0.19901 0.38191-0.29581 0.39844-0.2793zm-237.82 2.2402c0.088 0-0.10136 0.28862-0.41992 0.64062s-0.65028 0.64063-0.73828 0.64063 0.10136-0.28863 0.41992-0.64063 0.65028-0.64062 0.73828-0.64062zm235.26 0.32031c0.0165 0.0165-0.0803 0.14661-0.2793 0.40039-0.24155 0.308-0.49414 0.56055-0.56054 0.56055-0.26337 0-0.0936-0.26359 0.43945-0.68164 0.25377-0.19901 0.38387-0.29582 0.40039-0.2793zm-2.5606 2.5606c0.0165 0.0165-0.0803 0.14661-0.27929 0.40039-0.24155 0.308-0.49415 0.55859-0.56055 0.55859-0.26337 0-0.0936-0.26164 0.43945-0.67969 0.25377-0.19901 0.38388-0.29581 0.40039-0.27929zm-2.5605 2.5605c0.0165 0.0165-0.0803 0.14467-0.2793 0.39844-0.24155 0.308-0.49415 0.56055-0.56055 0.56055-0.26337 0-0.0936-0.26164 0.43946-0.67969 0.25377-0.19901 0.38387-0.29581 0.40039-0.2793zm-2.5606 2.5586c0.0165 0.0165-0.0803 0.14661-0.2793 0.40039-0.24155 0.308-0.49219 0.56054-0.55859 0.56054-0.26337 0-0.0936-0.26359 0.43945-0.68164 0.25377-0.19901 0.38192-0.29581 0.39844-0.27929zm-2.5586 2.5605c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.24155 0.308-0.49415 0.56055-0.56055 0.56055-0.26337 0-0.0936-0.26359 0.43946-0.68164 0.25377-0.19901 0.38387-0.29581 0.40039-0.2793zm-2.5606 2.5606c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.24155 0.308-0.49414 0.55859-0.56054 0.55859-0.26337 0-0.0936-0.26163 0.43945-0.67968 0.25377-0.19901 0.38387-0.29582 0.40039-0.2793zm-2.5606 2.5606c0.0165 0.0165-0.0803 0.14466-0.27929 0.39843-0.24155 0.308-0.4922 0.56055-0.5586 0.56055-0.26337 0-0.0955-0.26359 0.4375-0.68164 0.25377-0.19901 0.38388-0.29386 0.40039-0.27734zm-2.5586 2.5586c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53307-0.68164 0.70284-0.68164 0.43945 0-0.0664 0.25255-0.31897 0.56055-0.56054 0.25377-0.19901 0.38387-0.29582 0.40039-0.2793zm-2.5606 2.5606c0.0165 0.0165-0.0803 0.14661-0.27929 0.40039-0.41804 0.53307-0.68164 0.70284-0.68164 0.43945-1e-5 -0.0664 0.25254-0.31898 0.56054-0.56055 0.25377-0.19901 0.38387-0.29581 0.40039-0.27929zm-2.5605 2.5605c0.0165 0.0165-0.0803 0.14467-0.2793 0.39844-0.41804 0.53307-0.67969 0.70285-0.67969 0.43946 0-0.0664 0.2506-0.31703 0.5586-0.5586 0.25375-0.19901 0.38387-0.29581 0.40039-0.2793z" clip-path="url(#a)" stroke-width=".32"/>
        <g transform="matrix(.98211 .20501 -.1883 1.0693 59.77 -43.241)" fill-opacity=".81768" fill-rule="evenodd">
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        </g>
        <g transform="matrix(2.226 .38608 -.42679 2.0137 36.022 -188.17)" fill-opacity=".81768" fill-rule="evenodd">
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        </g>
        <g transform="matrix(3.1589 .7315 -.60565 3.8153 34.586 -514.5)" fill-opacity=".81768" fill-rule="evenodd">
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        </g>
        <g transform="matrix(1.8114 .39722 -.34729 2.0718 113.82 -252.72)" fill-opacity=".81768" fill-rule="evenodd">
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        </g>
        <g transform="matrix(1.141 3.3865 -1.5922 .11958 431.21 -239.05)" fill-opacity=".81768" fill-rule="evenodd">
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        </g>
        <g transform="matrix(2.3296 -.49975 -.44666 -2.6065 154.21 755.09)" fill-opacity=".81768" fill-rule="evenodd">
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        </g>
        </g>
        </g>
        </svg>
        <p>{{pluckcom('Contact_us_menu_title',item.component)}}</p></a></li>
        </ul>
        </div>
        </div>
        <div class="mb_menu">
        <div id="dl-menu" class="dl-menuwrapper">
        <button class="dl-trigger" id="showMenu">Open Menu</button>
        <ul class="dl-menu">
        <li><a href="/Nirvana/aboutus.html"><svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46668" version="1.1" id="svg8" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" sodipodi:docname="About_Us_Icon.svg"> <defs id="defs2"> <clipPath clipPathUnits="userSpaceOnUse" id="clipPath6266"> <rect style="fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.72308987;stroke-opacity:1" id="rect6268" width="235.73741" height="82.482758" x="-308.72379" y="-6.8863735" /> </clipPath> <clipPath clipPathUnits="userSpaceOnUse" id="clipPath6274"> <ellipse style="fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.80294245;stroke-opacity:1" id="ellipse6276" cx="-192.50114" cy="17.866276" rx="64.168037" ry="64.273895" /> </clipPath> <clipPath clipPathUnits="userSpaceOnUse" id="clipPath6284"> <rect style="fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.72308987;stroke-opacity:1" id="rect6286" width="177.67633" height="117.21391" x="-267.78333" y="-14.123268" /> </clipPath> <clipPath clipPathUnits="userSpaceOnUse" id="clipPath6290"> <ellipse style="fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.79538357;stroke-opacity:1" id="ellipse6292" cx="-193.01503" cy="28.365179" rx="66.930298" ry="60.109688" /> </clipPath> </defs> <sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.53740115" inkscape:cx="155.77825" inkscape:cy="310.99283" inkscape:document-units="mm" inkscape:current-layer="layer2" showgrid="false" units="px" showborder="true" inkscape:window-width="1366" inkscape:window-height="705" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" inkscape:snap-smooth-nodes="false" showguides="false" inkscape:pagecheckerboard="true" /> <metadata id="metadata5"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:groupmode="layer" id="layer2" inkscape:label="Inner Layer" style="display:inline"> <g id="g826"> <path inkscape:connector-curvature="0" id="path6169" d="M 67.624966,21.570191 C 50.079683,21.593611 34.121997,33.809605 26.651305,49.684934 35.541276,35.634474 51.000981,29.520625 67.627668,29.498224 84.14445,29.499324 99.528527,35.482067 108.46628,49.371652 100.92601,33.66149 85.050942,21.587089 67.624966,21.570191 Z M 41.409519,47.560126 c -1.056269,-0.03934 -0.421925,0.427362 1.474607,2.160595 2.583537,2.361098 3.665746,3.61332 4.459603,5.159771 l 0.514493,1.001977 v 12.474737 c 0,13.589044 -0.03504,13.008239 0.906097,14.956045 0.705872,1.460891 2.968648,3.283317 5.323168,4.287429 0.439987,0.187641 0.943737,0.325832 1.119459,0.306535 0.374722,-0.04114 0.367918,0.394024 0.108025,-7.227864 -0.189582,-5.557975 -0.241699,-15.586175 -0.08575,-16.490068 0.06785,-0.393228 0.180615,-0.571424 0.357849,-0.565806 0.142261,0.0045 1.062999,0.951015 2.046488,2.10388 2.977584,3.490354 9.59872,10.537002 12.054774,12.829206 6.164043,5.752857 11.419265,8.621974 15.797324,8.625505 3.279975,0.0026 5.367435,-1.270032 6.86732,-4.187507 1.775973,-3.454526 2.28875,-8.285835 2.20448,-20.760617 l -0.05199,-7.732231 -0.514489,-1.000625 c -0.623612,-1.213114 -1.988995,-2.593049 -3.587943,-3.626426 -1.346427,-0.870167 -3.156466,-1.777575 -3.290181,-1.649478 -0.05016,0.04806 -0.0067,0.724156 0.09587,1.502288 0.471864,3.575377 0.538501,5.168514 0.457099,10.868467 -0.05956,4.168692 -0.151628,6.224676 -0.320712,7.151571 -0.575357,3.154054 -1.232738,3.875538 -3.51907,3.862066 -2.125944,-0.01254 -3.374787,-0.602741 -6.813296,-3.221313 -2.481531,-1.889791 -4.99933,-3.920727 -8.38716,-6.766037 C 60.674082,54.944061 55.991857,51.556338 52.650685,50.065742 50.082596,48.920032 47.866129,48.357931 43.938087,47.855858 42.718876,47.700021 41.889636,47.578005 41.409514,47.560126 Z M 27.000375,85.781729 C 34.47107,101.65707 50.428756,113.87309 67.974037,113.89648 85.400011,113.87958 101.27508,101.80518 108.81536,86.095016 99.877596,99.984603 84.493522,105.96734 67.976736,105.96845 51.350051,105.94604 35.890349,99.832191 27.000375,85.781729 Z" style="stroke-width:0.15635954" /> <g inkscape:label="Layer 1" id="layer1" transform="matrix(1.0281448,0,0,1.0281447,-1.4400028,-170.00676)"> <path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="M 67.27977,165.35295 A 65.879319,65.879319 0 0 0 1.4005815,231.23213 65.879319,65.879319 0 0 0 67.27977,297.11131 65.879319,65.879319 0 0 0 133.15894,231.23213 65.879319,65.879319 0 0 0 67.27977,165.35295 Z m 0,5.06739 A 60.81168,60.81168 0 0 1 128.09155,231.23213 60.81168,60.81168 0 0 1 67.27977,292.04392 60.81168,60.81168 0 0 1 6.4679725,231.23213 60.81168,60.81168 0 0 1 67.27977,170.42034 Z" id="path2255" inkscape:connector-curvature="0" /> </g> </g> </g> </svg>{{pluckcom('About_us_menu_title',item.component)}}</a></li>
        <li><a href="/Nirvana/services.html"><svg version="1.1" id="svg3730" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:osb="http://www.openswatchbook.org/uri/2009/osb" xmlns:dc="http://purl.org/dc/elements/1.1/" sodipodi:docname="Arab.svg" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" enable-background="new 0 0 512 512" xml:space="preserve"> <sodipodi:namedview pagecolor="#ffffff" id="base" borderopacity="1.0" bordercolor="#666666" inkscape:cx="99.376519" inkscape:zoom="0.70710678" showgrid="false" inkscape:cy="126.53654" showguides="true" units="px" inkscape:guide-bbox="true" inkscape:snap-global="false" inkscape:pagecheckerboard="true" inkscape:window-maximized="1" inkscape:window-y="-8" inkscape:current-layer="layer2" inkscape:pageshadow="2" inkscape:pageopacity="0.0" inkscape:window-x="-8" inkscape:window-height="705" inkscape:document-units="mm" inkscape:window-width="1366"> </sodipodi:namedview> <g id="layer1" transform="translate(0,-161.5333)" inkscape:label="Layer 1" inkscape:groupmode="layer"> </g> <g id="layer3" inkscape:label="outer" inkscape:groupmode="layer"> </g> <g> <path d="M257.3,0C116.9,0,2.6,114.8,2.6,255.9s114.3,255.9,254.7,255.9C397.7,511.7,512,397,512,255.9 S397.7,0,257.3,0z M345.1,468c-0.5-15-0.9-30.2-1.4-34.4c-0.9-8,1.3-14.9,6.9-20.8c16-16.5,32-33.2,48-49.8c2.6-2.7,5-5.4,6.6-8.8 c10.8-23.3,21.6-46.5,32.5-69.8c4-8.6,7.6-17,7.4-26.9c-0.2-10.1,0-20.3,0.6-30.5c0.8-14.2,1.2-28.4,1.3-42.6 c0.1-10.2-6.4-17.3-15.8-18.1c-8.6-0.8-15.9,5-17.8,14.7c-3.2,16.3-5.9,32.7-9,49c-0.6,3.1,0.4,4.7,3,6.2 c13.2,7.9,14.8,20.5,5.2,32.7c-21.7,27.5-45.8,53.1-64.6,82.8c-1,1.6-2.1,4.3-4.5,2.7c-2.2-1.4-0.8-3.7,0.3-5.4 c5.3-8,10.1-16.3,16.2-23.8c16.5-20.3,33.1-40.6,49.6-60.9c5.7-7,5.2-15.1-0.8-20.9c-5.7-5.5-13.9-5.8-20.5-0.1 c-5.7,4.9-10.8,10.5-16.4,15.6c-12.2,11.1-21.9,25-37.9,31.6c-5.8,2.4-11.1,5.8-16.6,8.7c-5.4,2.9-10.2,6.6-13.7,11.8 c-8.6,12.9-17.2,25.8-25.7,38.7c-3.7,5.5-5.3,11.5-5.7,18.2c-1.1,17-0.7,34-0.8,47c0,13.1,0,47.9,0,70.2c-4.7,0.3-9.3,0.5-14.1,0.5 c-4.7,0-9.3-0.2-14-0.5c0-20.7,0-55.7,0-66.2c0,0-0.1,0-0.1,0c0-12.6-0.1-25.1,0-37.7c0.2-15.2-2.8-29.1-12.7-41.3 c-6.4-7.8-11.5-16.7-17-25.2c-4.8-7.6-11-13.2-19.2-17c-17.6-8-33.4-18.4-46.1-33.4c-2.8-3.4-6.2-6.3-9.4-9.4 c-4.7-4.4-9.2-9.1-14.3-12.9c-6.5-4.7-14.7-3.9-19.2,1.2c-5.4,6-5.4,14.8-0.2,21.2c17,20.8,34.2,41.6,50.9,62.7 c5.6,7,10.1,15,15.1,22.5c1,1.6,2,3.6-0.1,4.9c-1.9,1.2-3-0.7-3.9-2c-2-2.9-3.9-5.9-5.9-8.7c-4.2-6.1-8.2-12.3-12.8-18 c-15.6-19.4-31.5-38.5-47.2-57.9c-9.3-11.6-6.8-24.7,6.1-32.3c2.2-1.3,3.3-2.4,2.8-5.2c-3.1-16.9-6.1-33.8-9.2-50.7 c-1-5.2-4-9.1-8.7-11.4c-12.6-6.3-25.2,2.9-24.7,18c0.8,24.4,1.5,48.7,2.1,73.1c0.2,6.7,1.6,13,4.5,19.2 c10.9,23,21.7,46.1,32.3,69.3c3.2,7,7.2,13.1,12.6,18.6c14.8,14.9,29.3,30.1,43.9,45.2c5.8,6,8.9,13,7.9,21.6 c-0.5,4.4-0.9,20.1-1.4,34.9c-82.7-34.6-141-116.7-141-212.2c0-126.7,102.6-229.8,228.7-229.8C383.4,26.1,486,129.2,486,255.9 C486,351.3,427.8,433.3,345.1,468z"/> <path d="M287.3,230.5c0.2-6.7,0.6-13.3,0.8-20c0.1-1.9,0.7-3.5,1.9-4.9c4.2-5,6.5-10.8,6.5-17.5 c0-6.1,0-12.2,0-18.3c0.1-10.2,0.4-20.4-0.9-30.5c-0.3-2.6-1.5-4.4-3.9-5.2c-5.1-1.8-10.3-3.4-15.4-5.2c-1.8-0.6-3-0.3-4.2,1.2 c-3.9,5.3-7.9,10.6-11.9,15.8c-2.2,2.9-3.5,2.9-5.7,0c-4.1-5.4-8.1-10.7-12.2-16.1c-0.9-1.3-2-1.6-3.5-1.1 c-5.3,1.8-10.6,3.6-15.9,5.4c-2,0.7-3.4,2.1-3.7,4.4c-0.4,4.6-1.1,9.1-1.1,13.8c0,11.7,0.1,23.4,0,35c-0.1,7.1,2.2,13.2,6.7,18.6 c1.1,1.3,1.6,2.6,1.6,4.2c0.3,6.8,0.6,13.6,0.8,20.4c0.3,9.1,6.8,16.6,15.8,17.2c9.5,0.7,19,0.7,28.5,0 C280.5,247.1,287.1,239.5,287.3,230.5z"/> <path d="M231.5,94.8c-0.1,13.8,12,26.1,25.7,26.2c13.5,0.1,25.7-12,25.8-25.7c0.1-14-11.5-25.9-25.5-26 C243.6,69.2,231.7,80.9,231.5,94.8z"/> <path d="M183.4,233.2c-19.1-19.1-31.1-45.5-31-74.6c0.1-58,47.1-105.2,104.8-105.3V41.8c-0.1,0-0.2,0-0.3,0 c-64.2,0.4-116.1,52.9-115.9,117.3c0.1,33.3,14.2,63.2,36.6,84.4l-4.1,7.2l49,13.2l-35.8-36L183.4,233.2z"/> <path d="M337.2,74l4.1-7.1l-49-13.2l35.8,36l3.1-5.4c19.1,19.1,31.1,45.4,31,74.5c-0.1,58-47.3,105.3-104.9,105.3 v11.5c0.3,0,0.6,0,0.9,0c63.7-0.4,115.8-53.2,115.5-117.1C373.5,125.2,359.5,95.2,337.2,74z"/> </g> </svg>{{pluckcom('Services_menu_Icon',item.component)}}</a></li>
        <li><a href="/Nirvana/travel-guide.html"><svg xmlns:osb="http://www.openswatchbook.org/uri/2009/osb" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46666" version="1.1" id="svg5506" style="enable-background:new" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" sodipodi:docname="travel_guide.svg"> <defs id="defs5500"> <linearGradient osb:paint="solid" id="linearGradient6884"> <stop id="stop6882" offset="0" style="stop-color:#f10000;stop-opacity:1;" /> </linearGradient> <linearGradient osb:paint="solid" id="linearGradient6864"> <stop id="stop6862" offset="0" style="stop-color:#e50000;stop-opacity:1;" /> </linearGradient> </defs> <sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.9609375" inkscape:cx="256" inkscape:cy="256" inkscape:document-units="px" inkscape:current-layer="layer2" showgrid="false" showborder="true" inkscape:snap-grids="false" inkscape:snap-to-guides="false" units="px" inkscape:window-width="1366" inkscape:window-height="705" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" inkscape:snap-others="false" inkscape:object-nodes="false" inkscape:snap-nodes="false" inkscape:snap-global="false" inkscape:pagecheckerboard="true" /> <metadata id="metadata5503"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Outer Border" inkscape:groupmode="layer" id="layer1" transform="translate(0,-161.53334)" style="opacity:1"> <path style="stroke-width:2.92485166" d="m 3631.011,-429.20229 c 2.7825,2.39641 5.4307,4.97742 8.5142,6.96031 0.6611,0.42511 -0.1271,-1.77932 -0.7725,-2.22779 -2.4753,-1.71971 -5.089,-3.25371 -7.7417,-4.73252 z" id="path6681" inkscape:connector-curvature="0" /> <path style="stroke-width:2.92485166" d="m 1113.9983,-375.41848 a 1273.4841,1209.5962 0 0 0 -0.3163,5.99858 1273.4841,1209.5962 0 0 0 0.4548,12.19359 c -0.078,-6.12291 -0.1259,-12.20205 -0.1385,-18.19217 z" id="path6303" inkscape:connector-curvature="0" /> </g> <g inkscape:groupmode="layer" id="layer2" inkscape:label="Lens" style="opacity:1" transform="translate(0,-376.53335)"> <path style="opacity:1;fill-opacity:1;stroke-width:0.00834122;enable-background:new" d="M 67.648286,376.53335 A 67.648439,67.733392 0 0 0 -3.3920189e-6,444.26668 67.648439,67.733392 0 0 0 67.648286,512.00001 67.648439,67.733392 0 0 0 119.08487,488.1415 l -0.94707,-0.86733 a 21.636547,21.340013 0 0 1 -8.28056,2.98428 62.444715,62.523133 0 0 1 -42.208954,16.53135 62.444715,62.523133 0 0 1 -62.4448129,-62.52312 62.444715,62.523133 0 0 1 62.4448129,-62.52311 62.444715,62.523133 0 0 1 62.444814,62.52311 62.444715,62.523133 0 0 1 -2.77309,18.41361 21.636547,21.340013 0 0 1 1.03688,6.49738 21.636547,21.340013 0 0 1 -1.51118,7.71675 67.648439,67.733392 0 0 0 8.45087,-32.62774 67.648439,67.733392 0 0 0 -67.648294,-67.73333 z m 0.947589,10.4632 c -31.377515,9e-5 -56.813607,25.35242 -56.813477,56.62603 7e-5,31.27346 25.436101,56.62594 56.813477,56.62602 12.430546,5e-5 23.92704,-3.98167 33.280675,-10.73127 a 21.077885,21.299547 0 0 1 -9.411364,-4.67668 c -7.236016,4.17749 -15.465486,6.40017 -23.869311,6.41499 -2.427886,-0.0199 -4.850557,-0.22438 -7.247294,-0.61125 -0.185615,-0.43854 -0.473667,-0.81706 -1.143195,-1.5323 -0.228823,-0.24444 -0.467254,-0.56757 -0.750949,-0.64358 0.203384,-0.36784 0.407301,-0.7352 0.625017,-1.08846 1.68712,-2.73737 2.89793,-3.09275 1.066811,-7.02102 -0.228923,-3.28475 -0.04004,-3.98264 -1.680988,-4.26518 -0.498515,-0.086 -0.791821,-0.13465 -1.013652,-0.13612 -0.488077,-0.003 -0.630564,0.22146 -1.855434,0.78649 -0.149053,0.19459 -0.449731,0.30849 -0.447472,0.58413 3.59e-4,0.0398 0.02378,0.0544 0.05884,0.0527 -1.590929,0.88272 -3.262199,1.61354 -4.622332,3.04686 -0.237509,0.34026 -0.451613,0.64465 -0.651852,0.91844 -0.471727,0.0671 -0.945897,0.11474 -1.423447,0.1356 -2.45827,0.10736 -3.370512,2.38725 -2.762767,3.91679 -0.06302,0.14867 -0.123936,0.29846 -0.18322,0.44957 -4.923042,-2.5556 -9.362251,-5.94636 -13.117591,-10.02041 -0.04384,-0.19277 -0.08319,-0.37973 -0.129028,-0.56796 -0.185578,-0.76433 -0.337024,-1.66895 -0.337024,-2.01055 0,-0.53177 0.04873,-0.66972 0.33857,-0.96016 0.227544,-0.22797 0.455709,-0.339 0.695208,-0.339 0.325203,0 0.356585,-0.0333 0.355086,-0.3776 -0.002,-0.568 -0.267715,-1.45083 -0.685401,-2.27758 -0.205691,-0.40709 -0.446806,-1.08618 -0.536242,-1.50882 -0.210095,-0.99149 -0.211342,-3.03089 -0.0021,-3.81822 0.178453,-0.67155 0.577412,-1.42355 0.832493,-1.56985 0.291634,-0.16724 0.503217,-0.89252 0.577532,-1.97717 0.06692,-0.97578 0.05223,-1.08897 -0.177543,-1.37844 -0.319778,-0.40267 -2.23994,-1.69347 -2.650253,-1.78159 -1.068627,-0.2294 -1.643828,-0.73737 -1.643828,-1.45145 0,-0.24295 -0.05243,-0.47375 -0.116127,-0.5132 -0.06362,-0.0393 -0.11561,-0.18734 -0.11561,-0.32805 0,-0.43466 -1.024771,-1.84179 -3.199399,-4.394 -1.188296,-1.39471 -1.300369,-1.68309 -1.16384,-2.99157 0.0798,-0.76558 -0.08518,-1.9705 -0.33754,-2.45908 -0.09508,-0.18423 -0.477297,-0.6713 -0.84901,-1.0822 -1.055413,-1.16665 -1.169518,-1.49686 -1.169518,-3.39056 v -1.59279 l -0.726174,-0.70095 c -0.313775,-0.30338 -0.743299,-0.63735 -1.147841,-0.91479 0.903594,-6.85102 3.291733,-13.42415 6.999042,-19.26319 0.292844,-0.12012 0.584764,-0.23118 0.80256,-0.3025 0.425275,-0.13925 0.830729,-0.32191 0.900621,-0.40628 0.06991,-0.0843 0.30183,-0.51263 0.515601,-0.9513 0.665981,-1.36652 1.139567,-1.60985 3.023918,-1.55315 3.0169,0.0907 3.185983,0.062 3.185983,-0.54293 0,-0.35638 0.196086,-0.73195 0.380892,-0.7312 0.0774,4.4e-4 0.522502,0.26121 0.989394,0.57996 l 0.848494,0.57943 h 1.019843 c 0.785962,0 1.054466,-0.0414 1.171069,-0.18202 0.08319,-0.10062 0.191114,-0.58384 0.239992,-1.07386 0.226226,-2.2691 0.4483,-3.31247 0.913525,-4.28864 0.251823,-0.52839 0.692546,-1.2124 0.979586,-1.51978 0.506274,-0.54225 0.79579,-0.70867 1.936467,-1.11298 1.124045,-0.39834 1.477026,-0.91483 1.608732,-2.35581 0.136988,-1.49892 0.669824,-2.35144 1.584474,-2.5347 0.257307,-0.0517 0.418977,6.2e-4 0.675595,0.22322 0.290224,0.25023 0.336386,0.3696 0.327216,0.85221 -0.007,0.35528 -0.147994,0.88001 -0.384506,1.43111 -0.471966,1.09978 -0.711797,2.1103 -0.599208,2.52531 l 0.08671,0.31814 1.09107,-0.008 c 1.333413,-0.0122 1.994432,-0.19517 3.974603,-1.10567 1.850406,-0.85079 2.532364,-1.05397 3.39346,-1.00919 l 0.673532,0.0355 0.0351,0.86524 c 0.01908,0.47586 0.09558,0.93928 0.170318,1.02952 0.195945,0.23653 1.412407,0.11616 2.033495,-0.20131 1.195948,-0.61118 1.92392,-1.60756 2.156847,-2.95141 0.15983,-0.92197 0.536012,-1.79623 0.924364,-2.14876 0.171174,-0.15532 0.643871,-0.27765 1.631441,-0.42245 2.383883,-0.3495 4.142863,-0.976 5.096126,-1.81445 0.884506,-0.77798 1.312329,-2.02535 0.940876,-2.74488 -0.09528,-0.18456 -0.152291,-0.38116 -0.174963,-0.57631 l 5.18e-4,-10e-4 c 3.888012,0.14093 7.744284,0.75433 11.483051,1.82697 0.678359,0.81388 1.155521,1.50675 1.245387,1.84104 0.171234,0.63701 -0.08959,1.40114 -0.853652,2.5008 -0.536759,0.77249 -0.65263,1.03382 -0.767465,1.73622 -0.202795,1.24073 -0.303485,1.35038 -1.244872,1.34819 -0.42862,-6.5e-4 -0.857169,-0.0514 -0.952749,-0.11161 -0.130416,-0.0825 -0.188279,-0.42245 -0.231735,-1.36227 l -0.05781,-1.25171 -0.433538,-0.21226 c -0.238199,-0.11665 -0.889971,-0.25794 -1.448218,-0.31449 -0.558289,-0.0563 -1.327604,-0.18876 -1.709893,-0.29363 -1.351271,-0.3707 -2.415043,-0.06 -3.652547,1.06708 -0.811481,0.73905 -1.386859,1.13021 -1.850276,1.25691 -0.659281,0.18029 -1.594711,0.5885 -2.045365,0.89341 -0.671446,0.45429 -0.966683,1.11116 -0.966683,2.14823 0,0.89222 0.213327,1.35547 0.83817,1.81914 0.429152,0.31849 1.543446,0.40063 2.027818,0.14969 0.44585,-0.23097 0.536181,-0.13378 0.72153,0.77136 0.170236,0.83156 0.446315,1.06338 1.06268,0.89288 0.872394,-0.24129 1.192788,-0.98165 1.161778,-2.68229 -0.02457,-1.36027 0.128346,-1.91133 0.727721,-2.62493 0.395983,-0.47137 0.655026,-0.47503 1.042038,-0.0146 0.690353,0.82188 0.703468,0.88631 0.680755,3.46775 -0.02407,2.75751 -0.0777,3.24083 -0.393795,3.58091 -0.210964,0.22687 -0.350101,0.2493 -1.562796,0.2493 -1.168054,0 -1.399324,-0.0338 -1.878662,-0.27433 -0.66854,-0.33526 -1.488431,-0.44699 -2.371551,-0.32284 -0.626003,0.0879 -0.705741,0.13721 -1.312998,0.80944 -0.409314,0.45307 -0.738128,0.7167 -0.897008,0.71816 -0.138038,6.8e-4 -0.511839,0.15647 -0.830431,0.34579 -0.318589,0.18932 -0.631475,0.34506 -0.695207,0.34578 -0.06362,6.8e-4 -0.272322,-0.12387 -0.463471,-0.27694 -0.316152,-0.2532 -0.352477,-0.35567 -0.405149,-1.13123 -0.05583,-0.82121 -0.06971,-0.85356 -0.368509,-0.88819 -0.445218,-0.0517 -0.544344,-0.2279 -0.732366,-1.29916 -0.283286,-1.61409 -0.729266,-2.20623 -1.467319,-1.94849 -0.31796,0.11092 -1.911783,1.88618 -3.018242,3.36135 -0.454739,0.60625 -0.573765,1.1901 -0.298831,1.46554 0.220293,0.22068 0.842319,0.16766 1.077133,-0.0923 0.115335,-0.12763 0.246673,-0.2326 0.291606,-0.2326 0.04504,0 0.112547,0.30054 0.150189,0.66757 0.108743,1.05797 0.589479,1.28239 1.532864,0.71556 0.421308,-0.25317 1.202096,-0.3951 1.609765,-0.29259 0.376156,0.0947 0.687766,0.56234 0.586822,0.88089 -0.175248,0.55317 -0.499809,0.69782 -1.563315,0.69782 -0.621479,0 -1.049915,0.0522 -1.136486,0.13925 -0.16509,0.16533 -0.187936,1.5056 -0.02838,1.66529 0.203243,0.20362 0.675146,1.61373 0.716885,2.14146 l 0.04129,0.52206 -0.679207,0.0349 c -0.483733,0.0247 -0.735113,-0.0152 -0.873269,-0.1403 -0.266875,-0.24191 -0.989033,-0.53249 -1.322804,-0.53249 -0.205022,0 -0.366012,0.14501 -0.626563,0.56483 -0.270771,0.43639 -0.470306,0.61144 -0.874816,0.76615 l -0.524372,0.20027 -0.250318,1.00397 c -0.137628,0.55223 -0.328799,1.1259 -0.42528,1.27465 -0.09648,0.1488 -0.147482,0.34287 -0.113544,0.43131 0.03386,0.0883 0.493939,0.37725 1.022424,0.6415 1.358323,0.67913 1.481768,0.81422 1.481768,1.61887 0,0.5394 -0.05883,0.74074 -0.327732,1.11141 -0.516482,0.71206 -1.541438,1.60194 -3.848158,3.34205 -1.36798,1.03189 -2.575883,2.05956 -3.354234,2.85388 -0.66906,0.68274 -1.464845,1.40911 -1.768213,1.61418 -0.744234,0.50315 -1.112354,0.91821 -1.245386,1.40452 -0.06112,0.22337 -0.158995,1.50301 -0.217801,2.84345 -0.0779,1.77565 -0.156638,2.55412 -0.291089,2.86848 -0.392368,0.91744 -0.209424,2.8223 0.471728,4.90668 1.633228,4.99761 3.779534,6.95201 7.627157,6.94539 1.446331,-9.8e-4 2.281933,-0.12583 4.749811,-0.69991 0.955788,-0.22235 2.166257,-0.4335 2.69051,-0.46886 l 0.953265,-0.0641 0.437148,0.45687 c 0.24035,0.2513 0.593357,0.71548 0.784496,1.03161 0.419932,0.69436 0.787317,0.84916 2.039172,0.85846 0.99425,0.007 1.561891,0.1633 1.728473,0.47513 0.06242,0.11674 0.113543,0.5324 0.113543,0.92313 0,0.58576 -0.06951,0.84128 -0.396892,1.45928 -0.316322,0.59706 -0.377987,0.81532 -0.303992,1.07385 0.123646,0.43182 0.437566,1.04397 0.755077,1.4718 0.142101,0.1915 0.322833,0.54601 0.402055,0.78805 0.0792,0.24204 0.746518,1.3388 1.482283,2.43717 1.603225,2.39338 2.113469,3.40556 2.200199,4.36375 0.05903,0.65209 0.03935,0.72466 -0.320507,1.19486 -0.210645,0.27525 -0.564625,0.69411 -0.787075,0.93095 -0.379264,0.40372 -0.414166,0.50812 -0.562051,1.6825 -0.243632,1.93441 -0.213822,2.0291 1.061134,3.3723 l 1.083843,1.14166 0.03871,1.44467 0.03922,1.44467 0.456247,0.73329 c 0.439498,0.70708 0.456983,0.77806 0.520245,1.99438 0.06182,1.19068 0.08469,1.28105 0.400504,1.61105 0.29442,0.30764 0.511944,0.41358 0.939846,0.45792 0.06382,0.005 0.45498,-0.0325 0.869139,-0.0866 0.527228,-0.0688 1.176414,-0.29532 2.165621,-0.75624 1.018879,-0.47476 1.768005,-0.73401 2.68638,-0.92887 0.700422,-0.14861 1.456902,-0.38332 1.680989,-0.52206 0.17596,-0.10892 0.576842,-0.48065 0.981651,-0.90071 l 0.112516,0.26756 c -0.03865,-0.0901 -0.07412,-0.17928 -0.111483,-0.26912 0.110282,-0.11445 0.222003,-0.23139 0.32825,-0.34891 1.123607,-1.24263 1.88354,-1.75025 2.848958,-1.90155 0.004,-6.5e-4 0.0085,-10e-4 0.01238,-0.002 a 21.077885,21.299547 0 0 1 -2.615672,-10.22954 21.077885,21.299547 0 0 1 18.707115,-21.13815 c 0.0327,-0.10526 0.0651,-0.21021 0.0893,-0.30771 0.23152,-0.9319 0.22108,-1.88237 -0.0222,-2.03297 -0.22129,-0.13697 -1.86461,0.43429 -2.68638,0.93408 -1.611877,0.98031 -2.310068,1.08644 -3.323783,0.50538 -1.414391,-0.81071 -4.062973,-4.0214 -7.298905,-8.85007 -1.500465,-2.23899 -2.41911,-4.18903 -2.11659,-4.49205 0.194896,-0.19525 0.512089,-0.0351 1.79092,0.90331 0.657084,0.48215 1.501126,1.032 1.875564,1.22198 1.419695,0.72034 2.350829,1.85821 4.422594,5.40423 1.936166,3.31389 2.521551,3.91353 3.38417,3.4667 0.88647,-0.4592 2.40572,-2.53716 4.54852,-6.22254 0.64941,-1.11687 1.28883,-2.18204 1.42087,-2.3678 0.14265,-0.20069 0.26053,-0.58607 0.29057,-0.94869 0.049,-0.59127 0.0344,-0.62815 -0.4743,-1.17138 -0.5454,-0.58267 -1.54358,-1.24884 -2.0588,-1.37375 -0.26362,-0.064 -0.3234,-0.0159 -0.51766,0.41567 -0.27092,0.60182 -0.60353,1.03579 -0.79378,1.03579 -0.24073,0 -2.808742,-1.41852 -3.539003,-1.95475 -0.810191,-0.59493 -1.231672,-1.17278 -1.336741,-1.83166 -0.06802,-0.4273 -0.04285,-0.50532 0.236381,-0.72546 0.256778,-0.20238 0.471233,-0.24617 1.193775,-0.24617 0.72504,0 1.177708,0.0939 2.561998,0.53249 0.92476,0.29309 2.09007,0.68487 2.58935,0.87046 0.49923,0.18556 1.06202,0.33744 1.25106,0.33744 0.18899,0 0.70853,-0.18919 1.15455,-0.42037 0.6978,-0.36154 0.89141,-0.41261 1.38577,-0.36508 0.68927,0.0664 1.00683,0.3228 1.10088,0.88871 0.21661,1.30393 0.38827,1.64497 1.22629,2.43248 0.92412,0.8685 1.21296,1.31499 1.70318,2.6291 0.61734,1.65483 1.95492,4.77632 2.49749,5.82877 0.60097,1.16559 1.54696,2.28178 1.87969,2.2176 0.0607,-0.012 0.10657,-0.0518 0.14813,-0.14551 -0.0201,2.04357 -0.17289,4.08361 -0.45574,6.10727 a 21.077885,21.299547 0 0 1 7.87902,7.2119 c 1.04552,-4.29528 1.60356,-8.78065 1.60357,-13.39636 1.3e-4,-31.27376 -25.43684,-56.62615 -56.814509,-56.62603 z m 12.833729,36.6843 1.236096,0.0396 1.236098,0.0386 0.747849,0.64254 c 0.411474,0.35338 0.877073,0.72388 1.034298,0.82299 0.379662,0.23927 0.565661,0.59806 0.565661,1.09107 0,0.59365 -0.519088,1.0769 -1.272739,1.18495 -0.30414,0.0435 -0.656577,0.14761 -0.782948,0.23156 -0.622028,0.41326 -1.170551,1.58915 -1.170551,2.50915 0,0.41493 0.05463,0.57875 0.318444,0.94868 0.358101,0.50226 0.68866,0.64133 1.144227,0.48191 0.283594,-0.0992 0.319111,-0.0836 0.414441,0.1815 0.05723,0.15921 0.127239,0.78575 0.156383,1.39252 0.05243,1.08797 0.04854,1.11723 -0.283864,2.1112 -0.185306,0.55434 -0.407824,1.1029 -0.494953,1.21885 -0.154047,0.20496 -0.177775,0.20213 -0.863462,-0.0991 -0.387743,-0.17034 -0.916365,-0.45679 -1.17468,-0.63681 -0.340001,-0.23695 -0.729111,-0.37616 -1.410026,-0.50433 -1.146969,-0.21593 -1.319345,-0.14434 -1.650539,0.68844 -0.187664,0.47185 -0.232224,0.76935 -0.262185,1.7602 -0.01968,0.65587 -0.07651,1.25704 -0.126449,1.33515 -0.110931,0.17324 -0.892172,0.18922 -1.025006,0.0209 -0.05263,-0.0666 -0.396562,-0.21925 -0.76385,-0.33848 -1.012209,-0.32858 -2.451075,-1.18001 -2.975407,-1.76126 -0.534257,-0.59223 -0.788686,-0.67419 -1.592216,-0.51111 -0.328137,0.0666 -0.656892,0.0917 -0.730819,0.0558 -0.384768,-0.18709 -0.734588,-2.30214 -0.53418,-3.22992 0.122147,-0.56569 0.06252,-0.68114 -0.353021,-0.68114 -0.338976,0 -0.966686,0.36111 -0.966686,0.55597 0,0.10859 -1.068849,0.99235 -1.29958,1.07438 -0.09858,0.0352 -0.950022,0.0755 -1.892078,0.0908 l -1.297,0.0219 c -0.432145,-0.33251 -0.873793,-0.66756 -1.327964,-1.00501 l -0.0046,-0.74216 -0.0031,-0.74163 0.395859,-0.0219 c 0.662018,-0.0362 0.914183,-0.30538 1.55196,-1.65433 0.41376,-0.87507 0.674422,-1.30024 0.915588,-1.49318 0.185489,-0.14835 0.4923,-0.53693 0.681273,-0.86315 0.352718,-0.60895 0.989933,-1.20124 1.815694,-1.68772 l 0.468634,-0.27537 0.244638,0.22844 c 0.134573,0.1259 0.408186,0.53126 0.607984,0.9007 0.392577,0.72592 0.484417,0.78613 1.653118,1.08585 0.405771,0.10407 0.657198,0.24234 0.828882,0.45531 0.228252,0.28314 0.246187,0.3724 0.246187,1.21519 0,1.14429 0.0794,1.40595 0.507859,1.66894 0.439556,0.26984 1.429471,0.30133 1.555054,0.0501 0.04684,-0.0937 0.08611,-0.63621 0.08671,-1.20633 6.19e-4,-0.76604 0.03496,-1.07233 0.131092,-1.17347 0.178975,-0.18821 0.446688,-0.0576 0.717918,0.351 0.192798,0.29035 0.227607,0.45671 0.227607,1.07803 0,0.63432 0.03096,0.77328 0.226574,1.02118 0.186245,0.23603 0.278188,0.27325 0.515599,0.20862 0.158831,-0.0431 0.353708,-0.12184 0.433539,-0.17472 0.263638,-0.17475 0.447988,-0.85619 0.427342,-1.58027 -0.01458,-0.50629 0.01947,-0.73377 0.133674,-0.89341 0.198481,-0.27796 0.739292,-0.38571 1.101906,-0.21957 0.512776,0.23499 0.6662,-0.18502 0.83456,-2.29427 0.114965,-1.44048 0.290116,-2.05153 0.734949,-2.56077 z m 25.550816,25.83934 v 5.2e-4 a 19.745815,19.721118 0 0 0 -19.745549,19.72111 19.745815,19.721118 0 0 0 19.745549,19.72111 19.745815,19.721118 0 0 0 10.41985,-2.99731 l 9.36079,8.68109 c 2.01285,1.86676 5.23512,1.84956 7.2251,-0.0391 1.98999,-1.88822 1.97153,-4.91121 -0.0412,-6.77797 l -9.72464,-9.01905 a 19.745815,19.721118 0 0 0 2.50625,-9.56927 19.745815,19.721118 0 0 0 -19.74606,-19.72111 z m 0,7.61557 a 12.121193,12.106032 0 0 1 12.12149,12.10606 12.121193,12.106032 0 0 1 -12.12149,12.10606 12.121193,12.106032 0 0 1 -12.120976,-12.10606 12.121193,12.106032 0 0 1 12.120976,-12.10606 z m -51.745743,31.68062 c -0.003,0.18118 5.31e-4,0.34789 0.0093,0.5085 -0.125331,-0.0314 -0.250661,-0.063 -0.375734,-0.0954 0.124426,-0.1335 0.246623,-0.27097 0.366443,-0.41306 z m 3.963252,0.11317 c 0.232866,0.37299 0.602516,0.81408 -0.133159,0.72703 -0.153797,-0.0184 0.04136,-0.40081 0.06193,-0.60133 0.02377,-0.042 0.04745,-0.0837 0.07122,-0.1257 z" id="path6853-0" inkscape:connector-curvature="0" /> </g> </svg>{{pluckcom('Travel_guides_menu_title_',item.component)}}</a></li>
        <li><a href="/Nirvana/events.html"><svg xmlns:osb="http://www.openswatchbook.org/uri/2009/osb" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46667" version="1.1" id="svg8" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" sodipodi:docname="Events_Logo.svg"> <defs id="defs2"> <linearGradient osb:paint="solid" id="linearGradient6884"> <stop id="stop6882" offset="0" style="stop-color:#f10000;stop-opacity:1;" /> </linearGradient> <linearGradient osb:paint="solid" id="linearGradient6864"> <stop id="stop6862" offset="0" style="stop-color:#e50000;stop-opacity:1;" /> </linearGradient> </defs> <sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.65738834" inkscape:cx="-119.9205" inkscape:cy="256.10316" inkscape:document-units="mm" inkscape:current-layer="layer2" showgrid="false" showborder="true" units="px" inkscape:object-nodes="false" inkscape:object-paths="false" inkscape:snap-intersection-paths="true" inkscape:snap-smooth-nodes="true" inkscape:snap-nodes="true" inkscape:window-width="1366" inkscape:window-height="705" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" showguides="true" inkscape:guide-bbox="true" inkscape:pagecheckerboard="true" /> <metadata id="metadata5"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Outer Border" inkscape:groupmode="layer" id="layer1" transform="translate(-15.083317,-131.65091)" style="display:inline;opacity:1"> <rect y="204.36469" x="128.77872" height="10.021408" width="0" id="rect4702" style="opacity:0.328;stroke-width:0.26458332" /> </g> <g inkscape:groupmode="layer" id="layer2" inkscape:label="Square" style="display:inline" transform="translate(-15.083317,29.882414)"> <g id="g1392"> <g style="display:inline;opacity:1;" inkscape:label="Layer 1" id="layer1-7" transform="translate(23.141937,-185.98952)"> <g transform="matrix(1.0281448,0,0,1.0281447,-9.4986227,-13.899648)" id="layer1-4" inkscape:label="Layer 1"> <path inkscape:connector-curvature="0" id="path2255" d="M 67.27977,165.35295 A 65.879319,65.879319 0 0 0 1.4005815,231.23213 65.879319,65.879319 0 0 0 67.27977,297.11131 65.879319,65.879319 0 0 0 133.15894,231.23213 65.879319,65.879319 0 0 0 67.27977,165.35295 Z m 0,5.06739 A 60.81168,60.81168 0 0 1 128.09155,231.23213 60.81168,60.81168 0 0 1 67.27977,292.04392 60.81168,60.81168 0 0 1 6.4679725,231.23213 60.81168,60.81168 0 0 1 67.27977,170.42034 Z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /> </g> </g> <path inkscape:connector-curvature="0" id="rect862" d="m 54.42565,-1.0412447 c -1.83652,0 -3.3156,1.47821103 -3.3156,3.314731 v 4.3963412 h -8.9185 c -3.20424,-3.3e-6 -5.78374,2.5794851 -5.78374,5.7837425 v 58.505772 c 0,3.204261 2.5795,5.783741 5.78374,5.783741 h 81.25021 c 3.20425,0 5.78373,-2.57948 5.78373,-5.783741 V 12.45357 c 0,-3.2042574 -2.57948,-5.7837425 -5.78373,-5.7837425 h -8.97441 V 2.5687873 c 0,-1.83651797 -1.4782,-3.31472997 -3.31473,-3.31472997 -1.83653,0 -3.31561,1.478212 -3.31561,3.31472997 V 6.6698275 H 57.74038 V 2.2734863 c 0,-1.83651997 -1.47821,-3.314731 -3.31473,-3.314731 z M 44.54174,21.242759 h 75.78186 l 0.25774,26.04955 h 0.17735 v 16.551807 c -0.23672,3.014678 -2.21636,5.421536 -4.73532,5.821308 h -13.3096 v 0.193961 l -53.00423,0.06291 c -2.57958,-0.233128 -4.64759,-2.172856 -4.99131,-4.651456 z m 20.992709,3.763798 c -0.546678,0 -0.986377,0.74333 -0.986377,1.666978 v 3.931545 c 0,0.923645 0.439699,1.666975 0.986377,1.666975 h 7.43412 c 0.546679,0 0.986379,-0.74333 0.986379,-1.666975 v -3.931545 c 0,-0.923648 -0.4397,-1.666978 -0.986379,-1.666978 z m 27.447436,0.147645 c -0.546678,0 -0.986382,0.743332 -0.986382,1.666974 v 3.930674 c 0,0.923643 0.439704,1.667848 0.986382,1.667848 H 100.416 c 0.54668,0 0.98726,-0.744205 0.98726,-1.667848 v -3.930674 c 0,-0.923642 -0.44058,-1.666974 -0.98726,-1.666974 z m 12.986335,0 c -0.54668,0 -0.98638,0.743327 -0.98638,1.666974 v 3.930674 c 0,0.923643 0.4397,1.667848 0.98638,1.667848 h 7.43412 c 0.54667,0 0.98636,-0.744205 0.98636,-1.667848 v -3.930674 c 0,-0.923647 -0.43969,-1.666974 -0.98636,-1.666974 z m -26.867312,0.167749 c -0.546679,0 -0.986383,0.74333 -0.986383,1.666976 v 3.931546 c 0,0.923647 0.439704,1.666975 0.986383,1.666975 h 7.434114 c 0.546679,0 0.987256,-0.743328 0.987256,-1.666975 v -3.931546 c 0,-0.923646 -0.440577,-1.666976 -0.987256,-1.666976 z M 51.83782,35.742296 c -0.54668,0 -0.98638,0.743314 -0.98638,1.666977 v 3.931545 c 0,0.923646 0.4397,1.666975 0.98638,1.666975 h 7.434114 c 0.546682,0 0.987259,-0.743329 0.987259,-1.666975 v -3.931545 c 0,-0.923663 -0.440577,-1.666977 -0.987259,-1.666977 z m 13.696629,0.10834 c -0.546678,0 -0.986377,0.743313 -0.986377,1.666976 v 3.931546 c 0,0.923645 0.439699,1.666976 0.986377,1.666976 h 7.43412 c 0.546679,0 0.986379,-0.743331 0.986379,-1.666976 v -3.931546 c 0,-0.923663 -0.4397,-1.666976 -0.986379,-1.666976 z m 27.447436,0.147644 c -0.546678,0 -0.986382,0.74333 -0.986382,1.666977 v 3.931545 c 0,0.923646 0.439704,1.666977 0.986382,1.666977 H 100.416 c 0.54668,0 0.98726,-0.743331 0.98726,-1.666977 v -3.931545 c 0,-0.923647 -0.44058,-1.666977 -0.98726,-1.666977 z m 12.986335,0 c -0.54668,0 -0.98638,0.74333 -0.98638,1.666977 v 3.931545 c 0,0.923646 0.4397,1.666977 0.98638,1.666977 h 7.43412 c 0.54667,0 0.98636,-0.743331 0.98636,-1.666977 v -3.931545 c 0,-0.923647 -0.43969,-1.666977 -0.98636,-1.666977 z m -26.867312,0.167748 c -0.546679,0 -0.986383,0.744205 -0.986383,1.66785 v 3.930672 c 0,0.923645 0.439704,1.667849 0.986383,1.667849 h 7.434114 c 0.546679,0 0.987256,-0.744204 0.987256,-1.667849 v -3.930672 c 0,-0.923645 -0.440577,-1.66785 -0.987256,-1.66785 z M 51.83782,46.586372 c -0.54668,0 -0.98638,0.743332 -0.98638,1.666978 v 3.931546 c 0,0.923643 0.4397,1.666973 0.98638,1.666973 h 7.434114 c 0.546682,0 0.987259,-0.74333 0.987259,-1.666973 V 48.25335 c 0,-0.923646 -0.440577,-1.666978 -0.987259,-1.666978 z m 13.696629,0.108339 c -0.546678,0 -0.986377,0.743332 -0.986377,1.666975 v 3.931549 c 0,0.923647 0.439699,1.666975 0.986377,1.666975 h 7.43412 c 0.546679,0 0.986379,-0.743328 0.986379,-1.666975 v -3.931547 c 0,-0.923644 -0.4397,-1.666973 -0.986379,-1.666973 z m 27.447436,0.147645 c -0.546678,0 -0.986382,0.743332 -0.986382,1.666975 v 3.931546 c 0,0.923648 0.439704,1.666978 0.986382,1.666978 H 100.416 c 0.54668,0 0.98726,-0.74333 0.98726,-1.666978 v -3.931542 c 0,-0.923646 -0.44058,-1.666975 -0.98726,-1.666975 z m 12.986335,0 c -0.54668,0 -0.98638,0.743314 -0.98638,1.666975 v 3.931546 c 0,0.923648 0.4397,1.666978 0.98638,1.666978 h 7.43412 c 0.54667,0 0.98636,-0.74333 0.98636,-1.666978 v -3.931542 c 0,-0.923664 -0.43969,-1.666975 -0.98636,-1.666975 z m -26.867312,0.168628 c -0.546679,0 -0.986383,0.743328 -0.986383,1.666975 v 3.93067 c 0,0.923652 0.439704,1.667854 0.986383,1.667854 h 7.434114 c 0.546679,0 0.987256,-0.744202 0.987256,-1.667854 v -3.930667 c 0,-0.923646 -0.440577,-1.666973 -0.987256,-1.666973 z M 51.44991,57.398127 c -0.54668,0 -0.98638,0.743315 -0.98638,1.666978 v 3.931545 c 0,0.923645 0.4397,1.666975 0.98638,1.666975 h 7.434114 c 0.546678,0 0.986382,-0.74333 0.986382,-1.666975 v -3.931542 c 0,-0.923663 -0.439704,-1.666978 -0.986382,-1.666978 z m 13.566451,0.315397 c -0.546677,0 -0.986383,0.743328 -0.986383,1.66698 v 3.931544 c 0,0.923645 0.439706,1.666975 0.986383,1.666975 h 7.434116 c 0.546677,0 0.986382,-0.74333 0.986382,-1.666975 v -3.93154 c 0,-0.923649 -0.439705,-1.666979 -0.986382,-1.666979 z" style="opacity:1;fill-opacity:1;stroke-width:0.44732258" /> </g> </g> <g inkscape:groupmode="layer" id="layer3" inkscape:label="Small Square" style="display:inline" transform="translate(-15.083317,29.882414)" /> </svg>{{pluckcom('Events_menu_title',item.component)}}</a></li>
        <li><a href="/Nirvana/package.html"><svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46667" version="1.1" id="svg8" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" sodipodi:docname="travel_final.svg"> <defs id="defs2"> <clipPath clipPathUnits="userSpaceOnUse" id="clipPath51"> <rect style="fill-rule:evenodd" id="rect53" width="499.8577" height="227.66798" x="1.0118577" y="99.162056" /> </clipPath> </defs> <sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.9296875" inkscape:cx="256" inkscape:cy="256" inkscape:document-units="mm" inkscape:current-layer="layer1" showgrid="false" units="px" inkscape:object-nodes="true" showguides="true" inkscape:guide-bbox="true" inkscape:window-width="1366" inkscape:window-height="705" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" inkscape:pagecheckerboard="true" inkscape:snap-nodes="false" inkscape:snap-global="false" /> <metadata id="metadata5"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(0,-161.53331)"> <g id="g332"> <g inkscape:label="Layer 1" id="layer1-4" transform="matrix(1.0281448,0,0,1.0281447,-0.87081532,-7.9042566)"> <path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="M 67.27977,165.35295 A 65.879319,65.879319 0 0 0 1.4005815,231.23213 65.879319,65.879319 0 0 0 67.27977,297.11131 65.879319,65.879319 0 0 0 133.15894,231.23213 65.879319,65.879319 0 0 0 67.27977,165.35295 Z m 0,5.06739 A 60.81168,60.81168 0 0 1 128.09155,231.23213 60.81168,60.81168 0 0 1 67.27977,292.04392 60.81168,60.81168 0 0 1 6.4679725,231.23213 60.81168,60.81168 0 0 1 67.27977,170.42034 Z" id="path2255" inkscape:connector-curvature="0" /> </g> <path inkscape:connector-curvature="0" id="rect892" d="m 25.160057,205.20891 1.970354,4.10538 8.723695,-4.18689 -1.970354,-4.10538 z" style="stroke-width:0.26458332" /> <path inkscape:transform-center-y="-1.4229692" inkscape:transform-center-x="-3.9843138" style="stroke-width:0.26458332" d="m 10.777633,218.19516 3.22213,3.21781 6.837665,-6.84684 -3.22213,-3.21781 z" id="path898" inkscape:connector-curvature="0" /> <path inkscape:connector-curvature="0" id="path900" d="m 102.63388,206.74955 1.97035,-4.10538 8.7237,4.18689 -1.97035,4.10538 z" style="stroke-width:0.26458332" /> <path style="stroke-width:0.23867022" d="m 117.03215,216.70592 2.64582,-3.52482 5.71527,5.93765 -2.64582,3.52482 z" id="path902" inkscape:connector-curvature="0" /> <path sodipodi:nodetypes="cccccccsscccscsccccccccccc" inkscape:connector-curvature="0" id="path68" d="m 61.488706,219.9099 c -0.282942,-0.26853 -0.280154,-0.2836 0.532027,-2.85729 1.302494,-7.01015 1.633452,-12.1821 0.923637,-13.10683 -0.411489,-0.53614 -0.168253,-0.43641 -4.435752,-0.37303 -4.993995,0.0741 -5.072246,0.0977 -7.147636,2.15184 -1.563263,1.54716 -2.227665,1.94382 -2.682326,1.60133 -0.888006,-0.66895 -1.177125,-2.55644 -1.172871,-7.65691 0.0038,-3.94843 0.143346,-5.48912 0.566912,-6.23657 0.556192,-0.98147 1.167344,-0.71864 3.624063,1.55861 2.177829,2.01873 2.964818,2.48379 4.558997,2.69405 3.613583,0.47665 5.907299,0.1264 6.89736,-1.23898 0.237362,-2.34546 0.418652,-3.1006 0.154661,-5.9631 -0.213647,-4.13064 -0.179589,-4.80783 -0.698767,-8.73054 -0.252302,-1.90629 1.417962,-1.65509 3.537639,0.65203 1.252812,1.36356 2.345,2.99813 4.642641,6.94813 2.864522,4.92454 4.029811,6.37124 5.689878,7.06381 0.862902,0.36001 2.575421,0.42112 5.429171,0.19378 3.032956,-0.24183 5.016272,0.4187 5.880132,1.9579 0.405467,0.7225 0.446326,0.92651 0.40072,2.0007 -0.05749,1.35437 -0.315259,1.81491 -1.269438,2.26671 -0.89047,0.4217 -1.93788,0.54811 -5.273431,0.6364 -4.833791,0.12782 -5.588073,0.31682 -7.035237,1.76176 -1.023044,1.02146 -1.98806,2.51731 -4.428876,6.86503 -3.176237,5.65782 -4.09782,6.9371 -5.417193,7.51989 -0.868443,0.38363 -2.981162,0.57151 -3.276311,0.29146 z" style="stroke-width:0.15312754" /> <path inkscape:connector-curvature="0" id="path307" d="m -88.508683,210.76804 c 0,0 -1.138375,11.66835 0,12.80672 1.138375,1.13838 34.720448,10.81457 40.412326,7.96863 5.691875,-2.84594 25.898038,-25.61344 18.498597,-26.75182 -7.399438,-1.13837 -43.82745,-1.70756 -43.82745,-1.70756 z" style="fill:none;stroke-width:0.26458332" /> <g transform="translate(0,-1.0583333)" id="g318"> <path style="stroke-width:0.32753712" d="M 67.702188,228.23687 A 46.204662,42.632331 0.68660245 0 0 21.62763,270.52609 l 6.367551,-0.005 a 39.707746,36.673705 0.67417453 0 1 39.586255,-36.20104 39.707746,36.673705 0.67417453 0 1 39.805544,36.13897 l 6.64466,-0.005 A 46.204662,42.632331 0.68660245 0 0 67.702188,228.23671 Z" id="path128" inkscape:connector-curvature="0" /> <path style="fill-opacity:1;stroke-width:0.30711973" d="m 54.619746,280.11627 c -3.611411,-0.58053 -5.961211,-1.33636 -11.260774,-3.6221 l -4.430012,-1.93013 0.247077,-2.03072 c 0.186835,-2.84058 0.143206,-3.90005 0.0813,-6.11524 l -0.01406,-3.83427 -0.710859,-0.73727 c -2.842089,-2.32311 -5.682036,-3.26068 -7.679536,-2.82546 -1.060033,0.20488 -1.191322,1.69273 -1.893338,3.98241 -0.533399,1.56373 -0.525305,1.9526 -0.702025,1.9331 -1.323612,-0.14596 -2.332303,-1.11775 -2.524724,-1.71037 -0.194019,-0.32232 -0.169784,-1.10677 0.0064,-2.51379 1.477058,-6.23652 1.469251,-8.48498 8.547516,-16.66896 2.557196,-2.95665 2.525997,-2.9742 8.518018,-7.19794 4.101208,-1.9272 4.212674,-2.03046 8.633197,-3.37056 l 8.54893,-1.10217 7.582608,-0.29188 c 7.697567,-0.0503 7.844977,-0.11022 17.098138,1.674 9.264728,4.48676 10.759893,4.49627 15.199911,9.0269 4.535587,4.01453 4.648097,5.66798 6.954927,9.88053 1.8195,5.47461 1.2333,3.45272 1.89444,5.80287 -0.0964,1.69505 -0.26165,2.24854 -0.82011,2.74512 -1.0856,0.96523 -1.56936,0.52847 -2.17639,-1.96501 -1.27637,-5.24267 -4.28101,-10.10724 -8.662474,-14.02455 -4.025085,-3.59869 -10.702995,-7.56439 -14.6094,-8.67585 -1.017898,-0.28959 -2.51027,-0.7482 -3.316402,-1.01907 -2.105481,-0.70753 -18.203157,-1.0087 -18.816096,-0.35204 -0.644922,0.69093 -2.435851,7.17076 -2.478597,8.9679 -0.03596,1.51078 0.05559,1.73552 1.10079,2.70343 1.498394,1.38757 2.269039,1.71696 5.322916,2.27517 2.728808,0.4988 4.495098,1.12405 5.146361,1.82175 0.232741,0.24932 0.503583,1.87227 0.642625,3.85072 0.29975,4.26504 0.86422,5.36143 4.708754,9.14616 1.50959,1.48611 2.744702,2.88734 2.744702,3.11387 0,0.51488 -1.485979,1.85953 -2.312868,2.09289 -0.340169,0.096 -0.618501,0.29153 -0.618501,0.43451 0,0.14299 -0.435465,0.701 -0.967709,1.23998 -1.826599,1.84967 -3.087086,4.75546 -3.760553,8.66912 l -0.186879,1.08597 -6.255123,-0.0262 c -3.819108,-0.0162 -7.239452,-0.18425 -8.782647,-0.4323 z" id="path878" inkscape:connector-curvature="0" sodipodi:nodetypes="cscccccccsccscccccccccsscccccccccscscscccc" /> <path style="stroke-width:0.26458332" d="m 39.255195,271.38212 -0.609235,5.84375 c 0,0 7.786646,9.50576 33.03091,7.60052 25.244255,-1.90523 -1.276995,-14.07191 -1.276995,-14.07191 z" id="path311" inkscape:connector-curvature="0" /> </g> </g> </g> </svg>{{pluckcom('Packages_menu_title',item.component)}}</a></li>
        <li><a href="/Nirvana/offers.html"><svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" version="1.1" id="svg2" width="512" height="512" viewBox="0 0 512 512" sodipodi:docname="Offer White New.svg" inkscape:version="0.92.4 (5da689c313, 2019-01-14)"><metadata id="metadata8"><rdf:RDF><cc:Work rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /><dc:title></dc:title></cc:Work></rdf:RDF></metadata><defs id="defs6" /><sodipodi:namedview borderopacity="1" objecttolerance="10" gridtolerance="10" guidetolerance="10" inkscape:pageopacity="0" inkscape:pageshadow="2" inkscape:window-width="1366" inkscape:window-height="705" id="namedview4" showgrid="false" inkscape:zoom="0.65186406" inkscape:cx="58.092532" inkscape:cy="242.71671" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" inkscape:current-layer="layer2" inkscape:pagecheckerboard="true" /><g inkscape:groupmode="layer" id="layer5" inkscape:label="Inner 3" /><g inkscape:groupmode="layer" id="layer4" inkscape:label="inner 2" /><g inkscape:groupmode="layer" id="layer3" inkscape:label="Inner" /><g inkscape:groupmode="layer" id="layer2" inkscape:label="Outer" style="display:inline"><g id="g1294"><g inkscape:label="Layer 1" id="layer1" transform="matrix(3.8872253,0,0,3.8859945,-5.4222514,-642.23081)"><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="M 67.27977,165.35295 A 65.879319,65.879319 0 0 0 1.4005815,231.23213 65.879319,65.879319 0 0 0 67.27977,297.11131 65.879319,65.879319 0 0 0 133.15894,231.23213 65.879319,65.879319 0 0 0 67.27977,165.35295 Z m 0,5.06739 A 60.81168,60.81168 0 0 1 128.09155,231.23213 60.81168,60.81168 0 0 1 67.27977,292.04392 60.81168,60.81168 0 0 1 6.4679725,231.23213 60.81168,60.81168 0 0 1 67.27977,170.42034 Z" id="path2255" inkscape:connector-curvature="0" /></g><g transform="translate(-7.9129803,-21.176722)" id="g1279"><g id="g158" transform="rotate(6.3275467,306.73838,399.25664)"><g transform="matrix(0.52441485,-0.12559829,0.12893762,0.52543277,569.74728,63.382243)" id="g28-2"><path inkscape:connector-curvature="0" id="path38-0" d="m -845.11814,502.94756 c -3.22842,-1.29151 -6.82013,-4.66785 -8.59552,-8.08011 -2.11099,-4.05726 -2.1054,-317.55829 0.006,-321.59932 1.89416,-3.6257 6.45575,-7.83374 9.90438,-9.13672 1.89028,-0.7142 25.59343,-1.06362 72.15105,-1.06362 h 69.33595 l 0.36214,-17.25 c 0.33834,-16.11655 0.53442,-17.61927 2.98411,-22.87001 4.73051,-10.13948 12.90984,-17.26759 22.85165,-19.91472 3.29636,-0.87769 23.08957,-1.19018 74,-1.16829 68.06578,0.0293 69.61514,0.0731 75.07935,2.12446 10.26291,3.85284 18.66316,13.15314 21.41183,23.70603 1.01597,3.90055 1.50882,10.62382 1.50882,20.58263 v 14.7899 h 69.84272 69.84271 l 4.40729,2.32502 c 2.74756,1.44945 5.2484,3.80312 6.64083,6.25 l 2.23355,3.92498 0.0164,158.60212 0.0165,158.60212 -2.25,3.30594 c -1.2375,1.81826 -3.52048,4.24731 -5.07328,5.39788 l -2.82328,2.09194 -240.67672,0.18984 c -158.54992,0.12506 -241.52963,-0.15136 -243.17621,-0.81007 z m 321.99949,-355.42943 c 0,-17.4708 -0.55639,-19.48481 -6.67128,-24.14864 l -3.67339,-2.8017 h -69.7545 c -69.21012,0 -69.77654,0.0163 -72.57767,2.09195 -1.55274,1.15057 -3.83566,3.57962 -5.07316,5.39789 -2.09968,3.08506 -2.25,4.36502 -2.25,19.15805 v 15.85211 h 80 80 z" /><path inkscape:connector-curvature="0" id="path36-6" d="m -845.61865,501.66855 c -3.76091,-1.66421 -5.99586,-4.14995 -7.96186,-8.85524 -1.36172,-3.25904 -1.5375,-21.37737 -1.53256,-157.9634 0.004,-103.04223 0.3486,-155.51725 1.03843,-158.00107 1.12625,-4.05526 4.93282,-9.11624 8.31385,-11.05359 1.61405,-0.92486 19.64521,-1.35072 73.14214,-1.72746 l 71,-0.5 0.5,-17.52591 c 0.49478,-17.34284 0.53433,-17.59561 3.78675,-24.19736 3.92854,-7.97416 10.0873,-13.76721 18.06616,-16.99338 l 5.64709,-2.28335 h 71 71 l 5.5,2.59017 c 6.73466,3.17161 11.17797,6.83886 14.87255,12.27495 4.80431,7.06889 5.4458,10.10334 6.05354,28.63488 l 0.57391,17.5 71,0.5 c 53.49693,0.37674 71.52808,0.8026 73.14214,1.72746 3.44073,1.97156 7.22297,7.05788 8.34394,11.22086 0.70265,2.60941 0.9538,54.11656 0.7763,159.20453 -0.25893,153.30686 -0.28865,155.39175 -2.26238,158.74262 -1.1,1.86751 -3.27048,4.34251 -4.82328,5.5 l -2.82328,2.10453 -240.67672,0.21414 c -198.21706,0.17636 -241.20597,-0.0201 -243.67672,-1.11338 z m 323.30502,-352.60076 c 0.17482,-8.31083 -0.15058,-16.06105 -0.76236,-18.15725 -1.29938,-4.45222 -6.65964,-9.36476 -11.76533,-10.78262 -5.13962,-1.42728 -131.36148,-1.44219 -136.49628,-0.0161 -4.76697,1.32392 -9.67796,5.46501 -11.41702,9.62719 -1.43045,3.42353 -2.02422,32.33529 -0.69145,33.66805 0.36992,0.36992 36.70742,0.55742 80.75,0.41667 l 80.07742,-0.25592 z" /><path inkscape:connector-curvature="0" id="path34-6" d="m -843.11865,501.90031 c -1.925,-0.54753 -4.5705,-1.97134 -5.87888,-3.16402 -5.3827,-4.90668 -5.12112,3.50445 -5.12112,-164.6685 0,-170.72531 -0.40192,-159.53091 5.94213,-165.5 l 3.18846,-3 71.10602,-0.26974 c 49.98144,-0.1896 71.50076,-0.59733 72.43471,-1.37244 1.00571,-0.83467 1.32868,-5.07459 1.32868,-17.44275 0,-13.98087 0.29357,-17.12469 2.03335,-21.77465 2.86169,-7.64855 8.73368,-13.9081 16.74826,-17.85367 l 6.67634,-3.28675 h 71.52102 71.52103 l 6.30048,2.90922 c 7.73281,3.57059 14.27642,10.50764 17.14577,18.17667 1.76392,4.7145 2.05375,7.79508 2.05375,21.82918 0,12.36816 0.32297,16.60808 1.32868,17.44275 0.93396,0.77511 22.45601,1.18284 72.44483,1.37244 l 71.11613,0.26974 3.58144,3.47162 c 1.96979,1.90938 4.03976,5.05938 4.59993,7 0.67767,2.34765 0.91986,55.95823 0.72374,160.20516 l -0.29475,156.67679 -2.10453,2.82321 c -1.15749,1.55277 -3.63249,3.72323 -5.5,4.82322 -3.36767,1.98363 -5.34373,2.00135 -241.39547,2.16402 -155.44333,0.10712 -239.21407,-0.18131 -241.5,-0.8315 z m 320.8,-338.03252 c 0.8217,-0.8217 1.2,-5.71825 1.2,-15.53211 0,-15.26701 -0.90809,-19.39789 -5.29773,-24.09925 -4.99175,-5.34623 -2.35626,-5.16864 -76.70227,-5.16864 -74.34601,0 -71.71052,-0.17759 -76.70227,5.16864 -4.38964,4.70136 -5.29773,8.83224 -5.29773,24.09925 0,9.81386 0.3783,14.71041 1.2,15.53211 1.73785,1.73785 159.86215,1.73785 161.6,0 z" /><path inkscape:connector-curvature="0" id="path32-2" d="m -843.78574,500.97468 c -3.4447,-1.00185 -8.02087,-5.7711 -8.86011,-9.23396 -0.28965,-1.19511 -0.40201,-72.90248 -0.24971,-159.34971 0.27655,-156.9737 0.27957,-157.18036 2.3387,-159.93822 1.13398,-1.51878 3.30421,-3.68923 4.82272,-4.82321 2.71983,-2.03109 3.82452,-2.0659 74.18822,-2.33748 55.4664,-0.21409 71.44704,-0.54927 71.51575,-1.5 0.0487,-0.67337 0.23703,-8.2402 0.41861,-16.81518 0.18157,-8.57498 0.82287,-17.42078 1.42511,-19.65733 2.55658,-9.49443 11.77404,-18.8992 21.29343,-21.72616 7.6918,-2.28422 139.85694,-2.28422 147.54874,0 9.49768,2.82051 18.73668,12.2311 21.27961,21.67483 0.59464,2.20832 1.10631,10.60412 1.13704,18.65733 0.0307,8.05321 0.27123,15.64319 0.53442,16.86662 l 0.47854,2.22442 71.32528,0.27558 c 70.26077,0.27147 71.3665,0.30635 74.08623,2.33737 1.51851,1.13398 3.68874,3.30443 4.82272,4.82322 2.05953,2.7584 2.06207,2.93487 2.31331,160.93821 0.24711,155.4026 0.21726,158.23284 -1.70226,161.37245 -1.07458,1.75761 -3.19294,4.00761 -4.70746,5 -2.63296,1.72523 -13.2336,1.81152 -241.79774,1.96833 -143.28676,0.0983 -240.31274,-0.20498 -242.21115,-0.75711 z M -520.72564,163.486 c 1.02085,-2.66028 0.66321,-27.67559 -0.45314,-31.69554 -1.30526,-4.7002 -6.36809,-10.58581 -10.64213,-12.37162 -4.76197,-1.98968 -137.83351,-1.98968 -142.59548,0 -4.27404,1.78581 -9.33687,7.67142 -10.64213,12.37162 -1.11635,4.01995 -1.47399,29.03526 -0.45314,31.69554 0.54665,1.42454 8.73734,1.58179 82.39301,1.58179 73.65567,0 81.84636,-0.15725 82.39301,-1.58179 z" /><path inkscape:connector-curvature="0" id="path30-3" d="m -844.99716,499.35788 c -1.85817,-0.9154 -4.21899,-3.04266 -5.24626,-4.72723 -1.8028,-2.95635 -1.86775,-8.53993 -1.86775,-160.56286 0,-154.35294 0.0389,-157.56385 1.94852,-160.69567 4.39344,-7.20545 -0.54977,-6.74839 78.99916,-7.30433 l 71.54484,-0.5 0.52487,-18.07154 c 0.52181,-17.96627 0.54414,-18.1107 3.83384,-24.79302 3.9209,-7.96447 10.57405,-13.79557 18.38812,-16.1161 7.655,-2.27329 139.85134,-2.27329 147.50634,0 7.81407,2.32053 14.46722,8.15163 18.38812,16.1161 3.2897,6.68232 3.31203,6.82675 3.83384,24.79302 l 0.52487,18.07154 71.54472,0.5 c 79.54878,0.55594 74.60586,0.0989 78.99928,7.30433 1.90959,3.13182 1.94852,6.34273 1.94852,160.69567 0,156.21224 -0.0165,157.52706 -2.01815,160.81018 -4.0664,6.66969 15.07587,6.19211 -247.19225,6.16705 -220.21715,-0.021 -238.53826,-0.14895 -241.66063,-1.68714 z m 324.96079,-335.44383 c 0.78142,-1.46009 1.02074,-7.06286 0.7431,-17.39647 -0.39801,-14.8138 -0.49278,-15.37715 -3.36746,-20.01863 -1.92948,-3.11535 -4.49029,-5.58518 -7.3652,-7.10352 l -4.40729,-2.32764 h -68.68543 -68.68543 l -4.40729,2.32764 c -2.87491,1.51834 -5.43572,3.98817 -7.3652,7.10352 -2.87468,4.64148 -2.96945,5.20483 -3.36746,20.01863 -0.27764,10.33362 -0.0383,15.93638 0.7431,17.39648 l 1.15265,2.15373 h 81.92963 81.92963 z" /></g><path style="stroke-width:0.19473971" d="m 311.93552,192.48934 c 0.36412,0.11529 0.86309,0.4599 1.10882,0.76579 0.43125,0.53683 0.6533,1.68154 6.38531,32.91781 5.81987,31.71501 5.93404,32.37628 5.71339,33.09458 -0.50763,1.65261 0.35197,1.36506 -13.73752,4.59543 l -12.67183,2.90533 0.58829,3.73374 c 0.58485,3.71198 0.58634,3.74253 0.25476,5.24442 -0.39519,1.79006 -1.35547,3.2488 -2.65404,4.0317 -1.27213,0.76696 -24.72122,5.94543 -26.16479,5.7782 -1.47356,-0.1707 -2.87356,-1.1082 -3.86935,-2.59107 -0.83549,-1.24416 -0.8449,-1.27296 -1.61487,-4.94406 l -0.77449,-3.69261 -12.70951,2.69985 c -14.13139,3.00191 -13.23737,2.90219 -14.28836,1.59378 -0.45681,-0.56869 -0.58478,-1.22691 -6.40465,-32.94192 -5.88997,-32.09705 -5.93662,-32.36785 -5.70535,-33.12085 0.46982,-1.52971 -2.90765,-0.68173 43.61462,-10.95031 39.06308,-8.62216 42.31772,-9.31356 42.92957,-9.11981 z m -44.99392,81.65344 c -0.0836,0.33062 0.0853,1.4912 0.52412,3.60358 0.62915,3.02821 0.66721,3.14025 1.35213,3.98133 0.45971,0.56453 1.00708,0.97169 1.57428,1.17105 l 0.86953,0.30562 12.18347,-2.69059 12.18348,-2.69059 0.69401,-0.6509 c 0.4527,-0.4246 0.81381,-1.03239 1.0386,-1.74808 0.33491,-1.0663 0.33048,-1.18576 -0.15748,-4.24516 -0.34038,-2.13413 -0.59408,-3.27596 -0.78774,-3.54536 l -0.28566,-0.39737 -14.53275,3.20939 -14.53274,3.2094 z" id="path30-3-6" inkscape:connector-curvature="0" /></g><path inkscape:connector-curvature="0" style="stroke-width:0.59603173" d="m 265.09734,75.506604 c -0.93957,-0.0021 -1.90039,0.11814 -2.79407,0.361668 l -2.86607,0.781744 -30.13889,28.333644 -30.13887,28.33365 -6.14066,-6.40369 c -4.13432,-4.31193 -7.13803,-7.02328 -9.19573,-8.29996 -5.56704,-3.45407 -13.05132,-4.07123 -19.07945,-1.57595 -3.20951,1.32855 -3.89625,1.93735 -33.28019,29.53775 -21.97798,20.64401 -30.39088,28.80811 -31.44896,30.52541 -3.191167,5.17936 -3.760537,11.58376 -1.592529,17.893 1.122706,3.26725 1.662089,3.9986 8.206729,11.11395 l 7.00454,7.61513 -29.920344,28.12857 c -20.090565,18.88762 -30.173498,28.65513 -30.692706,29.73121 -0.947235,1.9632 -1.168471,5.63599 -0.480659,7.97428 0.766486,2.60622 130.910459,138.34726 133.505719,139.24756 2.18269,0.75718 5.13555,0.76097 7.06484,0.0104 0.46136,-0.17947 9.29678,-8.30746 24.86221,-22.82663 -16.96584,8.832 -26.63281,13.78006 -27.0773,13.82559 -1.86919,0.19143 -4.44757,-0.52218 -6.16874,-1.70741 -2.04651,-1.40926 -82.47777,-151.14019 -82.50882,-153.59882 -0.0279,-2.2059 1.06598,-5.35656 2.37441,-6.84228 0.71718,-0.81436 11.91823,-6.91578 34.09565,-18.57207 l 33.02717,-17.36045 -4.25167,-8.3296 c -3.9738,-7.78206 -4.26586,-8.54706 -4.44613,-11.66816 -0.34811,-6.027 1.71912,-11.48004 5.77567,-15.23338 1.345,-1.24446 10.69329,-6.34859 34.94975,-19.08419 32.43019,-17.02711 33.17828,-17.39484 36.30743,-17.78313 5.87718,-0.7293 12.26464,1.60852 16.28148,5.96048 1.4847,1.60857 3.44398,4.69732 5.99909,9.4537 l 3.79572,7.06284 33.26891,-17.48476 24.0201,-12.62506 -53.05834,-55.32822 -2.59267,-0.793439 c -0.80834,-0.247279 -1.72705,-0.371345 -2.66662,-0.373417 z m -91.22706,50.933106 c 2.92742,0.12867 4.7412,1.70842 10.63413,7.85452 l 6.45606,6.73271 -34.52207,32.45428 -34.52207,32.45428 -6.57952,-6.86305 c -6.14118,-6.40505 -6.60799,-7.02132 -6.98268,-9.20888 -0.22082,-1.28929 -0.24477,-3.26754 -0.0524,-4.39562 0.34706,-2.03509 0.58604,-2.27201 30.45161,-30.34933 l 30.09883,-28.29761 2.74879,-0.27694 c 0.85778,-0.0865 1.59373,-0.13404 2.2693,-0.10436 z m 67.37397,22.721 -33.22733,17.46442 c -32.96778,17.32761 -33.23395,17.47699 -34.03572,19.16962 -0.44443,0.93827 -0.90781,2.67015 -1.03078,3.8484 -0.20865,1.99913 0.0474,2.6483 3.84279,9.71354 l 4.06716,7.5704 38.10744,-20.028 38.10749,-20.02994 -3.98904,-7.42565 c -4.48244,-8.34416 -5.2646,-9.16744 -9.37397,-9.86397 z m 127.59321,30.20278 14.05939,26.1673 12.63122,23.50639 6.63119,-6.24389 0.34888,-2.05182 c 0.19242,-1.12811 0.17039,-3.10431 -0.0505,-4.39362 l -0.40123,-2.34576 z" id="path38-0-6-7" /><path style="opacity:1;fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:2.51415491;stroke-opacity:1" d="m 153.37425,374.56221 a 29.487607,50.927267 50.206338 0 0 -14.95933,41.87304 29.487607,50.927267 50.206338 0 0 52.56843,1.68637 29.487607,50.927267 50.206338 0 0 10.27878,-7.82507 l -3.15503,-2.35607 a 20.198386,39.450089 64.736938 0 1 -4.26592,2.38712 20.198386,39.450089 64.736938 0 1 -45.89564,0.75892 20.198386,39.450089 64.736938 0 1 14.918,-29.44542 z" id="path1260" inkscape:connector-curvature="0" /></g></g></g></svg>{{pluckcom('Offer_menu_title',item.component)}}</a></li>
        
        <!--<li><a href="#"><img class="img-fluid" src="/Nirvana/images/icons/our-partners-icon.png"><p>Our Partners</p></a></li>-->
        <!--<li><a href="/Nirvana/awards.html"><svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46667" version="1.1" id="svg1699" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" sodipodi:docname="award_final.svg"><defs id="defs1693" /><sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="1.0117187" inkscape:cx="81.050181" inkscape:cy="256" inkscape:document-units="mm" inkscape:current-layer="g6121" showgrid="false" units="px" showguides="true" inkscape:guide-bbox="true" inkscape:window-width="1366" inkscape:window-height="705" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" inkscape:pagecheckerboard="true" /><metadata id="metadata1696"><rdf:RDF><cc:Work rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /><dc:title></dc:title></cc:Work></rdf:RDF></metadata><g inkscape:label="Border" inkscape:groupmode="layer" id="layer1" transform="translate(0,-161.53332)" /><g transform="translate(0,-161.53332)" id="g6020" inkscape:groupmode="layer" inkscape:label="Border copy" /><g inkscape:groupmode="layer" id="layer3" inkscape:label="Star" style="opacity:1" /><g inkscape:groupmode="layer" id="layer4" inkscape:label="Leaves" style="display:inline" /><g style="display:inline" inkscape:label="Leaves copy" id="g6121" inkscape:groupmode="layer"><g id="g6179" transform="matrix(1.0281447,0,0,1.0281447,-2.0333159,-1.7793499)"><path inkscape:connector-curvature="0" id="path2255" d="M 67.856841,163.26396 A 65.879318,65.879318 0 0 0 1.9776577,229.14314 65.879318,65.879318 0 0 0 67.856841,295.02233 65.879318,65.879318 0 0 0 133.73602,229.14314 65.879318,65.879318 0 0 0 67.856841,163.26396 Z m 0,5.06739 a 60.811679,60.811679 0 0 1 60.811789,60.81179 60.811679,60.811679 0 0 1 -60.811789,60.8118 60.811679,60.811679 0 0 1 -60.8117924,-60.8118 60.811679,60.811679 0 0 1 60.8117924,-60.81179 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" transform="translate(0,-161.53332)" /><path inkscape:connector-curvature="0" id="path6022" d="m 34.953471,193.26883 a 48.595592,48.595592 0 0 0 -17.500224,37.28661 48.595592,48.595592 0 0 0 34.568434,46.48296 h 0.10232 v -2.72955 A 45.7293,45.7293 0 0 1 20.31974,230.82623 45.7293,45.7293 0 0 1 36.712538,195.80666 v -2.53783 z m 60.471766,0 v 2.52956 a 45.7293,45.7293 0 0 1 16.353083,35.02784 45.7293,45.7293 0 0 1 -32.325756,43.70586 v 2.50631 h 0.730188 A 48.595592,48.595592 0 0 0 114.64431,230.55544 48.595592,48.595592 0 0 0 97.198257,193.26883 Z" style="opacity:1;fill-opacity:1;stroke:none;stroke-width:2.09375501;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" transform="translate(0,-161.53332)" /><path transform="matrix(0.71554811,0,0,0.69633923,19.302004,20.530454)" inkscape:transform-center-y="-3.2928064" inkscape:transform-center-x="-0.12318555" d="M 97.348917,114.11569 67.40613,97.955864 37.119028,113.46077 43.235105,79.989822 19.129838,55.976354 52.852568,51.449999 68.241796,21.103958 82.967513,51.77746 116.58385,57.036043 91.962109,80.519665 Z" inkscape:randomized="0" inkscape:rounded="0" inkscape:flatsided="false" sodipodi:arg2="1.5816696" sodipodi:arg1="0.9533511" sodipodi:r2="25.618816" sodipodi:r1="51.237633" sodipodi:cy="72.338562" sodipodi:cx="67.684685" sodipodi:sides="5" id="path5964" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" sodipodi:type="star" /><g id="g6099" style="display:inline;"><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 40.775554,114.54435 c -3.126113,-0.7203 -6.949163,-3.02955 -8.731844,-5.27433 -0.615899,-0.77556 -0.605299,-1.06131 0.0484,-1.30481 0.663058,-0.24699 2.62215,-0.58249 3.420294,-0.58575 1.768287,-0.007 5.819133,1.42775 9.24071,3.27342 0.898014,0.48441 1.650345,0.8564 1.671846,0.82665 0.0215,-0.0297 -0.267653,-0.88935 -0.642566,-1.91026 -1.012749,-2.75775 -1.259675,-3.86025 -1.267103,-5.65749 -0.0058,-1.40349 0.159517,-2.58017 0.362506,-2.58017 0.04888,0 0.368905,0.42345 0.711164,0.94098 1.498927,2.26656 2.752932,6.35801 2.87482,9.37971 0.05911,1.46535 -0.04999,1.87135 -0.601341,2.23779 -0.888213,0.59035 -5.511767,1.01718 -7.08689,0.65426 z" id="path860" inkscape:connector-curvature="0" /><path inkscape:connector-curvature="0" id="path5993" d="m 25.431516,102.87336 c -2.481155,-1.37502 -5.990743,-4.882146 -5.852599,-5.848481 0.06157,-0.430645 1.748012,-0.614613 3.368369,-0.36744 1.557452,0.237614 4.384334,1.682254 7.197231,3.678151 1.052911,0.74709 1.971103,1.31534 2.040427,1.26277 0.06932,-0.0526 -0.07155,-1.14174 -0.313066,-2.420391 -0.308772,-1.634756 -0.38769,-2.914827 -0.265874,-4.312561 0.24649,-2.82828 0.580965,-2.898379 1.385132,-0.290305 0.839014,2.721099 1.062967,4.841695 0.761463,7.210217 -0.239129,1.87854 -0.246544,1.89456 -0.990444,2.13814 -0.487628,0.15967 -1.690572,0.13473 -3.442855,-0.0714 -2.250204,-0.26467 -2.88986,-0.42574 -3.887784,-0.97873 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 17.913737,90.196178 c -1.958503,-1.909671 -4.307633,-6.001804 -3.917415,-6.824049 0.1739,-0.366432 1.806679,-0.06438 3.261177,0.603285 1.398018,0.641783 3.662312,2.708934 5.764851,5.263051 0.787016,0.956044 1.495778,1.715635 1.575029,1.687979 0.07925,-0.02765 0.240646,-1.036356 0.358669,-2.241551 0.150881,-1.540847 0.421913,-2.702399 0.913112,-3.913263 0.993927,-2.450159 1.326821,-2.420181 1.378584,0.124133 0.054,2.654576 -0.307488,4.604576 -1.229103,6.630182 -0.730958,1.606563 -0.742237,1.618773 -1.506257,1.630165 -0.500817,0.0075 -1.623377,-0.347023 -3.212794,-1.014565 -2.041059,-0.857229 -2.598119,-1.177339 -3.385846,-1.945367 z" id="path5995" inkscape:connector-curvature="0" /><path inkscape:connector-curvature="0" id="path5997" d="m 15.036926,78.373003 c -1.521321,-1.936373 -3.144744,-5.906349 -2.707223,-6.620343 0.19498,-0.31819 1.607447,0.116521 2.821143,0.868257 1.166565,0.722581 2.941137,2.832642 4.51802,5.372292 0.590253,0.95063 1.133147,1.714022 1.206436,1.696426 0.07329,-0.01759 0.328968,-0.924299 0.568181,-2.014896 0.305831,-1.394331 0.675703,-2.430161 1.246094,-3.489711 1.154171,-2.143975 1.445681,-2.084246 1.20719,0.247385 -0.248818,2.432634 -0.786923,4.180653 -1.829602,5.943432 -0.826977,1.398107 -0.838331,1.408176 -1.516329,1.34442 -0.444429,-0.0418 -1.399113,-0.474925 -2.732329,-1.239641 -1.712055,-0.982015 -2.169695,-1.32881 -2.781589,-2.107583 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 14.62871,65.942556 c -0.873514,-2.230168 -1.27155,-6.382869 -0.663767,-6.925053 0.270858,-0.241624 1.464316,0.559514 2.383712,1.600126 0.883684,1.000237 1.944318,3.463215 2.700317,6.270801 0.282983,1.050923 0.574394,1.914185 0.647583,1.918361 0.0732,0.0041 0.565695,-0.768497 1.09446,-1.717049 0.676026,-1.212726 1.311106,-2.073616 2.139584,-2.900337 1.676406,-1.672849 1.931128,-1.535436 1.054854,0.569066 -0.914243,2.195691 -1.90581,3.67264 -3.371438,5.021762 -1.162423,1.070028 -1.175823,1.076218 -1.789351,0.82662 -0.402167,-0.16362 -1.169738,-0.834843 -2.196801,-1.921078 -1.318907,-1.394894 -1.647804,-1.846262 -1.999159,-2.743208 z" id="path6008" inkscape:connector-curvature="0" /><path inkscape:connector-curvature="0" id="path6010" d="m 17.050108,53.532041 c -0.412313,-2.153907 -0.07426,-5.958607 0.563733,-6.344727 0.284322,-0.172075 1.22425,0.749173 1.87733,1.840021 0.627706,1.048517 1.169414,3.44336 1.379126,6.097311 0.0785,0.993415 0.196054,1.819325 0.261234,1.835355 0.06517,0.01608 0.63806,-0.596887 1.273071,-1.36204 0.811864,-0.978246 1.527868,-1.646668 2.41223,-2.2519 1.789485,-1.224674 1.995725,-1.058271 0.854066,0.689129 -1.191111,1.823096 -2.331306,2.986271 -3.876781,3.954892 -1.225752,0.768242 -1.238851,0.771569 -1.749251,0.444019 -0.334567,-0.214712 -0.912924,-0.947608 -1.655268,-2.097575 -0.953286,-1.476739 -1.173649,-1.938177 -1.339511,-2.804459 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 21.579037,44.015664 c 0.06035,-1.922575 1.079636,-5.114055 1.69976,-5.322121 0.276357,-0.09272 0.904068,0.876077 1.253735,1.935002 0.336071,1.017827 0.340237,3.171436 0.01055,5.483114 -0.123401,0.865297 -0.181257,1.594723 -0.128541,1.620949 0.05271,0.02623 0.660616,-0.388443 1.350903,-0.921485 0.882532,-0.681496 1.623585,-1.116206 2.496602,-1.464537 1.766532,-0.704841 1.911122,-0.522853 0.598757,0.753664 -1.369212,1.331816 -2.568237,2.108588 -4.0768,2.641097 -1.196481,0.422348 -1.20833,0.422682 -1.582325,0.04442 -0.245152,-0.247954 -0.59954,-0.986181 -1.014263,-2.112831 -0.532569,-1.446794 -0.632641,-1.884004 -0.608396,-2.657251 z" id="path6012" inkscape:connector-curvature="0" /><path inkscape:connector-curvature="0" id="path6014" d="m 28.545103,34.63887 c 0.571825,-1.84726 2.319783,-4.665485 2.914898,-4.699679 0.265212,-0.01524 0.548995,1.093358 0.566803,2.214217 0.01709,1.077351 -0.561077,3.165979 -1.472079,5.317656 -0.341002,0.805406 -0.588324,1.49682 -0.549602,1.536476 0.03872,0.03965 0.679032,-0.198099 1.422913,-0.528345 0.951053,-0.422222 1.712487,-0.643441 2.565268,-0.745293 1.725583,-0.206095 1.802073,0.0094 0.3168,0.892215 -1.549674,0.921135 -2.801507,1.350231 -4.256353,1.458955 -1.153875,0.08624 -1.164263,0.08336 -1.387091,-0.384303 -0.146063,-0.306555 -0.254606,-1.117836 -0.310654,-2.321912 -0.07197,-1.546222 -0.04083,-1.997039 0.189131,-2.740004 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 34.940405,31.888019 c 1.424889,-1.307304 4.35497,-2.860485 4.88621,-2.590088 0.236748,0.120501 -0.07687,1.221032 -0.626385,2.198105 -0.528211,0.939133 -2.080224,2.451713 -3.951486,3.851005 -0.700442,0.523777 -1.262521,0.996311 -1.249063,1.050078 l -1.785018,0.403336 c 0.02834,-0.338389 0.623065,-1.572962 1.181496,-2.641182 0.717116,-1.371761 0.971218,-1.745441 1.544284,-2.271251 z" id="path6024" inkscape:connector-curvature="0" sodipodi:nodetypes="cccsccccc" /></g><g transform="translate(-0.52916663,2.1166667)" id="g6141"><path inkscape:connector-curvature="0" id="path6101" d="m 91.728036,112.33907 c 3.126113,-0.7203 6.949163,-3.02955 8.731844,-5.27433 0.6159,-0.77556 0.6053,-1.06131 -0.0484,-1.30481 -0.663058,-0.24699 -2.62215,-0.58249 -3.420294,-0.58575 -1.768287,-0.007 -5.819133,1.42775 -9.24071,3.27342 -0.898014,0.48441 -1.650345,0.8564 -1.671846,0.82665 -0.0215,-0.0297 0.267653,-0.88935 0.642566,-1.91026 1.012749,-2.75775 1.259675,-3.86025 1.267103,-5.65749 0.0058,-1.40349 -0.159517,-2.580167 -0.362506,-2.580167 -0.04888,0 -0.368905,0.42345 -0.711164,0.940977 -1.498927,2.26656 -2.752932,6.35801 -2.87482,9.37971 -0.05911,1.46535 0.04999,1.87135 0.601341,2.23779 0.888213,0.59035 5.511767,1.01718 7.08689,0.65426 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 107.07207,100.66808 c 2.48116,-1.375017 5.99075,-4.882143 5.8526,-5.848478 -0.0616,-0.430645 -1.74801,-0.614613 -3.36837,-0.36744 -1.55745,0.237614 -4.38433,1.682254 -7.19723,3.678151 -1.05291,0.74709 -1.9711,1.31534 -2.04042,1.26277 -0.0693,-0.0526 0.0715,-1.14174 0.31306,-2.420391 0.30877,-1.634756 0.38769,-2.914827 0.26588,-4.312561 -0.24649,-2.82828 -0.58097,-2.898379 -1.385136,-0.290305 -0.839014,2.721099 -1.062967,4.841695 -0.761463,7.210217 0.239129,1.878537 0.246544,1.894557 0.990444,2.138137 0.487625,0.15967 1.690575,0.13473 3.442855,-0.0714 2.2502,-0.26467 2.88986,-0.42574 3.88778,-0.97873 z" id="path6103" inkscape:connector-curvature="0" /><path inkscape:connector-curvature="0" id="path6105" d="m 114.58985,87.990901 c 1.95851,-1.909671 4.30764,-6.001804 3.91742,-6.824049 -0.1739,-0.366432 -1.80668,-0.06438 -3.26118,0.603285 -1.39802,0.641783 -3.66231,2.708934 -5.76485,5.263051 -0.78702,0.956044 -1.49578,1.715635 -1.57503,1.687979 -0.0793,-0.02765 -0.24064,-1.036356 -0.35867,-2.241551 -0.15088,-1.540847 -0.42191,-2.702399 -0.91311,-3.913263 -0.99393,-2.450159 -1.32682,-2.420181 -1.37858,0.124133 -0.054,2.654576 0.30748,4.604576 1.2291,6.630182 0.73096,1.606563 0.74224,1.618773 1.50626,1.630165 0.50081,0.0075 1.62337,-0.347023 3.21279,-1.014565 2.04106,-0.857229 2.59812,-1.177339 3.38585,-1.945367 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 117.46666,76.167726 c 1.52133,-1.936373 3.14475,-5.906349 2.70723,-6.620343 -0.19498,-0.31819 -1.60745,0.116521 -2.82115,0.868257 -1.16656,0.722581 -2.94113,2.832642 -4.51802,5.372292 -0.59025,0.95063 -1.13314,1.714022 -1.20643,1.696426 -0.0733,-0.01759 -0.32897,-0.924299 -0.56818,-2.014896 -0.30583,-1.394331 -0.67571,-2.430161 -1.2461,-3.489711 -1.15417,-2.143975 -1.44568,-2.084246 -1.20719,0.247385 0.24882,2.432634 0.78693,4.180653 1.82961,5.943432 0.82697,1.398107 0.83833,1.408176 1.51632,1.34442 0.44443,-0.0418 1.39912,-0.474925 2.73233,-1.239641 1.71206,-0.982015 2.1697,-1.32881 2.78159,-2.107583 z" id="path6107" inkscape:connector-curvature="0" /><path inkscape:connector-curvature="0" id="path6109" d="m 117.87488,63.737279 c 0.87351,-2.230168 1.27155,-6.382869 0.66377,-6.925053 -0.27086,-0.241624 -1.46432,0.559514 -2.38372,1.600126 -0.88368,1.000237 -1.94431,3.463215 -2.70031,6.270801 -0.28298,1.050923 -0.5744,1.914185 -0.64758,1.918361 -0.0732,0.0041 -0.5657,-0.768497 -1.09446,-1.717049 -0.67603,-1.212726 -1.31111,-2.073616 -2.13959,-2.900337 -1.6764,-1.672849 -1.93113,-1.535436 -1.05485,0.569066 0.91424,2.195691 1.90581,3.67264 3.37144,5.021762 1.16242,1.070028 1.17582,1.076218 1.78935,0.82662 0.40216,-0.16362 1.16973,-0.834843 2.1968,-1.921078 1.3189,-1.394894 1.6478,-1.846262 1.99916,-2.743208 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 115.45348,51.326764 c 0.41232,-2.153907 0.0743,-5.958607 -0.56373,-6.344727 -0.28432,-0.172075 -1.22425,0.749173 -1.87733,1.840021 -0.62771,1.048517 -1.16941,3.44336 -1.37913,6.097311 -0.0785,0.993415 -0.19605,1.819325 -0.26123,1.835355 -0.0652,0.01608 -0.63806,-0.596887 -1.27307,-1.36204 -0.81187,-0.978246 -1.52787,-1.646668 -2.41223,-2.2519 -1.78949,-1.224674 -1.99573,-1.058271 -0.85407,0.689129 1.19111,1.823096 2.33131,2.986271 3.87678,3.954892 1.22576,0.768242 1.23885,0.771569 1.74925,0.444019 0.33457,-0.214712 0.91293,-0.947608 1.65527,-2.097575 0.95329,-1.476739 1.17365,-1.938177 1.33951,-2.804459 z" id="path6111" inkscape:connector-curvature="0" /><path inkscape:connector-curvature="0" id="path6113" d="m 110.92455,41.810387 c -0.0603,-1.922575 -1.07963,-5.114055 -1.69976,-5.322121 -0.27635,-0.09272 -0.90406,0.876077 -1.25373,1.935002 -0.33607,1.017827 -0.34024,3.171436 -0.0105,5.483114 0.1234,0.865297 0.18126,1.594723 0.12854,1.620949 -0.0527,0.02623 -0.66062,-0.388443 -1.3509,-0.921485 -0.88254,-0.681496 -1.62359,-1.116206 -2.49661,-1.464537 -1.76653,-0.704841 -1.91112,-0.522853 -0.59875,0.753664 1.36921,1.331816 2.56823,2.108588 4.0768,2.641097 1.19648,0.422348 1.20833,0.422682 1.58232,0.04442 0.24515,-0.247954 0.59954,-0.986181 1.01427,-2.112831 0.53256,-1.446794 0.63264,-1.884004 0.60839,-2.657251 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /><path style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 103.95849,32.433593 c -0.57183,-1.84726 -2.31979,-4.665485 -2.9149,-4.699679 -0.26521,-0.01524 -0.549,1.093358 -0.5668,2.214217 -0.0171,1.077351 0.56107,3.165979 1.47208,5.317656 0.341,0.805406 0.58832,1.49682 0.5496,1.536476 -0.0387,0.03965 -0.67903,-0.198099 -1.42292,-0.528345 -0.95105,-0.422222 -1.712483,-0.643441 -2.565264,-0.745293 -1.725583,-0.206095 -1.802073,0.0094 -0.3168,0.892215 1.549674,0.921135 2.801504,1.350231 4.256354,1.458955 1.15387,0.08624 1.16426,0.08336 1.38709,-0.384303 0.14606,-0.306555 0.25461,-1.117836 0.31065,-2.321912 0.072,-1.546222 0.0408,-1.997039 -0.18913,-2.740004 z" id="path6115" inkscape:connector-curvature="0" /><path sodipodi:nodetypes="cccsccccc" inkscape:connector-curvature="0" id="path6117" d="m 97.563185,29.682742 c -1.424889,-1.307304 -4.35497,-2.860485 -4.88621,-2.590088 -0.236748,0.120501 0.07687,1.221032 0.626385,2.198105 0.528211,0.939133 2.080224,2.451713 3.951486,3.851005 0.700442,0.523777 1.262521,0.996311 1.249063,1.050078 l 1.785021,0.403336 c -0.0283,-0.338389 -0.623068,-1.572962 -1.181499,-2.641182 -0.717116,-1.371761 -0.971218,-1.745441 -1.544284,-2.271251 z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /></g></g></g></svg>{{pluckcom('Awards_menu_title',item.component)}}</a></li>-->
        <li><a href="/Nirvana/gallery-list.html"><svg xmlns:osb="http://www.openswatchbook.org/uri/2009/osb" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46667" version="1.1" id="svg8" inkscape:version="0.92.3 (2405546, 2018-03-11)"> <defs id="defs2"> <linearGradient id="linearGradient6884" osb:paint="solid"> <stop style="stop-color:#f10000;stop-opacity:1;" offset="0" id="stop6882" /> </linearGradient> <linearGradient id="linearGradient6864" osb:paint="solid"> <stop style="stop-color:#e50000;stop-opacity:1;" offset="0" id="stop6862" /> </linearGradient> </defs> <sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.49497475" inkscape:cx="399.62837" inkscape:cy="-20.003769" inkscape:document-units="mm" inkscape:current-layer="g843" showgrid="false" units="px" showguides="true" inkscape:guide-bbox="true" inkscape:window-width="1920" inkscape:window-height="1017" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1"> <sodipodi:guide position="78.241068,130.11831" orientation="0,1" id="guide845" inkscape:locked="false" /> <sodipodi:guide position="5.4806545,62.838543" orientation="1,0" id="guide847" inkscape:locked="false" /> <sodipodi:guide position="70.681547,5.3861607" orientation="0,1" id="guide849" inkscape:locked="false" /> <sodipodi:guide position="130.54353,67.019903" orientation="1,0" id="guide851" inkscape:locked="false" /> </sodipodi:namedview> <metadata id="metadata5"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(0,-161.53332)"> <g style="enable-background:new" id="g843" transform="translate(0.42039757,160.49011)"> <path style="opacity:1;fill-opacity:1;stroke:none;stroke-width:0.9653374;stroke-opacity:1" d="M 256.31055 0.01171875 A 255.80007 255.99442 0 0 0 0.51171875 256.00586 A 255.80007 255.99442 0 0 0 256.31055 512 A 255.80007 255.99442 0 0 0 512.11133 256.00586 A 255.80007 255.99442 0 0 0 256.31055 0.01171875 z M 257.05273 20.214844 A 236.33929 236.33929 0 0 1 493.39258 256.55273 A 236.33929 236.33929 0 0 1 257.05273 492.89258 A 236.33929 236.33929 0 0 1 20.714844 256.55273 A 236.33929 236.33929 0 0 1 257.05273 20.214844 z " transform="matrix(0.26458333,0,0,0.26458333,-0.42039757,1.04321)" id="path857" /> <path style="opacity:1;fill-rule:evenodd;stroke-width:0.31607604" d="m 29.191599,33.484221 c -6.119621,0 -11.046038,4.8107 -11.046038,10.786747 v 48.773263 c 0,5.976047 4.926417,10.787379 11.046038,10.787379 h 74.946281 c 6.11962,0 11.04666,-4.811332 11.04666,-10.787379 V 44.270968 c 0,-5.976047 -4.92704,-10.786747 -11.04666,-10.786747 z m -2.081927,7.003451 h 78.628498 c 1.15297,0 2.08131,0.947134 2.08131,2.124234 V 94.81769 c 0,1.177098 -0.92834,2.124862 -2.08131,2.124862 H 27.109672 c -1.152969,0 -2.08131,-0.947764 -2.08131,-2.124862 V 42.611906 c 0,-1.1771 0.928341,-2.124234 2.08131,-2.124234 z" id="rect871" inkscape:connector-curvature="0" /> <path style="opacity:1;fill-opacity:1;stroke:none;stroke-width:0.32026237;stroke-opacity:1" d="m 56.01806,48.103691 a 5.7632579,5.883865 0 0 0 -5.763528,5.884139 5.7632579,5.883865 0 0 0 5.763528,5.883507 5.7632579,5.883865 0 0 0 5.763527,-5.883507 5.7632579,5.883865 0 0 0 -5.763527,-5.884139 z m 18.665533,8.246011 c -1.000316,0.02917 -1.61577,1.070016 -1.61577,1.070016 L 61.060991,78.17727 56.148063,70.158154 55.618142,69.351061 c 0,0 -0.660579,-1.072696 -1.5811,-1.246352 -0.920521,-0.173656 -1.508051,0.73062 -1.508051,0.73062 l -8.065843,14.11372 c 0,0 -0.820788,1.791244 -0.608544,2.614682 0.212254,0.823436 1.197277,1.030198 1.197277,1.030198 0,0 44.625433,0.02043 45.005659,0 0.38021,-0.02044 1.090604,-0.183819 1.540859,-1.011238 0.450258,-0.827418 -0.241434,-2.128024 -0.241434,-2.128024 L 76.629946,57.664943 c 0,0 -0.620801,-1.184446 -1.741442,-1.307024 -0.07003,-0.0077 -0.138222,-0.01015 -0.204911,-0.0082 z" id="path904" inkscape:connector-curvature="0" /> </g> </g> </svg>{{pluckcom('Gallery_menu',item.component)}}</a></li>
        <li><a href="/Nirvana/blog.html"><svg xmlns:osb="http://www.openswatchbook.org/uri/2009/osb" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46667" version="1.1" id="svg8" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" sodipodi:docname="blog.svg"> <defs id="defs2"> <linearGradient osb:paint="solid" id="linearGradient6884"> <stop id="stop6882" offset="0" style="stop-color:#f10000;stop-opacity:1;" /> </linearGradient> <linearGradient osb:paint="solid" id="linearGradient6864"> <stop id="stop6862" offset="0" style="stop-color:#e50000;stop-opacity:1;" /> </linearGradient> </defs> <sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.9609375" inkscape:cx="256" inkscape:cy="256" inkscape:document-units="mm" inkscape:current-layer="layer2" showgrid="false" showguides="false" inkscape:guide-bbox="true" units="px" inkscape:lockguides="true" inkscape:window-width="1366" inkscape:window-height="705" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" inkscape:snap-others="false" inkscape:object-nodes="false" inkscape:snap-nodes="false" inkscape:snap-global="false" inkscape:pagecheckerboard="true"> <sodipodi:guide position="-222.70963,215.23058" orientation="1,0" id="guide3731" inkscape:locked="true" /> </sodipodi:namedview> <metadata id="metadata5"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(0,-161.53332)" style="display:inline" /> <g inkscape:groupmode="layer" id="layer2" inkscape:label="s" style="display:inline;opacity:1"> <g id="g4073" transform="matrix(1.0428068,0,0,1.077978,-10.398381,6.8098933)"> <path d="m -76.706692,49.596344 a 14.36668,13.748156 0 0 1 -7.18334,11.906252 14.36668,13.748156 0 0 1 -14.36668,0 14.36668,13.748156 0 0 1 -7.183338,-11.906253 l 14.366678,1e-6 z" sodipodi:end="3.1415927" sodipodi:start="0" sodipodi:ry="13.748156" sodipodi:rx="14.36668" sodipodi:cy="49.596344" sodipodi:cx="-91.073372" sodipodi:type="arc" id="path3723" style="display:inline;stroke-width:0.10764731" transform="matrix(-0.72231588,-0.69156328,0.72706926,-0.68656412,0,0)" /> <path inkscape:connector-curvature="0" id="path2255" d="M 69.791146,-6.0890897 A 62.815645,64.963908 85.312558 0 0 10.177947,61.825175 62.815645,64.963908 85.312558 0 0 80.057737,119.12174 62.815645,64.963908 85.312558 0 0 139.67093,51.207474 62.815645,64.963908 85.312558 0 0 69.791146,-6.0890897 Z m 0.394851,4.8155735 A 57.983673,59.966685 85.312558 0 1 134.69066,51.615828 57.983673,59.966685 85.312558 0 1 79.662886,114.30616 57.983673,59.966685 85.312558 0 1 15.158211,61.416821 57.983673,59.966685 85.312558 0 1 70.185997,-1.2735162 Z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /> <path id="path3713" transform="scale(0.26458333)" d="m 339.53516,77.193359 -157.2168,171.673831 -0.002,-0.002 -0.002,0.006 -1.70507,1.86133 0.73437,0.70703 -23.10937,61.16602 -18.80079,50.88476 70.08789,-20.88867 50.53711,-15.41406 0.70313,0.67578 1.13086,-1.23437 3.42383,-1.04493 -1.33203,-1.24023 155.70312,-170.02148 z m 23.10546,43.244141 16.44727,15.57617 -146.97656,153.83399 -16.44727,-15.57422 z" style="stroke-width:0.4254483" inkscape:connector-curvature="0" /> </g> </g> </svg>{{pluckcom('Blog_MenuTitle',item.component)}}</a></li>
        <li><a href="/Nirvana/csr.html"><svg xmlns:osb="http://www.openswatchbook.org/uri/2009/osb" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="512" height="512" viewBox="0 0 135.46666 135.46667" version="1.1" id="svg1000" inkscape:version="0.92.4 (5da689c313, 2019-01-14)" sodipodi:docname="csr-final.svg"> <defs id="defs994"> <inkscape:perspective id="perspective23" inkscape:persp3d-origin="67.73333 : 45.155557 : 1" inkscape:vp_z="135.46666 : 67.733335 : 1" inkscape:vp_y="0 : 1000 : 0" inkscape:vp_x="0 : 67.733335 : 1" sodipodi:type="inkscape:persp3d" /> <linearGradient id="linearGradient6884" osb:paint="solid"> <stop style="stop-color:#f10000;stop-opacity:1;" offset="0" id="stop6882" /> </linearGradient> <linearGradient id="linearGradient6864" osb:paint="solid"> <stop style="stop-color:#e50000;stop-opacity:1;" offset="0" id="stop6862" /> </linearGradient> </defs> <sodipodi:namedview id="base" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.9296875" inkscape:cx="256" inkscape:cy="256" inkscape:document-units="mm" inkscape:current-layer="layer1" showgrid="false" units="px" showguides="true" inkscape:guide-bbox="true" inkscape:pagecheckerboard="true" inkscape:window-width="1366" inkscape:window-height="705" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" /> <metadata id="metadata997"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(0,-161.53332)"> <g inkscape:label="icons" id="layer1-9" transform="translate(-217.76658,-139.42923)" /> <g id="layer3" inkscape:label="image" transform="translate(-217.76658,22.104086)" /> <g id="layer2" inkscape:label="main" transform="translate(-217.76658,22.104086)" /> <g id="g1662"> <path style="opacity:1;stroke-width:0.77719426" d="m 48.862077,195.1728 c -5.295233,0.0211 -9.570284,4.51348 -9.548716,10.03523 0.02118,5.5213 4.330196,9.98023 9.624945,9.95999 5.294233,-0.0211 9.568043,-4.51141 9.547172,-10.03215 -0.02118,-5.52121 -4.328683,-9.98249 -9.623401,-9.96307 z m 38.680302,-0.14547 c -5.295234,0.0211 -9.570285,4.51348 -9.548716,10.03523 0.02118,5.52129 4.330195,9.98022 9.624949,9.95996 5.294741,-0.0211 9.569576,-4.51082 9.548736,-10.03212 -0.02119,-5.52179 -4.329733,-9.98327 -9.624969,-9.96307 z M 28.714698,208.40874 A 13.241498,12.698227 89.749623 0 0 16.067685,221.69712 13.241498,12.698227 89.749623 0 0 28.81567,234.89147 13.241498,12.698227 89.749623 0 0 41.462714,221.6015 13.241498,12.698227 89.749623 0 0 28.714698,208.40874 Z m 39.350894,-0.14816 A 13.241498,12.698227 89.749623 0 0 55.418578,221.54894 13.241498,12.698227 89.749623 0 0 68.166565,234.74326 13.241498,12.698227 89.749623 0 0 80.815151,221.4533 13.241498,12.698227 89.749623 0 0 68.065592,208.26058 Z m 39.352468,-0.14818 a 13.241498,12.698227 89.749623 0 0 -12.647043,13.28836 13.241498,12.698227 89.749623 0 0 12.748023,13.19436 13.241498,12.698227 89.749623 0 0 12.64703,-13.28995 13.241498,12.698227 89.749623 0 0 -12.74801,-13.19277 z m -65.626901,6.97538 c -0.126146,0.23052 -0.250463,0.46742 -0.374032,0.70961 1.303496,1.75172 2.015078,3.90287 2.025786,6.11963 0.01512,4.57456 -1.412877,6.01317 -4.216186,8.12967 l 1.928898,2.03048 c 4.564444,7.75742 7.472173,22.13715 7.660201,37.88395 l 0.232919,-2.7e-4 c 0.06987,-15.89573 2.9214,-30.40636 7.493831,-38.13982 l 1.199832,-1.82015 c -2.087674,-1.44101 -3.67388,-3.36068 -3.693481,-8.12316 -0.0091,-2.37466 0.786659,-4.6754 2.248086,-6.49604 -0.02722,-0.0502 -0.05535,-0.10181 -0.08349,-0.15208 -1.993539,1.63722 -4.496051,2.53981 -7.082412,2.55214 -2.692901,0.009 -5.298803,-0.94835 -7.340015,-2.69351 z m 38.6803,-0.14577 c -0.09861,0.18123 -0.196316,0.36572 -0.293415,0.55403 1.374671,1.77806 2.12951,3.99104 2.138886,6.27453 0.01213,4.65345 -1.708047,6.70852 -3.557632,8.64898 l 1.268801,1.51112 c 4.564444,7.75745 7.472175,22.13715 7.660201,37.88397 l 0.234437,-2.5e-4 c 0.06982,-15.89572 2.921395,-30.40637 7.493826,-38.13979 l 1.018002,-1.95153 c -2.517095,-2.44368 -3.458292,-6.14208 -3.51323,-7.99182 -0.0066,-2.309 0.749041,-4.54985 2.139256,-6.3465 -0.0553,-0.10129 -0.111262,-0.20196 -0.166974,-0.30085 -1.993536,1.63718 -4.496079,2.53977 -7.082409,2.55212 -2.692905,0.009 -5.298809,-0.94833 -7.34002,-2.69352 z m -61.12123,19.84191 a 59.033696,19.540911 89.781196 0 0 -5.879979,15.35656 57.251259,58.197098 89.868059 0 0 35.047865,34.39037 59.033696,19.540911 89.781196 0 0 -10.06513,-49.55791 14.835553,14.740534 0 0 1 -9.380258,3.37928 14.835553,14.740534 0 0 1 -9.722498,-3.5683 z m 39.350894,-0.14817 a 59.033696,19.540911 89.781196 0 0 -9.868388,50.00726 57.251259,58.197098 89.868059 0 0 19.536739,3.31853 57.251259,58.197098 89.868059 0 0 19.503135,-3.44996 59.033696,19.540911 89.781196 0 0 -10.06873,-49.68677 14.835553,14.740534 0 0 1 -9.380258,3.37923 14.835553,14.740534 0 0 1 -9.722498,-3.56829 z m 39.352471,-0.14817 a 59.033696,19.540911 89.781196 0 0 -9.867147,49.92025 57.251259,58.197098 89.868059 0 0 34.778243,-34.6718 59.033696,19.540911 89.781196 0 0 -5.80834,-15.05941 14.835553,14.740534 0 0 1 -9.38184,3.37929 14.835553,14.740534 0 0 1 -9.720916,-3.56833 z" id="path4599-1-3-2" inkscape:connector-curvature="0" /> <g inkscape:label="Layer 1" id="layer1-1"> <g transform="matrix(1.0281448,0,0,1.0281447,-1.4400028,-8.4734356)" id="layer1-4" inkscape:label="Layer 1"> <path inkscape:connector-curvature="0" id="path2255" d="M 67.27977,165.35295 A 65.879319,65.879319 0 0 0 1.4005815,231.23213 65.879319,65.879319 0 0 0 67.27977,297.11131 65.879319,65.879319 0 0 0 133.15894,231.23213 65.879319,65.879319 0 0 0 67.27977,165.35295 Z m 0,5.06739 A 60.81168,60.81168 0 0 1 128.09155,231.23213 60.81168,60.81168 0 0 1 67.27977,292.04392 60.81168,60.81168 0 0 1 6.4679725,231.23213 60.81168,60.81168 0 0 1 67.27977,170.42034 Z" style="opacity:1;fill-opacity:1;stroke:#4d4d4d;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /> </g> </g> </g> </g> <g inkscape:groupmode="layer" id="layer4" inkscape:label="Layer 2" /> </svg>{{pluckcom('Community_Services_Title',item.component)}}</a></li>
        <li><a href="/Nirvana/contactus.html"><svg width="512" height="512" version="1.1" viewBox="0 0 135.47 135.47" xmlns="http://www.w3.org/2000/svg" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
        <defs>
        <clipPath id="a">
        <ellipse cx="200.24" cy="211.56" rx="125.59" ry="127.8" fill="#999" fill-opacity=".81768" fill-rule="evenodd"/>
        </clipPath>
        </defs>
        <metadata>
        <rdf:RDF>
        <cc:Work rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"/>
        <dc:title/>
        </cc:Work>
        </rdf:RDF>
        </metadata>
        <g>
        <g transform="matrix(1.0281 0 0 1.0281 -1.44 -170.01)">
        <path d="m67.28 165.35a65.879 65.879 0 0 0-65.879 65.879 65.879 65.879 0 0 0 65.879 65.879 65.879 65.879 0 0 0 65.879-65.879 65.879 65.879 0 0 0-65.879-65.879zm0 5.0674a60.812 60.812 0 0 1 60.812 60.812 60.812 60.812 0 0 1-60.812 60.812 60.812 60.812 0 0 1-60.812-60.812 60.812 60.812 0 0 1 60.812-60.812z" stroke-width="0"/>
        </g>
        <g transform="matrix(.50393 0 0 .50393 -33.173 -38.879)">
        <path d="m208.32 33.498c-21.496-0.04264-37.986 2.6379-58.721 9.5469-24.795 8.2621-47.022 21.714-67.01 40.555l-3.0098 2.8379 1.0898 1.1855 1.0898 1.1875-1.1719-1.0762-1.1719-1.0742-2.8574 2.9941c-3.0977 3.2481-6.0323 6.5798-8.8438 9.9785 0.97424-0.42199 1.9618-0.64833 2.9199-0.65039 4.3429-0.0093 8.0553 4.2979 6.9238 12.949-1.0005 7.6494-0.58972 5.9225-1.8906 11.68 4.7663-6.8644 10.623-14.067 15.779-19.326 1.7882-1.8239 2.6272-2.9041 2.4629-3.1699-0.16927-0.27389-0.11959-0.32356 0.1543-0.15429 0.26575 0.16424 1.3466-0.67218 3.1699-2.4551 5.0794-4.9669 9.5784-8.7059 16.811-13.969 23.51-17.108 51.285-27.32 80.994-29.777 4.5181-0.37373 9.0397-0.55401 13.549-0.54297 31.564 0.07726 62.603 9.5028 88.852 27.166 9.0359 6.0803 15.245 11.161 23.318 19.084 23.755 23.315 39.338 52.624 45.441 85.465 2.1565 11.604 3.0646 25.2 2.4473 36.639-1.0088 18.693-4.4718 34.854-11.109 51.84-2.134 5.4612-7.8892 17.179-11.002 22.4-14.006 23.492-33.283 42.77-56.775 56.775-4.9875 2.9734-16.868 8.8464-21.76 10.758-17.421 6.8063-33.274 10.262-52 11.336-22.134 1.2692-46.041-2.6634-67.359-11.082-4.8927-1.9321-16.231-7.4084-20.801-10.047-5.6114-3.2402-13.845-8.7406-17.76-11.863-27.177-21.677-44.898-46.806-55.143-78.197-6.4581-19.788-8.9757-41.501-7.2324-62.398 1.8387-22.042 7.664-42.316 17.67-61.416-0.52348 0.70003-1.0553 1.3933-1.6035 2.0742-0.37536 0.46625-1.1175 0.57599-1.4277 1.0879-1.0949 1.8065-1.8036 3.8201-2.7051 5.7305-2.5063 3.9836-5.0132 7.9676-7.5195 11.951-1.9117 3.1315-3.7842 6.2868-5.7344 9.3945-1.0009 1.5951-1.8937 3.2814-3.1172 4.7129-0.09191 0.10754-0.18403 0.24421-0.27539 0.36328-1.3264 6.995-4.3661 12.161-9.0762 18.504-1.0656 1.435-2.3575 2.6866-3.5078 4.0547-0.18119 0.21549-0.48768 0.97485-0.48242 0.69336 0.06029-3.2291 0.43176-6.4456 0.64844-9.668-0.76193-0.44585-1.5235-0.8919-2.2852-1.3379-0.48863 3.3-0.89589 6.6496-1.2012 10.037 2.1618 3.8953 3.1147 6.8377-0.42774 5.9062-0.72337 12.443-0.23409 25.157 1.5176 37.111 1.5702 10.716 3.9218 20.493 7.541 31.359 15.151 45.488 47.44 82.818 90.521 104.65 22.237 11.269 46.478 17.741 71.84 19.182 6.4933 0.36877 19.203 0.13367 26.24-0.48633 14.342-1.2637 26.853-3.8915 41.279-8.6699 15.367-5.0902 29.129-11.778 42.561-20.682 46.79-31.015 76.414-81.246 81.006-137.35 0.51036-6.2278 0.51244-21.589 4e-3 -28.32-3.0972-40.987-20.559-80.448-48.852-110.4-4.2803-4.5315-4.6657-4.9099-9.9336-9.7656-19.429-17.908-44.843-32.365-69.824-39.719-18.261-5.3751-33.24-7.5502-52.24-7.5879zm84.16 30.193c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.55859 0.49214 0.55859 0.55859 0 0.26337-0.26164 0.09554-0.67968-0.4375-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm2.5586 2.5586c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.56055 0.4941 0.56055 0.56055 0 0.26337-0.2636 0.09359-0.68164-0.43945-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm2.5606 2.5605c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.56055 0.4941 0.56055 0.56055 0 0.26337-0.2636 0.09359-0.68164-0.43945-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm2.5606 2.5605c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.55859 0.4941 0.55859 0.56055 0 0.26337-0.26165 0.09359-0.67969-0.43945-0.19902-0.25377-0.29581-0.38387-0.27929-0.40039zm2.5605 2.5605c0.0165-0.01652 0.14467 0.08028 0.39844 0.2793 0.308 0.24155 0.56055 0.49214 0.56055 0.55859 0 0.26337-0.26165 0.09359-0.67969-0.43945-0.19902-0.25377-0.29581-0.38192-0.2793-0.39844zm2.5586 2.5586c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.56054 0.49415 0.56054 0.56055 0 0.26338-0.2636 0.09359-0.68164-0.43945-0.19902-0.25377-0.29581-0.38387-0.27929-0.40039zm2.5605 2.5605c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.56055 0.49405 0.56055 0.56055 0 0.26338-0.2636 0.09359-0.68164-0.43945-0.19902-0.25377-0.29581-0.38387-0.2793-0.40039zm2.5606 2.5605c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.55859 0.49209 0.5586 0.55859 0 0.26338-0.26165 0.09554-0.67969-0.4375-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm2.5606 2.5586c0.0165-0.01652 0.14466 0.08028 0.39844 0.2793 0.30799 0.24155 0.56054 0.49405 0.56054 0.56055 0 0.26338-0.2636 0.09359-0.68164-0.43945-0.19902-0.25377-0.29386-0.38387-0.27734-0.40039zm2.5586 2.5605c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.56055 0.49405 0.56055 0.56055 0 0.26338-0.2636 0.09359-0.68164-0.43945-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm21.121 0.64062c0.0165 0.01652-0.0803 0.14662-0.27929 0.40039-0.41805 0.53308-0.68164 0.70283-0.68164 0.43945 0-0.06645 0.25254-0.319 0.56054-0.56055 0.25378-0.19902 0.38388-0.29582 0.40039-0.2793zm-254.71 1.5996c0.088 0 1.8837 1.7278 3.9902 3.8398s3.7579 3.8398 3.6699 3.8398-1.8837-1.7278-3.9902-3.8398-3.7579-3.8398-3.6699-3.8398zm236.15 0.32031c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.55859 0.49405 0.55859 0.56055 0 0.26338-0.26165 0.09359-0.67968-0.43945-0.19903-0.25377-0.29582-0.38387-0.2793-0.40039zm16.641 0c0.0165 0.01652-0.0803 0.14662-0.27929 0.40039-0.41805 0.53308-0.68164 0.70283-0.68164 0.43945 0-0.06645 0.25254-0.319 0.56054-0.56055 0.25378-0.19902 0.38388-0.29581 0.40039-0.2793zm-14.08 2.5605c0.0165-0.01652 0.14467 0.08028 0.39844 0.2793 0.308 0.24155 0.56055 0.49219 0.56055 0.55859 0 0.26338-0.26164 0.09359-0.67969-0.43945-0.19901-0.25377-0.29581-0.38192-0.2793-0.39844zm11.52 0c0.0165 0.01652-0.0803 0.14467-0.2793 0.39844-0.41805 0.53308-0.68164 0.70283-0.68164 0.43945 0-0.06645 0.25255-0.31704 0.56055-0.55859 0.25377-0.19902 0.38387-0.29581 0.40039-0.2793zm-8.9609 2.5586c0.0165-0.01652 0.14661 0.08028 0.40039 0.2793 0.308 0.24155 0.56055 0.49415 0.56055 0.56055 0 0.26338-0.26359 0.09359-0.68164-0.43945-0.19901-0.25377-0.29582-0.38387-0.2793-0.40039zm6.4004 0c0.0165 0.01652-0.0803 0.14662-0.2793 0.40039-0.41805 0.53308-0.67968 0.70283-0.67969 0.43945 1e-5 -0.06645 0.2506-0.319 0.5586-0.56055 0.25377-0.19902 0.38387-0.29581 0.40039-0.2793zm-238.46 2.5684c0.04562 0.01329 0.01495 0.11868-0.11328 0.32617-0.11354 0.18371-0.03301 0.54449 0.17969 0.80078 0.21244 0.25629 0.30623 0.46484 0.20703 0.46484-0.09916 0-0.27444-0.15381-0.39062-0.3418-0.11619-0.18803-0.44233-0.25406-0.72266-0.14648-0.40952 0.15716-0.39339 0.0683 0.08203-0.45703 0.394-0.43534 0.68178-0.66864 0.75781-0.64648zm235.26 0.66406c0.1059 0 0.21084 0.05263 0.31445 0.15625 0.20724 0.20724 0.10001 0.31445-0.31445 0.31445-0.41442 0-0.52365-0.10721-0.31641-0.31445 0.10362-0.10362 0.21051-0.15625 0.31641-0.15625zm-1.9199 1.8887c0.0165 0.01652-0.0803 0.14662-0.2793 0.40039-0.41805 0.53308-0.68164 0.70088-0.68164 0.4375 0-0.0665 0.25255-0.31704 0.56055-0.55859 0.25377-0.19902 0.38387-0.29581 0.40039-0.2793zm3.8398 0c0.0165-0.01652 0.14662 0.08028 0.40039 0.2793 0.308 0.24155 0.5586 0.49209 0.5586 0.55859 0 0.26338-0.26164 0.0955-0.67969-0.4375-0.19901-0.25377-0.29581-0.38387-0.2793-0.40039zm2.5606 2.5606c0.0165-0.0165 0.14466 0.0783 0.39844 0.27734 0.308 0.24155 0.56054 0.49405 0.56054 0.56055 0 0.26338-0.26163 0.0936-0.67968-0.43946-0.19901-0.25377-0.29582-0.38192-0.2793-0.39843zm2.5586 2.5586c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24155 0.56055 0.49404 0.56055 0.56054 0 0.26338-0.26359 0.0936-0.68164-0.43945-0.19901-0.25377-0.29581-0.38387-0.2793-0.40039zm-109.7 1.8613-0.49024 1.709c-0.27007 0.94035-0.59013 2.1419-0.71093 2.6699-0.1208 0.52801-0.99643 3.814-1.9453 7.3027-0.94888 3.4887-1.6419 6.4773-1.541 6.6406 0.10091 0.16327 0.3815 0.29688 0.62304 0.29688 1.0565 0 10.937 3.4389 14.834 5.1621 7.6982 3.4045 15.801 8.3102 21.805 13.201 3.3973 2.7676 9.5976 8.8311 12.307 12.037 2.553 3.0213 5.9594 7.8197 7.6875 10.83 0.62174 1.083 1.614 2.8134 2.207 3.8457 1.6996 2.9584 4.3097 8.8117 5.8203 13.053 2.3736 6.6639 3.723 12.337 4.8457 20.371 0.73973 5.2939 0.72543 17.438-0.0273 22.619-1.0562 7.2691-1.0563 6.7186 0 6.7324 0.50851 7e-3 4.3074 0.64041 8.4434 1.4082 8.7306 1.6207 9.1721 1.6635 9.4141 0.9004 0.31809-1.0033 1.1082-6.7188 1.4922-10.803 0.56765-6.0375 0.21626-19.624-0.65234-25.26-3.2308-20.965-11.211-39.224-24.152-55.264-2.981-3.6947-11.799-12.334-16.021-15.697-7.4744-5.9536-16.829-11.596-25.84-15.582-4.3332-1.9171-14.644-5.5491-16.928-5.9629zm112.26 0.69922c0.0165-0.0165 0.14662 0.0803 0.40039 0.27929 0.308 0.24155 0.56055 0.49415 0.56055 0.56055 0 0.26337-0.26359 0.0936-0.68164-0.43945-0.19901-0.25377-0.29582-0.38387-0.2793-0.40039zm-275.33 1.502c-9.3116 12.838-16.785 26.738-22.514 41.893 2.1587-3.4304 4.3181-6.8606 6.4766-10.291 2.5138-3.6256 4.7345-7.4728 7.541-10.877 0.24454-0.29661 0.62812-0.71414 1.041-1.1348 0.2684-0.75653 0.5552-1.5132 0.86328-2.2188 0.31384-0.71874 0.75638-1.3755 1.1348-2.0625 0.88962-1.4068 1.6435-2.9084 2.6699-4.2188 0.21163-0.27018 0.51577-0.68936 0.83789-1.1191 0.58125-2.4735 0.47269-2.1917 1.207-6.752 0.18544-1.1515 0.43865-2.2205 0.74219-3.2188zm277.89 1.0586c0.0165-0.0165 0.14661 0.0803 0.40039 0.2793 0.308 0.24155 0.55859 0.49208 0.55859 0.5586 0 0.26336-0.26164 0.0936-0.67969-0.43946-0.19901-0.25377-0.29581-0.38192-0.27929-0.39844zm2.5586 2.5586c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24154 0.56055 0.49404 0.56055 0.56054 0 0.26337-0.26359 0.0936-0.68164-0.43945-0.19901-0.25377-0.29582-0.38387-0.2793-0.40039zm2.5606 2.5605c0.0165-0.0165 0.14661 0.0803 0.40039 0.2793 0.308 0.24155 0.56054 0.49405 0.56054 0.56055 0 0.26337-0.26359 0.0936-0.68164-0.43945-0.19901-0.25377-0.29581-0.38387-0.27929-0.4004zm-120 0.96094c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24155 0.56054 0.49219 0.56054 0.55859 0 0.26337-0.2636 0.0936-0.68164-0.43945-0.19902-0.25377-0.29581-0.38192-0.27929-0.39844zm122.56 1.5996c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.30802 0.24155 0.5586 0.49209 0.5586 0.55859 0 0.26337-0.26164 0.0955-0.67969-0.4375-0.19901-0.25377-0.29581-0.38387-0.2793-0.40039zm-120 0.95899c0.0165-0.0165 0.14663 0.0803 0.40039 0.27929 0.308 0.24155 0.5586 0.49405 0.5586 0.56055 0 0.26337-0.26165 0.0936-0.67969-0.43945-0.19902-0.25377-0.29581-0.38388-0.2793-0.40039zm122.56 1.6016c0.0165-0.0165 0.14466 0.0783 0.39844 0.27734 0.308 0.24155 0.56055 0.49405 0.56055 0.56055 0 0.26337-0.26164 0.0936-0.67969-0.43945-0.19901-0.25377-0.29582-0.38192-0.2793-0.39844zm-120 0.95898c0.0165-0.0165 0.14467 0.0803 0.39844 0.2793 0.308 0.24155 0.56055 0.49405 0.56055 0.56055 0 0.26337-0.26165 0.0936-0.67969-0.43946-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm122.56 1.5996c0.0165-0.0165 0.14661 0.0803 0.40039 0.2793 0.308 0.24155 0.56054 0.49415 0.56054 0.56055 0 0.26337-0.26359 0.0936-0.68164-0.43946-0.19901-0.25377-0.29581-0.38387-0.27929-0.40039zm-120 0.96094c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24155 0.56054 0.49209 0.56054 0.55859 0 0.26337-0.2636 0.0955-0.68164-0.4375-0.19902-0.25377-0.29581-0.38387-0.27929-0.40039zm-92.699 1.0977c-2.7932-1.2e-4 -5.5418 0.2581-7.4531 0.77343-8.1417 2.1952-13.646 6.1531-18.137 13.043-2.487 3.8154-4.7339 9.091-5.7109 13.406-0.74013 3.269 0.24872 11.935 2.1172 18.559 2.1005 7.4461 8.5411 23.607 14.238 35.723 18.739 39.85 45.056 77.446 70.846 101.21 3.08 2.8384 6.6078 5.9677 7.8398 6.9551 5.135 4.1155 11.982 7.8836 15.84 8.7168 2.9133 0.62913 11.39 0.64598 14.641 0.0293 5.2242-0.99102 9.0611-2.6674 12.912-5.6426 5.6817-4.3895 9.8419-11.095 11.557-18.627 0.93735-4.117 0.9538-10.728 0.0371-14.926-3.2711-14.979-14.641-26.389-29.387-29.486-3.1293-0.65734-10.044-0.50199-13.545 0.30274-4.5666 1.0495-10.019 3.8099-13.197 6.6816l-1.168 1.0547-3.3359-3.5977c-4.6866-5.0514-7.4521-8.2497-11.199-12.957-1.7859-2.2436-3.4069-4.1851-3.6016-4.3144-0.1947-0.1293-0.23606-0.23724-0.0937-0.24024 0.14227-3e-3 -0.37548-0.83175-1.1504-1.8438-4.8315-6.3096-12.072-18.153-16.773-27.439-3.3393-6.5957-8.416-18.233-8.416-19.291 0-0.14472 0.68098-0.59249 1.5117-0.99609 8.3864-4.0744 14.567-11.821 17.098-21.43 0.8378-3.1808 1.1386-9.8742 0.60742-13.518-0.21542-1.4776-0.82908-4.0546-1.3633-5.7266-4.1057-12.85-14.273-22.511-26.992-25.648-2.09-0.51551-4.9275-0.77332-7.7207-0.77343zm215.26 0.50195c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24155 0.56055 0.49404 0.56055 0.56054 0 0.26337-0.26359 0.0936-0.68164-0.43945-0.19901-0.25377-0.29581-0.38387-0.2793-0.40039zm-120 0.96094c0.0165-0.0165 0.14662 0.0783 0.40039 0.27734 0.308 0.24155 0.56055 0.49405 0.56055 0.56055 0 0.26337-0.2636 0.0936-0.68164-0.43946-0.19902-0.25377-0.29582-0.38192-0.2793-0.39843zm122.56 1.5996c0.0165-0.0165 0.14661 0.0803 0.40039 0.27929 0.308 0.24155 0.55859 0.4921 0.55859 0.5586 0 0.26337-0.26163 0.0955-0.67968-0.4375-0.19901-0.25377-0.29582-0.38388-0.2793-0.40039zm-120 0.95898c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24155 0.55859 0.49414 0.55859 0.56054 0 0.26337-0.26164 0.0936-0.67968-0.43945-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm-27.328 8.0547c-0.43009-0.0103-0.51921 0.21089-0.68164 0.95703-0.12806 0.58829-1.1495 4.4519-2.2695 8.5879-1.12 4.136-2.0332 7.736-2.0312 8 3e-3 0.33606 1.5613 0.99073 5.1973 2.1816 2.8563 0.93556 6.5604 2.3566 8.2324 3.1582 18.32 8.7825 31.095 24.252 36.154 43.781 2.2016 8.4977 2.6624 18.719 1.2461 27.607-0.20404 1.2805-0.30386 2.3954-0.22266 2.4766 0.12997 0.12996 14.23 2.7584 16.742 3.1211 0.528 0.0762 1.1299 0.1611 1.3359 0.1875 0.20602 0.0264 0.45737-0.34803 0.56055-0.83203 0.62866-2.949 1.1882-8.3226 1.3574-13.041 0.57917-16.148-3.4789-32.274-11.668-46.375-10.368-17.853-27.865-31.662-48.545-38.311-2.112-0.67901-4.2932-1.3198-4.8477-1.4238-0.2344-0.044-0.41718-0.0727-0.56055-0.0762zm-175.56 27.473c-0.14266 0.47999-0.29599 0.95122-0.43555 1.4336-0.65074 2.2493-1.0592 3.7234-1.4492 5.2812 2.2693-0.33511 4.9302-0.17552 7.4629-0.17774 1.643-0.80492 2.9373-1.9344 3.5762-3.6738 1.1596-3.1573-4.6275-2.6806-9.1543-2.8633zm163.71 0.35157c-0.11832 0.12893-3.3058 11.815-4.1797 15.322-0.28501 1.144-0.58853 2.318-0.67383 2.6094-0.11956 0.40835 0.13295 0.57821 1.1035 0.74218 2.2817 0.38551 9.4249 3.5247 12.801 5.625 3.7851 2.3549 8.489 6.6118 10.863 9.832 3.4175 4.6352 6.1553 11.009 7.3125 17.023 0.73125 3.8006 0.75357 15.679 0.0293 15.686-0.41359 4e-3 -0.26093 0.95895 0.16015 1.002 0.22 0.0225 0.68707 0.0845 1.0391 0.13672 1.1111 0.16469 13.702 2.4177 15.041 2.6914 2.3627 0.48312 2.2882 0.53744 2.707-1.9883 0.59256-3.5732 0.79138-13.19 0.34766-16.799-1.1584-9.422-4.6171-19.036-9.5254-26.48-6.4502-9.7828-16.66-17.961-28.09-22.5-3.4853-1.384-8.7573-3.0965-8.9355-2.9023zm95.176 19.482c0.0165-0.0165 0.14662 0.0783 0.40039 0.27734 0.308 0.24155 0.55859 0.49405 0.55859 0.56055 0 0.26337-0.26164 0.0936-0.67968-0.43945-0.19902-0.25377-0.29582-0.38192-0.2793-0.39844zm-169.62 2.2383c0.088 0-0.98502 1.1526-2.3848 2.5606s-2.6171 2.5605-2.7051 2.5605 0.98503-1.1525 2.3848-2.5605 2.6171-2.5606 2.7051-2.5606zm172.17 0.32031c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24155 0.56055 0.49405 0.56055 0.56055 0 0.26337-0.2636 0.0936-0.68164-0.43946-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm2.5606 2.5606c0.0165-0.0165 0.14662 0.0803 0.40039 0.2793 0.308 0.24155 0.56055 0.49404 0.56055 0.56054 0 0.26337-0.2636 0.0936-0.68164-0.43945-0.19902-0.25377-0.29582-0.38387-0.2793-0.40039zm-65.92 2.5606c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.67969 0.70087-0.67969 0.4375 0-0.0665 0.2506-0.31705 0.5586-0.5586 0.25377-0.19902 0.38387-0.29581 0.40039-0.27929zm68.48 0c0.0165-0.0165 0.14662 0.0803 0.40039 0.27929 0.308 0.24155 0.55859 0.4921 0.55859 0.5586 0 0.26337-0.26165 0.0955-0.67969-0.4375-0.19902-0.25377-0.29581-0.38388-0.27929-0.40039zm-71.041 2.5586c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.67968 0.70282-0.67968 0.43945 0-0.0665 0.25254-0.31899 0.56054-0.56054 0.25377-0.19902 0.38192-0.29582 0.39844-0.2793zm73.602 0c0.0165-0.0165 0.14467 0.0803 0.39844 0.2793 0.308 0.24155 0.56055 0.49414 0.56055 0.56054 0 0.26337-0.26165 0.0936-0.67969-0.43945-0.19902-0.25377-0.29581-0.38387-0.2793-0.40039zm-76.16 2.5606c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.68164 0.70282-0.68164 0.43945 0-0.0664 0.25255-0.319 0.56055-0.56055 0.25377-0.19902 0.38387-0.29581 0.40039-0.27929zm78.719 0c0.0165-0.0165 0.14662 0.0803 0.40039 0.27929 0.308 0.24155 0.56054 0.49405 0.56054 0.56055 0 0.26337-0.2636 0.0936-0.68164-0.43945-0.19902-0.25377-0.29581-0.38388-0.27929-0.40039zm-81.279 2.5605c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.68164 0.70283-0.68164 0.43946 0-0.0665 0.25255-0.319 0.56055-0.56055 0.25377-0.19902 0.38387-0.29581 0.40039-0.2793zm-43.52 43.52c0.0165 0.0165-0.0803 0.14663-0.2793 0.4004-0.41804 0.53308-0.68164 0.70282-0.68164 0.43945 0-0.0665 0.25255-0.319 0.56055-0.56055 0.25377-0.19902 0.38387-0.29581 0.40039-0.2793zm-2.5606 2.5606c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41802 0.53308-0.67968 0.70087-0.67968 0.4375 0-0.0665 0.25059-0.31704 0.55859-0.55859 0.25377-0.19902 0.38387-0.29582 0.40039-0.2793zm-2.5605 2.5606c0.0165 0.0165-0.0803 0.14467-0.2793 0.39844-0.41804 0.53308-0.67969 0.70282-0.67969 0.43945 0-0.0665 0.25255-0.319 0.56055-0.56055 0.25377-0.19902 0.38192-0.29386 0.39844-0.27734zm-119.84 50.08 17.201 17.117c9.46 9.4154 17.199 17.157 17.199 17.201 0 0.2248-1.3688-1.1312-17.281-17.119zm302.24 3.6797c0.0165 0.0165-0.0803 0.14467-0.2793 0.39844-0.24157 0.308-0.49415 0.56055-0.56055 0.56055-0.26339 0-0.0936-0.26165 0.43946-0.67969 0.25376-0.19902 0.38387-0.29581 0.40039-0.2793zm-117.44 0.95899c0.0165 0.0165-0.0803 0.14662-0.27929 0.40039-0.41804 0.53308-0.68164 0.70282-0.68164 0.43945-2e-5 -0.0665 0.25254-0.31899 0.56054-0.56054 0.25377-0.19902 0.38387-0.29582 0.40039-0.2793zm114.88 1.5996c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41805 0.53308-0.67969 0.70282-0.67969 0.43945 0-0.0664 0.25255-0.31899 0.56055-0.56054 0.25378-0.19903 0.38192-0.29582 0.39844-0.2793zm-117.44 0.96094c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.67969 0.70282-0.67969 0.43945 0-0.0664 0.2506-0.319 0.5586-0.56055 0.25375-0.19902 0.38387-0.29581 0.40039-0.27929zm114.88 1.5996c0.0165 0.0165-0.0803 0.14663-0.27929 0.4004-0.41805 0.53308-0.68164 0.70282-0.68164 0.43945 0-0.0665 0.25254-0.319 0.56054-0.56055 0.25378-0.19902 0.38389-0.2958 0.40039-0.2793zm-117.44 0.96094c0.0165 0.0165-0.0803 0.14467-0.2793 0.39844-0.41804 0.53308-0.67969 0.70282-0.67969 0.43945 0-0.0665 0.25255-0.31704 0.56055-0.55859 0.25377-0.19902 0.38192-0.29582 0.39844-0.2793zm114.88 1.5996c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41805 0.53308-0.68164 0.70087-0.68164 0.4375 0-0.0665 0.25255-0.31704 0.56055-0.55859 0.25377-0.19902 0.38387-0.29582 0.40039-0.2793zm-117.44 0.95899c0.0165 0.0165-0.0803 0.14662-0.27929 0.40039-0.41804 0.53308-0.68164 0.70282-0.68164 0.43945 0-0.0664 0.25254-0.319 0.56054-0.56055 0.25377-0.19902 0.38388-0.29581 0.40039-0.27929zm114.88 1.6016c0.0165 0.0165-0.0803 0.14467-0.2793 0.39844-0.41805 0.53308-0.67968 0.70282-0.67968 0.43945 0-0.0664 0.25059-0.319 0.55859-0.56055 0.25378-0.19902 0.38388-0.29386 0.40039-0.27734zm-117.44 0.95898c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.68164 0.70283-0.68164 0.43946 0-0.0664 0.25255-0.319 0.56055-0.56055 0.25377-0.19902 0.38387-0.29581 0.40039-0.2793zm114.88 1.5996c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41805 0.53308-0.68164 0.70283-0.68164 0.43946 0-0.0665 0.25255-0.319 0.56055-0.56055 0.25377-0.19902 0.38387-0.29582 0.40039-0.2793zm-117.44 0.96094c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.67968 0.70087-0.67968 0.4375 0-0.0665 0.25059-0.31704 0.55859-0.55859 0.25377-0.19902 0.38387-0.29582 0.40039-0.2793zm114.88 1.5996c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41805 0.53308-0.68164 0.70282-0.68164 0.43945 0-0.0665 0.25255-0.31899 0.56055-0.56054 0.25378-0.19902 0.38387-0.29582 0.40039-0.2793zm-117.44 0.96094c0.0165 0.0165-0.0803 0.14466-0.2793 0.39843-0.41804 0.53308-0.68164 0.70283-0.68164 0.43946 0-0.0664 0.25255-0.319 0.56055-0.56055 0.25377-0.19902 0.38387-0.29386 0.40039-0.27734zm114.88 1.5996c0.0165 0.0165-0.0803 0.14662-0.27929 0.40039-0.41805 0.53308-0.67969 0.70087-0.67969 0.4375 0-0.0664 0.25059-0.31705 0.55859-0.5586 0.25378-0.19902 0.38388-0.29581 0.40039-0.27929zm-117.44 0.95898c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53308-0.68164 0.70282-0.68164 0.43945 0-0.0664 0.25255-0.31899 0.56055-0.56054 0.25377-0.19902 0.38387-0.29582 0.40039-0.2793zm114.88 1.5996c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.24157 0.308-0.49219 0.56055-0.55859 0.56055-0.26339 0-0.0936-0.2636 0.43945-0.68164 0.25377-0.19902 0.38192-0.29582 0.39844-0.2793zm-2.5586 2.5606c0.0165 0.0165-0.0803 0.14662-0.27929 0.40039-0.24157 0.308-0.49415 0.56054-0.56055 0.56054-0.26339 0-0.0936-0.2636 0.43945-0.68164 0.25378-0.19902 0.38388-0.29581 0.40039-0.27929zm-2.5605 2.5605c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41805 0.53297-0.68164 0.70285-0.68164 0.43946 0-0.0664 0.25255-0.31898 0.56055-0.56055 0.25377-0.19901 0.38387-0.29581 0.40039-0.2793zm-10.08 1.7598 1.6797 1.5879c0.924 0.87338 1.6797 1.6291 1.6797 1.6797 0 0.23878-0.2959-0.0268-1.7715-1.5879zm-228 0.80078c0.01652 0.0165-0.08028 0.14466-0.2793 0.39844-0.41804 0.53297-0.68164 0.70284-0.68164 0.43945 0-0.0664 0.25255-0.31702 0.56055-0.55859 0.25377-0.19901 0.38387-0.29582 0.40039-0.2793zm235.52 0c0.0165 0.0165-0.0803 0.14466-0.2793 0.39844-0.41805 0.53297-0.67969 0.70284-0.67969 0.43945 0-0.0664 0.2506-0.31702 0.5586-0.55859 0.25377-0.19901 0.38387-0.29582 0.40039-0.2793zm-238.13 2.5586c0.088 0-0.17648 0.36079-0.58984 0.80079s-0.82411 0.80078-0.91211 0.80078 0.17844-0.36078 0.5918-0.80078 0.82216-0.80079 0.91016-0.80079zm235.52 0c0.088 0-0.17846 0.36079-0.5918 0.80079s-0.82411 0.80078-0.91211 0.80078 0.17846-0.36078 0.5918-0.80078 0.82411-0.80079 0.91211-0.80079zm-235.31 1.7598 15.6 15.52c8.58 8.5353 15.602 15.555 15.602 15.6 0 0.22499-1.2569-1.0174-15.682-15.518zm235.52 0 4.0801 3.9961c2.244 2.1973 4.0801 4.0334 4.0801 4.0801 0 0.22989-0.45293-0.2062-4.1641-3.9961zm-238.24 0.80078c0.01652 0.0165-0.08028 0.14662-0.2793 0.40039-0.24155 0.308-0.49415 0.56055-0.56055 0.56055-0.26337 0-0.09359-0.26359 0.43945-0.68164 0.25377-0.19901 0.38387-0.29581 0.40039-0.2793zm235.52 0c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.24157 0.308-0.49414 0.56055-0.56055 0.56055-0.26339 0-0.0936-0.26359 0.43946-0.68164 0.25377-0.19901 0.38387-0.29581 0.40039-0.2793zm-238.08 2.5606c0.01652 0.0165-0.08028 0.14661-0.2793 0.40039-0.41804 0.53297-0.68164 0.70089-0.68164 0.4375 0-0.0664 0.25255-0.31702 0.56055-0.5586 0.25377-0.199 0.38387-0.29581 0.40039-0.27929zm235.52 0c0.0165 0.0165-0.0803 0.14661-0.2793 0.40039-0.24155 0.308-0.49414 0.55859-0.56054 0.55859-0.26339 0-0.0936-0.26164 0.43945-0.67969 0.25378-0.199 0.38388-0.29581 0.40039-0.27929zm-238.08 2.5586c0.01652 0.0165-0.08028 0.14662-0.2793 0.40039-0.24155 0.308-0.49415 0.56055-0.56055 0.56055-0.26337 0-0.09359-0.26359 0.43945-0.68164 0.25377-0.19901 0.38387-0.29581 0.40039-0.2793zm235.52 0c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.24155 0.308-0.4922 0.56055-0.5586 0.56055-0.26337 0-0.0936-0.26359 0.43946-0.68164 0.25377-0.19901 0.38191-0.29581 0.39844-0.2793zm-237.82 2.2402c0.088 0-0.10136 0.28862-0.41992 0.64062s-0.65028 0.64063-0.73828 0.64063 0.10136-0.28863 0.41992-0.64063 0.65028-0.64062 0.73828-0.64062zm235.26 0.32031c0.0165 0.0165-0.0803 0.14661-0.2793 0.40039-0.24155 0.308-0.49414 0.56055-0.56054 0.56055-0.26337 0-0.0936-0.26359 0.43945-0.68164 0.25377-0.19901 0.38387-0.29582 0.40039-0.2793zm-2.5606 2.5606c0.0165 0.0165-0.0803 0.14661-0.27929 0.40039-0.24155 0.308-0.49415 0.55859-0.56055 0.55859-0.26337 0-0.0936-0.26164 0.43945-0.67969 0.25377-0.19901 0.38388-0.29581 0.40039-0.27929zm-2.5605 2.5605c0.0165 0.0165-0.0803 0.14467-0.2793 0.39844-0.24155 0.308-0.49415 0.56055-0.56055 0.56055-0.26337 0-0.0936-0.26164 0.43946-0.67969 0.25377-0.19901 0.38387-0.29581 0.40039-0.2793zm-2.5606 2.5586c0.0165 0.0165-0.0803 0.14661-0.2793 0.40039-0.24155 0.308-0.49219 0.56054-0.55859 0.56054-0.26337 0-0.0936-0.26359 0.43945-0.68164 0.25377-0.19901 0.38192-0.29581 0.39844-0.27929zm-2.5586 2.5605c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.24155 0.308-0.49415 0.56055-0.56055 0.56055-0.26337 0-0.0936-0.26359 0.43946-0.68164 0.25377-0.19901 0.38387-0.29581 0.40039-0.2793zm-2.5606 2.5606c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.24155 0.308-0.49414 0.55859-0.56054 0.55859-0.26337 0-0.0936-0.26163 0.43945-0.67968 0.25377-0.19901 0.38387-0.29582 0.40039-0.2793zm-2.5606 2.5606c0.0165 0.0165-0.0803 0.14466-0.27929 0.39843-0.24155 0.308-0.4922 0.56055-0.5586 0.56055-0.26337 0-0.0955-0.26359 0.4375-0.68164 0.25377-0.19901 0.38388-0.29386 0.40039-0.27734zm-2.5586 2.5586c0.0165 0.0165-0.0803 0.14662-0.2793 0.40039-0.41804 0.53307-0.68164 0.70284-0.68164 0.43945 0-0.0664 0.25255-0.31897 0.56055-0.56054 0.25377-0.19901 0.38387-0.29582 0.40039-0.2793zm-2.5606 2.5606c0.0165 0.0165-0.0803 0.14661-0.27929 0.40039-0.41804 0.53307-0.68164 0.70284-0.68164 0.43945-1e-5 -0.0664 0.25254-0.31898 0.56054-0.56055 0.25377-0.19901 0.38387-0.29581 0.40039-0.27929zm-2.5605 2.5605c0.0165 0.0165-0.0803 0.14467-0.2793 0.39844-0.41804 0.53307-0.67969 0.70285-0.67969 0.43946 0-0.0664 0.2506-0.31703 0.5586-0.5586 0.25375-0.19901 0.38387-0.29581 0.40039-0.2793z" clip-path="url(#a)" stroke-width=".32"/>
        <g transform="matrix(.98211 .20501 -.1883 1.0693 59.77 -43.241)" fill-opacity=".81768" fill-rule="evenodd">
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        </g>
        <g transform="matrix(2.226 .38608 -.42679 2.0137 36.022 -188.17)" fill-opacity=".81768" fill-rule="evenodd">
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        </g>
        <g transform="matrix(3.1589 .7315 -.60565 3.8153 34.586 -514.5)" fill-opacity=".81768" fill-rule="evenodd">
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        </g>
        <g transform="matrix(1.8114 .39722 -.34729 2.0718 113.82 -252.72)" fill-opacity=".81768" fill-rule="evenodd">
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        </g>
        <g transform="matrix(1.141 3.3865 -1.5922 .11958 431.21 -239.05)" fill-opacity=".81768" fill-rule="evenodd">
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        </g>
        <g transform="matrix(2.3296 -.49975 -.44666 -2.6065 154.21 755.09)" fill-opacity=".81768" fill-rule="evenodd">
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        <path d="m102.61 193.59a6.1659 7.169 0 0 1-5.4563 7.1213"/>
        </g>
        </g>
        </g>
        </svg>{{pluckcom('Contact_us_menu_title',item.component)}}</a></li>        
        </ul>
        </div>
        </div>
        </div></div>`,
    data() {
        return {
            username: '',
            password: '',
            emailId: '',
            retrieveEmailId: '',
            retrieveBookRefid: '',
            usererrormsg: { empty: false, invalid: false },
            psserrormsg: false,
            userFirstNameErormsg: false,
            userLastNameErrormsg: false,
            userEmailErormsg: { empty: false, invalid: false },
            userPasswordErormsg: false,
            userVerPasswordErormsg: false,
            userPwdMisMatcherrormsg: false,
            userTerms: false,
            userforgotErrormsg: { empty: false, invalid: false },
            retrieveBkngRefErormsg: false,
            retrieveEmailErormsg: false,
            userlogined: this.checklogin(),
            userinfo: [],
            registerUserData: {
                firstName: '',
                lastName: '',
                emailId: '',
                password: '',
                verifyPassword: '',
                terms: '',
                title: 'Mr'
            },
            Languages: [],
            language: 'en',
            content: null,
            getdata: true,
            active_el: (sessionStorage.active_el) ? sessionStorage.active_el : 1,
            bEmail: '',
            bRef: '',
            bService: 'F',
            show: 'show',
            hide: 'hide'
        }
    },
    methods: {
        loginaction: function() {

            if (!this.username.trim()) {
                this.usererrormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.username.trim())) {
                this.usererrormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.usererrormsg = { empty: false, invalid: false };
            }
            if (!this.password) {
                this.psserrormsg = true;
                return false;

            } else {
                this.psserrormsg = false;
                var self = this;
                login(this.username, this.password, function(response) {
                    if (response == false) {
                        self.userlogined = false;
                        alert("Invalid username or password.");
                    } else {
                        self.userlogined = true;
                        self.userinfo = JSON.parse(localStorage.User);
                        $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                        try {
                            packageDetailInfo.packageInfo.UserName = response.firstName + ' ' + response.lastName;
                            packageDetailInfo.packageInfo.UserEmail = response.emailId;
                            packageDetailInfo.packageInfo.UserPhone = response.contactNumber;
                            self.$eventHub.$emit('logged-in', { userName: self.username, password: self.password });
                            signArea.headerLogin({ userName: self.username, password: self.password })
                        } catch (error) {

                        }
                    }
                });

            }



        },
        validEmail: function(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        checklogin: function() {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);
                return true;
            } else {
                this.userlogined = false;
                return false;
            }
        },
        logout: function() {
            this.userlogined = false;
            this.userinfo = [];
            localStorage.profileUpdated = false;
            try {
                packageDetailInfo.packageInfo.UserName = "";
                packageDetailInfo.packageInfo.UserEmail = "";
                packageDetailInfo.packageInfo.UserPhone = "";
                this.$eventHub.$emit('logged-out');
                signArea.logout()
            } catch (error) {

            }
            commonlogin();
            // signOut();
            // signOutFb();
        },
        registerUser: function() {
            if (this.registerUserData.firstName.trim() == "") {
                this.userFirstNameErormsg = true;
                return false;
            } else {
                this.userFirstNameErormsg = false;
            }
            if (this.registerUserData.lastName.trim() == "") {
                this.userLastNameErrormsg = true;
                return false;
            } else {
                this.userLastNameErrormsg = false;
            }
            if (this.registerUserData.emailId.trim() == "") {
                this.userEmailErormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.registerUserData.emailId.trim())) {
                this.userEmailErormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.userEmailErormsg = { empty: false, invalid: false };
            }
            var vm = this;
            registerUser(this.registerUserData.emailId, this.registerUserData.firstName, this.registerUserData.lastName, this.registerUserData.title, function(response) {
                if (response.isSuccess == true) {
                    vm.username = response.data.data.user.loginId;
                    vm.password = response.data.data.user.password;
                    login(vm.username, vm.password, function (response) {
                        if (response != false) {
                            $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                            window.location.href = "/edit-my-profile.html?edit-profile=true";
                        }
                    });
                }

            });
        },

        forgotPassword: function() {
            if (this.emailId.trim() == "") {
                this.userforgotErrormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.emailId.trim())) {
                this.userforgotErrormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.userforgotErrormsg = { empty: false, invalid: false };
            }

            var datas = {
                agencyCode: localStorage.AgencyCode,
                emailId: this.emailId,
                logoUrl: window.location.origin + "/" + localStorage.AgencyFolderName + "/website-informations/logo/logo.png",
                websiteUrl: window.location.origin,
                resetUri: window.location.origin + "/" + localStorage.AgencyFolderName + "/reset-password.html"

            };
            $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:none;background:grey !important;");

            var huburl = ServiceUrls.hubConnection.baseUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var requrl = ServiceUrls.hubConnection.hubServices.forgotPasswordUrl;
            axios.post(huburl + portno + requrl, datas)
                .then((response) => {
                    if (response.data != "") {
                        alert(response.data);
                    } else {
                        alert("Error in forgot password. Please contact admin.");
                    }
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:#fad41a !important");

                })
                .catch((err) => {
                    console.log("FORGOT PASSWORD  ERROR: ", err);
                    if (err.response.data.message == 'No User is registered with this emailId.') {
                        alert(err.response.data.message);
                    } else {
                        alert('We have found some technical difficulties. Please contact admin!');
                    }
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:#fad41a !important");

                })

        },

        retrieveBooking: function() {
            if (this.bEmail == "") {
                //alert('Email required !');
                this.retrieveEmailErormsg = true;
                return false;

            } else if (!this.validEmail(this.bEmail)) {
                //alert('Invalid Email !');
                this.retrieveEmailErormsg = true;
                return false;

            } else {
                this.retrieveEmailErormsg = false;
            }
            if (this.bRef == "") {
                this.retrieveBkngRefErormsg = true;
                return false;

            } else {
                this.retrieveBkngRefErormsg = false;
            }
            if (!this.retrieveBkngRefErormsg && !this.retrieveEmailErormsg && !this.retrieveEmailErormsg) {
                switch (this.bService) {
                    case 'F':
                        this.retBookFlight();
                        break;
                    case 'H':
                        var vm = this;
                        var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;

                        axios({
                            method: "get",
                            url: hubUrl + "/hotelBook/bookingbyref/" + vm.bRef + ":" + vm.bEmail,
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer ' + localStorage.access_token
                            }
                        }).then(response => {
                            window.sessionStorage.setItem('userAction', vm.bRef);
                            window.location.href = "/Hotels/hotel-detail.html#/hotelConfirmation";

                        }).catch(error => {
                            alertify.alert('Error!', 'Booking details not found!');
                        });
                        break;
                    default:
                        break;
                }
            }

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        activate: function(el) {
            sessionStorage.active_el = el;
            this.active_el = el;
        },
        getpagecontent: function() {
            var self = this;
            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Master page/Master page/Master page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    self.content = response.data;
                    self.getdata = false;
                    window.localStorage.setItem("cmsContent", JSON.stringify(response.data));
                    var header = self.pluck('Header', self.content.area_List);
                    document.title = self.pluck('Page_Title', header[0].component);
                    self.show = self.pluckcom('showpassword_label', header[0].component);
                    self.hide = self.pluckcom('hidepassword_label', header[0].component);
                    Vue.nextTick(function() {
                        (

                            function() {
                                jqurufunctions();
                                self.active_el = (sessionStorage.active_el) ? sessionStorage.active_el : 1;
                                //Login/Signup modal window - by CodyHouse.co
                                function ModalSignin(element) {
                                    this.element = element;
                                    this.blocks = this.element.getElementsByClassName('js-signin-modal-block');
                                    this.switchers = this.element.getElementsByClassName('js-signin-modal-switcher')[0].getElementsByTagName('a');
                                    this.triggers = document.getElementsByClassName('js-signin-modal-trigger');
                                    this.hidePassword = this.element.getElementsByClassName('js-hide-password');
                                    this.init();
                                };

                                ModalSignin.prototype.init = function() {
                                    var self1 = this;
                                    //open modal/switch form
                                    for (var i = 0; i < this.triggers.length; i++) {
                                        (function(i) {
                                            self1.triggers[i].addEventListener('click', function(event) {
                                                if (event.target.hasAttribute('data-signin')) {
                                                    event.preventDefault();
                                                    self1.showSigninForm(event.target.getAttribute('data-signin'));
                                                }
                                            });
                                        })(i);
                                    }

                                    //close modal
                                    this.element.addEventListener('click', function(event) {
                                        if (hasClass(event.target, 'js-signin-modal') || hasClass(event.target, 'js-close')) {
                                            event.preventDefault();
                                            removeClass(self1.element, 'cd-signin-modal--is-visible');
                                        }
                                    });
                                    //close modal when clicking the esc keyboard button
                                    document.addEventListener('keydown', function(event) {
                                        (event.which == '27') && removeClass(self1.element, 'cd-signin-modal--is-visible');
                                    });

                                    // //hide/show password
                                    // for (var i = 0; i < this.hidePassword.length; i++) {
                                    //     (function(i) {
                                    //         self1.hidePassword[i].addEventListener('click', function(event) {
                                    //             self1.togglePassword(self1.hidePassword[i]);
                                    //         });
                                    //     })(i);
                                    // }

                                    //IMPORTANT - REMOVE THIS - it's just to show/hide error messages in the demo

                                };

                                // ModalSignin.prototype.togglePassword = function(target) {
                                //     var password = target.previousElementSibling;
                                //     ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text'): password.setAttribute('type', 'password');
                                //     target.textContent = ('Hide' == target.textContent) ? 'Show' : 'Hide';
                                //     putCursorAtEnd(password);
                                // }

                                ModalSignin.prototype.showSigninForm = function(type) {
                                    // show modal if not visible
                                    !hasClass(this.element, 'cd-signin-modal--is-visible') && addClass(this.element, 'cd-signin-modal--is-visible');
                                    // show selected form
                                    for (var i = 0; i < this.blocks.length; i++) {
                                        this.blocks[i].getAttribute('data-type') == type ? addClass(this.blocks[i], 'cd-signin-modal__block--is-selected') : removeClass(this.blocks[i], 'cd-signin-modal__block--is-selected');
                                    }
                                    //update switcher appearance
                                    var switcherType = (type == 'signup') ? 'signup' : 'login';
                                    for (var i = 0; i < this.switchers.length; i++) {
                                        this.switchers[i].getAttribute('data-type') == switcherType ? addClass(this.switchers[i], 'cd-selected') : removeClass(this.switchers[i], 'cd-selected');
                                    }
                                };

                                ModalSignin.prototype.toggleError = function(input, bool) {
                                    // used to show error messages in the form
                                    toggleClass(input, 'cd-signin-modal__input--has-error', bool);
                                    toggleClass(input.nextElementSibling, 'cd-signin-modal__error--is-visible', bool);
                                }

                                var signinModal = document.getElementsByClassName("js-signin-modal")[0];
                                if (signinModal) {
                                    new ModalSignin(signinModal);
                                }

                                // toggle main navigation on mobile
                                var mainNav = document.getElementsByClassName('js-main-nav')[0];
                                if (mainNav) {
                                    mainNav.addEventListener('click', function(event) {
                                        if (hasClass(event.target, 'js-main-nav')) {
                                            var navList = mainNav.getElementsByTagName('ul')[0];
                                            toggleClass(navList, 'cd-main-nav__list--is-visible', !hasClass(navList, 'cd-main-nav__list--is-visible'));
                                        }
                                    });
                                }

                                //class manipulations - needed if classList is not supported
                                function hasClass(el, className) {
                                    if (el.classList) return el.classList.contains(className);
                                    else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
                                }

                                function addClass(el, className) {
                                    var classList = className.split(' ');
                                    if (el.classList) el.classList.add(classList[0]);
                                    else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
                                    if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
                                }

                                function removeClass(el, className) {
                                    var classList = className.split(' ');
                                    if (el.classList) el.classList.remove(classList[0]);
                                    else if (hasClass(el, classList[0])) {
                                        var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
                                        el.className = el.className.replace(reg, ' ');
                                    }
                                    if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
                                }

                                function toggleClass(el, className, bool) {
                                    if (bool) addClass(el, className);
                                    else removeClass(el, className);
                                }
                                // $("#modal_retrieve").leanModal({
                                //     top: 100,
                                //     overlay: 0.6,
                                //     closeButton: ".modal_close"
                                // });
                                //credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
                                function putCursorAtEnd(el) {
                                    if (el.setSelectionRange) {
                                        var len = el.value.length * 2;
                                        el.focus();
                                        el.setSelectionRange(len, len);
                                    } else {
                                        el.value = el.value;
                                    }
                                };
                            })();
                    }.bind(self));


                }).catch(function(error) {
                    console.log('Error');
                    this.content = [];
                });
            });
        },
        // retrieveBooking: function () {
        //     switch (this.bService) {
        //         case 'F':
        //             this.retBookFlight();
        //             break;
        //         case 'H':
        //             //for Kelvin
        //             break;
        //         default:
        //             break;
        //     }
        // },
        retBookFlight: function() {
            if (this.bEmail == "") {
                alert('Email required !');
                return false;
            } else if (!this.validEmail(this.bEmail)) {
                alert('Invalid Email !');
                return false;
            } else {
                this.retrieveEmailErormsg = false;
            }
            if (this.bRef == "") {
                this.retrieveBkngRefErormsg = true;
                return false;
            } else {
                this.retrieveBkngRefErormsg = false;
            }
            var bookData = {
                BkngRefID: this.bRef,
                emailId: this.bEmail,
                redirectFrom: 'retrievebooking',
                isMailsend: false
            };
            localStorage.bookData = JSON.stringify(bookData);
            window.location.href = '/Flights/flight-confirmation.html';
        },
        showhidepassword: function(event) {
            var password = event.target.previousElementSibling;
            ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text'): password.setAttribute('type', 'password');
            event.target.textContent = (this.hide == event.target.textContent) ? this.show : this.hide;
        },
    },
    mounted: function() {

        document.onreadystatechange = () => {
            if (document.readyState == "complete") {
                if (localStorage.direction == 'rtl') {
                    $(".arborder").addClass("arbrder_left");
                    $(".ar_direction").addClass("ar_direction1");
                    $(".ajs-modal").addClass("ar_direction1");
                } else {
                    $(".arborder").removeClass("arbrder_left");
                    $(".ar_direction").removeClass("ar_direction1");
                    $(".ajs-modal").removeClass("ar_direction1");
                }
            }
        }


        if (localStorage.IsLogin == "true") {
            this.userlogined = true;
            this.userinfo = JSON.parse(localStorage.User);

        } else {
            this.userlogined = false;

        }
        this.getpagecontent();

    },
    created: function() {

    },
    watch: {
        updatelogin: function() {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);

            } else {
                this.userlogined = false;

            }

        },
        bRef: function() {
            this.retrieveBkngRefErormsg = false;
        }

    }
});
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});

Vue.prototype.$eventHub = new Vue({
    data: {}
});

var mapComponent = Vue.component('mapcomponent', {
    template: `<div v-if="getdata"></div><div class="contact_head" id="contact" v-else>
    
    <div class="contact1">
 
        <div class="container" style="width: 100%;">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 country">
                    <div class="row">
                        <h3>{{mainBranhTitle}}</h3>
                    </div>
                    <div class="row uae">
                        <div class="col-md-3 col-sm-6 col-xs-3" @click="changeCurrentLocation(brnch)" v-if="brnch.Status"  v-for="(brnch, index) in mainBranches">
                            <img class="" :src="brnch.Class_Icon">
                            <p>{{brnch.Branch_Name}}</p>
                        </div>
                        
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 country">
                    <div class="row">
                        <h3>{{globalBranchTitle}}</h3>
                    </div>
                        <div class="row uae">
                            <div class="col-md-3 col-sm-6 col-xs-3"   @click="changeCurrentLocation(brnch)" v-if="brnch.Status" v-for="(brnch, index) in globalBranches">
                            <img class="" :src="brnch.Class_Icon">
                            <p>{{brnch.Branch_Name}}</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="map_sec" v-if="showMap">
        <iframe
            :src="'https://www.google.com/maps/embed/v1/place?q='+currentLocationDetails.Latitude + ',' + currentLocationDetails.Longitude +'&zoom=14&key=AIzaSyBoOq8WuZ48t8uO4_XK_ACLJyIsOLX0F58'"
            width="100%" height="450" frameborder="0" style="border:0" allowfullscreen="">
        </iframe>
    </div>
</div>`,
    data() {
        return {
            content: null,
            getdata: true,
            currentLocationDetails: {},
            showMap: false,
            contactTitle: '',
            mainBranches:[],
            globalBranches:[],
            mainBranhTitle:"",
            globalBranchTitle:""
        }

    },
    methods: {
        changeCurrentLocation: function(brnch) {
            this.currentLocationDetails = brnch;
            this.$eventHub.$emit('select-location', brnch);
        },
        getpagecontent: function() {
            var self = this;
            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact us/Contact us/Contact us.ftl';
                var cmsurl = huburl + portno + homecms;
                var contactUsDetailsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact Us Details/Contact Us Details/Contact Us Details.ftl';
                
                axios.get(contactUsDetailsPage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    
                    let areaList=response.data.area_List;
                    var pageSettings={};
                    var mainBranchData={};
                    var globalBranchData={};
                    if(areaList.length>0&&areaList[0].Page_Settings!=undefined){
                        pageSettings = self.getAllMapData(areaList[0].Page_Settings.component);
                    }
                    if(areaList.length>1&&areaList[1].Main_Branch!=undefined){
                        mainBranchData = self.getAllMapData(areaList[1].Main_Branch.component);
                    }
                    if(areaList.length>2&&areaList[2].Global_Branches!=undefined){
                        globalBranchData = self.getAllMapData(areaList[2].Global_Branches.component);
                    }
                    self.mainBranhTitle=pageSettings.Main_Branch_Title.toUpperCase();
                    self.globalBranchTitle=pageSettings.Global_Branch_Title.toUpperCase();
                    self.mainBranches=mainBranchData.Branches;
                    self.globalBranches=globalBranchData.Branches;
                    
                    self.contactTitle = pageSettings.Page_Title;

                    for (let index = 0; index < self.mainBranches.length; index++) {
                        const element = self.mainBranches[index];
                        if(element.Status==true){
                            self.currentLocationDetails = element;
                            break;
                        }
                        
                    }

                    self.$eventHub.$emit('select-location', self.currentLocationDetails);
                    self.content = response.data;
                    self.getdata = false;
                   
                   
                   
                }).catch(function(error) {
                    console.log('Error');
                    this.content = [];
                });

            });
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
    },
    created: function() {
        this.getpagecontent();
        if (window.location.pathname == "/Nirvana/contactus.html") {
            this.showMap = true;
        }
    }
});

var FooterComponent = Vue.component('footeritem', {
    template: `<div class="ar_direction"><mapcomponent :key="key"></mapcomponent>
    <div  v-if="getdata"></div><div v-else v-for="item in pluck('Footer',content.area_List)"><div class="footer_address_sec" >
    <div class="container">
    <div class="row">
    <div class="col-lg-12 flx_package">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 about_footer">
    <h4>{{currentLocationDetails.Branch_Name||pluckcom('Branch_name',item.component)}}</h4>
    <p >{{currentLocationDetails.Branch_address||pluckcom('Branch_address',item.component)}}</p>
    <div class="flx_sec">
    
    <div id="sicialdiv">
    <h6>Follow Us</h6>
    <table>
    <tbody>
    <tr>
    <td><a :href="currentLocationDetails.Facebook||pluckcom('Facebook_link',item.component)" target="_blank"><img src="/Nirvana/images/contact-icon/fbfooter.png" width="25px" alt="Nirvana Facebook"></a></td>
    <td> <a :href="currentLocationDetails.Twitter||pluckcom('Twitter_link',item.component)" target="_blank"><img src="/Nirvana/images/contact-icon/twitterfooter.png" width="25px" alt="Nirvana Twitter"></a></td> 
    <td> <a :href="currentLocationDetails.Instagram||pluckcom('Instagram_link',item.component)" target="_blank"><img src="/Nirvana/images/contact-icon/afooter.png" width="25px" alt="Nirvana Instagram"></a></td> 
    <td> <a :href="currentLocationDetails.Linkedin||pluckcom('LinkedIn_link',item.component)" target="_blank"><img src="/Nirvana/images/contact-icon/infooter.png" width="25px" alt="Nirvana Linkedin"></a></td> 
    <td> <a :href="currentLocationDetails.Youtube||pluckcom('Play_store_link',item.component)" target="_blank"><img src="/Nirvana/images/contact-icon/tubefooter.png" width="25px" alt="Nirvana Youtube"></a></td>
    </tr>
    </tbody>
    </table> 
    </div>
    </div>
    </div>
     <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 footer-widget ftr-contact">
    <div id="Contactinfodiv">
    <div class="col-xs-6 col-sm-3 col-md-12 col-lg-12 n_sec">
    <i class="fa fa-phone" aria-hidden="true"></i>&nbsp;<a dir="ltr" :href="'tel:'+currentLocationDetails.Phone_number||'+971 2 304 3322'"> {{currentLocationDetails.Phone_number||'+971 2 304 3322'}}</a>  
    </div> 
    <div class="col-xs-12 col-sm-3 col-md-12 col-lg-12 n_sec">
    <i class="fa fa-print" aria-hidden="true"></i>&nbsp;<span dir="ltr">{{currentLocationDetails.Fax_number||currentLocationDetails.Phone_number||'+971 2 304 3322'}}</span></div> 
    <div class="col-xs-12 col-sm-3 col-md-12 col-lg-12 n_sec"> 
    <i class="fa fa-envelope" aria-hidden="true"></i> <a :href="'mailto:'+(currentLocationDetails.email_id||'info@ntravel.ae')"> {{currentLocationDetails.email_id||'info@ntravel.ae'}}</a>
    </div> 
    <div class="col-xs-12 col-sm-3 col-md-12 col-lg-12 n_sec"> 
    <i class="fa fa-globe" aria-hidden="true"></i><a :href="currentLocationDetails.website||'http://www.ntravel.ae'" target="_blank"> {{currentLocationDetails.website||'www.ntravel.ae'}}</a></div>
    </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 contact_field_sec">
    <div class="row">
        <h5>{{pluckcom('Contact_us_caption',item.component)}}</h5>
        <br>
        <div class="subscribe_sec" style="width: 49%;margin-left: 4px;">
           <h6>Inquire For</h6>
        </div>
        <div class="subscribe_sec" style="margin-top: 5px;">
            <a href="#" class="Subscribebtn" data-toggle="modal" data-target="#subscribe">{{pluckcom('Newsletter_button_label',item.component)}}</a>
        </div>
        
        <div class="subscribe_sec" style="width: 49%;margin-left: 4px;margin-top: 5px;">
            <select id="departmentSel" class="form-control" v-model="selectedDepartment">
                <option v-for="item in allDepartmentList" :value="item">{{item.Title}}</option>
            </select>
        </div>
    </div>
    <form>
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 flx_sec" style="padding: 0px;">
    <div class="col-xs-12 col-sm-6" style="padding: 0px;">
    <div class="form-group">
    <input style="font-size: 10px;" v-model="cntusername" type="text" class="form-control textresize" id="txtuname" :placeholder="pluckcom('Name_placeholder',item.component)" required="">
    </div>
    </div>
    <div class="col-xs-12 col-sm-6" style="padding: 0px;">
    <div class="form-group">
    <input style="font-size: 10px;" v-model="cntemail" type="email" class="form-control" id="txtumail" :placeholder="pluckcom('Email_placeholder',item.component)" required="">
    </div>
    </div>
    </div>
    </div>
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 flx_sec" style="padding: 0px;">
    <div class="col-xs-12 col-sm-6" style="padding: 0px;">
    <div class="form-group">
    <input style="font-size: 10px;" type="text" v-model="cntcontact" class="form-control textresize" id="txtcontact" :placeholder="pluckcom('Contact_placeholder',item.component)" onkeydown="return ((((event.keyCode>=48)&amp;&amp;(event.keyCode<=57))||((event.keyCode>=96)&amp;&amp;(event.keyCode<=105))||((event.keyCode==08))||((event.keyCode==46))) &amp;&amp; event.keyCode!=32);" maxlength="15">
    </div>
    </div>
    <div class="col-xs-12 col-sm-6" style="padding: 0px;">
    <div class="form-group">
    <input style="font-size: 10px;" type="text" v-model="cntsubject" class="form-control" id="txtsubject" :placeholder="pluckcom('Subject_placeholder',item.component)">
    </div>
    </div>
    </div>
    </div>
    <div class="row">
    <div class="col-xs-12" style="padding: 0px;">
    <div class="form-group">
    <textarea style="font-size: 10px; height: 70px;" v-model="cntmessage" class="form-control" rows="7" id="txtmessage" :placeholder="pluckcom('Message_placeholder',item.component)"></textarea>
    </div>
    </div>
    </div>
    <div class="f_btn"><a href="#" id="btncontactus" v-on:click.prevent="sendcontactus" class="btn btn-orange btn-block pull-right sb_btn">{{pluckcom('Submit_button_label',item.component)}}</a></div>
    </form>
    <div class="row">
            
        <div class="modal fade" id="subscribe" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content new-content">
    <div class="modal-header">
    <h3>Subscribe for Newsletter</h3>
    <button type="button" class="close" data-dismiss="modal">×</button>
    </div>
    <div class="modal-body book-now-form-wrap">
    <input type="text" class="book-form" v-model="newsltrname" name="name" id="newslname" placeholder="Your Name" title="* Please provide your name" required="required">
    <input type="email" class="book-form" v-model="newsltremail" name="email" id="newslemail" placeholder="Your Email Address" title="* Please provide your email address" required="required">
    <input type="submit" v-on:click="sendnewsletter" class=" mt-20 btn btn-orange btn-block" id="form-submit" value="Send Message" style="float: left;max - width: 100 %;left: 0;">
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="footer_bottom">
    <div class="container" style="width:100%">
    <div class="row flx_sec">
    <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11" id="terms">
    <ul v-if="getdata"></ul>
    <ul :class="textDirection=='rtl'?'border_right':''" v-else v-for="itemh in pluck('Header',content.area_List)" class="flx_package">
    <li><a class="footermenuitms" href="/">{{pluckcom('Home_menu_lable',itemh.component)}}</a></li>
    <li><a class="footermenuitms" href="/Nirvana/aboutus.html">{{pluckcom('About_us_menu_title',itemh.component)}}</a></li>
    <li><a class="footermenuitms" href="/Nirvana/travel-guide.html">{{pluckcom('Member_companies_menu_tiltle',itemh.component)}} </a></li>
    <li><a class="footermenuitms" href="/Nirvana/events.html">{{pluckcom('Events_menu_title',itemh.component)}}</a></li>
    <li><a class="footermenuitms" href="/Nirvana/package.html">{{pluckcom('Packages_menu_title',itemh.component)}}</a></li>
    <li><a class="footermenuitms" href="/Nirvana/offers.html">{{pluckcom('Offer_menu_title',itemh.component)}}</a></li>
    <li><a class="footermenuitms" href="/Nirvana/services.html">{{pluckcom('Services_menu_Icon',itemh.component)}}</a></li>
    <li><a class="footermenuitms" href="/Nirvana/awards.html">{{pluckcom('Awards_menu_title',itemh.component)}}</a></li>
    <li><a class="footermenuitms" href="/Nirvana/contactus.html">{{pluckcom('Contact_us_menu_title',itemh.component)}} </a></li>
    <li><a class="footermenuitms" href="/Nirvana/gallery-list.html">{{pluckcom('Gallery_menu',itemh.component)}}</a></li>
    <li><a class="footermenuitms" href="/Nirvana/blog.html">{{pluckcom('Blog_MenuTitle',itemh.component)}}</a></li>
    <li><a class="footermenuitms" href="/Nirvana/csr.html">{{pluckcom('Community_Services_Title',itemh.component)}}</a></li>
    <li><a class="footermenuitms" href="/Nirvana/terms-and-conditions.html">{{pluckcom('Terms_and_conditions_menu',itemh.component)}}</a></li>
    <li><a class="footermenuitms" href="/Nirvana/privacy-policy.html">{{pluckcom('Privacy_policy_menu',itemh.component)}}</a></li>
    <li><a class="footermenuitms" href="/Nirvana/qhse-policy.html">{{pluckcom('QHSE_policy_menu_',itemh.component)}}</a></li>
    <li><a class="footermenuitms" href="/Nirvana/quality-objectives.html">{{pluckcom('Quality_objective_menu',itemh.component)}}</a></li>
    </ul>
    <p id="CopyRight">{{pluckcom('Copyright_info',item.component)}}</p>
    </div>
    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
    <img class="visaimg img-fluid" src="/Nirvana/images/we-accept.png">
    </div>
    </div>
    </div>
    </div></div></div>`,
    data() {
        return {
            content: null,
            getdata: true,
            newsltrname: '',
            newsltremail: '',
            cntusername: '',
            cntemail: '',
            cntcontact: '',
            cntsubject: '',
            cntmessage: '',
            currentLocationDetails: {},
            textDirection: localStorage.direction,
            key: 0,
            allDepartmentList:[],
            selectedDepartment:{}
        }

    },
    props: {
        newKey: String
    },
    methods: {
        selectLocation: function(brnch) {
            this.currentLocationDetails = brnch;
        },
        getpagecontent: function() {


            var self = this;
            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Master page/Master page/Master page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    self.content = response.data;
                    var footer = self.pluck('Footer', self.content.area_List);
                    var allDepartmentListtemp = self.pluckcom('Department', footer[0].component);
                    if(allDepartmentListtemp==undefined){
                        allDepartmentListtemp=[];
                    }
                    self.allDepartmentList=allDepartmentListtemp;
                    if(self.allDepartmentList!=undefined&&self.allDepartmentList.length>0){
                        self.selectedDepartment= self.allDepartmentList[0]; 
                    }

                    self.getdata = false;
                    self.key = Math.random();
                    self.$eventHub.$on('select-location', self.selectLocation);
                }).catch(function(error) {
                    console.log('Error');
                    this.content = [];
                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);

                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        sendnewsletter: async function() {


            if (!this.newsltrname) {
                alertify.alert('Alert', 'Name required.').set('closable', false);
                return false;
            }
            if (!this.newsltremail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            } else {
                var fromEmail = JSON.parse(localStorage.User).loginNode.email;

                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.newsltrname,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                var filterValue = "type='Subscribe Details' AND keyword3='Contact Us'  AND keyword2='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                    return false;
                }
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertSubscibeData = { type: "Subscribe Details", keyword1: this.newsltrname, keyword2: this.newsltremail, keyword3: "Contact Us", date1: requestedDate, nodeCode: agencyCode };

                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                const url = huburl + portno + "/cms/data";

                let responseObject = await this.gettingCMSData(url, "POST", insertSubscibeData, "en", "");
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, custmail);
                    alertify.alert('Newsletter', 'Thank you for subscribing !');
                } catch (e) {

                }


            }
        },
        async gettingCMSData(cmsURL, methodName, bodyData, languageCode, type) {
            var data = bodyData;
            if (data != null && data != undefined) {
                data = JSON.stringify(data);
            }
            const response = await fetch(cmsURL, {
                method: methodName, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json', 'Accept-Language': languageCode },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                let object = { area_List: [] }
                return object;
            }

        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {
            var allDBData = [];
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.gettingCMSData(cmsURL, "POST", requestObject, "en", "");
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        }

        ,
        sendcontactus: async function() {
            if (!this.cntusername) {
                alertify.alert('Alert', 'Name required.').set('closable', false);

                return false;
            }
            if (!this.cntemail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.cntcontact) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            if (!this.cntsubject) {
                alertify.alert('Alert', 'Subject required.').set('closable', false);
                return false;
            }

            if (!this.cntmessage) {
                alertify.alert('Alert', 'Message required.').set('closable', false);
                return false;
            } else {
                var fromEmail = JSON.parse(localStorage.User).loginNode.email;

                var footer = this.pluck('Footer', this.content.area_List);
                var toEmail = this.pluckcom('To_Email', footer[0].component);
                var departmentName="";
                if(this.selectedDepartment!=undefined&&this.selectedDepartment.Title!=undefined&&this.selectedDepartment.Email_ID!=undefined){
                    toEmail=this.selectedDepartment.Email_ID;
                    departmentName=this.selectedDepartment.Title;
                }

                var postData = {
                    type: "AdminContactUsRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.cntusername,
                    emailId: this.cntemail,
                    contact: this.cntcontact,
                    message: this.cntmessage,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.cntusername,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };


                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Contact Us",
                    keyword1: this.cntusername,
                    keyword2: this.cntemail,
                    keyword3: this.cntcontact,
                    keyword4: this.cntsubject,
                    keyword5:departmentName,
                    text1: this.cntmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                const url = huburl + portno + "/cms/data";
                let responseObject = await this.gettingCMSData(url, "POST", insertContactData, "en", "");
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;

                    sendMailService(emailApi, postData);
                    sendMailService(emailApi, custmail);
                    this.cntemail = '';
                    this.cntusername = '';
                    this.cntcontact = '';
                    this.cntmessage = '';
                    this.cntsubject = '';
                    alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
                } catch (e) {

                }



            }
        },
    },
    mounted: function() {
        // document.onreadystatechange = () => {
        //  if (document.readyState == "complete") {
        if (localStorage.direction == 'rtl') {
            // $(".arborder").addClass("arbrder_left");
            $(".ar_direction").addClass("ar_direction1");
            // $(".ajs-modal").addClass("ar_direction1");
        } else {
            //   $(".arborder").removeClass("arbrder_left");
            $(".ar_direction").removeClass("ar_direction1");
            //    $(".ajs-modal").removeClass("ar_direction1");
        }
        // }
        // }

        this.getpagecontent();

    },
    created: function() {
        this.$eventHub.$on('select-location', this.selectLocation);
    },
    beforeDestroy: function() {
        this.$eventHub.$off('select-location');
    }
});

var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});







/**
 * jquery.dlmenu.js v1.0.1
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Codrops
 * http://www.codrops.com
 */
;
(function($, window, undefined) {

    'use strict';

    // global
    var Modernizr = window.Modernizr,
        $body = $('body');

    $.DLMenu = function(options, element) {
        this.$el = $(element);
        this._init(options);
    };

    // the options
    $.DLMenu.defaults = {
        // classes for the animation effects
        animationClasses: { classin: 'dl-animate-in-1', classout: 'dl-animate-out-1' },
        // callback: click a link that has a sub menu
        // el is the link element (li); name is the level name
        onLevelClick: function(el, name) { return false; },
        // callback: click a link that does not have a sub menu
        // el is the link element (li); ev is the event obj
        onLinkClick: function(el, ev) { return false; }
    };

    $.DLMenu.prototype = {
        _init: function(options) {

            // options
            this.options = $.extend(true, {}, $.DLMenu.defaults, options);
            // cache some elements and initialize some variables
            this._config();

            var animEndEventNames = {
                    'WebkitAnimation': 'webkitAnimationEnd',
                    'OAnimation': 'oAnimationEnd',
                    'msAnimation': 'MSAnimationEnd',
                    'animation': 'animationend'
                },
                transEndEventNames = {
                    'WebkitTransition': 'webkitTransitionEnd',
                    'MozTransition': 'transitionend',
                    'OTransition': 'oTransitionEnd',
                    'msTransition': 'MSTransitionEnd',
                    'transition': 'transitionend'
                };
            // animation end event name
            this.animEndEventName = animEndEventNames[Modernizr.prefixed('animation')] + '.dlmenu';
            // transition end event name
            this.transEndEventName = transEndEventNames[Modernizr.prefixed('transition')] + '.dlmenu',
                // support for css animations and css transitions
                this.supportAnimations = Modernizr.cssanimations,
                this.supportTransitions = Modernizr.csstransitions;

            this._initEvents();

        },
        _config: function() {
            this.open = false;
            this.$trigger = this.$el.children('.dl-trigger');
            this.$menu = this.$el.children('ul.dl-menu');
            this.$menuitems = this.$menu.find('li:not(.dl-back)');
            this.$el.find('ul.dl-submenu').prepend('<li class="dl-back"><a href="#">back</a></li>');
            this.$back = this.$menu.find('li.dl-back');
            $(".dl-trigger").unbind('click');
        },
        _initEvents: function() {

            var self = this;

            this.$trigger.on('click.dlmenu', function() {

                if (self.open) {
                    self._closeMenu();
                } else {
                    self._openMenu();
                }
                return false;

            });

            this.$menuitems.on('click.dlmenu', function(event) {

                event.stopPropagation();

                var $item = $(this),
                    $submenu = $item.children('ul.dl-submenu');

                if ($submenu.length > 0) {

                    var $flyin = $submenu.clone().css('opacity', 0).insertAfter(self.$menu),
                        onAnimationEndFn = function() {
                            self.$menu.off(self.animEndEventName).removeClass(self.options.animationClasses.classout).addClass('dl-subview');
                            $item.addClass('dl-subviewopen').parents('.dl-subviewopen:first').removeClass('dl-subviewopen').addClass('dl-subview');
                            $flyin.remove();
                        };

                    setTimeout(function() {
                        $flyin.addClass(self.options.animationClasses.classin);
                        self.$menu.addClass(self.options.animationClasses.classout);
                        if (self.supportAnimations) {
                            self.$menu.on(self.animEndEventName, onAnimationEndFn);
                        } else {
                            onAnimationEndFn.call();
                        }

                        self.options.onLevelClick($item, $item.children('a:first').text());
                    });

                    return false;

                } else {
                    self.options.onLinkClick($item, event);
                }

            });

            this.$back.on('click.dlmenu', function(event) {

                var $this = $(this),
                    $submenu = $this.parents('ul.dl-submenu:first'),
                    $item = $submenu.parent(),

                    $flyin = $submenu.clone().insertAfter(self.$menu);

                var onAnimationEndFn = function() {
                    self.$menu.off(self.animEndEventName).removeClass(self.options.animationClasses.classin);
                    $flyin.remove();
                };

                setTimeout(function() {
                    $flyin.addClass(self.options.animationClasses.classout);
                    self.$menu.addClass(self.options.animationClasses.classin);
                    if (self.supportAnimations) {
                        self.$menu.on(self.animEndEventName, onAnimationEndFn);
                    } else {
                        onAnimationEndFn.call();
                    }

                    $item.removeClass('dl-subviewopen');

                    var $subview = $this.parents('.dl-subview:first');
                    if ($subview.is('li')) {
                        $subview.addClass('dl-subviewopen');
                    }
                    $subview.removeClass('dl-subview');
                });

                return false;

            });

        },
        closeMenu: function() {
            if (this.open) {
                this._closeMenu();
            }
        },
        _closeMenu: function() {
            var self = this,
                onTransitionEndFn = function() {
                    self.$menu.off(self.transEndEventName);
                    self._resetMenu();
                };

            this.$menu.removeClass('dl-menuopen');
            this.$menu.addClass('dl-menu-toggle');
            this.$trigger.removeClass('dl-active');

            if (this.supportTransitions) {
                this.$menu.on(this.transEndEventName, onTransitionEndFn);
            } else {
                onTransitionEndFn.call();
            }

            this.open = false;
        },
        openMenu: function() {
            if (!this.open) {
                this._openMenu();
            }
        },
        _openMenu: function() {
            var self = this;
            // clicking somewhere else makes the menu close
            $body.off('click').on('click.dlmenu', function() {
                self._closeMenu();
            });
            this.$menu.addClass('dl-menuopen dl-menu-toggle').on(this.transEndEventName, function() {
                $(this).removeClass('dl-menu-toggle');
            });
            this.$trigger.addClass('dl-active');
            this.open = true;
        },
        // resets the menu to its original state (first level of options)
        _resetMenu: function() {
            this.$menu.removeClass('dl-subview');
            this.$menuitems.removeClass('dl-subview dl-subviewopen');
        }
    };

    var logError = function(message) {
        if (window.console) {
            window.console.error(message);
        }
    };

    $.fn.dlmenu = function(options) {
        if (typeof options === 'string') {
            var args = Array.prototype.slice.call(arguments, 1);
            this.each(function() {
                var instance = $.data(this, 'dlmenu');
                if (!instance) {
                    logError("cannot call methods on dlmenu prior to initialization; " +
                        "attempted to call method '" + options + "'");
                    return;
                }
                if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
                    logError("no such method '" + options + "' for dlmenu instance");
                    return;
                }
                instance[options].apply(instance, args);
            });
        } else {
            this.each(function() {
                var instance = $.data(this, 'dlmenu');
                if (instance) {
                    instance._init();
                } else {
                    instance = $.data(this, 'dlmenu', new $.DLMenu(options, this));
                }
            });
        }
        return this;
    };

})(jQuery, window);







// $(document).ready(function(){
function setpoup() {
    //Login/Signup modal window - by CodyHouse.co
    function ModalSignin(element) {
        this.element = element;
        this.blocks = this.element.getElementsByClassName('js-signin-modal-block');
        this.switchers = this.element.getElementsByClassName('js-signin-modal-switcher')[0].getElementsByTagName('a');
        this.triggers = document.getElementsByClassName('js-signin-modal-trigger');
        this.hidePassword = this.element.getElementsByClassName('js-hide-password');
        this.init();
    };

    ModalSignin.prototype.init = function() {
        var self = this;
        //open modal/switch form
        for (var i = 0; i < this.triggers.length; i++) {
            (function(i) {
                self.triggers[i].addEventListener('click', function(event) {
                    if (event.target.hasAttribute('data-signin')) {
                        event.preventDefault();
                        self.showSigninForm(event.target.getAttribute('data-signin'));
                    }
                });
            })(i);
        }

        //close modal
        this.element.addEventListener('click', function(event) {
            if (hasClass(event.target, 'js-signin-modal') || hasClass(event.target, 'js-close')) {
                event.preventDefault();
                removeClass(self.element, 'cd-signin-modal--is-visible');
            }
        });
        //close modal when clicking the esc keyboard button
        document.addEventListener('keydown', function(event) {
            (event.which == '27') && removeClass(self.element, 'cd-signin-modal--is-visible');
        });

        //hide/show password
        for (var i = 0; i < this.hidePassword.length; i++) {
            (function(i) {
                self.hidePassword[i].addEventListener('click', function(event) {
                    self.togglePassword(self.hidePassword[i]);
                });
            })(i);
        }

        //IMPORTANT - REMOVE THIS - it's just to show/hide error messages in the demo
        this.blocks[0].getElementsByTagName('form')[0].addEventListener('submit', function(event) {
            event.preventDefault();
            self.toggleError(document.getElementById('signin-email'), true);
        });
        this.blocks[1].getElementsByTagName('form')[0].addEventListener('submit', function(event) {
            event.preventDefault();
            self.toggleError(document.getElementById('signup-username'), true);
        });
    };

    ModalSignin.prototype.togglePassword = function(target) {
        var password = target.previousElementSibling;
        ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text'): password.setAttribute('type', 'password');
        target.textContent = ('Hide' == target.textContent) ? 'Show' : 'Hide';
        putCursorAtEnd(password);
    }

    ModalSignin.prototype.showSigninForm = function(type) {
        // show modal if not visible
        !hasClass(this.element, 'cd-signin-modal--is-visible') && addClass(this.element, 'cd-signin-modal--is-visible');
        // show selected form
        for (var i = 0; i < this.blocks.length; i++) {
            this.blocks[i].getAttribute('data-type') == type ? addClass(this.blocks[i], 'cd-signin-modal__block--is-selected') : removeClass(this.blocks[i], 'cd-signin-modal__block--is-selected');
        }
        //update switcher appearance
        var switcherType = (type == 'signup') ? 'signup' : 'login';
        for (var i = 0; i < this.switchers.length; i++) {
            this.switchers[i].getAttribute('data-type') == switcherType ? addClass(this.switchers[i], 'cd-selected') : removeClass(this.switchers[i], 'cd-selected');
        }
    };

    ModalSignin.prototype.toggleError = function(input, bool) {
        // used to show error messages in the form
        toggleClass(input, 'cd-signin-modal__input--has-error', bool);
        toggleClass(input.nextElementSibling, 'cd-signin-modal__error--is-visible', bool);
    }

    var signinModal = document.getElementsByClassName("js-signin-modal")[0];
    if (signinModal) {
        new ModalSignin(signinModal);
    }

    // toggle main navigation on mobile
    var mainNav = document.getElementsByClassName('js-main-nav')[0];
    if (mainNav) {
        mainNav.addEventListener('click', function(event) {
            if (hasClass(event.target, 'js-main-nav')) {
                var navList = mainNav.getElementsByTagName('ul')[0];
                toggleClass(navList, 'cd-main-nav__list--is-visible', !hasClass(navList, 'cd-main-nav__list--is-visible'));
            }
        });
    }

    //class manipulations - needed if classList is not supported
    function hasClass(el, className) {
        if (el.classList) return el.classList.contains(className);
        else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
    }

    function addClass(el, className) {
        var classList = className.split(' ');
        if (el.classList) el.classList.add(classList[0]);
        else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
        if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
    }

    function removeClass(el, className) {
        var classList = className.split(' ');
        if (el.classList) el.classList.remove(classList[0]);
        else if (hasClass(el, classList[0])) {
            var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
            el.className = el.className.replace(reg, ' ');
        }
        if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
    }

    function toggleClass(el, className, bool) {
        if (bool) addClass(el, className);
        else removeClass(el, className);
    }

    //credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
    function putCursorAtEnd(el) {
        if (el.setSelectionRange) {
            var len = el.value.length * 2;
            el.focus();
            el.setSelectionRange(len, len);
        } else {
            el.value = el.value;
        }
    };

}

function jqurufunctions() {
    $('.nav ul li a[href^="#"]').on('click', function(event) {
        var target = $(this.getAttribute('href'));
        if (target.length) {
            event.preventDefault();

            $('html, body').stop().animate({
                scrollTop: target.offset().top - 0
            }, 1200);
        }
    });
    $("#advanceSearch").unbind('click');
    $('#advanceSearch').click(function() {

        $('.ad_view').toggle();
    });
    $('#dl-menu').dlmenu({
        animationClasses: { classin: 'dl-animate-in-3', classout: 'dl-animate-out-3' }
    });
}
$(function() {
    var selectedClass = "";
    $(".filter").click(function() {
        selectedClass = $(this).attr("data-rel");
        $("#gallery").fadeTo(100, 0.1);
        $("#gallery div").not("." + selectedClass).fadeOut().removeClass('animation');
        setTimeout(function() {
            $("." + selectedClass).fadeIn().addClass('animation');
            $("#gallery").fadeTo(300, 1);
        }, 300);
    });
});
// });

function searchArray(nameKey, myArray, tagName) {
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i][tagName] === nameKey) {
            return myArray[i];
        }
    }
}

//Google
function googleInit() {
    console.log('google inited');
    gapi.load('auth2', () => {
        gapi.auth2.init({ client_id: '509151388330-lsap2aj7ace202lmav6l16eflekel2ih.apps.googleusercontent.com' }).then(() => {
            // DO NOT ATTEMPT TO RENDER BUTTON UNTIL THE 'Init' PROMISE RETURNS
            renderButton();
            myGoogleButtonReg();
        });
    })
}

function renderButton() {
    gapi.signin2.render('myGoogleButton', {
        'scope': 'profile email',
        'width': 240,
        'height': 40,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });
}

function myGoogleButtonReg() {
    gapi.signin2.render('myGoogleButtonReg', {
        'scope': 'profile email',
        'width': 240,
        'height': 40,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });
}

function changeShowTxt() {
    var langauagee = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
    if (langauagee == "ar") {
        if ($('.changeShowTxtCls').text() == 'Show') {
           
            $('.changeShowTxtCls').text('تبين');
        }
    } else {
        if ($('.changeShowTxtCls').text() == 'تبين') {
           
            $('.changeShowTxtCls').text('Show');
        }
    }
}