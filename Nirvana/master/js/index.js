$(document).ready(function () {

    if (localStorage.direction == 'rtl') {
        $(".ar_direction").addClass("ar_direction1");
    } else {
        $(".ar_direction").removeClass("ar_direction1");
    }

});
generalInformation = {
    systemSettings: {       
        calendarDisplay: 1,
       
    },
};
const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var flightserchfromComponent = Vue.component('flightserch', {
    data() {
        return {
            access_token: '',
            KeywordSearch: '',
            resultItemsarr: [],
            autoCompleteProgress: false,
            highlightIndex: 0
        }
    },
    props: {
        itemText: String,
        itemId: String,
        placeHolderText: String,
        returnValue: Boolean,
        id: { type: String, default: '', required: false },
        leg: Number,
        //KeywordSearch:String
    },

    template: `<div class="autocomplete">
        <input type="text" :placeholder="placeHolderText" :id="id" :data-aircode="KeywordSearch" autocomplete="off" 
        v-model="KeywordSearch" class="cont_txt01" 
        :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !autoCompleteProgress  }" 
        @input="onSelectedAutoCompleteEvent(KeywordSearch)"
            @keydown.down="down"
            @keydown.up="up"
            @keydown.tab="autoCompleteProgress=false"
            @keydown.esc="autoCompleteProgress=false"
            @keydown.enter="onSelected(resultItemsarr[highlightIndex])"/>
        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>        
        <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length>0">
            <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item)">
                {{ item.label }}
            </li>
        </ul>
    </div>`,


    methods: {
        up: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex < this.resultItemsarr.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
            if (this.$refs.options[this.highlightIndex]) {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            }

        },
        onSelectedAutoCompleteEvent: _.debounce(async function (keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
              this.autoCompleteProgress = true;
              self.resultItemsarr = await getFlightResultUsingElasticSearch(keywordEntered);
            } else {
                this.autoCompleteProgress = false;
                this.resultItemsarr = [];

            }
        }, 100),
        onSelected: function (item) {
            this.KeywordSearch = item.label;
            this.resultItemsarr = [];
            var targetWhenClicked = $(event.target).parent().parent().find("input").attr("id");
            if (maininstance.triptype != 'M') {
                if (event.target.id == "Departurefrom" || targetWhenClicked == "Departurefrom") {
                    $('#DepartureTo').focus();
                } else if (event.target.id == "DepartureTo" || targetWhenClicked == "DepartureTo") {
                    $('#deptDate01').focus();
                }
            } else {
                if (event.target.id == "Departurefrom" || targetWhenClicked == "Departurefrom") {
                    $('#DepartureTo').focus();
                } else if (event.target.id == "DepartureTo" || targetWhenClicked == "DepartureTo") {
                    $('#deptDate01').focus();
                } else if (event.target.id == "DeparturefromLeg2" || targetWhenClicked == "DeparturefromLeg2") {
                    $('#ArrivalfromLeg2').focus();
                } else if (event.target.id == "ArrivalfromLeg2" || targetWhenClicked == "ArrivalfromLeg2") {
                    $('#txtLeg2Date').focus();
                } else if (event.target.id == "DeparturefromLeg3" || targetWhenClicked == "DeparturefromLeg3") {
                    $('#ArrivalfromLeg3').focus();
                } else if (event.target.id == "ArrivalfromLeg3" || targetWhenClicked == "ArrivalfromLeg3") {
                    $('#txtLeg3Date').focus();
                } else if (event.target.id == "DeparturefromLeg4" || targetWhenClicked == "DeparturefromLeg4") {
                    $('#ArrivalfromLeg4').focus();
                } else if (event.target.id == "ArrivalfromLeg4" || targetWhenClicked == "ArrivalfromLeg4") {
                    $('#txtLeg4Date').focus();
                } else if (event.target.id == "DeparturefromLeg5" || targetWhenClicked == "DeparturefromLeg5") {
                    $('#ArrivalfromLeg5').focus();
                } else if (event.target.id == "ArrivalfromLeg5" || targetWhenClicked == "ArrivalfromLeg5") {
                    $('#txtLeg5Date').focus();
                } else if (event.target.id == "DeparturefromLeg6" || targetWhenClicked == "DeparturefromLeg6") {
                    $('#ArrivalfromLeg6').focus();
                } else if (event.target.id == "ArrivalfromLeg6" || targetWhenClicked == "ArrivalfromLeg6") {
                    $('#txtLeg6Date').focus();
                }

            }

            this.$emit('air-search-completed', item.code, item.label, this.leg);


        }
    }


});

var maininstance = new Vue({
    i18n,
    el: '#indexmain',
    name: 'indexmain',
    data: {
        content: {
            area_List: []
        },
        packageSearchResult: [],
        allPackages: [],
        getdata: true,
        toppackages: null,
        uaeattractions: null,
        OfferPackages: null,
        Services: null,
        getservice: false,
        ServiceTitle: '',
        getpackage: false,
        getuaepackage: false,
        getSpecialPackage: false,
        triptype: "O",
        placeholderfrom: "Enter Airport or City",
        placeholderTo: "Enter Airport or City",
        placeholderDepartureDate: "Enter Departure Date",
        placeholderReturnDate: "Enter Returning Date",
        Flight_form_caption: "",
        package_form_caption: "",
        package_search_field: "",
        Oneway: "",
        Round_trip: "",
        Multicity: "",
        Advance_search_label: "",
        preferred_airline_placeholder: "",
        Direct_flight_option_label: "",
        Flight_tab_name: "",
        Hotel_tab_name: "",
        Package_tab_name: "",
        BannerTitle_1: "",
        BannerTitle_2: "",
        CityFrom: '',
        CityTo: '',
        validationMessage: '',
        adtcount: 1,
        chdcount: 0,
        infcount: 0,
        preferAirline: '',
        returnValue: true,
        legcount: 1,
        legs: [],
        cityList: [],
        adt: "adult",
        adults: [

            { 'value': 1, 'text': '1 Adult' },
            { 'value': 2, 'text': '2 Adults' },
            { 'value': 3, 'text': '3 Adults' },
            { 'value': 4, 'text': '4 Adults' },
            { 'value': 5, 'text': '5 Adults' },
            { 'value': 6, 'text': '6 Adults' },
            { 'value': 7, 'text': '7 Adults' },
            { 'value': 8, 'text': '8 Adults' },
            { 'value': 9, 'text': '9 Adults' }
        ],
        children: [
            { 'value': 0, 'text': 'Children (2-11 yrs)' },
            { 'value': 1, 'text': '1 Child' },
            { 'value': 2, 'text': '2 Children' },
            { 'value': 3, 'text': '3 Children' },
            { 'value': 4, 'text': '4 Children' },
            { 'value': 5, 'text': '5 Children' },
            { 'value': 6, 'text': '6 Children' },
            { 'value': 7, 'text': '7 Children' },
            { 'value': 8, 'text': '8 Children' }

        ],
        infants: [
            { 'value': 0, 'text': 'Infants (0-1 yr)' },
            { 'value': 1, 'text': '1 Infant' }

        ],
        cabinclass: [{ 'value': 'Y', 'text': 'Economy' },
        { 'value': 'C', 'text': 'Business' },
        { 'value': 'F', 'text': 'First' }
        ],
        selected_cabin: 'Y',
        selected_adult: 1,
        selected_child: 0,
        selected_infant: 0,
        totalAllowdPax: 9,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        arabic_dropdown: '',
        mainBanner: null,
        childLabel: "",
        childrenLabel: "",
        adultLabel: "",
        adultsLabel: "",
        ageLabel: "",
        agesLabel: "",
        infantLabel: "",
        searchBtnLabel: "",
        addUptoLabel: "",
        tripsLabel: "",
        tripLabel: "",
        hotelInit: Math.random(), //hotel init
        packagePag: {},
        pkgHtml: ""

    },
    methods: {
        Departurefrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (AirportCode == this.CityTo) {
                    this.returnValue = false;
                    this.validationMessage = "Departure and arrival airports should not be same !";
                    var self = this;
                    this.CityFrom = '';
                    setTimeout(function () {
                        self.validationMessage = '';
                    }, 1500);
                } else {
                    this.returnValue = true;
                    this.CityFrom = AirportCode;
                }
            } else {
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    this.cityList[index].from = AirportCode
                } else {
                    this.cityList.push({
                        id: leg,
                        from: AirportCode,
                        to: ''

                    });
                }
            }


        },
        Arrivalfrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (this.CityFrom == AirportCode) {
                    this.returnValue = false;
                    this.validationMessage = "Departure and arrival airports should not be same !";
                    this.CityTo = '';
                    var self = this;
                    setTimeout(function () {
                        self.validationMessage = '';
                    }, 1500);
                } else {
                    this.returnValue = true;
                    this.CityTo = AirportCode;
                }
            } else {
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    this.cityList[index].to = AirportCode
                } else {
                    this.cityList.push({
                        id: leg,
                        from: '',
                        to: AirportCode

                    });
                }
            }
        },
        SearchFlight: function () {

            var Departuredate = $('#deptDate01').val() == "" ? "" : $('#deptDate01').datepicker('getDate');
            if (!this.CityFrom) {
                this.validationMessage = "Please fill origin !";
                var self = this;
                setTimeout(function () {
                    self.validationMessage = '';
                }, 1500);
                return false;
            }
            if (!this.CityTo) {
                this.validationMessage = "Please fill destination !";
                var self = this;
                setTimeout(function () {
                    self.validationMessage = '';
                }, 1500);
                return false;
            }
            if (!Departuredate) {
                this.validationMessage = "Please choose departure date !";
                var self = this;
                setTimeout(function () {
                    self.validationMessage = '';
                }, 1500);
                return false;
            }
            var sec1TravelDate = moment(Departuredate).format('DD|MM|YYYY');

            var sectors = this.CityFrom + '-' + this.CityTo + '-' + sec1TravelDate;
            if (this.triptype == 'R') {
                var ArrivalDate = $('#retDate').val() == "" ? "" : $('#retDate').datepicker('getDate');
                if (!isNullorUndefined(ArrivalDate)) {
                    ArrivalDate = moment(ArrivalDate).format('DD|MM|YYYY');
                    if (!ArrivalDate) {
                        this.validationMessage = "Please choose return date !";
                        var self = this;
                        setTimeout(function () {
                            self.validationMessage = '';
                        }, 1500);
                        return false;
                    } else {
                        sectors += '/' + this.CityTo + '-' + this.CityFrom + '-' + ArrivalDate;
                    }
                } else {
                    this.validationMessage = "Please choose return date !";
                    var self = this;
                    setTimeout(function () {
                        self.validationMessage = '';
                    }, 1500);
                    return false;
                }
            }
            if (this.triptype == 'M') {
                for (var legValue = 1; legValue <= this.legcount; legValue++) {
                    var temDeparturedate = $('#txtLeg' + (legValue + 1) + 'Date').val() == "" ? "" : $('#txtLeg' + (legValue + 1) + 'Date').datepicker('getDate');
                    if (temDeparturedate != "" && this.cityList[legValue - 1].from != "" && this.cityList[legValue - 1].to != "") {
                        var departureFrom = this.cityList[legValue - 1].from;
                        var arrivalTo = this.cityList[legValue - 1].to;
                        var travelDate = moment(temDeparturedate).format('DD|MM|YYYY');
                        sectors += '/' + departureFrom + '-' + arrivalTo + '-' + travelDate;
                    } else {
                        this.validationMessage = "Please fill the Trip " + (legValue + 1) + "   fields !";
                        var self = this;
                        setTimeout(function () {
                            self.validationMessage = '';
                        }, 1500);
                        return false;
                    }
                }
            }
            if ($('#checkbox-list-6').is(':checked')) {
                var directFlight = "DF";
            } else {
                var directFlight = "AF";
            }
            var adult = this.selected_adult;
            var child = this.selected_child;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            var searchUrl = '/Flights/flight-listing.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-all-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
            // searchUrl = searchUrl.toLocaleLowerCase();
            window.location.href = searchUrl;
        },
        AddNewLeg() {
            if (this.legcount <= 5) {
                ++this.legcount;
                var legno = 1;
                if (this.cityList.length > 0) {
                    legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                    legno = legno + 1;
                }

                this.cityList.push({
                    id: legno,
                    from: '',
                    to: ''
                });

            }
            console.log(this.cityList);
        },
        DeleteLeg(leg) {
            var legs = this.legcount;
            if (legs > 1) {
                --this.legcount;
                var legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    this.cityList.splice(index, 1);

                }


            }
        },
        setCalender() {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = generalInformation.systemSettings.calendarDisplay;
            $("#deptDate01").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);

                }
            });

            $("#retDate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) { }
            });

            $("#txtLeg2Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg3Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg2Date").val();
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg4Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg3Date").val();
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg5Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg4Date").val();
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg6Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg5Date").val();
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) { }
            });


        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },

        getPagecontent: async function () {
            var self = this;
            // self.toppackages = null;

            var restObject = await gettingRestClientObject();
            getAgencycode(async function (response) {

                self.content = {
                    area_List: []
                };
                self.toppackages = null;
                self.OfferPackages = null;
                self.mainBanner = null;
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';

                var homecms = commonPath + Agencycode + '/Template/Home/Home/Home.ftl';
                var topackageurl = huburl + portno + commonPath + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';
                var ourserviceurl = huburl + portno + commonPath + Agencycode + '/Master Table/Services/Services/Services.ftl';
                var cmsurl = huburl + portno + homecms;
                var pkg = [];
                var pckgResult = null;


                var generalPageURL = huburl + portno + commonPath + Agencycode + '/Template/General Details/General Details/General Details.ftl';
                var generalTempData = await restObject.gettingCMSData(generalPageURL, "GET", undefined, langauage, "");


                // var packagePag = {};
                var attractionPag = {};
                if (generalTempData != undefined && generalTempData.area_List != undefined) {

                    if (generalTempData.area_List.length > 2) {
                        self.packagePag = await restObject.getAllMapData(generalTempData.area_List[2].Package_Page.component);

                    }
                    if (generalTempData.area_List.length > 5) {
                        attractionPag = await restObject.getAllMapData(generalTempData.area_List[5].Attraction_Page.component);

                    }
                }

                var cmsUrlsArr = [topackageurl, ourserviceurl, cmsurl];
                var headers = { headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage } };

                axios.all(cmsUrlsArr.map(function (url) { return axios.get(url, headers); }))
                    .then(axios.spread(function (topackage, ourservice, cms) {

                        //CMS
                        self.content = cms.data;

                        // var bannerCompnt = self.pluck('main', self.content.area_List);
                        // var bannerList = self.pluck('Banners', bannerCompnt[0].component);
                        // self.mainBanner = bannerList[0];
                        // var bannerHtml = "";
                        // for (let i = 0; i < self.mainBanner.length; i++) {
                        //     var ImgUri = self.mainBanner[i].Image;
                        //     var active = "";
                        //     if (i == 0) { active = 'active'; }
                        //     bannerHtml += ' <div class="carousel-item ' + active + '">'
                        //         + '<img src="' + ImgUri + '" alt="banner" style="width:100%;">'
                        //         + '<div class="carousel-caption">'
                        //         + '<h3><span>' + self.BannerTitle_1 + '</span><br>' + self.BannerTitle_2 + '</h3>'
                        //         + ' </div>'
                        //         + '</div>'
                        // }
                        //  $(".carousel-inner").empty();
                        //    $(".carousel-inner").append(bannerHtml);
                        self.getdata = false;

                        $(".hotel_section").css("background", "url(" + self.pluck('Home_offer_section', self.content.area_List)[0].component[3].Background + ") 50% 0%");

                        var searchForm = self.pluck('Search_form', self.content.area_List);
                        self.placeholderfrom = self.pluckcom('Flight_departure_placeholder', searchForm[0].component);
                        self.placeholderTo = self.pluckcom('Flight_arrival_placeholder', searchForm[0].component);
                        self.placeholderDepartureDate = self.pluckcom('Flight_departure_date_placeholder', searchForm[0].component);
                        self.placeholderReturnDate = self.pluckcom('Flight_arrival_date_placeholder', searchForm[0].component);
                        self.Flight_form_caption = self.pluckcom('Flight_form_caption', searchForm[0].component);
                        self.package_form_caption = self.pluckcom('Package_Form_Caption', searchForm[0].component);
                        self.package_search_field = self.pluckcom('Package_Search_Place_Holder', searchForm[0].component);
                        self.Oneway = self.pluckcom('Oneway', searchForm[0].component);
                        self.Round_trip = self.pluckcom('Round_trip', searchForm[0].component);
                        self.Multicity = self.pluckcom('Multicity', searchForm[0].component);
                        self.Advance_search_label = self.pluckcom('Advance_search_label', searchForm[0].component);
                        self.preferred_airline_placeholder = self.pluckcom('preferred_airline_placeholder', searchForm[0].component);
                        self.Direct_flight_option_label = self.pluckcom('Direct_flight_option_label', searchForm[0].component);
                        self.Flight_tab_name = self.pluckcom('Flight_tab_name', searchForm[0].component);
                        self.Hotel_tab_name = self.pluckcom('Hotel_tab_name', searchForm[0].component);
                        self.Package_tab_name = self.pluckcom('Package_tab_name', searchForm[0].component);

                        var mainComp = self.pluck('main', self.content.area_List);
                        self.BannerTitle_1 = self.pluckcom('Title_1', mainComp[0].component);
                        self.BannerTitle_2 = self.pluckcom('Title_2', mainComp[0].component);

                        self.childLabel = self.pluckcom('child_label', searchForm[0].component);
                        self.childrenLabel = self.pluckcom('children_label_', searchForm[0].component);
                        self.adultLabel = self.pluckcom('Adult_label', searchForm[0].component);
                        self.adultsLabel = self.pluckcom('adults_label', searchForm[0].component);
                        self.ageLabel = self.pluckcom('Age_Label', searchForm[0].component);
                        self.agesLabel = self.pluckcom('Ages_Label', searchForm[0].component);
                        self.infantLabel = self.pluckcom('Infant_label', searchForm[0].component);

                        self.searchBtnLabel = self.pluckcom('Search_button_label', searchForm[0].component);
                        self.addUptoLabel = self.pluckcom('Add_up_to', searchForm[0].component);
                        self.tripsLabel = self.pluckcom('Trips', searchForm[0].component);
                        self.tripLabel = self.pluckcom('Trip_Label', searchForm[0].component);

                        var packageComp = self.pluck('Top_package_Section', self.content.area_List);
                        self.packageTitle = self.pluckcom('Title', packageComp[0].component);
                        self.packageSubtitle = self.pluckcom('Subtitle', packageComp[0].component);

                        self.cabinclass = [
                            { 'value': 'Y', 'text': self.pluckcom('Economy_class', searchForm[0].component) },
                            { 'value': 'C', 'text': self.pluckcom('Business_class', searchForm[0].component) },
                            { 'value': 'F', 'text': self.pluckcom('First_class', searchForm[0].component) }
                        ];


                        //Package
                        var topackageResult = topackage.data;
                        if (topackageResult != undefined && topackageResult.Values != undefined) {
                            topackageResult = topackageResult.Values;
                        }
                        topackageResult = topackageResult.filter(function (el) {
                            return el.To_Date && (new Date(el.To_Date.replace(/-/g, "/"))).getTime() >= Date.now()
                        });
                        self.allPackages = topackageResult;
                        self.toppackages = topackageResult.filter(function (el) {
                            return el.Status == true &&
                                el.Show_In_Home_Page == true &&
                                el.Holiday_Package == true
                        });


                        self.OfferPackages = topackageResult.filter(function (el) {
                            return el.Status == true &&
                                el.Show_In_Home_Page == true &&
                                el.Offer_Package == true
                        });
                        if (self.toppackages != null) {
                            self.getpackage = true;
                        }
                        if (self.OfferPackages != null) {
                            self.getSpecialPackage = true;
                        }

                        var areaList = self.content.area_List;
                        var Top_package = self.pluckcom('Top_package_Section', areaList);
                        var title = self.pluckcom('Title', Top_package.component);
                        var Subtitles = self.pluckcom('Subtitle', Top_package.component);
                        var btnLabel = self.pluckcom('Button_lable', Top_package.component);



                        var pkgHtml = "";
                        var image;
                        var pkgName;
                        for (let i = 0; i < self.toppackages.length; i++) {
                            image = self.toppackages[i].Thumbnail_Image;
                            pkgName = self.toppackages[i].Name;
                            desc = self.toppackages[i].Short_description;
                            var link = self.getmoreinfo(self.toppackages[i].More_details_page_link, 'pkg');
                            pkgHtml += '<div class="item">' +
                                '<a href="' + link + '">' +
                                '<div class="tour_offer">' +
                                '<img src="' + image + '" class="img-responsive" alt="' + self.toppackages[i].Name + '" />' +
                                '<div class="tour_offer_cont">' +
                                '<h3>' + pkgName + '</h3>' +
                                '</div>' +
                                '</div>' +
                                '</a>' +
                                '</div>';



                        }

                        self.pkgHtml = pkgHtml;
                        // $(".tour_package").empty();

                        // $(".tour_package").append('<div class="container" v-else v-for="item in pluck("Top_package_Section",content.area_List)"> ' +
                        //     '<h4 v-cloak>' + packagePag.Page_Title + '</h4>' +
                        //     '<h5>' + packagePag.Home_Page_Short_Description + '</h5>' +
                        //     ' <div class="owl-carousel owl-theme owl-custom-arrow" id="owl-tour-offers">' +
                        //     '' + pkgHtml + '' +
                        //     '</div>' +
                        //     '<div class="view-all">' +
                        //     '<a href="/Nirvana/package.html">' + packagePag.View_Button_Name + '</a>' +
                        //     '</div>' +
                        //     '</div>');


                        ////////////////Services/////////////////// 
                        var areaList1 = self.content.area_List;
                        var Servicessection = self.pluckcom('Our_Services_Section', areaList1);
                        self.ServiceTitle = self.pluckcom('Title', Servicessection.component);

                        //Services
                        var ourserviceResult = ourservice.data;
                        if (ourserviceResult != undefined && ourserviceResult.Values != undefined) {
                            ourserviceResult = ourserviceResult.Values;
                            self.Services = ourserviceResult;
                            if (ourserviceResult.length > 0) {
                                self.getservice = true;
                                var html_services = "";

                                for (let i = 0; i < self.Services.length; i++) {
                                    image = self.Services[i].Thumbnail_Image;
                                    var link = self.getmoreinfoforservice(self.Services[i].More_details_page_link);//"/Nirvana/serviceinfo.html?page="+self.Services[i].More_details_page_link+"&from=home";

                                    html_services += '<div class="item">' +
                                        '<a href="' + link + '" >' +
                                        '<div class="tour_offer">' +
                                        '<img src="' + image + '" class="img-responsive" alt="' + self.Services[i].Title + '" />' +
                                        '<div class="tour_offer_cont">' +
                                        '<h3>' + self.Services[i].Title + '</h3>' +
                                        '</div>' +
                                        '</div>' +
                                        '</a>' +
                                        '</div>';


                                }
                                $(".uae_attraction").empty();
                                $(".uae_attraction").append('<div class="container"> ' +
                                    '<h4 v-cloak>' + self.ServiceTitle + '</h4>' +
                                    ' <div class="owl-carousel owl-theme owl-custom-arrow" id="owl-uae-attractions">' +
                                    '' + html_services + '' +
                                    '</div>' +
                                    '</div>');
                            }
                        }

                        // hotelSearch.key = Math.random();
                        self.$nextTick(function () { owlcarosl(); });
                        // setTimeout(function () { owlcarosl(); }, 3000);

                    }));
            });
        },
        redirectToLink: function(url) {
            if(url) {
                if (url.indexOf("#hotel") >= 0) {
                    $("#hotelTab").click();
                    setTimeout(function() {
                        window.location.href = "#hotelTab";
                    }, 50);
                } else if (url.indexOf("#flight") >= 0) {
                    $("#flightTab").click();
                    setTimeout(function() {
                        window.location.href = "#flightTab";
                    }, 50);
                } else if (url.indexOf("#package") >= 0) {
                    $("#packageTab").click();
                    setTimeout(function() {
                        window.location.href = "#packageTab";
                    }, 50);
                } else {
                    window.location.href = url;
                }
            }
        },
        onKeyUpAutoCompleteEvent: _.debounce(function (event) {
            var self = this;
            let searchResult = [];

            let keywordEntered = "";
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                keywordEntered = event.target.value;
            }

            if (keywordEntered.length > 2) {


                for (let i = 0; i < this.allPackages.length; i++) {
                    let Destination = this.allPackages[i].Destination;
                    if ((this.allPackages[i].Holiday_Package == true || this.allPackages[i].Special_Package == true) &&
                        this.allPackages[i].Status == true &&
                        Destination != undefined &&
                        Destination.toLowerCase().includes(keywordEntered.toLowerCase())) {
                        searchResult.push(this.allPackages[i]);
                    }

                }


            }
            var resArr = [];
            searchResult.filter(function (item) {
                var i = resArr.findIndex(function (x) { return x.Destination.trim() == item.Destination.trim() });
                if (i <= -1) {
                    resArr.push(item);
                }
                return null;
            });

            this.packageSearchResult = resArr;

        }, 100),
        updateResults: function (item) {
            var link = "/Nirvana/package.html?destination=" + item.Destination;
            window.location.href = link;
        },
        onAdultChange: function () {
            alert(this.selected);
            //this.disp = this.selected;
        },
        onCheck: function (e) {
            var currid = $(e.target).attr('id');
             
            console.log(currid);
            var selectedDate = $("#deptDate01").val();
            $("#retDate").datepicker("option", "minDate", selectedDate);
            $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
        },
        getmoreinfo(url, type) {
            if (type == undefined || type == '') {
                type = "pkg";
            }
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.');
                    url=url.split('/')[url.split('/').length-1];
                    url = "/Nirvana/packageInfo.html?page=" + url + "&from=" + type;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;
        },
        getmoreinfoforservice(url) {
            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.');
                url=url.split('/')[url.split('/').length-1];
                url = "/Nirvana/servicesinfo.html?page=" + url + "&from=service";
            }
            else {
                url = "#";
            }

            return url
        },
    },
    mounted: function () {
        this.setCalender();
        this.getPagecontent();

        // this.getPackageContent();
        if (localStorage.direction == 'rtl') {
            this.arabic_dropdown = "dwn_icon";
            $(".ar_direction").addClass("ar_direction1");
        } else {
            this.arabic_dropdown = "";
            $(".ar_direction").removeClass("ar_direction1");
        }

    },
    updated: function () {
        var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
        var numberofmonths = generalInformation.systemSettings.calendarDisplay;
        $("#deptDate01").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            onSelect: function (selectedDate) {
                $("#retDate").datepicker("option", "minDate", selectedDate);
                $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);

            }
        });

        $("#retDate").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#deptDate01").val();
                $("#retDate").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) { }
        });

        $("#txtLeg2Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#deptDate01").val();
                $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) {
                $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#txtLeg3Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#txtLeg2Date").val();
                $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) {
                $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#txtLeg4Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#txtLeg3Date").val();
                $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) {
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#txtLeg5Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#txtLeg4Date").val();
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) {
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#txtLeg6Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#txtLeg5Date").val();
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) { }
        });

    }
});

  
function owlcarosl() {
    owloffer();
    owltourjs();
    owlpartner();
    owluaejs();
    $('#myCarousel').carousel();
    $(".pageload").hide();
    $("body").css("overflow-y", "auto");
    $('.carousel-inner > div').first().addClass('active');
}
/**Sorting Function for AutoCompelte End**/

function isNullorUndefined(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) { status = true; }
    return status;
}

function onPaxChange(e) {
    var currentadult = parseInt($('#adultdrp option:selected').val());
    var currentchild = parseInt($('#childdrp option:selected').val());
    var currentinfant = parseInt($('#infantdrp option:selected').val());
    var currenttotal = parseInt(currentadult) + parseInt(currentchild);
    var child = 9 - currentadult;
    maininstance.children = [
        { 'value': 0, 'text': 'Children (2-11 yrs)' }
    ];
    for (i = 1; i <= child; i++) {
        maininstance.children.push({ 'value': i, 'text': i + ' Child' + (i > 1 ? 'ren' : '') });
    }
    if (currentchild > child) {
        currentchild = child;
    }
    currentchild = (currentchild < 0) ? 0 : currentchild;
    maininstance.selected_child = currentchild;
    var infant = parseInt(currentadult);
    if (infant + parseInt(currentadult) + parseInt(currentchild) > 9) {
        infant = 9 - (parseInt(currentadult) + parseInt(currentchild));
    }
    infant = ((parseInt(infant) < 0) ? 0 : infant);
    if (infant == 0) maininstance.selected_infant = 0;

    // var infant = (((currenttotal > 2 && currenttotal < 7) && (currentadult > 3)) ? 3 : ((currenttotal > 6) ? (9 - currenttotal) : currentadult));
    maininstance.infants = [
        { 'value': 0, 'text': 'Infants (0-1 yr)' }
    ];
    for (i = 1; i <= infant; i++) {
        maininstance.infants.push({ 'value': i, 'text': i + ' Infant' + (i > 1 ? 's' : '') });
    }
    if (currentinfant > infant) {
        currentinfant = infant;
    }
    currentinfant = (currentinfant < 0) ? 0 : currentinfant;
    maininstance.selected_infant = currentinfant;
}

