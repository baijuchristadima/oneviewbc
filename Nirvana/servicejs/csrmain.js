const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var csrListPage = new Vue({
    i18n,
    el: '#csrMainPage',
    name: 'csrMainPage',
    data: {
        restObject:undefined,
        agencyCode:{},
        direction:'ltr',
        langauage:"en",
        generalPageData:{},
        followersPageData:{},
        listOfCategories:[],
        listOfBannerItem:[],
        categoryType:"",
        box1Data:[],
        box2Data:[],
        box3Data:[],
        allCsrData:[],
        popularCsrData:[],
        instaListData:[],
        getData:false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        instagramAccount: ''

    },
    mounted: function () {
        this.getPagecontent();
    },
    methods: {
        async redirectURL(item){
            if(item!=undefined&&item.More_details_page_link!=undefined){

                var url=await this.restObject.callCSRInfoPage(item.More_details_page_link);
                window.location.href=url;
            }
        },
        async redirectCategoryPage(item){
            let url="/Nirvana/csr.html";
            if(item!=undefined&&item.Id!=undefined&&item.Id!=''){
                url=url+"?type="+item.Id;
            }
            window.location.href=url;
        },
        getPagecontent: async function () {
            var self = this;
            this.restObject=await gettingRestClientObject();
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.direction = langauage == "ar" ? "rtl" : "ltr";
                self.langauage=langauage;
                self.agencyCode=Agencycode;
                var commonPath='/persons/source?path=/B2B/AdminPanel/CMS/';
                var generalPageURL=huburl + portno + commonPath + Agencycode + '/Template/General Details/General Details/General Details.ftl';
                var lisPageURL=huburl + portno + commonPath + Agencycode + '/Master Table/List Of CSR/List Of CSR/List Of CSR.ftl';
                self.constructGeneralData(generalPageURL);
                self.constructContentData(lisPageURL);
            });
        },
        async constructGeneralData(generalPageURL){

            

            

                
            var generalTempData=await this.restObject.gettingCMSData(generalPageURL,"GET",undefined,this.langauage,"");
            if(generalTempData!=undefined&&generalTempData.area_List!=undefined){
                if(generalTempData.area_List.length>7 && generalTempData.area_List[7].CSR_Main_Page!=undefined){
                    var tempPageContentData=await this.restObject.getAllMapData(generalTempData.area_List[7].CSR_Main_Page.component);
                    this.generalPageData=tempPageContentData;
                }
                if(generalTempData.area_List.length>8 && generalTempData.area_List[8].Followers_Details!=undefined){
                    var tempPageContentData=await this.restObject.getAllMapData(generalTempData.area_List[8].Followers_Details.component);
                    this.followersPageData=tempPageContentData;
                }
                if(generalTempData.area_List.length>9 && generalTempData.area_List[9].Social_Media!=undefined){
                    var tempPageContentData=await this.restObject.getAllMapData(generalTempData.area_List[9].Social_Media.component);
                    if(tempPageContentData!=undefined&&tempPageContentData.Insta_User_Name){
                        this.instagramAccount = tempPageContentData.Insta_User_Name;
                        this.constructInstaImages(tempPageContentData.Insta_User_Name);
                    }
                }
                
            }
        },
        async constructContentData(lisPageURL){

            var generalTempData=await this.restObject.gettingCMSData(lisPageURL,"GET",undefined,this.langauage,"");
            var allCsrTempData=[];
            var type =await this.restObject.getQueryStringValue('type');

            if(generalTempData!=undefined&&generalTempData.Values!=undefined){

                allCsrTempData =  generalTempData.Values.filter(
                    function (el) {
                        return el.Status;
                    }
                );
                allCsrTempData.sort( ( a, b) => {
                    return new Date(b.Date).getTime() - new Date(a.Date).getTime();
                });
                var filterValue="type='View Statistics' AND keyword1='CSR Pages'";
                var filteredList=allCsrTempData;
                this.categoryType=type;
                if(type!=undefined&&type!=''){
                    filteredList =  filteredList.filter(
                        function (el) {
                            return el.Category_Id==type;
                        }
                    );
                    filterValue="type='View Statistics' AND keyword1='CSR Pages' AND keyword4='"+type+"'";
                }
                var temPopularList=[];
                var allDBData=await this.restObject.getDbData4Table(this.agencyCode,filterValue,"number1");
               
                if(allDBData!=undefined&&allDBData.length>0){
                   for(let i=0;i<allDBData.length;i++) {
                        for(let k=0;k<filteredList.length;k++){
                            let url=filteredList[k].More_details_page_link;
                            url=url.substring(0, url.lastIndexOf('_'))+".ftl";
                            if(url==allDBData[i].keyword5){
                                filteredList[k].views=allDBData[i].number1;
                                temPopularList.push(filteredList[k]);
                                break;
                            }
                        }
                    }
                }


                var box1TempData=[];
                var box2TempData=[];
                var box3TempData=[];

                for(let i=0;i<filteredList.length;i++){
                    let rowObject=filteredList[i];
                    if(rowObject.User_Image==undefined||rowObject.User_Image==null||rowObject.User_Image==''){
                        rowObject.User_Image='images/comment-user.png';
                    }
                    rowObject.createdDate="";
                    if(rowObject.Date!=undefined&&rowObject.Date!=''){
                        let createdDate=moment(rowObject.Date).format('MMM DD, YYYY');
                        rowObject.createdDate=createdDate;
                    }
                    if(box1TempData.length==0){
                        box1TempData.push(rowObject);
                    }else if(box2TempData.length==0){
                        box2TempData.push(rowObject);
                    }else if(box3TempData.length==0){
                        box3TempData.push(rowObject);
                    }

                }
                var tempBannerList =  filteredList.filter(
                    function (el) {
                        return el.Show_In_Home_Banner;
                    }
                ); 
                var balCSRList=[];
                if(filteredList.length>=3){
                    balCSRList=filteredList.slice(3, filteredList.length);
                }
                this.listOfBannerItem=  tempBannerList; 
                this.box1Data=box1TempData;
                this.box2Data=box2TempData;
                this.box3Data=box3TempData;
                this.allCsrData= balCSRList;
                this.popularCsrData=temPopularList.slice(0, 5);
                
                var tempInstaListData=[];
                tempInstaListData.push({Image:"https://ovit-cms.s3.eu-west-1.amazonaws.com/B2B/AdminPanel/CMS/AGY427/Images/06082019052012.jpg"});
                tempInstaListData.push({Image:"https://ovit-cms.s3.eu-west-1.amazonaws.com/B2B/AdminPanel/CMS/AGY427/Images/06082019052012.jpg"});
                
                
               // this.instaListData=tempInstaListData;
                
                this.owlConstruct();   
            }
            this.constructListOfCategories(allCsrTempData);
            this.getData=true;
            
        },async constructInstaImages(userName){
            var tempInstaListData=[];
            let url="https://www.instagram.com/"+userName+"/";
            var self=this;
            axios.get(url,{ headers: { 'content-type': 'application/json' } }).then(async function (response) {
                const jsonObject1 = response.data.match(/<script type="text\/javascript">window\._sharedData = (.*)<\/script>/)[1].slice(0, -1);
                let jsonObject=JSON.parse(jsonObject1);
                tempInstaListData= await self.restObject.constructInstaImages(jsonObject);
                if(tempInstaListData!=undefined){
                    self.instaListData=tempInstaListData.slice(0,10);
                }
            });
            
        },
        async constructListOfCategories(allBannerTempData){
            var tempListOfCategories=[];
            for(let i=0;i<allBannerTempData.length;i++){
                let there=false;
                for(let k=0;k<tempListOfCategories.length;k++){
                    if(tempListOfCategories[k].Id==allBannerTempData[i].Category_Id){
                        there=true;
                        tempListOfCategories[k].Count=Number(tempListOfCategories[k].Count)+1;
                        break;
                    }
                }
                if(!there){
                    let cateObject={Id:allBannerTempData[i].Category_Id,Name:allBannerTempData[i].Category_Name,Count:1};
                    tempListOfCategories.push(cateObject);
                }
            }
            tempListOfCategories.sort( ( a, b) => {
                var nameA=a.Name.toLowerCase(), nameB=b.Name.toLowerCase();
                if (nameA < nameB) //sort string ascending
                    return -1;
                if (nameA > nameB)
                    return 1;
                return 0;

               // return new Date(b.Date).getTime() - new Date(a.Date).getTime();
            });

            this.listOfCategories=tempListOfCategories;
        },
        async owlConstruct(){
            var self=this;
            Vue.nextTick(function () {
                $("#csr").owlCarousel({
                    autoplay:true,
                    autoPlay : 8000,
                    autoplayHoverPause:true, 
                    stopOnHover : false,  
                    items : 1,
                    margin:10,  
                    lazyLoad : true,
                    navigation : true,
                    itemsDesktop : [1199, 1],
                    itemsDesktopSmall : [991,1],
                      itemsTablet : [600, 1]
                  });
            
                     $( ".owl-prev").html('<i class="fa  fa-angle-left"></i>');
                     $( ".owl-next").html('<i class="fa  fa-angle-right"></i>');
            }.bind(self));
        }
    }
});