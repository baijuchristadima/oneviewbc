
const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var galleryList = new Vue({
    i18n,
    el: '#photosPage',
    name: 'photosPage',
    data: {
        content: null,
        getdata: true,
        Photos: [],
        imageType: '2',
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        cats: [],
    },
    mounted: function () {
        this.getPagecontent();
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {

            var Temparry = [];
            contentArry.map(function (item) {

                if (item[key] != undefined) {

                    Temparry = item[key];
                }
            });

            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var packagepage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Videos/Videos/Videos.ftl';

                axios.get(packagepage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    console.log(response.data);
                    self.content = response.data;
                    self.getdata = false;

                    console.log(response.data);

                    var Result = response.data;
                    if (Result != null || Result.area_List.length > 0) {
                        Result = self.pluck('main', Result.area_List);
                        self.cats = self.pluckcom('Category_List', Result[0].component);
                        if (self.cats.length > 0) {
                            self.cats.forEach(element => {
                                if (element.Videos.length > 0) {
                                    element.Videos.forEach(pic => {
                                        self.Photos.push({
                                            'imgLink': pic.Video,
                                            'cat': element.Name,
                                        })
                                    });
                                }
                            });
                        }
                    }

                    Vue.nextTick(function () {
                        var selectedClass = "";
                        $(".filter").click(function () {
                            selectedClass = $(this).attr("data-rel");
                            $("#gallery").fadeTo(100, 0.1);
                            $("#gallery div").not("." + selectedClass).fadeOut().removeClass('animation');
                            setTimeout(function () {
                                $("." + selectedClass).fadeIn().addClass('animation');
                                $("#gallery").fadeTo(300, 1);
                            }, 300);
                        });
                    }.bind(self));
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });


            });

        }
    }

});