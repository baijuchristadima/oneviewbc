const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var galleryList = new Vue({
    i18n,
    el: '#galleryPage',
    name: 'galleryPage',
    data: {
        content: null,
        getdata: true,
        photoTitle:'',
        videoTitle:'',
        photoUrl:'',
        videoUrl:'',
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    },
    mounted: function () {
        this.getPagecontent();
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
               
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var packagepage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Gallery/Gallery/Gallery.ftl';

                axios.get(packagepage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                  
                    self.content = response.data;
                    self.getdata = false;
                    var Result = response.data;

                    if (Result != null || Result.area_List.length > 0) {
                      
                        Result = self.pluck('main', Result.area_List);
                        self.photoUrl = self.pluckcom('Photos_thumbnail_image', Result[0].component);
                        self.videoUrl=self.pluckcom('Videos_thumbnail_image', Result[0].component);
                        self.photoTitle=self.pluckcom('Photos_label', Result[0].component);
                        self.videoTitle=self.pluckcom('Videos_label', Result[0].component);
                    }

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });


            });

        }
    }

});