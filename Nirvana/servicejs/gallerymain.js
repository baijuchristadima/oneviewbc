const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var galleryListPage = new Vue({
    i18n,
    el: '#galleryListPage',
    name: 'galleryListPage',
    data: {
        restObject:undefined,
        agencyCode:{},
        direction:'ltr',
        langauage:"en",
        pageContentData:{},
        listOfCategories:[],
        listOfImages:[],
        getData:false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    },
    mounted: function () {
        this.getPagecontent();
    },
    methods: {
        async redirectURL(item){
            if(item!=undefined&&item.row_Object!=undefined){
                var url=await this.restObject.callGalleryInfoPage(item.row_Object.More_details_page_link);
                window.location.href=url;
            }
        },
        getPagecontent: async function () {
            var self = this;
            this.restObject=await gettingRestClientObject();
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.direction = langauage == "ar" ? "rtl" : "ltr";
                self.langauage=langauage;
                self.agencyCode=Agencycode;
                var commonPath='/persons/source?path=/B2B/AdminPanel/CMS/';
                var generalPageURL=huburl + portno + commonPath + Agencycode + '/Template/General Details/General Details/General Details.ftl';
                var lisPageURL=huburl + portno + commonPath + Agencycode + '/Master Table/List Of Gallery/List Of Gallery/List Of Gallery.ftl';
                self.constructGeneralData(generalPageURL);
                self.constructContentData(lisPageURL);
            });
        },
        async constructGeneralData(generalPageURL){
            var generalTempData=await this.restObject.gettingCMSData(generalPageURL,"GET",undefined,this.langauage,"");
            if(generalTempData!=undefined&&generalTempData.area_List!=undefined){
                if(generalTempData.area_List.length>6){
                    var tempPageContentData=await this.restObject.getAllMapData(generalTempData.area_List[6].Gallery_Main_Page.component);
                    this.pageContentData=tempPageContentData;
                }
            }
        },
        async constructContentData(lisPageURL){
            let tempListOfImages=[];
            var filterValue="type='View Statistics' AND keyword1='Gallery Pages'";
            var allDBData=await this.restObject.getDbData4Table(this.agencyCode,filterValue,"date1");

            var generalTempData=await this.restObject.gettingCMSData(lisPageURL,"GET",undefined,this.langauage,"");
            if(generalTempData!=undefined&&generalTempData.Values!=undefined){
                var commonPath='/persons/source?path=/';
                for(let i=0;i<generalTempData.Values.length;i++){
                    let rowObject=generalTempData.Values[i];
                    let tagImage="";
                    if(rowObject.Is_Popular==true){
                        tagImage="images/popular-tag.png";
                    }
                    let views="0";
                    let url=rowObject.More_details_page_link;
                    url=url.substring(0, url.lastIndexOf('_'))+".ftl";
                    for(let k=0;k<allDBData.length;k++){
                        if(allDBData[k].keyword5==url){
                            views=allDBData[k].number1;
                            break;
                        }
                    }
                    let uniqueID=rowObject.Category_Name;
                    uniqueID = uniqueID.replace(/ /g, '');
                    let temp={uniqueID:uniqueID,Name:rowObject.Name,Category_Name:rowObject.Category_Name,Views:views,
                                Video_Minutes:"",Home_Image:rowObject.Home_Image,Tag_Image:tagImage,row_Object:rowObject};
                    tempListOfImages.push(temp);
                }
            }
            await this.constructListOfCategories(tempListOfImages);
            this.listOfImages=tempListOfImages;
            this.getData=true;
            this.dataFilterReConstruct();
            
        },
        async constructListOfCategories(tempListOfImages){
            var tempListOfCategories=[];
            let temp1={Name:"All",Value:"all",uniqueID:"all"};
            tempListOfCategories.push(temp1);
            for(let i=0;i<tempListOfImages.length;i++){
                let there=false;
                for(let k=0;k<tempListOfCategories.length;k++){
                    if(tempListOfCategories[k].Value==tempListOfImages[i].Category_Name){
                        there=true;
                        break;
                    }
                }
                if(!there){
                    let uniqueID=tempListOfImages[i].Category_Name;
                    uniqueID = uniqueID.replace(/ /g, '');
                    let temp2={uniqueID:uniqueID,Name:tempListOfImages[i].Category_Name,Value:tempListOfImages[i].Category_Name};
                    tempListOfCategories.push(temp2);
                }
            }
            this.listOfCategories=tempListOfCategories;
        },
        async dataFilterReConstruct(){
            var self=this;
            Vue.nextTick(function () {
                $(".filter-button").click(function () {
                    var value = $(this).attr('data-filter');
                    if(value == "all") {
                        //$('.filter').removeClass('hidden');
                        $('.filter').show('1000');
                    }else {
                        $(".filter").not('.'+value).hide('3000');
                        $('.filter').filter('.'+value).show('3000');
                    }
                });
            }.bind(self));
        }
    }
});