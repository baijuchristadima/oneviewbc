const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var awardList = new Vue({
    i18n,
    el: '#awardPage',
    name: 'awardPage',
    data: {
        content: null,
        getdata: true,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        yearResult: [],
        buttonLabel: '',
        photo_index: ''
    },
    mounted: function() {
        this.getPagecontent();
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function() {
            var self = this;
            getAgencycode(function(response) {

                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var packagepage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Award Info/Award Info/Award Info.ftl';

                axios.get(packagepage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {

                    self.content = response.data;
                    self.getdata = false;
                    Result = response.data;
                    if (Result != null || Result.area_List.length > 0) {
                        self.yearResult = self.pluckcom('Years_List', Result.area_List[0].main.component);

                        $(".awardsbanner").css("background-image", "url(" + self.pluckcom('Banner_Image', Result.area_List[0].main.component) + ")");

                    }

                }).catch(function(error) {
                    console.log('Error');
                    self.content = [];
                });

            });
        },
        showMoreImages: function(event) {
            targetId = event.currentTarget.id;
            var picId = targetId.replace("award_", "photo_");
            $("#" + picId).slideToggle("slow");
            viewPic();
        },
    }
});

function viewPic() {
    $(".awards_pic").owlCarousel({
        items: 3,
        itemsCustom: false,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: [600, 1],
        itemsMobile: [479, 1],
        singleItem: false,
        itemsScaleUp: false,

        //Autoplay
        autoPlay: true,
        stopOnHover: true,

        // Navigation
        navigation: true,
        navigationText: ['<img style="width:3px"  src="images/left-arrow.png">', '<img style="width:3px"  src="images/right-arrow.png">'],
        rewindNav: true,
        scrollPerPage: false,

        //Pagination
        pagination: false,
        paginationNumbers: false,

        // Responsive 
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
    });
}