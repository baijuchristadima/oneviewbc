const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var Packageinfo = new Vue({
    i18n,
    el: '#Packageinfo',
    name: 'Packageinfo',
    data: {
        content: null,
        getdata: true,
        pagecontent: null,
        getpagecontent: true,
        gallery: null,
        galleryon: false,
        packagetabs: null,
        packagename: '',
        refNum: '',
        cityPkg: null,
        weatherData: null,
        weatherImg: 'https://openweathermap.org/img/w/',
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        username: '',
        Emailid: '',
        contactno: '',
        bookdate: '',
        adtcount: 1,
        chdcount: 0,
        infcount: 0,
        message: '',
        hasCity: false,
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var packageurl = getQueryStringValue('page');
                if (packageurl != "") {
                    packageurl = packageurl.split('-').join(' ');
                  //  langauage = packageurl.split('_')[1];
                    packageurl = packageurl.split('_')[0];
                    var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
                    var pageinfourl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Package++Detail+page/Package++Detail+page/Package++Detail+page.ftl';
                    axios.get(topackageurl, {
                        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                    }).then(function (response) {
                        self.content = response.data;
                        var Result = self.pluck('main', self.content.area_List);
                        self.packagetabs = self.pluckcom('Package_tabs', Result[0].component);
                        self.packagename = self.pluckcom('Title', Result[0].component);
                        self.refNum = self.pluckcom('Reference_No', Result[0].component);
                        self.cityPkg = self.pluckcom('City', Result[0].component);
                        if (self.cityPkg.length > 0) {
                            self.getWeather(function (resp) {
                                self.weatherData = resp;
                            });
                        }

                        var imgResult = self.pluckcom('Gallery', Result[0].component);
                        if (imgResult != "") {
                            self.gallery = imgResult;
                            self.galleryon = true;
                            setTimeout(function () { owlcarosl(); }, 3000);
                        }
                        self.getdata = false;
                        self.setclaneder();
                    }).catch(function (error) {
                        console.log('Error');
                        self.content = [];
                        self.getdata = true;
                    });
                    axios.get(pageinfourl, {
                        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                    }).then(function (response) {
                        self.pagecontent = response.data;
                        self.getpagecontent = false
                    }).catch(function (error) {
                        console.log('Error');
                        self.pagecontent = [];
                    });

                }


            });
        },
        bookpackage: function () {
            if (!this.username) {
                alertify.alert('Alert', 'Name required.').set('closable', false);
                return false;
            }
            if (!this.Emailid) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.Emailid.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.contactno) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.contactno.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            this.bookdate = $('#traveldate').val();
            if (!this.bookdate) {
                alertify.alert('Alert', 'pick a date.').set('closable', false);
                return false;
            }
            if (!this.message) {
                alertify.alert('Alert', 'comment required.').set('closable', false);
                return false;
            }
            else {
                 
                var fromPage = getQueryStringValue('from');
                var fromEmail = JSON.parse(localStorage.User).loginNode.email;
                var toEmail = '';

                switch (fromPage.toLocaleLowerCase()) {
                    case 'eve':
                        toEmail = fromEmail;
                        break;
                    case 'pkg':
                        toEmail = fromEmail;
                        break;
                    case 'off':
                        toEmail = fromEmail;
                        break;
                    case 'uae':
                        toEmail = fromEmail;
                        break;

                    default:
                        toEmail = fromEmail;
                        break;
                }
                
                var postData = {
                    type: "PackageBookingRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + '.xhtml?ln=logo',
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    packegeName: this.packagename,
                    personName: this.username,
                    emailAddress: this.Emailid,
                    contact: this.contactno,
                    departureDate: this.bookdate,
                    adults: this.adtcount,
                    child2to5: this.chdcount,
                    child6to11: "0",
                    infants: this.infcount,
                    message: this.message,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(this.Emailid) ? this.Emailid : [this.Emailid],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + '.xhtml?ln=logo',
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.username,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };

                var emailApi = ServiceUrls.emailServices.emailApi;

                sendMailService(emailApi, postData);
                sendMailService(emailApi, custmail);
                alertify.alert('Book package', 'Thank you for booking !');
            }



        },
        setclaneder: function () {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            $("#traveldate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    // $(this).parents('.txt_animation').addClass('focused');
                }
            });
        },
        getWeather: function (callback) {
            var self = this;
            var weatCity = this.cityPkg;
            axios.get('https://api.openweathermap.org/data/2.5/weather?q=' + weatCity + '&units=metric&appid=7c0ff0999be50513159abbf786d93a0a', {
                headers: { 'content-type': 'application/json' }
            }).then(function (response) {
                self.hasCity = true
                callback(response);
            });
        },
    },
    mounted: function () {

        this.getPagecontent();
        var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
        $("#traveldate").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: 1,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            onSelect: function (selectedDate) {
                // $(this).parents('.txt_animation').addClass('focused');
            }
        });


    },
    updated: function () {
        var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
        var self = this;
        if (!this.bookdate) {
            $("#traveldate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    self.bookdate = selectedDate;
                }
            });
        }
    }
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
function owlcarosl() {
    owlgallery();
}

