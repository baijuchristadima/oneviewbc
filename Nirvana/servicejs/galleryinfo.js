const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var galleryInfoPage = new Vue({
    i18n,
    el: '#galleryInfoPage',
    name: 'galleryInfoPage',
    data: {
        restObject:undefined,
        agencyCode:{},
        direction:'ltr',
        langauage:"en",
        pageContentData:{},
        categorySection:{},
        pageDetailInfo:{},
        photoSectionData:{Photos:[]},
        videoSectionData:{},
        videoPlayObjec:{},
        allPhotos:[],
        upNextVideos:[],
        getData:false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    },
    mounted: function () {
        this.getPagecontent();
    },
    methods: {
        async changeVideo(item){
            if(item!=undefined&&item.Video!=undefined){
                this.videoPlayObjec=item;
                this.updateVideoViewCount();
            }
        },
        getPagecontent: async function () {
            var self = this;
            this.restObject=await gettingRestClientObject();
            var packageurl =await this.restObject.getQueryStringValue('page');
            if(packageurl==undefined||packageurl!=''){
                getAgencycode(async function (response) {
                    var Agencycode = response;
                    var huburl = ServiceUrls.hubConnection.cmsUrl;
                    var portno = ServiceUrls.hubConnection.ipAddress;
                    var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                    self.direction = langauage == "ar" ? "rtl" : "ltr";
                    self.langauage=langauage;
                    self.agencyCode=Agencycode;
                    var commonPath='/persons/source?path=/B2B/AdminPanel/CMS/';
                    var generalPageURL=huburl + portno + commonPath + Agencycode + '/Template/General Details/General Details/General Details.ftl';
                    self.constructGeneralData(generalPageURL);

                    var packageName = packageurl.split('_')[0];
                    packageurl = "List Of Gallery/" + packageName + "/" + packageurl;
                    packageurl = packageurl.split('-').join(' ');

                    //packageurl = packageurl.split('-').join(' ');
                    var pageLink="B2B/AdminPanel/CMS/AGY20373/Template/"+packageurl+".ftl";
                    pageLink=huburl + portno + "/persons/source?path=/"+await self.restObject.constructPath(pageLink);
                    self.constructPageDetails(pageLink);

                });
            }
            
        },
        async constructGeneralData(generalPageURL){
            var generalTempData=await this.restObject.gettingCMSData(generalPageURL,"GET",undefined,this.langauage,"");
            if(generalTempData!=undefined&&generalTempData.area_List!=undefined){
                if(generalTempData.area_List.length>6){
                    var tempPageContentData=await this.restObject.getAllMapData(generalTempData.area_List[6].Gallery_Main_Page.component);
                    this.pageContentData=tempPageContentData;
                }
            }
        },
        async constructPageDetails(pageLink){
            var generalTempData=await this.restObject.gettingCMSData(pageLink,"GET",undefined,this.langauage,"");
            if(generalTempData!=undefined&&generalTempData.area_List!=undefined){
                if(generalTempData.area_List.length>0){
                    var tempPhotoSectionData=await this.restObject.getAllMapData(generalTempData.area_List[0].Category_Details.component);
                    this.categorySection=tempPhotoSectionData;
                }
                if(generalTempData.area_List.length>1){
                    var tempPhotoSectionData=await this.restObject.getAllMapData(generalTempData.area_List[1].Gallery_Details.component);
                    this.pageDetailInfo=tempPhotoSectionData;
                }
                if(generalTempData.area_List.length>2){
                    var tempPhotoSectionData=await this.restObject.getAllMapData(generalTempData.area_List[2].Photo_Section.component);
                    this.photoSectionData=tempPhotoSectionData;
                }
                if(generalTempData.area_List.length>3){
                    var tempPhotoSectionData=await this.restObject.getAllMapData(generalTempData.area_List[3].Video_Section.component);
                    var upNextVideosTempData=[];
                    if(tempPhotoSectionData.Videos!=undefined&&tempPhotoSectionData.Videos.length>0){
                        tempPhotoSectionData.Videos= tempPhotoSectionData.Videos.filter(function (el) {
                            return el.Status;
                        });
                        for(let i=0;i<tempPhotoSectionData.Videos.length;i++){
                            upNextVideosTempData.push(tempPhotoSectionData.Videos[i]);
                        }
                        if(upNextVideosTempData.length>0){
                            this.videoPlayObjec=upNextVideosTempData[0];
                        }
                    }
                    this.upNextVideos=upNextVideosTempData;
                    this.videoSectionData=tempPhotoSectionData;
                }
            }
            this.construtcAllPhotos();
            this.getData=true;
            
            await  this.updateVideoViewCount();
            await  this.constructPageViewCounts(pageLink);
            
        },
        async construtcAllPhotos(){
            if(this.photoSectionData!=undefined&&this.photoSectionData.Photos!=undefined){
                let endLoop=7;
                if(this.allPhotos.length>0){
                    endLoop=this.photoSectionData.Photos.length;
                }
                var tempAllPhotos=[];
                for(let i=0;i<endLoop;i++){
                    if(this.photoSectionData.Photos.length>i){
                        tempAllPhotos.push(this.photoSectionData.Photos[i]);
                    }
                }
                this.allPhotos=tempAllPhotos;
                
            }
            
        },async updateVideoViewCount(){
            if(this.videoPlayObjec!=undefined&&this.videoPlayObjec.Name!=undefined&&this.pageDetailInfo!=undefined){
                let lastUpdateDate=await moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');

                var filterValue="type='View Statistics' AND keyword1='Gallery Videos' AND keyword5='"+this.pageDetailInfo.Name.trim()+"_"+this.videoPlayObjec.Name.trim()+"'";
                var allDBData=await this.restObject.getDbData4Table(this.agencyCode,filterValue,"date1");
                var blogDBobject=undefined;
                if(allDBData!=undefined&&allDBData.length>0){
                    blogDBobject=allDBData[0];
                }
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var cmsURL = huburl + portno + '/cms/data';
                var apiMethod="POST";
                var views=1;
                let requestObject={
                    type:"View Statistics",
                    nodeCode:this.agencyCode,
                    keyword1:"Gallery Videos",
                    keyword2:this.pageDetailInfo.Name,
                    keyword3:this.videoPlayObjec.Name,
                    keyword4:this.videoPlayObjec.Title,
                    keyword5:this.pageDetailInfo.Name.trim()+"_"+this.videoPlayObjec.Name.trim(),
                    number1:1,
                    date1:lastUpdateDate
                }
                if(blogDBobject!=undefined){
                    requestObject.number1=Number(blogDBobject.number1)+1;
                    apiMethod="PUT";
                    cmsURL=cmsURL+"/"+blogDBobject.id;
                }
                var responseObject =await this.restObject.gettingCMSData(cmsURL,apiMethod,requestObject,"en","");
               // console.log(responseObject);
            }
            await this.constructVIewCounts();
        },getDataUri:function(url, callback) {
            var image = new Image();
            var timestamp = new Date().getTime();
            image.crossOrigin = "anonymous";
            image.onload = function () {
                var canvas = document.createElement('canvas');
                canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
                canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size
                canvas.getContext('2d').drawImage(this, 0, 0);
                canvas.toBlob(callback,'image/jpg', 1.00)
            };
            image.src = url+ '?' + timestamp;;
        },async downlaodImage(item){
            this.getDataUri(item.Image, function(dataUri) {
                let link=document.createElement('a');
                link.href=window.URL.createObjectURL(dataUri);
                link.download=item.Name;
                link.click();
            });
            /*var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var commonPath='/persons/source?path=/B2B/AdminPanel/CMS/';
            var generalPageURL=huburl + portno + commonPath + this.agencyCode + '/Config/Config.ftl';
            var pageContentTempData=await this.restObject.gettingCMSData(generalPageURL,"GET",undefined,"en","");
            if(pageContentTempData!=undefined&&pageContentTempData.AWS_SETTING!=undefined){
                let accessKEY=pageContentTempData.AWS_SETTING.ACCESS_KEY;
                let secretKey=pageContentTempData.AWS_SETTING.SECRET_KEY;
                let bucketName=pageContentTempData.AWS_SETTING.BUCKET_NAME;
                let regionID=pageContentTempData.AWS_SETTING.REGION_ID;
                AWS.config.credentials = new AWS.Credentials(accessKEY, secretKey);
                AWS.config.region=regionID;
                var s3 = new AWS.S3();
                s3.getObject(
                    { Bucket: bucketName, Key: item.Image.split('.amazonaws.com/')[1] },
                    function (error, data) {
                        if (error != null) {
                        } else {
                            let blob=new Blob([data.Body], {type: data.ContentType});
                            let objectData = data.Body.toString('utf-8');
                            let link=document.createElement('a');
                            link.href=window.URL.createObjectURL(blob);
                            link.download=item.Name;
                            link.click();
                        }
                    }
                );
            }*/
            
        }, async constructVIewCounts(){
            if(this.videoPlayObjec!=undefined&&this.videoPlayObjec.Name!=undefined&&this.pageDetailInfo!=undefined&&this.pageDetailInfo.Name!=undefined){
                var filterValue="type='View Statistics' AND keyword1='Gallery Videos'";
                var allDBData=await this.restObject.getDbData4Table(this.agencyCode,filterValue,"date1");
                let tempVideoObject=[];
                for(let i=0;i<this.upNextVideos.length;i++){
                    let videoObject=this.upNextVideos[i];
                    videoObject.Views="0";
                    for(let k=0;k<allDBData.length;k++){
                        if(this.pageDetailInfo.Name+"_"+videoObject.Name==allDBData[k].keyword5){
                            videoObject.Views=allDBData[k].number1;
                            break;
                        }
                    }
                    tempVideoObject.push(videoObject);
                }
                this.upNextVideos=tempVideoObject;
            }
        }, async constructPageViewCounts(pageLink){

            
            if( (pageLink!=undefined&&pageLink!='' &&this.categorySection!=undefined&&
                this.categorySection.Category_Name!=undefined && this.pageDetailInfo!=undefined && 
                this.pageDetailInfo.Name!=undefined)){

                var commonPath='/persons/source?path=/';
                pageLink=pageLink.split(commonPath)[1];
                var sessionValue=window.sessionStorage.getItem("Gallery_Page");
                if(sessionValue==undefined||sessionValue!=pageLink){
                   
                    
                    var filterValue="type='View Statistics' AND keyword1='Gallery Pages' AND keyword5='"+pageLink+"'";
                    var allDBData=await this.restObject.getDbData4Table(this.agencyCode,filterValue,"date1");
                    var blogDBobject=undefined;
                    if(allDBData!=undefined&&allDBData.length>0){
                        blogDBobject=allDBData[0];
                    }
                    var huburl = ServiceUrls.hubConnection.cmsUrl;
                    var portno = ServiceUrls.hubConnection.ipAddress;
                    var cmsURL = huburl + portno + '/cms/data';
                    var apiMethod="POST";
                    let lastUpdateDate=await moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');

                    let requestObject={
                        type:"View Statistics",
                        nodeCode:this.agencyCode,
                        keyword1:"Gallery Pages",
                        keyword2:this.categorySection.Category_Name,
                        keyword3:this.pageDetailInfo.Name,
                        keyword4:"NA",
                        keyword5:pageLink,
                        number1:1,
                        date1:lastUpdateDate
                    }
                    if(blogDBobject!=undefined){
                        requestObject.number1=Number(blogDBobject.number1)+1;
                        apiMethod="PUT";
                        cmsURL=cmsURL+"/"+blogDBobject.id;
                    }
                    let sessionObject={Value:pageLink};
                    var responseObject =await this.restObject.gettingCMSData(cmsURL,apiMethod,requestObject,"en","");
                    window.sessionStorage.setItem("Gallery_Page",pageLink);
                    
                }
                
            }

        }
    }
});