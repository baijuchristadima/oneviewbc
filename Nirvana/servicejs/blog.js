const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var bloglist = new Vue({
    i18n,
    el: '#blogpage',
    name: 'blogpage',
    data: {
        restObject: undefined,
        agencyCode: {},
        direction: 'ltr',
        titleData: {},
        featuredStory: [],
        box1Data: { rowObject: undefined },
        box2Data: { rowObject: undefined },
        box3Data: { rowObject: undefined },
        headerData: [],
        recentPostList: [],
        trendingList: [],
        mostViewList: [],
        popularList: [],
        getData: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1)

    },
    methods: {
        async redirectBlogPage(blogItem) {
            if (blogItem != undefined && blogItem.Id != undefined && blogItem.More_details_page_link != undefined) {
                await this.restObject.redirectBlogPage(blogItem, this.agencyCode);
            }
        },
        async redirectCategoryPage(headerItem) {
            if (headerItem != undefined && headerItem.id != undefined) {
                window.sessionStorage.setItem('categoryID', headerItem.id);
                window.location.href = "/Nirvana/blog-categories.html?ID=" + headerItem.id;
            }
        },
        async constructTrendingBlogs(masteTempData, dbData, type) {
            var allTrendingItems = [];
            if (masteTempData != undefined && masteTempData.Values != undefined) {

                var lastAddedDate = new Date();
                if (masteTempData.Values.length > 0) {
                    var lastObject = masteTempData.Values[0];
                    lastAddedDate = new Date(lastObject.Created_Date);
                }
                var currentDate = lastAddedDate;
                currentDate.setDate(currentDate.getDate() - 1);
                var allDatas = masteTempData.Values.filter(
                    function(el) {
                        var createDate = new Date(el.Created_Date);
                        return el.Status && (currentDate.getTime() <= createDate.getTime());
                    }
                );
                if (dbData != undefined) {
                    var allDBLatestData = dbData.filter(
                        function(el) {
                            var createDate = new Date(el.date2);
                            return (currentDate.getTime() <= createDate.getTime());
                        }
                    );
                    allDBLatestData.sort((a, b) => {
                        return Number(b.number2) - Number(a.number2);
                    });

                    for (let k = 0; k < allDBLatestData.length; k++) {
                        let rowObject = await this.restObject.checkListByID(allDatas, allDBLatestData[k].keyword1);
                        if (rowObject != undefined) {
                            rowObject = await this.restObject.rowObjectConsturc(rowObject);
                            rowObject.comments = allDBLatestData[k].number3;
                            allTrendingItems.push(rowObject);
                        }
                    }
                }
                /*for(let i=0;i<allDatas.length;i++){
                    let rowObject=await this.restObject.checkListByID(allTrendingItems,allDatas[i].Id);
                    if(rowObject==undefined){
                        rowObject=allDatas[i];
                        rowObject=await this.restObject.rowObjectConsturc(rowObject);
                        allTrendingItems.push(rowObject);
                    }
                }*/
            }
            this.trendingList = allTrendingItems;
        },
        async constructMostViewBlogs(masteTempData, dbData, type) {
            var allMostViewItems = [];
            if (masteTempData != undefined && masteTempData.Values != undefined) {
                var allDatas = masteTempData.Values
                if (dbData != undefined) {
                    var allDBLatestData = dbData;
                    allDBLatestData.sort((a, b) => {
                        return Number(b.number2) - Number(a.number2);
                    });
                    for (let k = 0; k < allDBLatestData.length; k++) {
                        let rowObject = await this.restObject.checkListByID(allDatas, allDBLatestData[k].keyword1);
                        if (rowObject != undefined) {
                            rowObject = await this.restObject.rowObjectConsturc(rowObject);
                            rowObject.comments = allDBLatestData[k].number3;
                            allMostViewItems.push(rowObject);
                        }
                    }
                }
                for (let i = 0; i < allDatas.length; i++) {
                    let rowObject = await this.restObject.checkListByID(allMostViewItems, allDatas[i].Id);
                    if (rowObject == undefined) {
                        rowObject = allDatas[i];
                        rowObject = await this.restObject.rowObjectConsturc(rowObject);
                        allMostViewItems.push(rowObject);
                    }
                }
            }
            this.mostViewList = allMostViewItems;
        },
        async constructAllViewBlogs(masteTempData, dbData, type) {
            var allMostViewItems = [];
            let tempStoryObject = [];
            if (masteTempData != undefined && masteTempData.Values != undefined) {
                var allDatas = masteTempData.Values;
                for (let i = 0; i < allDatas.length; i++) {
                    let rowObject = await this.restObject.checkListByID(allMostViewItems, allDatas[i].Id);
                    if (rowObject == undefined) {
                        rowObject = allDatas[i];
                        rowObject = await this.restObject.rowObjectConsturc(rowObject);
                        allMostViewItems.push(rowObject);
                        if (rowObject.Show_in_Featured_Story != undefined && rowObject.Show_in_Featured_Story) {
                            tempStoryObject.push(rowObject);

                        }
                    }
                }
            }
            this.featuredStory = tempStoryObject;
            this.popularList = allMostViewItems;
        },
        getPagecontent: async function() {
            var self = this;
            this.restObject = await gettingRestClientObject();
            getAgencycode(async function(response) {
                var Agencycode = response;
                self.agencyCode = Agencycode;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.direction = langauage == "ar" ? "rtl" : "ltr";

                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var blogpageURL = huburl + portno + commonPath + Agencycode + '/Template/Blog/Blog/Blog.ftl';
                var masterTableURL = huburl + portno + commonPath + Agencycode + '/Master Table/Blog Info/Blog Info/Blog Info.ftl';

                var blogTempData = await self.restObject.gettingCMSData(blogpageURL, "GET", undefined, langauage, "");
                var masteTempData = await self.restObject.gettingCMSData(masterTableURL, "GET", undefined, langauage, "");

                if (masteTempData != undefined && masteTempData.Values != undefined) {
                    masteTempData.Values = masteTempData.Values.filter(function(el) {
                        return el.Status;
                    });
                    masteTempData.Values.sort((a, b) => {
                        return new Date(b.Created_Date) - new Date(a.Created_Date);
                    });
                }

                self.constructBlogPageData(blogTempData, masteTempData);
            });
        },
        async constructBlogPageData(blogTempData, masteTempData) {
            if (blogTempData != undefined && blogTempData.area_List != undefined) {
                var allDBData = await this.restObject.getDbData4Blogs(this.agencyCode);
                this.constructTrendingBlogs(masteTempData, allDBData, "Trending");
                this.constructMostViewBlogs(masteTempData, allDBData, "Most View");
                this.constructAllViewBlogs(masteTempData, allDBData, "Popular");
                this.constructBoxData(masteTempData, allDBData);
                this.headerDataConstruct(masteTempData);
                this.constructRecentPost(masteTempData);
                var titleDataTemp = await this.restObject.getAllMapData(blogTempData.area_List[0].Page_Details.component);
                this.splitWords(titleDataTemp.Recent_Post_Title, 'Recent Post', titleDataTemp);
                this.splitWords(titleDataTemp.Blog_Category_Title, 'Categories', titleDataTemp);
                this.titleData = titleDataTemp;
                this.getData = true;
            }
        },
        async headerDataConstruct(masteTempData) {
            var allHeaderItems = [];
            if (masteTempData != undefined && masteTempData.Values != undefined) {
                for (let i = 0; i < masteTempData.Values.length; i++) {
                    let boxObject = masteTempData.Values[i];
                    let categoryID = boxObject.Category_Id;
                    let name = boxObject.Category_Name;
                    let rowObject = await this.restObject.checkListByID(allHeaderItems, categoryID);
                    if (rowObject == undefined) {
                        rowObject = { id: categoryID, Name: name, Count: 1 };
                        allHeaderItems.push(rowObject);
                    } else {
                        rowObject.Count = Number(rowObject.Count) + 1;
                    }
                }
            }
            this.headerData = allHeaderItems;
        },
        async constructRecentPost(masteTempData) {
            var allPostItems = [];
            if (masteTempData != undefined && masteTempData.Values != undefined) {
                for (let i = 0; i < masteTempData.Values.length; i++) {
                    if (masteTempData.Values.length > i) {
                        let boxObject = masteTempData.Values[i];
                        let createdDate = moment(boxObject.Created_Date).format('MMM DD YYYY');
                        let title = boxObject.Title;
                        let rowObject = { id: boxObject.Id, title: title, createdDate: createdDate, comments: "1", rowObject: boxObject };
                        allPostItems.push(rowObject);
                    }
                    if (allPostItems.length == 3) {
                        break;
                    }

                }
            }
            this.recentPostList = allPostItems;
        },
        async constructBoxData(masteTempData, allDBData) {
            if (masteTempData != undefined && masteTempData.Values != undefined) {
                var boxDatas = masteTempData.Values.filter(function(el) {
                    return el.Show_in_Main_Blog;
                });
                for (let i = 0; i < 3; i++) {
                    if (boxDatas.length > i) {
                        let boxObject = boxDatas[i];
                        let rowObject = await this.restObject.getBLogDbData(allDBData, boxObject.Id);

                        let requestedDate = moment(boxObject.Created_Date).format('MMMM DD,YYYY');
                        var tagStr = await this.restObject.contstrutcTags(boxObject.Tags);
                        var socialMediaItems = await this.restObject.consturctSocialMedia(boxObject.Social_Media);
                        let boxNewObject = {
                            image: boxObject.Blog_Image,
                            tag: tagStr,
                            title: boxObject.Title,
                            createdDate: requestedDate,
                            views: "0",
                            comments: "0",
                            socialMedia: socialMediaItems,
                            rowObject: boxObject
                        };
                        if (rowObject != undefined) {

                            boxNewObject.views = rowObject.number2;
                            boxNewObject.comments = rowObject.number3;
                        }
                        if (i == 0) {

                            this.box1Data = boxNewObject;
                        } else if (i == 1) {
                            this.box2Data = boxNewObject;
                        } else if (i == 2) {
                            this.box3Data = boxNewObject;
                        }



                    }

                }
            }



        },
        async splitWords(value, type, titleDataTemp) {
            var firstWord = '';
            var secondWord = '';
            if (value != undefined && value != null && value != '') {
                var res = value.split(" ");
                if (res.length > 0) {
                    firstWord = res[0];
                }
                if (res.length > 1) {
                    secondWord = res[1];
                }
            }
            if (type == 'Recent Post') {
                titleDataTemp.postword1 = firstWord;
                titleDataTemp.postword2 = secondWord;
            } else if (type == 'Categories') {
                titleDataTemp.category1 = firstWord;
                titleDataTemp.category2 = secondWord;
            } else if (type == 'Flickr') {
                titleDataTemp.flickr1 = firstWord;
                titleDataTemp.flickr2 = secondWord;
            } else if (type == 'widget') {
                titleDataTemp.widget1 = firstWord;
                titleDataTemp.widget2 = secondWord;
            }
        }
    },
    mounted: function() {
        this.getPagecontent();
    },
});