const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})

var packagelist = new Vue({
    i18n,
    el: '#aboutUsPage',
    name: 'aboutUsPage',
    data: {
        content: null,
        getdata: true,
        packages: null,
        getpackage: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        spPackages: null,
        spTitle: '',
        descTitle: '',
        desc: '',
        pageImage:"",
        chairMsg: '',
        chairImg: '',
        visionMission: '',
        sisters: '',
        abudhabi: '',
        China: '',
        Ksa: '',
        Jordan: '',
        Spain: '',
        Uk: '',
        Egypt: '',
        key: 0,
        Sister_companies_label: "",
        Read_button_label: "",
        Chairman_message_label: "",
        Mission_vision_label: "",
        Page_Description: ""
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function() {
            var self = this;

            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/About Us/About Us/About Us.ftl';
                axios.get(aboutUsPage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (response.data.area_List.length > 0) {
                        self.spTitle = self.pluckcom('Page_Title', response.data.area_List[0].main.component);
                        self.Page_Description = self.pluckcom('Page_Description', response.data.area_List[0].main.component);
                        self.descTitle = self.pluckcom('Description_Heading', response.data.area_List[0].main.component);
                        self.desc = self.pluckcom('Description', response.data.area_List[0].main.component);
                        self.pageImage = self.pluckcom('Page_Image', response.data.area_List[0].main.component);
                        self.chairMsg = self.pluckcom('Chairman_Msg', response.data.area_List[0].main.component);
                        self.chairImg = self.pluckcom('Chairman_Picture', response.data.area_List[0].main.component);
                        self.visionMission = self.pluckcom('Mission_Vision', response.data.area_List[0].main.component);
                        self.sisters = self.pluckcom('Sister_Companies', response.data.area_List[0].main.component);
                        self.abudhabi = self.pluckcom('abudhabi', response.data.area_List[0].main.component);
                        self.China = self.pluckcom('China', response.data.area_List[0].main.component);
                        self.Ksa = self.pluckcom('Ksa', response.data.area_List[0].main.component);
                        self.Jordan = self.pluckcom('Jordan', response.data.area_List[0].main.component);
                        self.Spain = self.pluckcom('Spain', response.data.area_List[0].main.component);
                        self.Uk = self.pluckcom('Uk', response.data.area_List[0].main.component);
                        self.Egypt = self.pluckcom('Egypt', response.data.area_List[0].main.component);

                        self.Sister_companies_label = self.pluckcom('Sister_companies_label', response.data.area_List[0].main.component);
                        self.Read_button_label = self.pluckcom('Read_button_label', response.data.area_List[0].main.component);
                        self.Chairman_message_label = self.pluckcom('Chairman_message_label', response.data.area_List[0].main.component);
                        self.Mission_vision_label = self.pluckcom('Mission_vision_label', response.data.area_List[0].main.component);

                        $(".spHeader").css("background-image", "url(" + self.pluckcom('Banner_Image', response.data.area_List[0].main.component) + ")");

                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        getmoreinfo(url) {

            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url = "/Nirvana/book-now.html?page=" + url;
            } else {
                url = "#";
            }
            return url;
        }
    },
    mounted: function() {
        $("#abudhabi").show();
        this.getPagecontent();
    },
});

function activemap(region) {
    $(".slidesec").hide();
    $("#" + region).show();
}