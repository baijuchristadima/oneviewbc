const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var csrInfoPage = new Vue({
    i18n,
    el: '#csrInfoPage',
    name: 'csrInfoPage',
    data: {
        restObject:undefined,
        agencyCode:{},
        direction:'ltr',
        langauage:"en",
        generalPageData:{},
        followersPageData:{},
        listOfCategories:[],
        popularCsrData:[],
        instaListData:[],
        CSR_Details:{},
        viewCount:"",
        category_Details:{},
        Page_Creator_Details:{},
        getData:false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        instagramAccount: ''
    },
    mounted: function () {
        this.getPagecontent();
    },
    methods: {
        async redirectURL(item){
            if(item!=undefined&&item.More_details_page_link!=undefined){

                var url=await this.restObject.callCSRInfoPage(item.More_details_page_link);
                window.location.href=url;
            }
        },
        async redirectCategoryPage(item){
            let url="/Nirvana/csr.html";
            if(item!=undefined&&item.Id!=undefined&&item.Id!=''){
                url=url+"?type="+item.Id;
            }
            window.location.href=url;
        },
        getPagecontent: async function () {
            var self = this;
            this.restObject=await gettingRestClientObject();
            var packageurl =await this.restObject.getQueryStringValue('page');
            getAgencycode(async function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.direction = langauage == "ar" ? "rtl" : "ltr";
                self.langauage=langauage;
                self.agencyCode=Agencycode;
                var commonPath='/persons/source?path=/B2B/AdminPanel/CMS/';
                var generalPageURL=huburl + portno + commonPath + Agencycode + '/Template/General Details/General Details/General Details.ftl';
                var lisPageURL=huburl + portno + commonPath + Agencycode + '/Master Table/List Of CSR/List Of CSR/List Of CSR.ftl';
                self.constructGeneralData(generalPageURL);
                self.constructContentData(lisPageURL,packageurl);
                

            });
        },
        async construtccsrData(pageLink){
            var generalTempData=await this.restObject.gettingCMSData(pageLink,"GET",undefined,this.langauage,"");
            if(generalTempData!=undefined&&generalTempData.area_List!=undefined){
                if(generalTempData.area_List.length>0 && generalTempData.area_List[0].Category_Details!=undefined){
                    var tempCSR_DetailsData=await this.restObject.getAllMapData(generalTempData.area_List[0].Category_Details.component);
                    this.category_Details=tempCSR_DetailsData;
                }
                if(generalTempData.area_List.length>1 && generalTempData.area_List[1].CSR_Details!=undefined){
                    var tempCSR_DetailsData=await this.restObject.getAllMapData(generalTempData.area_List[1].CSR_Details.component);
                    this.CSR_Details=tempCSR_DetailsData;
                }
                if(generalTempData.area_List.length>3 && generalTempData.area_List[3].Page_Creator_Details!=undefined){
                    var tempPage_Creator_Details=await this.restObject.getAllMapData(generalTempData.area_List[3].Page_Creator_Details.component);
                    if(tempPage_Creator_Details.User_Image==undefined||tempPage_Creator_Details.User_Image==null||tempPage_Creator_Details.User_Image==''){
                        tempPage_Creator_Details.User_Image='images/comment-user.png';
                    }
                    this.Page_Creator_Details=tempPage_Creator_Details;
                }
                this.constructPageViewCounts(pageLink);
            }
            
        },
        async constructGeneralData(generalPageURL){
            var generalTempData=await this.restObject.gettingCMSData(generalPageURL,"GET",undefined,this.langauage,"");
            if(generalTempData!=undefined&&generalTempData.area_List!=undefined){
                if(generalTempData.area_List.length>7 && generalTempData.area_List[7].CSR_Main_Page!=undefined){
                    var tempPageContentData=await this.restObject.getAllMapData(generalTempData.area_List[7].CSR_Main_Page.component);
                    this.generalPageData=tempPageContentData;
                }
                if(generalTempData.area_List.length>8 && generalTempData.area_List[8].Followers_Details!=undefined){
                    var tempPageContentData=await this.restObject.getAllMapData(generalTempData.area_List[8].Followers_Details.component);
                    this.followersPageData=tempPageContentData;
                }
                if(generalTempData.area_List.length>9 && generalTempData.area_List[9].Social_Media!=undefined){
                    var tempPageContentData=await this.restObject.getAllMapData(generalTempData.area_List[9].Social_Media.component);
                    if(tempPageContentData!=undefined&&tempPageContentData.Insta_User_Name){
                        this.instagramAccount = tempPageContentData.Insta_User_Name;
                        this.constructInstaImages(tempPageContentData.Insta_User_Name);
                    }
                }
            }
        },async constructInstaImages(userName){
            var tempInstaListData=[];
            let url="https://www.instagram.com/"+userName+"/";
            var self=this;
            axios.get(url,{ headers: { 'content-type': 'application/json' } }).then(async function (response) {
                const jsonObject1 = response.data.match(/<script type="text\/javascript">window\._sharedData = (.*)<\/script>/)[1].slice(0, -1);
                let jsonObject=JSON.parse(jsonObject1);
                tempInstaListData= await self.restObject.constructInstaImages(jsonObject);
                if(tempInstaListData!=undefined){
                    self.instaListData=tempInstaListData.slice(0,10);
                }
            });
            
        },
        async constructContentData(lisPageURL,packageurl){
            let tempListOfImages=[];
            
            var packageName = packageurl.split('_')[0];
            packageurl = "List Of CSR/" + packageName + "/" + packageurl;
            packageurl = packageurl.split('-').join(' ');

            var generalTempData=await this.restObject.gettingCMSData(lisPageURL,"GET",undefined,this.langauage,"");
            var allCsrTempData=[];
            if(generalTempData!=undefined&&generalTempData.Values!=undefined){
                allCsrTempData =  generalTempData.Values.filter(
                    function (el) {
                        return el.Status;
                    }
                );
                allCsrTempData.sort( ( a, b) => {
                    return new Date(b.Date) - new Date(a.Date);
                });
                var temPopularList=[];
                var filterValue="type='View Statistics' AND keyword1='CSR Pages'";
                var allDBData=await this.restObject.getDbData4Table(this.agencyCode,filterValue,"number1");
                if(allDBData!=undefined&&allDBData.length>0){

                   let packageCheckURL= packageurl;
                   if(packageCheckURL!=undefined&&packageCheckURL!=''){
                    packageCheckURL=packageCheckURL.substring(0, packageCheckURL.lastIndexOf('_'))+".ftl";
                   }
                   for(let i=0;i<allDBData.length;i++) {
                        for(let k=0;k<allCsrTempData.length;k++){
                            let url=allCsrTempData[k].More_details_page_link;
                            url=url.substring(0, url.lastIndexOf('_'))+".ftl";
                            if(url==allDBData[i].keyword5){
                                let rowObject=allCsrTempData[k];
                                rowObject.createdDate="";
                                if(rowObject.Date!=undefined&&rowObject.Date!=''){
                                    let createdDate=moment(rowObject.Date).format('MMM DD, YYYY');
                                    rowObject.createdDate=createdDate;
                                }
                                temPopularList.push(rowObject);
                                if(url.includes(packageCheckURL)){
                                    this.viewCount=allDBData[i].number1;
                                }
                                break;
                            }
                        }
                    }
                }
            }
            this.constructListOfCategories(allCsrTempData);
            if(packageurl!=undefined&&packageurl!=''){
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                //packageurl = packageurl.split('-').join(' ');
                var pageLink="B2B/AdminPanel/CMS/AGY427/Template/"+packageurl+".ftl";
                pageLink=huburl + portno + "/persons/source?path=/"+await this.restObject.constructPath(pageLink);
                this.construtccsrData(pageLink);
            }
            this.getData=true;
        },
        async constructListOfCategories(allBannerTempData){
            var tempListOfCategories=[];
            for(let i=0;i<allBannerTempData.length;i++){
                let there=false;
                for(let k=0;k<tempListOfCategories.length;k++){
                    if(tempListOfCategories[k].Id==allBannerTempData[i].Category_Id){
                        there=true;
                        tempListOfCategories[k].Count=Number(tempListOfCategories[k].Count)+1;
                        break;
                    }
                }
                if(!there){
                    let cateObject={Id:allBannerTempData[i].Category_Id,Name:allBannerTempData[i].Category_Name,Count:1};
                    tempListOfCategories.push(cateObject);
                }
            }
            tempListOfCategories.sort( ( a, b) => {
                var nameA=a.Name.toLowerCase(), nameB=b.Name.toLowerCase();
                if (nameA < nameB) //sort string ascending
                    return -1;
                if (nameA > nameB)
                    return 1;
                return 0;

               // return new Date(b.Date).getTime() - new Date(a.Date).getTime();
            });
            this.listOfCategories=tempListOfCategories;

        }, async constructPageViewCounts(pageLink){
            if( (pageLink!=undefined&&pageLink!='' &&this.category_Details!=undefined&&
                this.category_Details.Category_Name!=undefined && this.CSR_Details!=undefined && 
                this.CSR_Details.Title!=undefined)){
                var commonPath='/persons/source?path=/';
                pageLink=pageLink.split(commonPath)[1];
                var sessionValue=window.sessionStorage.getItem("CSR_Page");
                if(sessionValue==undefined||sessionValue!=pageLink){
                    var filterValue="type='View Statistics' AND keyword1='CSR Pages' AND keyword5='"+pageLink+"'";
                    var allDBData=await this.restObject.getDbData4Table(this.agencyCode,filterValue,"date1");
                    var blogDBobject=undefined;
                    if(allDBData!=undefined&&allDBData.length>0){
                        blogDBobject=allDBData[0];
                    }
                    var huburl = ServiceUrls.hubConnection.cmsUrl;
                    var portno = ServiceUrls.hubConnection.ipAddress;
                    var cmsURL = huburl + portno + '/cms/data';
                    var apiMethod="POST";
                    let lastUpdateDate=await moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let requestObject={
                        type:"View Statistics",
                        nodeCode:this.agencyCode,
                        keyword1:"CSR Pages",
                        keyword2:this.category_Details.Category_Name,
                        keyword3:this.CSR_Details.Title,
                        keyword4:this.category_Details.Category_Id,
                        keyword5:pageLink,
                        number1:1,
                        date1:lastUpdateDate
                    }
                    if(blogDBobject!=undefined){
                        requestObject.number1=Number(blogDBobject.number1)+1;
                        apiMethod="PUT";
                        cmsURL=cmsURL+"/"+blogDBobject.id;
                    }
                    var responseObject =await this.restObject.gettingCMSData(cmsURL,apiMethod,requestObject,"en","");
                    window.sessionStorage.setItem("CSR_Page",pageLink);
                }
            }
        }
    }
});