var qhsePolicy_vue = new Vue({
    el: "#div_qhsePolicy",
    data: {
        Page_Title:'',
        QHSE_Body:''
    },
    mounted() {
        this.getPagecontent();
    },
    methods: {
      pluck(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
          if (item[key] != undefined) {
            Temparry.push(item[key]);
          }
        });
        return Temparry;
      },
      pluckcom(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
          if (item[key] != undefined) {
            Temparry = item[key];
          }
        });
        return Temparry;
      },
      getPagecontent: function () {
        var self = this;
        getAgencycode(function (response) {
          var Agencycode = response;
          var huburl = ServiceUrls.hubConnection.cmsUrl;
          var portno = ServiceUrls.hubConnection.ipAddress;
          var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
          self.dir = langauage == "ar" ? "rtl" : "ltr";
          var feedbackPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/QHSE Policy/QHSE Policy/QHSE Policy.ftl';
          axios.get(feedbackPage, {
            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
          }).then(function (response) {
            self.content = response.data;
            self.getdata = false;
            if (response.data.area_List.length > 0) {
              var mainComp = response.data.area_List[0].main;
              self.Page_Title = self.pluckcom('Page_Title', mainComp.component);
              self.QHSE_Body = self.pluckcom('QHSE_Body', mainComp.component);
            }
          }).catch(function (error) {
            console.log('Error');
            self.content = [];
          });
        });
  
      }   
    }
  });