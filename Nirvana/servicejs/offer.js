const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var packagelist = new Vue({
    i18n,
    el: '#offerpage',
    name: 'offerpage',
    data: {
        restObject:undefined,
        langauage:"en",
        pageCommonData:{},
        offerPackages:[],
        UaeAttarctions:[],
        getpackage: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    },
    methods: {
        getPagecontent: async function () {
            var self = this;
            this.restObject=await gettingRestClientObject();
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                self.langauage=langauage;
                var commonPath='/persons/source?path=/B2B/AdminPanel/CMS/';
                var generalPageURL=huburl + portno + commonPath + Agencycode + '/Template/General Details/General Details/General Details.ftl';
                var listPackageURL=huburl + portno + commonPath + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';
                self.constructGeneralPageDetails(generalPageURL);
                self.constructPackageDetails(listPackageURL);
            });
        },
        async constructGeneralPageDetails(generalPageURL){
            var generalTempData=await this.restObject.gettingCMSData(generalPageURL,"GET",undefined,this.langauage,"");
            var titleDataTemp={};
            if(generalTempData!=undefined&&generalTempData.area_List!=undefined){
                if(generalTempData.area_List.length>4){
                    titleDataTemp =await this.restObject.getAllMapData(generalTempData.area_List[4].Offer_Page.component);
                }
            }
            this.pageCommonData=titleDataTemp;
        },
        async constructPackageDetails(listPackageURL){
            var listPackageTempData=await this.restObject.gettingCMSData(listPackageURL,"GET",undefined,this.langauage,"");
            var offerPackagesTemp=[];
            var UaeAttractiontemp=[];
            if(listPackageTempData!=undefined&&listPackageTempData.Values!=undefined){
                var allValues=listPackageTempData.Values;
                this.getpackage = true;
                offerPackagesTemp = allValues.filter(function (el) {
                    return el.To_Date && (new Date(el.To_Date.replace(/-/g, "/"))).getTime() >= Date.now() && 
                        el.Status == true && el.Offer_Package == true
                });
                UaeAttractiontemp = allValues.filter(function (el) {
                    return el.Status == true &&
                        el.UAE_Attraction == true 
                });
            }
            this.offerPackages =offerPackagesTemp;
            this.UaeAttarctions=UaeAttractiontemp;
        },
        async getmoreinfo(url,item) {
            url=await this.restObject.callPackageInfoPage(url,item);
            window.location.href=url;
        }
    },
    mounted: function () {
        this.getPagecontent();
    },
});