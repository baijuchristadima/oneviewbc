const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var packagelist = new Vue({
    i18n,
    el: '#serivcesPage',
    name: 'serivcesPage',
    data: {
        restObject: undefined,
        langauage: "en",
        pageCommonData: {
            Title: 'NIRVANA SERVICES',
            Description: ''
        },
        Services: [],
        getservice: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    },
    methods: {
        getPagecontent: async function () {
            var self = this;
            this.restObject = await gettingRestClientObject();
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                self.langauage = langauage;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var generalPageURL = huburl + portno + commonPath + Agencycode + '/Template/Service Page/Service Page/Service Page.ftl';
                var listservices = huburl + portno + commonPath + Agencycode + '/Master Table/Services/Services/Services.ftl';
                self.constructGeneralPageDetails(generalPageURL);
                self.constructserivselist(listservices);
            });

        },
        async constructGeneralPageDetails(generalPageURL) {
            var self = this;
            var generalTempData = await this.restObject.gettingCMSData(generalPageURL, "GET", undefined, this.langauage, "");
            var titleDataTemp = {};
            if (generalTempData != undefined && generalTempData.area_List != undefined) {
                if (generalTempData.area_List.length > 0) {
                    titleDataTemp = self.pluck('main', generalTempData.area_List);
                    self.pageCommonData.Title = self.pluckcom('Title', titleDataTemp[0].component);
                    self.pageCommonData.Description = self.pluckcom('Description', titleDataTemp[0].component);

                    $(".servicesbanner").css("background-image", "url(" + self.pluckcom('Background_Image', titleDataTemp[0].component)+ ")");

                }

            }

        },
        async constructserivselist(listservices) {
            var self = this;
            var listserviceTempData = await this.restObject.gettingCMSData(listservices, "GET", undefined, this.langauage, "");

            if (listserviceTempData != undefined && listserviceTempData.Values != undefined) {
                var allValues = listserviceTempData.Values;
                if (allValues.length > 0) {
                    self.Services = allValues;
                    self.getservice = true;
                }

            }

        },
        async getmoreinfo(url) {
            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url=url.split('/')[url.split('/').length-1];
                url = "/Nirvana/servicesinfo.html?page=" + url + "&from=service";
                window.location.href = url;
            }
            else {
                url = "#";
            }


        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
    },
    mounted: function () {
        this.getPagecontent();
    },
});