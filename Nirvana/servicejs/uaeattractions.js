const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var packagelist = new Vue({
    i18n,
    el: '#uaeattraction',
    name: 'uaeattraction',
    data: {
        restObject:undefined,
        langauage:"en",
        pageCommonData:{},
        uaeAttractionPackages:[],
        getpackage: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    },
    methods: {
        getPagecontent: async function () {
            var self = this;
            this.restObject=await gettingRestClientObject();
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                self.langauage=langauage;
                var commonPath='/persons/source?path=/B2B/AdminPanel/CMS/';
                var generalPageURL=huburl + portno + commonPath + Agencycode + '/Template/General Details/General Details/General Details.ftl';
                var listPackageURL=huburl + portno + commonPath + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';
                self.constructGeneralPageDetails(generalPageURL);
                self.constructPackageDetails(listPackageURL);
            });
        },
        async constructGeneralPageDetails(generalPageURL){
            var generalTempData=await this.restObject.gettingCMSData(generalPageURL,"GET",undefined,this.langauage,"");
            var titleDataTemp={};
            if(generalTempData!=undefined&&generalTempData.area_List!=undefined){
                if(generalTempData.area_List.length>5){
                    titleDataTemp =await this.restObject.getAllMapData(generalTempData.area_List[5].Attraction_Page.component);
                }
            }
            this.pageCommonData=titleDataTemp;
        },
        async constructPackageDetails(listPackageURL){
            var listPackageTempData=await this.restObject.gettingCMSData(listPackageURL,"GET",undefined,this.langauage,"");
            var uaeAttractionPackagesTemp=[];
            if(listPackageTempData!=undefined&&listPackageTempData.Values!=undefined){
                var allValues=listPackageTempData.Values;
                this.getpackage = true;
                uaeAttractionPackagesTemp = allValues.filter(function (el) {
                    return el.To_Date && (new Date(el.To_Date.replace(/-/g, "/"))).getTime() >= Date.now() && 
                        el.Status == true && el.UAE_Attraction == true
                });
            }
            this.uaeAttractionPackages =uaeAttractionPackagesTemp;
        },
        async getmoreinfo(url) {
            url=await this.restObject.callPackageInfoPage(url,"uae");
            window.location.href=url;
        }
    },
    mounted: function () {
        this.getPagecontent();
    },
});