const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var packageDetailInfo = new Vue({
    i18n,
    el: '#packageDetailInfo',
    name: 'packageDetailInfo',
    data: {
        restObject: undefined,
        agencyCode: {},
        direction: 'ltr',
        langauage: "en",
        bookFormData: {},
        pageCommonData: {},
        pageContentData: {},
        extraTab: [],
        bookingInfoData: {},
        headerComponent: {},
        footerComponent: {},
        weatherData: {},
        weatherCity: {},
        quantityList: {},
        quantityListInfant: true,
        quantityListChild: true,
        packageInfoDetail: {},
        emailConfiguration: {},
        popularPackage: {
            Banner_Image: "",
            Destination: "",
            Days: "",
            Rating: "",
            Price: "",
            Tag_Image: "",
        },
        packageInfo: {
            AdultSize: "1",
            ChildSize: "0",
            InfantSize: "0",
            AdultTotal: "0",
            ChildTotal: "0",
            InfantTotal: "0",
            Currency: "AED",
            TotalAmount: "0",
            TotalRate: "0",
            TravelDate: "",
            UserName: "",
            UserEmail: "",
            UserPhone: "",
            UserCaptcha: "",
            validCaptcha: ""
        },
        selCategory: "Select",
        selectedList: [],
        relatedTours: [],
        getData: false,
        weatherImg: 'https://openweathermap.org/img/w/',
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        enqtxtname: "",
        enqtxtemail: "",
        enqtxtphone: "",
        enqtxtmsg: "",
        CoupenCode: "",
        CoupenInfo: {
            Amount: "",
            ApplyCoupen: false,
            CoupenCode: "",
            newPrice: ""
        }

    },
    methods: {
        async redirectBlogPage(blogItem) {
            if (blogItem != undefined && blogItem.Id != undefined && blogItem.More_details_page_link != undefined) {
                await this.restObject.redirectBlogPage(blogItem, this.agencyCode);
            }

        },
        async contructSelectedFields() {
            let consValue = "";
            var self = this;
            let rateValue = self.$n(parseFloat(this.packageInfo.TotalRate / self.CurrencyMultiplier), 'currency', self.selectedCurrency);
            for (let i = 0; i < this.selectedList.length; i++) {
                let object = this.selectedList[i];
                let Label = object.Label;
                let LabelValue = object.LabelValue;


                if (Label != undefined && Label != '' && LabelValue != undefined && LabelValue != '') {
                    let rowObject = Label + " : " + LabelValue;
                    if (consValue == '') {
                        consValue = rowObject;
                    } else {
                        consValue = consValue + ",\n" + rowObject;
                    }
                }
            }
            // if (consValue != '') {
            //     consValue = consValue + ",\nTotal : " + rateValue
            // }
            // if (self.CoupenInfo.ApplyCoupen == true) {
            //     var coponprice = self.$n(parseFloat(self.CoupenInfo.Amount / self.CurrencyMultiplier), 'currency', self.selectedCurrency);
            //     var grandprice = self.$n(parseFloat(self.CoupenInfo.newPrice / self.CurrencyMultiplier), 'currency', self.selectedCurrency);
            //     consValue = consValue + ",\nCoupon Discount : " + coponprice;
            //     consValue = consValue + ",\nGrandTotal : " + grandprice;

            // }

            return consValue;
        }, async constructComponents(type) {
            var self = this;
            fetch("/Nirvana/js/downloadTemplate.html").then(function (response) { return response.text() }).then(
                function (html) {
                    var parser = new DOMParser();
                    var doc = parser.parseFromString(html, "text/html");
                    var element = doc.getElementById('mainDIv');
                    var titleDIV = element.querySelector('#titleDIV');
                    var apptitleDIV = element.querySelector('#apptitleDIV');
                    var totalDIV = element.querySelector('#totalDIV');
                    var durationDIV = element.querySelector('#durationDIV');
                    var overviewDIVelement = element.querySelector('#overviewDIV');
                    var logo = element.querySelector('#logo');
                    var contactNumber = element.querySelector('#contactNumber');
                    var contactEmail = element.querySelector('#contactEmail');
                    if (self.headerComponent != undefined && self.headerComponent.Logo != undefined && self.headerComponent.Logo.length > 0) {
                        logo.src = "/Nirvana/website-informations/logo/logo.png" || self.headerComponent.Logo[0].Image;
                    }
                    if (self.headerComponent != undefined && self.headerComponent.Header_phonenumber != undefined) {
                        contactNumber.textContent = self.headerComponent.Header_phonenumber;
                    }

                    titleDIV.textContent = self.packageInfoDetail.Name;
                    apptitleDIV.textContent = self.packageInfoDetail.Applicable_Title;
                    totalDIV.textContent = document.getElementById('totalRateTxt').textContent;//self.packageInfo.TotalRate;
                    durationDIV.textContent = self.bookingInfoData.Days + "/" + self.bookingInfoData.Nights;

                    for (let i = 0; i < self.pageContentData.Package_Overview.length; i++) {
                        let item = self.pageContentData.Package_Overview[i];

                        var rootDIv = document.createElement("div");
                        var detailDIv = document.createElement("div");
                        var ol = document.createElement("ol");

                        rootDIv.setAttribute("style", "font-family: 'Open Sans', sans-serif;font-size:17px;color: #1e1e1e;font-weight: 800;margin-bottom: 5px;padding-bottom: 5px;border-bottom: 1px solid #d9d9d9;");
                        detailDIv.setAttribute("style", "font-family: 'Open Sans', sans-serif;font-size:13px;color: #1e1e1e;font-weight: 400;margin-bottom: 5px;");
                        ol.setAttribute("style", "padding-left: 16px;page-break-inside: avoid;");

                        rootDIv.textContent = item.Tab_name;
                        detailDIv.innerHTML = item.Description;
                        overviewDIVelement.innerHTML = overviewDIVelement.innerHTML + rootDIv.outerHTML;
                        overviewDIVelement.innerHTML = overviewDIVelement.innerHTML + detailDIv.outerHTML;
                    }
                    var priceDIv = document.createElement("div");
                    var table = document.createElement("table");
                    var headerTR = document.createElement("tr");
                    let allPriceHeader = document.getElementById('priceHeadr');
                    let allPriceRows = document.getElementById('priceBodyContent');

                    priceDIv.textContent = self.pageCommonData.Price_Title;
                    priceDIv.setAttribute("style", "font-family: 'Open Sans', sans-serif;font-size:17px;color: #1e1e1e;font-weight: 800;margin-bottom: 5px;padding-bottom: 5px;border-bottom: 1px solid #d9d9d9;");
                    table.setAttribute("style", "width: 100%;float: left; border-collapse: collapse; border: 1px solid #000;font-family: 'Open Sans', sans-serif;font-size:14px;text-align: left;");
                    table.setAttribute("cellspacing", "0");
                    table.setAttribute("cellpadding", "0");
                    table.setAttribute("border", "1");
                    headerTR.setAttribute("style", "page-break-inside: avoid;");
                    for (let i = 0; i < allPriceHeader.childNodes.length - 1; i++) {
                        let rowObject = allPriceHeader.childNodes[i];
                        if (rowObject.tagName) {
                            var headerTH1 = document.createElement("th");
                            headerTH1.setAttribute("style", "background: #1e1e1e;padding: 5px;color: #FFF;font-weight: 500;");
                            headerTH1.textContent = rowObject.textContent;
                            headerTR.innerHTML = headerTR.innerHTML + headerTH1.outerHTML
                        }
                    }
                    table.innerHTML = headerTR.outerHTML;
                    for (let i = 0; i < allPriceRows.childNodes.length; i++) {
                        let rowObject = allPriceRows.childNodes[i];
                        var bodyTR = document.createElement("tr");
                        bodyTR.setAttribute("style", "page-break-inside: avoid;");
                        var j = 0;
                        for (j = 0; j < rowObject.cells.length - 1; j++) {
                            var bodyTH1 = document.createElement("td");
                            bodyTH1.setAttribute("style", "padding: 5px;");
                            bodyTH1.textContent = rowObject.cells[j].textContent;
                            bodyTR.innerHTML = bodyTR.innerHTML + bodyTH1.outerHTML;
                        }
                        table.innerHTML = table.innerHTML + bodyTR.outerHTML;
                    }
                    var imageTitleDIv = document.createElement("div");
                    imageTitleDIv.setAttribute("style", "font-family: 'Open Sans', sans-serif;font-size:17px;color: #1e1e1e;font-weight: 800;margin-bottom: 5px;padding-bottom: 5px;border-bottom: 1px solid #d9d9d9;");
                    imageTitleDIv.textContent = "";
                    var imageMainDiv = document.createElement("div");
                    imageMainDiv.setAttribute("style", "width: 100%;float: left;padding-top: 10px;");

                    for (let i = 0; i < self.pageContentData.Package_Gallery.length; i++) {
                        let item = self.pageContentData.Package_Gallery[i];
                        var rootDIv = document.createElement("div");
                        var imag = document.createElement("img");
                        rootDIv.setAttribute("style", "width: 23%;float: left;padding: 5px 1%;");
                        imag.setAttribute("style", "width: 100%;");
                        imag.src = item.Image;
                        rootDIv.innerHTML = imag.outerHTML;
                        imageMainDiv.innerHTML = imageMainDiv.innerHTML + rootDIv.outerHTML;
                        if (i == 2) {
                            // break;
                        }
                    }
                    var div = document.createElement("div");
                    div.setAttribute("style", "font-family: 'Open Sans', sans-serif;font-size:17px;color: #1e1e1e;font-weight: 800;margin-bottom: 5px;padding-bottom: 5px;border-bottom: 1px solid #d9d9d9;");
                    div.textContent = "";
                    overviewDIVelement.innerHTML = overviewDIVelement.innerHTML + priceDIv.outerHTML + table.outerHTML + div.outerHTML + imageTitleDIv.outerHTML + imageMainDiv.outerHTML;
                    self.constructImage(element, type);
                }).catch(function (err) {
                    console.log('Failed to fetch page: ', err);
                });

        }, async downloadPDF() {
            this.constructComponents('Download');
        }, async print() {
            this.constructComponents('Print');
        }, async printElement(pageContent) {
            var mywindow = window.open("", "new div", "height=400,width=600");
            mywindow.document.write('<html><head><title>' + this.packageInfoDetail.Name + '</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&display=swap" rel="stylesheet">');
            mywindow.document.write('</head><body>');
            mywindow.document.write(pageContent.outerHTML);
            mywindow.document.write('</body></html>');
            mywindow.document.close();
            mywindow.focus();
            mywindow.print();
        }, async loadConverter(pageContent) {

            var iframe = document.createElement('iframe');
            $('body').append($(iframe));
            var iframedoc = iframe.contentDocument || iframe.contentWindow.document;
            iframe.style.width = "100%";
            iframe.style.height = "100%";
            $('body', $(iframedoc)).html(pageContent);
            var self = this;
            html2canvas(iframedoc.body, {
                scale: 1, allowTaint: false,
                useCORS: false
            }).then(function (canvas) {
                var imgData = canvas.toDataURL('image/png', 1.0);
                var imgWidth = 207;
                var pageHeight = 390;
                var imgHeight = (canvas.height * imgWidth / canvas.width);
                var heightLeft = imgHeight;
                var doc = jsPDF('p', 'pt', [(canvas.width / 2), (canvas.height - 150)], 'a4');
                var position = 0;
                doc.addImage(imgData, 'PNG', 0, 0, (canvas.width / 2), 600, '', 'FAST');
                heightLeft -= pageHeight;
                while (heightLeft >= 0) {
                    position = heightLeft - imgHeight;
                    doc.addPage();
                    doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight, '', 'FAST');
                    heightLeft -= pageHeight;
                }
                doc.save(self.packageInfoDetail.Name + '.pdf');
                $('iframe').remove();
            }).catch(function (error) {
                console.log('Error');
                this.content = [];
            });
        }, async constructImage(pageContent, type) {
            let needImages = true;
            let allImages = pageContent.getElementsByTagName("img");
            if (needImages == false || type == 'Print') {
                if (type == 'Download') {
                    this.loadConverter(pageContent);
                } else if (type == 'Print') {
                    this.printElement(pageContent);
                }
            } else {
                this.callBackUR4Download(allImages, 0, pageContent, type);
            }
        }, callBackUR4Download: function (allImages, index, pageContent, type) {
            console.log(type);
            if (allImages.length > index) {
                this.getDataUri(allImages[index], pageContent, index, allImages, type);
            } else {
                if (type == 'Download') {
                    this.loadConverter(pageContent);
                } else if (type == 'Print') {
                    this.printElement(pageContent);
                }
            }
        }, getDataUri: function (img, pageContent, index, allImages, type) {
            var self = this;
            var image = new Image();
            var timestamp = new Date().getTime();
            image.crossOrigin = "anonymous";
            image.onload = function () {
                var canvas = document.createElement('canvas');
                canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
                canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size
                canvas.getContext('2d').drawImage(this, 0, 0);
                img.src = canvas.toDataURL('image/jpg');
                self.callBackUR4Download(allImages, Number(index) + 1, pageContent, type);
            };
            image.src = img.src + '?' + timestamp;;
        }, async getBlob(img, pageContent, need2Go, type, AWS_SETTING) {
            let accessKEY = AWS_SETTING.ACCESS_KEY;
            let secretKey = AWS_SETTING.SECRET_KEY;
            let bucketName = AWS_SETTING.BUCKET_NAME;
            let regionID = AWS_SETTING.REGION_ID;
            AWS.config.credentials = new AWS.Credentials(accessKEY, secretKey);
            AWS.config.region = regionID;
            var s3 = new AWS.S3();
            var self = this;
            await s3.getObject(
                { Bucket: bucketName, Key: img.src.split('.amazonaws.com/')[1] },
                async function (error, data) {
                    if (error != null) {
                        console.log("EROOR");
                    } else {
                        let blob = new Blob([data.Body], { type: data.ContentType });
                        let url = await window.URL.createObjectURL(blob);
                        img.src = url;
                        if (need2Go == true) {
                            if (type == 'Download') {
                                self.loadConverter(pageContent);
                            } else if (type == 'Print') {
                                self.printElement(pageContent);
                            }
                        }
                    }
                }
            );
        },
        async BookNow() {
            this.packageInfo.TravelDate = $('#traveldate').val();
            let StartDate = moment(this.packageInfo.TravelDate, 'dd/mm/yyyy').toDate();
            let travelDateValue = await moment(StartDate).format('YYYY-MM-DDThh:mm:ss');

            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.packageInfo.UserEmail.match(emailPat);
            let consValue = await this.contructSelectedFields();
            if (this.packageInfo.TravelDate == undefined || this.packageInfo.TravelDate == "") {
                alertify.alert('Alert', 'Please choose date to proceed.').set('closable', false);
                return;
            } else if (this.packageInfo.UserName == undefined || this.packageInfo.UserName == "") {
                alertify.alert('Alert', 'Please enter name to proceed.').set('closable', false);
                return;
            } else if (this.packageInfo.UserEmail == undefined || this.packageInfo.UserEmail == "") {
                alertify.alert('Alert', 'Please enter email to proceed.').set('closable', false);
                return;
            } else if (matchArray == null) {
                alertify.alert('Alert', 'Please enter valid email to proceed.').set('closable', false);
                return;
            } else if (this.packageInfo.UserPhone == undefined || this.packageInfo.UserPhone == "") {
                alertify.alert('Alert', 'Please enter phone to proceed.').set('closable', false);
                return;
            } else if (consValue == undefined || consValue == "") {
                alertify.alert('Alert', 'Please choose a type to proceed.').set('closable', false);
                return;
            } else if (this.packageInfo.UserCaptcha == undefined || this.packageInfo.UserCaptcha == "") {
                alertify.alert('Alert', 'Please enter captcha to proceed.').set('closable', false);
                return;
            } else if (Number(this.packageInfo.UserCaptcha) != Number(this.packageInfo.validCaptcha)) {
                this.consturctCaptchaValue(this.bookFormData);
                alertify.alert('Alert', 'Please enter valid captcha to proceed.').set('closable', false);
                return;
            }

            let requestedDate = await moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
            let fromDateVal = await moment(this.bookingInfoData.From_Date).format('YYYY-MM-DDThh:mm:ss');
            let toDateVal = await moment(this.bookingInfoData.To_Date).format('YYYY-MM-DDThh:mm:ss');

            let agencyCode = this.agencyCode;
            var originalPrice = this.packageInfo.TotalRate;
            if (this.CoupenInfo.ApplyCoupen == true) {
                originalPrice = this.CoupenInfo.newPrice
            }
            var couponprice = 0.00;
            if (this.CoupenInfo.ApplyCoupen == true) {
                couponprice = this.CoupenInfo.Amount;
            }
            var cocode = "N/A";
            if (this.CoupenInfo.ApplyCoupen == true) {
                cocode = this.CoupenInfo.CoupenCode
            }


            let requestObject = {
                type: "Booking Details",
                nodeCode: agencyCode,
                keyword1: "Package Booking",
                keyword2: this.packageInfoDetail.Reference_No,
                keyword3: this.packageInfoDetail.Name,
                keyword4: this.bookingInfoData.City,
                keyword5: this.bookingInfoData.Destination,
                keyword6: this.bookingInfoData.Price,
                keyword7: this.bookingInfoData.Days,
                keyword8: this.bookingInfoData.Nights,
                keyword9: this.packageInfo.UserName,
                keyword10: this.packageInfo.UserEmail,
                keyword11: this.packageInfo.UserPhone,
                keyword12: localStorage.selectedCurrency,
                keyword13: consValue,
                date1: fromDateVal,
                date2: toDateVal,
                date3: travelDateValue,
                date4: requestedDate,
                amount3:this.packageInfo.TotalRate,
                amount2: originalPrice,
                amount1: couponprice,
                keyword14: cocode

            };
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data';
            this.sendBookingEmail(travelDateValue);
            var responseObject = await this.restObject.gettingCMSData(cmsURL, "POST", requestObject, "en", "");
            this.consturctCaptchaValue(this.bookFormData);
            alertify.alert('Book package', 'Thank you for booking !');

        }, async getEmailObject(configureObject) {
            let returnObject = undefined;
            if (configureObject != undefined) {
                returnObject = configureObject;
            }
            return returnObject;
        },
        async sendBookingEmail(travelDateValue) {
            var fromPage = await this.restObject.getQueryStringValue('from');
            var fromEmail = JSON.parse(localStorage.User).loginNode.email;
            //this.emailConfiguration
            let configureObject = undefined;
            var toEmail = { To_Email: '' };
            switch (fromPage.toLocaleLowerCase()) {
                case 'eve':
                    configureObject = await this.getEmailObject(this.emailConfiguration.event);
                    toEmail = fromEmail;
                    break;
                case 'pkg':
                    configureObject = await this.getEmailObject(this.emailConfiguration.package);
                    toEmail = fromEmail;
                    break;
                case 'off':
                    configureObject = await this.getEmailObject(this.emailConfiguration.offer);
                    toEmail = fromEmail;
                    break;
                case 'uae':
                    configureObject = await this.getEmailObject(this.emailConfiguration.attraction);
                    toEmail = fromEmail;
                    break;

                default:
                    configureObject = { To_Email: fromEmail };
                    toEmail = fromEmail;
                    break;
            }

            var cc = [];
            let cusMessage = "Thank you for the request, your request is being reviewed our sales team will soon revert with the status.";
            if (configureObject != undefined && configureObject.To_Email != undefined) {
                toEmail = configureObject.To_Email;
            }
            if (configureObject != undefined && configureObject.CC != undefined) {
                cc = configureObject.CC;
            }
            if (configureObject != undefined && configureObject.Customer_Message != undefined) {
                cusMessage = configureObject.Customer_Message;
            }
            let agencyAddress = "";
            if (JSON.parse(localStorage.User) != undefined && JSON.parse(localStorage.User).loginNode != undefined &&
                JSON.parse(localStorage.User).loginNode.address != undefined) {
                agencyAddress = JSON.parse(localStorage.User).loginNode.address;
            }

            var postData = {
                type: "PackageBookingRequest",
                fromEmail: fromEmail,
                toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                ccEmails: cc,
                referenceID: this.packageInfoDetail.Reference_No,
                logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + '.xhtml?ln=logo',
                agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                agencyAddress: agencyAddress || "",
                packegeName: this.packageInfoDetail.Name,
                personName: this.packageInfo.UserName,
                emailAddress: this.packageInfo.UserEmail,
                contact: this.packageInfo.UserPhone,
                departureDate: travelDateValue,
                adults: this.packageInfo.AdultSize,
                child2to5: this.packageInfo.ChildSize,
                child6to11: "0",
                infants: this.packageInfo.InfantSize,
                message: "New Added",
                primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
            };
            var custmail = {
                type: "ThankYouRequest",
                fromEmail: fromEmail,
                toEmails: Array.isArray(this.packageInfo.UserEmail) ? this.packageInfo.UserEmail : [this.packageInfo.UserEmail],
                logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + '.xhtml?ln=logo',
                agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                agencyAddress: agencyAddress || "",
                personName: this.packageInfo.UserName,
                message: cusMessage,
                primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
            };

            var emailApi = ServiceUrls.emailServices.emailApi;
            
            sendMailService(emailApi, postData);
            sendMailService(emailApi, custmail);

            this.packageInfo.TravelDate = "";
            this.packageInfo.UserName = "";
            this.packageInfo.UserEmail = "";
            this.packageInfo.UserPhone = "";
            this.packageInfo.UserCaptcha = "";
            this.CoupenInfo.ApplyCoupen = false;
            this.CoupenInfo.Amount = "";
            this.CoupenInfo.CoupenCode = "";
            this.CoupenInfo.newPrice = "";
            this.CoupenCode = "";

            this.packageInfo.AdultSize= "1";
            this.packageInfo.ChildSize= "0";
            this.packageInfo.InfantSize= "0";
            this.constructPackageRateData();
            
            // this.packageInfo.Total_Quantity= "0";
            // this.packageInfo.ChildTotal= "0";
            // this.packageInfo.InfantTotal= "0";
        },
        async mobileNumberValidation(event) {
            if (event != undefined) {
                return ((((event.keyCode >= 48) && (event.keyCode <= 57)) || ((event.keyCode >= 96) && (event.keyCode <= 105)) || ((event.keyCode == 08)) || ((event.keyCode == 46))) && event.keyCode != 32);
            } else {
                this.packageInfo.UserPhone = "";
                return false;
            }

        },
        getPagecontent: async function () {
            var self = this;
            this.restObject = await gettingRestClientObject();
            var packageurl = await this.restObject.getQueryStringValue('page');
            getAgencycode(async function (response) {
                var Agencycode = response;
                self.agencyCode = Agencycode;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.direction = langauage == "ar" ? "rtl" : "ltr";
                self.langauage = langauage;


                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var generalPageURL = huburl + portno + commonPath + Agencycode + '/Template/General Details/General Details/General Details.ftl';
                var homecms = huburl + portno + commonPath + Agencycode + '/Template/Master page/Master page/Master page.ftl';
                var listPackageURL = huburl + portno + commonPath + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';
                self.constructGeneralData(generalPageURL);
                if (packageurl != undefined && packageurl != '') {
                    var packageName = packageurl.split('_')[0];
                    packageurl = "List-Of-Packages/" + packageName + "/" + packageurl;
                    packageurl = packageurl.split('-').join(' ');
                    var pageLink = "B2B/AdminPanel/CMS/" + Agencycode + "/Template/" + packageurl + ".ftl";
                    pageLink = huburl + portno + "/persons/source?path=/" + await self.restObject.constructPath(pageLink);
                    self.constuctPageContentData(pageLink, listPackageURL);
                    self.homeDetails(homecms);
                }

            });



        }, async homeDetails(homecms) {
            var pageContentTempData = await this.restObject.gettingCMSData(homecms, "GET", undefined, this.langauage, "");
            if (pageContentTempData != undefined && pageContentTempData.area_List != undefined) {
                let areaList = pageContentTempData.area_List;
                let headerCompo = {};
                let footerCompo = {};
                if (pageContentTempData.area_List.length > 0 && pageContentTempData.area_List[0].Header != undefined) {
                    headerCompo = await this.restObject.getAllMapData(pageContentTempData.area_List[0].Header.component);
                }
                if (pageContentTempData.area_List.length > 1 && pageContentTempData.area_List[1].Footer != undefined) {
                    footerCompo = await this.restObject.getAllMapData(pageContentTempData.area_List[1].Footer.component);
                }
                this.headerComponent = headerCompo;
                this.footerComponent = footerCompo;

            }

        }, async constuctPageContentData(pageLink, listPackageURL) {
            var pageContentTempData = await this.restObject.gettingCMSData(pageLink, "GET", undefined, this.langauage, "");
            var self = this;
            if (pageContentTempData != undefined && pageContentTempData.area_List != undefined) {

                if (pageContentTempData.area_List.length > 0) {
                    this.packageInfoDetail = await this.restObject.getAllMapData(pageContentTempData.area_List[0].Package_Info.component);
                }
                if (pageContentTempData.area_List.length > 1) {
                    var bookingInfoTempData = await this.restObject.getAllMapData(pageContentTempData.area_List[1].Booking_Info.component);
                    this.bookingInfoData = bookingInfoTempData;
                }
                if (pageContentTempData.area_List.length > 2) {
                    var detail_PageTempData = await this.restObject.getAllMapData(pageContentTempData.area_List[2].Detail_Page.component);
                    this.pageContentData = detail_PageTempData;

                    if (this.pageContentData.Price != undefined && this.pageContentData.Price.length > 0) {
                        this.selCategory = this.pageContentData.Price[0].Name;
                    }
                    if (this.pageContentData.Extra_Tab != undefined) {
                        this.extraTab = this.pageContentData.Extra_Tab;
                    }
                }
                this.constructRelatedTours(listPackageURL);
                this.constructWeatherData();
            }
            this.constructPackageRateData();
            this.getData = true;
            this.dropDownEnable();
            this.$nextTick(function () { self.callOwl(); }.bind(self));
        }, async inputFieldChange(event, type) {


        }, async updatePackageView() {

            if (this.packageInfoDetail != undefined && this.packageInfoDetail.Name != undefined && this.bookingInfoData != undefined) {

                let requestObject = {
                    Name: this.packageInfoDetail.Name, Reference_No: this.packageInfoDetail.Reference_No,
                    City: this.bookingInfoData.City, Destination: this.bookingInfoData.Destination,
                    From_Date: this.bookingInfoData.From_Date, To_Date: this.bookingInfoData.To_Date,
                    Price: this.bookingInfoData.Price, Days: this.bookingInfoData.Days,
                    Nights: this.bookingInfoData.Nights
                };
                this.restObject.updatePackageViewCount(requestObject, this.agencyCode);

            }

        },
        async constructGeneralData(generalPageURL) {

            var generalTempData = await this.restObject.gettingCMSData(generalPageURL, "GET", undefined, this.langauage, "");
            var emailConfigurationTempData = {};
            if (generalTempData != undefined && generalTempData.area_List != undefined) {
                this.constructQuantity();
                if (generalTempData.area_List.length > 0) {
                    var titleDataTemp = await this.restObject.getAllMapData(generalTempData.area_List[0].Package_Info_Page.component);
                    this.pageCommonData = titleDataTemp;
                }
                if (generalTempData.area_List.length > 1) {
                    var titleDataTemp = await this.restObject.getAllMapData(generalTempData.area_List[1].Book_Tour_Form.component);
                    await this.consturctCaptchaValue(titleDataTemp);
                    this.bookFormData = titleDataTemp;
                }
                if (generalTempData.area_List.length > 2 && generalTempData.area_List[2].Package_Page) {
                    var titleDataTemp = await this.restObject.getAllMapData(generalTempData.area_List[2].Package_Page.component);
                    emailConfigurationTempData.package = titleDataTemp;
                }
                if (generalTempData.area_List.length > 3 && generalTempData.area_List[3].Event_Page) {
                    var titleDataTemp = await this.restObject.getAllMapData(generalTempData.area_List[3].Event_Page.component);
                    emailConfigurationTempData.event = titleDataTemp;
                }
                if (generalTempData.area_List.length > 4 && generalTempData.area_List[4].Offer_Page) {
                    var titleDataTemp = await this.restObject.getAllMapData(generalTempData.area_List[4].Offer_Page.component);
                    emailConfigurationTempData.offer = titleDataTemp;
                }
                if (generalTempData.area_List.length > 5 && generalTempData.area_List[5].Attraction_Page) {
                    var titleDataTemp = await this.restObject.getAllMapData(generalTempData.area_List[5].Attraction_Page.component);
                    emailConfigurationTempData.attraction = titleDataTemp;
                }


            }
            this.emailConfiguration = emailConfigurationTempData;

            this.constructPackageRateData();
        }, async consturctCaptchaValue(titleDataTemp) {

            if (titleDataTemp != undefined && titleDataTemp.Captcha_Title != undefined && titleDataTemp.Captcha_Title != '') {
                var number1 = Math.floor(Math.random() * 10);
                var number2 = Math.floor(Math.random() * 10);
                var total = Number(number1) + Number(number2);
                this.packageInfo.validCaptcha = total;
                titleDataTemp.CaptchaConsValue = titleDataTemp.Captcha_Title + " " + number1 + "+" + number2 + " = ?";
            }
        }, async constructRelatedTours(listPackageURL) {
            var listPackageTempData = await this.restObject.gettingCMSData(listPackageURL, "GET", undefined, this.langauage, "");
            var tempTours = [];
            if (listPackageTempData != undefined && listPackageTempData.Values != undefined) {
                var allValues = listPackageTempData.Values;
                var self = this;
                var tempDataTours = allValues.filter(function (el) {
                    return el.To_Date && (new Date(el.To_Date.replace(/-/g, "/"))).getTime() >= Date.now() &&
                        el.Status == true && self.bookingInfoData.Destination == el.Destination;
                });
                var filterValue = "type='Package Details'";
                var allDBData = await this.restObject.getDbData4Table(this.agencyCode, filterValue, "number2");
                if (allDBData == undefined) {
                    allDBData = [];
                }
                for (let i = 0; i < tempDataTours.length; i++) {
                    let object = tempDataTours[i];
                    if (self.packageInfoDetail.Reference_No != object.Reference_No) {
                        var tempPopDataTours = allDBData.filter(function (el) {
                            return object.Reference_No == el.keyword1;
                        });
                        object.Ratings = "0";
                        if (tempPopDataTours.length > 0) {
                            object.Ratings = tempPopDataTours[0].number2;
                        }
                        tempTours.push(object);
                    }
                }
                if (allDBData != undefined && allDBData.length > 0) {
                    let popularObject = allDBData[0];
                    let Reference_No = popularObject.keyword1;
                    var tempPopDataTours = allValues.filter(function (el) {
                        return el.To_Date && (new Date(el.To_Date.replace(/-/g, "/"))).getTime() >= Date.now() &&
                            el.Status == true && Reference_No == el.Reference_No;
                    });
                    tempPopDataTours.sort((a, b) => {
                        return new Date(b.number2) - new Date(a.number2);
                    });

                    if (tempPopDataTours != undefined && tempPopDataTours.length > 0) {
                        var packageObj = tempPopDataTours[0];
                        packageObj.Tag_Image = "images/popular-tag.png";
                        packageObj.Rating = popularObject.number2;
                        this.popularPackage = packageObj;
                    }
                }
                this.updatePackageView();
            }
            this.relatedTours = tempTours;
        }, async constructQuantity() {
            var tempList = [
                { Name: "1", Value: "1" },
                { Name: "2", Value: "2" },
                { Name: "3", Value: "3" },
                { Name: "4", Value: "4" },
                { Name: "5", Value: "5" }
            ];
            this.quantityList = tempList;
        }, async constructWeatherData() {
            if (this.bookingInfoData != undefined && this.bookingInfoData.City != undefined) {
                var weatCity = this.bookingInfoData.City;
                var self = this;
                this.weatherCity = weatCity;
                var url = 'https://api.openweathermap.org/data/2.5/weather?q=' + weatCity + '&units=metric&appid=7c0ff0999be50513159abbf786d93a0a';
                var desturl = 'https://api.openweathermap.org/data/2.5/weather?q=' + this.bookingInfoData.Destination + '&units=metric&appid=7c0ff0999be50513159abbf786d93a0a';
                axios.get(url, {
                    headers: { 'content-type': 'application/json' }
                }).then(function (response) {
                    self.weatherData = response;

                }).catch(error => {


                    axios.get(desturl, { headers: { 'content-type': 'application/json' } }).then(function (response) {
                        self.weatherData = response;
                        self.weatherCity = self.bookingInfoData.Destination;

                    });;
                });;
            }
        }, async callOwl() {
            $("#package-in-item").owlCarousel({
                autoplay: true,
                autoPlay: 8000,
                autoplayHoverPause: true,
                stopOnHover: false,
                items: 2,
                margin: 10,
                lazyLoad: true,
                navigation: true,
                itemsDesktop: [1199, 2],
                itemsDesktopSmall: [991, 2],
                itemsTablet: [600, 1]
            });
            $(".owl-prev").html('<i class="fa  fa-angle-left"></i>');
            $(".owl-next").html('<i class="fa  fa-angle-right"></i>');
        }, async constructEnquiryButton(item) {
            if (item != undefined && item.Name != undefined) {
                this.selCategory = item.Name;
                this.constructPackageRateData();
                $('#bookingDiv')[0].scrollIntoView(true);
            }
        },
        async dropdownChange(event, type) {
            if (event != undefined) {
                let selectedObject = event.target.value;

                if (type == 'Adult') {
                    this.packageInfo.AdultSize = selectedObject;
                } else if (type == 'Child') {
                    this.packageInfo.ChildSize = selectedObject;
                } else if (type == 'Infant') {
                    this.packageInfo.InfantSize = selectedObject;
                } else if (type == 'Category') {
                   
                    if (selectedObject == "0") {

                        this.packageInfo.AdultSize = "1";
                        this.packageInfo.ChildSize = "0";
                        this.packageInfo.InfantSize = "0";
                    }
                    this.selCategory = selectedObject;
                }

            }
            this.constructPackageRateData();
            this.CoupenInfo.ApplyCoupen = false;
            this.CoupenInfo.Amount = "";
            this.CoupenInfo.CoupenCode = "";
            this.CoupenInfo.newPrice = "";
            this.CoupenCode = "";


        }, async constructPackageRateData() {
           

            var selectedCategoryObj = await this.getSelectedCategory(this.selCategory);
            var packageRate = await this.restObject.constructNumeber(this.bookingInfoData.Price);

            var adultRate = await this.restObject.constructNumeber(selectedCategoryObj.Adult);
            var childRate = await this.restObject.constructNumeber(selectedCategoryObj.Child);
            var infantRate = await this.restObject.constructNumeber(selectedCategoryObj.Infant);

            if (childRate == 0) {
                this.quantityListChild = true;
            } else {
                this.quantityListChild = false;
            }
            if (infantRate == 0) {
                this.quantityListInfant = true;
            } else {
                this.quantityListInfant = false;
            }

            var adultSize = await this.restObject.constructNumeber(this.packageInfo.AdultSize);
            var childSize = await this.restObject.constructNumeber(this.packageInfo.ChildSize);
            var infantSize = await this.restObject.constructNumeber(this.packageInfo.InfantSize);
            
            var adultTotal = Number(adultRate) * Number(adultSize);
            var childTotal = Number(childRate) * Number(childSize);
            var infantTotal = Number(infantRate) * Number(infantSize);

            var totalAmount = Number(adultTotal) + Number(childTotal) + Number(infantTotal);

            this.packageInfo.Adult = adultSize;
            this.packageInfo.Child = childSize;
            this.packageInfo.Infant = infantSize;
            this.packageInfo.AdultTotal = adultTotal;
            this.packageInfo.ChildTotal = childTotal;
            this.packageInfo.InfantTotal = infantTotal;
            this.packageInfo.TotalAmount = totalAmount;
            this.packageInfo.Currency = this.selectedCurrency;
            let allList = [];
            if (totalAmount == 0) {
                totalAmount = packageRate;
            } else {
                if (this.selCategory != 'Select') {
                    allList.push({
                        Value: this.selCategory + " " + this.pageContentData.Price_Column_1_Title,
                        Label: this.pageContentData.Price_Column_1_Title, LabelValue: this.selCategory, show: true
                    });
                    let need2Add = true;
                    if (this.packageInfo.Adult != 'Select') {
                        allList.push({
                            Value: this.packageInfo.AdultSize + " " + this.pageContentData.Price_Column_2_Title,
                            Label: this.pageContentData.Price_Column_2_Title, LabelValue: this.packageInfo.AdultSize, show: this.packageInfo.AdultSize == 0 ? false : true
                        });
                        need2Add = true;
                    }
                    if (this.packageInfo.Child != 'Select') {
                        allList.push({
                            Value: this.packageInfo.ChildSize + " " + this.pageContentData.Price_Column_3_Title,
                            Label: this.pageContentData.Price_Column_3_Title, LabelValue: this.packageInfo.ChildSize, show: this.packageInfo.ChildSize == 0 ? false : true
                        });
                    }
                    if (need2Add && this.packageInfo.InfantSize != 'Select') {
                        allList.push({
                            Value: this.packageInfo.InfantSize + " " + this.pageContentData.Price_Column_4_Title,
                            Label: this.pageContentData.Price_Column_4_Title, LabelValue: this.packageInfo.InfantSize, show: this.packageInfo.InfantSize == 0 ? false : true
                        });
                    }

                }
            }
            this.selectedList = allList.filter(function(el){return el.show==true });
            this.packageInfo.TotalRate = totalAmount;
            this.packageInfo.Total_Quantity = Number(adultSize) + Number(childSize) + Number(infantSize);;

        }, async getSelectedCategory(category) {
            var selectedCategoryObj = { Adult: "0", Infant: "0", Child: "0", Name: category };
            if (this.pageContentData != undefined && this.pageContentData.Price != undefined) {
                for (let i = 0; i < this.pageContentData.Price.length; i++) {
                    if (this.pageContentData.Price[i].Name == category) {
                        selectedCategoryObj = this.pageContentData.Price[i];
                        break;
                    }
                }
            }

            return selectedCategoryObj;
        }, async dropDownEnable() {
            let vm = this;
            this.$nextTick(function () {
                vm.setclaneder();
                $('#categorySel').on("change", function (e) {
                    vm.dropdownChange(e, 'Category')
                });
                $('#adultSel').on("change", function (e) {
                    vm.dropdownChange(e, 'Adult')
                });
                $('#childSel').on("change", function (e) {
                    vm.dropdownChange(e, 'Child')
                });
                $('#infantSel').on("change", function (e) {
                    vm.dropdownChange(e, 'Infant')
                });
                //$('.input-group.date').datepicker({format: "dd-mm-yyyy"}); 
            }.bind(vm));



        },
        setclaneder: function () {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            let vm = this;
            //$('.date').datepicker();
            $('.date').datepicker({
                autoclose: true,
                todayHighlight: false,
                format: "dd/mm/yyyy",
                startDate: new Date()
            });
            $('.date').change(function () {
                vm.packageInfo.TravelDate = $('#traveldate').val();
            });
        },
        sendAssistance: async function () {


            if (!this.enqtxtname) {
                alertify.alert('Alert', 'Name required.').set('closable', false);
                return false;
            }
            if (!this.enqtxtemail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            if (!this.enqtxtphone) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.enqtxtphone.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }

            if (!this.enqtxtmsg) {
                alertify.alert('Alert', 'Message required.').set('closable', false);
                return false;
            }

            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.enqtxtemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            } else {
                var fromPage = await this.restObject.getQueryStringValue('from');
                var fromEmail = JSON.parse(localStorage.User).loginNode.email;
                switch (fromPage.toLocaleLowerCase()) {
                    case 'eve':
                        toEmail = await this.getEmailObject(this.emailConfiguration.event);
                        break;
                    case 'pkg':
                        toEmail = await this.getEmailObject(this.emailConfiguration.package);
                        break;
                    case 'off':
                        toEmail = await this.getEmailObject(this.emailConfiguration.offer);
                        break;
                    case 'uae':
                        toEmail = await this.getEmailObject(this.emailConfiguration.attraction);
                        break;

                    default:
                        toEmail = fromEmail;
                        break;
                }

                var postData = {
                    type: "AdminContactUsRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(toEmail.To_Email) ? toEmail.To_Email : [toEmail.To_Email],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.enqtxtname,
                    emailId: this.enqtxtemail,
                    contact: this.enqtxtphone,
                    message: this.enqtxtmsg,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(this.enqtxtemail) ? this.enqtxtemail : [this.enqtxtemail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.enqtxtname,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };


                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Assistance Details",
                    keyword1: "Package Booking",
                    keyword2: this.enqtxtname,
                    keyword3: this.enqtxtemail,
                    keyword4: this.enqtxtphone,
                    keyword5: this.packageInfoDetail.Name,
                    text1: this.enqtxtmsg,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                const url = huburl + portno + "/cms/data";
                let responseObject = await this.gettingCMSData(url, "POST", insertContactData, "en", "");
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;

                    sendMailService(emailApi, postData);
                    sendMailService(emailApi, custmail);
                    this.enqtxtemail = '';
                    this.enqtxtname = '';
                    this.enqtxtphone = '';
                    this.enqtxtmsg = '';
                    alertify.alert('Enquiry', 'Thank you for contacting us. We shall get back to you.');
                    $('#emialRequest').modal('toggle');
                } catch (e) {

                }
            }
        },
        async gettingCMSData(cmsURL, methodName, bodyData, languageCode, type) {
            var data = bodyData;
            if (data != null && data != undefined) {
                data = JSON.stringify(data);
            }
            const response = await fetch(cmsURL, {
                method: methodName, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json', 'Accept-Language': languageCode },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                let object = { area_List: [] }
                return object;
            }
        },
        checkIfNumber: function (field) {
            var value = false;
            switch (field) {
                case "Adult":
                    for (var index = 0; index < this.pageContentData.Price.length; index++) {
                        var element = this.pageContentData.Price[index];
                        if (!isNaN(element.Adult) && (element.Adult || "na").toLowerCase() != "na"
                            && (element.Adult || "n/a").toLowerCase() != "n/a" && parseInt(element.Adult) != 0) {
                            value = true;
                            break;
                        }
                    }
                    break;
                case "Child":
                    for (var index = 0; index < this.pageContentData.Price.length; index++) {
                        var element = this.pageContentData.Price[index];
                        if (!isNaN(element.Child) && (element.Child || "na").toLowerCase() != "na"
                            && (element.Child || "n/a").toLowerCase() != "n/a" && parseInt(element.Child) != 0) {
                            value = true;
                            break;
                        }
                    }
                    break;
                case "Infant":
                    for (var index = 0; index < this.pageContentData.Price.length; index++) {
                        var element = this.pageContentData.Price[index];
                        if (!isNaN(element.Infant) && (element.Infant || "na").toLowerCase() != "na"
                            && (element.Infant || "n/a").toLowerCase() != "n/a" && parseInt(element.Infant) != 0) {
                            value = true;
                            break;
                        }
                    }
                    break;
                default:
                    break;
            }
            return value;
        },

        getGlobalcoupencode: function () {
            var self = this;
            var CoupenCode = self.CoupenCode;
            var packamt = this.packageInfo.TotalRate;
            if (CoupenCode == "") {
                alertify.alert('Alert', 'Please enter the coupon code');
            }

            else {
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;

                var query = {
                    _source: [
                        "keyword1",
                        "date1",
                        "date2",
                        "amount1",
                        "keyword3",
                        "amount2",
                        "amount3"
                    ],
                    query: {
                        bool: {
                            filter: [
                                {
                                    match_phrase: {
                                        keyword1: {
                                            query: CoupenCode
                                        }
                                    }
                                },
                                {
                                    range: {
                                        date1: {
                                            lte: "now"
                                        }
                                    }
                                },
                                {
                                    range: {
                                        date2: {
                                            gte: "now"
                                        }
                                    }
                                },
                                {
                                    range: {
                                        amount2: {
                                            lte: packamt
                                        }
                                    }
                                },
                                {
                                    range: {
                                        amount3: {
                                            gte: packamt
                                        }
                                    }
                                },
                                {
                                    match_phrase: {
                                        type: {
                                            query: "Coupon code-All Packages"
                                        }
                                    }
                                },
                                {
                                    match_phrase: {
                                        nodeCode: {
                                            query: agencyCode
                                        }
                                    }
                                },
                                {
                                    match_phrase: {
                                        keyword3: {
                                            query: "True"
                                        }
                                    }
                                }
                            ]
                        }
                    }

                };
                var client = new elasticsearch.Client({
                    host: [{
                        host: ServiceUrls.elasticSearch.elasticsearchHost,
                        auth: "nirvana:agy427",
                        protocol: ServiceUrls.elasticSearch.protocol,
                        port: ServiceUrls.elasticSearch.port,
                        requestTimeout: 60000
                    }],
                    log: 'trace'
                });
                client.search({ index: 'cms_forms_data', size: 5, pretty: true, filter_path: 'hits.hits._source', body: query }).then(function (resp) {
                    if (isEmpty(resp)) {
                        self.getSpecificcoupencode();
                    }
                    else {
                        console.log("coupencodeGlobal", resp);
                        var Result = resp.hits.hits;
                        if (Result.length > 0) {
                            Result = Result.find((element) => {
                                return (element._source.amount1 !== null || element._source.amount1 != null);
                            });
                            console.log(Result);
                            self.CoupenInfo.Amount = Result._source.amount1;
                            self.CoupenInfo.ApplyCoupen = true;
                            self.CoupenInfo.CoupenCode = CoupenCode;
                            var cuurntPrice = self.packageInfo.TotalAmount;
                            self.CoupenInfo.newPrice = parseFloat(cuurntPrice) - parseFloat(self.CoupenInfo.Amount);
                        }


                    }


                });


            }
        },
        getSpecificcoupencode: function () {
            var self = this;
            var CoupenCode = self.CoupenCode;
            var packamt = self.packageInfo.TotalRate;
            var Reference_No = self.packageInfoDetail.Reference_No;
            let agencyCode = JSON.parse(localStorage.User).loginNode.code;
            var query = {
                _source: [
                    "keyword1",
                    "keyword2",
                    "date1",
                    "date2",
                    "amount1",
                    "keyword4",
                    "amount2",
                    "amount3"
                ],
                query: {
                    bool: {
                        filter: [
                            {
                                match_phrase: {
                                    keyword1: {
                                        query: Reference_No
                                    }
                                }
                            },
                            {
                                match_phrase: {
                                    keyword2: {
                                        query: CoupenCode
                                    }
                                }
                            },
                            {
                                range: {
                                    date1: {
                                        lte: "now"
                                    }
                                }
                            },
                            {
                                range: {
                                    date2: {
                                        gte: "now"
                                    }
                                }
                            },
                            {
                                range: {
                                    amount2: {
                                        lte: packamt
                                    }
                                }
                            },
                            {
                                range: {
                                    amount3: {
                                        gte: packamt
                                    }
                                }
                            },
                            {
                                match_phrase: {
                                    type: {
                                        query: "Coupon code-Specific Packages"
                                    }
                                }
                            },
                            {
                                match_phrase: {
                                    nodeCode: {
                                        query: agencyCode
                                    }
                                }
                            },
                            {
                                match_phrase: {
                                    keyword4: {
                                        query: "True"
                                    }
                                }
                            }
                        ]
                    }
                }

            };
            var client = new elasticsearch.Client({
                host: [{
                    host: ServiceUrls.elasticSearch.elasticsearchHost,
                    auth: "nirvana:agy427",
                    protocol: ServiceUrls.elasticSearch.protocol,
                    port: ServiceUrls.elasticSearch.port,
                    requestTimeout: 60000
                }],
                log: 'trace'
            });
            client.search({ index: 'cms_forms_data', size: 5, pretty: true, filter_path: 'hits.hits._source', body: query }).then(function (resp) {
                if (isEmpty(resp)) {
                    alertify.alert('Alert', 'Coupon Code is Not Valid,Please enter a valid coupon code');
                    self.CoupenInfo.ApplyCoupen = false;
                    self.CoupenInfo.Amount = "";
                    self.CoupenInfo.CoupenCode = "";
                    self.CoupenInfo.newPrice = "";
                }
                else {
                    console.log("coupencode", resp);
                    var Result = resp.hits.hits;
                    if (Result.length > 0) {
                        Result = Result.find((element) => {
                            return (element._source.amount1 !== null || element._source.amount1 != null);
                        });
                        console.log(Result);
                        self.CoupenInfo.Amount = Result._source.amount1;
                        self.CoupenInfo.ApplyCoupen = true;
                        self.CoupenInfo.CoupenCode = CoupenCode;
                        var cuurntPrice = self.packageInfo.TotalAmount;
                        self.CoupenInfo.newPrice = parseFloat(cuurntPrice) - parseFloat(self.CoupenInfo.Amount);
                    }

                }


            });
        },
        clearCoupon: function () {
            var self = this;
            self.CoupenInfo.ApplyCoupen = false;
            self.CoupenInfo.Amount = "";
            self.CoupenInfo.CoupenCode = "";
            self.CoupenInfo.newPrice = "";
            self.CoupenCode = "";
        }
    },
    mounted: function () {
        var vm = this;
        this.getPagecontent();

        //this.dropDownEnable();
        this.$nextTick(function () {
            if (localStorage.IsLogin == true || localStorage.IsLogin == "true") {
                var User = JSON.parse(localStorage.User);
                vm.packageInfo.UserName = User.firstName + ' ' + User.lastName;
                vm.packageInfo.UserEmail = User.emailId;
                vm.packageInfo.UserPhone = User.contactNumber;
            }
        })
    },
    updated: function () {


    }
});
function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
function isNullorUndefined(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) { status = true; }
    return status;
}