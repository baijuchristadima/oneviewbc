const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var serivcesPage = new Vue({
    i18n,
    el: '#serivcesPage',
    name: 'serivcesPage',
    data: {
        restObject: undefined,
        Agencycode:'',
        langauage: "en",
        pageCommonData: {
            Title: 'NIRVANA SERVICES',
            Description: ''
        },
        servivsegallery:false,
        Services: {
            Title: '',
            ShortDesc: null,
            Description: null,
            SubDesc: null,
            SubTitle: null,
            Gallery: null,
            image:null,
            Title2: null,
            To_Email: "",
        },
        popularPackage:{
            Banner_Image:"",
            Destination:"",
            Days:"", 
            Rating:"",
            Price:"",
            Tag_Image:"",
        },
        content: null,
        getservice: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        enqtxtname: "",
        enqtxtemail: "",
        enqtxtphone: "",
        enqtxtmsg: "",
        headerComponent:"",
        footerComponent:"",
        servicesList: ""
    },
    methods: {
    sendAssistance: async function () {


        if (!this.enqtxtname) {
            alertify.alert('Alert', 'Name required.').set('closable', false);
            return false;
        }
        if (!this.enqtxtemail) {
            alertify.alert('Alert', 'Email Id required.').set('closable', false);
            return false;
        }
        if (!this.enqtxtphone) {
            alertify.alert('Alert', 'Mobile number required.').set('closable', false);
            return false;
        }
        if (this.enqtxtphone.length < 8) {
            alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
            return false;
        }

        if (!this.enqtxtmsg) {
            alertify.alert('Alert', 'Message required.').set('closable', false);
            return false;
        }

        var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        var matchArray = this.enqtxtemail.match(emailPat);
        if (matchArray == null) {
            alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
            return false;
        } else {
            var fromEmail = JSON.parse(localStorage.User).loginNode.email;
            var toEmail = this.Services.To_Email;

            var postData = {
                type: "AdminContactUsRequest",
                fromEmail: fromEmail,
                toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                personName: this.enqtxtname,
                emailId: this.enqtxtemail,
                contact: this.enqtxtphone,
                message: this.enqtxtmsg,
                primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
            };
            var custmail = {
                type: "UserAddedRequest",
                fromEmail: fromEmail,
                toEmails: Array.isArray(this.enqtxtemail) ? this.enqtxtemail : [this.enqtxtemail],
                logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                personName: this.enqtxtname,
                primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
            };


            let agencyCode = JSON.parse(localStorage.User).loginNode.code;
            let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
            let insertContactData = {
                type: "Assistance Details",
                keyword1: "Service Booking",
                keyword2: this.enqtxtname,
                keyword3: this.enqtxtemail,
                keyword4: this.enqtxtphone,
                keyword5: this.Services.Title,
                text1: this.enqtxtmsg,
                date1: requestedDate,
                nodeCode: agencyCode
            };
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/cms/data";
            let responseObject = await this.gettingCMSData(url, "POST", insertContactData, "en", "");
            try {
                let insertID = Number(responseObject);
                var emailApi = ServiceUrls.emailServices.emailApi;
                sendMailService(emailApi, postData);
                sendMailService(emailApi, custmail);
                this.enqtxtemail = '';
                this.enqtxtname = '';
                this.enqtxtphone = '';
                this.enqtxtmsg = '';
                alertify.alert('Enquiry', 'Thank you for contacting us. We shall get back to you.');
                $('#emialRequest').modal('toggle');

            } catch (e) {

            }
        }
    },
    async gettingCMSData(cmsURL, methodName, bodyData, languageCode, type) {
        var data = bodyData;
        if (data != null && data != undefined) {
            data = JSON.stringify(data);
        }
        const response = await fetch(cmsURL, {
            method: methodName, // *GET, POST, PUT, DELETE, etc.
            credentials: "same-origin", // include, *same-origin, omit
            headers: { 'Content-Type': 'application/json', 'Accept-Language': languageCode },
            body: data, // body data type must match "Content-Type" header
        });
        try {
            const myJson = await response.json();
            return myJson;
        } catch (error) {
            let object = { area_List: [] }
            return object;
        }
    },
    async homeDetails(homecms) {
        var pageContentTempData = await this.restObject.gettingCMSData(homecms, "GET", undefined, this.langauage, "");
        if (pageContentTempData != undefined && pageContentTempData.area_List != undefined) {
            let areaList = pageContentTempData.area_List;
            let headerCompo = {};
            let footerCompo = {};
            if (pageContentTempData.area_List.length > 0 && pageContentTempData.area_List[0].Header != undefined) {
                headerCompo = await this.restObject.getAllMapData(pageContentTempData.area_List[0].Header.component);
            }
            if (pageContentTempData.area_List.length > 1 && pageContentTempData.area_List[1].Footer != undefined) {
                footerCompo = await this.restObject.getAllMapData(pageContentTempData.area_List[1].Footer.component);
            }
            this.headerComponent = headerCompo;
            this.footerComponent = footerCompo;

        }

    },
        getPagecontent: async function () {
            var self = this;
            this.restObject = await gettingRestClientObject();
            var servicepageurl = await this.restObject.getQueryStringValue('page');
            getAgencycode(function (response) {
                var Agencycode = response;
                self.Agencycode=response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                self.langauage = langauage;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var generalPageURL = huburl + portno + commonPath + Agencycode + '/Template/Service Page/Service Page/Service Page.ftl';
                var homecms = huburl + portno + commonPath + Agencycode + '/Template/Master page/Master page/Master page.ftl';
                var listservices = huburl + portno + commonPath + Agencycode + '/Master Table/Services/Services/Services.ftl';

                if (servicepageurl != undefined && servicepageurl != '') {

                    var packageName = servicepageurl.split('_')[0];
                    servicepageurl = "Services/" + packageName + "/" + servicepageurl;
                    servicepageurl = servicepageurl.split('-').join(' ');
                    var pageLink = "B2B/AdminPanel/CMS/" + Agencycode + "/Template/" + servicepageurl + ".ftl";


                    // self.constructGeneralPageDetails(generalPageURL);
                    self.constructServicePagecontent(pageLink);
                    self.homeDetails(homecms);
                    self.constructserivselist(listservices);

                }

            });

        },
        async constructserivselist(listservices) {
            var self = this;
            var listserviceTempData = await this.restObject.gettingCMSData(listservices, "GET", undefined, this.langauage, "");

            if (listserviceTempData != undefined && listserviceTempData.Values != undefined) {
                var allValues = listserviceTempData.Values;
                if (allValues.length > 0) {
                    self.servicesList = allValues
                }

            }

        },
        async constructGeneralPageDetails(generalPageURL) {
            var self = this;
            var generalTempData = await this.restObject.gettingCMSData(generalPageURL, "GET", undefined, this.langauage, "");
            var titleDataTemp = {};
            if (generalTempData != undefined && generalTempData.area_List != undefined) {
                if (generalTempData.area_List.length > 0) {
                    titleDataTemp = self.pluck('main', generalTempData.area_List);
                    self.pageCommonData.Title = self.pluckcom('Title', titleDataTemp[0].component);
                    self.pageCommonData.Description = self.pluckcom('Description', titleDataTemp[0].component);
                }

            }

        },
        async constructServicePagecontent(pageLink) {
            var self = this;
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            pageLink = huburl + portno + "/persons/source?path=/" + await self.restObject.constructPath(pageLink);
            var listserviceTempData = await this.restObject.gettingCMSData(pageLink, "GET", undefined, this.langauage, "");
            //self.content = listserviceTempData;
            console.log(listserviceTempData.area_List);
            self.getservice=true
            if (listserviceTempData != undefined && listserviceTempData.area_List != undefined) {
               
                self.content = self.pluck('Service', listserviceTempData.area_List);               
                self.Services.Title=self.pluckcom('Title',self.content[0].component) ;
                self.Services.ShortDesc=self.pluckcom('Short_Description',self.content[0].component) ;
                self.Services.Description=self.pluckcom('Description',self.content[0].component) ;
                self.Services.SubDesc=self.pluckcom('Sub_Description',self.content[0].component) ;
                self.Services.SubTitle=self.pluckcom('Subtitle',self.content[0].component) ;
                self.Services.Gallery=self.pluckcom('Gallery',self.content[0].component) ;
                self.Services.image=self.pluckcom('Image',self.content[0].component) ;
                self.Services.Title2 = self.pluckcom('Title2', self.content[0].component);
                self.Services.To_Email = self.pluckcom('To_Email', self.content[0].component);
               if(self.Services.Gallery!=null)
               {
                self.servivsegallery=true;
                setTimeout(function () { eventinfo() }, 1000);
               
               }
            $(".servicesinfo_banner").css("background-image", "url(" + self.Services.image + ")");

            }
            var commonPath='/persons/source?path=/B2B/AdminPanel/CMS/';
            var listPackageURL=huburl + portno + commonPath + self.Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';
            self.constructRelatedTours(listPackageURL);

        },
        async getmoreinfo(url) {
            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.');
                url=url.split('/')[url.split('/').length-1];
                url = "/Nirvana/servicesinfo.html?page=" + url + "&from=service";
                window.location.href = url;
            }
            else {
                url = "#";
            }


        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = null;
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        async constructRelatedTours(listPackageURL){
            var listPackageTempData=await this.restObject.gettingCMSData(listPackageURL,"GET",undefined,this.langauage,"");
            var tempTours=[];
            if(listPackageTempData!=undefined&&listPackageTempData.Values!=undefined){
                var allValues=listPackageTempData.Values;
                var self=this;                
                var filterValue="type='Package Details'";
                var allDBData=await this.restObject.getDbData4Table(this.Agencycode,filterValue,"number2");
                if(allDBData==undefined){
                    allDBData=[];
                }
                
                if(allDBData!=undefined&&allDBData.length>0){
                    let popularObject=allDBData[0];
                    let Reference_No=popularObject.keyword1;
                    var tempPopDataTours = allValues.filter(function (el) {
                        return el.To_Date && (new Date(el.To_Date.replace(/-/g, "/"))).getTime() >= Date.now() && 
                            el.Status == true && Reference_No==el.Reference_No;
                    });
                    tempPopDataTours.sort( ( a, b) => {
                        return new Date(b.number2) - new Date(a.number2);
                    });

                    if(tempPopDataTours!=undefined&&tempPopDataTours.length>0){
                        var packageObj=tempPopDataTours[0];
                        packageObj.Tag_Image="images/popular-tag.png";
                        packageObj.Rating=popularObject.number2;
                        this.popularPackage=packageObj;
                    }
                }
              
            }           
        }
    },
    mounted: function () {
        this.getPagecontent();
    },
});