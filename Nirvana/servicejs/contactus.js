const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})

var packagelist = new Vue({
    i18n,
    el: '#aboutUsPage',
    name: 'aboutUsPage',
    data: {
        content: null,
        getdata: true,
        packages: null,
        getpackage: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        pkgPgBnrTitle: "",
        globalBranches:[]
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function() {
            var self = this;

            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                

                var contactUsDetailsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact Us Details/Contact Us Details/Contact Us Details.ftl';

                axios.get(contactUsDetailsPage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                        let areaList=response.data.area_List;
                        self.pkgPgBnrTitle = self.pluckcom('Page_Title', areaList[0].Page_Settings.component);
                        var mainBranchData={};
                        var globalBranchData={};
                        if(areaList.length>1&&areaList[1].Main_Branch!=undefined){
                            mainBranchData = self.getAllMapData(areaList[1].Main_Branch.component);
                        }
                        if(areaList.length>2&&areaList[2].Global_Branches!=undefined){
                            globalBranchData = self.getAllMapData(areaList[2].Global_Branches.component);
                        }
                        var allBranches=[];

                        var mainBranch={CX:mainBranchData.CX,CY:mainBranchData.CY,R:mainBranchData.R,Sub_Branches:[]};
                        
                        for (let index = 0; index < mainBranchData.Branches.length; index++) {
                            const element = mainBranchData.Branches[index];
                            if(element.Status==true){
                                mainBranch.Sub_Branches.push(element);
                            }
                        }
                        allBranches.push(mainBranch);
                        for (let index = 0; index < globalBranchData.Branches.length; index++) {
                            const element = globalBranchData.Branches[index];
                            const newElement=JSON.parse(JSON.stringify(element));
                            newElement.Sub_Branches=[];
                            
                            var gloaBranch={CX:element.CX,CY:element.CY,R:element.R,Sub_Branches:[]};
                            if(newElement.Status==true){
                                gloaBranch.Sub_Branches.push(newElement);
                            }
                            for (let k = 0; k < element.Sub_Branches.length; k++) {
                                const element2 = element.Sub_Branches[k];
                                if(element2.Status==true){
                                    gloaBranch.Sub_Branches.push(element2);
                                }
                                
                                
                            }
                            
                            allBranches.push(gloaBranch);
                            
                        }
                        
                        self.globalBranches=allBranches;
                        self.$nextTick(function(){
                            $("#0").show();
                        });
                    }
                }).catch(function(error) {
                    console.log('Error');
                });
            });

        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        getmoreinfo(url) {

            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url = "/Nirvana/book-now.html?page=" + url;
            } else {
                url = "#";
            }
            return url;
        }, activeSlider:function(region, item){
            $(".slidesec").hide();
            $("#" + region).show();
            $(item).attr("class", "activemap");
            $(item).siblings().attr("class", "");
        }

        
    },
    mounted: function() {
        
        this.getPagecontent();
        $("#0").show();
    },
});

function activemap(region, item) {
    $(".slidesec").hide();
    $("#" + region).show();
    $(item).attr("class", "activemap");
    $(item).siblings().attr("class", "");

}