const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var blogInfo = new Vue({
    i18n,
    el: '#blogInfopage',
    name: 'blogInfopage',
    data: {
        agencyCode:"",
        restObject:{},
        pageLinkValue:"",
        direction:'ltr',
        titleData:{},
        titleMainData:{},
        emailAddress:"",
        nameEmail:"",
        headerData:[],
        recentPostList:[],
        getData:false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    },
    methods: {
        async redirectBlogPage(blogItem){
            if( blogItem!=undefined && blogItem.Id!=undefined && blogItem.More_details_page_link!=undefined){
                await this.restObject.redirectBlogPage(blogItem,this.agencyCode);
            }
        },
        async redirectCategoryPage(headerItem){
            if(headerItem!=undefined&&headerItem.id!=undefined){
                window.sessionStorage.setItem('categoryID', headerItem.id);
                window.location.href="/Nirvana/blog-categories.html?ID="+headerItem.id;
            }
        },
        getPagecontent: async function () {
            var self = this;
            this.restObject=await gettingRestClientObject();
            var blogID=await this.restObject.getQueryStringValue("ID");
            var added=await this.restObject.getQueryStringValue("Added");
           
            this.pageLinkValue=window.sessionStorage.getItem('blog_page_Link');
            
            this.pageLinkValue=await this.restObject.constructPath(this.pageLinkValue);
            
            getAgencycode(async function (response) {
                var Agencycode = response;
                self.agencyCode=Agencycode;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.direction = langauage == "ar" ? "rtl" : "ltr";
                var queryParameter='/persons/source?path=/';
                var commonPath=queryParameter+'B2B/AdminPanel/CMS/';
                
                
                var blogmainpageURL = huburl + portno + commonPath + Agencycode + '/Template/Blog/Blog/Blog.ftl';
                var masterTableURL = huburl + portno + commonPath + Agencycode + '/Master Table/Blog Info/Blog Info/Blog Info.ftl';
                var masteTempData=await self.restObject.gettingCMSData(masterTableURL,"GET",undefined,langauage,"");

                var blogMainTempData=await self.restObject.gettingCMSData(blogmainpageURL,"GET",undefined,langauage,"");
                var blogTempData=undefined;
                if(masteTempData!=undefined && masteTempData.Values!=undefined){
                    masteTempData.Values = masteTempData.Values.filter(function (el) {
                        return el.Status ;
                    });
                    masteTempData.Values.sort( ( a, b) => {
                        return new Date(b.Created_Date) - new Date(a.Created_Date);
                    });
                   var blogItemList= masteTempData.Values.filter(function (el) {
                        return el.Status && el.Id==blogID;
                    });
                    if(blogItemList.length>0){

                        self.pageLinkValue=await self.restObject.constructPath(blogItemList[0].More_details_page_link);
                        var blogpageURL = huburl + portno + queryParameter+self.pageLinkValue;
                        blogTempData=await self.restObject.gettingCMSData(blogpageURL,"GET",undefined,langauage,"");
                        var viewRedirectObject={Blog:blogItemList[0],Agecny:Agencycode};
                        if(added=='false'){
                            self.restObject.updateViewCount(viewRedirectObject);
                        }
                        
                    }
                }
                
                
                
                
                
                
                self.constructBlogPageData(blogTempData,masteTempData,blogMainTempData);
            });

        },
        async constructBlogPageData(blogTempData,masteTempData,blogMainTempData){
            this.headerDataConstruct(masteTempData);
            var titleDataTemp={};
            if(blogTempData!=undefined&&blogTempData.area_List!=undefined){
                var titleDataTemp =await this.restObject.getAllMapData(blogTempData.area_List[0].Blog_Details.component);
                var tagStr=await this.restObject.contstrutcTags(titleDataTemp.Tags);
                var socialMediaItems=await this.restObject.consturctSocialMedia(titleDataTemp.Social_Media);
                titleDataTemp.tagValue=tagStr;
                titleDataTemp.socialMediaItems=socialMediaItems;
                this.titleData=titleDataTemp;
            }
            if(blogMainTempData!=undefined&&blogMainTempData.area_List!=undefined){
                var titleMainDataTemp =await this.restObject.getAllMapData(blogMainTempData.area_List[0].Page_Details.component);
                this.titleMainData=titleMainDataTemp;
            }
            this.constructRecentPost(masteTempData);
            this.getData=true;
        },
        async headerDataConstruct(masteTempData){
            var allHeaderItems=[];
            if(masteTempData!=undefined&&masteTempData.Values!=undefined){
                for(let i=0;i<masteTempData.Values.length;i++){
                    let boxObject=masteTempData.Values[i];
                    let categoryID=boxObject.Category_Id;
                    let name=boxObject.Category_Name;
                    let rowObject=await this.restObject.checkListByID(allHeaderItems,categoryID);
                    if(rowObject==undefined){
                        rowObject={id:categoryID,Name:name,Count:1};
                        allHeaderItems.push(rowObject);
                    }else{
                        rowObject.Count=Number(rowObject.Count)+1;
                    }
                }
            }
            this.headerData=allHeaderItems;
        },
        async constructRecentPost(masteTempData){
            var allPostItems=[];
            if(masteTempData!=undefined&&masteTempData.Values!=undefined){
                var self = this;
                var categoryID=this.titleData.Category_Id;
                var allDatas = masteTempData.Values.filter(function (el) {
                    return el.Category_Id==categoryID&&el.Status ;
                });

                for(let i=0;i<allDatas.length;i++){
                    if(allDatas.length>i){
                        let boxObject=allDatas[i];
                        let createdDate=moment(boxObject.Created_Date).format('MMM DD YYYY');
                        let title=boxObject.Title;
                        let rowObject={id:boxObject.Id,image:boxObject.Blog_Image,title:title,createdDate:createdDate,comments:"1",rowObject:boxObject};
                        allPostItems.push(rowObject);
                    }
                    if(allPostItems.length==3){
                        break;
                    }
                }
            }
            this.recentPostList=allPostItems;
        },
        async addSubscribe(){
          var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
          if(this.nameEmail==undefined||this.nameEmail==''){
            alertify.alert('Warning','Please enter name to proceed.').set('closable', false);
            return;
          }else if(this.emailAddress==undefined||this.emailAddress==''){
            alertify.alert('Warning','Please enter email to proceed.').set('closable', false);
            return;
          }
          var matchArray = this.emailAddress.match(emailPat);
          if (matchArray == null) {
            alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
            return ;
          }
          let requestedDate=moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
          var restObject = await gettingRestClientObject();
          let agencyCode=this.agencyCode;

            var filterValue="type='Subscribe Details' AND keyword3='Blogs'  AND keyword2='"+this.emailAddress+"'";
            var allDBData=await this.restObject.getDbData4Table(agencyCode,filterValue,"date1");
             if(allDBData!=undefined&&allDBData.length>0){
                 alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                 return false;
             }
          let requestObject={ type:"Subscribe Details",keyword1:this.nameEmail,keyword2:this.emailAddress,keyword3:"Blogs",date1:requestedDate,nodeCode:agencyCode};
          var huburl = ServiceUrls.hubConnection.cmsUrl;
          var portno = ServiceUrls.hubConnection.ipAddress;
          var cmsURL = huburl + portno + "/cms/data";
          let responseObject = await this.restObject.gettingCMSData(cmsURL,"POST", requestObject,"en", "");
          try {
            let insertID=Number(responseObject);
            var fromEmail = JSON.parse(localStorage.User).loginNode.email;

            var custmail = {
                type: "UserAddedRequest",
                fromEmail: fromEmail,
                toEmails: Array.isArray(this.emailAddress) ? this.emailAddress : [this.emailAddress],
                logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                personName: this.nameEmail,
                primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
            };
            var emailApi = ServiceUrls.emailServices.emailApi;

            sendMailService(emailApi, custmail);

            alertify.alert('Success','Email subscribe sent successfully.');
            this.emailAddress="";
          } catch(e) {
          
          }
      }
    },
    mounted: function () {
        this.getPagecontent();
    },
});