const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})

var packagelist = new Vue({
    i18n,
    el: '#aboutUsPage',
    name: 'aboutUsPage',
    data: {
        content: null,
        getdata: true,
        pageTitle: '',
        pageBody: '',
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Privacy Policy/Privacy Policy/Privacy Policy.ftl';
                axios.get(aboutUsPage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (response.data.area_List.length > 0) {
                        self.pageTitle = self.pluckcom('Page_Title', response.data.area_List[0].main.component);
                        self.pageBody = self.pluckcom('Privacy_Policy', response.data.area_List[0].main.component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        }
    },
    mounted: function () {
        $("#abudhabi").show();
        this.getPagecontent();
    },
});

function activemap(region) {
    $(".slidesec").hide();
    $("#" + region).show();
}