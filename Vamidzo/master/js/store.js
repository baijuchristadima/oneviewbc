
  const store = new Vuex.Store({
    state: {
      // selectedElement: "index",
      selectedElement: localStorage.active_element
        ? localStorage.active_element
        : "index",
    },
    // getters
    getters: {
      selectedElement: (state) => {
        return state.selectedElement;
      },
    },
    // mutations
    mutations: {
      setActive: (state, payload) => {
        state.selectedElement = payload;
        localStorage.setItem("active_element", payload);
      },
    },
    // actions
    actions: {
      setActive: (context, payload) => {
        context.commit("setActive", payload);
      },
    },
  });
