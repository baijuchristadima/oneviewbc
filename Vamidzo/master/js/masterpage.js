const commonHub2 = new Vue(); // common hub
// Distribute to components using global mixin
Vue.mixin({
  data: function () {
    return {
      commonHub2: commonHub2,
    };
  },
});
// var router = new VueRouter({
//   mode: "history",
//   routes: [],
// });
var HeaderComponent = Vue.component("headeritem", {
  template: `
    <div class="header-top" id="header">
    <div class="vld-parent">
    <loading :active.sync="isLoading" :can-cancel="false" :is-full-page="fullPage"></loading>
</div>
    <div class="container relative">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="top-nav">
                    <nav id="navbar-main" class="navbar navbar-default">
                        <div class="navbar-header">
                            <a href="/Vamidzo/index.html" class="navbar-brand"><img :src="TopSection.Logo_188x70px" alt="logo"></a>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                      <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                         class="icon-bar"></span> </button>
                        </div>
                        <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                            <ul class="nav navbar-nav">
                                <li :class="{ 'active': activeClass === '/index.html' || activeClass === '/' || activeClass === '#/' || activeClass === 'index'}" @click.prevent="redirection('#/')"><a href="#flights" aria-controls="flights" role="tab" data-toggle="tab">{{TopSection.Home_Label}}</a></li>
                                <li :class="{ 'active': activeClass==='#flights'}" @click.prevent="redirection('#flights')"><a href="#flights" aria-controls="flights" role="tab" data-toggle="tab">{{TopSection.Flight_Label}}</a>
                                </li>
                                <li :class="{ 'active': activeClass === '#hotels'}" @click.prevent="redirection('#hotels')"><a href="#hotels" aria-controls="hotel" role="tab" data-toggle="tab">{{TopSection.Hotels_Label}}</a></li>
                                <li :class="{ 'active': activeClass === '#packages'}" @click.prevent="redirection('#packages')"><a href="#packages" aria-controls="packages" role="tab" data-toggle="tab">{{TopSection.Holidays_Label}}</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>

            <div class="col-md-6 col-sm-11 col-xs-12">
                <div class="call-to-action">


                    <a id="modal_retrieve" href="#modal" class="btn-clta" :title="TopSection.Retrieve_Booking_Label">{{TopSection.View_Booking_Label}}</a>
                    <div id="modal" class="popupContainer" style="display:none;">
                        <header class="popupHeader"> <span class="header_title">{{TopSection.Booking_lookup_Label}}</span> <span class="modal_close"><i class="fa fa-times"></i></span> </header>
                        <section class="popupBody">
                            <div class="user_login">
                                <div>
                                    <label>{{TopSection.Enter_email_address_Label}}</label>
                                    <div class="validation_sec">
                                    <input type="text" id="text" name="text" v-model="retrieveEmailId" :placeholder="TopSection.Email_Address_Placeholder" />
                                    <span v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveEmailErormsg.empty||retrieveEmailErormsg.invalid }"  class="cd-signin-modal__error sp_validation">{{retrieveEmailErormsg.empty?getValidationMsgByCode('M02'):(retrieveEmailErormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                                    </div>
                                    <small>{{TopSection.Email_Address_Description}}</small>
                                    <label>{{TopSection.ID_Number_Label}}</label>
                                    <div class="validation_sec">
                                    <input type="text" id="text" name="text" v-model="retrieveBookRefid" :placeholder="TopSection.ID_Number_Placeholder" />
                                    <span v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveBkngRefErormsg }"  class="cd-signin-modal__error sp_validation">{{getValidationMsgByCode('M34')}}</span> </div>
                                    <small>{{TopSection.ID_Number_Description}}</small>
                                    <button type="submit" id="retrieve-booking" class="btn-blue" v-on:click="retrieveBooking">{{TopSection.Continue_Label}}</button>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="popup">
                        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#Signin" v-show="!userlogined">{{TopSection.Log_In_Label}}</button>


                        <button type="button" class="btn btn-info cr-btn btn-lg" data-toggle="modal" data-target="#cr-account" v-show="!userlogined">{{TopSection.Create_Account_Label}}</button>
                        <button type="button" @click="agentRedirect(AgentLoginLink)" class="btn btn-info agent-login">
                        {{TopSection._Agent_Login_Label}}</button>
                        
                        <div class="modal modal-hd  fade" id="Signin" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="model-text">
                                            <h3>{{TopSection.Register_Heading_Label}}<br><span>{{TopSection.Register_SubHeading_Label}}</span></h3>
                                            <p v-html="TopSection.Register_Content">
                                              
                                            </p>
                                        </div>
                                        <div class="model-form">
                                            <h3>{{TopSection.Log_In_Label}}</h3>
                                            <hr>
                                            <form>
                                               <div class="validation_sec">
                                                <input type="email" id="email" name="email" class="form-control" :placeholder="TopSection.Email_Placeholder" required v-model="username">
                                                <span class="cd-signin-modal__error sp_validation" v-bind:class="{ 'cd-signin-modal__error--is-visible': usererrormsg.empty||usererrormsg.invalid }">{{usererrormsg.empty?getValidationMsgByCode('M02'):(usererrormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                                                </div>
                                                <div class="validation_sec">
                                                <input type="password" id="password" :placeholder="TopSection.Password_Placeholder" class="form-control" required v-model="password">
                                                <span class="cd-signin-modal__error sp_validation" v-bind:class="{ 'cd-signin-modal__error--is-visible': psserrormsg }">
                                                {{getValidationMsgByCode('M34')}}</span>
                                                </div>
                                                <input type="submit" id="submit" name="submit" :value="TopSection.Log_In_Label"  v-on:click.prevent="loginaction">
                                            </form>
                                            <a class="forgot-pass" href="#" data-toggle="modal" data-target="#forgot-pass" data-dismiss="modal">{{TopSection.Forget_Password_Label}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="forgot-pass" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="model-text">
                                            <h3>{{TopSection.Register_Heading_Label}}<br><span> {{TopSection.Register_SubHeading_Label}}</span></h3>
                                            <p v-html="TopSection.Register_Content">
                                              
                                            </p>
                                        </div>
                                        <div class="model-form">
                                            <h3>{{TopSection.Forgot_Password_Header_Label}}</h3>
                                            <hr>
                                            <p v-html="TopSection.Forget_Passord_description"></p>
                                            <form>
                                                <div class="validation_sec">
                                                <input type="email" id="email"  v-model="emailId" name="email" class="form-control" :placeholder="TopSection.Email_Placeholder" required>
                                                <span class="cd-signin-modal__error sp_validation" v-bind:class="{ 'cd-signin-modal__error--is-visible': userforgotErrormsg.empty||userforgotErrormsg.invalid }">
                                                {{userforgotErrormsg.empty?getValidationMsgByCode('M02'):(userforgotErrormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                                            </div>
                                                <input type="submit" id="submit" name="submit" :value="TopSection.Reset_Password_Label" v-on:click.prevent="forgotPassword">
                                            </form>
                                            <a class="back-login" href="#" data-toggle="modal" data-target="#Signin" data-dismiss="modal"
                                            >{{TopSection.Backto_Login_Label}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="cr-account" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="model-text">
                                            <h3>{{TopSection.Register_Heading_Label}}<br><span>{{TopSection.Register_SubHeading_Label}}</span></h3>
                                            <p v-html="TopSection.Register_Content">
                                              
                                            </p>
                                        </div>
                                        <div class="model-form">
                                            <h3>{{TopSection.Create_Account_Label}}</h3>
                                            <hr>
                                            <form>
                                            <div class="validation_sec" style="z-index:999;">
                                                <select  v-model="registerUserData.title">
                                        <option v-for="titlelist in TopSection.Title_List">{{titlelist.Title}}</option>
                                       
                                     </select>
                                     </div>
                                     <div class="validation_sec">
                                                <input type="text" id="text" name="text" class="form-control name-se" :placeholder="TopSection.First_Name_Placeholder" required  v-model="registerUserData.firstName">
                                                <span class="cd-signin-modal__error sp_validation" v-bind:class="{ 'cd-signin-modal__error--is-visible': userFirstNameErormsg }">
                                                {{getValidationMsgByCode('M35')}}</span>
                                         </div>
                                         <div class="validation_sec">
                                                <input type="text" id="text" name="text" class="form-control name-se" :placeholder="TopSection.Last_Name_Placeholder" required  v-model="registerUserData.lastName">
                                                <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userLastNameErrormsg }">{{getValidationMsgByCode('M36')}}</span>
                                                </div>
                                                <div class="validation_sec">
                                                <input type="email" id="email" name="email" class="form-control" :placeholder="TopSection.Email_Placeholder" required  v-model="registerUserData.emailId">
                                                <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userEmailErormsg.empty||userEmailErormsg.invalid }">{{userEmailErormsg.empty?getValidationMsgByCode('M02'):(userEmailErormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                                                </div>
                                                <input type="checkbox" id="checkbox" v-model="isChecked"> {{TopSection.Offer_and_Deal_Label}}
                                                <input type="submit" id="submit" name="submit" :value="TopSection.Create_Account_Label"  v-on:click.prevent="registerUser">
                                            </form>
                                            <p class="cr-ac">{{TopSection.Already_have_account_Label}} <a href="#" data-toggle="modal" data-target="#Signin" data-dismiss="modal">{{TopSection.Log_In_Label}}</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--dashbo-profile-->
                    <div class="profile-tab"  v-show="userlogined">
                        <div class="dropdown">

                            <!--  Log in with updated profile -->
                            <a v-if="userinfo && userinfo.title && userinfo.title.id" class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">     
                            <img class="profile-user" v-if="[2,3,4].indexOf(userinfo.title.id)<0" src="/assets/images/user.png" alt="user">
                            <img class="profile-user" v-else src="/assets/images/user_lady.png" alt="user">
                            {{userinfo.firstName}} <i class="fa fa-angle-down"></i>
                            </a>
                            <!--  Log in with as guest -->
                            <a v-else class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">     
                            <img class="profile-user" src="/assets/images/user.png" alt="user">
                            {{userinfo.firstName}} <i class="fa fa-angle-down"></i>
                            </a>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="/customer-profile.html"><i class="fa fa-dashboard"></i>{{TopSection.Dashboard_Label}}</a>
                                <a class="dropdown-item" href="/my-profile.html"><i class="fa fa-user"></i>{{TopSection.My_Profile_Label}}</a>
                                <a class="dropdown-item" href="/my-bookings.html"><i class="fa  fa-file-text-o"></i>{{TopSection.My_Booking_Label}}</a>
                                <a class="dropdown-item"  href="#" v-on:click.prevent="logout"> <i class="fa  fa-power-off"></i>{{TopSection.Log_Out_Label}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

  `,
  data() {
    return {
      language: "en",
      active_el: sessionStorage.active_el ? sessionStorage.active_el : 0,
      isChecked: false,
      TopSection: {},
      validationList: {},
      isLoading: false,
      fullPage: true,
      userlogined: this.checklogin(),
      userlogined: "",
      userinfo: [],
      username: "",
      password: "",
      emailId: "",
      retrieveEmailId: "",
      retrieveBookRefid: "",
      retrieveEmailErormsg: {
        empty: false,
        invalid: false,
      },
      psserrormsg: false,
      userFirstNameErormsg: false,
      userLastNameErrormsg: false,
      userEmailErormsg: {
        empty: false,
        invalid: false,
      },
      userPasswordErormsg: false,
      userVerPasswordErormsg: false,
      userPwdMisMatcherrormsg: false,
      userTerms: false,
      userforgotErrormsg: {
        empty: false,
        invalid: false,
      },

      usererrormsg: {
        empty: false,
        invalid: false,
      },
      retrieveBkngRefErormsg: false,
      registerUserData: {
        firstName: "",
        lastName: "",
        emailId: "",
        password: "",
        verifyPassword: "",
        terms: "",
        title: "Mr",
      },
      sessionids: "",
      Ipaddress: "",
      DeviceInfo: "",
      pageType: "",
      PageName: "",
      activeClass: "",
      topSectionLabel: {},
      AgentLoginLink: "",
    };
  },
  // computed: {
  //   activeClass: {
  //     get() {
  //       return store.getters.selectedElement;
  //     },
  //     set(value) {
  //       store.commit("setActive", value);
  //     },
  //   },
  // },
  created() {
    var self = this;
    var hash = window.location.hash;
    if (hash) {
      self.activeClass = hash;
    } else {
      self.activeClass = "/";
    }
    if (typeof commonHub === "undefined") {
      commonHub2.$on("change_element", (element) => {
        self.activeClass = element;
        console.log(element);
      });
    } else {
      commonHub.$on("change_element", (element) => {
        self.activeClass = element;
        console.log(element);
      });
    }
    var pathArray = window.location.pathname.split("/");
    var path = "/" + pathArray[pathArray.length - 1];
    if (path != "/" && path != "/index.html") {
      self.activeClass = "";
    }
  },
  methods: {
    redirection(to) {
      var pathArray = window.location.pathname.split("/");
      var current = "/" + pathArray[pathArray.length - 1];
      if (current != "/" && current != "/index.html") {
        // this.activeClass = to;
        // store.commit("setActive", to);
        if (typeof commonHub === "undefined") {
          commonHub2.$emit("change_element", to);
          // router.push("index.html" + to);
          // router.push({ path: "/Vamidzo/index.html", params: { active: to } });
          location.replace("/Vamidzo/index.html" + to);
        } else {
          commonHub.$emit("change_element", to);
          location.replace("/Vamidzo/index.html" + to);
          // router.push({ path: "/Vamidzo/index.html", params: { active: to } });
          // router.push("index.html" + to);
        }
      } else {
        // this.activeClass = to;
        if (typeof commonHub === "undefined") {
          commonHub2.$emit("change_element", to);
          // store.commit("setActive", to);
        } else {
          commonHub.$emit("change_element", to);
        }
      }
    },
    agentRedirect(link) {
      window.open(link, "_blank");
    },
    activate: function (el) {
      sessionStorage.active_el = el;
      this.active_el = el;
      if (el == 1 || el == 2) {
        maininstance.actvetab = el;
      } else {
        maininstance.actvetab = 0;
      }
    },
    getpagecontent: function () {
      var self = this;
      self.isLoading = true;
      getAgencycode(function (response) {
        self.AgentLoginLink = JSON.parse(localStorage.User).loginNode.apUrl;
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = localStorage.Languagecode
          ? localStorage.Languagecode
          : "en";
        var pageurl =
          huburl +
          portno +
          "/persons/source?path=/B2B/AdminPanel/CMS/" +
          Agencycode +
          "/Template/Master/Master/Master.ftl";
        axios
          .get(pageurl, {
            headers: {
              "content-type": "text/html",
              Accept: "text/html",
              "Accept-Language": langauage,
            },
          })
          .then(function (response) {
            if (response.data.area_List.length) {
              var headerSection = pluck(
                "Header_Section",
                response.data.area_List
              );
              self.TopSection = getAllMapData(headerSection[0].component);

              var Footer = pluck("Footer_Section", response.data.area_List);
              self.topSectionLabel = getAllMapData(Footer[0].component);
            }
            self.isLoading = false;
          })
          .catch(function (error) {
            self.isLoading = false;
            console.log("Error");
          });
      });
    },
    getValidationMsgs: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = localStorage.Languagecode
          ? localStorage.Languagecode
          : "en";
        var pageurl =
          huburl +
          portno +
          "/persons/source?path=/B2B/AdminPanel/CMS/" +
          Agencycode +
          "/Template/Validation/Validation/Validation.ftl";
        axios
          .get(pageurl, {
            headers: {
              "content-type": "text/html",
              Accept: "text/html",
              "Accept-Language": langauage,
            },
          })
          .then(function (response) {
            if (response.data.area_List.length) {
              var validations = pluck("Validations", response.data.area_List);
              self.validationList = getAllMapData(validations[0].component);
              sessionStorage.setItem(
                "validationItems",
                JSON.stringify(self.validationList)
              );
            }
          })
          .catch(function (error) {
            console.log("Error");
          });
      });
    },
    loginaction: function () {
      if (!this.username.trim()) {
        this.usererrormsg = {
          empty: true,
          invalid: false,
        };
        return false;
      } else if (!this.validEmail(this.username.trim())) {
        this.usererrormsg = {
          empty: false,
          invalid: true,
        };
        return false;
      } else {
        this.usererrormsg = {
          empty: false,
          invalid: false,
        };
      }
      if (!this.password) {
        this.psserrormsg = true;
        return false;
      } else {
        this.psserrormsg = false;
        var self = this;
        self.isLoading = true;
        login(this.username, this.password, function (response) {
          if (response == false) {
            self.userlogined = false;
            self.isLoading = false;
            alertify.alert(
              this.getValidationMsgByCode("M07"),
              this.getValidationMsgByCode("M31")
            );
          } else {
            self.userlogined = true;
            self.userinfo = JSON.parse(localStorage.User);
            $("#Signin").modal("toggle");

            try {
              self.$eventHub.$emit("logged-in", {
                userName: self.username,
                password: self.password,
              });
              signArea.headerLogin({
                userName: self.username,
                password: self.password,
              });
              // self.username ='';
              // self.password = '';
              self.isLoading = false;
            } catch (error) {
              self.isLoading = false;
            }
          }
        });
      }
    },
    validEmail: function (email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },
    checklogin: function () {
      if (localStorage.IsLogin == "true") {
        this.userlogined = true;
        this.userinfo = JSON.parse(localStorage.User);
        return true;
      } else {
        this.userlogined = false;
        return false;
      }
    },
    logout: function () {
      this.userlogined = false;
      this.userinfo = [];
      localStorage.profileUpdated = false;
      try {
        this.$eventHub.$emit("logged-out");
        signArea.logout();
      } catch (error) {}
      commonlogin();
      // signOut();
      // signOutFb();
    },
    registerUser: function () {
      if (this.registerUserData.firstName.trim() == "") {
        this.userFirstNameErormsg = true;
        return false;
      } else {
        this.userFirstNameErormsg = false;
      }
      if (this.registerUserData.lastName.trim() == "") {
        this.userLastNameErrormsg = true;
        return false;
      } else {
        this.userLastNameErrormsg = false;
      }
      if (this.registerUserData.emailId == "") {
        this.userEmailErormsg = { empty: true, invalid: false };
        return false;
      } else if (!this.validEmail(this.registerUserData.emailId)) {
        this.userEmailErormsg = { empty: false, invalid: true };
        return false;
      } else {
        this.userEmailErormsg = { empty: false, invalid: false };
      }
      var vm = this;
      vm.isLoading = true;
      registerUser(
        vm.registerUserData.emailId,
        vm.registerUserData.firstName,
        vm.registerUserData.lastName,
        vm.registerUserData.title,
        function (response) {
          if (response.isSuccess == true) {
            try {
              if (vm.isChecked) {
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format(
                  "YYYY-MM-DDThh:mm:ss"
                );
                let insertSubscibeData = {
                  type: "Newsletter",
                  date1: requestedDate,
                  keyword2: vm.registerUserData.emailId,
                  nodeCode: agencyCode,
                };

                let responseObject = cmsRequestData(
                  "POST",
                  "cms/data",
                  insertSubscibeData,
                  null
                );
                vm.registerUserData.firstName = "";
                vm.registerUserData.emailId = "";
                vm.registerUserData.title = "";
              }
            } catch (error) {
              vm.isLoading = false;
            }
            vm.username = response.data.data.user.loginId;
            vm.password = response.data.data.user.password;
            login(vm.username, vm.password, function (response) {
              if (response != false) {
                //  $('#Signin').modal('toggle');

                //  $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                window.location.href =
                  "/edit-my-profile.html?edit-profile=true";
                vm.isLoading = false;
              }
            });
          }
          vm.isLoading = false;
        }
      );
    },
    retrieveBooking: function () {
      if (this.retrieveEmailId.trim() == "") {
        //alert('Email required !');

        this.retrieveEmailErormsg = {
          empty: true,
          invalid: false,
        };
        return false;
      } else if (!this.validEmail(this.retrieveEmailId)) {
        //alert('Invalid Email !');
        this.retrieveEmailErormsg = {
          empty: false,
          invalid: true,
        };
        return false;
      } else {
        this.retrieveEmailErormsg = {
          empty: false,
          invalid: false,
        };
      }
      if (this.retrieveBookRefid == "") {
        this.retrieveBkngRefErormsg = true;
        return false;
      } else {
        this.retrieveBkngRefErormsg = false;
      }
      this.isLoading = true;
      var bookData = {
        BkngRefID: this.retrieveBookRefid,
        emailId: this.retrieveEmailId,
        redirectFrom: "retrievebooking",
        isMailsend: false,
      };
      localStorage.bookData = JSON.stringify(bookData);
      this.isLoading = false;
      window.location.href = "/Flights/flight-confirmation.html";
    },
    forgotPassword: function () {
      if (this.emailId.trim() == "") {
        this.userforgotErrormsg = {
          empty: true,
          invalid: false,
        };
        return false;
      } else if (!this.validEmail(this.emailId.trim())) {
        this.userforgotErrormsg = {
          empty: false,
          invalid: true,
        };
        return false;
      } else {
        this.userforgotErrormsg = {
          empty: false,
          invalid: false,
        };
      }
      var self = this;
      self.isLoading = true;
      var datas = {
        emailId: this.emailId,
        agencyCode: localStorage.AgencyCode,
        logoUrl:
          window.location.origin +
          "/" +
          localStorage.AgencyFolderName +
          "/website-informations/logo/logo.png",
        websiteUrl: window.location.origin,
        resetUri:
          window.location.origin +
          "/" +
          localStorage.AgencyFolderName +
          "/reset-password.html",
      };
      $(".cd-signin-modal__input[type=submit]").css(
        "cssText",
        "pointer-events:none;background:grey !important;"
      );

      var huburl = ServiceUrls.hubConnection.baseUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var requrl = ServiceUrls.hubConnection.hubServices.forgotPasswordUrl;

      axios
        .post(huburl + portno + requrl, datas)
        .then((response) => {
          if (response.data != "") {
            alertify.alert(this.getValidationMsgByCode("M07"), response.data);
            this.emailId = "";
          } else {
            alertify.alert(
              this.getValidationMsgByCode("M07"),
              this.getValidationMsgByCode("M32")
            );
          }
          $(".cd-signin-modal__input[type=submit]").css(
            "cssText",
            "pointer-events:auto;background:var(--primary-color) !important"
          );
          self.isLoading = false;
          $("#forgot-pass").modal("toggle");
        })
        .catch((err) => {
          console.log("FORGOT PASSWORD  ERROR: ", err);
          if (
            err.response.data.message ==
            "No User is registered with this emailId."
          ) {
            alertify.alert(
              this.getValidationMsgByCode("M07"),
              err.response.data.message
            );
          } else {
            alertify.alert(
              this.getValidationMsgByCode("M07"),
              this.getValidationMsgByCode("M33")
            );
          }
          $(".cd-signin-modal__input[type=submit]").css(
            "cssText",
            "pointer-events:auto;background:var(--primary-color) !important"
          );

          self.isLoading = false;
        });
    },
    Getvisit: function () {
      var self = this;
      var currentUrl = window.location.pathname;
      if (currentUrl == "/Vamidzo/index.html" || currentUrl == "/Vamidzo/") {
        self.pageType = "Master";
        self.PageName = "Home";
      } else if (currentUrl == "/Vamidzo/package-detail.html") {
        self.pageType = "Package details";
        self.PageName = packageView.mainContent.Package_Title;
      } else {
        currentUrl = currentUrl.split("/Vamidzo/")[1];
        currentUrl = currentUrl.split(".html")[0];
        self.PageName = currentUrl;
        self.pageType = "Master";
      }

      var TempDeviceInfo = detect.parse(navigator.userAgent);
      self.DeviceInfo = TempDeviceInfo;
      this.showYourIp();
    },
    showYourIp: function () {
      var self = this;
      var ipUrl = ServiceUrls.externalServiceUrl.ipAddressApi;
      fetch(ipUrl)
        .then((x) => x.json())
        .then(({ ip }) => {
          let Ipaddress = ip;
          let sessionids = ip.replace(/\./g, "");
          this.sendip(Ipaddress, sessionids);
        });
    },
    sendip: async function (Ipaddress, sessionids) {
      var self = this;
      if (
        self.PageName == null ||
        self.PageName == undefined ||
        self.PageName == ""
      ) {
        var pathArray = window.location.pathname.split("/");
        var activePage = pathArray[pathArray.length - 1];
      }
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
      let requestedDate = moment(String(new Date())).format(
        "YYYY-MM-DDThh:mm:ss"
      );
      let insertVisitData = {
        type: "Visit",
        keyword1: sessionids,
        ip1: Ipaddress,
        keyword2: self.pageType,
        keyword3: self.PageName,
        keyword4: localStorage.selectedCurrency
          ? localStorage.selectedCurrency
          : "AED",
        keyword5: localStorage.Languagecode ? localStorage.Languagecode : "en",
        text1: self.DeviceInfo.device.type,
        text2: self.DeviceInfo.os.name,
        text3: self.DeviceInfo.browser.name,
        date1: requestedDate,
        nodeCode: agencyCode,
      };
      cmsRequestData("POST", "cms/data", insertVisitData, null);
    },
    getValidationMsgByCode: function (code) {
      if (sessionStorage.validationItems !== undefined) {
        var validationList = JSON.parse(sessionStorage.validationItems);
        for (let validationItem of validationList.Validation_List) {
          if (code === validationItem.Code) {
            return validationItem.Message;
          }
        }
      }
    },
  },

  mounted: function () {
    $("#modal_retrieve").leanModal({
      top: 100,
      overlay: 0.6,
      closeButton: ".modal_close",
    });
    $(function () {
      $(".selectpicker").selectpicker();
    });
    if (localStorage.IsLogin == "true") {
      this.userlogined = true;
      this.userinfo = JSON.parse(localStorage.User);
    } else {
      this.userlogined = false;
    }
    this.getpagecontent();
    this.getValidationMsgs();
    setTimeout(() => {
      this.Getvisit();
    }, 1000);
  },

  watch: {
    updatelogin: function () {
      if (localStorage.IsLogin == "true") {
        this.userlogined = true;
        this.userinfo = JSON.parse(localStorage.User);
      } else {
        this.userlogined = false;
      }
    },
  },
});

var headerinstance = new Vue({
  el: "header",
  name: "headerArea",
  // router,
  data() {
    return {
      key: "",
      content: null,
      getdata: true,
    };
  },
});
Vue.component("footeritem", {
  template: `
    <div id="footer">
    <div class="vld-parent">
    <loading :active.sync="isLoading" :can-cancel="false" :is-full-page="fullPage"></loading>
</div>
    <section class="ft-hotels">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-6 p-im-0">
                  <div class="hotels-img">
                     <img :src="FooterSection.Footer_Image_660x335px" alt="hotel">
                  </div>

               </div>
               <div class="container">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="hotel-info">
                           <h2><span>{{FooterSection.Footer_Hotel_Heading}}</span><br>{{FooterSection.Footer_Hotel_SubHeading}}</h2>
                           <p v-html="FooterSection.Footer_Hotel_Description"></p>
                           <a href="/Vamidzo/package.html" class="btn">{{FooterSection.Book_Now_Button_Label}}</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <div class="footer-in">
         <div class="container">
            <div class="row">

               <div class="col-md-12">
                  <div class="ftr-contact">
                     <ul>
                        <li> <span><i :class="FooterSection.Address_Icon"></i></span>
                           <p v-html="FooterSection.Address_Description_Label"></p>
                        </li>
                        <li><span><i :class="FooterSection.Email_Icon"></i> </span>
                              <a :href="'mailto:'+FooterSection.Email_Address">{{FooterSection.Email_Address}}</a>
                        </li>
                        <li><span><i :class="FooterSection.Phone_Icon"></i></span>
                           <a :href="'tel:'+FooterSection.Phone_Number">{{FooterSection.Phone_Number}}</a>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-3 col-sm-3">
                  <div class="about-footer">
                     <div class="ftr-logo"><a href="/Vamidzo/index.html"><img :src="TopSection.Logo_188x70px"
                              alt="footer-logo"></a></div>
                     <p v-html="FooterSection.Description"></p>

                  </div>
               </div>
               <div class="col-md-4 col-sm-4">
                  <div class="quick-link">
                     <h3>{{FooterSection.Quick_Link_Label}}</h3>
                     <ul class="ft-link">
                        <li><i class="fa   fa-angle-double-right"></i><a href="/Vamidzo/index.html">{{TopSection.Home_Label}}</a></li>
                        <li><i class="fa   fa-angle-double-right"></i><a href="/Vamidzo/about.html">{{FooterSection.About_us_Label}}</a></li>
                        <li><i class="fa   fa-angle-double-right"></i><a href="/Vamidzo/contact.html">{{FooterSection.Contact_Us_Label}}</a></li>
                        <li><i class="fa   fa-angle-double-right"></i><a href="/Vamidzo/privacypolicy.html">{{FooterSection.Privacy_Policy_Label}}</a>
                        </li>
                        <li><i class="fa   fa-angle-double-right"></i><a href="/Vamidzo/termsandconditions.html">{{FooterSection.Terms_of_Use_Label}}</a></li>
                        <li><i class="fa   fa-angle-double-right"></i><a href="/Vamidzo/package.html">{{TopSection.Holidays_Label}}</a></li>
                     </ul>
                     <h3>{{FooterSection.Follow_Us_Label}}</h3>
                     <ul class="social"><li v-for="social in FooterSection.Social_Media_List"><a :href="social.Url" target="_blank"><i :class="social.Icon"></i></a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-5 col-sm-4">
                  <div class="ft-newsletter">
                     <h3>{{FooterSection.Suscribe_and_Newsletter_Label}}</h3>
                     <input type="email" id="email" name="email" :placeholder="FooterSection.Email_Placeholder" v-model="newsltremail">
                     <button type="submit" @click.prevent="sendnewsletter">{{FooterSection.Send_Button_Label}}</button>
                  </div>
               </div>

            </div>

         </div>
      </div>
      <div class="ftr-btm">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="copyright">{{FooterSection.Copyright_Description}}</div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-4">
                  <div class="powered-by">{{FooterSection.Powered_By_Label}} &nbsp; <a href="http://www.oneviewit.com/" target="_blank"><img
                           alt="" :src="FooterSection.Powered_By_Image"></a></div>
               </div>
            </div>
         </div>
      </div>
      </div>
    `,
  data() {
    return {
      TopSection: {},
      FooterSection: {},
      isLoading: false,
      fullPage: true,
      newsltremail: null,
    };
  },
  methods: {
    getpagecontent: function () {
      var self = this;
      self.isLoading = true;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = localStorage.Languagecode
          ? localStorage.Languagecode
          : "en";
        var pageurl =
          huburl +
          portno +
          "/persons/source?path=/B2B/AdminPanel/CMS/" +
          Agencycode +
          "/Template/Master/Master/Master.ftl";

        axios
          .get(pageurl, {
            headers: {
              "content-type": "text/html",
              Accept: "text/html",
              "Accept-Language": langauage,
            },
          })
          .then(function (response) {
            if (response.data.area_List.length) {
              var headerSection = pluck(
                "Header_Section",
                response.data.area_List
              );
              self.TopSection = getAllMapData(headerSection[0].component);

              var FooterContent = pluck(
                "Footer_Section",
                response.data.area_List
              );
              self.FooterSection = getAllMapData(FooterContent[0].component);

              self.isLoading = false;
            }
          })
          .catch(function (error) {
            console.log("Error");
            self.isLoading = false;
          });
      });
    },
    getValidationMsgByCode: function (code) {
      if (sessionStorage.validationItems !== undefined) {
        var validationList = JSON.parse(sessionStorage.validationItems);
        for (let validationItem of validationList.Validation_List) {
          if (code === validationItem.Code) {
            return validationItem.Message;
          }
        }
      }
    },
    sendnewsletter: async function () {
      if (!this.newsltremail) {
        alertify
          .alert(
            this.getValidationMsgByCode("M07"),
            this.getValidationMsgByCode("M02")
          )
          .set("closable", false);
        return false;
      }

      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.newsltremail.match(emailPat);
      if (matchArray == null) {
        alertify
          .alert(
            this.getValidationMsgByCode("M07"),
            this.getValidationMsgByCode("M03")
          )
          .set("closable", false);
        return false;
      } else {
        try {
          this.isLoading = true;
          this.agencyCode = JSON.parse(localStorage.User).loginNode.code || "";
          var agencyCode = this.agencyCode;
          var filterValue =
            "type='Newsletter' AND keyword2='" + this.newsltremail + "'";
          var allDBData = await this.getDbData4Table(
            this.agencyCode,
            filterValue,
            "date1"
          );
          if (
            allDBData != undefined &&
            allDBData.data != undefined &&
            allDBData.count > 0
          ) {
            alertify
              .alert(
                this.getValidationMsgByCode("M07"),
                this.getValidationMsgByCode("M04")
              )
              .set("closable", false);
            this.isLoading = false;
            console.log(allDBData);
            return false;
          } else {
            try {
              // var frommail = JSON.parse(localStorage.User).loginNode.email;
              // var frommail = "itsolutionsoneview@gmail.com";
              var toEmail = JSON.parse(localStorage.User).loginNode.email;
              var frommail=JSON.parse(localStorage.User).loginNode.email;
              // var frommail = " itsolutionsoneview@gmail.com";
              var postData = {
                type: "AdminContactUsRequest",
                fromEmail: frommail,
                toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                logo:
                  ServiceUrls.hubConnection.logoBaseUrl +
                  JSON.parse(localStorage.User).loginNode.logo +
                  ".xhtml?ln=logo",
                agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                agencyAddress:
                  JSON.parse(localStorage.User).loginNode.address || "",
                personName: this.newsltremail.split("@")[0],
                emailId: this.newsltremail,
                //  contact: "9078978967",
                //  message: "hello",
              };
              var custmail = {
                type: "UserAddedRequest",
                fromEmail: frommail,
                toEmails: Array.isArray(this.newsltremail)
                  ? this.newsltremail
                  : [this.newsltremail],
                logo: JSON.parse(localStorage.User).loginNode.logo,
                agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                agencyAddress:
                  JSON.parse(localStorage.User).loginNode.address || "",
                personName: this.newsltremail.split("@")[0],
                primaryColor: "#ffffff",
                // primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                secondaryColor:
                  "#" +
                  JSON.parse(localStorage.User).loginNode.lookNfeel.secondary,
              };
              var agencyCode = this.agencyCode;
              let requestedDate = moment(String(new Date())).format(
                "YYYY-MM-DDThh:mm:ss"
              );
              let insertSubscibeData = {
                type: "Newsletter",
                date1: requestedDate,
                keyword2: this.newsltremail,
                nodeCode: agencyCode,
              };
              let responseObject = await cmsRequestData(
                "POST",
                "cms/data",
                insertSubscibeData,
                null
              );
              try {
                let insertID = Number(responseObject);
                this.newsltremail = "";
                this.newsltrname = "";
                var emailApi = ServiceUrls.emailServices.emailApi;
                sendMailService(emailApi, custmail);
                sendMailService(emailApi, postData);
                alertify
                  .alert(
                    this.getValidationMsgByCode("M15"),
                    this.getValidationMsgByCode("M23")
                  )
                  .set("closable", false);
                this.isLoading = false;
              } catch (e) {
                this.isLoading = false;
              }
            } catch (error) {
              this.isLoading = false;
            }
          }
        } catch (error) {
          this.isLoading = false;
        }
      }
    },
    async getDbData4Table(agencyCode, extraFilter, sortField) {
      var allDBData = [];
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var cmsURL = huburl + portno + "/cms/data/search/byQuery";
      var queryStr =
        "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
      if (extraFilter != undefined && extraFilter != "") {
        queryStr = queryStr + " AND " + extraFilter;
      }
      var requestObject = {
        query: queryStr,
        sortField: sortField,
        from: 0,
        orderBy: "desc",
      };
      let responseObject = await cmsRequestData(
        "POST",
        "cms/data/search/byQuery",
        requestObject,
        null
      );
      if (responseObject != undefined && responseObject.data != undefined) {
        allDBData = responseObject;
      }
      return allDBData;
    },
  },
  mounted: function () {
    this.getpagecontent();
  },
});

var footerinstance = new Vue({
  el: "footer",
  name: "footerArea",
  data() {
    return {
      key: 0,
      content: null,
      getdata: true,
    };
  },
});
