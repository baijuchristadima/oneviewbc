Vue.component('loading', VueLoading)
var contact = new Vue({
    el: '#contact',
    name: 'contact',
    data: {
        bannerSection: {},
        contactArea: {},
        isLoading: false,
        fullPage: true,
        cntuserfirstname: '',
        cntuserlastname: '',
        cntemail: '',
        cntcontact: '',
        cntmessage: '',
    },
    methods: {
        getContactData: function() {
            var self = this;
            self.isLoading = true;
            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var contctusurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                axios.get(contctusurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                        var bannerDetail = pluck('Banner_Area', response.data.area_List);
                        self.bannerSection = getAllMapData(bannerDetail[0].component);
                        var mainDetail = pluck('Main_Area', response.data.area_List);
                        self.contactArea = getAllMapData(mainDetail[0].component);
                    }
                    self.isLoading = false;
                }).catch(function(error) {
                    console.log(Error);
                    self.isLoading = false;
                });
            });

        },
        sendcontactus: async function() {

            if (!this.cntuserfirstname.trim()) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M35')).set('closable', false);

                return false;
            }
            if (!this.cntuserlastname.trim()) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M36')).set('closable', false);
                return false;
            }
            if (!this.cntemail.trim()) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M02')).set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M03')).set('closable', false);
                return false;
            }
            if (!this.cntcontact.trim()) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M19')).set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8 || this.cntcontact.length > 16) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M20')).set('closable', false);
                return false;
            }

            if (!this.cntmessage.trim()) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M22')).set('closable', false);
                return false;
            } else {
                try {
                    //var self =this;
                    this.isLoading = true;
                    //var toEmails = JSON.parse(localStorage.User).loginNode.email;
                    var toEmail = this.cntemail;
                    var frommail = JSON.parse(localStorage.User).loginNode.email;
                    // var frommail = "itsolutionsoneview@gmail.com";
                    var postData = {
                        type: "AdminContactUsRequest",
                        fromEmail: frommail,
                        toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                        logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                        agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                        agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                        personName: this.cntusername,
                        emailId: this.cntemail,
                        contact: this.cntcontact,
                        message: this.cntmessage,

                    };
                    var custmail = {
                        type: "UserAddedRequest",
                        fromEmail: frommail,
                        toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                        logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                        agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                        agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                        personName: this.cntuserfirstname + " "+this.cntuserlastname,
                        primaryColor: '#ffffff',
                        // primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                        secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary
                    };

                    let agencyCode = localStorage.AgencyCode;
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertContactData = {
                        type: "Contact US",
                        keyword1: this.cntuserfirstname,
                        keyword2: this.cntemail,
                        keyword4: this.cntcontact,
                        keyword3: this.cntuserlastname,
                        text1: this.cntmessage,
                        date1: requestedDate,
                        nodeCode: agencyCode
                    };
                    let responseObject = await cmsRequestData("POST", "cms/data", insertContactData, null);
                    try {
                        let insertID = Number(responseObject);
                        var emailApi = ServiceUrls.emailServices.emailApi;
                        sendMailService(emailApi, postData);
                        sendMailService(emailApi, custmail);
                        this.cntemail = '';
                        this.cntuserfirstname = '';
                        this.cntcontact = '';
                        this.cntmessage = '';
                        this.cntuserlastname = '';
                        this.isLoading = false;
                        alertify.alert(this.getValidationMsgByCode('M15'), this.getValidationMsgByCode('M23'));

                    } catch (e) {
                        this.isLoading = false;
                    }
                } catch (error) {
                    this.isLoading = false;
                }




            }
        },
        getValidationMsgByCode: function(code) {
            if (sessionStorage.validationItems !== undefined) {
                var validationList = JSON.parse(sessionStorage.validationItems);
                for (let validationItem of validationList.Validation_List) {
                    if (code === validationItem.Code) {
                        return validationItem.Message;
                    }
                }
            }
        },

    },
    mounted: function() {
        this.getContactData();
    }
});