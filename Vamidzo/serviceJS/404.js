//Vue.component('loading', VueLoading)
var contact = new Vue({
    el: '#errorpage',
    name: 'errorpage',
    data: {
        mainArea: {},
    },
    methods: {
        getData: function() {
            var self = this;
            // self.isLoading = true;
            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var contctusurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/404 Page/404 Page/404 Page.ftl';
                axios.get(contctusurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                        // console.log(response.data.area_List);

                        var mainDetail = pluck('Main_Section', response.data.area_List);
                        self.mainArea = getAllMapData(mainDetail[0].component);

                    }
                    self.isLoading = false;
                }).catch(function(error) {
                    console.log(Error);
                    // self.isLoading = false;
                });
            });

        },
    },
    mounted: function() {
        this.getData();
    }
});