Vue.component('loading', VueLoading)
var aboutPage = new Vue({
    el: '#policy',
    name: 'policy',
    data: {
        banner: {},
        privacypolicyArea: {},
        isLoading: false,
        fullPage: true,
    },
    methods: {
        getData: function() {
            var self = this;
            self.isLoading = true;
            getAgencycode(function(response) {
                var Agencycode = response;
                console.log(Agencycode);
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Privacy Policy Page/Privacy Policy Page/Privacy Policy Page.ftl';
                axios.get(aboutUsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                        var bannerDetail = pluck('Banner_Area', response.data.area_List);
                        self.banner = getAllMapData(bannerDetail[0].component);
                        var policyDetail = pluck('Main_Area', response.data.area_List);
                        self.privacypolicyArea = getAllMapData(policyDetail[0].component);
                        self.isLoading = false;

                    }
                }).catch(function(error) {
                    console.log(error);
                    self.isLoading = false;
                });
            });

        }
    },
    mounted: function() {
        this.getData();
    }
});