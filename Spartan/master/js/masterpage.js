var HeaderComponent = Vue.component('headeritem', {
    template: `<div v-cloak><div  v-if="getdata"></div>
    <div id="header" class="ar_direction">
    <nav id="navbar-main" class="navbar navbar-default">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand" href="/Spartan/index.html">
                    <img :src="headerSection.Logo_210_x_210px" alt="" class="hdr-logo">
                    <img style="width:37%" src="https://ov-cms.s3.ap-south-1.amazonaws.com/B2B/AdminPanel/CMS/AGY20621/Images/19062021043348.svg">
                </a>
              </div>
              <div class="call-to-action">
              <!--<div class="lang lag">
                    <currency-select></currency-select>          
                </div>--> 
                <div class="lang lag">
                    <lang-select @languagechange="headerData"></lang-select>
                </div>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
              <ul class="nav navbar-nav">
              <li  @click="activate(1)" :class="{ active : active_e == 9 }"><a href="/Spartan/index.html">{{headerSection.Home_Label}}</a></li>
              <li  @click="activate(11)" :class="{ active : active_e == 11 }"><a href="/Spartan/hotel-overview.html">{{headerSection.Hotel_Overview}}</a></li>
              <li  @click="activate(10)" :class="{ active : active_e == 10 }"><a href="/Spartan/camp-overview.html">{{headerSection.Self_Camping_Label}}</a></li>
              <!--<li  @click="activate(1)" :class="{ active : active_e == 1 }"><a href="/Spartan/flights.html">{{headerSection.Flights_Label}}</a></li>
              <li @click="activate(2)" :class="{ active : active_e == 2 }"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{headerSection.Accommodation_Label }}<b class="caret"></b></a>
				  <ul class="dropdown-menu multi-level">
					<li><a href="/Spartan/hotel.html">{{headerSection.Hotels_Label}}</a></li>
					<li><a href="/Spartan/tent-accommodation.html">{{headerSection.Tent_Accommodation_Label}}</a></li>
					<li tabindex="-1" class="dropdown-submenu"><a href="/Spartan/camp-overview.html">{{headerSection.Self_Camping_Label}}</a>
                        <ul class="dropdown-menu sub-menu-camping">
                            <li><a href="/Spartan/self-camping.html?set=SC1">{{headerSection.Book_Your_Space_Label}}</a></li>
                            <li><a href="/Spartan/self-camping.html?set=SC2">{{headerSection.Self_Camping_Package_Label}}</a></li>
                            <li><a href="/Spartan/self-camping.html?set=SC3">{{headerSection.Camping_Equipment_Label}}</a></li>
                        </ul>   
                    </li>
				  </ul>
				</li>-->
              <!--<li  @click="activate(2)" :class="{ active : active_e == 2 }"><a href="/Spartan/hotel.html">{{headerSection.Hotels_Label}}</a></li>-->
              <!--<li  @click="activate(2)" :class="{ active : active_e == 7 }"><a href="/Spartan/tent-accommodation.html">{{headerSection.Tent_Accommodation_Label}}</a></li>
              <li  @click="activate(3)" :class="{ active : active_e == 3 }"><a href="/Spartan/transfers.html">{{headerSection.Transfers_Label}}</a></li>
              <li  @click="activate(6)" :class="{ active : active_e == 6 }"><a href="/Spartan/car-rental.html">{{headerSection.Car_Rental_Label}}</a></li>
              <li  @click="activate(4)" :class="{ active : active_e == 4 }"><a href="/Spartan/tours.html">{{headerSection.Tour_Label}}</a></li>
              <li  @click="activate(5)" :class="{ active : active_e == 5 }"><a href="/Spartan/visa.html">{{headerSection.Visa_Label}}</a></li>-->
                <!--<li role="presentation" class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                  Others <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li> <a href ="meet-and-assist.html"><i class="fa fa-plus" aria-hidden="true"></i> Meet and Assit</a>
                  <li> <a href ="/Spartan/visa.html"><i class="fa fa-plus"></i> Visa</a></li>
                </ul>
                </li>-->
              </ul>
              <ul class="nav navbar-nav navbar-right right_area">
              <li  @click="activate(7)" :class="{ active : active_e == 7 }"><a href="/Spartan/plan-my-trip.html">{{headerSection.Plan_My_Trip_Label}}</a></li>
              <!--<li><a href="#" data-toggle="modal" data-target="#myModal">{{headerSection.View_Bookings_Label}}</a></li>-->
              <!--<li id="sign-bt-area" class="login-reg  js-signin-modal-trigger" v-show="!userlogined"><a href="javascript:void(0);" data-signin="login" ><span data-signin="login">{{headerSection.Login_Register_Label}}</span></a></li>
                <li v-show="userlogined" class="ar_direction">
                                <!--iflogin-->
                                <!--<label id="bind-login-info" for="profile2" class="profile-dropdown">
                                    <input type="checkbox" id="profile2">
                                    <img src="/assets/images/user.png">
                                    <span>{{userinfo.firstName+' '+userinfo.lastName }}</span>
                                    <label for="profile2">
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    </label>
                                    <ul>
                                        <li>
                                            <a href="/customer-profile.html">
                                                <i class="fa fa-th-large" aria-hidden="true"></i>{{headerSection.Dashboard_Label}}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/my-profile.html">
                                                <i class="fa fa-user" aria-hidden="true"></i>{{headerSection.My_Profile_Label}}
                                            </a>
                                        </li>
                                        <li>   
                                            <a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i>{{headerSection.My_Booking_Label}}</a>
                                        </li>
                                        <li>
                                            <a href="#" v-on:click.prevent="logout"><i class="fa fa-power-off" aria-hidden="true"></i>{{headerSection.Log_Out_Label}}</a>
                                        </li>
                                    </ul>
                                </label>-->
                                <!--ifnotlogin-->
                           <!-- </li>-->
                
                
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>
  <!--popup area-->
        <div class="cd-signin-modal js-signin-modal ar_direction"> <!-- this is the entire modal form, including the background -->
		<div class="cd-signin-modal__container"> <!-- this is the container wrapper -->
			<ul class="cd-signin-modal__switcher js-signin-modal-switcher js-signin-modal-trigger">
				<li><a href="#0" data-signin="login" data-type="login">{{headerSection.Sign_In_Label}}</a></li>
				<li><a href="#0" data-signin="signup" data-type="signup">{{headerSection.Register_Label}}</a></li>
			</ul>

			<div class="cd-signin-modal__block js-signin-modal-block" data-type="login"> <!-- log in form -->
                <div class="cd-signin-modal__form">
					<p class="cd-signin-modal__fieldset">
                        <label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" 
                        for="signin-email">Email</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signin-email" type="email" :placeholder="headerSection.Email_Label" v-model="username" >
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': usererrormsg.empty||usererrormsg.invalid }">{{usererrormsg.empty?'Please enter email!':(usererrormsg.invalid?'Email seems incorrect!':'')}}</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <label class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace" 
                        for="signin-password">Password</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signin-password" type="password"  :placeholder="headerSection.Password_Placeholder"  v-model="password" >
						<a v-on:click="showhidepassword" class="cd-signin-modal__hide-password js-hide-password changeShowTxtCls">{{headerSection.Show_Label}}</a>
                        <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': psserrormsg }">
                        Please enter password!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width" type="submit"
                        v-on:click="loginaction" :value="headerSection.Login_Button_Label">
                    </p>
                    <div id="myGoogleButton"></div>
                    <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false"
        data-use-continue-as="false" onlogin="checkLoginState();"></div>
                    <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="reset">{{headerSection.Forgot_Your_Password_Label}}</a></p>
				</div>
				
				
			</div> <!-- cd-signin-modal__block -->

			<div class="cd-signin-modal__block js-signin-modal-block" data-type="signup"> <!-- sign up form -->
                <div class="cd-signin-modal__form">
                    <p class="cd-signin-modal__fieldset">
                        <label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">Title</label>                        
                        <select v-model="registerUserData.title" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border down_select" id="signup-title">
                            <option selected>Mr</option>
                            <option selected>Ms</option>
                            <option>Mrs</option>
                        </select>
                        <span class="cd-signin-modal__error">Title seems incorrect!</span>
                    </p>
					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">First Name</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signup-firstname" type="text" :placeholder="headerSection.First_Name_Placeholder" v-model="registerUserData.firstName">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userFirstNameErormsg }" >{{headerSection.First_Name_Alert_}}</span>
					</p>

					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">Last Name</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signup-lastname" type="text" :placeholder="headerSection.Last_Name_Placeholder_" v-model="registerUserData.lastName">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userLastNameErrormsg }">{{headerSection.Last_Name_Alert}}</span>
					</p>

					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" for="signup-email">Email</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signup-email" type="email" :placeholder="headerSection.Email_Placeholder" v-model="registerUserData.emailId">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userEmailErormsg.empty||userEmailErormsg.invalid }">{{userEmailErormsg.empty? headerSection.Email_Alert :(userEmailErormsg.invalid? headerSection.Incorrect_Email_Alert :'')}}</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding" type="submit" 
                        :value="headerSection.Create_Account_Label" v-on:click="registerUser">
                    </p>
                    <div id="myGoogleButtonReg"></div>
                    <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false"
        data-use-continue-as="false" onlogin="checkLoginState();"></div>                    
				</div>
			</div> <!-- cd-signin-modal__block -->

			<div class="cd-signin-modal__block js-signin-modal-block" data-type="reset"> <!-- reset password form -->
				<p class="cd-signin-modal__message">{{headerSection.Forgot_Password_Description}}</p>

				<div class="cd-signin-modal__form">
					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" for="reset-email">E-mail</label>
						<input v-model="emailId" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" id="reset-email" type="email" :placeholder="headerSection.Email_Placeholder">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userforgotErrormsg.empty||userforgotErrormsg.invalid }">{{userforgotErrormsg.empty? headerSection.Email_Alert :(userforgotErrormsg.invalid? headerSection.Incorrect_Email_Alert :'')}}</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding" type="submit" 
                        :value="headerSection.Reset_Password_Button_Label" v-on:click="forgotPassword">
                    </p>
                    <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="login">{{headerSection.Back_To_Login_Label}}</a></p>
				</div>

				
			</div> <!-- cd-signin-modal__block -->
			<a href="#0" class="cd-signin-modal__close js-close">Close</a>
		</div> <!-- cd-signin-modal__container -->
	</div> 
        <!--popup area close-->
  <!--view booking popup-->
        <div class="modal fade ar_direction" id="myModal" role="dialog">
        <div class="modal-dialog  modal-smsp">
        
          <!-- Modal content-->
          <div class="modal-content ar_direction">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">{{headerSection.View_Bookings_Label}}</h4>
            </div>
            <div class="modal-body">
            <div class="user_login">
            <div>
            <p><label>{{headerSection.Email_Label}}</label></p>
            <div class="validation_sec">
            <input v-model="bEmail" type="text" id="txtretrivebooking" name="text" :placeholder="headerSection.Email_Placeholder" class=""> 
            <span v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveEmailErormsg }"  class="cd-signin-modal__error sp_validation">Email Required!</span>
            </div> 
            <p></p> 
            <p><label>{{headerSection.ID_Number_Label}}</label></p>
            <div class="validation_sec">
            <input v-model="bRef" type="text" id="text" name="text" :placeholder="headerSection.ID_Placeholder" class=""> 
            <span  v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveBkngRefErormsg }"   class="cd-signin-modal__error sp_validation">Booking Reference Id Required!</span>
            </div> 
            <p></p>
            <div class="viewradio_sec">
            <label class="radio_view">{{headerSection.Flight_Label}}
                <input v-model="bService" value="F" type="radio" checked="checked" name="radio">
                <span class="checkmark"></span>
            </label>
            <label class="radio_view" style="display:none">Hotel
                <input v-model="bService" value="H" type="radio" name="radio">
                <span class="checkmark"></span>
            </label>
            </div>
            <div class="btn-submit"><button v-on:click="retrieveBooking" type="submit" id="retrieve-booking" class="btn-blue">{{headerSection.Button_Label}}</button></div> 
            </div></div>
            </div>            
          </div>
          </div>
          <!--view booking popup close-->
    </div></div>`,
    data() {
        return {
            username: '',
            password: '',
            emailId: '',
            retrieveEmailId: '',
            retrieveBookRefid: '',
            usererrormsg: { empty: false, invalid: false },
            psserrormsg: false,
            userFirstNameErormsg: false,
            userLastNameErrormsg: false,
            userEmailErormsg: { empty: false, invalid: false },
            userPasswordErormsg: false,
            userVerPasswordErormsg: false,
            userPwdMisMatcherrormsg: false,
            userTerms: false,
            userforgotErrormsg: { empty: false, invalid: false },
            retrieveBkngRefErormsg: false,
            retrieveEmailErormsg: false,
            userlogined: this.checklogin(),
            userinfo: [],
            registerUserData: {
                firstName: '',
                lastName: '',
                emailId: '',
                password: '',
                verifyPassword: '',
                terms: '',
                title: 'Mr'
            },
            Languages: [],
            language: 'en',
            content: null,
            getdata: true,
            active_e: (sessionStorage.active_e) ? sessionStorage.active_e : 1,
            bEmail: '',
            bRef: '',
            bService: 'F',
            show: 'show',
            hide: 'hide',
            headerSection:{},
        }
    },
    methods: {
        loginaction: function () {

            if (!this.username.trim()) {
                this.usererrormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.username.trim())) {
                this.usererrormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.usererrormsg = { empty: false, invalid: false };
            }
            if (!this.password) {
                this.psserrormsg = true;
                return false;

            } else {
                this.psserrormsg = false;
                var self = this;
                login(this.username, this.password, function (response) {
                    if (response == false) {
                        self.userlogined = false;
                        alert("Invalid username or password.");
                    } else {
                        self.userlogined = true;
                        self.userinfo = JSON.parse(localStorage.User);
                        $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                        try {
                            packageDetailInfo.packageInfo.UserName = response.firstName + ' ' + response.lastName;
                            packageDetailInfo.packageInfo.UserEmail = response.emailId;
                            packageDetailInfo.packageInfo.UserPhone = response.contactNumber;
                            self.$eventHub.$emit('logged-in', { userName: self.username, password: self.password });
                            signArea.headerLogin({ userName: self.username, password: self.password })
                        } catch (error) {

                        }
                    }
                });

            }



        },
        validEmail: function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        checklogin: function () {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);
                return true;
            } else {
                this.userlogined = false;
                return false;
            }
        },
        logout: function () {
            this.userlogined = false;
            this.userinfo = [];
            localStorage.profileUpdated = false;
            try {
                packageDetailInfo.packageInfo.UserName = "";
                packageDetailInfo.packageInfo.UserEmail = "";
                packageDetailInfo.packageInfo.UserPhone = "";
                this.$eventHub.$emit('logged-out');
                signArea.logout()
            } catch (error) {

            }
            commonlogin();
            // signOut();
            // signOutFb();
        },
        registerUser: function () {
            if (this.registerUserData.firstName.trim() == "") {
                this.userFirstNameErormsg = true;
                return false;
            } else {
                this.userFirstNameErormsg = false;
            }
            if (this.registerUserData.lastName.trim() == "") {
                this.userLastNameErrormsg = true;
                return false;
            } else {
                this.userLastNameErrormsg = false;
            }
            if (this.registerUserData.emailId.trim() == "") {
                this.userEmailErormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.registerUserData.emailId.trim())) {
                this.userEmailErormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.userEmailErormsg = { empty: false, invalid: false };
            }
            var vm = this;
            registerUser(this.registerUserData.emailId, this.registerUserData.firstName, this.registerUserData.lastName, this.registerUserData.title, function (response) {
                if (response.isSuccess == true) {
                    vm.username = response.data.data.user.loginId;
                    vm.password = response.data.data.user.password;
                    login(vm.username, vm.password, function (response) {
                        if (response != false) {
                            $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                            window.location.href = "/edit-my-profile.html?edit-profile=true";
                        }
                    });
                }

            });
        },

        forgotPassword: function () {
            if (this.emailId.trim() == "") {
                this.userforgotErrormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.emailId.trim())) {
                this.userforgotErrormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.userforgotErrormsg = { empty: false, invalid: false };
            }

            var datas = {
                emailId: this.emailId,
                agencyCode: localStorage.AgencyCode,
                logoUrl: window.location.origin + "/" + localStorage.AgencyFolderName + "/website-informations/logo/logo.png",
                websiteUrl: window.location.origin,
                resetUri: window.location.origin + "/" + localStorage.AgencyFolderName + "/reset-password.html"
            };
            $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:none;background:grey !important;");

            var huburl = ServiceUrls.hubConnection.baseUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var requrl = ServiceUrls.hubConnection.hubServices.forgotPasswordUrl;
            axios.post(huburl + portno + requrl, datas)
                .then((response) => {
                    if (response.data != "") {
                        alert(response.data);
                    } else {
                        alert("Error in forgot password. Please contact admin.");
                    }
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:var(--primary-color) !important");

                })
                .catch((err) => {
                    console.log("FORGOT PASSWORD  ERROR: ", err);
                    if (err.response.data.message == 'No User is registered with this emailId.') {
                        alert(err.response.data.message);
                    } else {
                        alert('We have found some technical difficulties. Please contact admin!');
                    }
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:var(--primary-color) !important");

                })

        },

        retrieveBooking: function () {
            if (this.bEmail == "") {
                //alert('Email required !');
                this.retrieveEmailErormsg = true;
                return false;

            } else if (!this.validEmail(this.bEmail)) {
                //alert('Invalid Email !');
                this.retrieveEmailErormsg = true;
                return false;

            } else {
                this.retrieveEmailErormsg = false;
            }
            if (this.bRef == "") {
                this.retrieveBkngRefErormsg = true;
                return false;

            } else {
                this.retrieveBkngRefErormsg = false;
            }
            if (!this.retrieveBkngRefErormsg && !this.retrieveEmailErormsg && !this.retrieveEmailErormsg) {
                switch (this.bService) {
                    case 'F':
                        this.retBookFlight();
                        break;
                    case 'H':
                        var vm = this;
                        var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;

                        axios({
                            method: "get",
                            url: hubUrl + "/hotelBook/bookingbyref/" + vm.bRef + ":" + vm.bEmail,
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer ' + localStorage.access_token
                            }
                        }).then(response => {
                            window.sessionStorage.setItem('userAction', vm.bRef);
                            window.location.href = "/Hotels/hotel-detail.html#/hotelConfirmation";

                        }).catch(error => {
                            alertify.alert('Error!', 'Booking details not found!');
                        });
                        break;
                    default:
                        break;
                }
            }

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        activate: function (el) {
            sessionStorage.active_e = el;
            this.active_e = el;
        },
        headerData: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Master Page/Master Page/Master Page.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    if (response.data.area_List.length) {
                        var headerSection = pluck('Header_Section', self.content.area_List);
                        self.headerSection = getAllMapData(headerSection[0].component);
                           

                        // var Header = self.pluck('Header_Section', self.content.area_List);
                        // self.Header.Contact_Number = self.pluckcom('Contact_Number', Header[0].component);
                        // self.Header.logo_Image = self.pluckcom('Logo_Image', Header[0].component);
                        // self.Header.Contact_Email = self.pluckcom('Contact_Email', Header[0].component);
                        self.getdata = false;
                        self.swapStyle();
                        Vue.nextTick(function () {
                            (

                                function () {
                                    // self.active_el = (sessionStorage.active_el) ? sessionStorage.active_el : 0;
                                    //Login/Signup modal window - by CodyHouse.co
                                    function ModalSignin(element) {
                                        this.element = element;
                                        this.blocks = this.element.getElementsByClassName('js-signin-modal-block');
                                        this.switchers = this.element.getElementsByClassName('js-signin-modal-switcher')[0].getElementsByTagName('a');
                                        this.triggers = document.getElementsByClassName('js-signin-modal-trigger');
                                        this.hidePassword = this.element.getElementsByClassName('js-hide-password');
                                        this.init();
                                    };

                                    ModalSignin.prototype.init = function () {
                                        var self1 = this;
                                        //open modal/switch form
                                        for (var i = 0; i < this.triggers.length; i++) {
                                            (function (i) {
                                                self1.triggers[i].addEventListener('click', function (event) {
                                                    if (event.target.hasAttribute('data-signin')) {
                                                        event.preventDefault();
                                                        self1.showSigninForm(event.target.getAttribute('data-signin'));
                                                    }
                                                });
                                            })(i);
                                        }

                                        //close modal
                                        this.element.addEventListener('click', function (event) {
                                            if (hasClass(event.target, 'js-signin-modal') || hasClass(event.target, 'js-close')) {
                                                event.preventDefault();
                                                removeClass(self1.element, 'cd-signin-modal--is-visible');
                                            }
                                        });
                                        //close modal when clicking the esc keyboard button
                                        document.addEventListener('keydown', function (event) {
                                            (event.which == '27') && removeClass(self1.element, 'cd-signin-modal--is-visible');
                                        });

                                        // //hide/show password
                                        // for (var i = 0; i < this.hidePassword.length; i++) {
                                        //     (function(i) {
                                        //         self1.hidePassword[i].addEventListener('click', function(event) {
                                        //             self1.togglePassword(self1.hidePassword[i]);
                                        //         });
                                        //     })(i);
                                        // }

                                        //IMPORTANT - REMOVE THIS - it's just to show/hide error messages in the demo

                                    };

                                    // ModalSignin.prototype.togglePassword = function(target) {
                                    //     var password = target.previousElementSibling;
                                    //     ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text'): password.setAttribute('type', 'password');
                                    //     target.textContent = ('Hide' == target.textContent) ? 'Show' : 'Hide';
                                    //     putCursorAtEnd(password);
                                    // }

                                    ModalSignin.prototype.showSigninForm = function (type) {
                                        // show modal if not visible
                                        !hasClass(this.element, 'cd-signin-modal--is-visible') && addClass(this.element, 'cd-signin-modal--is-visible');
                                        // show selected form
                                        for (var i = 0; i < this.blocks.length; i++) {
                                            this.blocks[i].getAttribute('data-type') == type ? addClass(this.blocks[i], 'cd-signin-modal__block--is-selected') : removeClass(this.blocks[i], 'cd-signin-modal__block--is-selected');
                                        }
                                        //update switcher appearance
                                        var switcherType = (type == 'signup') ? 'signup' : 'login';
                                        for (var i = 0; i < this.switchers.length; i++) {
                                            this.switchers[i].getAttribute('data-type') == switcherType ? addClass(this.switchers[i], 'cd-selected') : removeClass(this.switchers[i], 'cd-selected');
                                        }
                                    };

                                    ModalSignin.prototype.toggleError = function (input, bool) {
                                        // used to show error messages in the form
                                        toggleClass(input, 'cd-signin-modal__input--has-error', bool);
                                        toggleClass(input.nextElementSibling, 'cd-signin-modal__error--is-visible', bool);
                                    }

                                    var signinModal = document.getElementsByClassName("js-signin-modal")[0];
                                    //  
                                    if (signinModal) {
                                        new ModalSignin(signinModal);
                                    }

                                    // toggle main navigation on mobile
                                    var mainNav = document.getElementsByClassName('js-main-nav')[0];
                                    if (mainNav) {
                                        mainNav.addEventListener('click', function (event) {
                                            if (hasClass(event.target, 'js-main-nav')) {
                                                var navList = mainNav.getElementsByTagName('ul')[0];
                                                toggleClass(navList, 'cd-main-nav__list--is-visible', !hasClass(navList, 'cd-main-nav__list--is-visible'));
                                            }
                                        });
                                    }

                                    //class manipulations - needed if classList is not supported
                                    function hasClass(el, className) {
                                        if (el.classList) return el.classList.contains(className);
                                        else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
                                    }

                                    function addClass(el, className) {
                                        var classList = className.split(' ');
                                        if (el.classList) el.classList.add(classList[0]);
                                        else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
                                        if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
                                    }

                                    function removeClass(el, className) {
                                        var classList = className.split(' ');
                                        if (el.classList) el.classList.remove(classList[0]);
                                        else if (hasClass(el, classList[0])) {
                                            var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
                                            el.className = el.className.replace(reg, ' ');
                                        }
                                        if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
                                    }

                                    function toggleClass(el, className, bool) {
                                        if (bool) addClass(el, className);
                                        else removeClass(el, className);
                                    }
                                    // $("#modal_retrieve").leanModal({
                                    //     top: 100,
                                    //     overlay: 0.6,
                                    //     closeButton: ".modal_close"
                                    // });
                                    //credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
                                    function putCursorAtEnd(el) {
                                        if (el.setSelectionRange) {
                                            var len = el.value.length * 2;
                                            el.focus();
                                            el.setSelectionRange(len, len);
                                        } else {
                                            el.value = el.value;
                                        }
                                    };
                                })();
                        }.bind(self));
                    }
                }).catch(function (error) {
                    console.log('Error', error);
                    self.menus = [];
                    self.header = [];

                });
            });
        },
        retrieveBooking: function () {
            switch (this.bService) {
                case 'F':
                    this.retBookFlight();
                    break;
                case 'H':
                    //for Kelvin
                    break;
                default:
                    break;
            }
        },
        retBookFlight: function () {
            if (this.bEmail == "") {
                alert('Email required !');
                return false;
            } else if (!this.validEmail(this.bEmail)) {
                alert('Invalid Email !');
                return false;
            } else {
                this.retrieveEmailErormsg = false;
            }
            if (this.bRef == "") {
                this.retrieveBkngRefErormsg = true;
                return false;
            } else {
                this.retrieveBkngRefErormsg = false;
            }
            var bookData = {
                BkngRefID: this.bRef,
                emailId: this.bEmail,
                redirectFrom: 'retrievebooking',
                isMailsend: false
            };
            localStorage.bookData = JSON.stringify(bookData);
            window.location.href = '/Flights/flight-confirmation.html';
        },
        showhidepassword: function (event) {
            var password = event.target.previousElementSibling;
            ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text') : password.setAttribute('type', 'password');
            event.target.textContent = (this.headerSection.Hide_Label == event.target.textContent) ? this.headerSection.Show_Label : this.headerSection.Hide_Label;
        },
        swapStyle:function(){
            if (localStorage.direction == 'rtl') {
                if (document.getElementById("styleid")) {
                    document.getElementById("styleid").setAttribute("href", 'css/style-rtl.css');
                }       
                if (document.getElementById("mediaid")) {
                    document.getElementById("mediaid").setAttribute("href", 'css/media-rtl.css');
                }      
                if (document.getElementById("bootstrapid")) {
                    document.getElementById("bootstrapid").setAttribute("href", 'css/bootstrap.rtl.min.css');
                }      
                loadExternalFile('masterCss3', 'link', 'stylesheet', 'text/css', '/Spartan/css/bootstrap.rtl.min.css');
                loadExternalFile('masterCss1', 'link', 'stylesheet', 'text/css', '/Spartan/css/style-rtl.css');
                loadExternalFile('masterCss2', 'link', 'stylesheet', 'text/css', '/Spartan/css/media-rtl.css');
                $(".ar_direction").addClass("ar_direction1");
                this.getValidationMsgs();
                alertify.defaults.glossary.ok = 'حسنا';
            } else {
                if (document.getElementById("styleid")) {
                    document.getElementById("styleid").setAttribute("href", 'css/style-ltr.css');
                }
                if (document.getElementById("mediaid")) {
                    document.getElementById("mediaid").setAttribute("href", 'css/media-ltr.css');
                }
                if (document.getElementById("bootstrapid")) {
                    document.getElementById("bootstrapid").removeAttribute("href",'css/bootstrap.rtl.min.css');
                }
                if (document.getElementById("masterCss3")) {
                    document.getElementById("masterCss3").removeAttribute("href",'/Spartan/css/bootstrap.rtl.min.css');
                }
                if (document.getElementById("masterCss1")) {
                    document.getElementById("masterCss1").removeAttribute("href",'/Spartan/css/style-rtl.css');
                }
                if (document.getElementById("masterCss2")) {
                    document.getElementById("masterCss2").removeAttribute("href",'/Spartan/css/media-rtl.css');
                }
               $(".ar_direction").removeClass("ar_direction1");
                this.getValidationMsgs();
                alertify.defaults.glossary.ok = 'OK';
            }
        },
        getValidationMsgs: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Validation/Validation/Validation.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    if (response.data.area_List.length) {
                        var validations = pluck('Main_Area', response.data.area_List);
                        self.validationList = getAllMapData(validations[0].component);
                        sessionStorage.setItem('validationItems', JSON.stringify(self.validationList));
                    }
                }).catch(function (error) {
                    console.log('Error');
                });
            });
        },
    },
    mounted: function () {

        document.onreadystatechange = () => {
            // if (document.readyState == "complete") {
            //     this.swapStyle();
            // }
        }

        if (localStorage.IsLogin == "true") {
            this.userlogined = true;
            this.userinfo = JSON.parse(localStorage.User);

        } else {
            this.userlogined = false;

        }
        if(!sessionStorage.validationItems){
            this.getValidationMsgs();
        }
        this.headerData();

    },
    created: function () {

    },
    watch: {
        updatelogin: function () {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);

            } else {
                this.userlogined = false;

            }

        },
        bRef: function () {
            this.retrieveBkngRefErormsg = false;
        }

    }
});
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});

Vue.prototype.$eventHub = new Vue({
    data: {}
});
var FooterComponent = Vue.component('footeritem', {
    template: `<div class="container ar_direction" v-cloak>
    <div class="row contact-footer">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 red-brdr ar_direction">
        <i aria-hidden="true" class="fa fa-phone"></i>
        <h5>{{footerSection.Phone_Label}}</h5>
        <p><a :href="'tel:'+footerSection.Phone_Number">{{footerSection.Phone_Number}}</a></p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 red-brdr">
        <i aria-hidden="true" class="fa fa-map-marker"></i>
        <h5>{{footerSection.Address_Label}}</h5>
        <p>{{footerSection.Address}}</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <i aria-hidden="true" class="fa fa-envelope"></i>
        <h5>{{footerSection.Email_Label}}</h5>
        <p><a :href="'mailto:'+footerSection.Email">{{footerSection.Email}}</a></p>
        </div>
    </div>
    <div class="row sec_footer">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 logo">
            <img :src="footerSection.Logo_210_x_210px" alt="logo">
            <p>{{footerSection.Logo_Description}}</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 footer_link">
            <h6><span>{{footerSection.Information_Label}}</span></h6>
            <ul>
                <li><a href="/Spartan/index.html">{{headerSection.Home_Label}}</a></li>
                <!--<li><a href="/Spartan/flights.html">{{headerSection.Flights_Label}}</a></li>-->
                <!--<li><a href="/Spartan/hotel.html">{{headerSection.Hotels_Label}}</a></li>-->
                <li><a href="/Spartan/hotel-overview.html">{{headerSection.Hotel_Overview}}</a></li>
                <li><a href="/Spartan/plan-my-trip.html">{{headerSection.Plan_My_Trip_Label}}</a></li>
                <li><a href="/Spartan/camp-overview.html">{{headerSection.Self_Camping_Label}}</a></li>
                <!--<li><a href="/Spartan/tent-accommodation.html">{{headerSection.Tent_Accommodation_Label}}</a></li>
                <li><a href="/Spartan/transfers.html">{{headerSection.Transfers_Label}}</a></li>
                <li><a href="/Spartan/car-rental.html">{{headerSection.Car_Rental_Label}}</a></li>
                <li><a href="/Spartan/tours.html">{{headerSection.Tour_Label}}</a></li>
                <li><a href="/Spartan/visa.html">{{headerSection.Visa_Label}}</a></li>-->
            </ul>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 newsletter">
            <div class="f_logo">
                <!--<a class="first-logo" href=""><img :src="footerSection.Sports_Council_Logo"></a>-->
                <a class="sec-logo" href=""><img :src="footerSection.Nirvana_Icon_183_x_64px"></a>
            </div>
        </div>
    </div>
    <div class="row main_copyright">      
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
        <div class="copyright">{{footerSection.Copyright_Description}}<a :href="footerSection.Terms_Of_Use_Link" target="_blank">{{footerSection.Terms_Of_Use_Label}}</a> {{footerSection.And_Label}} <a :href="footerSection.Privacy_Policy_Link" target="_blank">{{footerSection.Privacy_Policy_Label}}</a>.</div>        
        </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <div class="powered-by">{{footerSection.Powered_By_Label}}: &nbsp; <a href="http://www.oneviewit.com/" target="_blank"><img alt="" :src="footerSection.Oneview_Logo_92_x_33px"></a></div>
      </div>
    </div>
  </div></div>`,
    data() {
        return {
            content: null,
            getdata: true,
            newsltrname: '',
            newsltremail: '',
            cntusername: '',
            cntemail: '',
            cntcontact: '',
            cntsubject: '',
            cntmessage: '',
            currentLocationDetails: {},
            textDirection: localStorage.direction,
            key: 0,
            allDepartmentList: [],
            selectedDepartment: {},
            // Footer: { Phone_Number: '' },


            mainSection:{},
            headerSection:{},
            footerSection:{}

        }

    },
    props: {
        newKey: String
    },
    methods: {
        selectLocation: function (brnch) {
            this.currentLocationDetails = brnch;
        },
        getpagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Master Page/Master Page/Master Page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var headerSection = pluck('Header_Section', response.data.area_List);
                    self.headerSection = getAllMapData(headerSection[0].component);
                    

                    var FooterContent = pluck('Footer_Section', response.data.area_List);
                    self.footerSection = getAllMapData(FooterContent[0].component);
                    // self.swapStyle();


                    // var Footer = self.pluck('Footer_Section', self.content.area_List);
                    // self.Footer.Phone_Number = self.pluckcom('Phone_Number', Footer[0].component);
                    // self.Footer.Address = self.pluckcom('Address', Footer[0].component);
                    // self.Footer.Email = self.pluckcom('Email', Footer[0].component);
                    // self.Footer.Copyright_Content = self.pluckcom('Copyright_Content', Footer[0].component);
                    // self.Footer.Terms_Of_Use = self.pluckcom('Terms_Of_Use', Footer[0].component);
                    // self.Footer.Privacy_Policy = self.pluckcom('Privacy_Policy', Footer[0].component);
                    // self.Footer.Logo_1 = self.pluckcom('Logo_1', Footer[0].component);
                    // self.Footer.Logo_2 = self.pluckcom('Logo_2', Footer[0].component);
                    // self.Footer.Logo_3 = self.pluckcom('Logo_3', Footer[0].component);
                }).catch(function (error) {
                    console.log('Error', error);
                    this.content = [];
                });

            });

        },
        // swapStyle:function(){
        //     if (localStorage.direction == 'rtl') {
        //         $(".ar_direction").addClass("ar_direction1");
        //         // document.getElementById("styleid").setAttribute("href", 'css/style-rtl.css');
        //         // document.getElementById("mediaid").setAttribute("href", 'css/media-rtl.css');
        //         // document.getElementById("bootstrapid").setAttribute("href", 'css/bootstrap.rtl.min.css');
            
        //         loadExternalFile('masterCss1', 'link', 'stylesheet', 'text/css', '/Spartan/css/style-rtl.css');
        //         loadExternalFile('masterCss3', 'link', 'stylesheet', 'text/css', '/Spartan/css/bootstrap.rtl.min.css');
        //         loadExternalFile('masterCss2', 'link', 'stylesheet', 'text/css', '/Spartan/css/media-rtl.css');
                
        //         alertify.defaults.glossary.ok = 'حسنا';
        //     } else {
        //         $(".ar_direction").removeClass("ar_direction1");
        //         document.getElementById("styleid").setAttribute("href", 'css/style-ltr.css');
        //         document.getElementById("mediaid").setAttribute("href", 'css/media-ltr.css');
        //         document.getElementById("bootstrapid").removeAttribute("href",'css/bootstrap.rtl.min.css');
        //         alertify.defaults.glossary.ok = 'OK';
        //     }
        // },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);

                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
    },
    mounted: function () {
        document.onreadystatechange = () => {
            // if (document.readyState == "complete") {
            //     this.swapStyle();
            // }
        }
        this.getpagecontent();
    },
    created: function () {
    }
});

var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});

(function ($, window, undefined) {

    'use strict';

    // global
    var Modernizr = window.Modernizr,
        $body = $('body');

    $.DLMenu = function (options, element) {
        this.$el = $(element);
        this._init(options);
    };

    // the options
    $.DLMenu.defaults = {
        // classes for the animation effects
        animationClasses: { classin: 'dl-animate-in-1', classout: 'dl-animate-out-1' },
        // callback: click a link that has a sub menu
        // el is the link element (li); name is the level name
        onLevelClick: function (el, name) { return false; },
        // callback: click a link that does not have a sub menu
        // el is the link element (li); ev is the event obj
        onLinkClick: function (el, ev) { return false; }
    };

    $.DLMenu.prototype = {
        _init: function (options) {

            // options
            this.options = $.extend(true, {}, $.DLMenu.defaults, options);
            // cache some elements and initialize some variables
            this._config();

            var animEndEventNames = {
                'WebkitAnimation': 'webkitAnimationEnd',
                'OAnimation': 'oAnimationEnd',
                'msAnimation': 'MSAnimationEnd',
                'animation': 'animationend'
            },
                transEndEventNames = {
                    'WebkitTransition': 'webkitTransitionEnd',
                    'MozTransition': 'transitionend',
                    'OTransition': 'oTransitionEnd',
                    'msTransition': 'MSTransitionEnd',
                    'transition': 'transitionend'
                };
            // animation end event name
            this.animEndEventName = animEndEventNames[Modernizr.prefixed('animation')] + '.dlmenu';
            // transition end event name
            this.transEndEventName = transEndEventNames[Modernizr.prefixed('transition')] + '.dlmenu',
                // support for css animations and css transitions
                this.supportAnimations = Modernizr.cssanimations,
                this.supportTransitions = Modernizr.csstransitions;

            this._initEvents();

        },
        _config: function () {
            this.open = false;
            this.$trigger = this.$el.children('.dl-trigger');
            this.$menu = this.$el.children('ul.dl-menu');
            this.$menuitems = this.$menu.find('li:not(.dl-back)');
            this.$el.find('ul.dl-submenu').prepend('<li class="dl-back"><a href="#">back</a></li>');
            this.$back = this.$menu.find('li.dl-back');
            $(".dl-trigger").unbind('click');
        },
        _initEvents: function () {

            var self = this;

            this.$trigger.on('click.dlmenu', function () {

                if (self.open) {
                    self._closeMenu();
                } else {
                    self._openMenu();
                }
                return false;

            });

            this.$menuitems.on('click.dlmenu', function (event) {

                event.stopPropagation();

                var $item = $(this),
                    $submenu = $item.children('ul.dl-submenu');

                if ($submenu.length > 0) {

                    var $flyin = $submenu.clone().css('opacity', 0).insertAfter(self.$menu),
                        onAnimationEndFn = function () {
                            self.$menu.off(self.animEndEventName).removeClass(self.options.animationClasses.classout).addClass('dl-subview');
                            $item.addClass('dl-subviewopen').parents('.dl-subviewopen:first').removeClass('dl-subviewopen').addClass('dl-subview');
                            $flyin.remove();
                        };

                    setTimeout(function () {
                        $flyin.addClass(self.options.animationClasses.classin);
                        self.$menu.addClass(self.options.animationClasses.classout);
                        if (self.supportAnimations) {
                            self.$menu.on(self.animEndEventName, onAnimationEndFn);
                        } else {
                            onAnimationEndFn.call();
                        }

                        self.options.onLevelClick($item, $item.children('a:first').text());
                    });

                    return false;

                } else {
                    self.options.onLinkClick($item, event);
                }

            });

            this.$back.on('click.dlmenu', function (event) {

                var $this = $(this),
                    $submenu = $this.parents('ul.dl-submenu:first'),
                    $item = $submenu.parent(),

                    $flyin = $submenu.clone().insertAfter(self.$menu);

                var onAnimationEndFn = function () {
                    self.$menu.off(self.animEndEventName).removeClass(self.options.animationClasses.classin);
                    $flyin.remove();
                };

                setTimeout(function () {
                    $flyin.addClass(self.options.animationClasses.classout);
                    self.$menu.addClass(self.options.animationClasses.classin);
                    if (self.supportAnimations) {
                        self.$menu.on(self.animEndEventName, onAnimationEndFn);
                    } else {
                        onAnimationEndFn.call();
                    }

                    $item.removeClass('dl-subviewopen');

                    var $subview = $this.parents('.dl-subview:first');
                    if ($subview.is('li')) {
                        $subview.addClass('dl-subviewopen');
                    }
                    $subview.removeClass('dl-subview');
                });

                return false;

            });

        },
        closeMenu: function () {
            if (this.open) {
                this._closeMenu();
            }
        },
        _closeMenu: function () {
            var self = this,
                onTransitionEndFn = function () {
                    self.$menu.off(self.transEndEventName);
                    self._resetMenu();
                };

            this.$menu.removeClass('dl-menuopen');
            this.$menu.addClass('dl-menu-toggle');
            this.$trigger.removeClass('dl-active');

            if (this.supportTransitions) {
                this.$menu.on(this.transEndEventName, onTransitionEndFn);
            } else {
                onTransitionEndFn.call();
            }

            this.open = false;
        },
        openMenu: function () {
            if (!this.open) {
                this._openMenu();
            }
        },
        _openMenu: function () {
            var self = this;
            // clicking somewhere else makes the menu close
            $body.off('click').on('click.dlmenu', function () {
                self._closeMenu();
            });
            this.$menu.addClass('dl-menuopen dl-menu-toggle').on(this.transEndEventName, function () {
                $(this).removeClass('dl-menu-toggle');
            });
            this.$trigger.addClass('dl-active');
            this.open = true;
        },
        // resets the menu to its original state (first level of options)
        _resetMenu: function () {
            this.$menu.removeClass('dl-subview');
            this.$menuitems.removeClass('dl-subview dl-subviewopen');
        }
    };

    var logError = function (message) {
        if (window.console) {
            window.console.error(message);
        }
    };

    $.fn.dlmenu = function (options) {
        if (typeof options === 'string') {
            var args = Array.prototype.slice.call(arguments, 1);
            this.each(function () {
                var instance = $.data(this, 'dlmenu');
                if (!instance) {
                    logError("cannot call methods on dlmenu prior to initialization; " +
                        "attempted to call method '" + options + "'");
                    return;
                }
                if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
                    logError("no such method '" + options + "' for dlmenu instance");
                    return;
                }
                instance[options].apply(instance, args);
            });
        } else {
            this.each(function () {
                var instance = $.data(this, 'dlmenu');
                if (instance) {
                    instance._init();
                } else {
                    instance = $.data(this, 'dlmenu', new $.DLMenu(options, this));
                }
            });
        }
        return this;
    };

})(jQuery, window);







// $(document).ready(function(){
function setpoup() {
    //Login/Signup modal window - by CodyHouse.co
    function ModalSignin(element) {
        this.element = element;
        this.blocks = this.element.getElementsByClassName('js-signin-modal-block');
        this.switchers = this.element.getElementsByClassName('js-signin-modal-switcher')[0].getElementsByTagName('a');
        this.triggers = document.getElementsByClassName('js-signin-modal-trigger');
        this.hidePassword = this.element.getElementsByClassName('js-hide-password');
        this.init();
    };

    ModalSignin.prototype.init = function () {
        var self = this;
        //open modal/switch form
        for (var i = 0; i < this.triggers.length; i++) {
            (function (i) {
                self.triggers[i].addEventListener('click', function (event) {
                    if (event.target.hasAttribute('data-signin')) {
                        event.preventDefault();
                        self.showSigninForm(event.target.getAttribute('data-signin'));
                    }
                });
            })(i);
        }

        //close modal
        this.element.addEventListener('click', function (event) {
            if (hasClass(event.target, 'js-signin-modal') || hasClass(event.target, 'js-close')) {
                event.preventDefault();
                removeClass(self.element, 'cd-signin-modal--is-visible');
            }
        });
        //close modal when clicking the esc keyboard button
        document.addEventListener('keydown', function (event) {
            (event.which == '27') && removeClass(self.element, 'cd-signin-modal--is-visible');
        });

        //hide/show password
        for (var i = 0; i < this.hidePassword.length; i++) {
            (function (i) {
                self.hidePassword[i].addEventListener('click', function (event) {
                    self.togglePassword(self.hidePassword[i]);
                });
            })(i);
        }

        //IMPORTANT - REMOVE THIS - it's just to show/hide error messages in the demo
        this.blocks[0].getElementsByTagName('form')[0].addEventListener('submit', function (event) {
            event.preventDefault();
            self.toggleError(document.getElementById('signin-email'), true);
        });
        this.blocks[1].getElementsByTagName('form')[0].addEventListener('submit', function (event) {
            event.preventDefault();
            self.toggleError(document.getElementById('signup-username'), true);
        });
    };

    ModalSignin.prototype.togglePassword = function (target) {
        var password = target.previousElementSibling;
        ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text') : password.setAttribute('type', 'password');
        target.textContent = ('Hide' == target.textContent) ? 'Show' : 'Hide';
        putCursorAtEnd(password);
    }

    ModalSignin.prototype.showSigninForm = function (type) {
        // show modal if not visible
        !hasClass(this.element, 'cd-signin-modal--is-visible') && addClass(this.element, 'cd-signin-modal--is-visible');
        // show selected form
        for (var i = 0; i < this.blocks.length; i++) {
            this.blocks[i].getAttribute('data-type') == type ? addClass(this.blocks[i], 'cd-signin-modal__block--is-selected') : removeClass(this.blocks[i], 'cd-signin-modal__block--is-selected');
        }
        //update switcher appearance
        var switcherType = (type == 'signup') ? 'signup' : 'login';
        for (var i = 0; i < this.switchers.length; i++) {
            this.switchers[i].getAttribute('data-type') == switcherType ? addClass(this.switchers[i], 'cd-selected') : removeClass(this.switchers[i], 'cd-selected');
        }
    };

    ModalSignin.prototype.toggleError = function (input, bool) {
        // used to show error messages in the form
        toggleClass(input, 'cd-signin-modal__input--has-error', bool);
        toggleClass(input.nextElementSibling, 'cd-signin-modal__error--is-visible', bool);
    }

    var signinModal = document.getElementsByClassName("js-signin-modal")[0];
    if (signinModal) {
        new ModalSignin(signinModal);
    }

    // toggle main navigation on mobile
    var mainNav = document.getElementsByClassName('js-main-nav')[0];
    if (mainNav) {
        mainNav.addEventListener('click', function (event) {
            if (hasClass(event.target, 'js-main-nav')) {
                var navList = mainNav.getElementsByTagName('ul')[0];
                toggleClass(navList, 'cd-main-nav__list--is-visible', !hasClass(navList, 'cd-main-nav__list--is-visible'));
            }
        });
    }

    //class manipulations - needed if classList is not supported
    function hasClass(el, className) {
        if (el.classList) return el.classList.contains(className);
        else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
    }

    function addClass(el, className) {
        var classList = className.split(' ');
        if (el.classList) el.classList.add(classList[0]);
        else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
        if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
    }

    function removeClass(el, className) {
        var classList = className.split(' ');
        if (el.classList) el.classList.remove(classList[0]);
        else if (hasClass(el, classList[0])) {
            var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
            el.className = el.className.replace(reg, ' ');
        }
        if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
    }

    function toggleClass(el, className, bool) {
        if (bool) addClass(el, className);
        else removeClass(el, className);
    }

    //credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
    function putCursorAtEnd(el) {
        if (el.setSelectionRange) {
            var len = el.value.length * 2;
            el.focus();
            el.setSelectionRange(len, len);
        } else {
            el.value = el.value;
        }
    };

}

function jqurufunctions() {
    $('.nav ul li a[href^="#"]').on('click', function (event) {
        var target = $(this.getAttribute('href'));
        if (target.length) {
            event.preventDefault();

            $('html, body').stop().animate({
                scrollTop: target.offset().top - 0
            }, 1200);
        }
    });
    $("#advanceSearch").unbind('click');
    $('#advanceSearch').click(function () {

        $('.ad_view').toggle();
    });
    $('#dl-menu').dlmenu({
        animationClasses: { classin: 'dl-animate-in-3', classout: 'dl-animate-out-3' }
    });
}
$(function () {
    var selectedClass = "";
    $(".filter").click(function () {
        selectedClass = $(this).attr("data-rel");
        $("#gallery").fadeTo(100, 0.1);
        $("#gallery div").not("." + selectedClass).fadeOut().removeClass('animation');
        setTimeout(function () {
            $("." + selectedClass).fadeIn().addClass('animation');
            $("#gallery").fadeTo(300, 1);
        }, 300);
    });
});
// });

function searchArray(nameKey, myArray, tagName) {
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i][tagName] === nameKey) {
            return myArray[i];
        }
    }
}

//Google
function googleInit() {
    console.log('google inited');
    gapi.load('auth2', () => {
        gapi.auth2.init({ client_id: '509151388330-lsap2aj7ace202lmav6l16eflekel2ih.apps.googleusercontent.com' }).then(() => {
            // DO NOT ATTEMPT TO RENDER BUTTON UNTIL THE 'Init' PROMISE RETURNS
            renderButton();
            myGoogleButtonReg();
        });
    })
}

function renderButton() {
    gapi.signin2.render('myGoogleButton', {
        'scope': 'profile email',
        'width': 240,
        'height': 40,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });
}

function myGoogleButtonReg() {
    gapi.signin2.render('myGoogleButtonReg', {
        'scope': 'profile email',
        'width': 240,
        'height': 40,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });
}

function changeShowTxt() {
    var langauagee = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
    if (langauagee == "ar") {
        if ($('.changeShowTxtCls').text() == 'Show') {
            $('.changeShowTxtCls').text('تبين');
        }
    } else {
        if ($('.changeShowTxtCls').text() == 'تبين') {
            $('.changeShowTxtCls').text('Show');
        }
    }
}
$(document).ready(function () {
    setTimeout(function () {
        $(".page_loader").hide();
    }, 1000);
});