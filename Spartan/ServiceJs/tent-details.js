var currentUser = getUser();
const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var packageView = new Vue({
    i18n,
    el: '#tentdetails',
    name: 'tentdetails',
    data: {
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        isLoading: false,
        grandTotal:'',
        maincontent:{},
        BookingDetailsSection:{},
        RoomDetailsSection:{},
        selectedTents:[],
        tentUsers:[],


        maincontent: {
            Hotel_Name: '',
        },
        details: {
            Informations: '',
            Images: [{
                Image: ''
            }],
            Amenities: '',
        },
        Bookingdetails: {
            Instructions: '',
        },
        Roomdetails: {
            Instructions: '',
        },
        pageURLLink: '',

        email: '',
        phone: '',
        labelsSection:{},
        bannerSection:{},
        city: '',
        checkIn: '',
        checkOut: '',
        noOfNights: 0,
        norooms: 0,
        withPark: false,
        rooms: [],
        SelectedRooms: [{
            Price: ''
        }],
        occupancyArr: [],
        occupancyTotal: 1,
        tentType: "T1",
        tentImages: [],
        // roomTypePax: [],
        paxDetails: [],
        typeList: [{
                typeId: 1,
                name: 'SingleRoom',
                adult: 1,
                child: 0,
                pax: [{
                    paxId: 1,
                    title: 'Mr',
                    name: '',
                    surName: '',
                    type: 'Adult',
                    requested: true
                }]
            },
            {
                typeId: 2,
                name: 'DoubleRoom',
                adult: 2,
                child: 0,
                pax: [{
                        paxId: 1,
                        title: 'Mr',
                        name: '',
                        surName: '',
                        type: 'Adult',
                        requested: true
                    },
                    {
                        paxId: 2,
                        title: 'Mr',
                        name: '',
                        surName: '',
                        type: 'Adult',
                        requested: true
                    }
                ]
            },
            {
                typeId: 3,
                name: 'DoublePlusChild',
                adult: 2,
                child: 1,
                pax: [{
                        paxId: 1,
                        title: 'Mr',
                        name: '',
                        surName: '',
                        type: 'Adult',
                        requested: true
                    },
                    {
                        paxId: 2,
                        title: 'Mr',
                        name: '',
                        surName: '',
                        type: 'Adult',
                        requested: true
                    },
                    {
                        paxId: 3,
                        title: 'Mstr',
                        name: '',
                        surName: '',
                        type: 'Child',
                        requested: true
                    }
                ]
            },
            {
                typeId: 4,
                name: 'DoublePlusAdult',
                adult: 3,
                child: 0,
                pax: [{
                        paxId: 1,
                        title: 'Mr',
                        name: '',
                        surName: '',
                        type: 'Adult',
                        requested: true
                    },
                    {
                        paxId: 2,
                        title: 'Mr',
                        name: '',
                        surName: '',
                        type: 'Adult',
                        requested: true
                    },
                    {
                        paxId: 3,
                        title: 'Mr',
                        name: '',
                        surName: '',
                        type: 'Adult',
                        requested: true
                    }
                ]
            }
        ]
    },
    mounted() {
        // localStorage.removeItem("backUrl");
        sessionStorage.active_e = 2;
        var self = this;
        // this.setCalender();
        this.getPackagedetails();
        self.getLabels();
        // var urlParameters = this.getSearchData();
        // var obj = JSON.stringify(this.typeList.find((x) => x.typeId == Number(urlParameters.occupancy)))
        // this.roomTypePax = JSON.parse(obj).pax;
        // this.maxAdults = JSON.parse(obj).adult;
        // this.maxChild = JSON.parse(obj).child;

        this.$nextTick(function () {
            setTimeout(() => {
                $("#phone").intlTelInput({
                    initialCountry: currentUser.loginNode.country.code || "AE",
                    geoIpLookup: function (callback) {
                        $.get('https://ipinfo.io', function () {}, "jsonp").always(function (resp) {
                            var countryCode = (resp && resp.country) ? resp.country : "";
                            (countryCode);
                        });
                    },
                    separateDialCode: true,
                    autoPlaceholder: "off",
                    preferredCountries: ['ae', 'sa', 'om', 'qa'],
                    utilsScript: "../assets/js/intlTelInput.js?v=201903150000" // just for formatting/placeholders etc
                });
            }, 500);
        });

        // var vm = this;
        // this.$nextTick(function () {
        //     $('#roomno').on("change", function (e) {
        //         vm.dropdownChange(e, 'roomno')
        //         setTimeout(() => {
        //             $(".myselect").select2({
        //                 minimumResultsForSearch: Infinity
        //             });
        //         }, );
        //         var roomNo = Number(e.target.value);
        //         setTimeout(() => {
        //             for (let index = 0; index < roomNo; index++) {
        //                 $('#Adult-' + index).val('1');
        //                 $('#Adult-' + index).trigger('change');
        //             }
        //         }, );

        //     });
        //     $('#from2').on("change", function (e) {
        //         self.noOfnightsFind();
        //     });
        //     $(document.body).on("change", ".myselect", function (e) {
        //         var tags = e.target.id.split('-');
        //         console.log(tags);
        //         if (tags[0] == "Adult") {
        //             self.setAdults(tags[1], Number(e.target.value));
        //         } else if (tags[0] == "Child") {
        //             self.setChild(tags[1], Number(e.target.value));
        //         }
        //     });
        // })

    },
    computed: {
        total: function () {
            var self = this;
            return _.reduce(self.SelectedRooms, function (sum, n) {
                return sum + (Number(n.Price) * Number(self.noOfNights));
            }, 0);
        }
    },
    methods: {
        // noOfnightsFind: function () {
        //     let date1 = document.getElementById("from1").value;
        //     let date2 = document.getElementById("from2").value;
        //     if (date1 && date2) {
        //         var start = moment(date1, "DD/MM/YYYY");
        //         var end = moment(date2, "DD/MM/YYYY");
        //         var nights = end.diff(start, 'days')
        //         this.noOfNights = nights;
        //     }
        // },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        dateFormat: function (d) {
            return moment(d).format('DD-MMM-YYYY');
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPackagedetails: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                try {
                    var tenturl = getQueryStringValue('page');
                    var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                    if (tenturl != "") {
                        tenturl = tenturl.split('-').join(' ');
                        tenturl = tenturl.split('_')[0];
                        var totenturl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + tenturl + '.ftl';
                        self.pageURLLink = tenturl;
                        axios.get(totenturl, {
                            headers: {
                                'content-type': 'text/html',
                                'Accept': 'text/html',
                                'Accept-Language': langauage
                            }
                        }).then(function (response) {
                            self.city = getQueryStringValue('city');
                            self.checkIn = getQueryStringValue('cin');
                            self.checkOut = getQueryStringValue('cout');
                            self.norooms = getQueryStringValue('rooms');
                            self.noOfNights = getQueryStringValue('nights');
                            self.rooms = JSON.parse(getQueryStringValue('roomTypes'));
                            self.grandTotal = getQueryStringValue('grandTotal');
                            self.selectedTents = JSON.parse(getQueryStringValue('selectedTents'));

                            var main = self.pluck('Tent_Details', response.data.area_List);
                            if (main.length > 0) {
                                self.maincontent = self.getAllMapData(main[0].component);
                            }

                            var booking = self.pluck('Booking_Details_Section', response.data.area_List);
                            if (booking.length > 0) {
                                self.BookingDetailsSection = self.getAllMapData(booking[0].component);
                            }
                            var roombooking = self.pluck('Room_Details_Section', response.data.area_List);
                            if (roombooking.length > 0) {
                                self.RoomDetailsSection = self.getAllMapData(roombooking[0].component);
                            }
                            // self.generatePersonContact(self.rooms)
                            self.generatePersonContact(self.selectedTents);
                            
                            self.occupancyArr = self.selectedTents.map(function(e) {return e.Occupancy;});
                            self.occupancyTotal = self.occupancyArr.reduce(function(a,c) {
                                var b = 0;
                                if (c == "Single") {
                                    b = b + 1;
                                } else if (c == "Double") {
                                    b = b + 2;
                                } else if (c == "Triple") {
                                    b = b + 3;
                                }
                                return a+b;
                            }, 0);
                            self.tentType = self.selectedTents.map(function(e) {return e.tId;});
                            self.tentImages = self.maincontent.Images_List.filter(function(e){return self.tentType.indexOf(e.Name) != -1 || self.occupancyArr.indexOf(e.Name) != -1 });
                            setTimeout(() => {
                                $('.fotorama').fotorama();
                            }, 100);

                        })
                    }
                } catch (error) {
                    console.log(error);
                }
            });
        },
        generatePersonContact:function(tents){
            if(tents.length>0){
                tents.forEach(tent => {
                    let tentPersons =[];
                    for(let i = 0 ;i<Number(tent.Occupancy_ID);i++){
                        let p ={
                            name:"",
                            paxId:'',
                            requested:true,
                            surName:"",
                            title:"Mr",
                            type:"",

                        }
                        tentPersons.push(p);
                    }
                    this.tentUsers.push(tentPersons);
                   
                });
                console.log('tentUsers',this.tentUsers)
            }

        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                if (type == 'roomno') {
                    var val = Number(event.target.value);
                    this.addRomms(this.norooms, val);
                    this.norooms = val;
                }
            }
        },
        checkReservation: function () {
            // var self = this;
            // let dateObj1 = document.getElementById("from1");
            // let dateValue1 = dateObj1.value;
            // let dateObj2 = document.getElementById("from2");
            // let dateValue2 = dateObj2.value;
            // let room = document.getElementById("roomno");
            // let roomValue = room.value;
            // // let yourPrice = roomValue*
            // if (dateValue1 == undefined || dateValue1 == '') {
            //     alertify.alert('Alert', 'Select Check In date').set('closable', false);
            //     return false;
            // }
            // if (dateValue2 == undefined || dateValue2 == '') {
            //     alertify.alert('Alert', 'Select Check Out Date').set('closable', false);
            //     return false;
            // }
            // if (roomValue == undefined || roomValue == '') {
            //     alertify.alert('Alert', 'Please Select Rooms.').set('closable', false);
            //     return false;
            // }
            // if (dateValue1 == dateValue2) {
            //     alertify.alert('Alert', 'Check-In date and Check-Out date are same.').set('closable', false);
            //     return false;
            // } else {
            this.$nextTick(function () {
                $('html, body').animate({
                    scrollTop: $("#htlbooking").offset().top
                }, 1000);
            })
            // }
        },
        bookTent: function () {
            var self = this;
            // let dateObj1 = document.getElementById("from1");
            let dateValue1 = self.checkIn;
            // let dateObj2 = document.getElementById("from2");
            let dateValue2 = self.checkOut;
            // let room = document.getElementById("roomno");
            let roomValue = self.norooms;

            var flagPaxName = false
            self.tentUsers.forEach(tent => {
                tent.forEach(pax => {
                    if (pax.requested && (pax.name == '' || pax.surName == '')) {
                        flagPaxName = true;
                        return false;
                    }
                });
                if (flagPaxName) {
                    return false;
                }
            });
            if (flagPaxName) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M008')).set('closable', false);
                return false;
            }
            if (!self.email) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M009')).set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = self.email.match(emailPat);
            if (matchArray == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M019')).set('closable', false);
                return false;
            }
            if (!self.phone || self.phone == '') {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M010')).set('closable', false);
                return false;
            }
            if (self.phone.length < 8) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M046')).set('closable', false);
                return false;
            } else {
                self.isLoading = true;
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                var guestNames = '';
                var guestCount = 0;
                self.tentUsers.forEach((tent, index) => {
                    var roomSelect = self.selectedTents[index].Room_Type
                    guestNames += ('\n \n' + 'Tent - ' + (index + 1) + ' - ' + roomSelect);
                    tent.forEach(pax => {
                        if (pax.name) {
                            var surname = pax.surName ? pax.surName : '';
                            guestNames += (' \n' + pax.title  + ' ' + pax.name + ' ' + surname + ' - ' + pax.type);
                        }
                        if (pax.requested) {
                            guestCount++;
                        }
                    });
                });
                console.log(guestCount);
                var leadPax = self.tentUsers[0][0].title + ' ' + self.tentUsers[0][0].name + ' ' + self.tentUsers[0][0].surName;
                // var referenceNumber = agencyCode + '-HTL' + moment(new Date()).format("YYMMDDhhmmssSSS");
                var referenceNumber = agencyCode + '-';
                // dateValue1 = moment(dateValue1, 'DD/MM/YYYY').format('YYYY-MM-DDThh:mm:ss');
                dateValue1 = moment(dateValue1, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
                dateValue2 = moment(dateValue2, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
                // var roomType = self.withPark == 'true' ? 'Stay Including Park Access' : 'Only Stay' || 'Only Stay';
                var phoneNumber = $('.selected-dial-code').text() + self.phone;
                var sendingRequest = {
                    type: "Tent Accommodation Booking",
                    keyword1: leadPax.trim(),
                    keyword2: self.email,
                    keyword3: roomValue,
                    keyword4: self.maincontent.Tent_Name,
                    keyword5: guestCount, //totalPax
                    keyword6: phoneNumber,
                    // keyword10: referenceNumber,
                    date1: dateValue1,
                    date2: dateValue2,
                    amount1: self.grandTotal,
                    text2: guestNames.trim(),
                    // text3: roomType.trim(),
                    text10: JSON.stringify(self.tentUsers),
                    // keyword18: self.maincontent.Hotel_ID,
                    nodeCode: agencyCode
                };
                self.cmsRequestData("POST", "cms/data", sendingRequest, null).then(function (response) {
                    let insertID = Number(response);
                    var finalConfirmation = {
                        TentName: self.maincontent.Tent_Name,
                        packegeUrl: self.pageURLLink,
                        personName: leadPax,
                        emailAddress: self.email,
                        contact: phoneNumber,
                        checkIn: dateValue1,
                        checkOut: dateValue2,
                        adults: self.countGuest,
                        referenceNumber: referenceNumber + insertID,
                        cmsId: insertID,
                        amount: self.grandTotal,
                        TentId: self.maincontent.Tent_ID,
                    }
                    var updateDate = {
                        keyword10: referenceNumber + insertID
                    }
                    self.cmsFormUpdateData(updateDate, insertID).then(function (update) {
                        if (update.status == 200) {
                            window.sessionStorage.setItem("TentDetails", JSON.stringify(finalConfirmation));
                            window.sessionStorage.setItem("SelectedTents", JSON.stringify(self.selectedTents));
                            window.sessionStorage.setItem("emailStatustends", false);
                            if (referenceNumber && insertID) {
                                self.paymentFunction((referenceNumber + insertID));
                            }
                            console.log('Success:' + update.status);
                        } else {
                            alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M045')).set('closable', false);
                        }
                    }).catch(function (updateError) {
                        self.isLoading = false;
                        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M045')).set('closable', false);

                        console.error(updateError);
                    });
                }).catch(function (error) {
                    self.isLoading = false;
                    console.error(error);
                    alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M045')).set('closable', false);
                });
            }
        },
        paymentFunction: function (RefNumber) {
            try {
                var paymentDetails = {
                    "totalAmount": -188.88,
                    "customerEmail": "test@test.com",
                    "bookingReference": "",
                    "cartID": ""
                };
                var user = JSON.parse(localStorage.User);
                var returnURL = window.location.origin + "/Spartan/tentconfirmation.html"
                paymentDetails.bookingReference = RefNumber;
                paymentDetails.totalAmount = this.$n((this.grandTotal / this.CurrencyMultiplier), 'currency', this.selectedCurrency).replace(/[^\d.-]/g, "");
                paymentDetails.cartID = RefNumber;
                paymentDetails.currency = user.loginNode.currency || 'USD';
                paymentDetails.currentPayGateways = user.loginNode.paymentGateways;
                paymentDetails.customerEmail = this.email;
                var awsApi = true
                var paymentForm = paymentManager(paymentDetails, returnURL, window, awsApi);
                return paymentForm;

            } catch (error) {

            }
        },
        // getSearchData: function () {
        //     var urldata = this.getUrlVars();
        //     return urldata;
        // },
        cmsRequestData: async function (callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            return fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json'
                },
                body: data, // body data type must match "Content-Type" header
            }).then(function (response) {
                return response.json();
            });
        },
        cmsFormUpdateData: async function (data, id) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/cms/data/" + id;
            var callMethod = "PUT";
            if (data != null) {
                data = JSON.stringify(data);
            }
            try {
                let formPostRes = await axios({
                    url: url,
                    method: callMethod,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    data: data
                })
                return formPostRes;
            } catch (error) {
                console.log(error);
                return error.response.data.code;
            }
        },
        getLabels: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var Language = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var hotelcms = huburl + portno + commonPath + Agencycode + '/Template/Tent Detail Page/Tent Detail Page/Tent Detail Page.ftl';
              
                // var cmsurl = huburl + portno + hotelcms;
                axios.get(hotelcms, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': Language
                    }
                }).then(function (res) {
                    console.log(res);

                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var banner = pluck('Banner_Section', self.content);
                        self.bannerSection = getAllMapData(banner[0].component);
                        var main = pluck('Main_Section', self.content);
                        self.labelsSection = getAllMapData(main[0].component);
                    }

                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });
                // axios.get(labelsurl, {
                //     headers: {
                //         'content-type': 'text/html',
                //         'Accept': 'text/html',
                //         'Accept-Language': Language
                //     }
                // }).then(function (res) {
                //    // console.log(res);
                   
                // }).catch(function (error) {
                //     console.log('Error', error);
                //     // self.content = [];
                // });

                var transferUrl = huburl + portno + commonPath + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';
                axios.get(transferUrl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': Language
                    }
                }).then(function (res) {
                    var packageData = res.data;
                    self.Packages = packageData.Values;
                    self.Packages.forEach(function (item, index) {
                        if (item.Location) {
                            self.LocationList.push(item.Location.toUpperCase().trim());
                        }
                    });
                    self.LocationList = _.uniq(_.sortBy(self.LocationList));
                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });

            });
        },
        // dateChecker: function () {
        //     let dateObj1 = document.getElementById("from1");
        //     let dateValue1 = dateObj1.value;
        //     let dateObj2 = document.getElementById("from2");
        //     let dateValue2 = dateObj2.value;
        //     if (dateValue2 !== undefined || dateValue2 !== '') {
        //         // document.getElementById("to1").value = '';
        //         $('#from2').val('').trigger('change');
        //         $("#from2").val("");
        //         return false;
        //     }
        // },
        // setCalender() {
        //     var dateFormat = "dd/mm/yy"
        //     $("#from1").datepicker({
        //         // minDate: "0d",
        //         // maxDate: "360d",
        //         minDate: new Date('12/02/2021'),
        //         maxDate: new Date('12/05/2021'),
        //         numberOfMonths: 1,
        //         changeMonth: true,
        //         showOn: "both",
        //         buttonText: "<i class='fa fa-calendar'></i>",
        //         duration: "fast",
        //         showAnim: "slide",
        //         showOptions: {
        //             direction: "up"
        //         },
        //         showButtonPanel: false,
        //         dateFormat: dateFormat,
        //     });

        //     $("#from2").datepicker({
        //         minDate: new Date('12/02/2021'),
        //         maxDate: new Date('12/05/2021'),
        //         numberOfMonths: 1,
        //         changeMonth: true,
        //         showOn: "both",
        //         buttonText: "<i class='fa fa-calendar'></i>",
        //         duration: "fast",
        //         showAnim: "slide",
        //         showOptions: {
        //             direction: "up"
        //         },
        //         showButtonPanel: false,
        //         dateFormat: dateFormat,
        //         beforeShow: function (event, ui) {
        //             var selectedDate = $("#from1").val();
        //             $("#from2").datepicker("option", "minDate", selectedDate);
        //         }
        //     });
        // }
    }
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
jQuery(document).ready(function ($) {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
});

function getUser() {
    if (localStorage.IsLogin) {
        var user = JSON.parse(localStorage.User);
        return user;
    }
    return null;
}