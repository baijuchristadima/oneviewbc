const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var maininstance = new Vue({
    i18n,
    el: '#transfer-booking',
    name: 'transfer-booking',
    data: {
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        content: {
            area_List: []
        },
        Transfer: {
            Description_Title: null,
            Destination_From: null
        },
        Traveler_Details: {
            Terms_and_Conditions: null
        },
        pageURLLink: null,
        travellerInfo: {
            firstName: null,
            lastName: null,
            email: null,
            contactNumber: null
        },
        siteData: null,
        searchCriteria: {},
        checked: false,
        isBooking: false,
        labels:{},
        banner:{}
        // historyData: '',
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                packageurl = getQueryStringValue('page');
                if (packageurl != "") {
                    packageurl = packageurl.split('-').join(' ');
                    packageurl = packageurl.split('_')[0];
                    self.pageURLLink = packageurl;
                    var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
                    var siteData = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Transfer Booking Page/Transfer Booking Page/Transfer Booking Page.ftl';
                    var searchData = self.getSearchData();
                    // self.historyData = searchData;
                    if (searchData != "" || searchData != null) {
                        searchData.hour = searchData.hour.trim();
                        searchData.date = searchData.date.trim();
                        searchData.rdate = searchData.rdate.trim();
                        searchData.min = searchData.min.trim();
                        searchData.guest = searchData.guest.trim();
                        self.searchCriteria = Object.assign({}, searchData);
                        localStorage.setItem("backUrl", self.yourCallBackFunction(self.searchCriteria));
                        axios.get(topackageurl, {
                            headers: {
                                'content-type': 'text/html',
                                'Accept': 'text/html',
                                'Accept-Language': langauage
                            }
                        }).then(function (response) {
                            self.content = response.data;
                            var pagecontent = self.pluck('Transfers_Details_Section', self.content.area_List);
                            if (pagecontent.length > 0) {
                                self.Transfer.Destination_From = self.pluckcom('Destination_From', pagecontent[0].component);
                                self.Transfer.Destination_To = self.pluckcom('Destination_To', pagecontent[0].component);
                                self.Transfer.Vehicle_Name = self.pluckcom('Vehicle_Name', pagecontent[0].component);
                                self.Transfer.Description = self.pluckcom('Description', pagecontent[0].component);
                                self.Transfer.Cancellation_Policy = self.pluckcom('Cancellation_Policy', pagecontent[0].component);
                                self.Transfer.Availability_Status = self.pluckcom('Availability_Status', pagecontent[0].component);
                                self.Transfer.Price = self.pluckcom('Price', pagecontent[0].component);
                                self.Transfer.Extra_Hour_Rate = self.pluckcom('Extra_Hour_Rate', pagecontent[0].component);
                                self.Transfer.Full_Day_Rent = self.pluckcom('Full_Day_Rent', pagecontent[0].component);
                                self.Transfer.City = self.pluckcom('City', pagecontent[0].component);
                                self.Transfer.Max_Number_of_Passengers = self.pluckcom('_Max_Number_of_Passengers', pagecontent[0].component);
                                self.Transfer.Rate_Basis = self.pluckcom('Rate_Basis', pagecontent[0].component);
                                self.Transfer.Vehicle_Image = self.pluckcom('Vehicle_Image', pagecontent[0].component);

                            }
                        }).catch(function (error) {
                            console.log("2", error)
                            window.location.href = "/Spartan/transfer-booking.html";
                            self.content = [];
                        });
                    }
                    axios.get(siteData, {
                        headers: {
                            'content-type': 'text/html',
                            'Accept': 'text/html',
                            'Accept-Language': langauage
                        }
                    }).then(function (res) {
                        self.siteData = res.data;
                        if (siteData.length > 0) {
                        var main = pluck('Main_Section', self.siteData.area_List);
                        self.labels = getAllMapData(main[0].component);
                        var bannerDetails=pluck('Banner_Section',self.siteData.area_List);
                        self.banner=getAllMapData(bannerDetails[0].component);
                        }
                        // var siteData = self.pluck('Transfer_Details_Section', self.siteData.area_List);
                        // if (siteData.length > 0) {
                        //     self.Transfer.Description_Title = self.pluckcom('Description_Title', siteData[0].component);
                        //     self.Transfer.Cancellation_Policy_Title = self.pluckcom('Cancellation_Policy_Title', siteData[0].component);
                        // }
                        // var Traveler = self.pluck('Traveler_Details_Section', self.siteData.area_List);
                        // if (Traveler.length > 0) {
                        //     self.Traveler_Details.Terms_and_Conditions = self.pluckcom('Terms_and_Conditions', Traveler[0].component);
                        //     self.Traveler_Details.Description = self.pluckcom('Description', Traveler[0].component);
                        // }

                    }).catch(function (error) {
                        console.log("1", error)
                        window.location.href = "/Spartan/transfer-booking.html";
                        self.siteData = [];
                    });


                }
            });
        },
        getTemplate(template) {
            // var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
            var url = ServiceUrls.emailServices.getTemplate + "AGY75/" + template;
            // var url = ServiceUrls.emailServices.getTemplate + LoggedUser.loginNode.code + "/" + template;
            return axios.get(url);
        },
        bookingDeatils: function () {
            var self = this;
            if (!this.travellerInfo.firstName) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M017')).set('closable', false);
                return false;
            }
            if (!this.travellerInfo.lastName) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M018')).set('closable', false);
            }
            if (this.travellerInfo.email == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M009')).set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.travellerInfo.email.match(emailPat);
            if (matchArray == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M019')).set('closable', false);
                return false;
            }
            if (this.travellerInfo.contactNumber == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M020')).set('closable', false);
                return false;
            }
            if (!this.travellerInfo.contactNumber) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M020')).set('closable', false);
                return false;
            }
            if (this.travellerInfo.contactNumber.length < 8) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M046')).set('closable', false);
                return false;
            }
            if (this.checked == false) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M021')).set('closable', false);
                return false;
            } else {
                this.isBooking = true;
                var user = JSON.parse(localStorage.User);
                var fromEmail = user.loginNode.parentEmailId ? user.loginNode.parentEmailId : (user.loginNode.email ? user.loginNode.email : 'uaecrt@gmail.com') || 'uaecrt@gmail.com';
                var toEmail = user.loginNode.parentEmailId;
                var ccEmails = _.filter(user.loginNode.emailList, function (o) {
                    return o.emailTypeId == 3 && o.emailType.toLowerCase() == 'support';
                });
                ccEmails = _.map(ccEmails, function (o) {
                    return o.emailId
                });
                var agency = localStorage.getItem("AgencyCode");
                // var referenceNumber = agency + '-TFR' + moment(new Date()).format("YYMMDDhhmmssSSS");
                var referenceNumber = agency + '-';


                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                var bookDate = moment(self.searchCriteria.date, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
                if(self.searchCriteria.rdate){
                var returnDate = moment(self.searchCriteria.rdate, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
                 }
                var sendingRequest = {
                    type: "Transfer Booking",
                    keyword1: self.travellerInfo.firstName,
                    keyword2: self.travellerInfo.lastName,
                    keyword3: self.travellerInfo.email,
                    keyword11: self.travellerInfo.contactNumber,
                    keyword4: self.Transfer.Vehicle_Name,
                    keyword5: self.Transfer.City,
                    keyword6: self.Transfer.Destination_From,
                    keyword7: self.Transfer.Destination_To,
                    // keyword12: referenceNumber,
                    date1: bookDate,
                    date3:returnDate ? returnDate :'',
                    keyword9: self.searchCriteria.guest,
                    keyword10: self.searchCriteria.hour + ':' + self.searchCriteria.min,
                    amount1: (self.searchCriteria.rdate = undefined )? self.Transfer.Price :(self.Transfer.Price * 2),
                    date2: requestedDate,
                    nodeCode: agencyCode
                };
                self.cmsRequestData("POST", "cms/data", sendingRequest, null).then(function (response) {
                    let insertID = Number(response);
                    referenceNumber = referenceNumber + insertID;
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    var updateDate = {
                        keyword12: referenceNumber
                    }
                    self.cmsFormUpdateData(updateDate, insertID).then(function (update) {
                        if (update.status == 200) {
                            console.log('Success:' + update.status);
                            var phoneNumber = JSON.parse(localStorage.User).loginNode.phoneList.filter(function (phones) {
                                return phones.type.toLowerCase().includes('telephone') || phones.type.toLowerCase().includes('mobile')
                            });
                            var postData = {
                                type: "PackageBookingRequest",
                                fromEmail: fromEmail,
                                toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                                ccEmails: ccEmails,
                                logo: JSON.parse(localStorage.User).loginNode.logo,
                                // logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                                agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                                agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                                packegeName: self.Transfer.Vehicle_Name,
                                personName: self.travellerInfo.firstName,
                                emailAddress: self.travellerInfo.email,
                                contact: self.travellerInfo.contactNumber,
                                departureDate: moment(self.searchCriteria.date, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'),
                                adults: self.searchCriteria.guest,
                                child2to5: "NA",
                                child6to11: "NA",
                                infants: "NA",
                                message: "Transfer Booking Reference Number: " + referenceNumber,
                                primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                                secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                            };
                            var emailContent = {
                                logoUrl: JSON.parse(localStorage.User).loginNode.logo,
                                bookingLabel: "Booking ID",
                                bookingReference: referenceNumber,
                                agencyEmail: fromEmail,
                                personName: self.travellerInfo.firstName,
                                title: "Inquiry Received for Transfer",
                                servicesData: [{
                                        name: "Package Name",
                                        value: self.Transfer.Vehicle_Name
                                    },
                                    {
                                        name: "Guest Name",
                                        value: self.travellerInfo.firstName + " " + self.travellerInfo.lastName
                                    },
                                    {
                                        name: "Email Address",
                                        value: self.travellerInfo.email
                                    },
                                    {
                                        name: "Contact",
                                        value: self.travellerInfo.contactNumber
                                    },
                                    {
                                        name: "Package Start Date",
                                        value: moment(self.searchCriteria.date, 'DD/MM/YYYY').format("YYYY-MM-DD")
                                    },
                                    {
                                        name: "Adults",
                                        value: self.searchCriteria.guest
                                    },
                                    {
                                        name: "Child",
                                        value: "NA"
                                    },
                                    {
                                        name: "Infant",
                                        value: "NA"
                                    }
                                ],
                                emailMessage: "<p>Your inquiry is successfully received and is under booking ID&nbsp;" + referenceNumber + ".</p><p>One of our agent will get back to you shortly.</p>",
                                agencyPhone: phoneNumber[0].nodeContactNumber.number ? '+' + JSON.parse(localStorage.User).loginNode.country.telephonecode + "" + phoneNumber[0].nodeContactNumber.number : 'NA'
                            };

                            self.getTemplate("SpartanEmailTemplate").then(function (templateResponse) {
                                var data = templateResponse.data.data;
                                var emailTemplate = "";
                                if (data.length > 0) {
                                    for (var x = 0; x < data.length; x++) {
                                        if (data[x].enabled == true && data[x].type == "SpartanEmailTemplate") {
                                            emailTemplate = data[x].content;
                                            break;
                                        }
                                    }
                                };
                                var htmlGenerate = ServiceUrls.emailServices.htmlGenerate;
                                var emailData = {
                                    template: emailTemplate,
                                    content: emailContent
                                };
                                axios.post(htmlGenerate, emailData)
                                    .then(function (htmlResponse) {
                                        var emailPostData = {
                                            type: "AttachmentRequest",
                                            toEmails: Array.isArray(self.travellerInfo.email) ? self.travellerInfo.email : [self.travellerInfo.email],
                                            fromEmail: fromEmail,
                                            ccEmails: null,
                                            bccEmails: null,
                                            subject: "Booking Confirmation - " + referenceNumber,
                                            attachmentPath: "",
                                            html: htmlResponse.data.data
                                        };
                                        var mailUrl = ServiceUrls.emailServices.emailApiWithAttachment;
                                        sendMailServiceWithCallBack(mailUrl, emailPostData, function () {
                                            sendMailServiceWithCallBack(emailApi, postData, function () {
                                                self.isBooking = false;
                                                self.travellerInfo.email = '';
                                                self.travellerInfo.firstName = '';
                                                self.travellerInfo.contactNumber = '';
                                                self.travellerInfo.lastName = '';
                                                alertify.alert(getValidationMsgByCode('M023'), getValidationMsgByCode('M022')).set('closable', false);
                                                // setTimeout(function () { window.location.reload() }, 3000);
                                            });
                                        });
                                    }).catch(function (error) {
                                        console.log(error);
                                    })
                            })
                        } else {
                            alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M21')).set('closable', false);
                        }
                    }).catch(function (updateError) {
                        self.isLoading = false;
                        alertify.alert(getValidationMsgByCode('M023'), getValidationMsgByCode('M045')).set('closable', false);
                        console.error(updateError);
                    });
                }).catch(function (error) {
                    self.isLoading = false;
                    console.error(error);
                    alertify.alert(getValidationMsgByCode('M023'), getValidationMsgByCode('M045')).set('closable', false);
                });
            }
        },
        cmsRequestData: function (callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            return fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json'
                },
                body: data, // body data type must match "Content-Type" header
            }).then(function (response) {
                return response.json();
            });
        },
        cmsFormUpdateData: async function (data, id) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/cms/data/" + id;
            var callMethod = "PUT";
            if (data != null) {
                data = JSON.stringify(data);
            }
            try {
                let formPostRes = await axios({
                    url: url,
                    method: callMethod,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    data: data
                })
                return formPostRes;
            } catch (error) {
                console.log(error);
                return error.response.data.code;
            }
        },
        getSearchData: function () {
            var urldata = this.getUrlVars();
            // urldata = decodeURIComponent(urldata);
            return urldata;
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            let decodeUrl= decodeURIComponent(window.location.href);
            var hashes = decodeUrl.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        yourCallBackFunction: function (data) {
            if (data != null) {
                if (data != "") {
                    var customSearch =
                        "&date=" + data.date +
                        "&hour=" + (parseInt(data.hour) + 1) +
                        "&min=" + (parseInt(data.min) + 1) +
                        "&guest=" + data.guest +
                        "&city=" + data.city +
                        "&from=" + data.from +
                        "&to=" + data.to + "&";

                    url = "/Spartan/transfer-results.html?page=" + customSearch;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;
        },
        },    mounted: function () {
        localStorage.removeItem("backUrl");
        this.getPagecontent();
        sessionStorage.active_e = 3;
    },
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}