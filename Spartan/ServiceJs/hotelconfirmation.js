const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var hotelList = new Vue({
    i18n,
    el: '#hotelconfirmation',
    name: 'hotelconfirmation',
    data: {
        divHtlBkngSucces: false,
        isLoading: true,
        user: {
            "loginNode": {
                "address": "",
                "country": {
                    "code": "",
                    "telephonecode": ""
                },
                "phoneList": [{
                        "type": "",
                        "nodeContactNumber": {
                            "number": ""
                        },
                        "number": ""
                    },
                    {
                        "type": "",
                        "nodeContactNumber": {
                            "number": ""
                        },
                        "number": ""
                    },
                    {
                        "type": "",
                        "nodeContactNumber": {
                            "number": ""
                        },
                        "number": ""
                    }
                ],
            }
        },
        logoUrl: '',
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        maincontent: {},
        details: {},
        Bookingdetails: {},
        Roomdetails: {},
        hotelDetails: {
            hotelName: '',
            packegeUrl: '',
            personName: '',
            emailAddress: '',
            contact: '',
            checkIn: '',
            checkOut: '',
            adults: '',
            referenceNumber: '',
            cmsId: '',
        },
        SelectedRooms: {},
        bookDetailsfromCMS: {},
        phoneNumberAgency: '',
        telephonecode: '',
        paxDetails: {},
        labels:{}
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        getLabel: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = commonPath + Agencycode + '/Template/Hotel Confirmation/Hotel Confirmation/Hotel Confirmation.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (res) {
                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var main = pluck('Main_Section', self.content);
                        self.labels = getAllMapData(main[0].component);
                    }

                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });
            });
        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {

                if (type == 'from1') {
                    this.fromDate = event.target.value;
                } else if (type == 'to1') {
                    this.toDate = event.target.value;
                } else if (type == 'city') {
                    this.cityName = event.target.value;
                } else if (type == 'occupancy') {
                    this.occupancyType = event.target.value;
                }
            }
        },
        getCmsFormDataById: async function (id) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/cms/data/" + id;
            // var HubServices = this.HubServicesUrls;
            // var url = HubServices.CMS_SERVER + HubServices.CMS_Form + "/" + id;
            var callMethod = "GET";
            try {
                let formPostRes = await axios({
                    url: url,
                    method: callMethod,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                })
                return formPostRes;
            } catch (error) {
                console.log(error);
                return error.response.data.code;
            }
        },
        getPackagedetails: function (hotelurl) {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                if (hotelurl != "") {
                    var tohotelurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + hotelurl + '.ftl';
                    axios.get(tohotelurl, {
                        headers: {
                            'content-type': 'text/html',
                            'Accept': 'text/html',
                            'Accept-Language': langauage
                        }
                    }).then(function (response) {
                        var maincontent = self.pluck('Hotel_Details', response.data.area_List);
                        if (maincontent.length > 0) {
                            self.maincontent = self.getAllMapData(maincontent[0].component);
                            // self.maincontent.Hotel_Name = self.pluckcom('Hotel_Name', maincontent[0].component);
                            // self.maincontent.Rate = self.pluckcom('Rate', maincontent[0].component);
                            // self.checkPrice();
                        }
                        var details = self.pluck('Hotel_Details_Section', response.data.area_List);
                        if (details.length > 0) {
                            self.details.Informations = self.pluckcom('Informations', details[0].component);
                            self.details.Hotel_Image_Banner = self.pluckcom('Hotel_Image_Banner', details[0].component);
                            self.details.Amenities = self.pluckcom('Amenities', details[0].component);
                        }
                        var Bookingdetails = self.pluck('Booking_Details_Section', response.data.area_List);
                        if (Bookingdetails.length > 0) {
                            self.Bookingdetails.inDate = self.pluckcom('Main_Check_In_Date', Bookingdetails[0].component);
                            self.Bookingdetails.outDate = self.pluckcom('Main_Check_Out_Date', Bookingdetails[0].component);
                            self.Bookingdetails.nights = self.pluckcom('Number_of_Nights', Bookingdetails[0].component);
                            self.Bookingdetails.instructions = self.pluckcom('Instructions', Bookingdetails[0].component);

                        }
                        var Roomdetails = self.pluck('Room_Details_Section', response.data.area_List);
                        if (Roomdetails.length > 0) {
                            self.Roomdetails.Room_Details = self.pluckcom('Room_Details', Roomdetails[0].component);
                            self.Roomdetails.Total_Rooms = self.pluckcom('Total_Rooms', Roomdetails[0].component);
                            self.Roomdetails.Instructions = self.pluckcom('Instructions', Roomdetails[0].component);

                        }

                    })
                }
            });
        },
        getTemplate(template) {
            // var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
            var url = ServiceUrls.emailServices.getTemplate + "AGY75/" + template;
            // var url = ServiceUrls.emailServices.getTemplate + LoggedUser.loginNode.code + "/" + template;
            return axios.get(url);
        },
        sentMail: function () {
            var self = this;
            var user = JSON.parse(localStorage.User);
            var fromEmail = user.loginNode.parentEmailId ? user.loginNode.parentEmailId : (user.loginNode.email ? user.loginNode.email : 'uaecrt@gmail.com') || 'uaecrt@gmail.com';
            var toEmail = user.loginNode.parentEmailId;
            var ccEmails = _.filter(user.loginNode.emailList, function (o) {
                return o.emailTypeId == 3 && o.emailType.toLowerCase() == 'support';
            });
            ccEmails = _.map(ccEmails, function (o) {
                return o.emailId
            });
            var postData = {
                type: "PackageBookingRequest",
                fromEmail: fromEmail,
                toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                ccEmails: ccEmails,
                logo: user.loginNode.logo || "",
                agencyName: user.loginNode.name || "",
                agencyAddress: user.loginNode.address || "",
                packegeName: this.maincontent.Hotel_Name,
                personName: this.hotelDetails.personName,
                emailAddress: this.hotelDetails.emailAddress,
                contact: this.hotelDetails.contact,
                departureDate: 'NA',
                adults: this.hotelDetails.adults,
                child2to5: "NA",
                child6to11: "NA",
                infants: "NA",
                message: "Hotel Booking Reference Number: " + this.hotelDetails.referenceNumber,
                primaryColor: '#' + user.loginNode.lookNfeel.primary,
                secondaryColor: '#' + user.loginNode.lookNfeel.secondary
            };
            var custmail = {
                type: "ThankYouRequest",
                message: "Your Hotel Booking Reference Number: " + this.hotelDetails.referenceNumber,
                fromEmail: fromEmail,
                toEmails: Array.isArray(this.hotelDetails.emailAddress) ? this.hotelDetails.emailAddress : [this.hotelDetails.emailAddress],
                logo: user.loginNode.logo || "",
                agencyName: user.loginNode.name || "",
                agencyAddress: user.loginNode.address || "",
                personName: this.hotelDetails.personName,
                primaryColor: '#' + user.loginNode.lookNfeel.primary,
                secondaryColor: '#' + user.loginNode.lookNfeel.secondary
            };
            try {
                var emailApi = ServiceUrls.emailServices.emailApi;
                // sendMailService(emailApi, postData);
                // sendMailService(emailApi, custmail);

                var phoneNumber = JSON.parse(localStorage.User).loginNode.phoneList.filter(function (phones) {
                    return phones.type.toLowerCase().includes('telephone') || phones.type.toLowerCase().includes('mobile')
                });

                var message = "";

                if (this.divHtlBkngSucces) {
                    message = "<p>Your hotel booking was successful and is under booking ID&nbsp;" + this.hotelDetails.referenceNumber + ".</p><p>We will send you an email with your hotel confirmation number, hotel booking voucher and invoice.</p><p>Please note this can take up to 2 hours.</p>";
                } else {
                    message = "<p>Your booking has failed.</p>"
                }

                var nights = moment(new Date(self.bookDetailsfromCMS.date2)).diff(moment(new Date(self.bookDetailsfromCMS.date1)), 'days');

                var roomDetails = JSON.parse(self.bookDetailsfromCMS.text10);

                var filteredRoom = [];
                roomDetails.forEach(function (room) {
                    filteredRoom.push(room.filter(function (r) {
                        return r.requested;
                    }))
                });

                var adult = 0;
                var child = 0;

                for (var i = 0; i < filteredRoom.length; i++) {
                    for (var j = 0; j < filteredRoom[i].length; j++) {
                        if (filteredRoom[i][j].type.toLowerCase() == "adult") {
                            adult++;
                        } else if (filteredRoom[i][j].type.toLowerCase() == "child") {
                            child++;
                        }
                    }
                }
                var emailContent = {
                    message: message,
                    title: "Inquiry Received for Hotel",
                    agencyPhone: phoneNumber[0].nodeContactNumber.number ? '+' + JSON.parse(localStorage.User).loginNode.country.telephonecode + "" + phoneNumber[0].nodeContactNumber.number : 'NA',
                    agencyEmail: fromEmail || "",
                    logoUrl: JSON.parse(localStorage.User).loginNode.logo,
                    fullName: this.hotelDetails.personName,
                    bookingRefId: this.hotelDetails.referenceNumber,
                    hotelRef: "Pending",
                    hotelName: this.maincontent.Hotel_Name,
                    hotelAddress: this.maincontent.Hotel_Address,
                    nights: nights + (nights > 1 ? ' Nights ' : ' Night '),
                    adults: ' ' + adult + (adult > 1 ? ' Adults ' : ' Adult '),
                    child: ' ' + child + (child > 1 ? ' Children ' : ' Child '),
                    room: ' ' + self.bookDetailsfromCMS.keyword3 + (self.bookDetailsfromCMS.keyword3 > 1 ? ' Rooms' : ' Room'),
                    checkIn: moment(new Date(self.bookDetailsfromCMS.date1), 'DD/MM/YYYY').format("YYYY-MM-DD"),
                    checkOut: moment(new Date(self.bookDetailsfromCMS.date2), 'DD/MM/YYYY').format("YYYY-MM-DD"),
                    roomDetails: filteredRoom.map(function (r, i) {
                        return {
                            name: ("Room " + (i + 1) + ' - (' + self.bookDetailsfromCMS.text3 + ') - ' + self.SelectedRooms[i].Room_Type).trim(),
                            pax: r.map(function (e) {
                                return (e.name + " " + e.surName).toUpperCase();
                            }).join("<br/>")
                        }
                    })
                }

                self.getTemplate("SpartanHotelEmailTemplate").then(function (templateResponse) {
                    var data = templateResponse.data.data;
                    var emailTemplate = "";
                    if (data.length > 0) {
                        for (var x = 0; x < data.length; x++) {
                            if (data[x].enabled == true && data[x].type == "SpartanHotelEmailTemplate") {
                                emailTemplate = data[x].content;
                                break;
                            }
                        }
                    };
                    var htmlGenerate = ServiceUrls.emailServices.htmlGenerate;
                    var emailData = {
                        template: emailTemplate,
                        content: emailContent
                    };
                    axios.post(htmlGenerate, emailData)
                        .then(function (htmlResponse) {
                            var emailPostData = {
                                type: "AttachmentRequest",
                                toEmails: Array.isArray(self.hotelDetails.emailAddress) ? self.hotelDetails.emailAddress : [self.hotelDetails.emailAddress],
                                fromEmail: fromEmail,
                                ccEmails: null,
                                bccEmails: null,
                                subject: "Booking Confirmation - " + self.hotelDetails.referenceNumber,
                                attachmentPath: "",
                                html: htmlResponse.data.data
                            };
                            var mailUrl = ServiceUrls.emailServices.emailApiWithAttachment;
                            sendMailServiceWithCallBack(mailUrl, emailPostData, function () {
                                sendMailServiceWithCallBack(emailApi, postData, function () {
                                    // self.isBooking = false;
                                });
                            });
                        }).catch(function (error) {
                            console.log(error);
                        })
                })
            } catch (e) {
                console.log(e);
            }
        },
        dayFromDate: function (date) {
            return moment(date).format('dddd');
        },
        formatDate: function (date) {
            return moment(date).format('YYYY-MM-DD');
        },
        guestCounts: function (room) {
            var PaxList = room.filter(pax => (pax.requested));
            var adult = 0;
            var child = 0;
            PaxList.forEach(element => {
                if (element.type == "Adult") {
                    adult++;
                } else if (element.type == "Child") {
                    child++;
                }
            });
            var guests = (adult ? (adult + ' Adult(s)') : '') + (child ? (', ' + child + ' Child') : '');
            return guests.trim();
        },
        leadPax: function (room) {
            var PaxList = room.filter(pax => (pax.requested && pax.type == "Adult"));
            if (PaxList) {
                return (PaxList[0].name + ' ' + PaxList[0].surName).trim();
            }
            return '';
        },
        cmsRequestData: async function (callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            return fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json'
                },
                body: data, // body data type must match "Content-Type" header
            }).then(function (response) {
                return response.json();
            });
        },
        cmsFormUpdateData: async function (data, id) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/cms/data/" + id;
            var callMethod = "PUT";
            if (data != null) {
                data = JSON.stringify(data);
            }
            try {
                let formPostRes = await axios({
                    url: url,
                    method: callMethod,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    data: data
                })
                return formPostRes;
            } catch (error) {
                console.log(error);
                return error.response.data.code;
            }
        },
        insertHotelData: function () {
            var self = this;
            let agencyCode = JSON.parse(localStorage.User).loginNode.code;
            var sendingRequest = {
                type: "Hotel Rooms",
                keyword1: self.hotelDetails.HotelId,
                text1: self.bookDetailsfromCMS.keyword4,
                number1: Number(self.bookDetailsfromCMS.keyword3),
                nodeCode: agencyCode
            };
            self.cmsRequestData("POST", "cms/data", sendingRequest, null).then(function (response) {
                let insertID = Number(response);
                console.log('InsertedHotelRoom' + insertID)
            }).catch(function (error) {
                console.error(error);
            });
        },
        updateHotelData: function (id, rooms) {
            var self = this;
            var totalRooms = Number(rooms) + Number(self.bookDetailsfromCMS.keyword3);
            var updateDate = {
                number1: totalRooms
            }
            self.cmsFormUpdateData(updateDate, id).then(function (update) {
                if (update.status == 200) {
                    console.log('Room Updated')
                }
            }).catch(function (error) {
                console.log('UpdatedRoomError:' + error);
            });
        },
        updateRoomCount: function () {
            var self = this;
            var filterValue = "type='Hotel Rooms' AND keyword1='" + self.hotelDetails.HotelId + "'";
            self.getDbDataTableValue(filterValue, "ingest_timestamp").then(function (response) {
                if (response != undefined && response.data != undefined && response.status == 200) {
                    var bookings = response.data.data;
                    var selected = bookings.find(ele => (ele.keyword1 == self.hotelDetails.HotelId))
                    self.updateHotelData(selected.id, selected.number1);
                } else if (response == 404) {
                    self.insertHotelData();
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getDbDataTableValue: async function (extraFilter, sortField) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/cms/data/search/byQuery";
            let agencyCode = JSON.parse(localStorage.User).loginNode.code;
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            if (requestObject != null) {
                requestObject = JSON.stringify(requestObject);
            }
            try {
                let allDBData = await axios({
                    url: url,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    data: requestObject
                })
                return allDBData;
            } catch (error) {
                console.log(error);
                return error.response.data.code;
            }
        },
    },
    mounted: function () {
        sessionStorage.active_e = 2;
        var self = this;
        this.user = JSON.parse(localStorage.User);
        this.isLoading = true;
        this.getLabel();
        var emailStatus = window.sessionStorage.getItem("emailStatusHtl");
        this.hotelDetails = JSON.parse(window.sessionStorage.getItem("HotelDetails"));
        this.SelectedRooms = JSON.parse(window.sessionStorage.getItem("SelectedRooms"));
        this.hotelDetails.checkIn = moment(this.hotelDetails.checkIn, "DD/MM/YYYY");
        this.hotelDetails.checkOut = moment(this.hotelDetails.checkIn, "DD/MM/YYYY");
        this.logoUrl = JSON.parse(localStorage.User).loginNode.logo;
        this.phoneNumberAgency = JSON.parse(localStorage.User).loginNode.phoneList[1].number;
        this.telephonecode = '+' + JSON.parse(localStorage.User).loginNode.country.telephonecode;
        this.getPackagedetails(this.hotelDetails.packegeUrl);
        this.getCmsFormDataById(this.hotelDetails.cmsId).then(function (response) {
            console.log(response);
            self.bookDetailsfromCMS = response.data;
            self.paxDetails = JSON.parse(response.data.text10);
            if (response.data.keyword8.toLowerCase() == "success") {
                self.divHtlBkngSucces = true;
                self.isLoading = false;
                if (emailStatus === 'false') {
                    window.sessionStorage.setItem("emailStatusHtl", true);
                    self.sentMail();
                    self.updateRoomCount();
                }
                return
            }
            self.divHtlBkngSucces = false;
            self.isLoading = false;
        }).catch(function (error) {
            self.isLoading = false;
            console.error(error);
        });
    },
});