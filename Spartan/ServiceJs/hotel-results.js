const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var HotelList = new Vue({
    i18n,
    el: '#Hotels',
    name: 'Hotels',
    data: {
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        Hotels: [],
        hotelFull: [],


        cityName: 'ABU DHABI',
        cityNameUi: 'أبو ظبي',
        fromDate: '',
        toDate: '',
        roomCount: 0,
        rooms: [],
        nights: 0,

        occupancyId: '',
        locationHotels: [],
        OccupancyHotels: [],
        pageLoad: true,
        showOccupancy: false,
        labelsSection:{},
        bannerSection:{},
        Banner:{
            Banner_Image:''
        },
        labels:{},
        LocationList: [],
        language: (localStorage.Languagecode) ? localStorage.Languagecode : "en"
    },
    methods: {
        addRoom: function () {
            var self = this;
            if (self.roomCount < 5) {
                self.roomCount++;
                let type = _.first(self.OccupancyHotels).Occupancy_ID;
                self.rooms.push(Number(type));
            }
        },
        removeRoom: function () {
            var self = this;
            if (self.roomCount > 1) {
                self.rooms = self.rooms.slice(0, self.rooms.length - 1);
                self.roomCount--;
            }
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getOccupancy: function (isForLanguage) {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = commonPath + Agencycode + '/Template/Hotel Configuration/Hotel Configuration/Hotel Configuration.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (res) {
                    if (res.data.area_List.length) {
                        var content = res.data.area_List;
                        var val = self.pluck('Configurations', content);
                        self.OccupancyHotels = self.pluckcom('Room_Occupancy_List', val[0].component);
                        self.cityName = "ABU DHABI";
                    }
                    if (isForLanguage) {
                        self.removeRoom();
                    }

                }).catch(function (error) {
                    console.log('Error', error);
                });
            });
        },
        noOfnightsFind: function (date1, date2) {
            if (date1 && date2) {
                var start = moment(date1, "DD/MM/YYYY");
                var end = moment(date2, "DD/MM/YYYY");
                var nights = end.diff(start, 'days')
                return nights;
            }
        },
        getRoomPrice: function(room) {
            var self = this;
            var priceList = [];
            self.rooms.forEach(element => {
                room.forEach(element2 => {
                    if (element == element2.Occupancy_ID) {
                        priceList.push(element2.Price)
                    }
                });
            });
            var price = _.reduce(priceList, function (sum, n) {
                return sum + Number(n);
            }, 0);
           return self.nights * price;
        },
        getPackage: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var tohotelurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Hotel/Hotel/Hotel.ftl';
                axios.get(tohotelurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    var hotelDate = response.data;
                    try {
                        self.cityName = getQueryStringValue('city').trim();
                        self.fromDate = getQueryStringValue('cin').trim();
                        self.toDate = getQueryStringValue('cout').trim();
                        self.roomCount = parseInt(getQueryStringValue('rooms').trim());
                        self.rooms = JSON.parse(getQueryStringValue('roomtype'));
                        self.nights = self.noOfnightsFind(self.fromDate, self.toDate);

                        if (hotelDate != undefined && hotelDate.Values != undefined) {
                            self.hotelFull = hotelDate.Values;
                            self.hotelFull.forEach(function (item, index) {
                                self.locationHotels.push(item.Location.toUpperCase().trim());
                            });
                            self.locationHotels = _.uniq(_.sortBy(self.locationHotels));




                            // var filter = [];
                            // self.rooms.forEach(element => {
                            //     filter.push({
                            //         "Occupancy_ID": element,
                            //     })
                            // });
                            // var uniqFilter = _.uniqWith(filter, _.isEqual);
                            // var cond = _.map(uniqFilter, filter1 => filter1.Occupancy_ID);

                            var cond = _.uniq(self.rooms, true);
                            var hotelList = [];
                            self.hotelFull.forEach(function (hotel) {
                                if (hotel.Status && (hotel.Location.toLowerCase().trim() == self.cityName.toLowerCase() || hotel.Location.toLowerCase().trim() == 'أبو ظبي')) {
                                    let {
                                        Room_Details,
                                        ...hotelWithoutRoomsDetails
                                    } = hotel;
                                    var hotelSelected = _.filter((hotel.Room_Details), room1 => _.indexOf(cond, Number(room1.Occupancy_ID)) !== -1);
                                    if (hotelSelected.length) {
                                        var priceList = [];
                                        self.rooms.forEach(element => {
                                            hotelSelected.forEach(element2 => {
                                                if (element == element2.Occupancy_ID) {
                                                    priceList.push(element2.Price)
                                                }
                                            });
                                        });
                                        hotelSelected.sort(function (a, b) { return Number(a.Price) - Number(b.Price);});
                                        priceList.sort();
                                        var hotelWithOut = hotelSelected.filter((element) => (!element.Including_Park_Access));
                                        var hotelWith = hotelSelected.filter((element) => (element.Including_Park_Access));
                                        
                                        // var price = _.reduce(priceList, function (sum, n) {
                                        //     return sum + Number(n);
                                        // }, 0);
                                        var priceWithOut = self.getRoomPrice(hotelWithOut);
                                        var priceWith = self.getRoomPrice(hotelWith);
                                        var price = Math.min(priceWithOut, priceWith);
                                        if (price == 0) {
                                            price = Math.max(priceWithOut, priceWith);
                                        }
                                        // var Including_Park_Access = false;
                                        if ((moment('2021-12-02').isBetween(moment(self.fromDate, "DD/MM/YYYY"), moment(self.toDate, "DD/MM/YYYY")) 
                                        || moment('2021-12-02').isSame(moment(self.fromDate, "DD/MM/YYYY")) 
                                        || moment('2021-12-02').isSame(moment(self.toDate, "DD/MM/YYYY"))) && hotel.Hotel_ID != "HOTEL2") {
                                            price = parseFloat(price) + 27.17;
                                            priceWithOut = parseFloat(priceWithOut) + 27.17;
                                            priceWith = parseFloat(priceWith) + 27.17;
                                        }
                                        hotelList.push({
                                            ...hotelWithoutRoomsDetails,
                                            price,
                                            ...{Including_Park_Access: hotelWith.length >= 1},
                                            ...{
                                                'Room_Details': [
                                                    {   
                                                        Including_Park_Access: false,
                                                        data: hotelWithOut,
                                                        price: priceWithOut
                                                    },
                                                    {
                                                        Including_Park_Access: true,
                                                        data: hotelWith,
                                                        price: priceWith
                                                    }
                                                ]
                                            }
                                        });
                                        // var hotelWithOut = hotelSelected.filter((element) => (!element.Including_Park_Access));
                                        // var hotelWith = hotelSelected.filter((element) => (element.Including_Park_Access));
                                        // if (hotelWithOut.length) {
                                        //     var priceList = [];
                                        //     self.rooms.forEach(element => {
                                        //         hotelWithOut.forEach(element2 => {
                                        //             if (element == element2.Occupancy_ID) {
                                        //                 priceList.push(element2.Price)
                                        //             }
                                        //         });
                                        //     });
                                        //     var price = _.reduce(priceList, function (sum, n) {
                                        //         return sum + Number(n);
                                        //     }, 0);
                                        //     price = (self.nights * price);
                                        //     var Including_Park_Access = false;
                                        //     hotelList.push({
                                        //         ...hotelWithoutRoomsDetails,
                                        //         price,
                                        //         Including_Park_Access,
                                        //         ...{
                                        //             'Room_Details': hotelWithOut
                                        //         }
                                        //     });
                                        // }
                                        // if (hotelWith.length) {
                                        //     var priceList2 = [];
                                        //     self.rooms.forEach(element => {
                                        //         hotelWith.forEach(element2 => {
                                        //             if (element == element2.Occupancy_ID) {
                                        //                 priceList2.push(element2.Price)
                                        //             }
                                        //         });
                                        //     });

                                        //     var price = _.reduce(priceList2, function (sum, n) {
                                        //         return sum + Number(n);
                                        //     }, 0);
                                        //     var Including_Park_Access = true;
                                        //     price = (self.nights * price);
                                        //     hotelList.push({
                                        //         ...hotelWithoutRoomsDetails,
                                        //         price,
                                        //         Including_Park_Access,
                                        //         ...{
                                        //             'Room_Details': hotelWith
                                        //         },
                                        //     });
                                        // }

                                    }
                                }

                            });

                            var filteredHotels = _.sortBy(hotelList, [function (o) {
                                return o.price;
                            }]);
                            var filteredHotelsWithPrice = filteredHotels.filter(hotel => (Number(hotel.Minimum_Number_of_Nights) != NaN && self.nights >= Number(hotel.Minimum_Number_of_Nights)));
                        }
                        $("#from1").val(self.fromDate);
                        $("#from2").val(self.toDate);
                        // $("#city").val(self.cityName);
                        // $("#occupancy").val(self.occupancyId).trigger('change');



                        // Scheck room available
                        var hits = 0;
                        var bookings = [];
                        var hotelsWithAvailableRooms = [];
                        var filterValue = "type='Hotel Rooms'";
                        self.getDbDataTableValue(filterValue, "ingest_timestamp").then(function (response) {
                            if (response != undefined && response.data != undefined && response.status == 200) {
                                hits = response.data.count;
                                bookings = response.data.data;
                                if (hits > 0) {
                                    filteredHotelsWithPrice.forEach(hotel => {
                                        var available = bookings.find(ele => (hotel.Hotel_ID.toLowerCase() == ele.keyword1.toLowerCase()));
                                        if (available) {
                                            var availableRooms = Number(hotel.Total_Rooms) - Number(available.number1)
                                            if (Number(availableRooms) >= Number(self.roomCount)) {
                                                hotelsWithAvailableRooms.push(hotel)
                                            }
                                        } else {
                                            hotelsWithAvailableRooms.push(hotel)
                                        }
                                        hotel.showRoomSelection = false;
                                    });
                                    self.Hotels = hotelsWithAvailableRooms;
                                    self.pageLoad = false;
                                }
                                // else {
                                //     self.Hotels = filteredHotelsWithPrice;
                                // }
                            } else if (response == 404) {
                                self.Hotels = filteredHotelsWithPrice;
                                self.pageLoad = false;
                            }
                        }).catch(function (error) {
                            self.pageLoad = false;
                            console.log(error);
                        });

                    } catch (error2) {
                        console.log('Error', error2);
                        self.pageLoad = false;
                    }
                }).catch(function (error3) {
                    console.log('Error', error3);
                    self.pageLoad = false;
                });
            });
        },
        getDbDataTableValue: async function (extraFilter, sortField) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/cms/data/search/byQuery";
            let agencyCode = JSON.parse(localStorage.User).loginNode.code;
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            if (requestObject != null) {
                requestObject = JSON.stringify(requestObject);
            }
            try {
                let allDBData = await axios({
                    url: url,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    data: requestObject
                })
                return allDBData;
            } catch (error) {
                console.log(error);
                return error.response.data.code;
            }
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            let decodeUrl= decodeURIComponent(window.location.href);
            var hashes = decodeUrl.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        getmoreinfo(hotel, doNotProceedBooking, index) {
            if (hotel.Room_Details.length > 1 && doNotProceedBooking) {
                hotel.showRoomSelection = !hotel.showRoomSelection
            } else {
                var self = this;
                try {
                    var url = hotel.More_details_page_link;
                    var park = _.first(hotel.Room_Details).Including_Park_Access;
                    if (hotel.Including_Park_Access) {
                        park = hotel.Room_Details[index != undefined ? index : hotel.Room_Details.length - 1].Including_Park_Access
                    }
                    var urlParam =
                        "&city=" + self.cityName +
                        "&cin=" + self.fromDate +
                        "&cout=" + self.toDate +
                        "&rooms=" + self.roomCount +
                        "&nights=" + self.nights +
                        "&parking=" + park +
                        "&roomTypes=" + JSON.stringify(self.rooms);
                    if (url != null) {
                        if (url != "") {
                            url = url.split("/Template/")[1];
                            url = url.split(' ').join('-');
                            url = url.split('.').slice(0, -1).join('.')
                            url = "/Spartan/hotel-details.html?page=" + url + urlParam;
                            window.location.href = url;
                        } else {
                            url = "#";
                        }
                    } else {
                        url = "#";
                    }
                    return url;
                } catch (error) {
                    console.log(error);
                }
            }
        },
        dateChecker: function () {
            let dateObj1 = document.getElementById("from1");
            let dateValue1 = dateObj1.value;
            let dateObj2 = document.getElementById("from2");
            let dateValue2 = dateObj2.value;
            if (dateValue2 !== undefined || dateValue2 !== '') {
                // document.getElementById("to1").value = '';
                $('#from2').val('').trigger('change');
                $("#from2").val("");
                return false;
            }
        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                if (type == 'city') {
                    this.cityName = event.target.value;
                } else if (type == 'from1') {
                    this.fromDate = event.target.value;
                } else if (type == 'from2') {
                    this.toDate = event.target.value;
                    this.nights = this.noOfnightsFind(this.fromDate, this.toDate);
                }
                // else if (type == 'occupancy') {
                //     this.occupancyId = event.target.value;
                // }
            }
        },
        modifySearch: function () {
            var self = this;
            self.pageLoad = true;
            if (self.cityName == undefined || self.cityName == '' || self.cityName == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M011')).set('closable', false);
                return false;
            } else if (self.fromDate == undefined || self.fromDate == '' || self.fromDate == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M006')).set('closable', false);
                return false;
            } else if (self.toDate == undefined || self.toDate == '' || self.toDate == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M007')).set('closable', false);
                return false;
            }
            //  else if (self.occupancyId == undefined || self.occupancyId == '' || self.occupancyId == null) {
            //     alertify.alert('Alert', 'Please select Occupancy.').set('closable', false);
            //     return false;
            // }
            else {
                var searchCriteria =
                "&city=" + self.cityName +
                "&cin=" + self.fromDate +
                "&cout=" + self.toDate +
                "&rooms=" + self.roomCount +
                "&roomtype=" + JSON.stringify(self.rooms);
                // searchCriteria = searchCriteria.split(' ').join('-');
                var uri = "/Spartan/hotel-results.html?" + searchCriteria;
                window.location.href = uri;
                // try {
                //     var filter = [];
                //     self.rooms.forEach(element => {
                //         filter.push({
                //             "Occupancy_ID": element,
                //         })
                //     });

                //     var uniqFilter = _.uniqWith(filter, _.isEqual);
                //     var cond = _.map(uniqFilter, filter1 => filter1.Occupancy_ID);

                //     var hotelList = [];
                //     self.hotelFull.forEach(function (hotel) {
                //         if (hotel.Status && hotel.Location.toLowerCase().trim() == self.cityName.toLowerCase()) {
                //             let {
                //                 Room_Details,
                //                 ...hotelWithoutRoomsDetails
                //             } = hotel;
                //             var hotelSelected = _.filter((hotel.Room_Details), room1 => _.indexOf(cond, Number(room1.Occupancy_ID)) !== -1);
                //             if (hotelSelected.length) {
                //                 var hotelWithOut = hotelSelected.filter((element) => (!element.Including_Park_Access));
                //                 var hotelWith = hotelSelected.filter((element) => (element.Including_Park_Access));
                //                 if (hotelWithOut.length) {
                //                     var priceList = [];
                //                     self.rooms.forEach(element => {
                //                         hotelWithOut.forEach(element2 => {
                //                             if (element == element2.Occupancy_ID) {
                //                                 priceList.push(element2.Price)
                //                             }
                //                         });
                //                     });
                //                     var price = _.reduce(priceList, function (sum, n) {
                //                         return sum + Number(n);
                //                     }, 0);
                //                     price = (self.nights * price);
                //                     var Including_Park_Access = false;
                //                     hotelList.push({
                //                         ...hotelWithoutRoomsDetails,
                //                         price,
                //                         Including_Park_Access,
                //                         ...{
                //                             'Room_Details': hotelWithOut
                //                         }
                //                     });
                //                 }
                //                 if (hotelWith.length) {
                //                     var priceList2 = [];
                //                     self.rooms.forEach(element => {
                //                         hotelWith.forEach(element2 => {
                //                             if (element == element2.Occupancy_ID) {
                //                                 priceList2.push(element2.Price)
                //                             }
                //                         });
                //                     });

                //                     var price = _.reduce(priceList2, function (sum, n) {
                //                         return sum + Number(n);
                //                     }, 0);
                //                     var Including_Park_Access = true;
                //                     price = (self.nights * price);
                //                     hotelList.push({
                //                         ...hotelWithoutRoomsDetails,
                //                         price,
                //                         Including_Park_Access,
                //                         ...{
                //                             'Room_Details': hotelWith
                //                         },
                //                     });
                //                 }
                //             }
                //         }
                //     });

                //     var filteredHotels = _.sortBy(hotelList, [function (o) {
                //         return o.price;
                //     }]);
                //     filteredHotelsWithPrice = filteredHotels.filter(hotel => (Number(hotel.Minimum_Number_of_Nights) != NaN && self.nights >= Number(hotel.Minimum_Number_of_Nights)));

                //     var hits = 0;
                //     var bookings = [];
                //     var hotelsWithAvailableRooms = [];
                //     var filterValue = "type='Hotel Rooms'";
                //     self.getDbDataTableValue(filterValue, "ingest_timestamp").then(function (response) {
                //         if (response != undefined && response.data != undefined && response.status == 200) {
                //             hits = response.data.count;
                //             bookings = response.data.data;
                //             if (hits > 0) {
                //                 filteredHotelsWithPrice.forEach(hotel => {
                //                     var available = bookings.find(ele => (hotel.Hotel_ID.toLowerCase() == ele.keyword1.toLowerCase()));
                //                     if (available) {
                //                         var availableRooms = Number(hotel.Total_Rooms) - Number(available.number1)
                //                         if (Number(availableRooms) >= Number(self.roomCount)) {
                //                             hotelsWithAvailableRooms.push(hotel)
                //                         }
                //                     } else {
                //                         hotelsWithAvailableRooms.push(hotel)
                //                     }
                //                 });
                //                 self.Hotels = hotelsWithAvailableRooms;
                //                 self.pageLoad = false;
                //             }
                //             // else {
                //             //     self.Hotels = filteredHotelsWithPrice;
                //             // }
                //         } else if (response == 404) {
                //             self.Hotels = filteredHotelsWithPrice;
                //             self.pageLoad = false;
                //         }
                //     }).catch(function (error) {
                //         console.log(error);
                //         self.pageLoad = false;
                //     });

                // } catch (error) {
                //     console.log('Error', error);
                //     self.pageLoad = false;
                // }
            }
        },
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#from1").datepicker({
                // minDate: "0d",
                // maxDate: "360d",
                minDate: new Date('11/20/2021'),
                maxDate: new Date('12/08/2021'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
            });

            $("#from2").datepicker({
                minDate: new Date('11/20/2021'),
                maxDate: new Date('12/08/2021'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $('#from1').datepicker('getDate', '+1d');
                    selectedDate.setDate(selectedDate.getDate() + 2);
                    $("#from2").datepicker("option", "minDate", selectedDate);
                }
            });
        },
        getLabels: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var Language = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.language = Language;
                var hotelcms = huburl + portno + commonPath + Agencycode + '/Template/Hotel Result Page/Hotel Result Page/Hotel Result Page.ftl';
                var labelsurl = huburl + portno + commonPath + Agencycode + '/Template/Home Page/Home Page/Home Page.ftl';
                // var cmsurl = huburl + portno + hotelcms;
                axios.get(hotelcms, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': Language
                    }
                }).then(function (res) {
                    console.log(res);

                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var banner = pluck('Banner_Section', self.content);
                        self.bannerSection = getAllMapData(banner[0].component);
                        var main = pluck('Main_Section', self.content);
                        self.labelsSection = getAllMapData(main[0].component);
                    }

                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });
                axios.get(labelsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': Language
                    }
                }).then(function (res) {
                    console.log(res);
                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var main = pluck('Index_Section', self.content);
                        self.labels = getAllMapData(main[0].component);
                        self.getPackage();
                        self.getOccupancy(false);
                    }

                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });

                var transferUrl = huburl + portno + commonPath + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';
                axios.get(transferUrl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': Language
                    }
                }).then(function (res) {
                    var packageData = res.data;
                    self.Packages = packageData.Values;
                    self.Packages.forEach(function (item, index) {
                        if (item.Location) {
                            self.LocationList.push(item.Location.toUpperCase().trim());
                        }
                    });
                    self.LocationList = _.uniq(_.sortBy(self.LocationList));
                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });

            });
        },
        // getWeather: async function () {
        //     var weatCity = this.city;
        //     axios.get('https://api.openweathermap.org/data/2.5/weather?lat=' + JSON.parse(window.sessionStorage.getItem("cityLocation")).lat + '&lon=' + JSON.parse(window.sessionStorage.getItem("cityLocation")).lon + '&units=metric&appid=7c0ff0999be50513159abbf786d93a0a', {
        //         headers: {
        //             'content-type': 'application/json'
        //         }
        //     }).then(function (response) {
        //         console.log(response);
        //         callback(response);
        //     });
        // },
    },
    computed: {
        noOfnights() {
            try {
                return this.nights > 1 ? this.nights + ' ' + this.labels.Nights_Label : this.nights == 1 ? '1 ' + this.labels.Nights_Label : '';
                // }
            } catch (error) {
                return ''
            }
        }
    },
    mounted: function () {
        // var historyPage = localStorage.getItem("backUrl") ? localStorage.getItem("backUrl") : null;
        // if (historyPage) {
        //     localStorage.removeItem("backUrl");
        //     window.location.href = historyPage;
        // }
        this.getLabels();
        this.getOccupancy(false);
        this.getPackage();
        this.setCalender();
        sessionStorage.active_e = 2;
        var vm = this;
        this.$nextTick(function () {
            $('#city').on("change", function (e) {
                vm.dropdownChange(e, 'city');
            });
            $('#from1').on("change", function (e) {
                vm.dropdownChange(e, 'from1')
            });
            $('#from2').on("change", function (e) {
                vm.dropdownChange(e, 'from2')
            });
            // $('#occupancy').on("change", function (e) {
            //     vm.dropdownChange(e, 'occupancy')
            // });
        })
    },
    watch: {
        rooms: function () {
            var self = this;
            var occupancySummary = 0;
            var withChild = false;
            for (var index = 0; index < self.rooms.length; index++) {
                if (self.rooms[index] == 3) {
                    withChild = true;
                    occupancySummary += 2;
                } else if (self.rooms[index] == 4){
                    occupancySummary += 3;
                }
                else {
                    occupancySummary += self.rooms[index];
                }
                
            }
            self.occupancySummary = occupancySummary + (occupancySummary > 1 ? " " + this.labels.Adults_Label : " " + this.labels.Adult_Label) + (withChild ? ", 1 " + this.labels.Child_Label : "");
        },
    }
});

jQuery(document).ready(function ($) {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}