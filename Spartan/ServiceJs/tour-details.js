const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var packageView = new Vue({
  i18n,
  el: '#Packagedetails',
  name: 'Packagedetails',
  data: {
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
    CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    maincontent: {
      Image: '',
      Title: '',
      Hours: '',
      Minutes: '',
      Description: '',
      Price: '',
      ChildPrice: ''
    },
    Inclusions: {},
    Terms: {},
    Gallery: {},
    pageURLLink: '',
    // Booking form
    name: '',
    email: '',
    phone: '',
    message: '',
    adult: '',
    child: 0,
    adulttotal: '',
    childtotal: '',
    grandtotal: '',
    searchCriteria: {},
    isBooking: false,
    headerArea:{},
    MainData:{}
  },
  mounted() {
    localStorage.removeItem("backUrl");
   this.getPageDetails();  
    this.getPackagedetails();
    this.setCalender();
    var vm = this;
    this.$nextTick(function () {
      $('#from').on("change", function (e) {
        vm.dropdownChange(e, 'from')
      });
    })
    sessionStorage.active_e = 4;
  },
  methods: {
    dropdownChange: function (event, type) {
      if (event != undefined && event.target != undefined && event.target.value != undefined) {
        if (type == 'from') {
          this.searchCriteria.date = event.target.value;
        }
      }
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
      getPackagedetails: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var packageurl = getQueryStringValue('page');
        var Language = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        if (packageurl != "") {
          packageurl = packageurl.split('-').join(' ');
          packageurl = packageurl.split('_')[0];
          var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
          var searchData = self.getSearchData();
          if (searchData != "" || searchData != null) {
            searchData.date = searchData.date.trim();
            searchData.adult = searchData.adult;
            self.searchCriteria = Object.assign({}, searchData);
            localStorage.setItem("backUrl", self.yourCallBackFunction(self.searchCriteria));
            self.pageURLLink = packageurl;
            axios.get(topackageurl, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': Language
              }
            }).then(function (response) {
              var maincontent = self.pluck('Information_Area', response.data.area_List);
              if (maincontent.length > 0) {
                self.maincontent.Title = self.pluckcom('Package_Title', maincontent[0].component);
                self.maincontent.Image = self.pluckcom('Package_Image', maincontent[0].component);
                self.maincontent.Hours = self.pluckcom('Hours', maincontent[0].component);
                self.maincontent.Minutes = self.pluckcom('Minutes', maincontent[0].component);
                self.maincontent.Description = self.pluckcom('Description', maincontent[0].component);
                self.maincontent.Price = self.pluckcom('Price_Of_Adult', maincontent[0].component);
                self.maincontent.ChildPrice = self.pluckcom('Price_Of_Child', maincontent[0].component);
              }

              var Inclusions = self.pluck('Inclusions_Area', response.data.area_List);
              if (Inclusions.length > 0) {
                self.Inclusions.Inclusion = self.pluckcom('Inclusion', Inclusions[0].component);
              }

              var Terms = self.pluck('Terms_And_Conditions_Area', response.data.area_List);
              if (Terms.length > 0) {
                self.Terms.Term = self.pluckcom('Terms_And_conditions', Terms[0].component);
              }
              var Gallery = self.pluck('Gallery_Area', response.data.area_List);
              if (Gallery.length > 0) {
                self.Gallery.Images = self.pluckcom('Images', Gallery[0].component);
              }
              self.calc();
            })
          }
        }
      });
    },
    getPageDetails() {
      var self=this;
      getAgencycode(function (response) {
          var Agencycode = response;
          var huburl = ServiceUrls.hubConnection.cmsUrl;
          var portno = ServiceUrls.hubConnection.ipAddress;
          var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
          var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Tour Details Page/Tour Details Page/Tour Details Page.ftl';
          var cmsurl = huburl + portno + homecms;
          axios.get(cmsurl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
          }).then(function (response) {
              self.content = response.data;
              var headerSection = pluck('Banner_Section', response.data.area_List);
              self.headerArea = getAllMapData(headerSection[0].component);
                var Main = pluck('Main_Section', response.data.area_List);
              self.MainData = getAllMapData(Main[0].component);
                  }).catch(function (error) {
              console.log('Error', error);
              this.content = [];
          });

      });

     },
    getSearchData: function () {
      var urldata = this.getUrlVars();
      // urldata = decodeURIComponent(urldata);
      return urldata;
    },
    getTemplate(template) {
      // var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
      var url = ServiceUrls.emailServices.getTemplate + "AGY75/" + template;
      // var url = ServiceUrls.emailServices.getTemplate + LoggedUser.loginNode.code + "/" + template;
      return axios.get(url);
    },
    getUrlVars: function () {
      var vars = [],
        hash;
      let decodeUrl= decodeURIComponent(window.location.href);
      var hashes = decodeUrl.slice(window.location.href.indexOf('?') + 1).split('&');
      for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars[hash[0]] = hash[1];
      }
      return vars;
    },
    sendbooking: function () {
      var self = this;
      let dateObj = document.getElementById("from");
      let dateValue = dateObj.value;
      if (!this.name) {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M032')).set('closable', false);
        return false;
      }
      if (!this.email) {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M009')).set('closable', false);
        return false;
      }
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.email.match(emailPat);
      if (matchArray == null) {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M019')).set('closable', false);
        return false;
      }
      if (!this.phone) {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M010')).set('closable', false);
        return false;
      }
      if (this.phone.length < 8) {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M046')).set('closable', false);
        return false;
      }
      if (dateValue == undefined || dateValue == '') {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M030')).set('closable', false);
        return false;
      }

      if (!this.message) {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M033')).set('closable', false);
        return false;
      } else {
        this.isBooking = true;
        var user = JSON.parse(localStorage.User);
        var fromEmail = user.loginNode.parentEmailId ? user.loginNode.parentEmailId : (user.loginNode.email ? user.loginNode.email : 'uaecrt@gmail.com') || 'uaecrt@gmail.com';
        var toEmail = user.loginNode.parentEmailId;
        var ccEmails = _.filter(user.loginNode.emailList, function (o) {
          return o.emailTypeId == 3 && o.emailType.toLowerCase() == 'support';
        });
        ccEmails = _.map(ccEmails, function (o) {
          return o.emailId
        });
        var agency = localStorage.getItem("AgencyCode");
        // var referenceNumber = agency + '-TOR' + moment(new Date()).format("YYMMDDhhmmssSSS");
        var referenceNumber = agency + '-';

        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        dateValue = moment(this.searchCriteria.date, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
          type: "Tour Booking",
          keyword1: this.name,
          keyword2: this.email,
          keyword3: this.maincontent.Title,
          keyword5: this.phone,
          // keyword10: '',
          date1: dateValue,
          number2: this.searchCriteria.adult,
          number3: this.child,
          text1: this.message,
          nodeCode: agencyCode,
          date2: requestedDate
        };
        self.cmsRequestData("POST", "cms/data", insertContactData, null).then(function (response) {
          let insertID = Number(response);
          referenceNumber = referenceNumber + insertID;
          var emailApi = ServiceUrls.emailServices.emailApi;
          var updateDate = {
            keyword10: referenceNumber
          }
          self.cmsFormUpdateData(updateDate, insertID).then(function (update) {
            if (update.status == 200) {
              try {
                var phoneNumber = JSON.parse(localStorage.User).loginNode.phoneList.filter(function (phones) {
                  return phones.type.toLowerCase().includes('telephone') || phones.type.toLowerCase().includes('mobile')
                });
                var postData = {
                  type: "PackageBookingRequest",
                  fromEmail: fromEmail,
                  toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                  ccEmails: ccEmails,
                  logo: JSON.parse(localStorage.User).loginNode.logo,
                  // logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                  agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                  agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                  packegeName: self.maincontent.Title,
                  personName: self.name,
                  emailAddress: self.email,
                  contact: self.phone,
                  departureDate: dateValue,
                  adults: self.searchCriteria.adult,
                  child2to5: self.child,
                  child6to11: "NA",
                  infants: "NA",
                  message: self.message + '<br>Tour Booking Reference Number: ' + referenceNumber,
                  primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                  secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                var emailContent = {
                  logoUrl: JSON.parse(localStorage.User).loginNode.logo,
                  bookingLabel: "Booking ID",
                  bookingReference: referenceNumber,
                  agencyEmail: fromEmail,
                  title: "Inquiry Received for Tour",
                  personName: self.name,
                  servicesData: [{
                      name: "Package Name",
                      value: self.maincontent.Title
                    },
                    {
                      name: "Guest Name",
                      value: self.name
                    },
                    {
                      name: "Email Address",
                      value: self.email
                    },
                    {
                      name: "Contact",
                      value: self.phone
                    },
                    {
                      name: "Package Start Date",
                      value: moment(dateValue).format("YYYY-MM-DD")
                    },
                    {
                      name: "Adults",
                      value: self.searchCriteria.adult
                    },
                    {
                      name: "Child",
                      value: self.child
                    },
                    {
                      name: "Infant",
                      value: "NA"
                    }
                  ],
                  emailMessage: "<p>Your inquiry is successfully received and is under booking ID&nbsp;" + referenceNumber + ".</p><p>One of our agent will get back to you shortly.</p>",
                  agencyPhone: phoneNumber[0].nodeContactNumber.number ? '+' + JSON.parse(localStorage.User).loginNode.country.telephonecode + "" + phoneNumber[0].nodeContactNumber.number : 'NA'
                };
                self.getTemplate("SpartanEmailTemplate").then(function (templateResponse) {
                  var data = templateResponse.data.data;
                  var emailTemplate = "";
                  if (data.length > 0) {
                    for (var x = 0; x < data.length; x++) {
                      if (data[x].enabled == true && data[x].type == "SpartanEmailTemplate") {
                        emailTemplate = data[x].content;
                        break;
                      }
                    }
                  };
                  var htmlGenerate = ServiceUrls.emailServices.htmlGenerate;
                  var emailData = {
                    template: emailTemplate,
                    content: emailContent
                  };
                  axios.post(htmlGenerate, emailData)
                    .then(function (htmlResponse) {
                      var emailPostData = {
                        type: "AttachmentRequest",
                        toEmails: Array.isArray(self.email) ? self.email : [self.email],
                        fromEmail: fromEmail,
                        ccEmails: null,
                        bccEmails: null,
                        subject: "Booking Confirmation - " + referenceNumber,
                        attachmentPath: "",
                        html: htmlResponse.data.data
                      };
                      var mailUrl = ServiceUrls.emailServices.emailApiWithAttachment;
                      sendMailServiceWithCallBack(mailUrl, emailPostData, function () {
                        sendMailServiceWithCallBack(emailApi, postData, function () {
                          self.isBooking = false;
                          self.name = '';
                          self.email = '';
                          self.phone = '';
                          self.message = '';
                          self.searchCriteria.date = "";
                          self.searchCriteria.adult = "1";
                          $('#from').val('');
                          self.child = "0";
                          self.calc();
                          alertify.alert(getValidationMsgByCode('M023'), getValidationMsgByCode('M022')).set('closable', false);
                        });
                      });
                    }).catch(function (error) {
                      console.log(error);
                    })
                })
              } catch (e) {
                console.log(e);
              }
            }
          }).catch(function (error) {
            self.isLoading = false;
            console.error(error);
            alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M045')).set('closable', false);
          });
        }).catch(function (error) {
          self.isLoading = false;
          console.error(error);
          alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M045')).set('closable', false);
        });

      }
    },
    cmsRequestData: async function (callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      return fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
        },
        body: data, // body data type must match "Content-Type" header
      }).then(function (response) {
        return response.json();
      });
    },
    cmsFormUpdateData: async function (data, id) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/cms/data/" + id;
      var callMethod = "PUT";
      if (data != null) {
        data = JSON.stringify(data);
      }
      try {
        let formPostRes = await axios({
          url: url,
          method: callMethod,
          headers: {
            'Content-Type': 'application/json',
          },
          data: data
        })
        return formPostRes;
      } catch (error) {
        console.log(error);
        return error.response.data.code;
      }
    },
    calc: function () {
      var vm = this;
      vm.adulttotal = vm.searchCriteria.adult * vm.maincontent.Price;
      vm.childtotal = vm.child * vm.maincontent.ChildPrice;
      vm.grandtotal = vm.adulttotal + vm.childtotal;
    },
    setCalender() {
      var dateFormat = "dd/mm/yy"
      $("#from").datepicker({
        minDate: "0d",
        maxDate: "360d",
        numberOfMonths: 1,
        changeMonth: true,
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>",
        duration: "fast",
        showAnim: "slide",
        showOptions: {
          direction: "up"
        },
        showButtonPanel: false,
        dateFormat: dateFormat,
      });
    },
    yourCallBackFunction: function (data) {
      if (data != null) {
        if (data != "") {
          var customSearch =
            "&city=" + data.city +
            "&date=" + data.date +
            "&adult=" + data.adult + "&";

          url = "/Spartan/tour-results.html?page=" + customSearch;
        } else {
          url = "#";
        }
      } else {
        url = "#";
      }
      return url;
    },

  }
})

function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}