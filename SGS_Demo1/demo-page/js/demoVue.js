
const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var holidypack = new Vue({
  i18n,
  el: '#holiday',
  name: 'holiday',
  data() {
    return {
      key: '',
      content: null,
      getdata: true,
      selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
      CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
      packages: [],
      packageList: [],
      pagebanner: {
        Book_Now_Label: "Book now",
        Day_Label: "Days",
        Night_Label: "Nights"
      },
      getpackage: false,
      allReview: [],
      isLoading: false,
      fullPage: true,
      visible: false,
      pkgSearch: "",

      currentPages: 1,
      fromPage: 1,
      totalpage: 1,
      constructedNumbers: [],
      package: [],
      pageLimit: 6,
      paginationLimit: 1,
      totalPkgs: '',
    }

  },
  mounted: function () {
    var searchTerm = getQueryStringValue('search');
    if (searchTerm) {
      this.pkgSearch = searchTerm;
    }
    this.viewReview();
    this.getpagecontent();
  },
  methods: {
    setTotalPackageCount: function () {
      if (this.packages != undefined && this.packages != undefined) {
        this.totalPkgs = Number(this.packages.length);
        this.totalpage = Math.ceil(this.totalPkgs / this.pageLimit);
        this.currentPage = 1;
        this.fromPage = 1;
        if (Number(this.totalPkgs) < 6) { }
        this.constructAllPagianationLink();
      }
    },
    constructAllPagianationLink: function () {
      let limit = this.paginationLimit;
      this.constructedNumbers = [];
      for (let i = Number(this.fromPage); i <= (Number(this.totalpage) + limit); i++) {
        if (i <= Number(this.totalpage)) {
          this.constructedNumbers.push(i);
        }
      }
      this.currentPage = this.constructedNumbers;
      this.setPackageItems();
    },
    prevNextEvent: function (type) {
      let limit = this.paginationLimit;
      if (type == 'Previous') {
        if (this.currentPages > this.totalpage) {
          this.currentPages = this.currentPages - 1;
        }
        this.fromPage = this.currentPages;
        if (Number(this.fromPage) != 1) {
          this.currentPages = Number(this.currentPages) - 1;
          this.setPackageItems();
        }

      } else if (type == 'Next') {
        if (this.currentPages == 'undefined' || this.currentPages == '') { }
        if (this.currentPages <= this.totalpage) {
          let limit = this.paginationLimit;
          this.fromPage = this.currentPages;
          var totalP = (this.totalpage) + 1;
          if (Number(this.fromPage) != totalP) {
            var count = this.currentPages + limit;
            if (Number(count) <= Number(this.totalpage)) {
              this.selectPacakges(this.currentPages);
            }

          }
        }
      }
    },
    selectPacakges: function (ev) {
      let limit = 1;
      this.currentPages = this.currentPage[ev];
      this.setPackageItems();
    },
    selected: function (ev) {
      let limit = 1;
      this.currentPages = this.currentPage[ev];
      this.setPackageItems();
    },
    setPackageItems: function () {
      if (this.packages != undefined && this.packages != undefined) {
        let start = 0;
        let end = Number(start) + Number(this.pageLimit);
        if (Number(this.currentPages) == 1) { } else {
          var limit = this.totalpage;
          start = (Number(this.currentPages) + Number(this.pageLimit)) - 2;
          end = Number(start) + Number(this.pageLimit);
        }
        this.package = this.packages.slice(start, end);
      }
    },

    packageListing: function (pkg) {
      this.showLoading();
      var self = this;
      self.packages = [];
      if (pkg) {
        var packageView = self.packageList.filter(function (el) {
          return el.Title.toLowerCase().includes(pkg.toLowerCase());
        });
        self.packages = packageView;
        carousel();
      } else {
        self.packages = self.packageList;
        carousel();
      }
      self.setTotalPackageCount();
      this.selectPacakges(0);

      // setTimeout(function () {
      //   carousel();
      //   console.log(1)
      // }, 2000);
    },
    showLoading() {
      var self = this;
      self.visible = true;
      setTimeout(function () {
        self.visible = false;
      }, 300);
    },
    moment: function () {
      return moment();
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getpagecontent: function () {
      this.isLoading = true;
      if (localStorage.getItem("AgencyCode") === null) {
        localStorage.AgencyCode = JSON.parse(localStorage.User).loginNode.code;

      }
      var Agencycode = localStorage.AgencyCode;
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
      var self = this;
      // var packagecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Holidays/Holidays/Holidays.ftl';
      var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Package List/Package List/Package List.ftl';
      var cmsurl = huburl + portno + homecms;
      // banner and labels
      var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Packages/Packages/Packages.ftl';
      axios.get(pageurl, {
        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
      }).then(function (response) {
        self.content = response.data;
        var pagecontent = self.pluck('Banner_Area', self.content.area_List);
        if (pagecontent.length > 0) {
          self.pagebanner.Breadcrumb1 = self.pluckcom('Breadcrumb1', pagecontent[0].component);
          self.pagebanner.Breadcrumb2 = self.pluckcom('Breadcrumb2', pagecontent[0].component);
          self.pagebanner.Banner_Title = self.pluckcom('Banner_Title', pagecontent[0].component);
          self.pagebanner.Banner_Image = self.pluckcom('Banner_Image', pagecontent[0].component);
          // self.pagebanner.Day_Label = self.pluckcom('Day_Label', pagecontent[0].component);
          // self.pagebanner.Night_Label = self.pluckcom('Night_Label', pagecontent[0].component);

        }
        var pagecontentsLabel = self.pluck('Labels', self.content.area_List);
        if (pagecontentsLabel.length > 0) {
          self.pagebanner.Book_Now_Label = self.pluckcom('Book_Now_Label', pagecontentsLabel[0].component);
          self.pagebanner.Day_Label = self.pluckcom('Day_Label', pagecontentsLabel[0].component);
          self.pagebanner.Night_Label = self.pluckcom('Night_Label', pagecontentsLabel[0].component);
          self.pagebanner.Search_Label = self.pluckcom('Search_Label', pagecontentsLabel[0].component);
          self.pagebanner.Search_Placeholder = self.pluckcom('Search_Placeholder', pagecontentsLabel[0].component);
        }


      }).catch(function (error) {
        console.log('Error');
        self.content = [];
      });


      axios.get(cmsurl, {
        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
      }).then(function (response) {
        self.isLoading = true;
        self.content = response.data;
        let holidayPackageListTemp = [];
        if (self.content != undefined && self.content.Values != undefined) {
          holidayPackageListTemp = self.content.Values.filter(function (el) {
            return el.Status == true
          });
        }
        self.packageList = holidayPackageListTemp;
        if (self.pkgSearch) {
          var packageView = self.packageList.filter(function (el) {
            return el.Title.toLowerCase().includes(self.pkgSearch.toLowerCase());
          });
          self.packages = packageView;
          carousel();
        } else {
          self.packages = holidayPackageListTemp;
          carousel();
        }
        self.getpackage = true;
        self.isLoading = false;
        self.setTotalPackageCount();
      }).catch(function (error) {
        console.log('Error');
        self.content = [];
      });

    },
    getmoreinfo(url) {
      if (url != null) {
        if (url != "") {
          url = url.split("/Template/")[1];
          url = url.split(' ').join('-');
          url = url.split('.').slice(0, -1).join('.')
          url = "/SGS_Demo1/demo-page/holidaydetails.html?page=" + url;

          console.log(this.url);
          window.location.href = url;
        }
        else {
          url = "#";
        }
      }
      else {
        url = "#";
      }
      console.log(this.url);
      return url;

    },
    viewReview: async function () {
      var self = this;
      self.isLoading = true;
      self.isLoading = true;
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
      let requestObject = { from: 0, size: 100, type: "Reviews", nodeCode: agencyCode, orderBy: "desc" };
      let responseObject = await this.cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
        if (responseObject != undefined && responseObject.data != undefined) {
          self.allReview = JSON.parse(JSON.stringify(responseObject)).data;

        }
        self.isLoading = false;
      });
    },
    async cmsRequestData(callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: { 'Content-Type': 'application/json' },
        body: data, // body data type must match "Content-Type" header
      });
      try {
        const myJson = await response.json();
        return myJson;
      } catch (error) {
        return object;
      }
    },
    getrating: function (url) {
      var self = this;
      url = url.split("/Template/")[1];
      url = url.split(' ').join('-');
      url = url.split('.').slice(0, -1).join('.');
      url = url.split('-').join(' ');
      url = url.split('_')[0];
      let reviewrate = [];
      reviewrate = self.allReview;
      reviewrate = reviewrate.filter(r => r.keyword4 == url)
      var sum = 0;
      if (reviewrate.length > 0) {
        $.each(reviewrate, function () {
          sum += this.number1 ? this.number1 : 0;
        })
        sum = sum / reviewrate.length;
      }
      return sum;
    },
  },
  filters: {
    moment: function (date) {
      return moment(date).format('DD MMM YYYY');
    }
  }
});


function carousel() {
  // get owl element
  var owl = $("#owl-demo-3");
  // get owl instance from element
  var owlInstance = owl.data("owlCarousel");
  // if instance is existing
  if (owlInstance != null) {
    owlInstance.reinit();
  } else {
    owl.owlCarousel({
      navigation: true, // Show next and prev buttons
      slideSpeed: 2000,
      pagination: false,
      paginationSpeed: 2000,
      singleItem: false,
      dots: true,
      mouseDrag: true,
      items: 4,
      transitionStyle: "goDown",
      itemsCustom: [
        [0, 1],
        [450, 1],
        [600, 2],
        [700, 2],
        [1000, 3],
        [1200, 3]
      ],
    });
  }
}

function owlcarosl() {

  $("#homepackage").owlCarousel({
    navigation: true, // Show next and prev buttons
    slideSpeed: 2000,
    pagination: false,
    paginationSpeed: 2000,
    singleItem: false,
    dots: true,
    mouseDrag: true,
    items: 4,
    transitionStyle: "goDown",
    itemsCustom: [
      [0, 1],
      [450, 1],
      [600, 2],
      [700, 2],
      [1000, 3],
      [1200, 3]
    ],
  });
}

var count = 0;
function myFun(f) {

  count += 1;

  f.myText.value = count;

}
var header = new Vue({
  el: 'head',
  name: 'header',

});
function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}