
const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
  })
  var privacy = new Vue({
    i18n,
    el: '#privacy',
    name: 'privacy',
    data() {
      return {
        key: '',
        content: null,
        getdata: true,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        packages: '',
        pagebanner:{
            Breadcrumb1:'',
            Privacy_Content:''
        },
        getpackage: false,
        allReview: [],
        pagecontents:{Privacy_Policy:''},
        isLoading: false,
      fullPage: true,
      }
  
    },
    mounted: function () {
      this.getpagecontent();
    },
    methods: {
      moment: function () {
        return moment();
      },
      pluck(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
          if (item[key] != undefined) {
            Temparry.push(item[key]);
          }
        });
        return Temparry;
      },
      pluckcom(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
          if (item[key] != undefined) {
            Temparry = item[key];
          }
        });
        return Temparry;
      },
      getpagecontent: function () {
        if (localStorage.getItem("AgencyCode") === null) {
          localStorage.AgencyCode = JSON.parse(localStorage.User).loginNode.code;
  
        }
        var Agencycode = localStorage.AgencyCode;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        var self = this;
        self.isLoading = true;
        // banner and labels
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Privacy Policy/Privacy Policy/Privacy Policy.ftl';
            axios.get(pageurl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
              self.content = response.data;
              var pagecontent = self.pluck('Banner_Area', self.content.area_List);
              if (pagecontent.length > 0) {
                self.pagebanner.Breadcrumb1 = self.pluckcom('Breadcrumb1', pagecontent[0].component);
                self.pagebanner.Breadcrumb2 = self.pluckcom('Breadcrumb2', pagecontent[0].component);
                self.pagebanner.Banner_Title = self.pluckcom('Banner_Title', pagecontent[0].component);
                self.pagebanner.Banner_Image = self.pluckcom('Banner_Image', pagecontent[0].component);
              }
              
  
            }).catch(function (error) {
              console.log('Error');
              self.content = [];
            });
        
        axios.get(pageurl, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          var pagecontents = self.pluck('Main_Area', self.content.area_List);
              if (pagecontents.length > 0) {
                self.pagecontents.Privacy_Policy = self.pluckcom('Privacy_Policy', pagecontents[0].component);
              }
  
              self.isLoading = false;
  
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
      },
      
     
    },
    filters: {
      moment: function (date) {
        return moment(date).format('DD MMM YYYY');
      }
    }
  
  });
  function owlcarosl() {
  
    $("#homepackage").owlCarousel({
      navigation: true, // Show next and prev buttons
      slideSpeed: 2000,
      pagination: false,
      paginationSpeed: 2000,
      singleItem: false,
      dots: true,
      mouseDrag: true,
      items: 4,
      transitionStyle: "goDown",
      itemsCustom: [
        [0, 1],
        [450, 1],
        [600, 2],
        [700, 2],
        [1000, 3],
        [1200, 3]
      ],
    });
  }
  
  
  var header = new Vue({
    el: 'head',
    name: 'header',
  
  });