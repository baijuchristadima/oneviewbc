var generalInformation = {
    systemSettings: {
        systemDateFormat: 'dd M y,D',
        calendarDisplay: 1,
        calendarDisplayInMobile: 1
    },
}
var cmsapp = new Vue({
    el: '#cmsdiv',
    name: 'cms',
    data() {
        return {
            key: '',
            content: null,
            getdata: true,
            dir: 'ltr',
            ipAddres: '',
            sessionids: '',
            Socialmedia: {},
            isLoading: false,
            fullPage: true,
        }
    },
    mounted: function () {
        // this.setip();
        this.getcmsdata();
    },
    methods: {
        moment: function () {
            return moment();
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getcmsdata: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Home/Home/Home.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    var backg = self.pluck('main', self.content.area_List);
                    var background = self.pluckcom('Banner', backg[0].component);
                    setTimeout(function () {
                        owlcarosl(background);
                    }, 100);
                    var Socialmedia = self.pluck('Social_Media_Aggregation', self.content.area_List);
                    self.Socialmedia.Title = self.pluckcom('Title', Socialmedia[0].component);
                    self.Socialmedia.Icon = self.pluckcom('Icon', Socialmedia[0].component);
                    self.isLoading = false;
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                    self.isLoading = false;
                });
            });
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json'
                },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        getPackages(url) {
            if (url != "") {
                url = '/SGS_Demo1/packages.html?search=' + url;
                window.location.href = url;
            }
            else {
                url = "#";
            }
            return url;
        },
    },
    filters: {
        moment: function (date) {
            return moment(date).format('DD MMM YYYY');
        }
    }

});

var holidaysSec = new Vue({
    el: '#holidays',
    name: 'holidaysSec',
    data() {
        return {
            pkgSearch: ''
        }
    },
    methods: {
        packageListing(name) {
            holidypack.packageListing(name);
        },
    },
});

function owlcarosl(imageUrl) {
    $('#main_banner').css('background-image', 'url(' + imageUrl + ')');
    $("#owl-demo-2").owlCarousel({
        items: 3,
        lazyLoad: true,
        loop: true,
        margin: 30,
        navigation: true,
        itemsDesktop: [991, 2],
        itemsDesktopSmall: [979, 2],
        itemsTablet: [768, 2],
        itemsMobile: [640, 1],
    });
    $(".owl-prev").html('<i class="fa fa-angle-left"></i>');
    $(".owl-next").html('<i class="fa fa-angle-right"></i>');
}
function carousel() {
    // get owl element
    var owl = $("#owl-demo-3");
    // get owl instance from element
    var owlInstance = owl.data("owlCarousel");
    // if instance is existing
    if (owlInstance != null) {
        owlInstance.reinit();
    } else {
        owl.owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 2000,
            pagination: false,
            paginationSpeed: 2000,
            singleItem: false,
            dots: true,
            mouseDrag: true,
            items: 4,
            transitionStyle: "goDown",
            itemsCustom: [
                [0, 1],
                [450, 1],
                [600, 2],
                [700, 2],
                [1000, 3],
                [1200, 3]
            ],
        });
    }
}

$(document).ready(function () {
    var active_el = (sessionStorage.active_el) ? sessionStorage.active_el : 3;
    if (active_el == 1) {
        $('.nav-tabs a[href="#flights"]').tab('show');
    } else if (active_el == 2) {
        $('.nav-tabs a[href="#hotels"]').tab('show');
    } else {
        $('.nav-tabs a[href="#holidays"]').tab('show');
    }

});