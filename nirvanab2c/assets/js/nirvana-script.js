$( function() {
    $( "#eventDate" ).datepicker({
      minDate: 0
    });
  } );
 
  $( document ).ready(function() {
       var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
}

if(mm<10) {
    mm='0'+mm
}

today = 'DEPART ON';
   $('#eventDate').val(today);
});


$( function() {
    $( "#eventDate2" ).datepicker({
      minDate: 0
    });
  } );
 
  $( document ).ready(function() {
       var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
}

if(mm<10) {
    mm='0'+mm
}

today = 'RETURN ON';
   $('#eventDate2').val(today);
});

$( function() {
    $( "#eventDate3" ).datepicker({
      minDate: 0
    });
  } );
 
  $( document ).ready(function() {
       var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
}

if(mm<10) {
    mm='0'+mm
}

today = 'DEPART ON';
   $('#eventDate3').val(today);
});


$( function() {
    $( "#eventDate4" ).datepicker({
      minDate: 0
    });
  } );
 
  $( document ).ready(function() {
       var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
}

if(mm<10) {
    mm='0'+mm
}

today = 'DEPART ON';
   $('#eventDate4').val(today);
});


$( function() {
    $( "#eventDate5" ).datepicker({
      minDate: 0
    });
  } );
 
  $( document ).ready(function() {
       var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
}

if(mm<10) {
    mm='0'+mm
}

today = 'DEPART ON';
   $('#eventDate5').val(today);
});


$( function() {
    $( "#eventDate6" ).datepicker({
      minDate: 0
    });
  } );
 
  $( document ).ready(function() {
       var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
}

if(mm<10) {
    mm='0'+mm
}

today = 'CHECK IN';
   $('#eventDate6').val(today);
});


$( function() {
    $( "#eventDate7" ).datepicker({
      minDate: 0
    });
  } );
 
  $( document ).ready(function() {
       var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
}

if(mm<10) {
    mm='0'+mm
}

today = 'CHECK OUT';
   $('#eventDate7').val(today);
});

      $('#owl-carousel').owlCarousel({
      loop: true,
      margin: 30,
      dots: true,
      nav: true,
      items: 3,
      navText: [
      '<img style="width: 28px; height: auto;" src="assets/images/icon/left-arrow.svg">',
      '<img style="width: 28px; height: auto;" src="assets/images/icon/right-arrow.svg">'
      ],
      navContainer: '.custom-nav',
      responsive:{
          0:{
              items: 1
          },
          600:{
              items: 3
          },
          1000:{
              items: 3
          }
      }
      })
      
      $('#owl-carousel-2').owlCarousel({
      loop: true,
      margin: 30,
      dots: true,
      nav: false,
      items: 2,
      responsive:{
          0:{
              items: 1
          },
          600:{
              items: 2
          },
          1000:{
              items: 2
          }
      }
      })
      
      $('#owl-carousel-3').owlCarousel({
      loop: true,
      margin: 30,
      dots: false,
      nav: true,
      items: 3,
      navText: [
      '<i class="las la-angle-left"></i>',
      '<i class="las la-angle-right"></i>'
      ],
      navContainer: '.custom-nav2',
      responsive:{
          0:{
              items: 1
          },
          600:{
              items: 3
          },
          1000:{
              items: 3
          }
      }
      })
      
      var numberSpinner = (function() {
        $('.number-spinner>.ns-btn>a').click(function() {
          var btn = $(this),
            oldValue = btn.closest('.number-spinner').find('input').val().trim(),
            newVal = 0;
      
          if (btn.attr('data-dir') === 'up') {
            newVal = parseInt(oldValue) + 1;
          } else {
            if (oldValue > 1) {
              newVal = parseInt(oldValue) - 1;
            } else {
              newVal = 1;
            }
          }
          btn.closest('.number-spinner').find('input').val(newVal);
        });
        $('.number-spinner>input').keypress(function(evt) {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
          }
          return true;
        });
      })();

      