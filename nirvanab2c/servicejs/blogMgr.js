function gettingRestClientObject() {
    var restObject = new Vue({
        data: {
            direction:'ltr'
        },
        methods: {
            async constructInstaImages(jsonObject){
                var tempInstaListData=[];
                if(jsonObject!=undefined&& jsonObject!=undefined&&jsonObject.entry_data!=undefined&&jsonObject.entry_data.ProfilePage!=undefined){
                    for(let i=0;i<jsonObject.entry_data.ProfilePage.length;i++){
                        let profilePage=jsonObject.entry_data.ProfilePage[i];
                        if(profilePage.graphql!=undefined&&profilePage.graphql.user!=undefined&&
                            profilePage.graphql.user.edge_owner_to_timeline_media!=undefined && 
                            profilePage.graphql.user.edge_owner_to_timeline_media.edges!=undefined){
                                let edges=profilePage.graphql.user.edge_owner_to_timeline_media.edges;
                                for(let k=0;k<edges.length;k++){
                                    let postObject=edges[k];
                                    if(postObject.node!=undefined){
                                        let thumbnailResourse=postObject.node.thumbnail_resources;
                                        let isVideo=postObject.node.is_video;
                                        if(!isVideo&&thumbnailResourse!=undefined&&thumbnailResourse.length>0){
                                            let src=thumbnailResourse[0].src;
                                            tempInstaListData.push({Image:src});
                                        }
                                    }
                                }
                        }
                    }
                }
                return tempInstaListData;
            },
            pluck(key, contentArry) {
                var Temparry = [];
                contentArry.map(function (item) {
                    if (item[key] != undefined) {
                        Temparry.push(item[key]);
                    }
                });
                return Temparry;
            },
            pluckcom(key, contentArry) {
                var Temparry = [];
                contentArry.map(function (item) {
                    if (item[key] != undefined) {
                        Temparry = item[key];
                    }
                });
                return Temparry;
            },
            async constructNumeber(data){
                var number=0;
                if(data!=undefined){
                    number = data.replace( /^\D+/g, '');
                    if(number==''){
                        number=0;
                    }
                }
                return number;
            },
            async checkListByID(mapList,id){
                var oldObject=undefined;
                for(let i=0;i<mapList.length;i++){
                    if((mapList[i].id!=undefined&& mapList[i].id==id) || (mapList[i].Id!=undefined&&mapList[i].Id==id)){
                        oldObject=mapList[i];
                        break;
                    }
                }
                return oldObject;
            },async contstrutcTags(tags){
                var tagStr="";
                if(tags!=undefined){
                    for(let m=0;m<tags.length;m++){
                        if(tagStr==''){
                            tagStr=tags[m];
                        }else{
                            tagStr=tagStr+", "+tags[m];
                        }
                    }
                }
                return tagStr;
                
            },async consturctSocialMedia(socialMediaItems){
                var allItems=[];
                if(socialMediaItems!=undefined){
                    for(let m=0;m<socialMediaItems.length;m++){
                        let socialObjec={icon:socialMediaItems[m].Icon,url:socialMediaItems[m].Url,name:socialMediaItems[m].Name};
                        allItems.push(socialObjec);
                    }
                }
                return allItems;
            },
            async getAllMapData(contentArry){
                var tempDataObject = {};
                contentArry.map(function (item) {
                    let allKeys=Object.keys(item)
                    for(let j=0;j<allKeys.length;j++){
                        let key=allKeys[j];
                        let value= item[key];
                        
                        if(key!='name' && key!='type'){
                            if(value==undefined||value==null){
                                value="";
                            }
                            tempDataObject[key]=value;
                        }
                    }
                });
                return tempDataObject;
            },
            async getBLogDbData(allDBData,blogID){
                var blogObj=undefined;
                for(let i=0;i<allDBData.length;i++){
                    if(allDBData[i].keyword1==blogID){
                        blogObj=allDBData[i];
                        break;
                    }
                }
                return blogObj;
            },async getDbData4Blogs(agencyCode,extraFilter){
                var allDBData=[];
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var cmsURL = huburl + portno + '/cms/data/search/byQuery';
                var queryStr="select * from cms_forms_data where nodeCode = '"+agencyCode+"' AND type='Blog Details'";
                if(extraFilter!=undefined&&extraFilter!=''){
                    queryStr=queryStr+" AND "+extraFilter;
                }
                var requestObject={
                    query:queryStr,
                    sortField:"date1",
                    from:0,
                    orderBy:"desc"
                };
                let responseObject =await this.gettingCMSData(cmsURL,"POST",requestObject,"en","");
                if( responseObject !=undefined && responseObject.data!=undefined){
                    allDBData=responseObject.data;
                }
                return allDBData;

            },async getDbData4Table(agencyCode,extraFilter,sortField){
                var allDBData=[];
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var cmsURL = huburl + portno + '/cms/data/search/byQuery';
                var queryStr="select * from cms_forms_data where nodeCode = '"+agencyCode+"'";
                if(extraFilter!=undefined&&extraFilter!=''){
                    queryStr=queryStr+" AND "+extraFilter;
                }
                var requestObject={
                    query:queryStr,
                    sortField:sortField,
                    from:0,
                    orderBy:"desc"
                };
                let responseObject =await this.gettingCMSData(cmsURL,"POST",requestObject,"en","");
                if( responseObject !=undefined && responseObject.data!=undefined){
                    allDBData=responseObject.data;
                }
                return allDBData;

            },
            async getBlogDetailsByQuery(agencyCode){
                var allDBData=[];
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var cmsURL = huburl + portno + '/cms/data/search';
    
                var requestObject={
                    type:"Blog Details",
                    nodeCode:agencyCode,
                    from:0,
                    size:1,
                    orderBy:"asc"
                };
                let responseObject =await this.gettingCMSData(cmsURL,"POST",requestObject,"en","");
                if( responseObject !=undefined && responseObject.count!=undefined){
                    requestObject.size=responseObject.count;
                    responseObject =await this.gettingCMSData(cmsURL,"POST",requestObject,"en","");
                    if( responseObject !=undefined && responseObject.count!=undefined){
                        allDBData=responseObject.data;
                    }
                }
                return allDBData;
            },
            async gettingCMSData(cmsURL,methodName,bodyData,languageCode,type){
                var data=bodyData;
                if (data != null &&data != undefined) {
                    data = JSON.stringify(data);
                }
                const response = await fetch(cmsURL,
                    {
                        method: methodName, // *GET, POST, PUT, DELETE, etc.
                        credentials: "same-origin", // include, *same-origin, omit
                        headers: { 'Content-Type': 'application/json', 'Accept-Language': languageCode },
                        body: data, // body data type must match "Content-Type" header
                    }
                );
                try {
                    const myJson = await response.json();
                    return myJson;
                } catch (error) {
                    let object = { area_List: [] }
                    return object;
                }
    
            },async updateViewCount(viewRedirectObject){

                if( (window.sessionStorage.getItem("Added")==undefined||window.sessionStorage.getItem("Added")=='false')&&viewRedirectObject!=undefined&&viewRedirectObject.Blog!=undefined&&viewRedirectObject.Blog.Id!=undefined){

                    var blogItem=viewRedirectObject.Blog;
                    var agencyCode=viewRedirectObject.Agecny;
                    var filterValue="keyword1='"+blogItem.Id+"'";
                    var allDBData=await this.getDbData4Blogs(agencyCode,filterValue);
                    var blogDBobject=undefined;
                    if(allDBData.length>0){
                        blogDBobject=allDBData[0];
                    }
                    let createdDate=await moment(blogItem.Created_Date).format('YYYY-MM-DDThh:mm:ss');
                    let lastUpdateDate=await moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    var likes=1;
                    var views=1;
                    var comments=1;
                    var huburl = ServiceUrls.hubConnection.cmsUrl;
                    var portno = ServiceUrls.hubConnection.ipAddress;
                    var cmsURL = huburl + portno + '/cms/data';
                    var apiMethod="POST";
    
                    if(blogDBobject!=undefined){
                        likes=blogDBobject.number1;
                        views=Number(blogDBobject.number2)+1;
                        comments=blogDBobject.number3;
                        apiMethod="PUT";
                        cmsURL=cmsURL+"/"+blogDBobject.id;
                    }
    
                    var insertRequestObject={ 
                        type:"Blog Details", 
                        nodeCode:agencyCode,
                        keyword1:blogItem.Id,
                        keyword2:blogItem.Title,
                        keyword3:blogItem.Category_Id,
                        keyword4:blogItem.Category_Name,
                        number1:likes,
                        number2:views,
                        number3:comments,
                        date1:createdDate,
                        date2:lastUpdateDate
                    };
                    var responseObject =await this.gettingCMSData(cmsURL,apiMethod,insertRequestObject,"en","");
                    window.sessionStorage.removeItem("View_Object");
                    window.sessionStorage.setItem("Added","true");
                    var searchParams = new URLSearchParams(window.location.search);
                    searchParams.set("Added", "true");
                    //window.location.search = searchParams.toString();
                    
                }


            },async updatePackageViewCount(requestObject,agencyCode){
                var sessionVar= window.sessionStorage.getItem("Package_View");

                if( (sessionVar==undefined||sessionVar!=requestObject.Reference_No)&&requestObject!=undefined&&requestObject.Reference_No!=undefined&&requestObject.Reference_No!=''){

                    var filterValue="type='Package Details' AND keyword1='"+requestObject.Reference_No+"'";
                    var allDBData=await this.getDbData4Table(agencyCode,filterValue,"date3");
                    var blogDBobject=undefined;
                    if(allDBData!=undefined&&allDBData.length>0){
                        blogDBobject=allDBData[0];
                    }
                    let fromDate=await moment(requestObject.From_Date).format('YYYY-MM-DDThh:mm:ss');
                    let toDate=await moment(requestObject.To_Date).format('YYYY-MM-DDThh:mm:ss');
                    let lastUpdateDate=await moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    var likes=1;
                    var views=1;
                    var comments=1;
                    var huburl = ServiceUrls.hubConnection.cmsUrl;
                    var portno = ServiceUrls.hubConnection.ipAddress;
                    var cmsURL = huburl + portno + '/cms/data';
                    var apiMethod="POST";
    
                    if(blogDBobject!=undefined){
                        likes=blogDBobject.number1;
                        views=Number(blogDBobject.number2)+1;
                        comments=blogDBobject.number3;
                        apiMethod="PUT";
                        cmsURL=cmsURL+"/"+blogDBobject.id;
                    }
    
                    var insertRequestObject={ 
                        type:"Package Details", 
                        nodeCode:agencyCode,
                        keyword1:requestObject.Reference_No,
                        keyword2:requestObject.Name,
                        keyword3:requestObject.City,
                        keyword4:requestObject.Destination,
                        keyword5:requestObject.Price,
                        keyword6:requestObject.Days,
                        keyword7:requestObject.Nights,
                        number1:likes,
                        number2:views,
                        number3:comments,
                        date1:fromDate,
                        date2:toDate,
                        date3:lastUpdateDate
                    };
                    var responseObject =await this.gettingCMSData(cmsURL,apiMethod,insertRequestObject,"en","");
                    window.sessionStorage.setItem("Package_View",requestObject.Reference_No);
                    
                }


            },
            async redirectBlogPage(blogItem,agencyCode){
                if(blogItem!=undefined&&blogItem.Id!=undefined&&blogItem.More_details_page_link!=undefined){
                    var url = "/Nirvana/blog-detail.html?ID=" + blogItem.Id + "&Added=false";
                    var viewRedirectObject={Blog:blogItem,Agecny:agencyCode};
                    window.sessionStorage.setItem('blog_page_Link', blogItem.More_details_page_link);
                    window.sessionStorage.setItem('View_Object', JSON.stringify(viewRedirectObject));
                    window.sessionStorage.setItem("Added","false");
                    window.location.href=url;
                }
            },
            async getQueryStringValue(key) {
                return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
            },async rowObjectConsturc(rowObject){
                var tagStr=await this.contstrutcTags(rowObject.Tags);
                rowObject.tagStr=tagStr;
                rowObject.dayNumber=moment(rowObject.Created_Date).format('DD');
                rowObject.monthStr=moment(rowObject.Created_Date).format('MMM');
                rowObject.comments="1";
                return rowObject;
            },async constructPath(pageLinkValue){
               
                var consPath="";
                if(pageLinkValue!=undefined){
                    var pageModifier=pageLinkValue.split('/');
                    for(let n=0;n<pageModifier.length;n++){
                        var folderName=pageModifier[n];
                        if((n+1)==pageModifier.length){
                            
                            folderName=folderName.split('_')[0].trim()+".ftl";
                        }
                        if(consPath==''){
                            consPath=folderName;
                        }else{
                            consPath=consPath+"/"+folderName;
                        }
                    }
                }
                return consPath;
                
            }, async callPackageInfoPage(url,type){
                if (url != null) {
                    if (url != "") {
                        url = url.split("/Template/")[1];
                        url = url.split(' ').join('-');
                        url = url.split('.').slice(0, -1).join('.');
                        url=url.split('/')[url.split('/').length-1];
                        url = "/Nirvana/packageInfo.html?page=" + url + "&from="+type;
                    }
                    else {
                        url = "#";
                    }
                }
                else {
                    url = "#";
                }
                return url;
            }, async callGalleryInfoPage(url){
                if (url != null) {
                    if (url != "") {
                        url = url.split("/Template/")[1];
                        url = url.split(' ').join('-');
                        url = url.split('.').slice(0, -1).join('.');
                        url=url.split('/')[url.split('/').length-1];
                        url = "/Nirvana/gallery-info.html?page=" + url;
                    }
                    else {
                        url = "#";
                    }
                }
                else {
                    url = "#";
                }
                return url;
            }, async callCSRInfoPage(url){
                if (url != null) {
                    if (url != "") {
                        url = url.split("/Template/")[1];
                        //url = url.replace('-','=');
                        //url = url.split(' ').join('-');
                        url = url.split('.').slice(0, -1).join('.');
                        url=url.split('/')[url.split('/').length-1];
                        url = "/Nirvana/csr-detail.html?page=" + url;
                    }
                    else {
                        url = "#";
                    }
                }
                else {
                    url = "#";
                }
                return url;
            }
        },
        mounted: function () {
            
        },
    });

    return restObject;

}


