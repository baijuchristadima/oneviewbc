$(document).ready(function () {

    if (localStorage.direction == 'rtl') {
        $(".ar_direction").addClass("ar_direction1");
    } else {
        $(".ar_direction").removeClass("ar_direction1");
    }

});
generalInformation = {
    systemSettings: {
        calendarDisplay: 1,

    },
};
const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var flightserchfromComponent = Vue.component('flightsearch', {
    data() {
        return {
            access_token: '',
            KeywordSearch: '',
            resultItemsarr: [],
            autoCompleteProgress: false,
            highlightIndex: 0
        }
    },
    props: {
        itemText: String,
        itemId: String,
        placeHolderText: String,
        returnValue: Boolean,
        id: { type: String, default: '', required: false },
        leg: Number,
        //KeywordSearch:String
    },

    template: `<div class="autocomplete">
        <input type="text" :placeholder="placeHolderText" :id="id" :data-aircode="KeywordSearch" autocomplete="off"
        v-model="KeywordSearch" class="loc-icon form-control"
        :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !autoCompleteProgress  }"
        @input="onSelectedAutoCompleteEvent(KeywordSearch)"
            @keydown.down="down"
            @keydown.up="up"
            @keydown.tab="autoCompleteProgress=false"
            @keydown.esc="autoCompleteProgress=false"
            @keydown.enter="onSelected(resultItemsarr[highlightIndex])"/>
        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
        <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length>0">
            <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item)">
                {{ item.label }}
            </li>
        </ul>
    </div>`,


    methods: {
        up: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex < this.resultItemsarr.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
            if (this.$refs.options[this.highlightIndex]) {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            }

        },
        onSelectedAutoCompleteEvent: _.debounce(async function (keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
              this.autoCompleteProgress = true;
              self.resultItemsarr = await getFlightResultUsingElasticSearch(keywordEntered);
            } else {
                this.autoCompleteProgress = false;
                this.resultItemsarr = [];

            }
        }, 100),
        onSelected: function (item) {
            this.KeywordSearch = item.label;
            this.resultItemsarr = [];
            var targetWhenClicked = $(event.target).parent().parent().find("input").attr("id");
            if (maininstance.triptype != 'M') {
                if (event.target.id == "Departurefrom" || targetWhenClicked == "Departurefrom") {
                    $('#DepartureTo').focus();
                } else if (event.target.id == "DepartureTo" || targetWhenClicked == "DepartureTo") {
                    $('#deptDate01').focus();
                }
            } else {
                if (event.target.id == "Departurefrom" || targetWhenClicked == "Departurefrom") {
                    $('#DepartureTo').focus();
                } else if (event.target.id == "DepartureTo" || targetWhenClicked == "DepartureTo") {
                    $('#deptDate01').focus();
                } else if (event.target.id == "DeparturefromLeg2" || targetWhenClicked == "DeparturefromLeg2") {
                    $('#ArrivalfromLeg2').focus();
                } else if (event.target.id == "ArrivalfromLeg2" || targetWhenClicked == "ArrivalfromLeg2") {
                    $('#txtLeg2Date').focus();
                } else if (event.target.id == "DeparturefromLeg3" || targetWhenClicked == "DeparturefromLeg3") {
                    $('#ArrivalfromLeg3').focus();
                } else if (event.target.id == "ArrivalfromLeg3" || targetWhenClicked == "ArrivalfromLeg3") {
                    $('#txtLeg3Date').focus();
                } else if (event.target.id == "DeparturefromLeg4" || targetWhenClicked == "DeparturefromLeg4") {
                    $('#ArrivalfromLeg4').focus();
                } else if (event.target.id == "ArrivalfromLeg4" || targetWhenClicked == "ArrivalfromLeg4") {
                    $('#txtLeg4Date').focus();
                } else if (event.target.id == "DeparturefromLeg5" || targetWhenClicked == "DeparturefromLeg5") {
                    $('#ArrivalfromLeg5').focus();
                } else if (event.target.id == "ArrivalfromLeg5" || targetWhenClicked == "ArrivalfromLeg5") {
                    $('#txtLeg5Date').focus();
                } else if (event.target.id == "DeparturefromLeg6" || targetWhenClicked == "DeparturefromLeg6") {
                    $('#ArrivalfromLeg6').focus();
                } else if (event.target.id == "ArrivalfromLeg6" || targetWhenClicked == "ArrivalfromLeg6") {
                    $('#txtLeg6Date').focus();
                }

            }

            this.$emit('air-search-completed', item.code, item.label, this.leg);


        }
    }


});

var maininstance = new Vue({
    i18n,
    el: '#indexmain',
    name: 'indexmain',
    data: {
        content: {
            area_List: []
        },
        packageSearchResult: [],
        allPackages: [],
        getdata: true,
        toppackages: null,
        uaeattractions: null,
        OfferPackages: null,
        Services: null,
        getservice: false,
        ServiceTitle: '',
        getpackage: false,
        getuaepackage: false,
        getSpecialPackage: false,
        triptype: "R",
        Flight_form_caption: "",
        package_form_caption: "",
        package_search_field: "",
        Oneway: "",
        Round_trip: "",
        Multicity: "",
        Advance_search_label: "",
        preferred_airline_placeholder: "",
        Direct_flight_option_label: "",
        Flight_tab_name: "",
        Hotel_tab_name: "",
        Package_tab_name: "",
        BannerTitle_1: "",
        BannerTitle_2: "",
        CityFrom: '',
        CityTo: '',
        validationMessage: '',
        adtcount: 1,
        chdcount: 0,
        infcount: 0,
        preferAirline: '',
        returnValue: true,
        legcount: 2,
        legs: [],
        cityList: [],
        adt: "adult",
        selected_cabin: 'Y',
        selected_adult: 1,
        selected_child: null,
        selected_infant: null,
        totalAllowdPax: 9,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        arabic_dropdown: '',
        mainBanner: null,
        childLabel: "",
        childrenLabel: "",
        adultLabel: "",
        adultsLabel: "",
        ageLabel: "",
        agesLabel: "",
        infantLabel: "",
        searchBtnLabel: "",
        addUptoLabel: "",
        tripsLabel: "",
        tripLabel: "",
        hotelInit: Math.random(), //hotel init
        packagePag: {},
        pkgHtml: "",
        flightSearchCityName: {
            cityFrom1: '',
            cityTo1: '',
            cityFrom2: '',
            cityTo2: '',
            cityFrom3: '',
            cityTo3: '',
            cityFrom4: '',
            cityTo4: '',
            cityFrom5: '',
            cityTo5: '',
            cityFrom6: '',
            cityTo6: ''
        },

    },
    methods: {
        Departurefrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (AirportCode == this.CityTo) {
                    this.returnValue = false;
                    this.validationMessage = "Departure and arrival airports should not be same !";
                    var self = this;
                    this.CityFrom = '';
                    setTimeout(function () {
                        self.validationMessage = '';
                    }, 1500);
                } else {
                    this.returnValue = true;
                    this.CityFrom = AirportCode;
                    this.validationMessage = "";
                }
            } else {
                var from = 'cityFrom' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].to) {
                        this.returnValue = false;
                        this.flightSearchCityName[from] = "";
                    this.validationMessage = "Departure and arrival airports should not be same !";
                } else {
                        this.cityList[index].from = AirportCode
                        this.flightSearchCityName[from] = AirportName;
                        this.returnValue = true;
                    this.validationMessage = "";
                }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: AirportCode,
                        to: ''

                    });
                    this.flightSearchCityName[from] = AirportName;
                    this.returnValue = true;
                    this.validationMessage = "";
                }
            }


        },
        Arrivalfrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (this.CityFrom == AirportCode) {
                    this.returnValue = false;
                    this.validationMessage = "Departure and arrival airports should not be same !";
                    this.CityTo = '';
                    var self = this;
                    setTimeout(function () {
                        self.validationMessage = '';
                    }, 1500);
                } else {
                    this.returnValue = true;
                    this.CityTo = AirportCode;
                    this.validationMessage = "";
                }
            } else {
                var to = 'cityTo' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].from) {
                        this.returnValue = false;
                        this.flightSearchCityName[to] = "";
                        this.validationMessage = "Departure and arrival airports should not be same !";

                    } else {
                        this.cityList[index].to = AirportCode;
                        this.flightSearchCityName[to] = AirportName;
                        this.returnValue = true;
                    this.validationMessage = "";
                }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: '',
                        to: AirportCode

                    });
                    this.flightSearchCityName[to] = AirportName;
                    this.returnValue = true;
                    this.validationMessage = "";
                }
            }
        },
        setTripType: function(type) {
            this.triptype = type;
        },
        MultiSearchFlight: function (e) {
            e.preventDefault();
            var sectors = '';
            var legDetails = [];
            for (var legValue = 1; legValue <= this.legcount; legValue++) {
                var temDeparturedate = $('#txtLeg' + (legValue) + 'Date').val() == "" ? "" : $('#txtLeg' + (legValue) + 'Date').datepicker('getDate');
                if (temDeparturedate != "" && this.cityList.length != 0 && this.cityList[legValue - 1].from != "" && this.cityList[legValue - 1].to != "") {
                    var departureFrom = this.cityList[legValue - 1].from;
                    var arrivalTo = this.cityList[legValue - 1].to;
                    var travelDate = moment(temDeparturedate).format('DD|MM|YYYY');
                    sectors += '/' + departureFrom + '-' + arrivalTo + '-' + travelDate;
                    legDetails.push(departureFrom + '|' + arrivalTo)
                } else {
                    this.validationMessage =  'Please fill the Trip ' + (legValue) + '   fields';


                    return false;
                }
            }
            var directFlight = this.direct_flight ? 'DF' : 'AF';

            var adult = this.selected_adult;
            var child = this.selected_child;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            getSuppliers(legDetails,
            function(supp) {
                var searchUrl = '/Flights/flight-listing.html?flight=' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                // searchUrl = searchUrl.toLocaleLowerCase();
                window.location.href = searchUrl;
            })
        },
        SearchFlight: function (e) {
            e.preventDefault();
            var Departuredate = $('#deptDate01').val() == "" ? "" : $('#deptDate01').datepicker('getDate');
            if (this.triptype == "O") {
                Departuredate = $('#deptDate02').val() == "" ? "" : $('#deptDate02').datepicker('getDate')
            }
            if (!this.CityFrom) {
                this.validationMessage = "Please fill origin !";
                var self = this;
                setTimeout(function () {
                    self.validationMessage = '';
                }, 1500);
                return false;
            }
            if (!this.CityTo) {
                this.validationMessage = "Please fill destination !";
                var self = this;
                setTimeout(function () {
                    self.validationMessage = '';
                }, 1500);
                return false;
            }
            if (!Departuredate) {
                this.validationMessage = "Please choose departure date !";
                var self = this;
                setTimeout(function () {
                    self.validationMessage = '';
                }, 1500);
                return false;
            }
            var sec1TravelDate = moment(Departuredate).format('DD|MM|YYYY');

            var sectors = this.CityFrom + '-' + this.CityTo + '-' + sec1TravelDate;
            if (this.triptype == 'R') {
                var ArrivalDate = $('#retDate').val() == "" ? "" : $('#retDate').datepicker('getDate');
                if (!isNullorUndefined(ArrivalDate)) {
                    ArrivalDate = moment(ArrivalDate).format('DD|MM|YYYY');
                    if (!ArrivalDate) {
                        this.validationMessage = "Please choose return date !";
                        var self = this;
                        setTimeout(function () {
                            self.validationMessage = '';
                        }, 1500);
                        return false;
                    } else {
                        sectors += '/' + this.CityTo + '-' + this.CityFrom + '-' + ArrivalDate;
                    }
                } else {
                    this.validationMessage = "Please choose return date !";
                    var self = this;
                    setTimeout(function () {
                        self.validationMessage = '';
                    }, 1500);
                    return false;
                }
            }
            if (this.triptype == 'M') {
                for (var legValue = 1; legValue <= this.legcount; legValue++) {
                    var temDeparturedate = $('#txtLeg' + (legValue + 1) + 'Date').val() == "" ? "" : $('#txtLeg' + (legValue + 1) + 'Date').datepicker('getDate');
                    if (temDeparturedate != "" && this.cityList[legValue - 1].from != "" && this.cityList[legValue - 1].to != "") {
                        var departureFrom = this.cityList[legValue - 1].from;
                        var arrivalTo = this.cityList[legValue - 1].to;
                        var travelDate = moment(temDeparturedate).format('DD|MM|YYYY');
                        sectors += '/' + departureFrom + '-' + arrivalTo + '-' + travelDate;
                    } else {
                        this.validationMessage = "Please fill the Trip " + (legValue + 1) + "   fields !";
                        var self = this;
                        setTimeout(function () {
                            self.validationMessage = '';
                        }, 1500);
                        return false;
                    }
                }
            }
            if ($('#checkbox-list-6').is(':checked')) {
                var directFlight = "DF";
            } else {
                var directFlight = "AF";
            }
            var adult = this.selected_adult || 1;
            var child = this.selected_child || 0;
            var infant = this.selected_infant || 0;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            var searchUrl = '/Flights/flight-listing.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-all-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
            // searchUrl = searchUrl.toLocaleLowerCase();
            window.location.href = searchUrl;
        },
        AddNewLeg(e) {
            e.preventDefault();
            if (this.legcount <= 5) {
                ++this.legcount;
                var legno = 1;
                if (this.cityList.length > 0) {
                    legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                    legno = legno + 1;
                }

                this.cityList.push({
                    id: legno,
                    from: '',
                    to: ''
                });

            }
            console.log(this.cityList);
        },
        DeleteLeg(e) {
            e.preventDefault();
            this.cityList = [];
            this.legcount = 2;
        },
        setCalender() {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = generalInformation.systemSettings.calendarDisplay;
            $("#deptDate01, #deptDate02").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                },
                onClose: function() {
                    $('#retDate').focus();
                }
            });

            $("#retDate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) { },
                onClose: function() {
                    $('#flight-search-adult').focus();
                }
            });

            $("#txtLeg2Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg3Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg2Date").val();
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg4Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg3Date").val();
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg5Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg4Date").val();
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg6Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg5Date").val();
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) { }
            });


        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },

        getPagecontent: async function () {
            var self = this;
            // self.toppackages = null;
        },
        redirectToLink: function(url) {
            if(url) {
                if (url.indexOf("#hotel") >= 0) {
                    $("#hotelTab").click();
                    setTimeout(function() {
                        window.location.href = "#hotelTab";
                    }, 50);
                } else if (url.indexOf("#flight") >= 0) {
                    $("#flightTab").click();
                    setTimeout(function() {
                        window.location.href = "#flightTab";
                    }, 50);
                } else if (url.indexOf("#package") >= 0) {
                    $("#packageTab").click();
                    setTimeout(function() {
                        window.location.href = "#packageTab";
                    }, 50);
                } else {
                    window.location.href = url;
                }
            }
        },
        onKeyUpAutoCompleteEvent: _.debounce(function (event) {
            var self = this;
            let searchResult = [];

            let keywordEntered = "";
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                keywordEntered = event.target.value;
            }

            if (keywordEntered.length > 2) {


                for (let i = 0; i < this.allPackages.length; i++) {
                    let Destination = this.allPackages[i].Destination;
                    if ((this.allPackages[i].Holiday_Package == true || this.allPackages[i].Special_Package == true) &&
                        this.allPackages[i].Status == true &&
                        Destination != undefined &&
                        Destination.toLowerCase().includes(keywordEntered.toLowerCase())) {
                        searchResult.push(this.allPackages[i]);
                    }

                }


            }
            var resArr = [];
            searchResult.filter(function (item) {
                var i = resArr.findIndex(function (x) { return x.Destination.trim() == item.Destination.trim() });
                if (i <= -1) {
                    resArr.push(item);
                }
                return null;
            });

            this.packageSearchResult = resArr;

        }, 100),
        updateResults: function (item) {
            var link = "/Nirvana/package.html?destination=" + item.Destination;
            window.location.href = link;
        },
        onAdultChange: function () {
            alert(this.selected);
            //this.disp = this.selected;
        },
        onCheck: function (e) {
            var currid = $(e.target).attr('id');

            console.log(currid);
            var selectedDate = $("#deptDate01").val();
            $("#retDate").datepicker("option", "minDate", selectedDate);
            $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
        },
        getmoreinfo(url, type) {
            if (type == undefined || type == '') {
                type = "pkg";
            }
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.');
                    url=url.split('/')[url.split('/').length-1];
                    url = "/Nirvana/packageInfo.html?page=" + url + "&from=" + type;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;
        },
        getmoreinfoforservice(url) {
            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.');
                url=url.split('/')[url.split('/').length-1];
                url = "/Nirvana/servicesinfo.html?page=" + url + "&from=service";
            }
            else {
                url = "#";
            }

            return url
        },
    },
    mounted: function () {
        this.setCalender();
        this.getPagecontent();

        // this.getPackageContent();
        if (localStorage.direction == 'rtl') {
            this.arabic_dropdown = "dwn_icon";
            $(".ar_direction").addClass("ar_direction1");
        } else {
            this.arabic_dropdown = "";
            $(".ar_direction").removeClass("ar_direction1");
        }
        this.$nextTick(function () {
            setTimeout(() => {


        $('#owl-carousel').owlCarousel({
            loop: true,
            margin: 30,
            dots: true,
            nav: true,
            items: 3,
            navText: [
            '<img style="width: 28px; height: auto;" src="assets/images/icon/left-arrow.svg">',
            '<img style="width: 28px; height: auto;" src="assets/images/icon/right-arrow.svg">'
            ],
            navContainer: '.custom-nav',
            responsive:{
                0:{
                    items: 1
                },
                600:{
                    items: 3
                },
                1000:{
                    items: 3
                }
            }
            })

            $('#owl-carousel-2').owlCarousel({
            loop: true,
            margin: 30,
            dots: true,
            nav: false,
            items: 2,
            responsive:{
                0:{
                    items: 1
                },
                600:{
                    items: 2
                },
                1000:{
                    items: 2
                }
            }
            })

            $('#owl-carousel-3').owlCarousel({
            loop: true,
            margin: 30,
            dots: false,
            nav: true,
            items: 3,
            navText: [
            '<i class="las la-angle-left"></i>',
            '<i class="las la-angle-right"></i>'
            ],
            navContainer: '.custom-nav2',
            responsive:{
                0:{
                    items: 1
                },
                600:{
                    items: 3
                },
                1000:{
                    items: 3
                }
            }
            })

            //   $('.number-spinner>.ns-btn>a').click(function() {
            //     var btn = $(this),
            //       oldValue = btn.closest('.number-spinner').find('input').val().trim(),
            //       newVal = 0;

            //     if (btn.attr('data-dir') === 'up') {
            //       newVal = parseInt(oldValue) + 1;
            //     } else {
            //       if (oldValue > 1) {
            //         newVal = parseInt(oldValue) - 1;
            //       } else {
            //         newVal = 1;
            //       }
            //     }
            //     btn.closest('.number-spinner').find('input').val(newVal);
            //   });
            //   $('.number-spinner>input').keypress(function(evt) {
            //     evt = (evt) ? evt : window.event;
            //     var charCode = (evt.which) ? evt.which : evt.keyCode;
            //     if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            //       return false;
            //     }
            //     return true;
            //   });
            }, 100);
            });

            (function ($) {
                'use strict';

                var prealoaderOption = $(window);
                prealoaderOption.on("load", function () {
                    var preloader = jQuery('.spinner');
                    var preloaderArea = jQuery('.preloader-area');
                    preloader.fadeOut();
                    preloaderArea.delay(350).fadeOut('slow');
                });
                jQuery(document).ready(function () {

                    /*

                    $('ul.nav li.dropdown').hover(function() {
                      $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
                    }, function() {
                      $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
                    });

                    $('.dropdown-menu li.dropdown-submenu').hover(function() {
                      $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
                    }, function() {
                      $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
                    });



                     //Submenu Dropdown Toggle
                if($('li.dropdown ul').length){
                    //Dropdown Button
                    $('.header-menu li.dropdown .dropdown-btn').on('click', function() {
                        $(this).prev('ul').slideToggle(500);
                    });

                    //Disable dropdown parent link
                    $('.header-menu .navigation li.dropdown > a,.hidden-bar .side-menu li.dropdown > a').on('click', function(e) {
                        e.preventDefault();
                    });
                }
            */


                    // jQuery('#bslider').slider();
                    // jQuery('#dslider').slider();
                    jQuery(".selector select").each(function () {
                        var obj = jQuery(this);
                        if (obj.parent().children(".custom-select").length < 1) {
                            obj.after("<span class='custom-select'>" + obj.children("option:selected").html() + "</span>");

                            if (obj.hasClass("white-bg")) {
                                obj.next("span.custom-select").addClass("white-bg");
                            }
                            if (obj.hasClass("full-width")) {
                                //obj.removeClass("full-width");
                                //obj.css("width", obj.parent().width() + "px");
                                //obj.next("span.custom-select").css("width", obj.parent().width() + "px");
                                obj.next("span.custom-select").addClass("full-width");
                            }
                        }
                    });

                    // Open the full screen search box
                    jQuery('#opensearch').on('click', function () {
                        document.getElementById("myOverlay").style.display = "block";
                    });

                    // Close the full screen search box

                    jQuery('#btnclose').on('click', function () {
                        document.getElementById("myOverlay").style.display = "none";
                    });



                    $(".testimonials-slider").owlCarousel({
                        "items": 1,
                        "autoPlay": true,
                        "navigation": false,
                        "dots": true,
                        "itemsDesktop": [1199, 2],
                        "itemsDesktopSmall": [980, 2],
                        "itemsTablet": [768, 2],
                        "itemsMobile": [479, 1],
                        "pagination": true,
                        "autoHeight": true,
                        "autoplay": false,
                        "loop": true,
                        "responsive": {
                            "0": {
                                "items": 1
                            },
                            "480": {
                                "items": 1
                            },
                            "768": {
                                "items": 1
                            },
                            "992": {
                                "items": 1
                            },
                            "1200": {
                                "items": 1
                            }
                        },
                    });


                    /*

                            if ($('.testimonials-slider').length) {
                                $('.testimonials-slider').owlCarousel({
                                    singleItem: true,
                                    "navigation": true,
                                    "navigationText": ['', ''],
                                    "pagination": false,

                                });
                            }

                             ==============================================================
                               Accordian Javascript
                             ==============================================================
                             */
                    if ($('.accordion').length) {
                        //custom animation for open/close
                        $.fn.slideFadeToggle = function (speed, easing, callback) {
                            return this.animate({ opacity: 'toggle', height: 'toggle' }, speed, easing, callback);
                        };

                        $('.accordion').accordion({
                            defaultOpen: 'section1',
                            cookieName: 'nav',
                            speed: 'slow',
                            animateOpen: function (elem, opts) { //replace the standard slideUp with custom function
                                elem.next().stop(true, true).slideFadeToggle(opts.speed);
                            },
                            animateClose: function (elem, opts) { //replace the standard slideDown with custom function
                                elem.next().stop(true, true).slideFadeToggle(opts.speed);
                            }
                        });
                    }
                    //Gallery Filters
                    if ($('.filter-list').length) {
                        $('.filter-list').mixItUp({});
                    }

                    // Active Reviews

                    $(".widget .review-rating").click(function () {
                        $(".widget .review-rating").removeClass("Default-clr");
                        $(this).addClass("Default-clr");
                    })

                    // //Price Range Slider

                    // if ($('.price-range-slider-one').length) {
                    //     $(".price-range-slider-one").slider({
                    //         range: true,
                    //         min: 0,
                    //         max: 9000,
                    //         values: [0, 9000],
                    //         slide: function (event, ui) {
                    //             $("input.property-amount-1").val(ui.values[0] + " - " + ui.values[1]);
                    //         }
                    //     });

                    //     $("input.property-amount-1").val($(".price-range-slider-one").slider("values", 0) + " - $" + $(".price-range-slider-one").slider("values", 1));
                    // }

                    // if ($('.price-range-slider-two').length) {
                    //     $(".price-range-slider-two").slider({
                    //         range: true,
                    //         min: 1,
                    //         max: 24,
                    //         values: [1, 24],
                    //         slide: function (event, ui) {
                    //             $("input.property-amount-2").val(ui.values[0] + " - " + ui.values[1]);
                    //         }
                    //     });

                    //     $("input.property-amount-2").val($(".price-range-slider-two").slider("values", 0) + " - " + $(".price-range-slider-two").slider("values", 1));
                    // }



                    // (function() {

                    // function addSeperator(nStr) {
                    //     nStr += '';
                    //     var x = nStr.split('.');
                    //     var x1 = x[0];
                    //     var x2 = x.length > 1 ? '.' + x[1] : '';
                    //     var rgx = /(\d+)(\d{3})/;
                    //     while (rgx.test(x1)) {
                    //         x1 = x1.replace(rgx, '$1' + '.' + '$2');
                    //     }
                    //     return x1 + x2;
                    // }

                    // function rangeInputChangeEventHandler(e) {
                    //     var rangeGroup = $(this).attr('name'),
                    //         minBtn = $(this).parent().children('.min'),
                    //         maxBtn = $(this).parent().children('.max'),
                    //         range_min = $(this).parent().children('.range_min'),
                    //         range_max = $(this).parent().children('.range_max'),
                    //         minVal = parseInt($(minBtn).val()),
                    //         maxVal = parseInt($(maxBtn).val()),
                    //         origin = $(this).context.className;

                    //     if (origin === 'min' && minVal > maxVal - 5) {
                    //         $(minBtn).val(maxVal - 5);
                    //     }
                    //     var minVal = parseInt($(minBtn).val());
                    //     $(range_min).html((minVal));


                    //     if (origin === 'max' && maxVal - 5 < minVal) {
                    //         $(maxBtn).val(5 + minVal);
                    //     }
                    //     var maxVal = parseInt($(maxBtn).val());
                    //     $(range_max).html((maxVal));
                    // }

                    // $('input[type="range"]').on('input', rangeInputChangeEventHandler);
                    // })();

                    /*$('ul.nav li.dropdown').hover(function() {
                      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
                    }, function() {
                      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
                    });
            */

                    // Show - hide box search on menu
                    $('.button-search').on('click', function () {
                        $('.nav-search').toggleClass('hide');
                    });

                    //hide box seach when click outside
                    $('body').on('click', function (event) {
                        if ($('.button-search').has(event.target).length === 0 && !$('.button-search').is(event.target) && $('.nav-search').has(event.target).length === 0 && !$('.nav-search').is(event.target)) {
                            if ($('.nav-search').hasClass('hide') === false) {
                                $('.nav-search').toggleClass('hide');
                            }
                        }
                    });
                    // =============== MOBILE MENU OPEN FUNCTION ===================

                    $(".menu-btn, .mobile-men-btn > a").on("click", function () {
                        $(".responsive-mobile-menu").addClass("active");
                    });
                    $(".close-menu-btn, html").on("click", function () {
                        $(".responsive-mobile-menu").removeClass("active");
                    });
                    $(".menu-btn, .mobile-men-btn > a, .responsive-mobile-menu").on("click", function (e) {
                        e.stopPropagation();
                    });


                    /*
                     * ----------------------------------------------------------------------------------------
                     *  PRELOADER JS
                     * ----------------------------------------------------------------------------------------
                     */





                    /*
                     * ----------------------------------------------------------------------------------------
                     *  CHANGE MENU BACKGROUND JS
                     * ----------------------------------------------------------------------------------------
                     */
                    var headertopoption = $(window);
                    var headTop = $('.header-top-area');

                    headertopoption.on('scroll', function () {
                        if (headertopoption.scrollTop() > 100) {
                            headTop.addClass('menu-bg');
                        } else {
                            headTop.removeClass('menu-bg');
                        }
                    });




                    /*
                     * ----------------------------------------------------------------------------------------
                     *  SMOTH SCROOL JS
                     * ----------------------------------------------------------------------------------------


                    $('a.smoth-scroll').on("click", function(e) {
                        var anchor = $(this);
                        $('html, body').stop().animate({
                            scrollTop: $(anchor.attr('href')).offset().top - 50
                        }, 1000);
                        e.preventDefault();
                    });

            */
                    /*
                     * ----------------------------------------------------------------------------------------
                     *  MAGNIFIC POPUP JS
                     * ----------------------------------------------------------------------------------------
                     */

                    $('.video-play').magnificPopup({
                        type: 'iframe'
                    });

                    var magnifPopup = function () {
                        $('.work-popup').magnificPopup({
                            type: 'image',
                            removalDelay: 300,
                            mainClass: 'mfp-with-zoom',
                            gallery: {
                                enabled: true
                            },
                            zoom: {
                                enabled: true, // By default it's false, so don't forget to enable it

                                duration: 300, // duration of the effect, in milliseconds
                                easing: 'ease-in-out', // CSS transition easing function

                                // The "opener" function should return the element from which popup will be zoomed in
                                // and to which popup will be scaled down
                                // By defailt it looks for an image tag:
                                opener: function (openerElement) {
                                    // openerElement is the element on which popup was initialized, in this case its <a> tag
                                    // you don't need to add "opener" option if this code matches your needs, it's defailt one.
                                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                                }
                            }
                        });
                    };
                    // Call the functions
                    magnifPopup();
                    /*
                     * ----------------------------------------------------------------------------------------
                     *  PARALLAX JS
                     * ----------------------------------------------------------------------------------------


                    var parallaxeffect = $(window);
                    parallaxeffect.stellar({
                        responsive: true,
                        positionProperty: 'position',
                        horizontalScrolling: false
                    });

             */


                    /*
                     * ----------------------------------------------------------------------------------------
                     *  PROGRESS BAR JS
                     * ----------------------------------------------------------------------------------------
                     */
                    $('.progress-bar > span').each(function () {
                        var $this = $(this);
                        var width = $(this).data('percent');
                        $this.css({
                            'transition': 'width 3s'
                        });
                        setTimeout(function () {
                            $this.appear(function () {
                                $this.css('width', width + '%');
                            });
                        }, 500);
                    });



                    /*
                     * ----------------------------------------------------------------------------------------
                     *  TESTIMONIAL JS
                     * ----------------------------------------------------------------------------------------
                     */
                    function callback(event) {
                        // Provided by the core
                        var element = event.target;         // DOM element, in this example .owl-carousel
                        var name = event.type;           // Name of the event, in this example dragged
                        var namespace = event.namespace;      // Namespace of the event, in this example owl.carousel
                        var items = event.item.count;     // Number of items
                        var item = event.item.index;     // Position of the current item
                        // Provided by the navigation plugin
                        var pages = event.page.count;     // Number of pages
                        var page = event.page.index;     // Position of the current page
                        var size = event.page.size;      // Number of items per page
                    }
                    $(".testimonial-list").owlCarousel({
                        "items": 4,
                        "autoPlay": false,
                        "navigation": false,
                        "dots": true,
                        "itemsDesktop": [1199, 2],
                        "itemsDesktopSmall": [980, 2],
                        "itemsTablet": [768, 2],
                        "itemsMobile": [479, 1],
                        "pagination": true,
                        "autoHeight": true,
                        "autoplay": false,
                        "loop": true,
                        "responsive": {
                            "0": {
                                "items": 1
                            },
                            "480": {
                                "items": 1
                            },
                            "768": {
                                "items": 2
                            },
                            "992": {
                                "items": 3
                            },
                            "1200": {
                                "items": 4
                            }
                        },
                    });


                    $(".tour-list").owlCarousel({
                        "items": 3,
                        "loop": true,
                        "navigation": true,
                        "autoHeight": true,
                        "margin": 10,
                        "dots": false,
                        "nav": true,
                        "autoplay": true,
                        "autoplayTimeout": 1000,
                        "navigationText": [""],
                        "itemsDesktop": [1199, 2],
                        "itemsDesktopSmall": [980, 2],
                        "itemsTablet": [768, 2],
                        "itemsMobile": [479, 1],
                        "pagination": false,
                        "autoplay": true,
                        "merge": true,
                        "responsive": {
                            "0": {
                                "items": 1
                            },
                            "480": {
                                "items": 3
                            },
                            "768": {
                                "items": 2
                            },
                            "992": {
                                "items": 3
                            },
                            "1200": {
                                "items": 3
                            }
                        },
                    });

                    $(".tour_details").owlCarousel({
                        "items": 1,
                        "autoPlay": true,
                        "navigation": true,
                        "navigationText": [""],
                        "itemsDesktop": [1199, 2],
                        "itemsDesktopSmall": [980, 2],
                        "itemsTablet": [768, 2],
                        "itemsMobile": [479, 1],
                        "pagination": false,
                        "autoHeight": true,
                        "loop": true,
                        "dots": true,
                        "autoplay": true,
                        "responsive": {
                            "0": {
                                "items": 1
                            },
                            "480": {
                                "items": 1
                            },
                            "768": {
                                "items": 1
                            },
                            "992": {
                                "items": 1
                            },
                            "1200": {
                                "items": 1
                            }
                        },
                    });

                    $(".review-list").owlCarousel({
                        "items": 3,
                        "autoPlay": false,
                        "margin": 30,
                        "nav": true,
                        "navigationText": [""],
                        "itemsDesktop": [1199, 2],
                        "itemsDesktopSmall": [980, 2],
                        "itemsTablet": [768, 2],
                        "itemsMobile": [479, 1],
                        "pagination": false,
                        "autoHeight": false,
                        "autoplay": false,
                        "autoplayTimeout": 1000,
                        "loop": true,
                        "dots": false,
                        "responsive": {
                            "0": {
                                "items": 1
                            },
                            "480": {
                                "items": 1
                            },
                            "768": {
                                "items": 2
                            },
                            "992": {
                                "items": 2
                            },
                            "1200": {
                                "items": 3
                            }
                        },

                    });
                    $(".blog-slider1").owlCarousel({
                        "items": 2,
                        "autoPlay": true,
                        "navigation": true,
                        "margin": 30,
                        "navigationText": [""],
                        "itemsDesktop": [1199, 2],
                        "itemsDesktopSmall": [980, 2],
                        "itemsTablet": [768, 2],
                        "itemsMobile": [479, 1],
                        "pagination": false,
                        "dots": false,
                        "navigation": true,
                        "autoHeight": false,
                        "loop": true,
                        "responsive": {
                            "0": {
                                "items": 1
                            },
                            "480": {
                                "items": 1
                            },
                            "768": {
                                "items": 1
                            },
                            "992": {
                                "items": 2
                            },
                            "1200": {
                                "items": 2
                            }
                        },
                    });
                    $(".blog-slider2").owlCarousel({
                        "items": 2,
                        "autoPlay": true,
                        "margin": 30,
                        "navigation": true,
                        "navigationText": [""],
                        "itemsDesktop": [1199, 2],
                        "itemsDesktopSmall": [980, 2],
                        "itemsTablet": [768, 2],
                        "itemsMobile": [479, 1],
                        "pagination": false,
                        "autoHeight": false,
                        "dots": false,
                        "navigation": true,
                        "loop": true,
                        "responsive": {
                            "0": {
                                "items": 1
                            },
                            "480": {
                                "items": 1
                            },
                            "768": {
                                "items": 1
                            },
                            "992": {
                                "items": 2
                            },
                            "1200": {
                                "items": 2
                            }
                        },
                    });
                    $(".related-posts-list").owlCarousel({
                        "items": 1,
                        "autoPlay": true,
                        "navigation": true,
                        "navigationText": [""],
                        "itemsDesktop": [1199, 2],
                        "itemsDesktopSmall": [980, 2],
                        "itemsTablet": [768, 2],
                        "itemsMobile": [479, 1],
                        "pagination": false,
                        "autoHeight": true,
                        "loop": true,
                        "dots": true,
                        "responsive": {
                            "0": {
                                "items": 1
                            },
                            "480": {
                                "items": 1
                            },
                            "768": {
                                "items": 1
                            },
                            "992": {
                                "items": 1
                            },
                            "1200": {
                                "items": 1
                            }
                        },
                    });
                    /*
                     * ----------------------------------------------------------------------------------------
                     *  COMPANY JS
                     * ----------------------------------------------------------------------------------------
                     */

                    $(".company-logo-list").owlCarousel({
                        "items": 5,
                        "autoPlay": true,
                        "navigation": false,
                        "itemsDesktop": [1199, 5],
                        "itemsDesktopSmall": [980, 4],
                        "itemsTablet": [768, 3],
                        "itemsTabletSmall": false,
                        "itemsMobile": [479, 2],
                        "pagination": false,
                        "autoHeight": true,
                        "navigationText": ["", ""],
                        "loop": true,
                        "responsive": {
                            "0": {
                                "items": 2
                            },
                            "480": {
                                "items": 2
                            },
                            "768": {
                                "items": 3
                            },
                            "992": {
                                "items": 4
                            },
                            "1200": {
                                "items": 5
                            }
                        },
                    });

                    $(".owl-brands").owlCarousel({
                        "items": 5,
                        "autoPlay": true,
                        "navigation": true,
                        "itemsDesktop": [1199, 5],
                        "itemsDesktopSmall": [980, 4],
                        "itemsTablet": [768, 3],
                        "itemsTabletSmall": false,
                        "itemsMobile": [479, 2],
                        "pagination": false,
                        "autoHeight": true,
                        "navigationText": ["", ""],
                        "loop": true,
                        "responsive": {
                            "0": {
                                "items": 1
                            },
                            "480": {
                                "items": 2
                            },
                            "768": {
                                "items": 3
                            },
                            "992": {
                                "items": 4
                            },
                            "1200": {
                                "items": 5
                            }
                        },

                    });

                    $(".company-logo-list2").owlCarousel({
                        "items": 5,
                        "autoPlay": true,
                        "navigation": false,
                        "itemsDesktop": [1199, 5],
                        "itemsDesktopSmall": [980, 4],
                        "itemsTablet": [768, 3],
                        "itemsTabletSmall": false,
                        "itemsMobile": [479, 2],
                        "pagination": false,
                        "autoHeight": true,
                        "loop": true,
                        "responsive": {
                            "0": {
                                "items": 1
                            },
                            "480": {
                                "items": 3
                            },
                            "768": {
                                "items": 3
                            },
                            "992": {
                                "items": 3
                            },
                            "1200": {
                                "items": 5
                            }
                        },

                    });


                    /*
                     * ----------------------------------------------------------------------------------------
                     *  EXTRA JS
                     * ----------------------------------------------------------------------------------------
                     */
                    // $(document).on('click', '.navbar-collapse.in', function (e) {
                    //     if ($(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle') {
                    //         $(this).collapse('hide');
                    //     }
                    // });
                    $('body').scrollspy({
                        target: '.navbar-collapse',
                        offset: 195
                    });



                    /*
                     * ----------------------------------------------------------------------------------------
                     *  SCROOL TO UP JS
                     * ----------------------------------------------------------------------------------------
                     */
                    $(window).on("scroll", function () {
                        if ($(this).scrollTop() > 250) {
                            $('.scrollup').fadeIn();
                        } else {
                            $('.scrollup').fadeOut();
                        }
                    });
                    $('.scrollup').on("click", function () {
                        $("html, body").animate({
                            scrollTop: 0
                        }, 800);
                        return false;
                    });

                    /*
                     * ----------------------------------------------------------------------------------------
                     *  SCROOL TO Down JS
                     * ----------------------------------------------------------------------------------------
                     */

                    $(window).on("scroll", function () {
                        if ($(this).scrollTop() > 100) {
                            $('.transparent-noborder').fadeIn();
                        }
                    });
                    $('.transparent-noborder').on("click", function () {
                        $("html, body").animate({
                            scrollTop: 1500
                        }, 800);
                        window.scrollTo(500, 0);
                        return false;
                    });




                    /*---------------------------------------------------
                        hotel Filter
                    ----------------------------------------------------*/
                    var Container = $('.container');
                    if (typeof imagesLoaded == 'function') {
                        Container.imagesLoaded(function () {
                            var hotel = $('.hotel-menu');
                            hotel.on('click', 'button', function () {
                                $(this).addClass('active').siblings().removeClass('active');
                                var filterValue = $(this).attr('data-filter');
                                $grid.isotope({
                                    filter: filterValue
                                });
                            });
                            var $grid = $('.hotel-items').isotope({
                                itemSelector: '.grid-item'
                            });

                        });
                    }
                    /*---------------------------------------------------
                        gallery Filter
                    ----------------------------------------------------*/
                    var Container = $('.container');
                    if (typeof imagesLoaded == 'function') {
                        Container.imagesLoaded(function () {
                            var gallery = $('.gallery-menu');
                            gallery.on('click', 'button', function () {
                                $(this).addClass('active').siblings().removeClass('active');
                                var filterValue = $(this).attr('data-filter');
                                $grid.isotope({
                                    filter: filterValue
                                });
                            });
                            var $grid = $('.gallery-items').isotope({
                                itemSelector: '.grid-item'
                            });

                        });
                    }
                    /*---------------------------------------------------
                            gallery Filter
                        ----------------------------------------------------*/
                    var Container = $('.container');
                    if (typeof imagesLoaded == 'function') {
                        Container.imagesLoaded(function () {
                            var gallery = $('.gallery-menu-5');
                            gallery.on('click', 'button', function () {
                                $(this).addClass('active').siblings().removeClass('active');
                                var filterValue = $(this).attr('data-filter');
                                $grid.isotope({
                                    filter: filterValue
                                });
                            });
                            var $grid = $('.gallery-items5').isotope({
                                itemSelector: '.grid-item'
                            });

                        });
                    }
                    /*---------------------------------------------------
                           gallery Filter
                       ----------------------------------------------------*/
                    var Container = $('.container');
                    if (typeof imagesLoaded == 'function') {
                        Container.imagesLoaded(function () {
                            var gallery = $('.gallery-menu-5-2');
                            gallery.on('click', 'button', function () {
                                $(this).addClass('active').siblings().removeClass('active');
                                var filterValue = $(this).attr('data-filter');
                                $grid.isotope({
                                    filter: filterValue,

                                });
                            });
                            var $grid = $('.gallery-items-5-2').isotope({
                                itemSelector: '.grid-item',
                                layoutMode: 'masonry',
                            });

                        });
                    }
                    /*---------------------------------------------------
                           gallery Filter
                       ----------------------------------------------------*/
                    var Container = $('.container');
                    if (typeof imagesLoaded == 'function') {
                        Container.imagesLoaded(function () {
                            var gallery = $('.gallery-menu-5-3');
                            gallery.on('click', 'button', function () {
                                $(this).addClass('active').siblings().removeClass('active');
                                var filterValue = $(this).attr('data-filter');
                                $grid.isotope({
                                    filter: filterValue
                                });
                            });
                            var $grid = $('.gallery-items-5-3').isotope({
                                itemSelector: '.grid-item'
                            });

                        });
                    }
                    //start masonry
                    jQuery(document).ready(function () {

                        $(function () {

                            //initialize
                            var $container = $('.stylemasonry');

                            $container.isotope({
                                itemSelector: '.singlemasonry'
                            });
                            //end initialize

                            //start masonry
                            $container.isotope({
                                itemSelector: '.singlemasonry'
                            });
                            // end masonry

                        });

                    });
                    //start masonry

                    // var $countDown = $('.count-down');

                    //     if ($countDown.length) {
                    //         var endDate = new Date($countDown.data("end-date"));
                    //         $countDown.countdown({
                    //             date: endDate,
                    //             render: function(data) {
                    //                 $(this.el).html(
                    //                     '<div><span class="time">' + this.leadingZeros(data.days, 2) + '</span> DAYS</div>' +
                    //                     '<span class="coln">:</span>' +
                    //                     '<div><span class="time">' + this.leadingZeros(data.hours, 2) + '</span> HOURS</div>' +
                    //                     '<span class="coln">:</span>' +
                    //                     '<div><span class="time">' + this.leadingZeros(data.min, 2) + '</span> MIN</div>' +
                    //                     '<span class="coln">:</span>' +
                    //                     '<div><span class="time">' + this.leadingZeros(data.sec, 2) + '</span> SEC</div>'
                    //                 );
                    //             }
                    //         });
                    //     }

                    /*
                     * ----------------------------------------------------------------------------------------
                     *  WOW JS
                     * ----------------------------------------------------------------------------------------
                     */
                    new WOW().init();




                    /*
                     * ----------------------------------------------------------------------------------------
                     *  TYPE EFFECT JS
                     * ----------------------------------------------------------------------------------------
                     */

                    var TxtType = function (el, toRotate, period) {
                        this.toRotate = toRotate;
                        this.el = el;
                        this.loopNum = 0;
                        this.period = parseInt(period, 10) || 1000;
                        this.txt = '';
                        this.tick();
                        this.isDeleting = false;
                    };

                    TxtType.prototype.tick = function () {
                        var i = this.loopNum % this.toRotate.length;
                        var fullTxt = this.toRotate[i];

                        if (this.isDeleting) {
                            this.txt = fullTxt.substring(0, this.txt.length - 1);
                        } else {
                            this.txt = fullTxt.substring(0, this.txt.length + 1);
                        }

                        this.el.innerHTML = '<span class="wrap">' + this.txt + '</span>';

                        var that = this;
                        var delta = 150 - Math.random() * 100;

                        if (this.isDeleting) {
                            delta /= 2;
                        }

                        if (!this.isDeleting && this.txt === fullTxt) {
                            delta = this.period;
                            this.isDeleting = true;
                        } else if (this.isDeleting && this.txt === '') {
                            this.isDeleting = false;
                            this.loopNum++;
                            delta = 500;
                        }

                        setTimeout(function () {
                            that.tick();
                        }, delta);
                    };

                    window.onload = function () {
                        var elements = document.getElementsByClassName('typewrite');
                        for (var i = 0; i < elements.length; i++) {
                            var toRotate = elements[i].getAttribute('data-type');
                            var period = elements[i].getAttribute('data-period');
                            if (toRotate) {
                                new TxtType(elements[i], JSON.parse(toRotate), period);
                            }
                        }
                        // INJECT CSS
                        var css = document.createElement("style");
                        css.type = "text/css";
                        css.innerHTML = ".typewrite > .wrap { border-right: 0.02em solid #fff}";
                        document.body.appendChild(css);
                    };

                });



                /*------------------------------------------------------------------
                 Validate
                 -------------------------------------------------------------------*/

                $("#submit").on("click", function () {
                    var errors = "";

                    var contact_name = document.getElementById("contact_name");
                    var contact_email_address = document.getElementById("contact_email");

                    if (contact_name.value == "") {
                        errors += 'Please provide your name.';
                    }
                    else if (contact_email_address.value == "") {
                        errors += 'Please provide an email address.';
                    }
                    else if (contact_email_address.value == "") {
                        errors += 'Please provide a valid email address.';
                    }


                    if (errors) {
                        document.getElementById("error").style.display = "block";
                        document.getElementById("error").innerHTML = errors;
                        return false;
                    }

                    else {

                        $.ajax({
                            type: "POST",
                            url: 'process.php',
                            data: $("#contact_form").serialize(),
                            success: function (msg) {
                                if (msg == 'success') {
                                    document.getElementById("error").style.display = "none";
                                    document.getElementById("contact_name").value = "";
                                    document.getElementById("contact_email").value = "";
                                    document.getElementById("message").value = "";
                                    $("#contact_form").hide();
                                    document.getElementById("success").style.display = "block";
                                    document.getElementById("success").innerHTML = "Thank You! We'll contact you shortly.";
                                } else {
                                    document.getElementById("error").style.display = "block";
                                    document.getElementById("error").innerHTML = "Oops! Something went wrong while prceeding.";
                                }
                            }

                        });

                    }
                });

                /*
                ---------------------------
                    dropdown submenu
                ---------------------------
                */

                $(document).ready(function () {
                    $('.dropdown-submenu').on("click", function (e) {
                        $(this).next('ul.dropdown-2').toggle();
                        e.stopPropagation();
                        e.preventDefault();
                    });
                });

                $(document).ready(function () {
                    $(".dropdown-submenu ul.dropdown-2 li a").click(function () {
                        var url = $(this).attr("href");
                        $(location).attr('href', url);
                    });
                });

                /*
                ---------------------------
                    Venobox
                ---------------------------
                */
                // $(document).ready(function () {
                //     $('.venobox').venobox();
                // });


                // $("#firstlink").venobox().trigger('click');


                // $('.venobox_custom').venobox({
                //     framewidth: '500px',        // default: ''
                //     frameheight: '400px',       // default: ''
                //     border: '5px',             // default: '0'
                //     bgcolor: '#5dff5e',         // default: '#fff'
                //     titleattr: 'data-title',    // default: 'title'
                //     numeratio: true,            // default: false
                //     infinigall: true            // default: false
                // });

                // $(document).on('scroll', '#ulId', function () {
                //     console.log('Event Fired');
                // });

            })(jQuery);


    },
    updated: function () {
        var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
        var numberofmonths = generalInformation.systemSettings.calendarDisplay;
        $("#deptDate01, #deptDate02").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            onSelect: function (selectedDate) {
                $("#retDate").datepicker("option", "minDate", selectedDate);
                $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);

            }
        });

        $("#retDate").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#deptDate01").val();
                $("#retDate").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) { }
        });

        $("#txtLeg2Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#deptDate01").val();
                $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) {
                $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#txtLeg3Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#txtLeg2Date").val();
                $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) {
                $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#txtLeg4Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#txtLeg3Date").val();
                $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) {
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#txtLeg5Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#txtLeg4Date").val();
                $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) {
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#txtLeg6Date").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: numberofmonths,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            beforeShow: function (event, ui) {
                var selectedDate = $("#txtLeg5Date").val();
                $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            },
            onSelect: function (selectedDate) { }
        });

    }
});


function isNullorUndefined(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) { status = true; }
    return status;
}