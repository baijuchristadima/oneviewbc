var HeaderComponent = Vue.component("headeritem", {
  template: `
    <div>
      <div class="header-top-area header_padding">
        <div class="container">
          <div class="top-badge">
            <a href="#"><img :src="headerContent.Badge_87_x_250px"></a>
          </div>
          <div class="row">
            <div class="col-xl-2 col-lg-2 col-md-12 home1 col-sm-12">
              <!-- START LOGO DESIGN AREA -->
              <div class="logo">
                <a href="index.html"><img alt="image" :src="headerContent.Logo_150_x_150px"></a>
              </div>
              <!-- END LOGO DESIGN AREA -->
            </div>
            <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12">
              <!-- START MENU DESIGN AREA -->
              <div class="mainmenu menu-home-1">
                <nav class="navbar navbar-expand-lg">
                  <a href="#" class="mob-nav-icon" v-if="!userlogined"><img src="headerContent.User_Logo_28_x_28px"></a>
                  <label id="bind-login-info" for="profile3" class="profile-dropdown  mob-nav-icon" v-if="userlogined">
                    <input type="checkbox" id="profile3">
                    <img v-if="[2,3,4].indexOf(userinfo.title?userinfo.title.id:1)<0" src="/assets/images/user.png">
                    <img v-else src="/assets/images/user_lady.png">
                    <!-- <span>{{userinfo.firstName}}</span> -->
                    <!-- <label for="profile2"><i class="fa fa-angle-down" aria-hidden="true"></i></label> -->
                    <ul>
                      <li><a href="/customer-profile.html"><i class="fa fa-th-large" aria-hidden="true"></i>My Dashboard</a></li>
                      <li><a href="/my-profile.html"><i class="fa fa-user" aria-hidden="true"></i>My Profile</a></li>
                      <li><a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i>My Bookings</a></li>
                      <li><a href="#" v-on:click.prevent="logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
                    </ul>
                  </label>
                  <!--ifnotlogin-->
                  <a class="mob-menu-bar" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <span><i class="fa fa-bars"></i></span>
                  </a>
                  <div class="collapse" id="collapseExample">
                    <div class="well">
                      <ul class="mob-nav-menu">
                        <li v-for="menu in headerContent.Menu_List"><a :href="menu.Url">{{menu.Menu_Name}}</a></li>
                      </ul>
                      <ul class="well-foot">
                        <li><a href="#" class="nav-icon"><img :src="headerContent.Email_Logo_31_x_22px"></a></li>
                        <li><a href="#" class="nav-icon"><img :src="headerContent.Phone_Logo_23_x_23px"></a></li>
                        <li>
                          <a role="button" data-toggle="collapse" href="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2">ENG
                          </a>
                        </li>
                      </ul>
                      <div class="collapse" id="collapseExample2">
                        <div class="well">
                          <ul class="mob-nav-menu">
                            <li><a href="#">ENG</a></li>
                            <li><a href="#">ARABIC</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="web-only-show">
                    <ul class="nav navbar-nav navbar-left ms-nav-4 ms-nav-10">
                      <li v-for="menu in headerContent.Menu_List"><a :href="menu.Url">{{menu.Menu_Name}}</a></li>
                    </ul>
                    <div class="right-icon-nav">
                      <ul class="nav navbar-nav navbar-right popup me-nav-2 nav-flex-option">
                        <li><a href="#" class="nav-icon"><img :src="headerContent.Email_Logo_31_x_22px"></a></li>
                        <li><a href="#" class="nav-icon"><img :src="headerContent.Phone_Logo_23_x_23px"></a></li>
                        <li class="sign-in-ico-hid" v-show="!userlogined"><a data-toggle="modal" data-target="#onboarding" class="nav-icon mob-none"><img :src="headerContent.User_Logo_28_x_28px"></a></li>
                        <li>
                          <label id="bind-login-info" for="profile2" class="profile-dropdown" v-if="userlogined">
                            <input type="checkbox" id="profile2">
                            <img v-if="[2,3,4].indexOf(userinfo.title?userinfo.title.id:1)<0" src="/assets/images/user.png">
                            <img v-else src="/assets/images/user_lady.png">
                            <span>{{userinfo.firstName}}</span>
                            <label for="profile2"><i class="fa fa-angle-down" aria-hidden="true"></i></label>
                            <ul>
                              <li><a href="/customer-profile.html"><i class="fa fa-th-large" aria-hidden="true"></i>My Dashboard</a></li>
                              <li><a href="/my-profile.html"><i class="fa fa-user" aria-hidden="true"></i>My Profile</a></li>
                              <li><a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i>My Bookings</a></li>
                              <li><a href="#" v-on:click.prevent="logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
                            </ul>
                          </label>
                        </li>
                        <li>
                          <div class="dropdown">
                            <div class="s-lang" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                              EN
                            </div>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                              <li><a class="dropdown-item" href="#">English</a></li>
                              <li><a class="dropdown-item" href="#">Arabic</a></li>
                            </ul>
                          </div>
                        </li>
                      </ul>
                    </div>
    
                  </div>
                </nav>
              </div>
              <!-- END MENU DESIGN AREA -->
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="onboarding" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <div class="model-text"><h3>Register with Nirvana Booking &amp; get following benefits</h3>
                  <ul>
                    <li><i class="fa fa-check-square-o"></i>Special offers for Flights, Hotels, Holidays</li>
                    <li><i class="fa fa-check-square-o"></i>Add and store traveller's information of family or friends</li>
                    <li><i class="fa fa-check-square-o"></i>Easy and quick checkout</li>
                    <li><i class="fa fa-check-square-o"></i>Manage Your Booking</li>
                  </ul>
                </div>
                <div class="model-form">
                  <h3 v-if="formType=='register'">Register</h3>
                  <h3 v-if="formType=='signin'">Sign in</h3>
                  <h3 v-if="formType=='forgot'">Reset Password</h3>
                  <hr>
                  <ul>
                    <li class="fb-model"><i class="fa fa-facebook"></i><a href="#">Facebook</a></li>
                    <li class="google-model"><i class="fa fa-google-plus"></i><a href="#">Google</a></li>
                  </ul>
    
                  <form v-if="formType=='register'">
                    <select v-model="registerUserData.title">
                      <option>Title</option>
                      <option>Mr</option>
                      <option>Ms</option>
                      <option>Mrs</option>
                    </select>
                    <input type="text" id="text" name="text" class="form-control name-se" :placeholder="signInAndRegister.First_name_placeholder" v-model="registerUserData.firstName">
                      <input type="text" id="text" name="text" class="form-control" :placeholder="signInAndRegister.Last_name_placeholder" v-model="registerUserData.lastName">
                      <input type="email" id="email" name="email" class="form-control" :placeholder="signInAndRegister.Email_placeholder" v-model="registerUserData.emailId">
                      <input type="checkbox" id="checkbox"><span>{{signInAndRegister.Register_message}}</span>
                      <p v-if="userFirstNameErormsg" style="color: red;margin-bottom:15px;">{{signInAndRegister.Firstname_error_message}}</p>
                      <p v-if="userLastNameErrormsg" style="color: red;margin-bottom:15px;">{{signInAndRegister.Lastname_error_message}}</p>
                      <p v-if="userEmailErormsg" style="color: red;margin-bottom:15px;">{{signInAndRegister.Email_error_message}}</p>
                      <input type="submit" id="submit" name="submit" :value="signInAndRegister.Register_button_name" @click.prevent="registerUser">
                  </form>
                  <form v-if="formType=='signin'">
                      <input type="email" id="email" name="email" class="form-control" :placeholder="signInAndRegister.Email_placeholder" v-model="username">
                      <input type="password" id="password" :placeholder="signInAndRegister.Password_placeholder" class="form-control" v-model="password">
                      <p v-if="usererrormsg" style="color: red;margin-bottom:15px;">{{signInAndRegister.Username_error_message}}</p>
                      <p v-if="psserrormsg" style="color: red;margin-bottom:15px;">{{signInAndRegister.Password_error_message}}</p>
                      <input type="submit" id="submit" name="submit" :value="signInAndRegister.Sign_in_button_name" @click.prevent="loginaction">
                    </form>
                    <form v-if="formType=='forgot'">
                        <input type="email" id="email" name="email" class="form-control" :placeholder="signInAndRegister.Email_placeholder" v-model="emailId">
                        <p v-if="userforgotErrormsg" style="color: red;margin-bottom:15px;">{{signInAndRegister.Email_error_message}}</p>
                        <input type="submit" id="submit" name="submit" :value="signInAndRegister.Reset_password_button_name" @click.prevent="forgotPassword">
                    </form>

                    <a v-if="formType=='signin'||formType=='register'" class="forgot-pass" href="#" @click.prevent="formType='forgot'">{{signInAndRegister.Forgot_password_link_name}}</a>
                    <a v-if="formType=='forgot'" class="forgot-pass" href="#" @click.prevent="formType='signin'">{{signInAndRegister.Back_to_login_name}}</a>
                    <p v-if="formType=='register'" class="cr-ac">{{signInAndRegister.Signin_link_question}} <a href="#" @click.prevent="formType='signin'">{{signInAndRegister.Sign_in_button_name}}</a></p>
                    <p v-if="formType=='signin'" class="cr-ac">{{signInAndRegister.Register_link_question}} <a href="#" @click.prevent="formType='register'">{{signInAndRegister.Register_button_name}}</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    `,
  data() {
    return {
      signInAndRegister: {},
      headerContent: {},
      username: '',
      password: '',
      emailId: '',
      retrieveEmailId: '',
      retrieveBookRefid: '',
      usererrormsg: false,
      psserrormsg: false,
      userFirstNameErormsg: false,
      userLastNameErrormsg: false,
      userEmailErormsg: false,
      userPasswordErormsg: false,
      userVerPasswordErormsg: false,
      userPwdMisMatcherrormsg: false,
      userPwdMisMatcherrormsg: false,
      userTerms: false,
      userforgotErrormsg: false,
      retrieveBkngRefErormsg: false,
      retrieveEmailErormsg: false,
      userlogined: this.checklogin(),
      userinfo: [],
      registerUserData: {
          firstName: '',
          lastName: '',
          emailId: '',
          password: '',
          verifyPassword: '',
          terms: '',
          title: 'Mr'
      },
      Languages: [],
      language: 'en',
      content: {area_List: []},
      getdata: true,
      active_el: (sessionStorage.active_el) ? sessionStorage.active_el : 1,
      isLoading: false,
      fullPage: true,
      PageName: '',
      DeviceInfo: '',
      sessionids: '',
      Ipaddress: '',
      pageType: '',
      formType: 'signin'
    };
  },
  methods: {
    loginaction: function () {
      if (!this.username) {
        this.usererrormsg = true;
        return false;
      } else if (!this.validEmail(this.username)) {
        this.usererrormsg = true;
        return false;
      } else {
        this.usererrormsg = false;
      }
      if (!this.password) {
        this.psserrormsg = true;
        return false;
      } else {
        this.psserrormsg = false;
        var self = this;
        login(this.username, this.password, function (response) {
          if (response == false) {
            self.userlogined = false;
            alert("Invalid username or password.");
          } else {
            self.userlogined = true;
            self.userinfo = JSON.parse(localStorage.User);
            $(".cd-signin-modal").removeClass("cd-signin-modal--is-visible");
            $("#onboarding").modal("hide");
            try {
              self.$eventHub.$emit("logged-in", {
                userName: self.username,
                password: self.password,
              });
              signArea.headerLogin({
                userName: self.username,
                password: self.password,
              });
            } catch (error) {}
          }
        });
      }
    },
    validEmail: function (email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },
    checklogin: function () {
      if (localStorage.IsLogin == "true") {
        this.userlogined = true;
        this.userinfo = JSON.parse(localStorage.User);
        return true;
      } else {
        this.userlogined = false;
        return false;
      }
    },
    logout: function () {
      this.userlogined = false;
      this.userinfo = [];
      localStorage.profileUpdated = false;
      try {
        this.$eventHub.$emit("logged-out");
        signArea.logout();
      } catch (error) {}
      commonlogin();
    },
    registerUser: function () {
      var vm = this;
      if (this.registerUserData.firstName == "") {
        this.userFirstNameErormsg = true;
        return false;
      } else {
        this.userFirstNameErormsg = false;
      }
      if (this.registerUserData.lastName == "") {
        this.userLastNameErrormsg = true;
        return false;
      } else {
        this.userLastNameErrormsg = false;
      }
      if (this.registerUserData.emailId == "") {
        this.userEmailErormsg = true;
        return false;
      } else if (!this.validEmail(this.registerUserData.emailId)) {
        this.userEmailErormsg = true;
        return false;
      } else {
        this.userEmailErormsg = false;
      }
      registerUser(this.registerUserData.emailId, this.registerUserData.firstName, this.registerUserData.lastName, this.registerUserData.title, function (response) {
        if (response.isSuccess == true) {
          vm.username = response.data.data.user.loginId;
          vm.password = response.data.data.user.password;
          login(vm.username, vm.password, function (response) {
            if (response != false) {
              $(".cd-signin-modal").removeClass("cd-signin-modal--is-visible");
              $("#onboarding").modal("hide");
              window.location.href = "/edit-my-profile.html?edit-profile=true";
            }
          });
        }
      });
    },

    forgotPassword: function () {
      var self = this;
      getAgencycode(function (response) {
        var agencyCodeResponse = response;

        if (self.emailId == "") {
          self.userforgotErrormsg = true;
          return false;
        } else {
          self.userforgotErrormsg = false;
        }

        var datas = {
          emailId: self.emailId,
          agencyCode: agencyCodeResponse,
          logoUrl: window.location.origin + "/" + localStorage.AgencyFolderName + "/website-informations/logo/logo.png",
          websiteUrl: window.location.origin,
          resetUri: window.location.origin + "/" + localStorage.AgencyFolderName + "/reset-password.html",
        };

        $(".model-form input[type=submit]").css("cssText", "pointer-events:none;background:grey !important;");

        var huburl = ServiceUrls.hubConnection.baseUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var requrl = ServiceUrls.hubConnection.hubServices.forgotPasswordUrl;
        axios
          .post(huburl + portno + requrl, datas)
          .then(response => {
            if (response.data != "") {
              alert(response.data);
            } else {
              alert("Error in forgot password. Please contact admin.");
            }
            $(".model-form input[type=submit]").css("cssText", "pointer-events:auto;background:#3a7fc1 !important");
          })
          .catch(err => {
            console.log("FORGOT PASSWORD  ERROR: ", err);
            if (err.response.data.message == "No User is registered with this emailId.") {
              alert(err.response.data.message);
            } else {
              alert("We have found some technical difficulties. Please contact admin!");
            }
            $(".model-form input[type=submit]").css("cssText", "pointer-events:auto;background:#3a7fc1 !important");
          });
      });
    },

    retrieveBooking: function () {
      if (this.retrieveEmailId == "") {
        //alert('Email required !');
        this.retrieveEmailErormsg = true;
        return false;
      } else if (!this.validEmail(this.retrieveEmailId)) {
        //alert('Invalid Email !');
        this.retrieveEmailErormsg = true;
        return false;
      } else {
        this.retrieveEmailErormsg = false;
      }
      if (this.retrieveBookRefid == "") {
        this.retrieveBkngRefErormsg = true;
        return false;
      } else {
        this.retrieveBkngRefErormsg = false;
      }
      var bookData = {
        BkngRefID: this.retrieveBookRefid,
        emailId: this.retrieveEmailId,
        redirectFrom: "retrievebooking",
        isMailsend: false,
      };
      localStorage.bookData = JSON.stringify(bookData);
      window.location.href = "/Flights/flight-confirmation.html";
    },

    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    activate: function (el) {
      sessionStorage.active_el = el;
      this.active_el = el;
    },
    getpagecontent: function () {
      var self = this;
      self.isLoading = true;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = localStorage.Languagecode ? localStorage.Languagecode : "en";
        var homecms = "/persons/source?path=/B2B/AdminPanel/CMS/" + Agencycode + "/Template/Master Page/Master Page/Master Page.ftl";
        var cmsurl = huburl + portno + homecms;
        axios
          .get(cmsurl, {
            headers: {
              "content-type": "text/html",
              Accept: "text/html",
              "Accept-Language": langauage,
            },
          })
          .then(function (response) {
            if (response) {
              var headerSection = pluck("Header_Section", response.data.area_List);
              self.headerContent = getAllMapData(headerSection[0].component);
              var headerSection = pluck("Signin_and_Register", response.data.area_List);
              self.signInAndRegister = getAllMapData(headerSection[0].component);
            }
           
            self.isLoading = false;
          })
          .catch(function (error) {
            console.log("Error");
            this.content = [];
          });
      });
    },
    async cmsRequestData(callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          "Content-Type": "application/json",
        },
        body: data, // body data type must match "Content-Type" header
      });
      try {
        const myJson = await response.json();
        return myJson;
      } catch (error) {
        return object;
      }
    },
  },
  mounted: function () {
    if (localStorage.IsLogin == "true") {
      this.userlogined = true;
      this.userinfo = JSON.parse(localStorage.User);
    } else {
      this.userlogined = false;
    }
    this.getpagecontent();
    // setTimeout(() =>{
    //     this.Getvisit();
    //  },1000)
  },
  watch: {
    updatelogin: function () {
      if (localStorage.IsLogin == "true") {
        this.userlogined = true;
        this.userinfo = JSON.parse(localStorage.User);
      } else {
        this.userlogined = false;
      }
    },
  },
});
var headerinstance = new Vue({
  el: "header",
  name: "headerArea",
  data() {
    return {
      key: "",
      content: null,
      getdata: true,
    };
  },
});
Vue.prototype.$eventHub = new Vue({
  data: {},
});
var FooterComponent = Vue.component("footeritem", {
  template: `
  <div>
  <div class="container footer-widgets">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 wow fadeInLeft" data-wow-delay="0.1s" style="visibility: visible; -webkit-animation-delay: 0.1s; -moz-animation-delay: 0.1s; animation-delay: 0.1s;">
      <div class="widget text-center">
        <img alt="image" class="footer-logo" :src="footerContent.Logo_150_x_150px">
        <div class="social-block text-center">
          <h4>{{footerContent.Follow_Us_Label}}</h4>
          <div class="social-icon">
            <a :href="item.Url" v-for="item in footerContent.Social_Media_List"><i class="lab" v-bind:class="item.Icon"></i></a>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 wow fadeInDown" data-wow-delay="0.1s" style="visibility: visible; -webkit-animation-delay: 0.1s; -moz-animation-delay: 0.1s; animation-delay: 0.1s;">
          <div class="widget">
            <ul class="widget-menu mar-3">
              <li v-for="(menuItem,index) in footerContent.Menu_List" v-if="index % 2 === 0"><a href="#">{{menuItem.Menu_Name}}</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; -webkit-animation-delay: 0.1s; -moz-animation-delay: 0.1s; animation-delay: 0.1s;">
          <div class="widget">
            <ul class="widget-menu">
            <li v-for="(menuItem,index) in footerContent.Menu_List" v-if="index % 2 !== 0"><a href="#">{{menuItem.Menu_Name}}</a></li>
            </ul>
          </div>
        </div>
        <!-- fourth block -->
        <div class="col-xs-12 col-sm-6 col-md-12 col-lg-4 wow fadeInRight" data-wow-delay="0.1s" style="visibility: visible; -webkit-animation-delay: 0.1s; -moz-animation-delay: 0.1s; animation-delay: 0.1s;">
          <div class="widget">
            <h4 class="text-center">{{footerContent.Newsletter_Description}}
            </h4>
            <div class="vertical-space-40"></div>
            <div class="vertical-space-10"></div>
            <form class="d-block">
              <div class="mb-3">
                <input type="text" class="form-control" id="exampleFormControlInput1" :placeholder="footerContent.Name_Placeholder">
              </div>
              <div class="mb-3">
                <input type="email" class="form-control" id="exampleFormControlInput2" :placeholder="footerContent.Email_Placeholder">
              </div>
              <div class="mb-3 d-block">
                <button type="submit" class="btn-subscribe">{{footerContent.Button_Label}}</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      
      <!-- third block -->
    </div>
    
  </div>
</div>
<div class="footer-bottom">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="footer-text text-center">
          <h6>{{footerContent.Copyright_Content}}</h6>
          <!--<script>
          document.querySelector('#copyright-year').innerText = new Date().getFullYear();
          </script>-->
        </div>
      </div>
    </div>
  </div>
</div>
</div>
    `,
  data() {
    return {
      footerContent:{}
    };
  },
  props: {
    newKey: String,
  },
  methods: {
    selectLocation: function (brnch) {
      this.currentLocationDetails = brnch;
    },
    getpagecontent: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = localStorage.Languagecode ? localStorage.Languagecode : "en";
        var homecms = "/persons/source?path=/B2B/AdminPanel/CMS/" + Agencycode + "/Template/Master Page/Master Page/Master Page.ftl";
        var cmsurl = huburl + portno + homecms;
        axios
          .get(cmsurl, {
            headers: { "content-type": "text/html", Accept: "text/html", "Accept-Language": langauage },
          })
          .then(function (response) {
            if(response){
              var footerSection = pluck('Footer_Section', response.data.area_List);
              self.footerContent = getAllMapData(footerSection[0].component);
            }
          })
          .catch(function (error) {
            console.log("Error");
          });
      });
    },
    // sendnewsletter: async function () {
    //   if (!this.newsltrname) {
    //     alertify.alert("Alert", "Name required.").set("closable", false);
    //     return false;
    //   }
    //   if (!this.newsltremail) {
    //     alertify.alert("Alert", "Email Id required.").set("closable", false);
    //     return false;
    //   }
    //   var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    //   var matchArray = this.newsltremail.match(emailPat);
    //   if (matchArray == null) {
    //     alertify.alert("Alert", "Your email address seems incorrect.").set("closable", false);
    //     return false;
    //   } else {
    //     var fromEmail = JSON.parse(localStorage.User).loginNode.email;

    //     var custmail = {
    //       type: "UserAddedRequest",
    //       fromEmail: fromEmail,
    //       toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
    //       logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
    //       agencyName: JSON.parse(localStorage.User).loginNode.name || "",
    //       agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
    //       personName: this.newsltrname,
    //       primaryColor: "#" + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
    //       secondaryColor: "#" + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary,
    //     };
    //     let agencyCode = JSON.parse(localStorage.User).loginNode.code;
    //     var filterValue = "type='Subscribe Details' AND keyword3='Contact Us'  AND keyword2='" + this.newsltremail + "'";
    //     var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

    //     if (allDBData != undefined && allDBData.length > 0) {
    //       alertify.alert("Alert", "Email address already enabled.").set("closable", false);
    //       return false;
    //     }
    //     let requestedDate = moment(String(new Date())).format("YYYY-MM-DDThh:mm:ss");
    //     let insertSubscibeData = { type: "Subscribe Details", keyword1: this.newsltrname, keyword2: this.newsltremail, keyword3: "Contact Us", date1: requestedDate, nodeCode: agencyCode };

    //     var huburl = ServiceUrls.hubConnection.cmsUrl;
    //     var portno = ServiceUrls.hubConnection.ipAddress;
    //     const url = huburl + portno + "/cms/data";

    //     let responseObject = await this.gettingCMSData(url, "POST", insertSubscibeData, "en", "");
    //     try {
    //       let insertID = Number(responseObject);
    //       var emailApi = ServiceUrls.emailServices.emailApi;
    //       sendMailService(emailApi, custmail);
    //       alertify.alert("Newsletter", "Thank you for subscribing !");
    //     } catch (e) {}
    //   }
    // },
    // async gettingCMSData(cmsURL, methodName, bodyData, languageCode, type) {
    //   var data = bodyData;
    //   if (data != null && data != undefined) {
    //     data = JSON.stringify(data);
    //   }
    //   const response = await fetch(cmsURL, {
    //     method: methodName, // *GET, POST, PUT, DELETE, etc.
    //     credentials: "same-origin", // include, *same-origin, omit
    //     headers: { "Content-Type": "application/json", "Accept-Language": languageCode },
    //     body: data, // body data type must match "Content-Type" header
    //   });
    //   try {
    //     const myJson = await response.json();
    //     return myJson;
    //   } catch (error) {
    //     let object = { area_List: [] };
    //     return object;
    //   }
    // },
    // async getDbData4Table(agencyCode, extraFilter, sortField) {
    //   var allDBData = [];
    //   var huburl = ServiceUrls.hubConnection.cmsUrl;
    //   var portno = ServiceUrls.hubConnection.ipAddress;
    //   var cmsURL = huburl + portno + "/cms/data/search/byQuery";
    //   var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
    //   if (extraFilter != undefined && extraFilter != "") {
    //     queryStr = queryStr + " AND " + extraFilter;
    //   }
    //   var requestObject = {
    //     query: queryStr,
    //     sortField: sortField,
    //     from: 0,
    //     orderBy: "desc",
    //   };
    //   let responseObject = await this.gettingCMSData(cmsURL, "POST", requestObject, "en", "");
    //   if (responseObject != undefined && responseObject.data != undefined) {
    //     allDBData = responseObject.data;
    //   }
    //   return allDBData;
    // },

    // sendcontactus: async function () {
    //   if (!this.cntusername) {
    //     alertify.alert("Alert", "Name required.").set("closable", false);

    //     return false;
    //   }
    //   if (!this.cntemail) {
    //     alertify.alert("Alert", "Email Id required.").set("closable", false);
    //     return false;
    //   }
    //   var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    //   var matchArray = this.cntemail.match(emailPat);
    //   if (matchArray == null) {
    //     alertify.alert("Alert", "Your email address seems incorrect.").set("closable", false);
    //     return false;
    //   }
    //   if (!this.cntcontact) {
    //     alertify.alert("Alert", "Mobile number required.").set("closable", false);
    //     return false;
    //   }
    //   if (this.cntcontact.length < 8) {
    //     alertify.alert("Alert", "Enter valid mobile number.").set("closable", false);
    //     return false;
    //   }
    //   if (!this.cntsubject) {
    //     alertify.alert("Alert", "Subject required.").set("closable", false);
    //     return false;
    //   }

    //   if (!this.cntmessage) {
    //     alertify.alert("Alert", "Message required.").set("closable", false);
    //     return false;
    //   } else {
    //     var fromEmail = JSON.parse(localStorage.User).loginNode.email;

    //     var footer = this.pluck("Footer", this.content.area_List);
    //     var toEmail = this.pluckcom("To_Email", footer[0].component);
    //     var departmentName = "";
    //     if (this.selectedDepartment != undefined && this.selectedDepartment.Title != undefined && this.selectedDepartment.Email_ID != undefined) {
    //       toEmail = this.selectedDepartment.Email_ID;
    //       departmentName = this.selectedDepartment.Title;
    //     }

    //     var postData = {
    //       type: "AdminContactUsRequest",
    //       fromEmail: fromEmail,
    //       toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
    //       logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
    //       agencyName: JSON.parse(localStorage.User).loginNode.name || "",
    //       agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
    //       personName: this.cntusername,
    //       emailId: this.cntemail,
    //       contact: this.cntcontact,
    //       message: this.cntmessage,
    //       primaryColor: "#" + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
    //       secondaryColor: "#" + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary,
    //     };
    //     var custmail = {
    //       type: "UserAddedRequest",
    //       fromEmail: fromEmail,
    //       toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
    //       logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
    //       agencyName: JSON.parse(localStorage.User).loginNode.name || "",
    //       agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
    //       personName: this.cntusername,
    //       primaryColor: "#" + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
    //       secondaryColor: "#" + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary,
    //     };

    //     let agencyCode = JSON.parse(localStorage.User).loginNode.code;
    //     let requestedDate = moment(String(new Date())).format("YYYY-MM-DDThh:mm:ss");
    //     let insertContactData = {
    //       type: "Contact Us",
    //       keyword1: this.cntusername,
    //       keyword2: this.cntemail,
    //       keyword3: this.cntcontact,
    //       keyword4: this.cntsubject,
    //       keyword5: departmentName,
    //       text1: this.cntmessage,
    //       date1: requestedDate,
    //       nodeCode: agencyCode,
    //     };
    //     var huburl = ServiceUrls.hubConnection.cmsUrl;
    //     var portno = ServiceUrls.hubConnection.ipAddress;
    //     const url = huburl + portno + "/cms/data";
    //     let responseObject = await this.gettingCMSData(url, "POST", insertContactData, "en", "");
    //     try {
    //       let insertID = Number(responseObject);
    //       var emailApi = ServiceUrls.emailServices.emailApi;

    //       sendMailService(emailApi, postData);
    //       sendMailService(emailApi, custmail);
    //       this.cntemail = "";
    //       this.cntusername = "";
    //       this.cntcontact = "";
    //       this.cntmessage = "";
    //       this.cntsubject = "";
    //       alertify.alert("Contact us", "Thank you for contacting us.We shall get back to you.");
    //     } catch (e) {}
    //   }
    // },
  },
  mounted: function () {
    this.getpagecontent();
  },
  // created: function () {
  //   this.$eventHub.$on("select-location", this.selectLocation);
  // },
  // beforeDestroy: function () {
  //   this.$eventHub.$off("select-location");
  // },
});
var footerinstance = new Vue({
  el: "footer",
  name: "footerArea",
  data() {
    return {
      key: 0,
      content: null,
      getdata: true,
    };
  },
});
