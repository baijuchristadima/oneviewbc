$(document).ready(function () {
    if (localStorage.direction == 'rtl') {
        $("#rangeSlider").removeClass("range_sec");
    }

});

//Hotel Search Niravan B2C start
Vue.directive('click-outside', {
    bind: function (el, binding, vnode) {
        el.clickOutsideEvent = function (event) {
            // here I check that click was outside the el and his childrens
            if (!(el == event.target || el.contains(event.target))) {
                // and if it did, call method provided in attribute value
                if (vnode.context[binding.expression]) {
                    vnode.context[binding.expression](event);
                }
            }
        };
        document.body.addEventListener('click', el.clickOutsideEvent)
    },
    unbind: function (el) {
        document.body.removeEventListener('click', el.clickOutsideEvent)
    },
});


var hotelRooms = Vue.component("hotel-rooms", {
    data: function () {
        return {
            adults: [
                { 'value': 1 },
                { 'value': 2 },
                { 'value': 3 },
                { 'value': 4 },
                { 'value': 5 },
                { 'value': 6 },
                { 'value': 7 }
            ],
            children: [
                { 'value': 0 },
                { 'value': 1 },
                { 'value': 2 },

            ],
            childrenAges: [
                { 'value': 1, },
                { 'value': 2, },
                { 'value': 3, },
                { 'value': 4, },
                { 'value': 5, },
                { 'value': 6, },
                { 'value': 7, },
                { 'value': 8, },
                { 'value': 9, },
                { 'value': 10 },
                { 'value': 11 },
                { 'value': 12 },
                // { 'value': 13 },
                // { 'value': 14 },
                // { 'value': 15 },
                // { 'value': 16 },
                // { 'value': 17 }
            ],
            selectedAdults: 1,
            selectedChildren: 0,
            selectedChildrenAges: [
                { child1: 1 },
                { child2: 1 }
            ]
        };
    },
    props: {
        roomId: Number,
        getRoomDetails: Function,
        childLabel: String,
        childrenLabel: String,
        adultLabel: String,
        adultsLabel: String,
        ageLabel: String,
        agesLabel: String,
        roomLabel: String
    },
    mounted: function () {
        this.$nextTick(function () {
            if (localStorage.direction == 'rtl') {
                //  this.arabic_dropdown = "dwn_icon";
                $(".en_dwn").addClass("dwn_icon");
            } else {
                //  this.arabic_dropdown = "";
                $(".en_dwn").removeClass("dwn_icon");
            }
        })
        this.$watch(function (vm) { return vm.selectedAdults, vm.selectedChildren, vm.selectedChildrenAges, Date.now(); },
            function () {
                // Executes if data above have changed.
                this.$emit("get-room-details", {
                    roomId: this.roomId,
                    selectedAdults: this.selectedAdults,
                    selectedChildren: this.selectedChildren,
                    selectedChildrenAges: this.selectedChildrenAges
                })
                this.$nextTick(function () {
                    if (localStorage.direction == 'rtl') {
                        //  this.arabic_dropdown = "dwn_icon";
                        $(".en_dwn").addClass("dwn_icon");
                    } else {
                        //  this.arabic_dropdown = "";
                        $(".en_dwn").removeClass("dwn_icon");
                    }
                })
            }
        )
    },
    methods: {
        addAdult: function() {
            if (this.selectedAdults <= 6) {
                this.selectedAdults++;
            }
        },
        removeAdult: function(){
            if (this.selectedAdults > 1) {
                this.selectedAdults--;
            }
        },
        addChild: function() {
            if (this.selectedChildren <= 1) {
                this.selectedChildren++;
            }
        },
        removeChild: function(){
            if (this.selectedChildren > 1) {
                this.selectedChildren--;
            }
        }
    },
    template: `
    <div>
    <div class="d-flex room-no">
    <h5>Room {{roomId+1}}</h5>
    </div>
    <div class="row mb-3 ml-0 arrange-l-p">
    <div class="col-12 arrange-l-p">
        <label for="exampleFormControlInput10" class="form-label num-spinner">Adults</label>
        <div class="number-spinner">
        <span class="ns-btn">
            <a href="#" @click.prevent="removeAdult"><span class="icon-minus"></span></a>
        </span>
        <input type="text" class="pl-ns-value" v-model.lazy="selectedAdults" readonly>
        <span class="ns-btn">
            <a href="#" @click.prevent="addAdult"><span class="icon-plus"></span></a>
        </span>
        </div>
    </div>
    <div class="col-12">
        <label for="exampleFormControlInput10" class="form-label num-spinner">Child <span>2-11 Yrs</span></label>
        <div class="number-spinner">
        <span class="ns-btn">
            <a @click.prevent="removeChild"><span class="icon-minus"></span></a>
        </span>
        <input type="text" class="pl-ns-value" :value="selectedChildren">
        <span class="ns-btn">
            <a @click.prevent="addChild"><span class="icon-plus"></span></a>
        </span>
        </div>
    </div>
    </div>
</div>
    `
});

var hotelSeatch = Vue.component("hotel-search", {
    data: function () {
        return {
            //city search
            isCompleted: false,
            keywordSearch: "",
            autoCompleteResult: [],
            cityCode: "",
            cityName: "",
            //supplier select
            showSupplierList: false,
            supplierList: [],
            selectedSuppliers: [],
            supplierSearchLabel: "All Suppliers",
            //coutries
            countryOfResidence: "AE",
            nationality: "AE",
            // datepicker
            checkIn: "",
            checkOut: "",
            numberOfNights: 1,
            numberOfRooms: 1,
            roomSelectionDetails: {
                room1: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room2: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room3: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room4: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room5: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                }
            },
            KMrange: 5,
            arabic_dropdown: '',
            highlightIndex: 0,
            content: {},
            searchForm: [],
            Hotel_form_caption: "",
            Hotel_city_placeholder: "",
            childLabel: "",
            childrenLabel: "",
            adultLabel: "",
            adultsLabel: "",
            ageLabel: "",
            agesLabel: "",
            roomLabel: "",
            roomsLabel: "",
            rangeLabel: "",
            searchBtnLabel: "",
            Search_range_label: "",
            validationMessage: ""
        }
    },
    created: function () {
        var agencyNode = window.localStorage.getItem("User");
        if (agencyNode) {
            agencyNode = JSON.parse(agencyNode);
            var servicesList = agencyNode.loginNode.servicesList;
            for (var i = 0; i < servicesList.length; i++) {
                if (servicesList[i].name == "Hotel" && servicesList[i].provider.length > 0) {
                    for (var j = 0; j < servicesList[i].provider.length; j++) {
                        this.supplierList.push({ id: servicesList[i].provider[j].id, name: servicesList[i].provider[j].name.toUpperCase(), supplierType: servicesList[i].provider[j].supplierType });
                        this.selectedSuppliers = this.supplierList.map(function (supplier) { return supplier.id });
                    }
                    break;
                } else if (servicesList[i].name == "Hotel" && servicesList[i].provider.length == 0) {
                    //setLoginPage("login");
                }
            }
            // this.getPagecontent();

        }
    },
    computed: {
        selectAll: {
            get: function () {
                return this.supplierList ? this.selectedSuppliers.length == this.supplierList.length : false;
            },
            set: function (value) {
                var selectedSuppliers = [];

                if (value) {
                    this.supplierList.forEach(function (supplier) {
                        selectedSuppliers.push(supplier.id);
                    });
                }

                this.selectedSuppliers = selectedSuppliers;
            }
        },
        noOfGuests: function() {
            var rooms = "";
            var adultCount = 0;
            var childCount = 0;

            for (var roomIndex = 1; roomIndex <= parseInt(this.numberOfRooms); roomIndex++) {
                var roomSelectionDetails = this.roomSelectionDetails['room' + roomIndex];
                adultCount += roomSelectionDetails.adult;
                childCount += roomSelectionDetails.children;
                rooms += "&room" + roomIndex + "=ADT_" + roomSelectionDetails.adult + ",CHD_" + roomSelectionDetails.children;
                for (var childIndex = 0; childIndex < roomSelectionDetails.childrenAges.length; childIndex++) {
                    rooms += "_" + roomSelectionDetails.childrenAges[childIndex]['child' + (childIndex + 1)];
                }
            }

            return {adultCount, childCount}
        }
    },
    methods: {
        onClickAutoCompleteEvent: function () {
            // if (this.autoCompleteResult.length > 0) {
            this.isCompleted = true;
            // } else {
            // this.isCompleted = false;
            // }
        },
        onKeyUpAutoCompleteEvent: _.debounce(function (keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
                var cityName = keywordEntered;
                var cityarray = cityName.split(' ');
                var uppercaseFirstLetter = "";
                for (var k = 0; k < cityarray.length; k++) {
                    uppercaseFirstLetter += cityarray[k].charAt(0).toUpperCase() + cityarray[k].slice(1).toLowerCase()
                    if (k < cityarray.length - 1) { uppercaseFirstLetter += " "; }
                }
                uppercaseFirstLetter = "*" + uppercaseFirstLetter + "*";
                var lowercaseLetter = "*" + cityName.toLowerCase() + "*";
                var uppercaseLetter = "*" + cityName.toUpperCase() + "*";


                var should = [];
                var i = 0;
                var supplier_type = "";
                for (var key in self.supplierList) {
                    // check if the property/key is defined in the object itself, not in parent
                    if (self.supplierList.hasOwnProperty(key)) {
                        supplier_type = [self.supplierList[key].supplierType, "Both"];
                        var supplierIndex = self.selectedSuppliers.indexOf(self.supplierList[key].id);
                        if (supplierIndex > -1) {

                            var supplier_id_query = {
                                bool: {
                                    must: [{
                                        match_phrase: { supplier_id: self.selectedSuppliers[supplierIndex] }
                                    },
                                    {
                                        terms: { supplier_type: supplier_type }
                                    }
                                    ]
                                }
                            };

                            should.push(supplier_id_query);
                            i++;
                        }
                    }
                }

                var query = {
                    query: {
                        bool: {
                            must: [{
                                bool: {
                                    should: [
                                        { wildcard: { supplier_city_name: uppercaseFirstLetter } },
                                        { wildcard: { supplier_city_name: uppercaseLetter } },
                                        { wildcard: { supplier_city_name: lowercaseLetter } }
                                    ]
                                }
                            },
                            { bool: { should } }
                            ]
                        }
                    }
                };

                var client = new elasticsearch.Client({
                    host: [{
                        host: ServiceUrls.elasticSearch.elasticsearchHost,
                        auth: ServiceUrls.elasticSearch.auth,
                        protocol: ServiceUrls.elasticSearch.protocol,
                        port: ServiceUrls.elasticSearch.port
                    }],
                    log: 'trace'
                });

                client.search({ index: 'city_map', size: 150, body: query }).then(function (resp) {
                    finalResult = [];
                    var hits = resp.hits.hits;
                    var Citymap = new Map();
                    for (var i = 0; i < hits.length; i++) {
                        Citymap.set(hits[i]._source.oneview_city_id, hits[i]._source);
                    }
                    var get_values = Citymap.values();
                    var Cityvalues = [];
                    for (var ele of get_values) {
                        Cityvalues.push(ele);
                    }
                    var results = SortInputFirst(cityName, Cityvalues);
                    for (var i = 0; i < results.length; i++) {
                        finalResult.push({
                            "code": results[i].oneview_city_id,
                            "label": results[i].supplier_city_name + ", " + results[i].oneview_country_name,
                            "location": results[i].location

                        });
                    }
                    var newData = [];
                    finalResult.forEach(function (item, index) {
                        if (item.label.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {

                            newData.push(item);
                        }
                    });
                    console.log(newData);
                    self.autoCompleteResult = newData;
                    self.isCompleted = true;
                    self.highlightIndex = 0;
                });

            } else {
                this.autoCompleteResult = [];
                this.isCompleted = false;
            }
        }, 100),
        updateResults: function (item) {
            this.keywordSearch = item.label;
            this.autoCompleteResult = [];
            this.cityCode = item.code;
            this.cityName = item.label;
            window.sessionStorage.setItem("cityLocation", JSON.stringify(item.location));
            $('#txtCheckInDate').focus();

        },
        showSupplierListDropdown: function () {
            if (this.supplierList.length != 1) {
                this.showSupplierList = !this.showSupplierList;
            }
            this.isCompleted = false;
        },
        getRoomDetails: function (data) {
            var selectedRoom = this.roomSelectionDetails['room' + (data.roomId + 1)];
            selectedRoom.adult = data.selectedAdults;
            selectedRoom.children = data.selectedChildren;
            if (data.selectedChildren == 0) {
                selectedRoom.childrenAges = [];
            } else if (data.selectedChildren == 1) {
                selectedRoom.childrenAges = [data.selectedChildrenAges[0]];
            } else if (data.selectedChildren == 2) {
                selectedRoom.childrenAges = data.selectedChildrenAges;
            }

            this.roomSelectionDetails['room' + (data.roomId + 1)] = selectedRoom;
        },
        setNumberOfNights: function () {
            var dt2 = $('#txtCheckOutDate');
            var startDate = $('#txtCheckInDate').datepicker('getDate');
            var a = $("#txtCheckInDate").datepicker('getDate').getTime();
            var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
            var c = 24 * 60 * 60 * 1000;
            var diffDates = Math.round(Math.abs((a - b) / c));

            if (event.target.value.match(/[^0-9\.]/gi)) {
                this.validationMessage ='Only numbers allowed.';
                this.numberOfNights = diffDates;
            } else if (event.target.value.match(/[d*\.]/gi)) {
                this.validationMessage ='Only whole numbers allowed.';
                this.numberOfNights = diffDates;
            } else if (event.target.value > 90) {
                this.validationMessage ='Maximum 90 nights allowed.';
                this.numberOfNights = diffDates;
            } else if (event.target.value == 0) {
                this.validationMessage ='Minimum one night allowed.';
                this.numberOfNights = diffDates;
            } else if (event.target.value == "") {
                this.numberOfNights = diffDates;
            } else {
                startDate.setDate(startDate.getDate() + this.numberOfNights);
                dt2.datepicker('setDate', startDate);
                eDate = dt2.datepicker('getDate').getTime();
                this.checkOut = $("#txtCheckOutDate").val();
            }
        },
        searchHotels: function () {
            var rooms = "";
            var adultCount = 0;
            var childCount = 0;

            for (var roomIndex = 1; roomIndex <= parseInt(this.numberOfRooms); roomIndex++) {
                var roomSelectionDetails = this.roomSelectionDetails['room' + roomIndex];
                adultCount += roomSelectionDetails.adult;
                childCount += roomSelectionDetails.children;
                rooms += "&room" + roomIndex + "=ADT_" + roomSelectionDetails.adult + ",CHD_" + roomSelectionDetails.children;
                for (var childIndex = 0; childIndex < roomSelectionDetails.childrenAges.length; childIndex++) {
                    rooms += "_" + roomSelectionDetails.childrenAges[childIndex]['child' + (childIndex + 1)];
                }
            }

            if (this.cityName.trim() == "" || this.cityCode == "") {
                this.validationMessage = "Please enter city name.";
            } else if (this.selectedSuppliers.length == 0) {
                this.validationMessage ='Please select supplier.';
            } else if (this.checkIn == "") {
                this.validationMessage ='Please select check in date.';
            } else if (this.checkOut == "") {
                this.validationMessage ='Please select check out date.';
            } else if (this.countryOfResidence == "") {
                this.validationMessage ='Please select country of residence.';
            } else if (this.nationality == "") {
                this.validationMessage ='Please select nationality.';
            } else if (this.numberOfNights == "") {
                this.validationMessage ='Please enter number of nights.';
            } else if (this.numberOfRooms == "") {
                this.validationMessage ='Please select number of rooms.';
            } else if (this.numberOfNights > 90) {
                this.validationMessage ='Maximum nights allowed in a single booking is 90.';
            } else if (parseInt(adultCount + childCount) > 9) {
                this.validationMessage ='Maximum occupancy in a single booking is 9.';
            } else {
                this.validationMessage = "";
                var checkIn = $('#txtCheckInDate').val() == "" ? "" : $('#txtCheckInDate').datepicker('getDate');
                var checkOut = $('#txtCheckOutDate').val() == "" ? "" : $('#txtCheckOutDate').datepicker('getDate');

                var searchCriteria = "nationality=" + this.nationality +
                    "&country_of_residence=" + this.countryOfResidence +
                    "&city_code=" + this.cityCode +
                    "&city=" + this.cityName +
                    "&cin=" + moment(checkIn).format('DD/MM/YYYY') +
                    "&cout=" + moment(checkOut).format('DD/MM/YYYY') +
                    "&night=" + this.numberOfNights +
                    "&adtCount=" + adultCount +
                    "&chdCount=" + childCount +
                    "&guest=" + (adultCount + childCount) +
                    "&num_room=" + this.numberOfRooms + rooms +
                    "&KMrange=" + this.KMrange +
                    // "&supplier=" + this.selectedSuppliers.join(",") +
                    // "&sort=" + "price-a" +
                    "&uuid=" + uuidv4();
                var uri = "/Hotels/hotel-listing.html?" + searchCriteria;
                 
                window.location.href = uri;
            }

        },
        hideSupplier: function () {
            this.showSupplierList = false;
        },
        hideCity: function () {
            this.isCompleted = false;
        },
        up: function () {
            if (this.isCompleted) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.isCompleted = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.isCompleted) {
                if (this.highlightIndex < this.autoCompleteResult.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.autoCompleteResult.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.isCompleted = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
           
            var liH = this.$refs.options[this.highlightIndex].clientHeight;
            if (liH == 50) {
                liH = 32;
            }
            if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
            }
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
     
        },
        addRooms: function() {
            if (this.numberOfRooms < 5) {
                this.numberOfRooms++;
            }
        },
        removeRooms: function() {
            if (this.numberOfRooms > 1) {
                this.numberOfRooms--;
            }
        }
    },
    watch: {
        selectedSuppliers: function () {
            if (this.selectedSuppliers.length == this.supplierList.length && this.supplierList.length != 1) {
                this.supplierSearchLabel = 'All Suppliers';
            } else if (this.selectedSuppliers.length == 0) {
                this.supplierSearchLabel = 'No supplier selected';
            } else if (this.selectedSuppliers.length > 4) {
                this.supplierSearchLabel = this.selectedSuppliers.length + ' supplier selected'
            } else {
                var vm = this;
                this.supplierSearchLabel = this.selectedSuppliers.map(function (id) {
                    return vm.supplierList.filter(function (supplier) {
                        return id == supplier.id;
                    })[0].name;
                }).join(",");
            }
            this.autoCompleteResult = [];
            this.keywordSearch = "";
        }
    },
    mounted: function () {



        if (localStorage.direction == 'rtl') {
            // $("#rangeSlider").removeClass("range_sec ");
            $("#rangeSlider").addClass("range_sec");
            this.arabic_dropdown = "dwn_icon";

            $(".ar_direction").addClass("ar_direction1");
        } else {
            $("#rangeSlider").removeClass("range_sec");

            this.arabic_dropdown = "";
            $(".ar_direction").removeClass("ar_direction1");
        }



        var vm = this;
        this.$nextTick(function () {
            var generalInformation = {
                systemSettings: {
                    systemDateFormat: 'dd M y,D',
                    calendarDisplay: 1,
                    calendarDisplayInMobile: 1
                },
            }

            var setDate = function (days) { return moment(new Date).add(days, 'days').format("DD/MM/YYYY"); };

            var startDate = setDate(1);
            var endDate = setDate(2);

            var dateFormat = generalInformation.systemSettings.systemDateFormat;

            var tempnumberofmonths = 1;
            // if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
            //     tempnumberofmonths = 2;
            // } else if (parseInt($(window).width()) > 999) {
            //     tempnumberofmonths = 3;
            // }

            var sDate = new Date(moment(startDate, "DD/MM/YYYY")).getTime();
            var eDate = new Date(moment(endDate, "DD/MM/YYYY")).getTime();

            //Checkin Date
            $("#txtCheckInDate").datepicker({
                dateFormat: dateFormat,
                minDate: 0, // 0 for search
                maxDate: "360d",
                numberOfMonths: parseInt(tempnumberofmonths),
                onSelect: function (date) {

                    var dt2 = $('#txtCheckOutDate');
                    var startDate = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('getDate');
                    sDate = startDate.getTime();
                    startDate.setDate(startDate.getDate() + 1);
                    dt2.datepicker('option', 'minDate', startDate);
                    dt2.datepicker('setDate', minDate);
                    var a = $("#txtCheckInDate").datepicker('getDate').getTime();
                    var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
                    var c = 24 * 60 * 60 * 1000;
                    var diffDays = Math.round(Math.abs((a - b) / c));
                    vm.numberOfNights = diffDays;
                    vm.checkIn = $("#txtCheckInDate").val();
                    $('.ui-datepicker-current-day').click();
                },
                beforeShowDay: function (date) {

                    var container = $(".multi_dropBox");
                    container.hide();

                    if (date.getTime() == eDate) {
                        return [true, 'event event-selected event-selected-right', ''];
                    } else if (date.getTime() <= eDate && $(this).datepicker('getDate') && date.getTime() >= $(this).datepicker('getDate').getTime()) {
                        if (date.getTime() == $(this).datepicker('getDate').getTime()) {
                            return [true, 'event-selected event-selected-left', ''];
                        } else {
                            return [true, 'event-selected', ''];
                        }
                    } else {
                        return [true, '', ''];
                    }
                }
            });
            // $("#txtCheckInDate").datepicker({ dateFormat: dateFormat }).datepicker("setDate", new Date().getDay + 1);
            // vm.checkIn = $("#txtCheckInDate").val();

            //Checkout Date
            $('#txtCheckOutDate').datepicker({
                dateFormat: dateFormat,
                minDate: 2, // 2 for search
                maxDate: "360d",
                numberOfMonths: parseInt(tempnumberofmonths),
                onSelect: function (date) {
                    var a = $("#txtCheckInDate").datepicker('getDate').getTime();
                    var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
                    var c = 24 * 60 * 60 * 1000;
                    var diffDays = Math.round(Math.abs((a - b) / c));
                    if (diffDays > 90) {
                        alertify.alert('Maximum Nights', 'Maximum 90 nights allowed.');
                        vm.setNumberOfNights();
                    } else {
                        vm.numberOfNights = diffDays;
                        eDate = $(this).datepicker('getDate').getTime();
                    }
                    vm.checkOut = $("#txtCheckOutDate").val();
                },
                beforeShowDay: function (date) {
                    if (date.getTime() == sDate) {
                        return [true, 'event event-selected event-selected-left', ''];
                    } else if (date.getTime() >= sDate && $(this).datepicker('getDate') && date.getTime() <= $(this).datepicker('getDate').getTime()) {
                        if (date.getTime() == $(this).datepicker('getDate').getTime()) {
                            return [true, 'event-selected event-selected-right', ''];
                        } else {
                            return [true, 'event-selected', ''];
                        }
                    } else {
                        return [true, '', ''];
                    }
                }
            });

            // $("#txtCheckOutDate").datepicker({ dateFormat: dateFormat }).datepicker("setDate", new Date().getDay + 2);
            // vm.checkOut = $("#txtCheckOutDate").val();

            var stepSlider = document.getElementById('rangeSlider');

            if (stepSlider.noUiSlider) {
                stepSlider.noUiSlider.updateOptions({
                    direction: localStorage.direction == 'rtl' ? 'rtl' : 'ltr'
                });
            } else {
                noUiSlider.create(stepSlider, {
                    start: [5],
                    step: 1,
                    direction: localStorage.direction == 'rtl' ? 'rtl' : 'ltr',
                    range: {
                        'min': [1],
                        'max': [10]
                    }
                });
            }

            stepSlider.noUiSlider.on('update', function (values, handle) {
                vm.KMrange = parseInt(values[handle]);
            });

        });

    },
    template: `
    <div class="tab-pane" id="hotels-search" role="tabpanel">
    <form>
      <div class="row">
        <div class="col-sm-12 col-md-4 col-lg-3">
          <div class="autocomplete">
              <input type="text" class="loc-icon form-control" 
              id="hotel-search" placeholder="Search for hotels or places"
              autocomplete="off" 
              style="padding-right: 20px;" v-model="keywordSearch" 
              @input="onKeyUpAutoCompleteEvent(keywordSearch)"
              @keydown.down="down"
              @keydown.up="up"
              @keydown.tab="isCompleted=false"
              @keydown.esc="isCompleted=false"
              @keydown.enter="updateResults(autoCompleteResult[highlightIndex])"
              @click="onClickAutoCompleteEvent">
              <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
              <ul ref="searchautocomplete" class="autocomplete-results" v-if="isCompleted&&autoCompleteResult.length!=0">
                <li ref="options" :class="{'autocomplete-result-active' : index == highlightIndex}" class="autocomplete-result" v-for="(item,index) in autoCompleteResult" :key="index" @click="updateResults(item)">
                    {{ item.label }}
                </li>
              </ul>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
          <input type="text" name="txtCheckInDate" id="txtCheckInDate" placeholder="CHECK IN" readonly="readonly" class="cal-icon form-control textbox-n">
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
          <input type="text" name="txtCheckOutDate" id="txtCheckOutDate" placeholder="CHECK OUT" readonly="readonly"" class="cal-icon form-control textbox-n">
        </div>

        <div class="col-sm-12 col-md-4 col-lg-3 room-select text-end">
          <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
              {{numberOfRooms}} Room, {{noOfGuests.adultCount}} Adult,  {{noOfGuests.childCount}} Children
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">

            <hotel-rooms :child-label="childLabel" :children-label="childrenLabel" :adult-label="adultLabel" 
            :adults-label="adultsLabel" :age-label="ageLabel" :ages-label="agesLabel" :room-label="roomLabel"
            v-for="(room, index) in parseInt(numberOfRooms)" :key="index" :roomId="index" @get-room-details="getRoomDetails">
            </hotel-rooms>
            <div class="d-flex">
            <button class="flex-grow-1 btn-add-room" @click.prevent="addRooms">Add room</button>
            <button v-if="numberOfRooms>1" class="btn-done-room" @click.prevent="removeRooms">Remove Room</button>
        </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
      <div v-if="validationMessage" class="validation_home" v-cloak>{{validationMessage}}</div>
        <div class="col-sm-12 text-end ta-large-right">
          <button type="submit" class="btn btn-nirvana-primary mt-3 mb-3" @click.prevent="searchHotels">Search</button>
        </div>
      </div>
      <div class="col-md-2 col-sm-4 search-col-padding" v-show="false">
      <div class="cont_txt km_slider">
        <label class="within_sec" style="font-size: 12px !important;font-weight: 400 !important;">{{Search_range_label}}</label>
        <div id="rangeSlider"></div>
        <label id="slider-step-value">{{KMrange}} {{rangeLabel}}</label>
        </div>
      </div>
    </form>

  </div>
    `
});

//Hotel Search Niravan B2C end

/**Sorting Function for AutoCompelte Start**/
function SortInputFirst(input, data) {
    var first = [];
    var others = [];
    for (var i = 0; i < data.length; i++) {
        if (data[i].supplier_city_name.toLowerCase().startsWith(input.toLowerCase())) {
            first.push(data[i]);
        } else {

            others.push(data[i]);
        }
    }
    first.sort(function (a, b) {
        var nameA = a.supplier_city_name.toLowerCase(),
            nameB = b.supplier_city_name.toLowerCase()
        if (nameA < nameB) //sort string ascending
            return -1
        if (nameA > nameB)
            return 1
        return 0 //default return value (no sorting)
    })
    others.sort(function (a, b) {
        var nameA = a.supplier_city_name.toLowerCase(),
            nameB = b.supplier_city_name.toLowerCase()
        if (nameA < nameB) //sort string ascending
            return -1
        if (nameA > nameB)
            return 1
        return 0 //default return value (no sorting)
    })
    return (first.concat(others));
}
/**Sorting Function for AutoCompelte End**/

/**UUID Generation Start**/
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
/**UUID Generation End**/