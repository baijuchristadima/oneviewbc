generalInformation = {
    systemSettings: {
        calendarDisplay: 1,

    },
};
var images = [];
var currimg = 0;
const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var flightserchfromComponent = Vue.component('flightserch', {
    data() {
        return {
            access_token: '',
            KeywordSearch: '',
            resultItemsarr: [],
            autoCompleteProgress: false,
            highlightIndex: 0
        }
    },
    props: {
        itemText: String,
        itemId: String,
        placeHolderText: String,
        returnValue: Boolean,
        id: { type: String, default: '', required: false },
        leg: Number,
        //KeywordSearch:String
    },

    template: `<div class="autocomplete">
        <input type="text" :placeholder="placeHolderText" :id="id" :data-aircode="KeywordSearch" autocomplete="off" 
        v-model="KeywordSearch" class="formtxt" 
        :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !autoCompleteProgress  }" 
        @input="onSelectedAutoCompleteEvent(KeywordSearch)"
            @keydown.down="down"
            @keydown.up="up"           
            @keydown.esc="autoCompleteProgress=false"
            @keydown.enter="onSelected(resultItemsarr[highlightIndex])"
            @keydown.tab="tabclick(resultItemsarr[highlightIndex])"/>
        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>        
        <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length>0">
            <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item)">
                {{ item.label }}
            </li>
        </ul>
    </div>`,


    methods: {
        up: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex < this.resultItemsarr.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
            if (this.$refs.options[this.highlightIndex]) {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            }

        },
        onSelectedAutoCompleteEvent: _.debounce(async function (keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
              this.autoCompleteProgress = true;
              self.resultItemsarr = await getFlightResultUsingElasticSearch(keywordEntered);

            } else {
                this.autoCompleteProgress = false;
                this.resultItemsarr = [];

            }
        }, 100),
        onSelected: function (item) {
            this.KeywordSearch = item.label;
            this.resultItemsarr = [];
            var targetWhenClicked = $(event.target).parent().parent().find("input").attr("id");
            if (maininstance.triptype != 'M') {
                if (event.target.id == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
                    $('#Cityto1').focus();
                } else if (event.target.id == "Cityto1" || targetWhenClicked == "Cityto1") {
                    $('#deptDate01').focus();
                }
            } else {
                var eventTarget = event.target.id;
                $(document).ready(function () {
                    if (eventTarget == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
                        $('#Cityto1').focus();
                    } else if (eventTarget == "Cityto1" || targetWhenClicked == "Cityto1") {
                        $('#deptDate01').focus();
                    } else if (eventTarget == "DeparturefromLeg1" || targetWhenClicked == "DeparturefromLeg1") {
                        $('#ArrivalfromLeg1').focus();
                    } else if (eventTarget == "ArrivalfromLeg1" || targetWhenClicked == "ArrivalfromLeg1") {
                        $('#txtLeg1Date').focus();
                    } else if (eventTarget == "DeparturefromLeg2" || targetWhenClicked == "DeparturefromLeg2") {
                        $('#ArrivalfromLeg2').focus();
                    } else if (eventTarget == "ArrivalfromLeg2" || targetWhenClicked == "ArrivalfromLeg2") {
                        $('#txtLeg2Date').focus();
                    } else if (eventTarget == "DeparturefromLeg3" || targetWhenClicked == "DeparturefromLeg3") {
                        $('#ArrivalfromLeg3').focus();
                    } else if (eventTarget == "ArrivalfromLeg3" || targetWhenClicked == "ArrivalfromLeg3") {
                        $('#txtLeg3Date').focus();
                    } else if (eventTarget == "DeparturefromLeg4" || targetWhenClicked == "DeparturefromLeg4") {
                        $('#ArrivalfromLeg4').focus();
                    } else if (eventTarget == "ArrivalfromLeg4" || targetWhenClicked == "ArrivalfromLeg4") {
                        $('#txtLeg4Date').focus();
                    } else if (eventTarget == "DeparturefromLeg5" || targetWhenClicked == "DeparturefromLeg5") {
                        $('#ArrivalfromLeg5').focus();
                    } else if (eventTarget == "ArrivalfromLeg5" || targetWhenClicked == "ArrivalfromLeg5") {
                        $('#txtLeg5Date').focus();
                    } else if (eventTarget == "DeparturefromLeg6" || targetWhenClicked == "DeparturefromLeg6") {
                        $('#ArrivalfromLeg6').focus();
                    } else if (eventTarget == "ArrivalfromLeg6" || targetWhenClicked == "ArrivalfromLeg6") {
                        $('#txtLeg6Date').focus();
                    }
                });

            }

            this.$emit('air-search-completed', item.code, item.label, this.leg);


        },
        tabclick: function (item) {
            if (!item) {

            } else {
                this.onSelected(item);
            }
        }

    },
    watch: {
        returnValue: function () {
            this.KeywordSearch = this.itemText;
        }

    }

});
var maininstance = new Vue({
    i18n,
    el: '#indexmain',
    name: 'indexmain',
    data: {
        titleDetails: { Title: '' },
        titleComponent: {Banner_Title:''},
        isMobileOrNot:false,
        banner: null,
        // content: null,
        MainArea: {
            Flights_Label: '',
            Hotels_Label: '',
            Holidays_Label: '',
            Holidays_Label: '',
            Oneway_Label: '',
            Round_Trip_Label: '',
            Multi_City_Label: '',
            Package_Title: '',
            Roomslabel:''
        },
        PromotionArea: {
            Image: '',
            Save_More_On_Label:'',
            Title:'',
            Description:'',
        },
        getpackage:false,
        destinations:null,
        air:null,
        packageArea:{

Destination_Title:'',
Airline_Title:''

        },
        // contentFlight:null,
        flightDeals: null,
        holidayPackages: null,
        triptype: "R",
        CityFrom: '',
        CityTo: '',
        validationMessage: '',
        adtcount: 1,
        chdcount: 0,
        infcount: 0,
        preferAirline: '',
        returnValue: true,
        legcount: 2,
        legs: [],
        cityList: [],
        adt: "adult",

        cabinclass: [{ 'value': 'Y', 'text': 'Economy' },
        { 'value': 'C', 'text': 'Business' },
        { 'value': 'F', 'text': 'First' }
        ],
        selected_cabin: 'Y',
        selected_adults: 1,
        selected_children: 0,
        selected_infant: 0,
        totalAllowdPax: 9,
        //selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
        //CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        arabic_dropdown: '',
        childLabel: "",
        childrenLabel: "",
        adultLabel: "",
        adultsLabel: "Adults",
        ageLabel: "year",
        agesLabel: "years",
        infantLabel: "Infant",
        searchBtnLabel: "Search",
        addUptoLabel: "add upto",
        tripsLabel: "trips",
        tripLabel: "trip",
        Totaltravaller: '1 Travellers, Economy',
        travellerdisply: false,
        travellerdisplymul: false,
        child: 0,
        flightSearchCityName: { cityFrom1: '', cityTo1: '', cityFrom2: '', cityTo2: '', cityFrom3: '', cityTo3: '', cityFrom4: '', cityTo4: '', cityFrom5: '', cityTo5: '', cityFrom6: '', cityTo6: '' },
        advncedsearch: false,
        isLoading: false,
        direct_flight: false,
        airlineList: AirlinesDatas,
        Airlineresults: [],
        selectedAirline: [],
        adultrange: '',
        childrange: '',
        infantrange: '',
        donelabel: 'Done',
        classlabel: 'class',
        hotelInit: Math.random(), //hotel init 
        iName: null,
        iDob: null,
        iPassportNo: null,
        iGender: null,
        iPhoneno: null,
        iEmail: null,
        iOccupation: null,
        iDestination: null,
        iPeriod: null,
        iDepartureDate: null,
        iReturnDate: null,
        iReturnDate: null,
        iAddress: null,
        iAddressOffice: null,
        iPurpose: null,
        insterms: true,
        cartimedisplay: false,
        actvetab:sessionStorage.active_el?(sessionStorage.active_el==0||sessionStorage.active_el==4)?1:sessionStorage.active_el:1 
    },
    methods: {
        pageContent: function () {
            var self = this;

            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Home/Home/Home.ftl';
                var holidaypackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Package/Package/Package.ftl';
                // var flightdealurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Flight deals/Flight deals/Flight deals.ftl';
                // self.eVisaUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/EVISA Page/EVISA Page/EVISA Page.ftl';
                // self.popupurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/News and Notice/News and Notice/News and Notice.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    if (response.data.area_List.length) {
                        var titleComponent = self.pluck('Title', self.content.area_List);
                        self.titleComponent.Banner_Title = self.pluckcom('Banner_Title', titleComponent[0].component);

                        var bannerCompnt = self.pluckcom('Banner_Image', titleComponent[0].component);
                        self.banner = bannerCompnt;

                        var Main_Area = self.pluck('Main_Area', self.content.area_List);
                        self.MainArea.Flights_Label = self.pluckcom('Flights_Label', Main_Area[0].component);
                        self.MainArea.Hotels_Label = self.pluckcom('Hotels_Label', Main_Area[0].component);
                        self.MainArea.Holidays_Label = self.pluckcom('Holidays_Label', Main_Area[0].component);
                        self.MainArea.Oneway_Label = self.pluckcom('Oneway_Label', Main_Area[0].component);
                        self.MainArea.Round_Trip_Label = self.pluckcom('Round_Trip_Label', Main_Area[0].component);
                        self.MainArea.Multi_City_Label = self.pluckcom('Multi_City_Label', Main_Area[0].component);
                        self.MainArea.Package_Title = self.pluckcom('Package_Title', Main_Area[0].component);
                        //self.MainArea.Roomslabel = self.pluckcom('RoomsLabel', Main_Area[0].component);

                        var PromotionArea = self.pluck('Promotion_Area', self.content.area_List);
                        self.PromotionArea.Image = self.pluckcom('Image', PromotionArea[0].component);
                        self.PromotionArea.Save_More_On_Label = self.pluckcom('Save_More_On_Label', PromotionArea[0].component);
                        self.PromotionArea.Title = self.pluckcom('Title', PromotionArea[0].component);
                        self.PromotionArea.Description = self.pluckcom('Description', PromotionArea[0].component);
            



                        var packageArea = self.pluck('Top_Destination_and_Airlines', self.content.area_List);
                        self.packageArea.Destination_Title = self.pluckcom('Destination_Title', packageArea[0].component);
                        self.packageArea.Airline_Title = self.pluckcom('Airline_Title', packageArea[0].component);

                        var desti = self.pluck('Top_Destination_and_Airlines', self.content.area_List);
                        self.destinations =  self.pluckcom('Destination', desti[0].component);

                        var desti = self.pluck('Top_Destination_and_Airlines', self.content.area_List);
                        self.air =  self.pluckcom('Airlines', desti[0].component);
                      



                   

                       // self.bindBanner();
                        self.holidayPackageCard(holidaypackageurl, langauage);
                        self.flightDealsCard(flightdealurl, langauage);

                        

                    }




                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });




        },
        holidayPackageCard(holidaypackageurl, langauage) {
            var self = this;
            axios.get(holidaypackageurl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {


                let holidayaPackageListTemp = [];
                if (response != undefined && response.data != undefined && response.data.Values != undefined) {
                    holidayaPackageListTemp = response.data.Values.filter(function (el) {
                        return el.Status == true &&
                            el.Show_In_Home_Page == true
                    });
                }
                self.holidayPackages = holidayaPackageListTemp;
                if (self.holidayPackages != null) {
                    self.getpackage = true;
                    setTimeout(function () { setCarousel() }, 10);
                }
                self.$nextTick(function(){
                    $(".pageload").hide();
                });
            }).catch(function (error) {
                console.log('Error');
                self.flightDeals = [];
            });

        },
        flightDealsCard(flightdealurl, langauage) {
            if (flightdealurl != undefined && langauage != undefined) {
                
            var self = this;
            axios.get(flightdealurl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
                // self.contentPackage = response.data.Values;
                var DealListTemp = [];
                if (response != undefined && response.data != undefined && response.data.Values != undefined) {
                    DealListTemp = response.data.Values.filter(function (el) {
                        return el.Status == true &&
                            el.Show_In_Home_Page == true
                    });
                    if (DealListTemp.length > 0) {
                        self.flightDeals = DealListTemp;
                      //  setTimeout(function () { dealsCarousel() }, 10);
                    }

                }


            }).catch(function (error) {
                console.log('Error');
                self.flightDeals = [];
            });

        }



        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = null;
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        Departurefrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (AirportCode == this.CityTo) {
                    this.returnValue = false;
                    alertify.alert('Alert', 'Departure and arrival airports should not be same !');

                    this.CityFrom = '';

                } else {
                    this.returnValue = true;
                    this.CityFrom = AirportCode;
                }
            } else {
                var from = 'cityFrom' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].to) {
                        this.returnValue = false;
                        this.flightSearchCityName[from] = "";
                        alertify.alert('Alert', 'Departure and arrival airports should not be same !');
                    } else {
                        this.cityList[index].from = AirportCode
                        this.flightSearchCityName[from] = AirportName;
                        this.returnValue = true;
                    }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: AirportCode,
                        to: ''

                    });
                    this.flightSearchCityName[from] = AirportName;
                    this.returnValue = true;
                }
            }


        },
        Arrivalfrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (this.CityFrom == AirportCode) {
                    this.returnValue = false;
                    alertify.alert('Alert', 'Departure and arrival airports should not be same !');
                    this.validationMessage = "";
                    this.CityTo = '';

                } else {
                    this.returnValue = true;
                    this.CityTo = AirportCode;
                }
            } else {
                var to = 'cityTo' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].from) {
                        this.returnValue = false;
                        this.flightSearchCityName[to] = "";
                        alertify.alert('Alert', 'Departure and arrival airports should not be same !');

                    } else {
                        this.cityList[index].to = AirportCode;
                        this.flightSearchCityName[to] = AirportName;
                        this.returnValue = true;
                    }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: '',
                        to: AirportCode

                    });
                    this.flightSearchCityName[to] = AirportName;
                    this.returnValue = true;
                }
            }
        },
        SearchFlight: function () {

            var Departuredate = $('#deptDate01').val() == "" ? "" : $('#deptDate01').datepicker('getDate');
            if (!this.CityFrom) {
                alertify.alert('Alert', 'Please fill origin !');

                return false;
            }
            if (!this.CityTo) {
                alertify.alert('Alert', 'Please fill destination ! ');
                return false;
            }
            if (!Departuredate) {
                alertify.alert('Alert', 'Please choose departure date !').set('closable', false);
                return false;
            }
            var sec1TravelDate = moment(Departuredate).format('DD|MM|YYYY');

            var sectors = this.CityFrom + '-' + this.CityTo + '-' + sec1TravelDate;
            if (this.triptype == 'R') {
                var ArrivalDate = $('#retDate').val() == "" ? "" : $('#retDate').datepicker('getDate');
                if (!isNullorUndefined(ArrivalDate)) {
                    ArrivalDate = moment(ArrivalDate).format('DD|MM|YYYY');
                    if (!ArrivalDate) {
                        alertify.alert('Alert', 'Please choose return  date !').set('closable', false);

                        return false;
                    } else {
                        sectors += '/' + this.CityTo + '-' + this.CityFrom + '-' + ArrivalDate;
                    }
                } else {
                    alertify.alert('Alert', 'Please choose return date !').set('closable', false);

                    return false;
                }
            }
            var directFlight = this.direct_flight ? 'DF' : 'AF';

            var adult = this.selected_adults;
            var child = this.selected_children;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            getSuppliers([this.CityFrom + '|' + this.CityTo],
            function(supp) {
                var searchUrl = '/Flights/flight-listing.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                // searchUrl = searchUrl.toLocaleLowerCase();
                sessionStorage.active_el=1;
                window.location.href = searchUrl;
            })
        },
        AddNewLeg() {
            if (this.legcount <= 5) {
                ++this.legcount;
                var legno = 1;
                if (this.cityList.length > 0) {
                    legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                    legno = legno + 1;
                }

                this.cityList.push({
                    id: legno,
                    from: '',
                    to: ''
                });

            }
            console.log(this.cityList);
        },
        DeleteLeg(leg) {
            var legs = this.legcount;
            if (legs > 1) {
                --this.legcount;
                var legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    this.cityList.splice(index, 1);

                }


            }
        },
        setCalender() {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = generalInformation.systemSettings.calendarDisplay;
            $("#deptDate01").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#retDate").datepicker("option", "minDate", selectedDate);

                }
            });

            $("#retDate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) { }
            });
            $("#txtLeg1Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);

                }
            });
            $("#txtLeg2Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg1Date").val();
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg3Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg2Date").val();
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg4Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg3Date").val();
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg5Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg4Date").val();
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg6Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg5Date").val();
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) { }
            });


        },
        setpaxcount(item, action) {
            if (item == 'adt') {
                if (action == 'plus') {
                    if (this.selected_adults < this.totalAllowdPax) {
                        ++this.selected_adults;
                    }
                } else {
                    if (this.selected_adults > 1) {
                        --this.selected_adults;
                        if (this.selected_adults < this.selected_infant) {
                            this.selected_infant = this.selected_adults;
                        }

                    }
                }
            } else if (item == 'chd') {
                if (action == 'plus') {
                    if (this.selected_adults + this.selected_children < this.totalAllowdPax) {
                        ++this.selected_children;
                    }
                } else {
                    if (this.selected_children > 0) {
                        --this.selected_children;
                    }
                }
            } else if (item == 'inf') {
                if (action == 'plus') {
                    if (this.selected_adults + this.selected_children + this.selected_infant < this.totalAllowdPax) {
                        if (this.selected_infant < this.selected_adults) {
                            ++this.selected_infant
                        }

                    }
                } else {
                    if (this.selected_infant > 0) {
                        --this.selected_infant
                    }
                }
            } else { }
            if (this.selected_adults + this.selected_children > this.totalAllowdPax) {
                this.selected_children = 0;
            }

            if (this.selected_adults + this.selected_children + this.selected_infant > this.totalAllowdPax) {
                this.selected_infant = 0;

            }
            var cabin = getCabinName(this.selected_cabin);
            var totalpax = this.selected_adults + this.selected_children + this.selected_infant;
            if (parseInt(totalpax) > 1) {
                totalpax = totalpax + ' Passengers';
            } else {
                totalpax = totalpax + ' Passenger'
            }
            this.Totaltravaller = totalpax + ',' + cabin;
        },
        clickoutside: function () {
            this.triptype = 'O';
        },

        MultiSearchFlight: function () {
            var sectors = '';
            var legDetails = [];
            for (var legValue = 1; legValue <= this.legcount; legValue++) {
                var temDeparturedate = $('#txtLeg' + (legValue) + 'Date').val() == "" ? "" : $('#txtLeg' + (legValue) + 'Date').datepicker('getDate');
                if (temDeparturedate != "" && this.cityList.length != 0 && this.cityList[legValue - 1].from != "" && this.cityList[legValue - 1].to != "") {
                    var departureFrom = this.cityList[legValue - 1].from;
                    var arrivalTo = this.cityList[legValue - 1].to;
                    var travelDate = moment(temDeparturedate).format('DD|MM|YYYY');
                    sectors += '/' + departureFrom + '-' + arrivalTo + '-' + travelDate;
                    legDetails.push(departureFrom + '|' + arrivalTo)
                } else {
                    alertify.alert('Alert', 'Please fill the Trip ' + (legValue) + '   fields !').set('closable', false);


                    return false;
                }
            }
            var directFlight = this.direct_flight ? 'DF' : 'AF';

            var adult = this.selected_adults;
            var child = this.selected_children;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            getSuppliers(legDetails,
            function(supp) {
                var searchUrl = '/Flights/flight-listing.html?flight=' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                // searchUrl = searchUrl.toLocaleLowerCase();
                window.location.href = searchUrl;
            })
        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/Elitetravel/Holidaypackagedetails.html?page=" + url;
                    console.log(url);
                    sessionStorage.active_el=4;
                    window.location.href =url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;
            
          },
        dateConvert: function (utc) {
            // this.value=this.date
            return (moment(utc).format("DD-MMM-YYYY"));
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        }, closeInsurance: function () {
            if ($("#flightTab") != undefined) {
                $("#flightTab").click();
            }

        },
        swapLocations: function (id) {
            if ((this.CityFrom) && (this.CityTo)) {
                var from = this.CityFrom;
                var to = this.CityTo;
                this.CityFrom = to;
                this.CityTo = from;
                swpaloc(id)
            }
        },

        
        insureClaender: function () {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = generalInformation.systemSettings.calendarDisplay;
            $("#insdob").datepicker({
                maxDate: "0d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                changeYear: true,
                yearRange: '-100:+0'

            });
            $("#insdeparture").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#insreturn").datepicker("option", "minDate", selectedDate);

                }
            });

            $("#insreturn").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#insdeparture").val();
                    $("#insreturn").datepicker("option", "minDate", selectedDate);
                },

            });
        },
        getAmount: function (amount) {
            amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
            return amount;
        },
        isMobile: function () {
            if (navigator.userAgent.match(/Android/i) ||
                navigator.userAgent.match(/webOS/i) ||
                navigator.userAgent.match(/iPhone/i) ||
                navigator.userAgent.match(/iPad/i) ||
                navigator.userAgent.match(/iPod/i) ||
                navigator.userAgent.match(/BlackBerry/i) ||
                navigator.userAgent.match(/Windows Phone/i)) {
                return true;
            }
            else { return false; }
        },

    },
    updated: function () {
        this.setCalender();
    },
    mounted: function () {
        var vm = this;
        this.pageContent();
        this.flightDealsCard();
        this.holidayPackageCard();
        this.setCalender();
        this.insureClaender();
        this.isMobileOrNot = this.isMobile();
    },
    watch: {
        triptype: function () {
            if (this.triptype == "M") {
                this.travellerdisply = false;
            }
        }


    }
});


function setCarousel() {

   
    $("#holidaypackage").owlCarousel({
        items: 3,
        itemsCustom: false,
        itemsDesktop: [2000, 3],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: [600, 2],
        itemsMobile: [479, 1],
        singleItem: false,
        itemsScaleUp: false,
        slideSpeed: 1000,
        //Autoplay
        autoPlay: true,
        stopOnHover: true,

        // Navigation
        navigation: false,
        navigationText: ['<i class="fa  fa-angle-left"></i>', '<i class="fa  fa-angle-right"></i>'],
        rewindNav: true,
        scrollPerPage: false,

        //Pagination
        pagination: true,
        paginationNumbers: false,

        // Responsive 
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
    });
}


function getCabinName(cabinCode) {
    var cabinClass = 'Economy';
    if (cabinCode == "F") {
        cabinClass = "First Class";
    } else if (cabinCode == "C") {
        cabinClass = "Business";
    } else {
        try { cabinClass = getCabinClassObject(cabinCode).BasicClass; } catch (err) { }
    }
    return cabinClass;
}

function isNullorUndefined(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) { status = true; }
    return status;
}



$(function () {
  //  loadCountryPicker();
})
function swpaloc(id) {
    var from = $("#Cityfrom" + id).val();
    var to = $("#Cityto" + id).val();
    $("#Cityfrom" + id).val(to);
    $("#Cityto" + id).val(from);
}

