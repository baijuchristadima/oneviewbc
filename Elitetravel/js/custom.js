$(document).ready(function() {
    $(function() {
        var dateFormat = "dd/mm/yy",
            from = $("#from, #from1, #from2, #from3, #from4, #from5, #from6").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                }
            }).on("change", function() {
                to.datepicker("option", "minDate", getDate(this));
            }),
            to = $("#to, #to1, #to2, #to3, #to4, #to5, #to6").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                }
            }).on("change", function() {
                from.datepicker("option", "maxDate", getDate(this));
            });

        function getDate(element) {
            var date;
            try {
                date = $.datepicker.parseDate(dateFormat, element.value);
            } catch (error) {
                date = null;
            }
            return date;
        }
    });
});