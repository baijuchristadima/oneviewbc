const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var contact = new Vue({
    i18n,
    el: '#privacypolicy',
    name: 'privacypolicy',
    data: {
        pagecontent:{
            Breadcrumb_1:'',
            Breadcrumb_1:'',
            Privacy_and_Policy:''
        },
        banner:{
            Banner_Image:'',
            Banner_Title:''
            
        },

    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getBanner: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Inner Banners/Inner Banners/Inner Banners.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var banner = self.pluck('Privacy_Policy', self.content.area_List);
                    if (banner.length > 0) {
                        self.banner.Banner_Image = self.pluckcom('Banner_Image', banner[0].component);
                        self.banner.Banner_Title = self.pluckcom('Banner_Title', banner[0].component);
                    }
                })
            });
        },

        getPage: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Privacy Policy/Privacy Policy/Privacy Policy.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var pagecontent = self.pluck('Privacy_Policy', self.content.area_List);
                    if (pagecontent.length > 0) {
                        self.pagecontent.Breadcrumb_1 = self.pluckcom('Breadcrumb_1', pagecontent[0].component);
                        self.pagecontent.Breadcrumb_2 = self.pluckcom('Breadcrumb_2', pagecontent[0].component);
                        self.pagecontent.Privacy_and_Policy = self.pluckcom('Privacy_and_Policy', pagecontent[0].component);
                    }
                })
            });
        },
    },
    mounted: function () {
        this.getPage();
        this.getBanner();
    },
});