const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var contact = new Vue({
    i18n,
    el: '#contactuspage',
    name: 'contactuspage',
    data: {

        cntusername: '',
        cntemail: '',
        cntcontact: '',
        cntmessage: '',
        cntnationality:'',
        cntwebsite:'',
        banner:{
            Banner_Image:'',
            Banner_Title:'',
            Title:'',
            
        },
        pagecontent: {
            Breadcrum1: '',
            Breadcrumb2: '',
            Description:'',

          },
          contact:{
            Phone_Label:'',
            Email_Us_Label:'',
            Phone_Number:'',
            Address_Label:'',
            Address:''
          },
          form:{
            Name_Placeholder:'',
            Email_Placeholder:'',
            Phone_Placeholder:'',
            Website_Placeholder:'',
            Message_Placeholder:'',
            Button_Label:''
          }

    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getBanner: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Inner Banners/Inner Banners/Inner Banners.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var banner = self.pluck('Contact_Us', self.content.area_List);
                    if (banner.length > 0) {
                        self.banner.Banner_Image = self.pluckcom('Banner_Image', banner[0].component);
                        self.banner.Banner_Title = self.pluckcom('Banner_Title', banner[0].component);
                    }
                })
            });
        },
        getPageTitle: function () {
            var self = this;
            getAgencycode(function (response) {
              var Agencycode = response;
              var huburl = ServiceUrls.hubConnection.cmsUrl;
              var portno = ServiceUrls.hubConnection.ipAddress;
              var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
              var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact Us Page/Contact Us Page/Contact Us Page.ftl';
              //var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Package/Package/Package.ftl';
              axios.get(pageurl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
              }).then(function (response) {
                self.content = response.data;
                var pagecontent = self.pluck('Main_Page', self.content.area_List);
                if (pagecontent.length > 0) {
                  self.pagecontent.Breadcrum1 = self.pluckcom('Breadcrumb_1', pagecontent[0].component);
                  self.pagecontent.Breadcrumb2 = self.pluckcom('Breadcrumb_2', pagecontent[0].component);
                  self.pagecontent.Title = self.pluckcom('Title', pagecontent[0].component);
                  self.pagecontent.Description = self.pluckcom('Description', pagecontent[0].component);
      
                }
                var contact = self.pluck('Contact_Info', self.content.area_List);
                if (contact.length > 0) {
                  self.contact.Phone_Label = self.pluckcom('Phone_Label', contact[0].component);
                  self.contact.Email_Us_Label = self.pluckcom('Email_Us_Label', contact[0].component);
                  self.contact.Email = self.pluckcom('Email', contact[0].component);
                  self.contact.Phone_Number = self.pluckcom('Phone_Number', contact[0].component);
                  self.contact.Address_Label = self.pluckcom('Address_Label', contact[0].component);
                  self.contact.Address = self.pluckcom('Address', contact[0].component);
                }
                var form = self.pluck('Contact_Form', self.content.area_List);
                if (form.length > 0) {
                self.form.Name_Placeholder = self.pluckcom('Name_Placeholder', form[0].component);
                  self.form.Email_Placeholder = self.pluckcom('Email_Placeholder', form[0].component);
                  self.form.Phone_Placeholder = self.pluckcom('Phone_Placeholder', form[0].component);
                  self.form.Website_Placeholder = self.pluckcom('Website_Placeholder', form[0].component);
                  self.form.Message_Placeholder = self.pluckcom('Message_Placeholder', form[0].component);
                  self.form.Button_Label = self.pluckcom('Button_Label', form[0].component);
                }
              }).catch(function (error) {
                console.log('Error');
              });
            });
          },
        sendcontactus: async function () {
            if (!this.cntusername) {
                alertify.alert('Alert', 'Name required.').set('closable', false);

                return false;
            }
            if (!this.cntemail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.cntcontact) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            if (!this.cntwebsite) {
                alertify.alert('Alert', 'enter website').set('closable', false);
                return false;
            }

            if (!this.cntmessage) {
                alertify.alert('Alert', 'Message required.').set('closable', false);
                return false;
            } else {
                var frommail = JSON.parse(localStorage.User).loginNode.email;
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl +JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.cntusername,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Contact us",
                    keyword1: this.cntusername,
                    keyword2: this.cntemail,
                    number1: this.cntcontact,
                    keyword3: this.cntwebsite,
                    text1: this.cntmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, custmail);
                    this.cntemail = '';
                    this.cntusername = '';
                    this.cntcontact = '';
                    this.cntmessage = '';
                    this.cntnationality = '';
                    alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
                } catch (e) {

                }



            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
    },
    mounted: function () {
        this.getBanner();
        this.getPageTitle();
    },
});