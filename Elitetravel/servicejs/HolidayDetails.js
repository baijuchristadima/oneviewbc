const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var packageView = new Vue({
  i18n,
  el: '#Holidaydetails',
  name: 'Holidaydetails',
  data: {
    content:null,
    packagecontent:null,
    getpackage: false,
    getdata: false,
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
    CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    pagecontent: {
      Breadcrum1: '',
      Breadcrumb2: '',
    },
    maincontent: {
      Image: '',
      Title: '',
      Title: '',
      Price: '',
      Itinerary:null,
      Image_Caption:''
    },
    booking:{
      Full_Name_Label:'',
      Full_Name_Placeholder:'',
      Email_Address_Label:'',
      Email_Address_Placeholder:'',
      Phone_Number_Label:'',
      Phone_Number_Placeholder:'',
      Number_of_Person_Label:'',
      Tour_Date_Label:'',
      Number_of_Person_Placeholder:'',
      Button_Label:''
    },
    review:{},
    //booking form
    cntusername: '',
    cntemail: '',
    cntcontact: '',
    cntdate: '',
    cntperson:'',

    //review form
    username:'',
    email:'',
    contact:'',
    comment:'',
    overitems: null,
    banner:{
      Banner_Image:'',
      Banner_Title:''
    },
    allReview:[],
    pageURLLink:''
  },
  mounted() {

    this.getPageTitle();
    this.getBanner();
    this.viewReview();

  },
  methods: {

    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getBanner: function () {
      var self = this;
      getAgencycode(function (response) {
          var Agencycode = response;
          var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
          var huburl = ServiceUrls.hubConnection.cmsUrl;
          var portno = ServiceUrls.hubConnection.ipAddress;
          var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Inner Banners/Inner Banners/Inner Banners.ftl';
          axios.get(pageurl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
          }).then(function (response) {
              self.content = response.data;
              var banner = self.pluck('Package_Details_Page', self.content.area_List);
              if (banner.length > 0) {
                  self.banner.Banner_Image = self.pluckcom('Banner_Image', banner[0].component);
                  self.banner.Banner_Title = self.pluckcom('Banner_Title', banner[0].component);
              }
          })
      });
  },

    getPageTitle: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Package Page Details/Package Page Details/Package Page Details.ftl';
        //var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Package/Package/Package.ftl';
        axios.get(pageurl, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          var pagecontent = self.pluck('Page_Labels', self.content.area_List);
          if (pagecontent.length > 0) {
            self.pagecontent.Breadcrum1 = self.pluckcom('Breadcrumb_1', pagecontent[0].component);
            self.pagecontent.Breadcrumb2 = self.pluckcom('Breadcrumb_2', pagecontent[0].component);

          }
          var booking = self.pluck('Booking_Form_Labels', self.content.area_List);
          if (booking.length > 0) {
            self.booking.Full_Name_Label = self.pluckcom('Full_Name_Label', booking[0].component);
            self.booking.Full_Name_Placeholder = self.pluckcom('Full_Name_Placeholder', booking[0].component);
            self.booking.Email_Address_Label = self.pluckcom('Email_Address_Label', booking[0].component);
            self.booking.Email_Address_Placeholder = self.pluckcom('Email_Address_Placeholder', booking[0].component);
            self.booking.Phone_Number_Label = self.pluckcom('Phone_Number_Label', booking[0].component);
            self.booking.Phone_Number_Placeholder = self.pluckcom('Phone_Number_Placeholder', booking[0].component);
            self.booking.Number_of_Person_Label = self.pluckcom('Number_of_Person_Label', booking[0].component);
            self.booking.Tour_Date_Label = self.pluckcom('Tour_Date_Label', booking[0].component);
            self.booking.Number_of_Person_Placeholder = self.pluckcom('Number_of_Person_Placeholder', booking[0].component);
            self.booking.Button_Label = self.pluckcom('Button_Label', booking[0].component);
          }
          var review = self.pluck('Review', self.content.area_List);
          self.review.Reviews_Title = self.pluckcom('Reviews_Title', review[0].component);
          self.review.Write_A_Review_Label = self.pluckcom('Write_A_Review_Label', review[0].component);
          self.review.Name_Label = self.pluckcom('Name_Label', review[0].component);
          self.review.Email_Label = self.pluckcom('Email_Label', review[0].component);
          self.review.Phone_number_Label = self.pluckcom('Phone_number_Label', review[0].component);
          self.review.Comment_Label = self.pluckcom('Comment_Label', review[0].component);
          self.review.Button_Label = self.pluckcom('Button_Label', review[0].component);

          self.getdata = true;

        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
        var packageurl = getQueryStringValue('page');
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        if (packageurl != "") {
          packageurl = packageurl.split('-').join(' ');
          packageurl = packageurl.split('_')[0];
          var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
          self.pageURLLink = packageurl;
          axios.get(topackageurl, {
            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
          }).then(function (response) {
            
            var maincontent = self.pluck('Package', response.data.area_List);
            if (maincontent.length > 0) {
              self.packagecontent =maincontent[0].component;
              self.maincontent.Image = self.pluckcom('Image', maincontent[0].component);
              self.maincontent.Title = self.pluckcom('Title', maincontent[0].component);
              self.maincontent.Itinerary=self.pluckcom('Itinerary', maincontent[0].component);
              self.maincontent.Price=self.pluckcom('Price',maincontent[0].component);
              self.maincontent.Image_Caption=self.pluckcom('Image_Caption',maincontent[0].component);
            }
          })
        }
      });
    },
    getAmount: function (amount) {
      amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
      return amount;
    },
    sendbooking: async function () {
      let dateObj = document.getElementById("from");
      let dateValue = dateObj.value;
      if (!this.cntusername) {
          alertify.alert('Alert', 'Name required.').set('closable', false);

          return false;
      }
      if (!this.cntemail) {
          alertify.alert('Alert', 'Email Id required.').set('closable', false);
          return false;
      }
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.cntemail.match(emailPat);
      if (matchArray == null) {
          alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
          return false;
      }
      if (!this.cntcontact) {
          alertify.alert('Alert', 'Mobile number required.').set('closable', false);
          return false;
      }
      if (this.cntcontact.length < 8) {
          alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
          return false;
      }
      if (dateValue == undefined || dateValue == '') {
          alertify.alert('Alert', 'enter website').set('closable', false);
          return false;
      }

      if (!this.cntperson) {
          alertify.alert('Alert', 'Message required.').set('closable', false);
          return false;
      } else {
          var fromEmail = JSON.parse(localStorage.User).loginNode.email;
          var postData = postData = {
            type: "PackageBookingRequest",
            fromEmail: fromEmail,
            toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
            ccEmails: [],
            logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
            agencyName: JSON.parse(localStorage.User).loginNode.name || "",
            agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
            packegeName:this.maincontent.Title,
            personName:this.cntusername,
            emailAddress: this.cntemail,
            contact: this.cntcontact,
            departureDate: dateValue,
            adults: this.cntperson,
            child2to5: "NA",
            child6to11: "NA",
            infants: "NA",
            message: "NA",
            primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
            secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
          var custmail = {
              type: "UserAddedRequest",
              fromEmail: fromEmail,
              toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
              logo: ServiceUrls.hubConnection.logoBaseUrl +JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
              agencyName: JSON.parse(localStorage.User).loginNode.name || "",
              agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
              personName: this.cntusername,
              primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
              secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
          };
          let agencyCode = JSON.parse(localStorage.User).loginNode.code;
          dateValue = moment(String(dateValue)).format('YYYY-MM-DDThh:mm:ss');
          let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
          let insertContactData = {
              type: "Booking",
              keyword1: this.cntusername,
              keyword2: this.cntemail,
              number1: this.cntcontact,
              number2: this.cntperson,
              date1: dateValue,
              nodeCode: agencyCode,
              date2:requestedDate
          };
          let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
          try {
              let insertID = Number(responseObject);
            var emailApi = ServiceUrls.emailServices.emailApi;
              sendMailService(emailApi, postData);
              sendMailService(emailApi, custmail);
              this.cntemail = '';
              this.cntusername = '';
              this.cntcontact = '';
              this.cntperson = '';
              this.cntdate = '';
              dateObj.value = null;
              alertify.alert('Booking', 'Thank you for Booking.We shall get back to you.');
          } catch (e) {

          }



      }
  },
  sendReview: async function () {
    let ratings = this.getUserRating();
    console.log(ratings);
    ratings = Number(ratings);
    if (ratings == 0) {
      alertify.alert('Alert','Rating required');
      return;
    }
    if (!this.username) {
        alertify.alert('Alert', 'Name required.').set('closable', false);

        return false;
    }
    if (!this.email) {
        alertify.alert('Alert', 'Email Id required.').set('closable', false);
        return false;
    }
    var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    var matchArray = this.email.match(emailPat);
    if (matchArray == null) {
        alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
        return false;
    }
    if (!this.contact) {
        alertify.alert('Alert', 'Mobile number required.').set('closable', false);
        return false;
    }

    if (!this.comment) {
        alertify.alert('Alert', 'Comment required.').set('closable', false);
        return false;
    } else {
        var frommail = JSON.parse(localStorage.User).loginNode.email;
        var custmail = {
            type: "UserAddedRequest",
            fromEmail: frommail,
            toEmails: Array.isArray(this.email) ? this.email : [this.email],
            logo: ServiceUrls.hubConnection.logoBaseUrl +JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
            agencyName: JSON.parse(localStorage.User).loginNode.name || "",
            agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
            personName: this.username,
            primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
            secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
            type: "Reviews",
            keyword1: this.username,
            keyword2: this.email,
            keyword3: this.contact,
            text1: this.comment,
            keyword4: this.pageURLLink,
            number1:ratings,
            nodeCode: agencyCode,
            date1:requestedDate
        };
        let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
            let insertID = Number(responseObject);
          var emailApi = ServiceUrls.emailServices.emailApi;
          sendMailService(emailApi, custmail);
            this.email = '';
            this.username = '';
            this.contact = '';
            this.comment = '';
            this.viewReview();
            alertify.alert('Review', 'Thank you for Review.');
        } catch (e) {

        }



    }
},
viewReview: async function () {
  var self=this;
  let allReview = [];
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
      let requestObject = { from: 0, size: 100, type: "Reviews", nodeCode: agencyCode, orderBy: "desc" };
     let responseObject = await this.cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
        if (responseObject != undefined && responseObject.data != undefined) {
          allResult = JSON.parse(JSON.stringify(responseObject)).data;
          for (let i = 0; i < allResult.length; i++) {
            if (allResult[i].keyword4 == self.pageURLLink) {
              let object = {
                Name: allResult[i].keyword1,
                Date: moment(String(allResult[i].date1), "YYYY-MM-DDThh:mm:ss").format('MMM DD,YYYY'),
                comment: allResult[i].text1,
                Ratings: allResult[i].number1,
              };
              allReview.push(object);
            }
          }
          self.allReview =allReview;
      }
      
      });
},
userRating: function (num) {
  var ulList = document.getElementById("ratingID");
  let m = 0;
  for (let i = 0; i < ulList.childNodes.length; i++) {
    let childNode = ulList.childNodes[i];
    if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
      m = m + 1;
      for (let k = 0; k < childNode.childNodes.length; k++) {
        let style = childNode.childNodes[k].style.color;
        if ((m) < Number(num)) {
          childNode.childNodes[k].style = "color: rgb(239, 158, 8)";
        } else if ((m) == Number(num)) {
          if (style.trim() == "rgb(239, 158, 8)") {
            childNode.childNodes[k].style = "color: a9a9a9;";
          } else {
            childNode.childNodes[k].style = "color: rgb(239, 158, 8);";
          }
        } else {
          childNode.childNodes[k].style = "color: a9a9a9";
        }
      }
    }
  }
},
getUserRating: function () {
  var ulList = document.getElementById("ratingID");
  let m = 0;
  for (let i = 0; i < ulList.childNodes.length; i++) {
    let childNode = ulList.childNodes[i];
    if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
      let style = "";
      for (let k = 0; k < childNode.childNodes.length; k++) {
        style = childNode.childNodes[k].style.color;
        if (style != undefined && style != '') {
          break;
        }
      }
      if (style.trim() == "a9a9a9") {
        break;
      } else if (style.trim() == "rgb(239, 158, 8)") {
        m = m + 1;
      }
    }
  }
  return m;
},
  
  async cmsRequestData(callMethod, urlParam, data, headerVal) {
    var huburl = ServiceUrls.hubConnection.cmsUrl;
    var portno = ServiceUrls.hubConnection.ipAddress;
    const url = huburl + portno + "/" + urlParam;
    if (data != null) {
        data = JSON.stringify(data);
    }
    const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: { 'Content-Type': 'application/json' },
        body: data, // body data type must match "Content-Type" header
    });
    try {
        const myJson = await response.json();
        return myJson;
    } catch (error) {
        return object;
    }
},


  }
})
function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
function responsivetab() {
  //Horizontal Tab
  $('#parentHorizontalTab').easyResponsiveTabs({
    type: 'default', //Types: default, vertical, accordion
    width: 'auto', //auto or any width like 600px
    fit: true, // 100% fit in a container
    tabidentify: 'hor_1', // The tab groups identifier
    activate: function (event) { // Callback function if tab is switched
      var $tab = $(this);
      var $info = $('#nested-tabInfo');
      var $name = $('span', $info);
      $name.text($tab.text());
      $info.show();
    }
  });

  // Child Tab
  $('#ChildVerticalTab_1').easyResponsiveTabs({
    type: 'vertical',
    width: 'auto',
    fit: true,
    tabidentify: 'ver_1', // The tab groups identifier
    activetab_bg: '#fff', // background color for active tabs in this group
    inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
    active_border_color: '#c1c1c1', // border color for active tabs heads in this group
    active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
  });

  //Vertical Tab
  $('#parentVerticalTab').easyResponsiveTabs({
    type: 'vertical', //Types: default, vertical, accordion
    width: 'auto', //auto or any width like 600px
    fit: true, // 100% fit in a container
    closed: 'accordion', // Start closed if in accordion view
    tabidentify: 'hor_1', // The tab groups identifier
    activate: function (event) { // Callback function if tab is switched
      var $tab = $(this);
      var $info = $('#nested-tabInfo2');
      var $name = $('span', $info);
      $name.text($tab.text());
      $info.show();
    }
  });
}