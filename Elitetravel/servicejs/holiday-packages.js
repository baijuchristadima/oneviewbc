const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var packageList = new Vue({
    i18n,
    el: '#holidayPackages',
    name: 'holidayPackages',
    data: {
        content: null,
        getdata: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        pageContent: {
            Title: '',
            Breadcrumbpath1: '',
            Breadcrumb2: '',
            Common_image: '',
        },
        packages: [],
        getpackage: false,
        banner: {
            Banner_Image: '',
            Banner_Title: ''

        },
        search: '',
        daysearch: '',
        sortBy: '',
        Pricesearch: '',
        allReview:[]
    },
    computed: {
        filteredList() {
            return this.packages.filter(pack => pack.Title.toLowerCase().includes(this.search.toLowerCase())).filter(pack => pack.Days.includes(this.daysearch))
                .filter(pack => pack.Price.includes(this.Pricesearch))
                .sort((a, b) => {
                    if (this.sortBy == 'price_high') {
                        return b.Price - a.Price;
                    }
                    else if (this.sortyBy == 'price_low') {
                        return a.Price - b.Price;
                    }
                });
        }
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getBanner: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Inner Banners/Inner Banners/Inner Banners.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var banner = self.pluck('Holidays_Page', self.content.area_List);
                    if (banner.length > 0) {
                        self.banner.Banner_Image = self.pluckcom('Banner_Image', banner[0].component);
                        self.banner.Banner_Title = self.pluckcom('Banner_Title', banner[0].component);
                    }
                })
            });
        },

        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Package/Package/Package.ftl';
                axios.get(topackageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    var packageData = response.data;
                    let holidayaPackageListTemp = [];
                    if (packageData != undefined && packageData.Values != undefined) {
                        holidayaPackageListTemp = packageData.Values.filter(function (el) {
                            return el.Status == true
                        });
                    }
                    self.packages = holidayaPackageListTemp;
                    self.getpackage = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/Elitetravel/Holidaypackagedetails.html?page=" + url;
                    console.log(url);
                    window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;

        },
        viewReview: async function () {
            var self=this;
            
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestObject = { from: 0, size: 100, type: "Reviews", nodeCode: agencyCode, orderBy: "desc" };
               let responseObject = await this.cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
                  if (responseObject != undefined && responseObject.data != undefined) {
                    self.allReview = JSON.parse(JSON.stringify(responseObject)).data;                   
                   
                }
                
                });
          },
          async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
         getrating:function(url){
             var self=this;
            url = url.split("/Template/")[1];
            url = url.split(' ').join('-');
            url = url.split('.').slice(0, -1).join('.');
            url=url.split('_')[0];
            let reviewrate=[];
            reviewrate =self.allReview.filter(r=>r.keyword4==url);
            var sum = 0;
             if (reviewrate.length > 0) {
                 $.each(reviewrate, function () {
                     sum += this.number1?this.number1:0;
                 })
                 sum=sum/reviewrate.length;
             }
           console.log(sum);
             return sum;
         },
        getAmount: function (amount) {
            amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
            return amount;
        }

    },
    mounted: function () {
        this.viewReview();
        this.getPagecontent();
        this.getBanner();
    },

});
function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}