const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var contact = new Vue({
    i18n,
    el: '#aboutus',
    name: 'aboutus',
    data: {
        pagecontent:{
            Breadcrumb_1:'',
            Breadcrumb_1:'',
            Description:'',
            Title:''

        },
        banner:{
            Banner_Image:'',
            Banner_Title:''
            
        },
        abouts:{
            About:null
        }
        

    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getBanner: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Inner Banners/Inner Banners/Inner Banners.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var banner = self.pluck('About_Us', self.content.area_List);
                    if (banner.length > 0) {
                        self.banner.Banner_Image = self.pluckcom('Banner_Image', banner[0].component);
                        self.banner.Banner_Title = self.pluckcom('Banner_Title', banner[0].component);
                    }
                })
            });
        },

        getPage: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/About Us/About Us/About Us.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var pagecontent = self.pluck('Main_Area', self.content.area_List);
                    if (pagecontent.length > 0) {
                        self.pagecontent.Breadcrumb_1 = self.pluckcom('Breadcrumb_1', pagecontent[0].component);
                        self.pagecontent.Breadcrumb_2 = self.pluckcom('Breadcrumb_2', pagecontent[0].component);
                        self.pagecontent.Description = self.pluckcom('Description', pagecontent[0].component);
                        self.pagecontent.Title = self.pluckcom('Title', pagecontent[0].component);
                    }

                    var abouts = self.pluck('About_Us',self.content.area_List);
                    if (abouts.length > 0) {
                    self.abouts.Title = self.pluckcom('Title',abouts[0].component);
                    self.abouts.About=self.pluckcom('About', abouts[0].component);
                    // self.abouts.About=self.pluckcom('About',Abouts[0].component);
                    }
                })
            });
        },
    },
    mounted: function () {
        sessionStorage.active_el=1;
        this.getPage();
        this.getBanner();
    },
});