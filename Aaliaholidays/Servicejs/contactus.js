const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var contact = new Vue({
    i18n,
    el: '#contactuspage',
    name: 'contactuspage',
    data: {
        pageContent: {
            Title: '',
        },
        Form:{Name_label:''},
        //form
        cntusername: '',
        cntemail: '',
        cntcontact: '',
        cntmessage: '',
        cntsuject:'',
        Locations:{},
        Banner:{Banner_Image:''},
        Location:{}
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact Us Page/Contact Us Page/Contact Us Page.ftl';
                var banner = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Banners/Banners/Banners.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language':langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var pagecontent = self.pluck('Title_area', self.content.area_List);
                    if (pagecontent.length > 0) {
                        self.pageContent.Title = self.pluckcom('Title', pagecontent[0].component); 
                    }
                    var Form = self.pluck('Form_Area', self.content.area_List);
                    if (Form.length > 0) {
                        self.Form.Name_label = self.pluckcom('Name_label', Form[0].component);
                        self.Form.Name_Placeholder = self.pluckcom('Name_Placeholder', Form[0].component);
                        self.Form.Email_label = self.pluckcom('Email_label', Form[0].component);
                        self.Form.Email_Placeholder = self.pluckcom('Email_Placeholder', Form[0].component);
                        self.Form.Phone_Label = self.pluckcom('Phone_Label', Form[0].component);
                        self.Form.Phone_Placeholder = self.pluckcom('Phone_Placeholder', Form[0].component);
                        self.Form.Subject_Label = self.pluckcom('Subject_Label', Form[0].component);
                        self.Form.Subject_Placeholder = self.pluckcom('Subject_Placeholder', Form[0].component);
                        self.Form.Message_Label = self.pluckcom('Message_Label', Form[0].component);
                        self.Form.Message_Placeholder = self.pluckcom('Message_Placeholder', Form[0].component);
                        self.Form.Button_label = self.pluckcom('Button_label', Form[0].component);
                        self.Form.Contact_Us_Title = self.pluckcom('Contact_Us_Title', Form[0].component);

                        var Location = self.pluck('Location', self.content.area_List);
                        self.Location.Map_Link = self.pluckcom('Map_Link', Location[0].component);
                        
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });

                axios.get(banner, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var Banner = self.pluck('Contact_Us_Banner', self.content.area_List);
                    if (Banner.length > 0) {
                        self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
                        self.Banner.Title = self.pluckcom('Title', Banner[0].component);
                        self.Banner.Breadcrumb_1 = self.pluckcom('Breadcrumb_1', Banner[0].component);
                        self.Banner.Breadcrumb_2 = self.pluckcom('Breadcrumb_2', Banner[0].component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                });

            });
        },
        sendcontactus: async function () {
            if (!this.cntusername) {
                alertify.alert('Alert', 'Name required.').set('closable', false);

                return false;
            }
            if (!this.cntemail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.cntcontact) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            if (!this.cntsuject) {
                alertify.alert('Alert', 'Subject required.').set('closable', false);
                return false;
            }

            if (!this.cntmessage) {
                alertify.alert('Alert', 'Message required.').set('closable', false);
                return false;
            } else {
                var frommail=JSON.parse(localStorage.User).loginNode.email;
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl +JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.cntusername,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Contact Us",
                    keyword1: this.cntusername,
                    keyword2: this.cntemail,
                    number1: this.cntcontact,
                    keyword3: this.cntsuject,
                    text1: this.cntmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, custmail);
                    this.cntemail = '';
                    this.cntusername = '';
                    this.cntcontact = '';
                    this.cntmessage = '';
                    this.cntsuject = '';
                    alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
                } catch (e) {

                }



            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        }
    },
    mounted: function () {
        this.getPagecontent();
        sessionStorage.active_e=3;
    },
});