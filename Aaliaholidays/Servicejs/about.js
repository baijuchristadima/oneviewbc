var aboutus = new Vue({
    el: '#aboutuspage',
    name: 'aboutuspage',
    data: {
        // content: null,
        getdata: false,
        aboutUsContent: {
            About_Us_Title: '',
            Subtitle: '',
            Description_Image: '',
            About_Us_Content:''
        },
        Mission:{},
        Banner:{Banner_Image:''},
        Choose:{},
        Location:{}
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/About us/About us/About us.ftl';
                var contact = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact Us Page/Contact Us Page/Contact Us Page.ftl';
                var banner = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Banners/Banners/Banners.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var Mission = self.pluck('Mission_and_Vision', self.content.area_List);
                    if (Mission.length > 0) {
                        self.Mission.Our_Mission_Title = self.pluckcom('Our_Mission_Title', Mission[0].component);
                        self.Mission.Mission_Content = self.pluckcom('Mission_Content', Mission[0].component);
                        self.Mission.Our_Vision_Title = self.pluckcom('Our_Vision_Title', Mission[0].component);
                        self.Mission.Vision_Content = self.pluckcom('Vision_Content', Mission[0].component);
                    }
                    var AboutcontentDetail = self.pluck('content', self.content.area_List);
                    if (AboutcontentDetail.length > 0) {
                        self.aboutUsContent.About_Us_Title = self.pluckcom('About_Us_Title', AboutcontentDetail[0].component);
                        self.aboutUsContent.Subtitle = self.pluckcom('Subtitle', AboutcontentDetail[0].component);
                        self.aboutUsContent.Description_Image = self.pluckcom('Description_Image', AboutcontentDetail[0].component);
                        self.aboutUsContent.About_Us_Content = self.pluckcom('About_Us_Content', AboutcontentDetail[0].component);
                    }


                    var Choose = self.pluck('Why_Choose_Area', self.content.area_List);
                    self.Choose.Competitive_Pricing_Title = self.pluckcom('Competitive_Pricing_Title', Choose[0].component);
                    self.Choose.Competitive_Pricing_Content = self.pluckcom('Competitive_Pricing_Content', Choose[0].component);
                    self.Choose.Award_Winning_Service_Title = self.pluckcom('Award_Winning_Service_Title', Choose[0].component);
                    self.Choose.Award_Winning_Service_Content = self.pluckcom('Award_Winning_Service_Content', Choose[0].component);
                    self.Choose.Worldwide_Coverage_Title = self.pluckcom('Worldwide_Coverage_Title', Choose[0].component);
                    self.Choose.Worldwide_Coverage_Content = self.pluckcom('Worldwide_Coverage_Content', Choose[0].component);

                }).catch(function (error) {
                    console.log('Error');
                });
                axios.get(banner, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var Banner = self.pluck('About_Us_Banner', self.content.area_List);
                    if (Banner.length > 0) {
                        self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
                        self.Banner.Title = self.pluckcom('Title', Banner[0].component);
                        self.Banner.Breadcrumb_1 = self.pluckcom('Breadcrumb_1', Banner[0].component);
                        self.Banner.Breadcrumb_2 = self.pluckcom('Breadcrumb_2', Banner[0].component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                });
                axios.get(contact, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language':langauage }
                }).then(function (response) {
                    self.content = response.data;
                        var Location = self.pluck('Location', self.content.area_List);
                        self.Location.Map_Link = self.pluckcom('Map_Link', Location[0].component);                   
                }).catch(function (error) {
                    console.log('Error');
                });


            });

        },
    },
    mounted: function () {
        this.getPagecontent();
        sessionStorage.active_e=1;
    },
})