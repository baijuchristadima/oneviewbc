const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var terms = new Vue({
    i18n,
    el: '#privacy',
    name: 'privacy',
    data() {
        return {
            Banner: {
                Banner_Image: ''
            },
            pagecontents: {
                Privacy: ''
            }
        }

    },
    mounted: function () {
        this.getpagecontent();
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getpagecontent: function () {
            if (localStorage.getItem("AgencyCode") === null) {
                localStorage.AgencyCode = JSON.parse(localStorage.User).loginNode.code;

            }
            var Agencycode = localStorage.AgencyCode;
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var self = this;

            // banner and labels
            var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Privacy and Policy/Privacy and Policy/Privacy and Policy.ftl';
            var banner = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Banners/Banners/Banners.ftl';
            var contact = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact Us Page/Contact Us Page/Contact Us Page.ftl';

            axios.get(banner, {
                headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                }
            }).then(function (response) {
                self.content = response.data;
                var Banner = self.pluck('Privacy_and_Policy', self.content.area_List);
                if (Banner.length > 0) {
                    self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
                    self.Banner.Title = self.pluckcom('Title', Banner[0].component);
                    self.Banner.Breadcrumb_1 = self.pluckcom('Breadcrumb_1', Banner[0].component);
                    self.Banner.Breadcrumb_2 = self.pluckcom('Breadcrumb_2', Banner[0].component);
                }
            }).catch(function (error) {
                console.log('Error');
            });
            axios.get(pageurl, {
                headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                }
            }).then(function (response) {
                self.content = response.data;
                var pagecontents = self.pluck('Privacy_and_Policy', self.content.area_List);
                if (pagecontents.length > 0) {
                    self.pagecontents.Privacy = self.pluckcom('Privacy_and_Policy_List', pagecontents[0].component);
                }
            }).catch(function (error) {
                console.log('Error');
            });
        },
    }
});