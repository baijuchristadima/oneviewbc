const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var terms = new Vue({
    i18n,
    el: '#terms',
    name: 'terms',
    data() {
        return {
            Banner: {
                Banner_Image: ''
            },
            pagecontents: {
                Terms_of_Use: ''
            }
        }

    },
    mounted: function () {
        this.getpagecontent();
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getpagecontent: function () {
            if (localStorage.getItem("AgencyCode") === null) {
                localStorage.AgencyCode = JSON.parse(localStorage.User).loginNode.code;

            }
            var Agencycode = localStorage.AgencyCode;
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var self = this;

            // banner and labels
            var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Terms of Use/Terms of Use/Terms of Use.ftl';
            var banner = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Banners/Banners/Banners.ftl';

            axios.get(banner, {
                headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                }
            }).then(function (response) {
                self.content = response.data;
                var Banner = self.pluck('Terms_of_Use', self.content.area_List);
                if (Banner.length > 0) {
                    self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
                    self.Banner.Title = self.pluckcom('Title', Banner[0].component);
                    self.Banner.Breadcrumb_1 = self.pluckcom('Breadcrumb_1', Banner[0].component);
                    self.Banner.Breadcrumb_2 = self.pluckcom('Breadcrumb_2', Banner[0].component);
                }
            }).catch(function (error) {
                console.log('Error');
            });
            axios.get(pageurl, {
                headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                }
            }).then(function (response) {
                self.content = response.data;
                var pagecontents = self.pluck('Terms_of_Use', self.content.area_List);
                if (pagecontents.length > 0) {
                    self.pagecontents.Terms_of_Use = self.pluckcom('Terms_of_Use_List', pagecontents[0].component);
                }
            }).catch(function (error) {
                console.log('Error');
                self.content = [];
            });
        },
    },
});