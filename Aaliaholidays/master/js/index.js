const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
 Vue.component('vue-multiselect', window.VueMultiselect.default);

var flightserchfromComponent = Vue.component('flightserch', {
    data() {
        return {
            access_token: '',
            KeywordSearch: '',
            resultItemsarr: [],
            autoCompleteProgress: false,
            highlightIndex: 0
        }
    },
    props: {
        itemText: String,
        itemId: String,
        placeHolderText: String,
        returnValue: Boolean,
        id: { type: String, default: '', required: false },
        leg: Number,
        //KeywordSearch:String
    },

    template: `<div class="autocomplete">
        <input type="text" :placeholder="placeHolderText" :id="id" :data-aircode="KeywordSearch" autocomplete="off" 
        v-model="KeywordSearch" class="form-control" 
        :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !autoCompleteProgress  }" 
        @input="onSelectedAutoCompleteEvent(KeywordSearch)"
            @keydown.down="down"
            @keydown.up="up"           
            @keydown.esc="autoCompleteProgress=false"
            @keydown.enter="onSelected(resultItemsarr[highlightIndex])"
            @keydown.tab="tabclick(resultItemsarr[highlightIndex])"/>
        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>        
        <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length>0">
            <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item)">
                {{ item.label }}
            </li>
        </ul>
    </div>`,


    methods: {
        up: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex < this.resultItemsarr.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
            if (this.$refs.options[this.highlightIndex]) {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            }

        },
        onSelectedAutoCompleteEvent: _.debounce(async function (keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
                this.autoCompleteProgress = true;
                self.resultItemsarr = await getFlightResultUsingElasticSearch(keywordEntered);


            } else {
                this.autoCompleteProgress = false;
                this.resultItemsarr = [];

            }
        }, 100),
        onSelected: function (item) {
            this.KeywordSearch = item.label;
            this.resultItemsarr = [];
            var targetWhenClicked = $(event.target).parent().parent().find("input").attr("id");
            if (maininstance.triptype != 'M') {
                if (event.target.id == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
                    $('#Cityto1').focus();
                } else if (event.target.id == "Cityto1" || targetWhenClicked == "Cityto1") {
                    $('#deptDate01').focus();
                }
            } else {
                var eventTarget = event.target.id;
                $(document).ready(function () {
                    if (eventTarget == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
                        $('#Cityto1').focus();
                    } else if (eventTarget == "Cityto1" || targetWhenClicked == "Cityto1") {
                        $('#deptDate01').focus();
                    } else if (eventTarget == "DeparturefromLeg1" || targetWhenClicked == "DeparturefromLeg1") {
                        $('#ArrivalfromLeg1').focus();
                    } else if (eventTarget == "ArrivalfromLeg1" || targetWhenClicked == "ArrivalfromLeg1") {
                        $('#txtLeg1Date').focus();
                    } else if (eventTarget == "DeparturefromLeg2" || targetWhenClicked == "DeparturefromLeg2") {
                        $('#ArrivalfromLeg2').focus();
                    } else if (eventTarget == "ArrivalfromLeg2" || targetWhenClicked == "ArrivalfromLeg2") {
                        $('#txtLeg2Date').focus();
                    } else if (eventTarget == "DeparturefromLeg3" || targetWhenClicked == "DeparturefromLeg3") {
                        $('#ArrivalfromLeg3').focus();
                    } else if (eventTarget == "ArrivalfromLeg3" || targetWhenClicked == "ArrivalfromLeg3") {
                        $('#txtLeg3Date').focus();
                    } else if (eventTarget == "DeparturefromLeg4" || targetWhenClicked == "DeparturefromLeg4") {
                        $('#ArrivalfromLeg4').focus();
                    } else if (eventTarget == "ArrivalfromLeg4" || targetWhenClicked == "ArrivalfromLeg4") {
                        $('#txtLeg4Date').focus();
                    } else if (eventTarget == "DeparturefromLeg5" || targetWhenClicked == "DeparturefromLeg5") {
                        $('#ArrivalfromLeg5').focus();
                    } else if (eventTarget == "ArrivalfromLeg5" || targetWhenClicked == "ArrivalfromLeg5") {
                        $('#txtLeg5Date').focus();
                    } else if (eventTarget == "DeparturefromLeg6" || targetWhenClicked == "DeparturefromLeg6") {
                        $('#ArrivalfromLeg6').focus();
                    } else if (eventTarget == "ArrivalfromLeg6" || targetWhenClicked == "ArrivalfromLeg6") {
                        $('#txtLeg6Date').focus();
                    }
                });

            }

            this.$emit('air-search-completed', item.code, item.label, this.leg);


        },
        tabclick: function (item) {
            if (!item) {

            }
            else {
                this.onSelected(item);
            }
        }

    },
    watch: {
        returnValue: function () {
            this.KeywordSearch = this.itemText;
        }
    }

});

var maininstance = new Vue({
    i18n,
    el: '#indexmain',
    name: 'indexmain',
    // components: { 
    //     VueMultiselect: window.VueMultiselect 
    //   },
    data: {
        content: {
            area_List: []
        },
        getdata: true,
        toppackages: null,
        holidayaPackageList: [],
        packagetitle: 'Holiday Packages',
        days: 'Days',
        nights: 'Nights',
        getpackage: false,
        triptype: "R",
        banner: {
            image: '',
            caption: []
        },
        getaboutus: false,
        aboutUs: {
            title: '',
            image: '',
            button: '',
            description: ''
        },
        gethomeoffer: false,
        offersection: {
            title: '',
            Subtitle: '',
            description: '',
            link: '',
            button: ''
        },
        placeholderfrom: "From",
        placeholderTo: "To",
        placeholderDepartureDate: "Departure Date",
        placeholderReturnDate: "Return Date",
        Flight_form_caption: "",
        Oneway: "",
        Round_trip: "",
        Multicity: "",
        Advance_search_label: "",
        preferred_airline_placeholder: "",
        Direct_flight_option_label: "",
        Flight_tab_name: "",
        Hotel_tab_name: "",
        Package_tab_name: "",
        CityFrom: '',
        CityTo: '',
        validationMessage: '',
        adtcount: 1,
        chdcount: 0,
        infcount: 0,
        preferAirline: '',
        returnValue: true,
        legcount: 2,
        legs: [],
        cityList: [],
        adt: "adult",
        adults: [

            { 'value': 1, 'text': '1 Adult' },
            { 'value': 2, 'text': '2 Adults' },
            { 'value': 3, 'text': '3 Adults' },
            { 'value': 4, 'text': '4 Adults' },
            { 'value': 5, 'text': '5 Adults' },
            { 'value': 6, 'text': '6 Adults' },
            { 'value': 7, 'text': '7 Adults' },
            { 'value': 8, 'text': '8 Adults' },
            { 'value': 9, 'text': '9 Adults' }
        ],
        children: [
            { 'value': 0, 'text': 'Children' },
            { 'value': 1, 'text': '1 Child' },
            { 'value': 2, 'text': '2 Children' },
            { 'value': 3, 'text': '3 Children' },
            { 'value': 4, 'text': '4 Children' },
            { 'value': 5, 'text': '5 Children' },
            { 'value': 6, 'text': '6 Children' },
            { 'value': 7, 'text': '7 Children' },
            { 'value': 8, 'text': '8 Children' }

        ],
        infants: [
            { 'value': 0, 'text': 'Infants' },
            { 'value': 1, 'text': '1 Infant' }

        ],
        cabinclass: [{ 'value': 'Y', 'text': 'Economy' },
        { 'value': 'C', 'text': 'Business' },
        { 'value': 'F', 'text': 'First' }
        ],
        selected_cabin: 'Y',
        selected_adult: 1,
        selected_child: 0,
        selected_infant: 0,
        totalAllowdPax: 9,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        arabic_dropdown: '',
        childLabel: "",
        childrenLabel: "",
        adultLabel: "",
        adultsLabel: "",
        ageLabel: "",
        agesLabel: "",
        infantLabel: "",
        searchBtnLabel: "",
        addUptoLabel: "",
        tripsLabel: "",
        tripLabel: "",
        Totaltravaller: '1 Travellers, Economy',
        travellerdisply: false,
        child: 0,
        flightSearchCityName: { cityFrom1: '', cityTo1: '', cityFrom2: '', cityTo2: '', cityFrom3: '', cityTo3: '', cityFrom4: '', cityTo4: '', cityFrom5: '', cityTo5: '', cityFrom6: '', cityTo6: '' },
        advncedsearch: false,
        isLoading: false,
        direct_flight: false,
        airlineList: AirlinesDatas,
        Airlineresults: [],
        selectedAirline: [],
        adultrange: '',
        childrange: '',
        infantrange: '',
        donelabel: '',
        classlabel: '',
        bnrcaption: false,
        hotelInit: Math.random(), //hotel init
        FormSearch:{},
        Homebanner:{Title_1:''},
        aboutUsContent:{About_Us_Content:''},
        Services:null,
        Choose:{},
        Location:{}
    },
    methods: {
        Departurefrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (AirportCode == this.CityTo) {
                    this.returnValue = false;
                    alertify.alert('Alert', 'Departure and arrival airports should not be same !');

                    this.CityFrom = '';

                } else {
                    this.returnValue = true;
                    this.CityFrom = AirportCode;
                }
            } else {
                var from = 'cityFrom' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].to) {
                        this.returnValue = false;
                        this.flightSearchCityName[from] = "";
                        alertify.alert('Alert', 'Departure and arrival airports should not be same !');
                    } else {
                        this.cityList[index].from = AirportCode
                        this.flightSearchCityName[from] = AirportName;
                        this.returnValue = true;
                    }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: AirportCode,
                        to: ''

                    });
                    this.flightSearchCityName[from] = AirportName;
                    this.returnValue = true;
                }
            }


        },
        Arrivalfrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (this.CityFrom == AirportCode) {
                    this.returnValue = false;
                    alertify.alert('Alert', 'Departure and arrival airports should not be same !');
                    this.validationMessage = "";
                    this.CityTo = '';

                } else {
                    this.returnValue = true;
                    this.CityTo = AirportCode;
                }
            } else {
                var to = 'cityTo' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].from) {
                        this.returnValue = false;
                        this.flightSearchCityName[to] = "";
                        alertify.alert('Alert', 'Departure and arrival airports should not be same !');

                    } else {
                        this.cityList[index].to = AirportCode;
                        this.flightSearchCityName[to] = AirportName;
                        this.returnValue = true;
                    }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: '',
                        to: AirportCode

                    });
                    this.flightSearchCityName[to] = AirportName;
                    this.returnValue = true;
                }
            }
        },
        SearchFlight: function () {

            var Departuredate = $('#deptDate01').val() == "" ? "" : $('#deptDate01').datepicker('getDate');
            if (!this.CityFrom) {
                alertify.alert('Alert', 'Please fill origin !');

                return false;
            }
            if (!this.CityTo) {
                alertify.alert('Alert', 'Please fill destination ! ');
                return false;
            }
            if (!Departuredate) {
                alertify.alert('Alert', 'Please choose departure date !').set('closable', false);
                return false;
            }
            var sec1TravelDate = moment(Departuredate).format('DD|MM|YYYY');

            var sectors = this.CityFrom + '-' + this.CityTo + '-' + sec1TravelDate;
            if (this.triptype == 'R') {
                var ArrivalDate = $('#retDate').val() == "" ? "" : $('#retDate').datepicker('getDate');
                if (!isNullorUndefined(ArrivalDate)) {
                    ArrivalDate = moment(ArrivalDate).format('DD|MM|YYYY');
                    if (!ArrivalDate) {
                        alertify.alert('Alert', 'Please choose return  date !').set('closable', false);

                        return false;
                    } else {
                        sectors += '/' + this.CityTo + '-' + this.CityFrom + '-' + ArrivalDate;
                    }
                } else {
                    alertify.alert('Alert', 'Please choose return date !').set('closable', false);

                    return false;
                }
            }
            var directFlight = this.direct_flight ? 'DF' : 'AF';

            var adult = this.selected_adult;
            var child = this.selected_child;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            getSuppliers([this.CityFrom + '|' + this.CityTo],
            function(supp) {
                var searchUrl = '/Flights/flight-listing.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                // searchUrl = searchUrl.toLocaleLowerCase();
                window.location.href = searchUrl;
            })
        },
        AddNewLeg() {
            if (this.legcount <= 5) {
                ++this.legcount;
                var legno = 1;
                if (this.cityList.length > 0) {
                    legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                    legno = legno + 1;
                }

                this.cityList.push({
                    id: legno,
                    from: '',
                    to: ''
                });

            }
            console.log(this.cityList);
        },
        DeleteLeg(leg) {
            var legs = this.legcount;
            if (legs > 1) {
                --this.legcount;
                var legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    this.cityList.splice(index, 1);

                }


            }
        },
        setCalender() {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = 1;//generalInformation.systemSettings.calendarDisplay;
            $("#deptDate01").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#retDate").datepicker("option", "minDate", selectedDate);

                }
            });

            $("#retDate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) { }
            });
            $("#txtLeg1Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);

                }
            });
            $("#txtLeg2Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg1Date").val();
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg3Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg2Date").val();
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg4Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg3Date").val();
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg5Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg4Date").val();
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg6Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg5Date").val();
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) { }
            });


        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },

        pageContent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var contact = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact Us Page/Contact Us Page/Contact Us Page.ftl';
                var service = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Services/Services/Services.ftl';
                var about = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/About us/About us/About us.ftl';
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Home/Home/Home.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    if (response.data.area_List.length) {
                        var FormSearch = self.pluck('Form_Search', self.content.area_List);
                        self.FormSearch.flightTab = self.pluckcom('Flight_tab_Name', FormSearch[0].component);
                        self.FormSearch.hotelTab = self.pluckcom('Hotel_tab_Name', FormSearch[0].component);
                        self.FormSearch.carTab = self.pluckcom('Car_tab_Name', FormSearch[0].component);
                        self.FormSearch.insuranceTab = self.pluckcom('Insurance_tab_Name', FormSearch[0].component);
                        self.FormSearch.oneWay = self.pluckcom('Oneway', FormSearch[0].component);
                        self.FormSearch.roundTrip = self.pluckcom('Round_Trip', FormSearch[0].component);
                        self.FormSearch.multiCity = self.pluckcom('Multi_City', FormSearch[0].component);
                        self.FormSearch.buttonLabel = self.pluckcom('Search_button_label', FormSearch[0].component);
                        self.FormSearch.flightDeparturePlaceholder = self.pluckcom('Flight_departure_placeholder', FormSearch[0].component);
                        self.FormSearch.flightArrivalPlaceholder = self.pluckcom('Flight_arrival_placeholder', FormSearch[0].component);
                        self.FormSearch.flightDatePlaceholderD = self.pluckcom('Flight_departure_date_placeholder', FormSearch[0].component);
                        self.FormSearch.flightDatePlaceholderR = self.pluckcom('Flight_arrival_date_placeholder', FormSearch[0].component);
                        self.FormSearch.economy = self.pluckcom('Economy_class', FormSearch[0].component);
                        self.FormSearch.business = self.pluckcom('Business_class', FormSearch[0].component);
                        self.FormSearch.first = self.pluckcom('First_class', FormSearch[0].component);
                        self.FormSearch.adults = self.pluckcom('adults_label', FormSearch[0].component);
                        self.FormSearch.children = self.pluckcom('children_label', FormSearch[0].component);
                        self.FormSearch.infants = self.pluckcom('Infants_label', FormSearch[0].component);
                        self.FormSearch.hotelCityPlaceholder = self.pluckcom('Hotel_city_placeholder', FormSearch[0].component);
                        self.FormSearch.hotelDatePlaceA = self.pluckcom('Hotel_arrival_date_placeholder', FormSearch[0].component);
                        self.FormSearch.hotelDatePlaceD = self.pluckcom('Hotel_departure_date_placeholder', FormSearch[0].component);
                        self.FormSearch.occupancy = self.pluckcom('Hotel_Occupancy_placeholder', FormSearch[0].component);
                        self.FormSearch.roomLabelSi = self.pluckcom('Room_single_label', FormSearch[0].component);
                        self.FormSearch.roomLabelDo = self.pluckcom('Room_double_label', FormSearch[0].component);
                        self.FormSearch.carStartingLocation = self.pluckcom('Car_Starting_location__placeholder', FormSearch[0].component);
                        self.FormSearch.carDropoffLocation = self.pluckcom('Car_drop_off_location__placeholder', FormSearch[0].component);
                        self.FormSearch.carPickupDateP = self.pluckcom('Car_pickup_date_placeholder', FormSearch[0].component);
                        self.FormSearch.carDropDateP = self.pluckcom('Car_drop_off_date_placeholder', FormSearch[0].component);
                        self.FormSearch.carPickupTimeP = self.pluckcom('Car_pickup_time_placeholder', FormSearch[0].component);
                        self.FormSearch.carPickupTimeL = self.pluckcom('Pickup_Time_label', FormSearch[0].component);
                        self.FormSearch.carDropTimeL = self.pluckcom('Drop_off_Time_label', FormSearch[0].component);

                        //self.childLabel = self.pluckcom('Adult_label', searchForm[0].component);
                        self.childLabel = self.pluckcom('child_label', FormSearch[0].component);
                        self.infantLabel = self.pluckcom('Infant_label', FormSearch[0].component);
                        self.childrenLabel = self.pluckcom('children_label', FormSearch[0].component);

                        var Homebanner = self.pluck('Banner', self.content.area_List);
                        self.Homebanner.Banner_Image = self.pluckcom('Banner_Image', Homebanner[0].component);
                        self.Homebanner.Title_1 = self.pluckcom('Title_1', Homebanner[0].component);
                        self.Homebanner.Title_2 = self.pluckcom('Title_2', Homebanner[0].component);


                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });

                axios.get(about, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var AboutcontentDetail = self.pluck('content', self.content.area_List);
                    if (AboutcontentDetail.length > 0) {
                        self.aboutUsContent.About_Us_Title = self.pluckcom('About_Us_Title', AboutcontentDetail[0].component);
                        self.aboutUsContent.Subtitle = self.pluckcom('Subtitle', AboutcontentDetail[0].component);
                        self.aboutUsContent.Description_Image = self.pluckcom('Description_Image', AboutcontentDetail[0].component);
                        self.aboutUsContent.About_Us_Content = self.pluckcom('About_Us_Content', AboutcontentDetail[0].component);
                    }
                    var Choose = self.pluck('Why_Choose_Area', self.content.area_List);
                    self.Choose.Competitive_Pricing_Title = self.pluckcom('Competitive_Pricing_Title', Choose[0].component);
                    self.Choose.Competitive_Pricing_Content = self.pluckcom('Competitive_Pricing_Content', Choose[0].component);
                    self.Choose.Award_Winning_Service_Title = self.pluckcom('Award_Winning_Service_Title', Choose[0].component);
                    self.Choose.Award_Winning_Service_Content = self.pluckcom('Award_Winning_Service_Content', Choose[0].component);
                    self.Choose.Worldwide_Coverage_Title = self.pluckcom('Worldwide_Coverage_Title', Choose[0].component);
                    self.Choose.Worldwide_Coverage_Content = self.pluckcom('Worldwide_Coverage_Content', Choose[0].component);

                }).catch(function (error) {
                    console.log('Error');
                });
                axios.get(service, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(async function (response) {
                    self.content = response.data;
                    var service = self.pluck('services_area', self.content.area_List);
                    if (service.length > 0) {
                        self.Services = self.pluckcom('Service_List', service[0].component);

                    }
                }).catch(function (error) {
                    console.log('Error');
                });
                axios.get(contact, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language':langauage }
                }).then(function (response) {
                    self.content = response.data;
                        var Location = self.pluck('Location', self.content.area_List);
                        self.Location.Map_Link = self.pluckcom('Map_Link', Location[0].component);                   
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });
        },
        onAdultChange: function () {
            alert(this.selected);
            //this.disp = this.selected;
        },
        onCheck: function (e) {
            var currid = $(e.target).attr('id');
           
            console.log(currid);
            var selectedDate = $("#deptDate01").val();
            $("#retDate").datepicker("option", "minDate", selectedDate);
            $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/easy2go/packageview.html?page=" + url + "&from=pkg";
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;
        },
        swapLocations: function (id) {
            if ((this.CityFrom) && (this.CityTo)) {
                var from = this.CityFrom;
                var to = this.CityTo;
                this.CityFrom = to;
                this.CityTo = from;
                swpaloc(id)
            }
        },
        showhidetraveller: function () {
            this.travellerdisply == true ? this.travellerdisply = false : this.travellerdisply = true
        },
        setCHDINFTravellers: function (event) {

            this.children = [];
            this.infants = [];
            this.child = this.totalAllowdPax - this.selected_adult;
            var chdtext = "Child";
            for (var chd = 0; chd <= this.child; chd++) {
                chdtext = chd == 0 ? chdtext = "0 " + this.childLabel : chd == 1 ? chdtext = chd + " " + this.childLabel : chdtext = chd + " " + this.childrenLabel;
                this.children.push({
                    'value': chd,
                    'text': chdtext
                })
            }
            if (this.selected_child > 0 && this.selected_child <= this.child) {
                this.selected_child = this.selected_child;
            } else {
                this.selected_child = 0;
            }
            infant = parseInt(this.selected_adult);
            if (infant + parseInt(this.selected_adult) + parseInt(this.selected_child) > 9) {
                infant = parseInt(this.totalAllowdPax) - (parseInt(this.selected_adult) + parseInt(this.selected_child));
            }
            infant = ((parseInt(infant) < 0) ? 0 : infant);
            if (infant == 0) this.selected_infant = 0;
            var inftext = "";
            for (var inf = 0; inf <= infant; inf++) {
                inftext = inf == 0 ? inftext = "0 " + this.infantLabel : inf == 1 ? inftext = " 1 " + this.infantLabel : inftext = inf + " " + this.infantLabel;
                this.infants.push({
                    'value': inf,
                    'text': inftext
                })
            }
            if (this.selected_infant > 0) {
                this.selected_infant = (this.selected_infant <= infant) ? this.selected_infant : 0;
            }
            totalpax = parseInt(this.selected_adult) + parseInt(this.selected_child) + parseInt(this.selected_infant);
            this.Totaltravaller = totalpax + " Travellers, " + getCabinName(this.selected_cabin.toUpperCase());

        },
        SetInfantTravellers: function () {
            this.infants = [];
            remiaingpax = parseInt(this.selected_adult);
            if (remiaingpax + parseInt(this.selected_adult) + parseInt(this.selected_child) > 9) {
                remiaingpax = parseInt(this.totalAllowdPax) - (parseInt(this.selected_adult) + parseInt(this.selected_child));
            }
            remiaingpax = ((parseInt(remiaingpax) < 0) ? 0 : remiaingpax);
            for (var inf = 0; inf <= remiaingpax; inf++) {
                inftext = inf == 0 ? inftext = "0 " + this.infantLabel : inf == 1 ? inftext = " 1 " + this.infantLabel : inftext = inf + " " + this.infantLabel;
                this.infants.push({
                    'value': inf,
                    'text': inftext
                })
            }
            if (this.selected_infant > 0) {
                this.selected_infant = (this.selected_infant <= remiaingpax) ? this.selected_infant : 0;
            }
            totalpax = parseInt(this.selected_adult) + parseInt(this.selected_child) + parseInt(this.selected_infant);
            this.Totaltravaller = totalpax + " Travellers, " + getCabinName(this.selected_cabin.toUpperCase());

        },
        setTravelInfo: function () {
            totalpax = parseInt(this.selected_adult) + parseInt(this.selected_child) + parseInt(this.selected_infant);
            this.Totaltravaller = totalpax + " Travellers, " + getCabinName(this.selected_cabin.toUpperCase());

        },
        getCabinName: function (cabin) {
            getCabinName(cabin)
        },
        MultiSearchFlight: function () {
            var sectors = '';
            var legDetails = [];

            for (var legValue = 1; legValue <= this.legcount; legValue++) {
                var temDeparturedate = $('#txtLeg' + (legValue) + 'Date').val() == "" ? "" : $('#txtLeg' + (legValue) + 'Date').datepicker('getDate');
                if (temDeparturedate != "" && this.cityList.length != 0 && this.cityList[legValue - 1].from != "" && this.cityList[legValue - 1].to != "") {
                    var departureFrom = this.cityList[legValue - 1].from;
                    var arrivalTo = this.cityList[legValue - 1].to;
                    var travelDate = moment(temDeparturedate).format('DD|MM|YYYY');
                    sectors += '/' + departureFrom + '-' + arrivalTo + '-' + travelDate;
                    legDetails.push(departureFrom + '|' + arrivalTo)
                } else {
                    alertify.alert('Alert', 'Please fill the Trip ' + (legValue) + '   fields !').set('closable', false);


                    return false;
                }
            }
            var directFlight = this.direct_flight ? 'DF' : 'AF';

            var adult = this.selected_adult;
            var child = this.selected_child;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            getSuppliers(legDetails,
            function(supp) {
                var searchUrl = '/Flights/flight-listing.html?flight=' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                // searchUrl = searchUrl.toLocaleLowerCase();
                window.location.href = searchUrl;
            })

        },
        limitText(count) {
            return `and ${count} other Airlines`
        },
        asyncFind(query) {
            if (query.length > 2) {
                this.isLoading = true;

                var newData = [];
                this.airlineList.filter(function (el) {
                    if (el.A.toLowerCase().indexOf(query.toLowerCase()) >= 0) {
                        newData.push({
                            "code": el.C,
                            "label": el.A
                        });
                    }
                });
                if (newData.length > 0) {
                    this.Airlineresults = newData;
                    this.isLoading = false;
                }
                else {
                    this.Airlineresults = [];
                    this.isLoading = false;
                }
            }


        },
        clearAll() {
            this.selectedAirline = []
        }

    },
    mounted: function () {
        this.setCalender();
        this.pageContent();
        sessionStorage.active_e=0;
    },
    updated: function () {
        this.setCalender();
    },
    watch: {
        selectedAirline: function () {
            if (this.selectedAirline.length > 4) {
                alert("exceed");
                return false;
            }
            else {
                var airlines = [];
                if (this.selectedAirline.length > 0) {
                    this.selectedAirline.forEach(function (airline) {
                        airlines.push(airline.code);
                    });
                }

                return this.preferAirline = airlines.length > 0 ? airlines.join('|') : '';
            }
        }

    }
});




function owlcarosl() {


    $(".pageload").hide();
    $("body").css("overflow-y", "auto");
    // $('.carousel-inner > div').first().addClass('active');
    $("#owl-demo-3").owlCarousel({
        rtl: true,
        autoplay: true,
        autoPlay: 8000,
        autoplayHoverPause: true,
        stopOnHover: false,
        items: 4,
        margin: 10,
        lazyLoad: true,
        navigation: true,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [600, 1]
    });

    $("#owl-demo-3 .owl-prev").html('<i class="fa  fa-angle-left"></i>');
    $("#owl-demo-3 .owl-next").html('<i class="fa  fa-angle-right"></i>');
}


function isNullorUndefined(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) { status = true; }
    return status;
}

function onPaxChange(e) {
    var currentadult = parseInt($('#adultdrp option:selected').val());
    var currentchild = parseInt($('#childdrp option:selected').val());
    var currentinfant = parseInt($('#infantdrp option:selected').val());
    var currenttotal = parseInt(currentadult) + parseInt(currentchild);
    var child = 9 - currentadult;
    maininstance.children = [
        { 'value': 0, 'text': 'Children (2-11 yrs)' }
    ];
    for (i = 1; i <= child; i++) {
        maininstance.children.push({ 'value': i, 'text': i + ' Child' + (i > 1 ? 'ren' : '') });
    }
    if (currentchild > child) {
        currentchild = child;
    }
    currentchild = (currentchild < 0) ? 0 : currentchild;
    maininstance.selected_child = currentchild;
    var infant = (((currenttotal > 2 && currenttotal < 7) && (currentadult > 3)) ? 3 : ((currenttotal > 6) ? (9 - currenttotal) : currentadult));
    maininstance.infants = [
        { 'value': 0, 'text': 'Infants (0-1 yr)' }
    ];
    for (i = 1; i <= infant; i++) {
        maininstance.infants.push({ 'value': i, 'text': i + ' Infant' + (i > 1 ? 's' : '') });
    }
    if (currentinfant > infant) {
        currentinfant = infant;
    }
    currentinfant = (currentinfant < 0) ? 0 : currentinfant;
    maininstance.selected_infant = currentinfant;
}



function swpaloc(id) {
    var from = $("#Cityfrom" + id).val();
    var to = $("#Cityto" + id).val();
    $("#Cityfrom" + id).val(to);
    $("#Cityto" + id).val(from);
}

function getCabinName(cabinCode) {
    var cabinClass = 'Economy';
    if (cabinCode == "F") {
        cabinClass = "First Class";
    } else if (cabinCode == "C") {
        cabinClass = "Business";
    } else {
        try { cabinClass = getCabinClassObject(cabinCode).BasicClass; } catch (err) { }
    }
    return cabinClass;
}