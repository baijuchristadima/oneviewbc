Vue.component('loading', VueLoading)
var aboutPage = new Vue({
    el:'#faqid',
    name:'faqid',
    data: {
        banner:{},
        faqSection:{},
        isLoading: false,
        fullPage: true,
    },
    methods: {
       getData: function() {
            var self = this;
            self.isLoading = true;
            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var faqurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/07 Faq/07 Faq/07 Faq.ftl';
                axios.get(faqurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                           // var bannerDetail =self.getAllMapData(response.data.area_List[0].main.component);
                           // self.banner= bannerDetail;
                            //var aboutDetail =self.getAllMapData(response.data.area_List[0].About_Us_Section.component);
                           // self.aboutArea=aboutDetail;
                            var bannerDetail = pluck('Banner_Section', response.data.area_List);
                            self.banner = getAllMapData(bannerDetail[0].component);
                            var faqDetail = pluck('Faq_Section', response.data.area_List);
                            self.faqSection = getAllMapData(faqDetail[0].component);
                        
                        self.isLoading = false;
                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });

        }
    },
    mounted: function() {  
        this.getData();
    }
});

