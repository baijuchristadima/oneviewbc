Vue.component('loading', VueLoading)
var aboutPage = new Vue({
    el:'#blog',
    name:'blog',
    data: {
        banner:{},
        blogData:{},
        blogDetails:[],
        blogList:[],
        blogDate:[],
        currentPage: 1,
        currentPages: 1,
        pageLimit:6,
        fromPage: 1,
        totalpage: 1,
        totalBlogs: 0,
        fromPage: 1,
        totalpage: 1,
        constructedNumbers: [],
        paginationLimit:1,
        isLoading: false,
        fullPage: true,
    },
    methods: {
       getData: function() {
            var self = this;
            self.isLoading = true;
            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var faqurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Blog/Blog/Blog.ftl';
                axios.get(faqurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                           // var bannerDetail =self.getAllMapData(response.data.area_List[0].main.component);
                           // self.banner= bannerDetail;
                            //var aboutDetail =self.getAllMapData(response.data.area_List[0].About_Us_Section.component);
                           // self.aboutArea=aboutDetail;
                        var bannerDetail = pluck('Banner_Section', response.data.area_List);
                        self.banner = getAllMapData(bannerDetail[0].component);                     
                        self.isLoading = false;
                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });
        },
        getBlog:function(){
            var self = this;
            self.isLoading = true;
            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var faqurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Blog List/Blog List/Blog List.ftl';
                axios.get(faqurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    
                    if (response.data!=undefined) {
                           // var bannerDetail =self.getAllMapData(response.data.area_List[0].main.component);
                           // self.banner= bannerDetail;
                            //var aboutDetail =self.getAllMapData(response.data.area_List[0].About_Us_Section.component);
                           // self.aboutArea=aboutDetail;
                        // var bannerDetail = self.pluck('Banner_Section', response.data.area_List);
                        // self.banner = self.getAllMapData(bannerDetail[0].component);
                       
                        var urlData = window.location.href.split('?');
                       
                        if(urlData[1] !== undefined){
                          var keyData = urlData[1].split('=');
                          var typeData = keyData[0];
                          var Item = decodeURIComponent(keyData[1]);
                          
                          for (const iterator of response.data.Values) {
                            if(typeData == 'category' && iterator.Category_Name !== undefined && iterator.Category_Name !== null) {
                              if(Item.toUpperCase() == iterator.Category_Name.toUpperCase()) {
                                self.blogDetails.push(iterator);
                              }
                             } else if(typeData == 'tag' && iterator.Tag_List !== undefined && iterator.Tag_List !== null) {

                               for (const tagD of iterator.Tag_List) {
                                 if(Item.toUpperCase() == tagD.Name.toUpperCase()) {
                                  self.blogDetails.push(iterator);
                                 }
                               }
                            }
                          }
                        } else {
                          self.blogDetails=response.data.Values;
                        }
                        
                         
                        //  self.blogDetails.forEach(element => {
                        //    self.blogDate =element.Date.format('MMM DD, YYYY');
                        //  });
                         self.totalBlog = Number(self.blogDetails.length);
                         self.totalpage = Math.ceil(self.totalBlog/ self.pageLimit);
                        self.currentPage = 1;
                         self.fromPage = 1;
                      if (Number(self.totalBlog) < 12) {
                       // this.pageLimit=3;
                         }
      
              self.constructAllPagianationLink();
                        self.isLoading = false;
                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });
        },
        dateConvert: function (utc) {
            return (moment(utc).format("MMM DD,YYYY"));
        },
        constructAllPagianationLink: function () {
            let limit = this.paginationLimit;
            this.constructedNumbers = [];
            for (let i = Number(this.fromPage); i <= (Number(this.totalpage) + limit); i++) {
              if (i <= Number(this.totalpage)) {
                this.constructedNumbers.push(i);
              }
            }
            this.currentPage = this.constructedNumbers;
            this.setBlogItems();
          },
          prevNextEvent: function (type) {
            let limit = this.paginationLimit;
            if (type == 'Previous') {
              if (this.currentPages > this.totalpage) {
                this.currentPages = this.currentPages - 1;
              }
      
              // this.currentPage = event.target.innerText;
              // this.currentPage=parseInt(this.currentPage);
              this.fromPage = this.currentPages;
              if (Number(this.fromPage) != 1) {
                // this.fromPage = Number(this.fromPage) - limit;
                this.currentPages = Number(this.currentPages) - 1;
                this.setBlogItems();
      
                // this.constructAllPagianationLink();
      
              }
      
            } else if (type == 'Next') {
              if (this.currentPages == 'undefined' || this.currentPages == '') {
                // this.currentPages = sessionStorage.getItem("currentpage");
              }
              if (this.currentPages <= this.totalpage) {
                let limit = this.paginationLimit;
                this.fromPage = this.currentPages;
                var totalP = (this.totalpage) + 1;
                if (Number(this.fromPage) != totalP) {
                  var count = this.currentPages + limit;
                  if (Number(count) <= Number(this.totalpage)) {
                    this.selectBlogs(this.currentPages);
                  }
      
                }
              }
            }
          },
          setBlogItems: function () {
            if (this.blogDetails != undefined && this.blogDetails != undefined) {
              let start = 0;
              let end = Number(start) + Number(this.pageLimit);
              if (Number(this.currentPages) == 1) {
              } else {
                var limit = this.totalpage;
                start = (Number(this.currentPages) + Number(this.pageLimit)) - 2;
                end = Number(start) + Number(this.pageLimit);
              }
              this.blogList = this.blogDetails.slice(start, end);
            }
          },
          selectBlogs: function (ev) {
            let limit = 1;
            console.log(ev);
            this.currentPages = this.currentPage[ev];
      
            this.setBlogItems();
          },
          selected: function (ev) {
            let limit = 1;
            console.log(ev);
            this.currentPages = this.currentPage[ev];
      
            this.setBlogItems();
          },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/EuroAfrica/blog-detail.html?page=" + url;
                    console.log(url);
                    window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;


    }   
    },
    mounted: function() {  
        this.getData();
        this.getBlog();
    }
});

