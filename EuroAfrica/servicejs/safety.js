Vue.component('loading', VueLoading)
var aboutPage = new Vue({
    el:'#safety',
    name:'safety',
    data: {
        banner:{},
        mainSection:{},
        isLoading: false,
        fullPage: true,
    },
    methods: {
       getData: function() {
            var self = this;
            self.isLoading = true;
            getAgencycode(function(response) {
                var Agencycode = response;
                console.log(Agencycode);
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/My Safety/My Safety/My Safety.ftl';
                axios.get(aboutUsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                            var bannerDetail = pluck('Banner_Section', response.data.area_List);
                            self.banner = getAllMapData(bannerDetail[0].component);

                            var main = pluck('Main_Section', response.data.area_List);
                            self.mainSection = getAllMapData(main[0].component);
                        
                        self.isLoading = false;
                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });

        }
    },
    mounted: function() {  
        this.getData();
    }
});

