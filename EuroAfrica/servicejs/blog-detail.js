Vue.component('loading', VueLoading)
var contact = new Vue({
    el: '#blog',
    name: 'blog',
    data: {
        BannerSection: {},
        BlogDetailSection: {},
        Blog: {},
        isLoading: false,
        fullPage: true,
        blogusername: '',
        blogemail: '',
        blogwebsite: '',
        blogcomment: '',
        allComment: [],
        CategoryList: [],
        TagList: [],
        RecentPost: [],
        pageURLLink: '',
        searchItem: '',
        searchRecentItem: [],
        showSearch: false,
    },
    methods: {
        getValidationMsgByCode: function (code) {
            if (sessionStorage.validationItems !== undefined) {
                var validationList = JSON.parse(sessionStorage.validationItems);
                for (let validationItem of validationList.Validation_List) {
                    if (code === validationItem.Code) {
                        return validationItem.Message;
                    }
                }
            }
        },
        getBlogData: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                console.log(Agencycode);
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var aboutUsurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Blog Details/Blog Details/Blog Details.ftl';
                axios.get(aboutUsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    if (response.data.area_List.length > 0) {
                        var bannerDetail = pluck('Banner_Section', response.data.area_List);
                        self.BannerSection = getAllMapData(bannerDetail[0].component);
                        var mainsection = pluck('Detail_Section', response.data.area_List);
                        self.BlogDetailSection = getAllMapData(mainsection[0].component);

                        self.isLoading = false;
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });

        },
        getPageTitle: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var packageurl = getQueryStringValue('page');
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                if (packageurl != "") {
                    packageurl = packageurl.split('-').join(' ');
                    packageurl = packageurl.split('_')[0];
                    var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
                    self.pageURLLink = packageurl;
                    axios.get(topackageurl, {
                        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                    }).then(function (response) {
                        var pagecontent = pluck('Main_Section', response.data.area_List);
                        self.Blog = getAllMapData(pagecontent[0].component);

                    })
                }
            });
        },
        dateConvert: function (utc) {
            // this.value=this.date
            return (moment(utc).format("ddd ,DD-MM-YYYY"));
        },
        postcomment: async function () {
            if (!this.blogusername.trim()) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M01')).set('closable', false);

                return false;
            }
            if (!this.blogemail.trim()) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M02')).set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.blogemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M03')).set('closable', false);
                return false;
            }
            if (!this.blogcomment.trim()) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M18')).set('closable', false);
                return false;
            } else {
                self.isLoading = true;


                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Blog Comment",
                    keyword1: this.blogusername,
                    keyword2: this.blogemail,
                    keyword3: this.blogwebsite,
                    keyword4: this.pageURLLink,
                    text1: this.blogcomment,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);

                    this.blogusername = '';
                    this.blogemail = '';
                    this.blogwebsite = '';
                    this.blogcomment = '';
                    this.viewComments();
                    self.isLoading = false;
                    alertify.alert(this.getValidationMsgByCode('M15'), this.getValidationMsgByCode('M30'));
                   

                } catch (e) {
                    self.isLoading = false;
                }



            }
        },
        viewComments: async function () {
            var self = this;
            let allComment = [];
            let agencyCode = JSON.parse(localStorage.User).loginNode.code;
            let requestObject = { from: 0, size: 100, type: "Blog Comment", nodeCode: agencyCode, orderBy: "desc" };
            let responseObject = await cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {

                if (responseObject != undefined && responseObject.data != undefined) {
                    allResult = JSON.parse(JSON.stringify(responseObject)).data;

                    for (let i = 0; i < allResult.length; i++) {
                      if (allResult[i].keyword4 == self.pageURLLink) {
                        let object = {
                            Name: allResult[i].keyword1,
                            Date: moment(String(allResult[i].date1), "YYYY-MM-DDThh:mm:ss").format('ddd ,DD-MM-YYYY'),
                            comment: allResult[i].text1
                        };
                        allComment.push(object);
                      }
                    }
                    self.allComment = allComment;



                }

            });
        },
        getAllBlogs() {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var faqurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Blog List/Blog List/Blog List.ftl';
                axios.get(faqurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {

                    if (response.data != undefined) {

                        var blogDetails = response.data.Values;
                        for (const iterator of blogDetails) {
                            var date = moment(String(iterator.Date), "YYYY-MM-DDThh:mm:ss").format('ddd ,DD-MM-YYYY');
                            let post = { Title: iterator.Blog_Title, Image: iterator.Blog_Image_360x402px, Date: date,Link: iterator.More_details_page_link, Thumb:iterator.Thumbnail_Image_286_x_174px};
                            self.RecentPost.push(post);
                            iterator.More_details_page_link = iterator.More_details_page_link.split('-').join(' ');
                            iterator.More_details_page_link = iterator.More_details_page_link.split('_')[0];
                            var faqurl = huburl + portno + '/persons/source?path=/' + iterator.More_details_page_link + '.ftl';
                           
                            axios.get(faqurl, {
                                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                            }).then(function (response) {
                                if (response.data != undefined) {
                                    var pagecontent = pluck('Main_Section', response.data.area_List);
                                    var Category = pluckcom('Category_Name', pagecontent[0].component);
                                    var tag = pluckcom('Tag_List', pagecontent[0].component);
                                    // var tittle = self.pluckcom('Blog_Title', pagecontent[0].component);
                                    // var image = self.pluckcom('Blog_Image_360x402px', pagecontent[0].component);
                                    // var date = moment(String(self.pluckcom('Date', pagecontent[0].component)), "YYYY-MM-DDThh:mm:ss").format('ddd ,DD-MM-YYYY');
                                    if(Category  !== undefined && Category.length !== 0) {
                                        let object = { Name: Category, count: 1 }

                                        if (self.CategoryList.length > 0) {
                                            var catCount = 0;
                                            for (const list of self.CategoryList) {
                                                if (Category.toUpperCase() == list.Name.toUpperCase()) {
                                                    catCount = catCount + 1;
                                                    list.count = list.count + 1;
                                                    break;
                                                }
                                            }
                                            if(catCount == 0) {
                                                self.CategoryList.push(object);
                                            }
    
                                        } else {
                                            self.CategoryList.push(object);
                                        }
                                    }
                                   if(tag !== undefined && tag.length !== 0) {
                                    for (const iterator of tag) {
                                        var count = 0;
                                        if (self.TagList.length > 0) {
                                            for (const list of self.TagList) {

                                                if (list.toUpperCase() == iterator.Name.toUpperCase()) {
                                                    count = count + 1;
                                                }

                                            }
                                            if (count == 0) {
                                                self.TagList.push(iterator.Name);
                                            }
                                        } else {
                                            self.TagList.push(iterator.Name);
                                        }
                                    }
                                   }

                                }
                            }).catch(function (error) {
                                console.log('Error');
                                self.isLoading = false;
                            });
                        }
                        self.isLoading = false;
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });
        },
        // search: async function () {
        //   var self = this;
        //   self.searchRecentItem = [];
        //   self.showSearch = false;
        //   if(self.searchItem !== '') {
        //     self.showSearch = true;
        //     for (const iterator of self.RecentPost) {
        //         if(iterator.Tittle.toUpperCase().includes(self.searchItem.toUpperCase())) {
        //             self.searchRecentItem.push(iterator);
        //         }
        //     }
        //   }
        // },
        // clickoutside: function () {
        //     var self = this;
        //     self.showSearch = false;
        //     self.searchItem = '';
        // },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/EuroAfrica/blog-detail.html?page=" + url;
                    console.log(url);
                    window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;
        },
        navigateSocialPage(url){
            if(url.includes('?')){
            var fullUrl=url+"="+encodeURIComponent(window.location.href);
            window.open(fullUrl, "_blank");
            } else {
            window.open(url, "_blank");
            }
        },
        passQuery(felid,qury) {
            if(felid == 'tag') {
                qury = "/EuroAfrica/blogs.html?tag=" + qury;
            } else {
                qury = "/EuroAfrica/blogs.html?category=" + qury;
            }
            console.log(qury);
            window.location.href = qury; 
        }
    },
    mounted: function () {
        this.getBlogData();
        this.getPageTitle();
        this.getAllBlogs();
        this.viewComments();
    }
});
function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));

}

