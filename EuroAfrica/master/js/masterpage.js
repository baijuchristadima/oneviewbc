var HeaderComponent = Vue.component('headeritem', {
    template: `  <div id="header">
    <div class="top">
    <div class="vld-parent">
    <loading :active.sync="isLoading" :can-cancel="false" :is-full-page="fullPage"></loading>
    </div>
      <div class="container">
        <div class="row ar_direction">
          <div class="col-lg-6 col-sm-7 col-md-7 hidden-xs">
            <div class="contact-info">
              <div class="phone-number"> <i :class="'fa '+ TopSection.Phone_Icon" aria-hidden="true"></i> <a :href="'tel:'+TopSection.Phone_Number">{{TopSection.Phone_Number}}</a> </div>
              <div class="email"> <i :class="'fa '+ TopSection.Email_Icon" aria-hidden="true"></i> <a :href="'mailto:'+TopSection.Email_Address"> {{TopSection.Email_Address}}</a> </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-5 col-sm-5 col-xs-12 pull-right">
        
          <div class="lang lag">
          <lang-select @languagechange="getpagecontent"></lang-select>
          </div>
          
            <div class="login">
            <a id="sign-bt-area" class="btn login-btn  js-signin-modal-trigger" data-signin="login" v-show="!userlogined"><i :class="'fa '+ TopSection.Login_User_Icon" aria-hidden="true"></i>{{TopSection.Login_Label}}</a>

            <!--<a class="btn"  href="#" data-toggle="modal" data-target="#myModal">View booking</a>-->
              <a>
              <!--iflogin-->
              <label id="bind-login-info" for="profile2" class="profile-dropdown btn" v-show="userlogined">
                  <input type="checkbox" id="profile2">
                  <img src="/assets/images/user.png">
                  <span>{{userinfo.firstName+' '+userinfo.lastName }}</span>
                  <label for="profile2">
                      <i class="fa fa-angle-down" aria-hidden="true"></i>
                  </label>
                  <ul>
                      <li>
                          <a href="/customer-profile.html">
                              <i class="fa fa-th-large" aria-hidden="true"></i>{{TopSection.Dashboard_Label}}
                          </a>
                      </li>
                      <li>
                          <a href="/my-profile.html">
                              <i class="fa fa-user" aria-hidden="true"></i>{{TopSection.My__Profile_Label}}
                          </a>
                      </li>
                      <li>   
                          <a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i>{{TopSection.My_Booking_Label}}</a>
                      </li>
                      <li>
                          <a href="#" v-on:click.prevent="logout"><i class="fa fa-power-off" aria-hidden="true"></i>{{TopSection.Logout_Label}}</a>
                      </li>
                  </ul>
              </label>
              </a>
              <!--ifnotlogin-->
            </div>
          </div>
        </div>
      </div>
    </div>
    <nav id="navbar-main" class="navbar navbar-default ar_direction">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a class="navbar-brand" href="/EuroAfrica/index.html"><img :src="MainSection.Logo_297x97px" alt=""></a> </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
              <ul class="nav navbar-nav ar_direction">
                <li :class="{ 'active': activeClass === '/index.html'}"><a href="/EuroAfrica/index.html">{{MainSection.Home_Label}}</a></li>
                <li :class="{ 'active': activeClass === '/about-us.html'}"><a href="/EuroAfrica/about-us.html">{{MainSection.About_Us_Label}}</a></li>
                <li :class="{ 'active': activeClass === '/holidays.html'}"><a href="/EuroAfrica/holidays.html">{{MainSection.Holidays_Label}}</a></li>
                <li :class="{ 'active': activeClass === '/blogs.html'}"><a href="/EuroAfrica/blogs.html">{{MainSection.Blog_Label}}</a></li>
                <li :class="{ 'active': activeClass === '/faq.html'}"><a href="/EuroAfrica/faq.html">{{MainSection.Faq_Label}}</a></li>
                <li :class="{ 'active': activeClass === '/contact-us.html'}"><a href="/EuroAfrica/contact-us.html">{{MainSection.Contact_Us_Label}}</a></li>
                <li v-if="checkMobileOrNot" :class="{ 'active': activeClass === '/privacypolicy.html'}"><a href="/EuroAfrica/privacypolicy.html">{{FooterSection.Privacy_Label}}</a></li> 
                <li v-if="checkMobileOrNot" :class="{ 'active': activeClass === '/termsandconditions.html'}"><a href="/EuroAfrica/termsandconditions.html">{{FooterSection.Terms_Label}}</a></li> 
                </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
    <div class="safety-belt ar_direction">
      <div class="container">
        <div class="row">
          <div class="col-lg-9 col-md-10 col-sm-12 col-xs-12 pull-right"> <img :src="MainSection.Safety_Image_130x35px" alt="">
            <p>{{MainSection.Safety_Description}}</p>
            <a href="/EuroAfrica/safety.html" class="explore-btn">{{MainSection.Safety_Label}}</a> </div>
        </div>
      </div>
    </div>
    <!--popup area-->
    <div class="cd-signin-modal js-signin-modal"> 
        <div class="cd-signin-modal__container"> <!-- this is the container wrapper -->
            <ul class="cd-signin-modal__switcher js-signin-modal-switcher js-signin-modal-trigger">
                <li><a href="#0" data-signin="login" data-type="login">{{TopSection.Sign_In_Label}}</a></li>
                <li><a href="#0" data-signin="signup" data-type="signup">{{TopSection.Register_Label}}</a></li>
            </ul>
            <div class="cd-signin-modal__block js-signin-modal-block" data-type="login"> <!-- log in form -->
                <div class="cd-signin-modal__form">
                    <p class="cd-signin-modal__fieldset">
                        <label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" 
                            for="signin-email">Email</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                            id="signin-email" type="email" :placeholder="TopSection.Email_Placeholder" v-model="username" >
                            <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': usererrormsg.empty||usererrormsg.invalid }">{{usererrormsg.empty?getValidationMsgByCode('M02'):(usererrormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                    </p>
                    <p class="cd-signin-modal__fieldset">
                        <label class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace" 
                            for="signin-password">Password</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                            id="signin-password" type="password"  :placeholder="TopSection.Password_Placeholder"  v-model="password" >
                            <a v-on:click="showhidepassword" class="cd-signin-modal__hide-password js-hide-password changeShowTxtCls">{{TopSection.Show_Label}}</a>
                            <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': psserrormsg }">
                            {{getValidationMsgByCode('M34')}}</span>
                    </p>

                    <p class="cd-signin-modal__fieldset">
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width" type="submit" v-on:click="loginaction" :value="TopSection.Login_Button_Label">
                    </p>
                    <div id="myGoogleButton"></div>
                    <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false"
                        data-use-continue-as="false" onlogin="checkLoginState();">
                    </div>
                    <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="reset">{{TopSection.Forgot_Password_Label}}</a></p>
                </div>
            
            
        </div> <!-- cd-signin-modal__block -->

        <div class="cd-signin-modal__block js-signin-modal-block" data-type="signup"> <!-- sign up form -->
            <div class="cd-signin-modal__form">
                <p class="cd-signin-modal__fieldset">
                    <label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">Title</label>                        
                    <select v-model="registerUserData.title" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border down_select" id="signup-title">
                        <option selected>Mr</option>
                        <option selected>Ms</option>
                        <option>Mrs</option>
                    </select>
                    <span class="cd-signin-modal__error">Title seems incorrect!</span>
                </p>
                <p class="cd-signin-modal__fieldset">
                    <label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">First Name</label>
                    <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                    id="signup-firstname" type="text" :placeholder="TopSection.First_Name_Placeholder" v-model="registerUserData.firstName">
                    <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userFirstNameErormsg }" >{{getValidationMsgByCode('M35')}}</span>
                </p>

                <p class="cd-signin-modal__fieldset">
                    <label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">Last Name</label>
                    <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                    id="signup-lastname" type="text" :placeholder="TopSection.Last_Name_Placeholder" v-model="registerUserData.lastName">
                    <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userLastNameErrormsg }">{{getValidationMsgByCode('M36')}}</span>
                </p>

                <p class="cd-signin-modal__fieldset">
                    <label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" for="signup-email">Email</label>
                    <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                    id="signup-email" type="email" :placeholder="TopSection.Email_Placeholder" v-model="registerUserData.emailId">
                    <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userEmailErormsg.empty||userEmailErormsg.invalid }">{{userEmailErormsg.empty?getValidationMsgByCode('M02'):(userEmailErormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                </p>

                <p class="cd-signin-modal__fieldset">
                    <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding" type="submit" 
                    :value="TopSection.Create_Account_Button_Label" v-on:click="registerUser">
                </p>
                <div id="myGoogleButtonReg"></div>
                <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false"
    data-use-continue-as="false" onlogin="checkLoginState();"></div>                    
            </div>
        </div> <!-- cd-signin-modal__block -->

        <div class="cd-signin-modal__block js-signin-modal-block" data-type="reset"> <!-- reset password form -->
            <p class="cd-signin-modal__message" v-html="TopSection.Forgot_Password_Description"></p>

            <div class="cd-signin-modal__form">
                <p class="cd-signin-modal__fieldset">
                    <label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" for="reset-email">E-mail</label>
                    <input v-model="emailId" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" id="reset-email" type="email" :placeholder="TopSection.Email_Placeholder">
                    <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userforgotErrormsg.empty||userforgotErrormsg.invalid }">{{userforgotErrormsg.empty?getValidationMsgByCode('M02'):(userforgotErrormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                </p>

                <p class="cd-signin-modal__fieldset">
                    <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding" type="submit" 
                    :value="TopSection.Reset_Password_Button_Label" v-on:click="forgotPassword">
                </p>
                <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="login">{{TopSection.Back_To_Login_Label}}</a></p>
            </div>

            
        </div> <!-- cd-signin-modal__block -->
        <a href="#0" class="cd-signin-modal__close js-close">Close</a>
    </div> <!-- cd-signin-modal__container -->
</div> 
    <!--popup area close-->
    <div v-show="false">
            <currency-select></currency-select>          
        </div>
    </div>
`,
    data() {
        return {
            // header: {
            //     logo_Image: '',
            //     Image_Name: '',
            //     Home_Label: '',
            //     Flights_Label: '',
            //     Hotels_Label: '',
            //     Holidays_Label: '',
            //     View_Bookings_Label: '',
            //     Login_Label: '',
            //     Agent_Login_Label: '',




            // },
            activePageName: '',
            username: '',
            password: '',
            emailId: '',
            usererrormsg: { empty: false, invalid: false },
            psserrormsg: false,
            userFirstNameErormsg: false,
            userLastNameErrormsg: false,
            userEmailErormsg: { empty: false, invalid: false },
            userPasswordErormsg: false,
            userVerPasswordErormsg: false,
            userPwdMisMatcherrormsg: false,
            userTerms: false,
            userforgotErrormsg: { empty: false, invalid: false },
            registerUserData: {
                firstName: '',
                lastName: '',
                emailId: '',
                password: '',
                verifyPassword: '',
                terms: '',
                title: 'Mr'
            },
            Languages: [],
            retrieveBookRefid: '',
            getdata: true,
            bEmail: '',
            bRef: '',
            bService: 'F',
            show: 'show',
            hide: 'hide',
            retrieveBkngRefErormsg: false,
            userlogined: this.checklogin(),
            userlogined: '',
            userinfo: [],
            language: 'en',
            active_el: (sessionStorage.active_el) ? sessionStorage.active_el : 0,
           
            contactUsPage: {},
            TopSection: {},
            MainSection: {},
            DeviceInfo:"",
            pageType:"",
            PageName:"",
            activeClass: '',
            isLoading: false,
            fullPage: true,
            FooterSection:{}
        }
    },
    created() {
        var self=this;
        var pathArray = window.location.pathname.split('/');
        self.activeClass ='/'+ pathArray[pathArray.length-1];
       
    },
    computed: {
        checkMobileOrNot: function () {
          if( screen.width <= 1020 ) {
            return true;
        }
        else {
            return false;
        }
        },
    },
    methods: {
        activate: function (el) {
            sessionStorage.active_el = el;
            this.active_el = el;
            if (el == 1 || el == 2) {
                maininstance.actvetab = el;
            }
            else {
                maininstance.actvetab = 0;
            }



        },
        getpagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/01 Master/01 Master/01 Master.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    if (response.data.area_List.length) {
                        var headerSection = pluck('Top_Section', response.data.area_List);
                        self.TopSection = getAllMapData(headerSection[0].component);
                        var mainContent = pluck('Main_Section', response.data.area_List);
                        self.MainSection = getAllMapData(mainContent[0].component);

                        var FooterContent = pluck('Footer_Section', response.data.area_List);
                        self.FooterSection = getAllMapData(FooterContent[0].component);
                    }
                    self.getdata = false;
                        Vue.nextTick(function () {
                            (

                                function () {
                                    self.active_el = (sessionStorage.active_el) ? sessionStorage.active_el : 0;
                                    //Login/Signup modal window - by CodyHouse.co
                                    function ModalSignin(element) {
                                        this.element = element;
                                        this.blocks = this.element.getElementsByClassName('js-signin-modal-block');
                                        this.switchers = this.element.getElementsByClassName('js-signin-modal-switcher')[0].getElementsByTagName('a');
                                        this.triggers = document.getElementsByClassName('js-signin-modal-trigger');
                                        this.hidePassword = this.element.getElementsByClassName('js-hide-password');
                                        this.init();
                                    };

                                    ModalSignin.prototype.init = function () {
                                        var self1 = this;
                                        //open modal/switch form
                                        for (var i = 0; i < this.triggers.length; i++) {
                                            (function (i) {
                                                self1.triggers[i].addEventListener('click', function (event) {
                                                    if (event.target.hasAttribute('data-signin')) {
                                                        event.preventDefault();
                                                        self1.showSigninForm(event.target.getAttribute('data-signin'));
                                                    }
                                                });
                                            })(i);
                                        }

                                        //close modal
                                        this.element.addEventListener('click', function (event) {
                                            if (hasClass(event.target, 'js-signin-modal') || hasClass(event.target, 'js-close')) {
                                                event.preventDefault();
                                                removeClass(self1.element, 'cd-signin-modal--is-visible');
                                            }
                                        });
                                        //close modal when clicking the esc keyboard button
                                        document.addEventListener('keydown', function (event) {
                                            (event.which == '27') && removeClass(self1.element, 'cd-signin-modal--is-visible');
                                        });

                                        // //hide/show password
                                        // for (var i = 0; i < this.hidePassword.length; i++) {
                                        //     (function(i) {
                                        //         self1.hidePassword[i].addEventListener('click', function(event) {
                                        //             self1.togglePassword(self1.hidePassword[i]);
                                        //         });
                                        //     })(i);
                                        // }

                                        //IMPORTANT - REMOVE THIS - it's just to show/hide error messages in the demo

                                    };

                                    // ModalSignin.prototype.togglePassword = function(target) {
                                    //     var password = target.previousElementSibling;
                                    //     ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text'): password.setAttribute('type', 'password');
                                    //     target.textContent = ('Hide' == target.textContent) ? 'Show' : 'Hide';
                                    //     putCursorAtEnd(password);
                                    // }

                                    ModalSignin.prototype.showSigninForm = function (type) {
                                        // show modal if not visible
                                        !hasClass(this.element, 'cd-signin-modal--is-visible') && addClass(this.element, 'cd-signin-modal--is-visible');
                                        // show selected form
                                        for (var i = 0; i < this.blocks.length; i++) {
                                            this.blocks[i].getAttribute('data-type') == type ? addClass(this.blocks[i], 'cd-signin-modal__block--is-selected') : removeClass(this.blocks[i], 'cd-signin-modal__block--is-selected');
                                        }
                                        //update switcher appearance
                                        var switcherType = (type == 'signup') ? 'signup' : 'login';
                                        for (var i = 0; i < this.switchers.length; i++) {
                                            this.switchers[i].getAttribute('data-type') == switcherType ? addClass(this.switchers[i], 'cd-selected') : removeClass(this.switchers[i], 'cd-selected');
                                        }
                                    };

                                    ModalSignin.prototype.toggleError = function (input, bool) {
                                        // used to show error messages in the form
                                        toggleClass(input, 'cd-signin-modal__input--has-error', bool);
                                        toggleClass(input.nextElementSibling, 'cd-signin-modal__error--is-visible', bool);
                                    }

                                    var signinModal = document.getElementsByClassName("js-signin-modal")[0];
                                   
                                    if (signinModal) {
                                        new ModalSignin(signinModal);
                                    }

                                    // toggle main navigation on mobile
                                    var mainNav = document.getElementsByClassName('js-main-nav')[0];
                                    if (mainNav) {
                                        mainNav.addEventListener('click', function (event) {
                                            if (hasClass(event.target, 'js-main-nav')) {
                                                var navList = mainNav.getElementsByTagName('ul')[0];
                                                toggleClass(navList, 'cd-main-nav__list--is-visible', !hasClass(navList, 'cd-main-nav__list--is-visible'));
                                            }
                                        });
                                    }

                                    //class manipulations - needed if classList is not supported
                                    function hasClass(el, className) {
                                        if (el.classList) return el.classList.contains(className);
                                        else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
                                    }

                                    function addClass(el, className) {
                                        var classList = className.split(' ');
                                        if (el.classList) el.classList.add(classList[0]);
                                        else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
                                        if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
                                    }

                                    function removeClass(el, className) {
                                        var classList = className.split(' ');
                                        if (el.classList) el.classList.remove(classList[0]);
                                        else if (hasClass(el, classList[0])) {
                                            var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
                                            el.className = el.className.replace(reg, ' ');
                                        }
                                        if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
                                    }

                                    function toggleClass(el, className, bool) {
                                        if (bool) addClass(el, className);
                                        else removeClass(el, className);
                                    }
                                    // $("#modal_retrieve").leanModal({
                                    //     top: 100,
                                    //     overlay: 0.6,
                                    //     closeButton: ".modal_close"
                                    // });
                                    //credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
                                    function putCursorAtEnd(el) {
                                        if (el.setSelectionRange) {
                                            var len = el.value.length * 2;
                                            el.focus();
                                            el.setSelectionRange(len, len);
                                        } else {
                                            el.value = el.value;
                                        }
                                    };
                                })();
                        }.bind(self));
                }).catch(function (error) {
                    console.log('Error');
                });
            });
        },
        getValidationMsgs: function () {
          var self = this;
          getAgencycode(function (response) {
              var Agencycode = response;
              var huburl = ServiceUrls.hubConnection.cmsUrl;
              var portno = ServiceUrls.hubConnection.ipAddress;
              var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
              var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Validation/Validation/Validation.ftl';
              axios.get(pageurl, {
                  headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
              }).then(function (response) {
                  if (response.data.area_List.length) {
                      var validations = pluck('Validations', response.data.area_List);
                      self.validationList = getAllMapData(validations[0].component);
                      sessionStorage.setItem('validationItems', JSON.stringify(self.validationList));
                  }
              }).catch(function (error) {
                  console.log('Error');
              });
          });
      },
        loginaction: function () {

            if (!this.username.trim()) {
                this.usererrormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.username.trim())) {
                this.usererrormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.usererrormsg = { empty: false, invalid: false };
            }
            if (!this.password) {
                this.psserrormsg = true;
                return false;

            } else {
                this.psserrormsg = false;
                var self = this;
                self.isLoading = true;
                login(this.username, this.password, function (response) {
                    if (response == false) {
                        self.userlogined = false;
                        self.isLoading = false;
                        alertify.alert(this.getValidationMsgByCode('M07'),this.getValidationMsgByCode('M31'));
                    } else {
                        self.userlogined = true;
                        self.userinfo = JSON.parse(localStorage.User);
                        $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                        try {
                            self.$eventHub.$emit('logged-in', { userName: self.username, password: self.password });
                            signArea.headerLogin({ userName: self.username, password: self.password })
                            self.isLoading = false;
                        } catch (error) {
                            self.isLoading = false;
                        }
                    }
                });

            }



        },
        validEmail: function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        checklogin: function () {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);
                return true;
            } else {
                this.userlogined = false;
                return false;
            }
        },
        logout: function () {
            this.userlogined = false;
            this.userinfo = [];
            localStorage.profileUpdated = false;
            try {
                this.$eventHub.$emit('logged-out');
                signArea.logout()
            } catch (error) {

            }
            commonlogin();
            // signOut();
            // signOutFb();
        },
        registerUser: function () {
            if (this.registerUserData.firstName.trim() == "") {
                this.userFirstNameErormsg = true;
                return false;
            } else {
                this.userFirstNameErormsg = false;
            }
            if (this.registerUserData.lastName.trim() == "") {
                this.userLastNameErrormsg = true;
                return false;
            } else {
                this.userLastNameErrormsg = false;
            }
            if (this.registerUserData.emailId.trim() == "") {
                this.userEmailErormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.registerUserData.emailId.trim())) {
                this.userEmailErormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.userEmailErormsg = { empty: false, invalid: false };
            }
            var vm = this;
            vm.isLoading = true;
            registerUser(this.registerUserData.emailId, this.registerUserData.firstName, this.registerUserData.lastName, this.registerUserData.title, function (response) {
                if (response.isSuccess == true) {
                    vm.username = response.data.data.user.loginId;
                    vm.password = response.data.data.user.password;
                    login(vm.username, vm.password, function (response) {
                        if (response != false) {
                            $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                            window.location.href = "/edit-my-profile.html?edit-profile=true";
                            vm.isLoading = false;
                        }
                    });
                }
                vm.isLoading = false;
            });
        },

        forgotPassword: function () {
            if (this.emailId.trim() == "") {
                this.userforgotErrormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.emailId.trim())) {
                this.userforgotErrormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.userforgotErrormsg = { empty: false, invalid: false };
            }
            this.isLoading = true;
            var datas = {
                emailId: this.emailId,
                agencyCode: localStorage.AgencyCode,
                logoUrl: window.location.origin + "/" + localStorage.AgencyFolderName + "/website-informations/logo/logo.png",
                websiteUrl: window.location.origin,
                resetUri: window.location.origin + "/" + localStorage.AgencyFolderName + "/reset-password.html"
            };
            $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:none;background:grey !important;");

            var huburl = ServiceUrls.hubConnection.baseUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var requrl = ServiceUrls.hubConnection.hubServices.forgotPasswordUrl;
            axios.post(huburl + portno + requrl, datas)
                .then((response) => {
                    if (response.data != "") {
                        alertify.alert(this.getValidationMsgByCode('M07'),response.data);
                        this.isLoading = false;
                    } else {
                        alertify.alert(this.getValidationMsgByCode('M07'),this.getValidationMsgByCode('M32'));
                        this.isLoading = false;
                    }
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:var(--primary-color) !important");

                })
                .catch((err) => {
                    console.log("FORGOT PASSWORD  ERROR: ", err);
                    this.isLoading = false;
                    if (err.response.data.message == 'No User is registered with this emailId.') {
                        alertify.alert(this.getValidationMsgByCode('M07'),err.response.data.message);
                        this.isLoading = false;
                    } else {
                        alertify.alert(this.getValidationMsgByCode('M07'),this.getValidationMsgByCode('M33'));
                        this.isLoading = false;
                    }
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:var(--primary-color) !important");

                })

        },
        showhidepassword: function (event) {
            var password = event.target.previousElementSibling;
            ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text') : password.setAttribute('type', 'password');
            event.target.textContent = (this.hide == event.target.textContent) ? this.show : this.hide;
        },
        Getvisit: function () {
            var self = this;
            var currentUrl = window.location.pathname;
            if (currentUrl == "/EuroAfrica/index.html") {
              self.pageType = "Master";
              self.PageName = "Home"
            }
            else if (currentUrl == "/EuroAfrica/holiday-detail.html") {
              self.pageType = "Package details";
              self.PageName = packageView.mainContent.Package_Name;
            } 
            else {
              self.PageName = currentUrl;
              self.pageType = "Master";
            }
            var TempDeviceInfo = detect.parse(navigator.userAgent);
            self.DeviceInfo = TempDeviceInfo;
            this.showYourIp();
          },
          showYourIp: function () {
            var self = this;
            var ipUrl = ServiceUrls.externalServiceUrl.ipAddressApi;
            fetch(ipUrl)
              .then(x => x.json())
              .then(({ ip }) => {
                let Ipaddress = ip;
                let sessionids = ip.replace(/\./g, '');
                this.sendip(Ipaddress,sessionids);
              });
          },
          sendip: async function (Ipaddress,sessionids) {
            var self = this;
            let agencyCode = JSON.parse(localStorage.User).loginNode.code;
            let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
            let insertVisitData = {
              type: "Visit",
              keyword1: sessionids,
              ip1: Ipaddress,
              keyword2: self.pageType,
              keyword3: self.PageName,
              keyword4: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'AED',
              keyword5: (localStorage.Languagecode) ? localStorage.Languagecode : "en",
              text1: self.DeviceInfo.device.type,
              text2: self.DeviceInfo.os.name,
              text3: self.DeviceInfo.browser.name,
              date1: requestedDate,
              nodeCode: agencyCode
            };
            cmsRequestData("POST", "cms/data", insertVisitData, null);

          },
          getValidationMsgByCode: function (code) {
            if (sessionStorage.validationItems !== undefined) {
              var validationList = JSON.parse(sessionStorage.validationItems);
              for (let validationItem of validationList.Validation_List) {
                if (code === validationItem.Code) {
                  return validationItem.Message;
                }
              }
            }
          }
    },
    mounted: function () {
        document.onreadystatechange = () => {
            if (document.readyState == "complete") {
                if (localStorage.direction == 'rtl') {
                    document.getElementById("styleid").setAttribute("href", 'css/rtl-style.css');
                    document.getElementById("mediaid").setAttribute("href", 'css/rtl-media.css');
                    $(".ar_direction").addClass("ar_direction1");
                    alertify.defaults.glossary.ok = 'حسنا';
                } else {
                    document.getElementById("styleid").setAttribute("href", 'css/ltr-style.css');
                    document.getElementById("mediaid").setAttribute("href", 'css/ltr-media.css');
                    $(".ar_direction").removeClass("ar_direction1");
                    alertify.defaults.glossary.ok = 'OK';
                }
            }
        }
        if (localStorage.IsLogin == "true") {
            this.userlogined = true;
            this.userinfo = JSON.parse(localStorage.User);

        } else {
            this.userlogined = false;

        }
        this.getpagecontent();
        this.getValidationMsgs();
        setTimeout(() =>{
        this.Getvisit();
        },1000)
        
    },
    watch: {
        updatelogin: function () {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);

            } else {
                this.userlogined = false;

            }

        }
    }
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});

Vue.component('footeritem', {
    props: {
        item: Number
    },
    template: `
    <div id="footer" class="ar_direction">
    <div id="newsletter" :style="{ background: 'url(' + NewsletterSection.Background_Image_1920x466px+ ')'}" v-if="!checkMobileOrNot">
    <div class="container">
      <div class="row title">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <svg x="0px" y="0px" width="68px" height="68px" viewBox="0 0 68 68" enable-background="new 0 0 68 68" xml:space="preserve">
  <g>
      <g>
          <path d="M61.797,8.311H6.204c-3.265,0-5.928,2.661-5.928,5.928v39.523c0,3.256,2.652,5.928,5.928,5.928h55.593    c3.256,0,5.928-2.652,5.928-5.928V14.239C67.725,10.982,65.072,8.311,61.797,8.311z M60.979,12.263L34.126,39.117L7.042,12.263    H60.979z M4.228,52.941V15.038l19.033,18.87L4.228,52.941z M7.022,55.736l19.045-19.045l6.674,6.615    c0.772,0.768,2.02,0.766,2.788-0.006l6.506-6.506l18.943,18.941H7.022z M63.773,52.941L44.832,34l18.941-18.942V52.941z"/>
      </g>
  </g>
  </svg>
           <h3>{{NewsletterSection.Title}}</h3>
           <p v-html="NewsletterSection.Description"></p>
        </div>
      </div>
      <div class="row">
          <form>
              <div class="col-lg-offset-2 col-md-offset-1 col-lg-3 col-md-4 col-sm-4 col-xs-12"><input class="input" :placeholder="NewsletterSection.Name_Placeholder" type="text"  v-model="newsltrname"></div>
              <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"><input class="input" :placeholder="NewsletterSection.Email_Placeholder" type="text"  v-model="newsltremail"></div>
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12"><button type="submit" class="sub-btn" @click.prevent="sendnewsletter">{{NewsletterSection.Submit_Label}}</button></div>
          </form>
      </div>
    </div>
  </div>
    <div class="container" v-if="!checkMobileOrNot">
      <div class="inner">
          <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 footer-align">
              <div class="about">
                <div class="ftr-logo"><img :src="FooterSection.Logo_240x64px" alt=""></div>
                <p v-html="FooterSection.Foo_Description"></p>
                <ul class="social">
                <li v-for="item in FooterSection.Social_Media_List"><a :href="item.Url" target="_blank"><i class="fa" v-bind:class="item.Item_Icon"></i></a></li>
               
                </ul>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-align">
              <div class="link">
                <h3>{{FooterSection.Quick_Link_Label}}</h3>
                <ul>
                  <li><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="/EuroAfrica/index.html">{{MainSection.Home_Label}}</a></li>
                  <li><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="/EuroAfrica/about-us.html">{{MainSection.About_Us_Label}}</a></li>
                  <li><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="/EuroAfrica/holidays.html">{{MainSection.Holidays_Label}}</a></li>
                  <li><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="/EuroAfrica/blogs.html">{{MainSection.Blog_Label}}</a></li>
                  <li><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="/EuroAfrica/contact-us.html">{{MainSection.Contact_Us_Label}}</a></li>
                  <li><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="/EuroAfrica/privacypolicy.html">{{FooterSection.Privacy_Label}}</a></li> 
                  <li><i class="fa fa-dot-circle-o" aria-hidden="true"></i><a href="/EuroAfrica/termsandconditions.html">{{FooterSection.Terms_Label}}</a></li> 
                </ul>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 footer-align">
              <div class="contact ar_direction">
                <h3>{{FooterSection.Get_In_Touch_Label}}</h3>
                <ul>
                  <li>
                    <i :class="'fa '+ FooterSection.Location_Icon" aria-hidden="true"></i>{{FooterSection.Location_Label}}
                    <span>{{FooterSection.Location_Description}}</span></li>
                  <li>
                    <i :class="'fa '+ TopSection.Email_Icon" aria-hidden="true"></i>{{FooterSection.Email_Label}}
                    <span><a :href="'mailto:'+TopSection.Email_Address">{{TopSection.Email_Address}}</a></span></li>
                  <li>
                    <i :class="'fa '+ TopSection.Phone_Icon" aria-hidden="true"></i>{{FooterSection.Phone_Label}}
                    <span><a :href="'tel:'+TopSection.Phone_Number">{{TopSection.Phone_Number}}</a></span></li>
                  </ul>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="ftr-btm ar_direction">
      <div class="container">
          <div class="inner">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 footer-align">
                  <div class="copyright">{{FooterSection.Copy_Right_Description}}</div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 footer-align">
                  <div class="powered-by">{{FooterSection.Powered_By_Label}} &nbsp; <a href="http://www.oneviewit.com/" target="_blank"><img alt="" :src="FooterSection.Powered_By_Logo_95x34px"></a></div>
                </div>
              </div>
              </div>
              <div class="back-top text-center" title="Back to Top"> <a href="#" id="toTop" class="scrollToTop" style="display: inline;"> <i class="fa fa-arrow-circle-up" aria-hidden="true"></i> </a> </div>
      </div>
    </div>
    </div>
    `,
    data() {
        return {
            newsltremail: null,
            newsltrname: null,
            TopSection:{},
            MainSection:{},
            FooterSection:{},
            NewsletterSection:{},
        }
    },
     computed: {
        checkMobileOrNot: function () {
            if( screen.width <= 1020 ) {
                return true;
            }
            else {
                return false;
            }
        },
    },
    methods: {
        getpagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/01 Master/01 Master/01 Master.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    if (response.data.area_List.length) {
                        var headerSection = pluck('Top_Section', response.data.area_List);
                        self.TopSection = getAllMapData(headerSection[0].component);
                        var mainContent = pluck('Main_Section', response.data.area_List);
                        self.MainSection = getAllMapData(mainContent[0].component);
                        var FooterContent = pluck('Footer_Section', response.data.area_List);
                        self.FooterSection = getAllMapData(FooterContent[0].component);
                        var NewsletterContent = pluck('Newsletter_Section', response.data.area_List);
                        self.NewsletterSection = getAllMapData(NewsletterContent[0].component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                });
            });

        },
        getValidationMsgByCode: function (code) {
          if (sessionStorage.validationItems !== undefined) {
            var validationList = JSON.parse(sessionStorage.validationItems);
            for (let validationItem of validationList.Validation_List) {
              if (code === validationItem.Code) {
                return validationItem.Message;
              }
            }
          }
        },
        sendnewsletter: async function () {
          if(!this.newsltrname) {
            alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M01')).set('closable', false);
          return false; 
           }
            if (!this.newsltremail) {
              alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M02')).set('closable', false);
              return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
              alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M03')).set('closable', false);
              return false;
            } else {
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                var filterValue = "type='Newsletter' AND keyword2='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M04')).set('closable', false);
                    return false;
                }
                else {
                  try {
                    var frommail = JSON.parse(localStorage.User).loginNode.email;
                    var custmail = {
                      type: "UserAddedRequest",
                      fromEmail: frommail,
                      toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                      // logo: this.ContactForm.Logo || "",
                      logo: ServiceUrls.hubConnection.logoBaseUrl +JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                      agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                      agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                      personName: this.newsltrname,
                      primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                      secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                    };
                    let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = {
                      type: "Newsletter",
                      date1: requestedDate,
                      keyword1: this.newsltrname,
                      keyword2: this.newsltremail,
                      nodeCode: agencyCode
      
      
      
                    };
                     let responseObject = await cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                      let insertID = Number(responseObject);
                      this.newsltremail = '';
                      this.newsltrname = '';
                      var emailApi = ServiceUrls.emailServices.emailApi;
                      sendMailService(emailApi, custmail);
                      alertify.alert(this.getValidationMsgByCode('M15'), this.getValidationMsgByCode('M05')).set('closable', false);
                    } catch (e) {
                    }
                  } catch (error) {
                  }
                }
            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {
            var allDBData = [];
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        }

    },
    mounted: function () {
      document.onreadystatechange = () => {
        if (document.readyState == "complete") {
            if (localStorage.direction == 'rtl') {
                $(".ar_direction").addClass("ar_direction1");
                document.getElementById("styleid").setAttribute("href", 'css/rtl-style.css');
                document.getElementById("mediaid").setAttribute("href", 'css/rtl-media.css');
                 alertify.defaults.glossary.ok = 'حسنا';
            } else {
                $(".ar_direction").removeClass("ar_direction1");
                document.getElementById("styleid").setAttribute("href", 'css/ltr-style.css');
                document.getElementById("mediaid").setAttribute("href", 'css/ltr-media.css');
                alertify.defaults.glossary.ok = 'OK';
            }
        }
    }
        // if (localStorage.direction == 'rtl') {
        //     document.getElementById("styleid").setAttribute("href", 'css/rtl-style.css');
        //     document.getElementById("mediaid").setAttribute("href", 'css/rtl-media.css');
        //     $(".ar_direction").addClass("ar_direction1");
        // } else {
        //     document.getElementById("styleid").setAttribute("href", 'css/ltr-style.css');
        //     document.getElementById("mediaid").setAttribute("href", 'css/ltr-media.css');
        //     $(".ar_direction").removeClass("ar_direction1");
        // }
        this.getpagecontent();

    }
})

var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});
function searchArray(nameKey, myArray, tagName) {
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i][tagName] === nameKey) {
            return myArray[i];
        }
    }
}

