/* Booking form JS
============================================================== */
$("ul.nav-tabs a").click(function (e) {
  e.preventDefault();  
    $(this).tab('show');
});
/*----------------------------------------------------*/
// Select2
/*----------------------------------------------------*/

// jQuery(document).ready(function($){
//     $('.myselect').select2({minimumResultsForSearch: Infinity,'width':'100%'});
//     $('b[role="presentation"]').hide();
//     $('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>');
// });


/***************************************************************
		BEGIN: VARIOUS DATEPICKER & SPINNER INITIALIZATION
***************************************************************/
$(document).ready(function () {
	$( function() {
    var dateFormat = "dd/mm/yy",
      from = $( "#from, #from1, #from2, #from3, #from4, #from5, #from6,#deptDate01" )
        .datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to, #to1, #to2, #to3, #to4, #to5, #to6" ).datepicker({
        minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
});

/* Scroll To Top JS
============================================================== */
jQuery(window).scroll(function () {
	scrollToTop('show');
});

jQuery(document).ready(function () {
	scrollToTop('click');
});

/* Animated Function */
function scrollToTop(i) {
	if (i == 'show') {
		if (jQuery(this).scrollTop() != 0) {
			jQuery('#toTop').fadeIn();
		} else {
			jQuery('#toTop').fadeOut();
		}
	}
	if (i == 'click') {
		jQuery('#toTop').click(function () {
			jQuery('body,html').animate({
				scrollTop: 0
			}, 500);
			return false;
		});
	}
}



/* Deals Slider JS
============================================================== */
    $(document).ready(function() {

      $("#owl-demo-").owlCarousel({
        items: 3,
        lazyLoad: true,
        loop: true,
        margin: 30,
		autoPlay: true,
        navigation : true,
         itemsDesktop : [991, 2],
        itemsDesktopSmall : [979, 2],
        itemsTablet : [768, 2],
		itemsMobile : [640, 1],  
      });
          $( ".owl-prev").html('<i class="fa fa-chevron-left"></i>');
         $( ".owl-next").html('<i class="fa fa-chevron-right"></i>');

    });

/* Package Slider JS
============================================================== */
    $(document).ready(function() {

      $("#owl-demo-").owlCarousel({
        items: 2,
        lazyLoad: true,
        loop: true,
        margin: 30,
		autoPlay: true,
        navigation : true,
         itemsDesktop : [1024, 1],
        itemsDesktopSmall : [979, 2],
        itemsTablet : [768, 2],
		itemsMobile : [640, 1],  
      });
          $( ".owl-prev").html('<i class="fa fa-chevron-left"></i>');
         $( ".owl-next").html('<i class="fa fa-chevron-right"></i>');
    });
		

/*---select-box-js---*/
    
//     jQuery(document).ready(function($){
//     $('.myselect').select2({minimumResultsForSearch: Infinity,'width':'100%'});
//     $('b[role="presentation"]').hide();
//     $('.select2-selection__arrow').append('<i class="fa  fa-chevron-down"></i>');
//     $("#Adultmyselect").append("<option value='0' selected>adult</option>");
// });
	    jQuery(document).ready(function($){
    $('.myselect-2').select2({minimumResultsForSearch: Infinity,'width':'100%'});
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('');
});


