var HeaderComponent = Vue.component('headeritem', {
    template: ` <div id="header">
    <nav id="navbar-main" class="navbar navbar-default navbar-fixed-top">
      <div class="container">
      <div class="row"   v-if="getdata"></div>
        <div class="row" v-else v-for="item in pluck('main',content.area_List)">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 flxBox ar_direction">
            <div class="navbar-header" >
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span
                  class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a class="navbar-brand" href="/"><img :src="pluckcom('Logo',item.component)" alt=""></a> </div>
            
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
              <ul class="nav navbar-nav"  role="tablist">

                <li :class="{ active : active_el == 1 }"><a href="/index.html">{{'Home'}}</a></li>
                <li :class="{ active : active_el == 2 }"><a href="aboutus.html">{{'About Us'}}</a></li>
                <li :class="{ active : active_el == 3 }"><a href="services.html">{{'Services'}}</a></li>
                
                <li><a id="modal_retrieve1" href="#modal" v-if="userlogined==false" class="btn-clta" :title="pluckcom('Retrieve_booking_label',item.component)">{{pluckcom('Retrieve_booking_label',item.component)}}</a></li>
              </ul>
            </div>
            <div class="call-to-action">
            <!--iflogin-->
            <label id="bind-login-info" for="profile2" class="profile-dropdown" v-show="userlogined">
                  <input type="checkbox" id="profile2">
                  <img v-if="[2,3,4].indexOf(userinfo.title?userinfo.title.id:1)<0" src="/assets/images/user.png">
                  <img v-else src="/assets/images/user_lady.png">
                  <span>{{userinfo.firstName+' '+userinfo.lastName }}</span>
                  <label for="profile2"><i class="fa fa-angle-down" aria-hidden="true"></i></label>
                  <ul v-for="prfitem in pluck('Dropdown_menu',content.area_List)" >
                    <li><a href="/customer-profile.html"><i class="fa fa-th-large" aria-hidden="true"></i> {{pluckcom('Dashboard_label',prfitem.component)}}</a></li>
                    <li><a href="/my-profile.html"><i class="fa fa-user" aria-hidden="true"></i>{{pluckcom('Profile_label',prfitem.component)}}</a></li>
                    <li><a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i> {{pluckcom('Bookings_label',prfitem.component)}}</a></li>
                    <li><a href="#" v-on:click.prevent="logout"><i class="fa fa-power-off" aria-hidden="true"></i> {{pluckcom('Logout_label',prfitem.component)}}</a></li>
                  </ul>
                </label>
                <!--ifnotlogin-->
              <div id="sign-bt-area" class="cd-main-nav js-main-nav" v-show="userlogined==false" >
                <ul class="cd-main-nav__list js-signin-modal-trigger">
                  <!-- inser more links here -->
                  <li><a class="cd-main-nav__item cd-main-nav__item--signin" href="javascript:void(0);" data-signin="login">{{pluckcom('Login_button_label',item.component)}}</a>
                  </li>
                </ul>
              </div>
              <div class="cd-signin-modal js-signin-modal">
                <!-- this is the entire modal form, including the background -->
                <div class="cd-signin-modal__container" v-for="signitem in pluck('Sign_in_and_Register',content.area_List)" >
                  <!-- this is the container wrapper -->
                  <ul class="cd-signin-modal__switcher js-signin-modal-switcher js-signin-modal-trigger">
                    <li><a href="#0" data-signin="login" data-type="login">{{pluckcom('Sign_in_tab_name',signitem.component)}}</a></li>
                    <li><a href="#0" data-signin="signup" data-type="signup">{{pluckcom('Register_tab_name',signitem.component)}}</a></li>
                  </ul>
                  <div class="cd-signin-modal__block js-signin-modal-block" data-type="login">
                    <!-- log in form -->
                     <div class="cd-signin-modal__form">
                      <div class="connect"> <a href="#"  v-for="imgite in pluckcom('Social_Network_Logo_List',signitem.component) "><img :src="imgite.Logo" alt=""></a>  </div>
                      <p class="cd-signin-modal__fieldset">
                        <label
                          class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace"
                          for="signin-email">{{pluckcom('Email_placeholder',signitem.component)}}</label>
                        <input v-bind:class="{ 'cd-signin-modal__input--has-error': usererrormsg }"
                          class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
                          id="signin-email" v-model="username" type="email" :placeholder="pluckcom('Email_placeholder',signitem.component)">
                        <span v-bind:class="{ 'cd-signin-modal__error--is-visible': usererrormsg }"  class="cd-signin-modal__error">Username seems incorrect!</span> </p>
                      <p class="cd-signin-modal__fieldset">
                        <label
                          class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace"
                          for="signin-password">{{pluckcom('password_placeholder',signitem.component)}}}</label>
                        <input v-bind:class="{ 'cd-signin-modal__input--has-error': psserrormsg }" 
                          class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
                          id="signin-password" type="password" v-model="password" :placeholder="pluckcom('password_placeholder',signitem.component)">
                          
                        <a href="javascript:void(0);" class="cd-signin-modal__hide-password js-hide-password">Show</a>
                         <span v-bind:class="{ 'cd-signin-modal__error--is-visible': psserrormsg }"
                          class="cd-signin-modal__error">Please enter the password!!</span> </p>
                      <p class="cd-signin-modal__fieldset">
                        <button class="cd-signin-modal__input cd-signin-modal__input--full-width" type="submit"
                          v-on:click="loginaction">{{pluckcom('Sign_in_button_name',signitem.component)}}</button>
                      </p>
                      <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0"
                          data-signin="reset">{{pluckcom('Forgot_password_link_name',signitem.component)}}</a></p>
                    </div>
                  </div>
                  <!-- cd-signin-modal__block -->

                  <div class="cd-signin-modal__block js-signin-modal-block" data-type="signup">
                    <!-- sign up form -->
                    <div class="cd-signin-modal__form">
                      <div class="connect"> <a href="#" v-for="imgite in pluckcom('Social_Network_Logo_List',signitem.component) "><img :src="imgite.Logo" alt=""></a> </div>
                      <p class="cd-signin-modal__fieldset">
                        <label
                          class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace"
                          for="signup-firstname">{{pluckcom('First_name_placeholder',signitem.component)}}</label>
                        <input v-model="registerUserData.firstName" v-bind:class="{ 'cd-signin-modal__input--has-error': userFirstNameErormsg }"
                          class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
                          id="signup-firstname" type="text" :placeholder="pluckcom('First_name_placeholder',signitem.component)">
                          <span v-bind:class="{ 'cd-signin-modal__error--is-visible': userFirstNameErormsg }"  class="cd-signin-modal__error">First Name seems incorrect!</span> </p> </p>
                      <p class="cd-signin-modal__fieldset">
                        <label
                          class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace"
                          for="signup-lastname">{{pluckcom('Last_name_placeholder',signitem.component)}}</label>
                        <input v-model="registerUserData.lastName" v-bind:class="{ 'cd-signin-modal__input--has-error': userLastNameErrormsg }"
                          class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
                          id="signup-lastname" type="text" :placeholder="pluckcom('Last_name_placeholder',signitem.component)">
                          <span v-bind:class="{ 'cd-signin-modal__error--is-visible': userLastNameErrormsg }"  class="cd-signin-modal__error">Last Name seems incorrect!</span> </p>
                      <p class="cd-signin-modal__fieldset">
                        <label
                          class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace"
                          for="signup-email">{{pluckcom('Email_placeholder',signitem.component)}}</label>
                        <input v-model="registerUserData.emailId" v-bind:class="{ 'cd-signin-modal__input--has-error': userEmailErormsg }"
                          class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
                          id="signup-email" type="email" :placeholder="pluckcom('Email_placeholder',signitem.component)">
                          <span v-bind:class="{ 'cd-signin-modal__error--is-visible': userEmailErormsg }"  class="cd-signin-modal__error">Email seems incorrect!</span> </p>
                      <p class="cd-signin-modal__fieldset" style="display: none;">
                        <label
                          class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace"
                          for="signup-password">Password</label>
                        <input v-model="registerUserData.password" v-bind:class="{ 'cd-signin-modal__input--has-error': userPasswordErormsg }"
                          class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
                          id="signup-password" type="text" placeholder="Password">
                        <a href="#0" class="cd-signin-modal__hide-password js-hide-password">Hide</a> 
                        <span v-bind:class="{ 'cd-signin-modal__error--is-visible': userPasswordErormsg }"  class="cd-signin-modal__error">Password Required!</span> </p>
                      <p class="cd-signin-modal__fieldset" style="display: none;">
                        <label
                          class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace"
                          for="signup-vpassword">Verify Password</label>
                        <input v-model="registerUserData.verifyPassword" v-bind:class="{ 'cd-signin-modal__input--has-error': userVerPasswordErormsg,'cd-signin-modal__input--has-error': userPwdMisMatcherrormsg }"
                        
                          class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
                          id="signup-vpassword" type="text" placeholder="Verify password">
                        <a href="#0" class="cd-signin-modal__hide-password js-hide-password">Hide</a> 
                        <span v-bind:class="{ 'cd-signin-modal__error--is-visible': userVerPasswordErormsg }"  class="cd-signin-modal__error">Verify Password Required!</span> </p>
                        <span v-bind:class="{ 'cd-signin-modal__error--is-visible': userPwdMisMatcherrormsg }"  class="cd-signin-modal__error">Password Mismach!</span> </p>
                      <p class="cd-signin-modal__fieldset">
                        <input type="checkbox"  v-model="registerUserData.terms" id="accept-terms" 
                        v-bind:class="{ 'cd-signin-modal__input--has-error': userTerms,'cd-signin-modal__input--has-error': userTerms }" class="cd-signin-modal__input ">
                        <label for="accept-terms">{{pluckcom('Register_message',signitem.component)}}</label>
                          <span v-bind:class="{ 'cd-signin-modal__error--is-visible': userTerms}"  class="cd-signin-modal__error">Accept Terms!</span>
                      </p>
                      <p class="cd-signin-modal__fieldset">
                        <input
                          class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding"
                          type="submit" v-on:click="registerUser" :value="pluckcom('Register_button_name',signitem.component)">
                      </p>
                    </div>
                  </div>
                  <!-- cd-signin-modal__block -->

                  <div class="cd-signin-modal__block js-signin-modal-block" data-type="reset">
                    <!-- reset password form -->
                    <p class="cd-signin-modal__message">{{pluckcom('Forgot_password_message',signitem.component)}}</p>
                    <div class="cd-signin-modal__form" >
                      <p class="cd-signin-modal__fieldset">
                        <label
                          class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace"
                          for="reset-email">{{pluckcom('Email_placeholder',signitem.component)}}</label>
                        <input v-model="emailId"  v-bind:class="{ 'cd-signin-modal__input--has-error': userforgotErrormsg }"
                          class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
                          id="reset-email" type="email" :placeholder="pluckcom('Email_placeholder',signitem.component)">
                          <span v-bind:class="{ 'cd-signin-modal__error--is-visible': userforgotErrormsg}"  class="cd-signin-modal__error">Email seems incorrect!</span>
                      <p class="cd-signin-modal__fieldset">
                        <input
                          class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding"
                          type="submit" v-on:click="forgotPassword" :value="pluckcom('Reset_password_button_name',signitem.component)">
                      </p>
                    </div>
                    <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0"
                        data-signin="login">{{pluckcom('Back_to_login_name',signitem.component)}}</a></p>
                  </div>
                  <!-- cd-signin-modal__block -->
                  <a href="#0" class="cd-signin-modal__close js-close">Close</a>
                </div>
                <!-- cd-signin-modal__container -->
              </div>
              <a id="modal_retrieve" href="#modal" v-if="userlogined==false" class="btn-clta" :title="pluckcom('Retrieve_booking_label',item.component)">{{pluckcom('Retrieve_booking_label',item.component)}}</a>
              <div id="modal" class="popupContainer" style="display:none;" v-for="retrvitem in pluck('Retrieve_the_Bookings',content.area_List)" >
                <header class="popupHeader"> <span class="header_title">{{pluckcom('Retrieve_booking_label',item.component)}}</span> <span
                    class="modal_close"><i class="fa fa-times"></i></span> </header>
                <section class="popupBody">
                  <!-- User Login -->
                  <div class="user_login">
                    <div>
                    <p>
                   
                      <label>{{pluckcom('Email_label',retrvitem.component)}}</label>
                      <div class="validation_sec">
                      <input v-model="retrieveEmailId" type="text" id="txtretrivebooking" v-bind:class="{ 'cd-signin-modal__input--has-error': retrieveEmailErormsg }" name="text" :placeholder="pluckcom('Email_placeholder',retrvitem.component)" />
                      <span v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveEmailErormsg }"  class="cd-signin-modal__error sp_validation">Email Required!</span></div>
                      <small>{{pluckcom('Email_text',retrvitem.component)}}</small>
                       </p>
                      <p>
                      
                      <label>{{pluckcom('ID_number_Label',retrvitem.component)}}</label>
                      <div class="validation_sec">
                      <input v-model="retrieveBookRefid" v-bind:class="{ 'cd-signin-modal__input--has-error': retrieveBkngRefErormsg }" type="text" id="text" name="text" :placeholder="pluckcom('ID_number_placeholder',retrvitem.component)" />
                      <span v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveBkngRefErormsg }"  class="cd-signin-modal__error sp_validation">Booking Reference Id Required!</span> </div>
                      <small>{{pluckcom('Id_number_text',retrvitem.component)}}</small>
                        </p>
                      <button type="submit" v-on:click="retrieveBooking" id="retrieve-booking" class="btn-blue">{{pluckcom('Submit_button_name',retrvitem.component)}}</button>
                    </div>
                  </div>
                </section>
              </div>
              
              <div class="currency lag">
              <currency-select></currency-select>              
                
              </div>
            <!--  

              <div class="lang lag">
                    <lang-select @languagechange="getpagecontent"></lang-select>
              </div>
            -->
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>`,
    data() {
        return {
            username: '',
            password: '',
            emailId: '',
            retrieveEmailId: '',
            retrieveBookRefid: '',
            usererrormsg: false,
            psserrormsg: false,
            userFirstNameErormsg: false,
            userLastNameErrormsg: false,
            userEmailErormsg: false,
            userPasswordErormsg: false,
            userVerPasswordErormsg: false,
            userPwdMisMatcherrormsg: false,
            userPwdMisMatcherrormsg: false,
            userTerms: false,
            userforgotErrormsg: false,
            retrieveBkngRefErormsg: false,
            retrieveEmailErormsg: false,
            userlogined: this.checklogin(),
            userinfo: [],
            registerUserData: {
                firstName: '',
                lastName: '',
                emailId: '',
                password: '',
                verifyPassword: '',
                terms: '',
                title: 'Mr'
            },
            Languages: [],
            language: 'en',
            content: null,
            getdata: true,
            active_el: "",
            isLoading: false,
            fullPage: true,
            PageName: '',
            DeviceInfo: '',
            sessionids: '',
            Ipaddress: '',
            pageType: '',

        }
    },
    methods: {
        loginaction: function () {

            if (!this.username) {
                this.usererrormsg = true;
                return false;
            } else if (!this.validEmail(this.username)) {
                this.usererrormsg = true;
                return false;
            } else {
                this.usererrormsg = false;
            }
            if (!this.password) {
                this.psserrormsg = true;
                return false;

            } else {
                this.psserrormsg = false;
                var self = this;
                login(this.username, this.password, function (response) {
                    if (response == false) {
                        self.userlogined = false;
                        alert("Invalid username or password.");
                    } else {
                        self.userlogined = true;
                        self.userinfo = JSON.parse(localStorage.User);
                        $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                        try {
                            self.$eventHub.$emit('logged-in', { userName: self.username, password: self.password });
                            signArea.headerLogin({ userName: self.username, password: self.password })
                        } catch (error) {

                        }
                    }
                });

            }



        },
        validEmail: function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        checklogin: function () {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);
                return true;
            } else {
                this.userlogined = false;
                return false;
            }
        },
        logout: function () {
            this.userlogined = false;
            this.userinfo = [];
            localStorage.profileUpdated = false;
            try {
                this.$eventHub.$emit('logged-out');
                signArea.logout()
            } catch (error) {

            }
            commonlogin();
        },
        registerUser: function () {
            var vm = this;
            if (this.registerUserData.firstName == "") {
                this.userFirstNameErormsg = true;
                return false;
            } else {
                this.userFirstNameErormsg = false;
            }
            if (this.registerUserData.lastName == "") {
                this.userLastNameErrormsg = true;
                return false;
            } else {
                this.userLastNameErrormsg = false;
            }
            if (this.registerUserData.emailId == "") {
                this.userEmailErormsg = true;
                return false;
            } else if (!this.validEmail(this.registerUserData.emailId)) {
                this.userEmailErormsg = true;
                return false;
            } else {
                this.userEmailErormsg = false;
            }
            registerUser(this.registerUserData.emailId, this.registerUserData.firstName, this.registerUserData.lastName, this.registerUserData.title, function (response) {
                if (response.isSuccess == true) {
                    vm.username = response.data.data.user.loginId;
                    vm.password = response.data.data.user.password;
                    login(vm.username, vm.password, function (response) {
                        if (response != false) {
                            $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                            window.location.href = "/edit-my-profile.html?edit-profile=true";
                        }
                    });
                }

            });
        },

        forgotPassword: function () {
            var self = this;
            getAgencycode(function (response) {
                var agencyCodeResponse = response;

                if (this.emailId == "") {
                    this.userforgotErrormsg = true;
                    return false;
                } else {
                    this.userforgotErrormsg = false;
                }

                var datas = {
                    emailId: self.emailId,
                    agencyCode: agencyCodeResponse,
                    logoUrl: window.location.href + "website-informations/logo/logo.png",
                    websiteUrl: window.location.origin,
                    resetUri: window.location.origin + "/" + window.location.pathname.split("/")[1] + "/reset-password.html"

                };

                $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:none;background:grey !important;");

                var huburl = ServiceUrls.hubConnection.baseUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var requrl = ServiceUrls.hubConnection.hubServices.forgotPasswordUrl;
                axios.post(huburl + portno + requrl, datas)
                    .then((response) => {
                        if (response.data != "") {
                            alert(response.data);
                        } else {
                            alert("Error in forgot password. Please contact admin.");
                        }
                        $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:#3a7fc1 !important");

                    })
                    .catch((err) => {
                        console.log("FORGOT PASSWORD  ERROR: ", err);
                        if (err.response.data.message == 'No User is registered with this emailId.') {
                            alert(err.response.data.message);
                        } else {
                            alert('We have found some technical difficulties. Please contact admin!');
                        }
                        $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:#3a7fc1 !important");

                    });
            });
        },

        retrieveBooking: function () {
            if (this.retrieveEmailId == "") {
                //alert('Email required !');
                this.retrieveEmailErormsg = true;
                return false;
            } else if (!this.validEmail(this.retrieveEmailId)) {
                //alert('Invalid Email !');
                this.retrieveEmailErormsg = true;
                return false;
            } else {
                this.retrieveEmailErormsg = false;
            }
            if (this.retrieveBookRefid == "") {
                this.retrieveBkngRefErormsg = true;
                return false;
            } else {
                this.retrieveBkngRefErormsg = false;
            }
            var bookData = {
                BkngRefID: this.retrieveBookRefid,
                emailId: this.retrieveEmailId,
                redirectFrom: 'retrievebooking',
                isMailsend: false
            };
            localStorage.bookData = JSON.stringify(bookData);
            window.location.href = '/Flights/flight-confirmation.html';
        },

        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        activeNav: function () {
            var pathname = window.location.pathname;
            if (pathname.includes("aboutus.html")) {
                this.active_el = 2;
            }
            else if (pathname.includes("services.html")) {
                this.active_el = 3;
            }
            else if (pathname.includes("holidaydetails.html")) {
                this.active_el = null;
            }
            else if (pathname.includes("FAQ.html")) {
                this.active_el = null;
            }
            else if (pathname.includes("privacypolicy.html")) {
                this.active_el = null;
            }
            else if (pathname.includes("termsofuse.html")) {
                this.active_el = null;
            }
            else if (pathname.includes("fileaclaim.html")) {
                this.active_el = null;
            }
            else {
                this.active_el = 1;
            }



        },
        getpagecontent: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Master page/Master page/Master page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    window.localStorage.setItem("cmsContent", JSON.stringify(response.data));

                    Vue.nextTick(function () {
                        (

                            function () {
                                //Login/Signup modal window - by CodyHouse.co
                                function ModalSignin(element) {
                                    this.element = element;
                                    this.blocks = this.element.getElementsByClassName('js-signin-modal-block');
                                    this.switchers = this.element.getElementsByClassName('js-signin-modal-switcher')[0].getElementsByTagName('a');
                                    this.triggers = document.getElementsByClassName('js-signin-modal-trigger');
                                    this.hidePassword = this.element.getElementsByClassName('js-hide-password');
                                    this.init();
                                };

                                ModalSignin.prototype.init = function () {
                                    var self1 = this;
                                    //open modal/switch form
                                    for (var i = 0; i < this.triggers.length; i++) {
                                        (function (i) {
                                            self1.triggers[i].addEventListener('click', function (event) {
                                                if (event.target.hasAttribute('data-signin')) {
                                                    event.preventDefault();
                                                    self1.showSigninForm(event.target.getAttribute('data-signin'));
                                                }
                                            });
                                        })(i);
                                    }

                                    //close modal
                                    this.element.addEventListener('click', function (event) {
                                        if (hasClass(event.target, 'js-signin-modal') || hasClass(event.target, 'js-close')) {
                                            event.preventDefault();
                                            removeClass(self1.element, 'cd-signin-modal--is-visible');
                                        }
                                    });
                                    //close modal when clicking the esc keyboard button
                                    document.addEventListener('keydown', function (event) {
                                        (event.which == '27') && removeClass(self1.element, 'cd-signin-modal--is-visible');
                                    });

                                    //hide/show password
                                    for (var i = 0; i < this.hidePassword.length; i++) {
                                        (function (i) {
                                            self1.hidePassword[i].addEventListener('click', function (event) {
                                                self1.togglePassword(self1.hidePassword[i]);
                                            });
                                        })(i);
                                    }

                                    //IMPORTANT - REMOVE THIS - it's just to show/hide error messages in the demo

                                };

                                ModalSignin.prototype.togglePassword = function (target) {
                                    var password = target.previousElementSibling;
                                    ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text') : password.setAttribute('type', 'password');
                                    target.textContent = ('Hide' == target.textContent) ? 'Show' : 'Hide';
                                    putCursorAtEnd(password);
                                }

                                ModalSignin.prototype.showSigninForm = function (type) {
                                    // show modal if not visible
                                    !hasClass(this.element, 'cd-signin-modal--is-visible') && addClass(this.element, 'cd-signin-modal--is-visible');
                                    // show selected form
                                    for (var i = 0; i < this.blocks.length; i++) {
                                        this.blocks[i].getAttribute('data-type') == type ? addClass(this.blocks[i], 'cd-signin-modal__block--is-selected') : removeClass(this.blocks[i], 'cd-signin-modal__block--is-selected');
                                    }
                                    //update switcher appearance
                                    var switcherType = (type == 'signup') ? 'signup' : 'login';
                                    for (var i = 0; i < this.switchers.length; i++) {
                                        this.switchers[i].getAttribute('data-type') == switcherType ? addClass(this.switchers[i], 'cd-selected') : removeClass(this.switchers[i], 'cd-selected');
                                    }
                                };

                                ModalSignin.prototype.toggleError = function (input, bool) {
                                    // used to show error messages in the form
                                    toggleClass(input, 'cd-signin-modal__input--has-error', bool);
                                    toggleClass(input.nextElementSibling, 'cd-signin-modal__error--is-visible', bool);
                                }

                                var signinModal = document.getElementsByClassName("js-signin-modal")[0];
                                if (signinModal) {
                                    new ModalSignin(signinModal);
                                }

                                // toggle main navigation on mobile
                                var mainNav = document.getElementsByClassName('js-main-nav')[0];
                                if (mainNav) {
                                    mainNav.addEventListener('click', function (event) {
                                        if (hasClass(event.target, 'js-main-nav')) {
                                            var navList = mainNav.getElementsByTagName('ul')[0];
                                            toggleClass(navList, 'cd-main-nav__list--is-visible', !hasClass(navList, 'cd-main-nav__list--is-visible'));
                                        }
                                    });
                                }

                                //class manipulations - needed if classList is not supported
                                function hasClass(el, className) {
                                    if (el.classList) return el.classList.contains(className);
                                    else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
                                }

                                function addClass(el, className) {
                                    var classList = className.split(' ');
                                    if (el.classList) el.classList.add(classList[0]);
                                    else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
                                    if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
                                }

                                function removeClass(el, className) {
                                    var classList = className.split(' ');
                                    if (el.classList) el.classList.remove(classList[0]);
                                    else if (hasClass(el, classList[0])) {
                                        var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
                                        el.className = el.className.replace(reg, ' ');
                                    }
                                    if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
                                }

                                function toggleClass(el, className, bool) {
                                    if (bool) addClass(el, className);
                                    else removeClass(el, className);
                                }
                                $("#modal_retrieve").leanModal({
                                    top: 100,
                                    overlay: 0.6,
                                    closeButton: ".modal_close"
                                });
                                $("#modal_retrieve1").leanModal({
                                    top: 100,
                                    overlay: 0.6,
                                    closeButton: ".modal_close"
                                });
                                //credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
                                function putCursorAtEnd(el) {
                                    if (el.setSelectionRange) {
                                        var len = el.value.length * 2;
                                        el.focus();
                                        el.setSelectionRange(len, len);
                                    } else {
                                        el.value = el.value;
                                    }
                                };
                            })();
                    }.bind(self));

                    self.isLoading = false;

                }).catch(function (error) {
                    console.log('Error');
                    this.content = [];
                });
            });
        },
        Getvisit: function () {
            var self = this;
            var currentUrl = window.location.pathname;
            if (currentUrl == "/SGS_Demo/" || "/SGS_Demo/index.html") {
                self.pageType = "Master";
                self.PageName = "Home"
            } else if (currentUrl == "/SGS_Demo/holidaydetails.html") {
                self.pageType = "Package details";
                self.PageName = packageView.packagecontent.Title;
            } else {
                page = currentUrl.substring(0, currentUrl.length - 5);
                var page = page.split('/');
                self.PageName = page[3];
                self.pageType = "Master";
            }
            var TempDeviceInfo = detect.parse(navigator.userAgent);
            self.DeviceInfo = TempDeviceInfo;
            this.showYourIp();
        },
        showYourIp: function () {
            var self = this;
            var ipUrl = ServiceUrls.externalServiceUrl.ipAddressApi;
            fetch(ipUrl)
                .then(x => x.json())
                .then(({ ip }) => {
                    self.Ipaddress = ip;
                    self.sessionids = ip.replace(/\./g, '');
                });
        },
        sendip: async function () {
            var self = this;
            if (self.PageName == null || self.PageName == undefined || self.PageName == "") {
                self.PageName = packageView.packagecontent.Title

            }
            let agencyCode = JSON.parse(localStorage.User).loginNode.code;
            let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
            let insertVisitData = {
                type: "Visit",
                keyword1: self.sessionids,
                ip1: self.Ipaddress,
                keyword2: self.pageType,
                keyword3: self.PageName,
                keyword4: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'AED',
                keyword5: (localStorage.Languagecode) ? localStorage.Languagecode : "en",
                text1: self.DeviceInfo.device.type,
                text2: self.DeviceInfo.os.name,
                text3: self.DeviceInfo.browser.name,
                date1: requestedDate,
                nodeCode: agencyCode
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data", insertVisitData, null);
            try {
                let insertID = Number(responseObject);

            } catch (e) {

            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json'
                },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
    },
    mounted: function () {
        this.activeNav();
        if (localStorage.IsLogin == "true") {
            this.userlogined = true;
            this.userinfo = JSON.parse(localStorage.User);

        } else {
            this.userlogined = false;

        }
        this.getpagecontent();
        this.Getvisit();
        setTimeout(() => {
            this.sendip();
        }, 2000);
    },
    watch: {
        updatelogin: function () {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);

            } else {
                this.userlogined = false;

            }

        }

    }

});

var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});

var FooterComponent = Vue.component('footeritem', {
    template: `<div class="container">
  <div class="row"   v-if="getdata"></div>
  <div class="row" v-else v-for="item in pluck('Footer',content.area_List)">

  <div class="fixed-bottom whatsappIconPosition" v-if="pluckcom('Is_Whatsapp_Enabled',item.component)">
    <a :href="pluckcom('Whatsapp_Link',item.component) + pluckcom('Whatsapp_Number_With_Country_Code',item.component)" target="_blank" class="whatsapp">
        <img :src="pluckcom('Whatsapp_Icon_70x70px',item.component)" class="whatsappIconSize"></a>
    </div>

    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
      <div class="link">
        <ul>
          <li v-for="Links in pluckcom('Links',item.component)"><a href="#" v-on:click="getmoreinfo(Links.URL)">{{Links.Page_Name}}</a></li>
          
        </ul>
      </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 card-padding"> <img v-for="img in pluckcom('Payment_types',item.component)" :src="img.Image" alt=""> </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 link">
      <ul>
        <li v-for="media in pluckcom('Social_Media_List',item.component)" style="cursor:pointer;">
        <a :href="media.Url" target="_blank"><i class="fa" v-bind:class="media.Icon" style="font-size: 1.7em;"></i></a>
        </li>
      </ul>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
      <div class="c-text"> {{pluckcom('Copy_right_Info',item.component)}} </div>
    </div>
  </div>
</div>`,
    data() {
        return {
            content: null,
            getdata: true,
            isLoading: false,
            fullPage: true,
        }

    },
    mounted: function () {
        var self = this;
        self.isLoading = true;
        getAgencycode(function (response) {
            var Agencycode = response;
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";

            var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Master page/Master page/Master page.ftl';
            var cmsurl = huburl + portno + homecms;
            axios.get(cmsurl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
                self.content = response.data;
                self.getdata = false;
                self.isLoading = false;
            }).catch(function (error) {
                console.log('Error');
                self.content = [];
            });
        });


    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = "/SGS_Demo/" + url;

                    window.location.href = url;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            console.log(this.url);
            return url;

        },

    }

});

var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',


});

function searchArray(nameKey, myArray, tagName) {
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i][tagName] === nameKey) {
            return myArray[i];
        }
    }
}