
const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var AboutUs = new Vue({
  i18n,
  el: '#AboutUs',
  name: 'AboutUs',
  data() {
    return {
      // content: null,
      getdata: true,
      selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
      CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
      pagebanner: {
        Breadcrumb1: '',
        About_Us_Content: '',
        About_Us_Main_Title: ''
      },
      pagecontents: {
        Mission_Title:'',
        Mission_Description:'',
        Vision_Title:'',
                Vision_Description:'',
                Values_Title:'',
                Values_Description:''
      },
      pagecontent:{About_Us_Main_Title:'',About_Us_Content:''},
      isLoading: false,
      fullPage: true,
    }

  },
  mounted: function () {
    this.getpagecontent();
  },
  methods: {
    moment: function () {
      return moment();
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getpagecontent: function () {
      if (localStorage.getItem("AgencyCode") === null) {
        localStorage.AgencyCode = JSON.parse(localStorage.User).loginNode.code;

      }
      var Agencycode = localStorage.AgencyCode;
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
      var self = this;
      self.isLoading = true;
      // banner and labels
      var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/About Us/About Us/About Us.ftl';
      axios.get(pageurl, {
        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
      }).then(function (response) {
        self.content = response.data;
        var pagecontent = self.pluck('Banner_Area', self.content.area_List);
        if (pagecontent.length > 0) {
          self.pagebanner.Breadcrumb1 = self.pluckcom('Breadcrumb1', pagecontent[0].component);
          self.pagebanner.Breadcrumb2 = self.pluckcom('Breadcrumb2', pagecontent[0].component);
          self.pagebanner.Banner_Title = self.pluckcom('Banner_Title', pagecontent[0].component);
          self.pagebanner.Banner_Image = self.pluckcom('Banner_Image', pagecontent[0].component);
        }

      }).catch(function (error) {
        console.log('Error');
        self.content = [];
      });

      axios.get(pageurl, {
        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
      }).then(function (response) {
        self.content = response.data;
        var pagecontent = self.pluck('Main_Area', self.content.area_List);
        if (pagecontent.length > 0) {
          self.pagecontent.About_Us_Main_Title = self.pluckcom('About_Us_Main_Title', pagecontent[0].component);
          self.pagecontent.About_Us_Content = self.pluckcom('About_Us_Content', pagecontent[0].component);
        }
        var pagecontents = self.pluck('Features_Area', self.content.area_List);
        if (pagecontents.length > 0) {
          self.pagecontents.Mission_Title = self.pluckcom('Mission_Title', pagecontents[0].component);
          self.pagecontents.Mission_Description = self.pluckcom('Mission_Description', pagecontents[0].component);
          self.pagecontents.Vision_Title = self.pluckcom('Vision_Title', pagecontents[0].component);
          self.pagecontents.Vision_Description = self.pluckcom('Vision_Description', pagecontents[0].component);
          self.pagecontents.Values_Title = self.pluckcom('Values_Title', pagecontents[0].component);
          self.pagecontents.Values_Description = self.pluckcom('Values_Description', pagecontents[0].component);
        }
        self.isLoading = false;
      }).catch(function (error) {
        console.log('Error');
        self.content = [];
      });
    },


  },
  filters: {
    moment: function (date) {
      return moment(date).format('DD MMM YYYY');
    }
  }

});
function owlcarosl() {

  $("#homepackage").owlCarousel({
    navigation: true, // Show next and prev buttons
    slideSpeed: 2000,
    pagination: false,
    paginationSpeed: 2000,
    singleItem: false,
    dots: true,
    mouseDrag: true,
    items: 4,
    transitionStyle: "goDown",
    itemsCustom: [
      [0, 1],
      [450, 1],
      [600, 2],
      [700, 2],
      [1000, 3],
      [1200, 3]
    ],
  });
}
