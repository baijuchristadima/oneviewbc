﻿var paxTitles = [
    { id: 1, name: 'Mr', paxType: ['ADT'], gender: 'M' },
    { id: 2, name: 'Mrs', paxType: ['ADT'], gender: 'F' },
    { id: 3, name: 'Ms', paxType: ['ADT'], gender: 'F' },
    { id: 4, name: 'Miss', paxType: ['CHD', 'INF'], gender: 'F' },
    { id: 5, name: 'Mstr', paxType: ['CHD', 'INF'], gender: 'M' }
]