function getAirportCodeFromName(qfrom) { var Tfrom = qfrom.substr(qfrom.lastIndexOf("(") + 1); var Farr = Tfrom.split(')'); Tfrom = Farr[0]; return Tfrom; }
function airportFromAirportCode(airportCode) {
    var airportName = _.where(AirlinesTimezone, { I: airportCode.toUpperCase() }); if (airportName == "") { airportName = airportCode; }
    else { airportName = airportName[0].N + '(' + airportCode.toUpperCase() + ')'; }
    return airportName;
}
function airportFromAirportCodeUi(airportCode) {
    if (localStorage.Languagecode == "ar" && localStorage.airportDetailsFromElastic) {
        var airportName = _.where(JSON.parse(localStorage.airportDetailsFromElastic), { I: airportCode.toUpperCase() }); if (airportName == "") { airportName = airportCode; }
        else { airportName = airportName[0].N + '(' + airportCode.toUpperCase() + ')'; }
        return airportName;
    } else {
        var airportName = _.where(AirlinesTimezone, { I: airportCode.toUpperCase() }); if (airportName == "") { airportName = airportCode; }
        else { airportName = airportName[0].N + '(' + airportCode.toUpperCase() + ')'; }
        return airportName;
    }
}
function airportFromAirportCodeSimple(airportCode) {
    var airportName = _.where(AirlinesTimezone, { I: airportCode.toUpperCase() });
    if (airportName == "") {
        airportName = airportCode;
    }
    else {
        airportName = airportName[0].N;
    }
    return airportName;
}
function airportLocationFromAirportCodeUi(airportCode) {
    if (localStorage.Languagecode == "ar" && localStorage.airportDetailsFromElastic) {
        var airportName = _.where(JSON.parse(localStorage.airportDetailsFromElastic), { I: airportCode.toUpperCase() }); if (airportName == "") { airportName = airportCode; }
        else { airportName = airportName[0].C }
        return airportName;
    } else {
        var airportName = _.where(AirlinesTimezone, { I: airportCode.toUpperCase() }); if (airportName == "") { airportName = airportCode; }
        else { airportName = airportName[0].C }
        return airportName;
    }
}
function airportLocationFromAirportCode(airportCode) {
    var airportName = _.where(AirlinesTimezone, { I: airportCode.toUpperCase() }); if (airportName == "") { airportName = airportCode; }
    else { airportName = airportName[0].C }
    return airportName;
}
function airportData(airportCode) {
    var airportName = _.where(AirlinesTimezone, { I: airportCode.toUpperCase() }); if (airportName == "") { airportName = airportCode; }
    return airportName;
}
function airportDataUi(airportCode) {
    if (localStorage.Languagecode == "ar" && localStorage.airportDetailsFromElastic) {
        var airportName = _.where(JSON.parse(localStorage.airportDetailsFromElastic), { I: airportCode.toUpperCase() }); if (airportName == "") { airportName = airportCode; }
        return airportName;
    } else {
        var airportName = _.where(AirlinesTimezone, { I: airportCode.toUpperCase() }); if (airportName == "") { airportName = airportCode; }
        return airportName;
    }
}
function getAirLineName(airlineCode) {
    var airLineName = _.where(AirlinesDatas, { C: airlineCode.toUpperCase() }); if (airLineName == "") { airLineName = airlineCode; }
    else { airLineName = airLineName[0].A; }
    return airLineName;
}
function getAirLineTimeZone(airlinecode) {
    var AirLineTimeZone = _.where(AirlinesTimezone, { I: airlinecode }); if (AirLineTimeZone == "") { AirLineTimeZone = "--h:--m"; }
    else { AirLineTimeZone = AirLineTimeZone[0].TZ; }
    return AirLineTimeZone;
}
function GetFlightdurationtime(FromGo, ToGo, DepTimeGo, ArriTimeGo) {
    var DepartureAirlineTimezone = getAirLineTimeZone(FromGo); var ArrivalAirlineTimezone = getAirLineTimeZone(ToGo); var DurationTimezone = DepartureAirlineTimezone - ArrivalAirlineTimezone; var DurationTimezoneMinutes = parseFloat(DurationTimezone) * 60; DurationTimezoneMilliseconds = parseFloat(DurationTimezoneMinutes) * 60000; var depariduration = moment(ArriTimeGo) - moment(DepTimeGo); var GrandDurationTimezone = depariduration + DurationTimezoneMilliseconds; GrandDurationTimezone = GrandDurationTimezone / 60000; var temphours = 0; var tempminutes = 0; var DTime = ''; if (GrandDurationTimezone > 59) {
        temphours = Math.floor(GrandDurationTimezone / 60); tempminutes = GrandDurationTimezone - (temphours * 60); if (tempminutes == 0) { DTime = temphours + 'h '; }
        else { DTime = temphours + 'h ' + tempminutes + 'm'; }
    }
    else { tempminutes = GrandDurationTimezone; DTime = tempminutes + 'm'; }
    if (DTime.indexOf("N") > -1) { DTime = ""; }
    return DTime;
}
function getGenderFromTitle(input) { var gender = paxTitles.filter(function (title) { return title.name.toUpperCase() == input.toUpperCase() }); if (hasArrayData(gender)) { try { return gender[0].gender.toUpperCase(); } catch (e) { return ""; } } else { return ''; } }
function getCabinName(cabinCode) {
    var cabinClass = 'Economy'; if (cabinCode == "F") { cabinClass = "First Class"; }
    else if (cabinCode == "C") { cabinClass = "Business"; }
    else { try { cabinClass = getCabinClassObject(cabinCode).BasicClass; } catch (err) { } }
    return cabinClass;
}
function getAirCraftName(aireqpcode) {
    var AirCraftName = _.where(AirplanesData, { C: aireqpcode }); if (AirCraftName == "") { AirCraftName = aireqpcode; }
    else { AirCraftName = AirCraftName[0].N; }
    return AirCraftName;
}
function get3letterCountryCode(input) { var countries = countryList.filter(function (country) { return country.twoLetter.toUpperCase() == input.toUpperCase() }); if (hasArrayData(countries)) { try { return countries[0].threeLetter; } catch (e) { return ""; } } else { return ""; } }
function getAircabinclass(cabin) {
    var cabinclass = 'Y'; switch (cabin) { case 'Economy': cabinclass = "Y"; break; case 'Business': cabinclass = "C"; break; case 'First Class': cabinclass = "F"; break; }
    return cabinclass;
}
function calcLayoverTime(arrDate, arrTime, depDate, depTime) { var arrDateTime = moment(arrDate + arrTime, 'DDMMYYHHmm').format('YYYY-MM-DDTHH:mm'); var depDateTime = moment(depDate + depTime, 'DDMMYYHHmm').format('YYYY-MM-DDTHH:mm'); var DifferenceMilli = moment(depDateTime) - moment(arrDateTime); var duration = moment.duration(DifferenceMilli); return duration.hours() + 'h ' + duration.minutes() + 'm' }
function getBaggageUnitString(baggageValue) {
    if (baggageValue == "PC" || baggageValue == "P" || baggageValue == "N") { return { bagunitVal: "Piece", bagunitDesc: "Airline typically permit 23kg baggage weight per piece" } }
    else { return { bagunitVal: "Kg", bagunitDesc: "" } }
}
function isRefundableFlight(refundString) {
    if (refundString == "Yes") { return 'true'; }
    else if (refundString == "TICKETS ARE NON-REFUNDABLE" || refundString == "No") { return 'false'; }
    else if (refundString == "PENALTY APPLIES" || refundString == "PENALTY APPLIES - CHECK RULES" || refundString == "SUBJ TO CANCELLATION/CHANGE PENALTY") { return 'penalty'; }
    else { return 'na'; }
}
function getAirportNameUsingElasticserch(Iatacode, id, callback) { 
    var uppercaseLetter = Iatacode.toUpperCase(); var query = { query: { bool: { should: [{ bool: { should: [{ wildcard: { iata_code: { value: uppercaseLetter, boost: 3.0 } } }] } },], must: [{ "exists": { "field": "iata_code" } }] } } }; 
    axios.post(ServiceUrls.elasticSearch.url, query, {
        headers: {
          "Content-Type": "application/json"
        }
        }).then(function (resp) { var hits = resp.data.hits.hits; callback(hits[0]._source, id); })
        .catch(function (error) {
            console.log(error)
        });
    // var client = new elasticsearch.Client({ host: [{ host: ServiceUrls.elasticSearch.elasticsearchHost, auth: ServiceUrls.elasticSearch.auth, protocol: ServiceUrls.elasticSearch.protocol, port: ServiceUrls.elasticSearch.port, requestTimeout: 60000 }], log: 'trace' }); client.search({ index: 'airport_info', size: 1,timeout: "3000ms", body: query })
    // .then(function (resp) { var hits = resp.hits.hits; callback(hits[0]._source, id); }) 
}
function isNullorUndefined(value) { var status = false; if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) { status = true; } return status; }

function get2letterCountryCode(input) {
    var CN = AirlinesTimezone.filter(function(a) { return a.I.toUpperCase() == input.toUpperCase()})[0].CN
    var countries = countryList.filter(function (country) { return country.name.toUpperCase() == CN.toUpperCase() || country.name.toUpperCase().includes(CN.toUpperCase()) });
    if (countries && countries.length > 0) { try { return countries[0].twoLetter; } catch (e) { return ""; } } else { return ""; }
}