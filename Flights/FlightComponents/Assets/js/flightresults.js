var commonAgencyCode = 'AGY435';
var searchSummary = new Vue({
    el: '#vueSummary',
    name: 'flSummary',
    data: {
        searchData: getSearchData(),
        tripType: getSearchData().request.content.criteria.triptype,
        Class: '',
        Traveller: '',
        Modify_Search: '',
        From_City: '',
        To_City: '',
        Departure_Date: '',
        Return_Date: '',
        Travellers: '',
        Adults: '',
        Children: '',
        Infant: '',
        Economy: '',
        Business: '',
        First_Class: '',
        Search: '',
        Weather: '',
        Budget: '',
        Stops: '',
        Airlines: '',
        Departure_Time: '',
        Arrival_Time: '',
        Sort_by: '',
        Price: '',
        Duration: '',
        Flights_Found: '',
        Adult: '',
        Child: '',
        Departure: '',
        Return: '',
        Flight_Details: '',
        Fare_Rules: '',
        Flight_Itinerary: '',
        Baggage_Information: '',
        Fare_Breakup: '',
        Flight_From: '',
        To: '',
        Layover_at: '',
        Check_In: '',
        Cabin: '',
        Fare: '',
        Taxes_and_Fees: '',
        Total: '',
        Book_Now: '',
        Refundable: '',
        Infants: '',
        Non_Refundable: '',
        Refundable_with_Penalty: '',
        Terminal: '',
        Free: '',
        Before_Departure: '',
        After_Departure: '',
        Change_Search_Query: '',
        Search_Again: '',
        Sorry_no_results: '',
        Via: '',
        From: '',
        Dates: '',
        Oneway: '',
        RoundTrip: '',
        MultiCity: '',
        Stop: '',
        Non_Stop: '',
        toSummary: [{}],
        fromSummary: [{}],
        viaCity: [{}]
    },
    created: function () {
        this.getPagecontent();
    },
    mounted: async function() {
        this.toSummary = await this.airportFromAirportCodeElasticSearch(this.searchData.request.content.criteria.originDestinationInformation[0].originLocation);
        if (this.getTripType()==2) {
            this.fromSummary = await this.airportFromAirportCodeElasticSearch(this.searchData.request.content.criteria.originDestinationInformation[0].destinationLocation);
        } else {
            this.fromSummary = await this.airportFromAirportCodeElasticSearch(this.searchData.request.content.criteria.originDestinationInformation[this.searchData.request.content.criteria.originDestinationInformation.length - 1].destinationLocation);
        }
        if (this.getTripType()==3) {
            this.viaCity = await this.getViaCities(this.searchData.request.content.criteria.originDestinationInformation)

        }
    },
    methods: {
        moment: function (date) {
            return moment(date);
        },
        getCabName: function (cab) {
            return getCabinClassName(cab);
        },
        airportFromAirportCode: function (airport) {
            return airportFromAirportCode(airport);
        },
        getTripType: function () {
            var urldata = getUrlVars().flight;
            if (urldata == undefined) {
                window.location.href = 'index.html';
            }
            var urld = urldata.substring(1, urldata.length).split('/')
            var triptypes = urld[urld.length - 1];
            var tripTyppee = (triptypes.split('-')[7] || "").toLowerCase();
            this.tripType = tripTyppee;
            if (tripTyppee === 'o') {
                return 1
            } else if (tripTyppee === 'r') {
                return 2
            } else {
                return 3
            }
        },
        getViaCities: async function (destinations) {
            var outStr = '';
            destinations.forEach(async function (dest, idx) {
                if (idx !== destinations.length - 1) {
                    if (outStr === '') {
                        outStr += await this.airportFromAirportCodeElasticSearch(dest.destinationLocation);
                    } else {
                        outStr += ', ' + await this.airportFromAirportCodeElasticSearch(dest.destinationLocation);
                    }
                }
            });
            return outStr;
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + commonAgencyCode + '/Template/Flight Result/Flight Result/Flight Result.ftl';
                axios.get(aboutUsPage, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (response.data.area_List.length > 0) {
                        var mainComp = response.data.area_List[0].main;
                        self.Class = self.pluckcom('Class', mainComp.component);
                        self.Traveller = self.pluckcom('Traveller', mainComp.component);
                        self.Modify_Search = self.pluckcom('Modify_Search', mainComp.component);
                        self.From_City = self.pluckcom('From_City', mainComp.component);
                        self.To_City = self.pluckcom('To_City', mainComp.component);
                        self.Departure_Date = self.pluckcom('Departure_Date', mainComp.component);
                        self.Return_Date = self.pluckcom('Return_Date', mainComp.component);
                        self.Travellers = self.pluckcom('Travellers', mainComp.component);
                        self.Adults = self.pluckcom('Adults', mainComp.component);
                        self.Children = self.pluckcom('Children', mainComp.component);
                        self.Infant = self.pluckcom('Infant', mainComp.component);
                        self.Economy = self.pluckcom('Economy', mainComp.component);
                        self.Business = self.pluckcom('Business', mainComp.component);
                        self.First_Class = self.pluckcom('First_Class', mainComp.component);
                        self.Search = self.pluckcom('Search', mainComp.component);
                        self.Weather = self.pluckcom('Weather', mainComp.component);
                        self.Budget = self.pluckcom('Budget', mainComp.component);
                        self.Stops = self.pluckcom('Stops', mainComp.component);
                        self.Airlines = self.pluckcom('Airlines', mainComp.component);
                        self.Departure_Time = self.pluckcom('Departure_Time', mainComp.component);
                        self.Arrival_Time = self.pluckcom('Arrival_Time', mainComp.component);
                        self.Sort_by = self.pluckcom('Sort_by', mainComp.component);
                        self.Price = self.pluckcom('Price', mainComp.component);
                        self.Duration = self.pluckcom('Duration', mainComp.component);
                        self.Flights_Found = self.pluckcom('Flights_Found', mainComp.component);
                        self.Adult = self.pluckcom('Adult', mainComp.component);
                        self.Child = self.pluckcom('Child', mainComp.component);
                        self.Departure = self.pluckcom('Departure', mainComp.component);
                        self.Return = self.pluckcom('Return', mainComp.component);
                        self.Flight_Details = self.pluckcom('Flight_Details', mainComp.component);
                        self.Fare_Rules = self.pluckcom('Fare_Rules', mainComp.component);
                        self.Flight_Itinerary = self.pluckcom('Flight_Itinerary', mainComp.component);
                        self.Baggage_Information = self.pluckcom('Baggage_Information', mainComp.component);
                        self.Fare_Breakup = self.pluckcom('Fare_Breakup', mainComp.component);
                        self.Flight_From = self.pluckcom('Flight_From', mainComp.component);
                        self.To = self.pluckcom('To', mainComp.component);
                        self.Layover_at = self.pluckcom('Layover_at', mainComp.component);
                        self.Check_In = self.pluckcom('Check_In', mainComp.component);
                        self.Cabin = self.pluckcom('Cabin', mainComp.component);
                        self.Fare = self.pluckcom('Fare', mainComp.component);
                        self.Taxes_and_Fees = self.pluckcom('Taxes_and_Fees', mainComp.component);
                        self.Total = self.pluckcom('Total', mainComp.component);
                        self.Book_Now = self.pluckcom('Book_Now', mainComp.component);
                        self.Refundable = self.pluckcom('Refundable', mainComp.component);
                        self.Infants = self.pluckcom('Infants', mainComp.component);
                        self.Non_Refundable = self.pluckcom('Non_Refundable', mainComp.component);
                        self.Refundable_with_Penalty = self.pluckcom('Refundable_with_Penalty', mainComp.component);
                        self.Terminal = self.pluckcom('Terminal', mainComp.component);
                        self.Free = self.pluckcom('Free', mainComp.component);
                        self.Before_Departure = self.pluckcom('Before_Departure', mainComp.component);
                        self.After_Departure = self.pluckcom('After_Departure', mainComp.component);
                        self.Change_Search_Query = self.pluckcom('Change_Search_Query', mainComp.component);
                        self.Search_Again = self.pluckcom('Search_Again', mainComp.component);
                        self.Sorry_no_results = self.pluckcom('Sorry_no_results', mainComp.component);
                        self.Via = self.pluckcom('Via', mainComp.component);
                        self.From = self.pluckcom('From', mainComp.component);
                        self.Dates = self.pluckcom('Dates', mainComp.component);
                        self.Oneway = self.pluckcom('Oneway', mainComp.component);
                        self.RoundTrip = self.pluckcom('RoundTrip', mainComp.component);
                        self.MultiCity = self.pluckcom('MultiCity', mainComp.component);
                        self.Stop = self.pluckcom('Stop', mainComp.component);
                        self.Non_Stop = self.pluckcom('Non_Stop', mainComp.component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        getmoreinfo(url) {

            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url = "/Nirvana/book-now.html?page=" + url;
            } else {
                url = "#";
            }
            return url;
        },
        airportFromAirportCodeElasticSearch: async function(code) {
            return await getAirportUsingElasticSearch(code);
        }
    },
    filters: {
        momDToFull: function (date) {
            return moment(date, 'DD-MM-YYYY').format('ddd, DD MMMM YYYY');
        }
    }
});

const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})

var searchResults = new Vue({
    i18n,
    el: '#vueResults',
    name: 'flResult',
    data: {
        allFlightRes: [],
        filteredFlightRes: [],
        flightResBind: [],
        currentScrollLimit: 15,
        tripType: getSearchData().request.content.criteria.tripType,
        // cdnDomain: cdnDomain,
        weatherImg: 'https://openweathermap.org/img/w/',
        weatherCity: airportData(getSearchData().request.content.criteria.originDestinationInformation[0].destinationLocation)[0].C,
        weatherCityUi: airportDataUi(getSearchData().request.content.criteria.originDestinationInformation[0].destinationLocation)[0].C,
        timeOfsetCity: airportData(getSearchData().request.content.criteria.originDestinationInformation[0].destinationLocation)[0].TZ,
        arrCityTime: '',
        totalResCount: 0,
        resStops: [],
        checkStops: [],
        resAirlines: [],
        checkAirlines: [],
        checkAirlinesMatrix: '',
        currency: '',
        lowestPrice: 0,
        highestPrice: 1,
        resDepLoc: [],
        selDeptLoc: [],
        resArrLoc: [],
        selArrtLoc: [],
        showDepLocReset: false,
        showArrLocReset: false,
        showStopReset: false,
        showAirReset: false,
        weatherData: null,
        sortValues: [],
        hideAllLoaders: false,
        hasError: false,
        errMsg: '',
        selectedCurrency: null,
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        priceSortOrder: true,
        durationSortOrder: true,
        deptSortOrder: true,
        ArrSortOrder: true,
        Nofarerules: '',
        Class: '',
        Traveller: '',
        Modify_Search: '',
        From_City: '',
        To_City: '',
        Departure_Date: '',
        Return_Date: '',
        Travellers: '',
        Adults: '',
        Children: '',
        Infant: '',
        Economy: '',
        Business: '',
        First_Class: '',
        Search: '',
        Weather: '',
        Budget: '',
        Stops: '',
        Airlines: '',
        Departure_Time: '',
        Arrival_Time: '',
        Sort_by: '',
        Price: '',
        Duration: '',
        Flights_Found: '',
        Adult: '',
        Child: '',
        Departure: '',
        Return: '',
        Flight_Details: '',
        Fare_Rules: '',
        Flight_Itinerary: '',
        Baggage_Information: '',
        Fare_Breakup: '',
        Flight_From: '',
        To: '',
        Layover_at: '',
        Check_In: '',
        Cabin: '',
        Fare: '',
        Taxes_and_Fees: '',
        Total: '',
        Book_Now: '',
        Refundable: '',
        Infants: '',
        Non_Refundable: '',
        Refundable_with_Penalty: '',
        Terminal: '',
        Free: '',
        Before_Departure: '',
        After_Departure: '',
        Change_Search_Query: '',
        Search_Again: '',
        Sorry_no_results: '',
        Via: '',
        From: '',
        Dates: '',
        Oneway: '',
        RoundTrip: '',
        MultiCity: '',
        Stop: '',
        Non_Stop: '',
        multipleCarrier: [],
        filterType: 1,
        Details: [],
        Summary: [],
        calendarDepartureDate: null,
        calendarReturnDate: null,
        selectedCalendarDepartureDate: null,
        selectedCalendarReturnDate: null,
        groupedRetDates: {},
        groupedDepDates: [],
        calendarClicked: false,
        cellNum: null,
        calendarSearch: false,
        spinnerHtml: 'Fetching more flights<span class="loader__dot">.</span><span class="loader__dot">.</span><span class="loader__dot">.</span>',
        searchingNewDates: false,
        showPricingCalendar: true,
        searchForMore: false

    },
    created() {
        var userNode = JSON.parse(localStorage.User);
        if (userNode) {
            this.selectedCurrency = (localStorage.selectedCurrency) ? localStorage.selectedCurrency : userNode.loginNode.currency;
        } else {
            this.selectedCurrency = (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD';
        }
        
        this.getPagecontent();
        sessionStorage.setItem("payvisit", false);
        var urldata = getUrlVars().flight;
        if (urldata == undefined) {
            window.location.href = 'index.html';
        }

        var urldata = decodeURIComponent(urldata);
        urldata = urldata.substring(1, urldata.length).split('/');
        var calendarSearch = parseInt(urldata[urldata.length - 1].split('-')[11]);

        websocket.connect();
        var self = this;
        // console.log("THIS" + rq);
        // setTimeout(function () {
        var rq = self.getSearchData(0);

        setTimeout(() => {
            if (parseInt(calendarSearch) == 1) {
                try {
                    rq = self.getSearchData(1);
                    self.hasError = false;
                    websocket.sendMessage(JSON.stringify(rq)); //send request to web socket server 
                    setTimeout(() => {
                        rq = self.getSearchData(0);
                        websocket.sendMessage(JSON.stringify(rq)); //send request to web socket server
                    }, 500);
                } catch (error) {
                    try {
                        rq = self.getSearchData(1);
                        self.hasError = false;
                        websocket.sendMessage(JSON.stringify(rq));
                        setTimeout(() => {
                            rq = self.getSearchData(0);
                            websocket.sendMessage(JSON.stringify(rq)); //send request to web socket server
                        }, 500);
                    } catch (error) {
                        self.errMsg = "Our systems seem to be experiencing an issue. Please refresh the page. " + error;
                        self.hideAllLoaders = true;
                        self.hasError = true;
                    }
                }
            } else {
                try {
                    rq = self.getSearchData(0);
                    self.hasError = false;
                    websocket.sendMessage(JSON.stringify(rq)); //send request to web socket server 
                } catch (error) {
                    try {
                        rq = self.getSearchData(0);
                        self.hasError = false;
                        websocket.sendMessage(JSON.stringify(rq));
                    } catch (error) {
                        self.errMsg = "Our systems seem to be experiencing an issue. Please refresh the page. " + error;
                        self.hideAllLoaders = true;
                        self.hasError = true;
                    }
                }
            }
        }, 1000);
        
        // }, 1000);

        this.getWeather(function (resp) {
            self.weatherData = resp;
        });
        this.getArrCityTime();

        setTimeout(function () {
            // if (!self.hideAllLoaders) {
            if (self.allFlightRes.length == 0) {
                self.hideAllLoaders = true;
                self.hasError = true;
            }
            websocket.disconnect();
        }, 180000);
    },
    methods: {
        getSearchData: function(type) {
            var urldata = getUrlVars().flight;
            if (urldata == undefined) {
                window.location.href = 'index.html';
            }
            return this.createflightSearchRequest(urldata.substring(1, urldata.length).split('/'), type);
        },
        createflightSearchRequest: function(tripLeg, type) {
            var originDestinationInformationArr = [];
            for (var i = 0; i < tripLeg.length - 1; i++) {
                var Trip = tripLeg[i];
                var TripFrom = Trip.split('-')[0].toUpperCase();;
                var TripTo = Trip.split('-')[1].toUpperCase();;
                var TripDate = Trip.split('-')[2];
                if (i == 0) {
                    TripDate = this.selectedCalendarDepartureDate ? this.selectedCalendarDepartureDate : Trip.split('-')[2];
                } else if (i == 1) {
                    TripDate = this.selectedCalendarReturnDate ?  this.selectedCalendarReturnDate : Trip.split('-')[2];
                } else {
                    TripDate = Trip.split('-')[2];
                }
                var TripArray = {
                    departureDate: moment(TripDate, 'DD|MM|YYYY').format("DD-MM-YYYY"),
                    originLocation: TripFrom,
                    destinationLocation: TripTo,
                    radiusInformation: {
                        _FromValue: '0',
                        _ToValue: '250'
                    }
                }
                originDestinationInformationArr.push(TripArray);
            }
            var triptypes = tripLeg[tripLeg.length - 1];
            var totalADT = triptypes.split('-')[0];
            var totalCHD = triptypes.split('-')[1];
            var totalINF = triptypes.split('-')[2];
            var tripClass = triptypes.split('-')[3];
            if (tripClass.toLowerCase() == 'all') {
                tripClass = 'All';
            } else {
                tripClass = tripClass.toUpperCase();
            }
            var tripTyppee = triptypes.split('-')[7];
            var totalpax = parseInt(totalADT) + parseInt(totalCHD) + parseInt(totalINF);
            var supplierCode = triptypes.split('-')[4];
            if (supplierCode == 'all') {
                supplierCode = [];
            } else {
                supplierCode = supplierCode.split('|');
            }
            nmberOfRec = triptypes.split('-')[5];
            var filter = false;
            var isfiltered = triptypes.split('-')[6];
            if (isfiltered == 'T') {
                filter = true;
            }
            var stopQuantity = triptypes.split('-')[10];
            if (stopQuantity.toLowerCase() == "df") {
                stopQuantity = "Direct";
            } else {
                stopQuantity = "All";
            }
            var preferAirline = triptypes.split('-')[9];
            if (preferAirline == "" || preferAirline == undefined) {
                preferAirline = [];
            } else {
                preferAirline = preferAirline.split('|');
            }
        
            var airProv = [];
            var UserData = JSON.parse(localStorage.User);
            var supDetails = [];
            if (UserData.loginNode.servicesList != undefined) {
                supDetails = UserData.loginNode.servicesList.filter(function (service) {
                    return service.name.toLowerCase().includes('air')
                });
                supDetails[0].provider.forEach(function (data) {
                    airProv.push(data.id);
                });
            }
        
            this.calendarSearch = parseInt(triptypes.split('-')[11]);
            this.calendarDepartureDate = tripLeg[0].split('-')[2];

            var date = this.calendarDepartureDate.split("|");
            var year = date[2].slice(2);
            var completeDate = date[0]+date[1]+year;
            if (this.selectedCalendarDepartureDate == null) {
                this.selectedCalendarDepartureDate = completeDate;
            }
            if (tripTyppee.toLowerCase() == "r") {
                this.calendarReturnDate = tripLeg[1].split('-')[2];

                var date1 = this.calendarReturnDate.split("|");
                var year1 = date1[2].slice(2);
                var completeDate1 = date1[0]+date1[1]+year1;
                if (this.selectedCalendarReturnDate == null) {
                    this.selectedCalendarReturnDate = completeDate1;
                }
            }
            var userNode = JSON.parse(localStorage.User);
            var flightSearchRQ = {
                "request": {
                    "service": "FlightRQ",
                    "token": localStorage.access_token,
                    "supplierCodes": airProv,
                    "hqCode":userNode.loginNode.solutionId ? userNode.loginNode.solutionId :"",
                    "content": {
                        "command": type == 1 ? "CalendarSearchRQ" : "FlightSearchRQ",
                        "criteria": {
                            "criteriaType": "Air",
                            "commonRequestSearch": {
                                "numberOfUnits": parseInt(totalpax),
                                "typeOfUnit": "PX",
                                "numberOfRec": nmberOfRec,
                                "isFiltered": filter
                            },
                            "originDestinationInformation": originDestinationInformationArr,
                            "preferredAirline": preferAirline,
                            "nonStop": false,
                            cabin: tripClass,
                            "isRefundable": false,
                            "maxStopQuantity": stopQuantity,
                            "tripType": tripTyppee,
                            "preferenceLevel": "Preferred",
                            "target": "Test",
                            "passengerTypeQuantity": {
                                adt: parseInt(totalADT),
                                chd: parseInt(totalCHD),
                                inf: parseInt(totalINF)
                            },
                            "requestOption": null,
                            "pricingSource": null,
                            "dealcode": null,
                            "rangeOfDates": type == 1 ? 3 : undefined 
                        },
                        "sort": [{
                            "field": "price",
                            "order": "asc",
                            "sequence": 1
                        }],
                        "filter": {
                            "noOfResults": "250",
                            "from": 1,
                            "to": 100,
                            "sequence": 1
                        },
                        "supplierSpecific": {
                            "test": "test"
                        }
                    }
                }
            }
        
            return flightSearchRQ;
        },
        openFlightData(event, tabName, fIndex) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(tabName).style.display = "block";
            event.currentTarget.className += " active";

            if (tabName.split('_')[0] === "Fare-Rules") {
                if (event.currentTarget.getAttribute('isOpened') === "0") {
                    event.currentTarget.setAttribute('isOpened', "1");
                    this.selectFareRule(fIndex, tabName);
                }
            }
        },
        getAirLineName: function (airlinecode) {
            return getAirLineName(airlinecode);
        },
        airportLocationFromAirportCode: function (airlinecode) {
            return airportLocationFromAirportCode(airlinecode);
        },
        airportLocationFromAirportCodeUi: function (airlinecode) {
            return airportLocationFromAirportCodeUi(airlinecode);
        },
        airportFromAirportCode: function (airport) {
            return airportFromAirportCode(airport);
        },
        airportFromAirportCodeUi: function (airport) {
            return airportFromAirportCodeUi(airport);
        },
        airportData: function (airlinecode) {
            return airportData(airlinecode);
        },
        airportDataUi: function (airlinecode) {
            return airportDataUi(airlinecode);
        },
        calcLayoverTime: function (arrDate, arrTime, depDate, depTime) {
            return calcLayoverTime(arrDate, arrTime, depDate, depTime);
        },
        getBaggageUnitString: function (baggageValue) {
            return getBaggageUnitString(baggageValue);
        },
        isRefundableFlight: function (refundString) {
            return isRefundableFlight(refundString);
        },
        rDeptLocChange: function (e) {
            if (this.selDeptLoc.indexOf(e) !== -1) {
                this.selDeptLoc.splice(this.selDeptLoc.indexOf(e), 1);
            } else {
                var splited = e.split('-');
                var index = this.selDeptLoc.findIndex(function (element) {
                    return element.split('-')[0] === splited[0];
                })
                if (index !== -1) {
                    Vue.set(this.selDeptLoc, index, e);
                } else {
                    this.selDeptLoc.push(e);
                }
            }
        },
        rArrLocChange: function (e) {
            if (this.selArrtLoc.indexOf(e) !== -1) {
                this.selArrtLoc.splice(this.selArrtLoc.indexOf(e), 1);
            } else {
                var splited = e.split('-');
                var index = this.selArrtLoc.findIndex(function (element) {
                    return element.split('-')[0] === splited[0];
                })
                if (index !== -1) {
                    Vue.set(this.selArrtLoc, index, e);
                } else {
                    this.selArrtLoc.push(e);
                }
            }
        },
        getAirLineNameTrim: function (airlinecode, limit) {
            var airN = getAirLineName(airlinecode);
            return airN.length < limit ? airN : airN.substring(0, limit) + "...";
        },
        getWeather: function (callback) {
            var weatCity = this.weatherCity;
            axios.get('https://api.openweathermap.org/data/2.5/weather?q=' + weatCity + '&units=metric&appid=7c0ff0999be50513159abbf786d93a0a', {
                headers: {
                    'content-type': 'application/json'
                }
            }).then(function (response) {
                console.log(response);
                callback(response);
            });
        },
        momGetDate: function (toFormat) {
            return moment().format(toFormat);
        },
        moment: function (date, format) {
            return moment(date, format);
        },
        getCalendarRows: function (dates) {
            var vm = this;
            var compressedDates = [];
            var calendarDepartureDatesGrouped = _.groupBy(dates, "depDate");
            $.each(calendarDepartureDatesGrouped, function(key, value) {
                var minFare = _.min(value, function(e){ return e.fare; });
                compressedDates.push({
                    depDate: key,
                    fare: minFare.fare,
                    description: minFare.description,
                    airline: minFare.airline,
                });
            });
            var sorted = compressedDates.sort(function (a, b) {
                return moment(a.depDate, 'DDMMYY').valueOf() - moment(b.depDate, 'DDMMYY').valueOf();
            });
            
            if (sorted.length == 7) {
                return sorted;
            }
            var newArr = [];
            for(var x = 0; x < 7; x++) {
                var data = [];
                try {
                    var data = _.filter(sorted, function(e){ return e.depDate == vm.groupedDepDates[x].depDate; });
                } catch (error) {}

                if (data.length == 0) {
                    newArr.push({
                        airline: "NA",
                        depDate: "",
                        description: "",
                        fare: 0
                    })
                } else {
                    newArr.push(data[0]);
                }
            }

            return newArr;
        },
        setCalendarFilter: function (itemDep, itemRet) {
            var vm = this;
            $(".highlight").removeClass("highlighted");
            $(".t-header").removeClass("h-highlighted");
            $(".owl-item .item").removeClass("active");
            if(itemRet) {
                $("#cell-"+itemRet+"-"+itemDep.depDate).addClass("highlighted");
                $("#header-"+itemRet).addClass("h-highlighted");
                $("#header-"+itemDep.depDate).addClass("h-highlighted");
            } else {
                $("#cell-"+itemDep.depDate).addClass("highlighted");
                $("#header-"+itemDep.depDate).addClass("h-highlighted");
            }
            if (this.allFlightRes.length > 0) {
                var temp = [];
                if(this.tripType.toLowerCase() == 'r') {
                    temp = _.filter(this.allFlightRes, function(e){
                        return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == itemDep.depDate
                        && e.groupOfFlights[1].flightDetails[0].fInfo.dateTime.depDate == itemRet;
                    });
                } else {
                    temp = _.filter(this.allFlightRes, function(e){
                        return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == itemDep.depDate;
                    });
                }
                temp = temp.filter(function(res) {
                        var sameFareDep = res.fare.amount >= itemDep.fare;
                        var sameDateDep = res.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == itemDep.depDate;
                        if (itemRet) {
                            var sameFareRet = res.fare.amount >= itemDep.fare;
                            var sameDateRet = res.groupOfFlights[1].flightDetails[0].fInfo.dateTime.depDate == itemRet;
                            return sameFareDep && sameDateDep && sameFareRet && sameDateRet;
                        }
                        return sameFareDep && sameDateDep;
                })
                vm.filteredFlightRes = temp;
                vm.currentScrollLimit = 15;
                vm.flightResBind = vm.filteredFlightRes.slice(0, vm.currentScrollLimit);
                this.searchForMore = temp.length == 1;

            }
            vm.cellNum = itemDep.depDate+itemRet;
            vm.calendarClicked = !vm.calendarClicked;
            this.selectedCalendarDepartureDate = itemDep.depDate;
            this.selectedCalendarReturnDate = itemRet;
            this.selFlSerList = [];
            this.processMetrix();
            $(".tabcontent").hide();
        },
        getCalendarSearch(flight) {
           
            var index = this.allFlightRes.findIndex(function (e) {
                if (e.groupOfFlights[1]) {
                    return e.fare.amount == flight.fare.amount
                        && e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime == flight.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime
                        && e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == flight.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate
                        && e.groupOfFlights[1].flightDetails[0].fInfo.dateTime.arrTime == flight.groupOfFlights[1].flightDetails[0].fInfo.dateTime.arrTime
                        && e.groupOfFlights[1].flightDetails[0].fInfo.dateTime.arrDate == flight.groupOfFlights[1].flightDetails[0].fInfo.dateTime.arrDate
                        && e.groupOfFlights[0].flightDetails[0].fInfo.flightNo == flight.groupOfFlights[0].flightDetails[0].fInfo.flightNo;
                } else {
                    return e.fare.amount == flight.fare.amount
                        && e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime == flight.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime
                        && e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == flight.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate
                        && e.groupOfFlights[0].flightDetails[0].fInfo.flightNo == flight.groupOfFlights[0].flightDetails[0].fInfo.flightNo;
                }
                
            });

            this.allFlightRes.splice(index, 1);

            this.searchingNewDates = true;
            var searchRequest = this.getSearchData(0);
            // this.Showsearchsummary(searchRequest);    
            console.log(searchRequest);
            if (websocket.webSocketStatus == "Disconnected") {
                websocket.connect(); //open websocket connection           
            }

            websocket.sendMessage(JSON.stringify(searchRequest)); //send request to web socket server
        },
        processFlightResult: function (response) {
            var vm = this;
            console.log(response)
            var now = moment(new Date());
            // this.hideAllLoaders = true;

            if (isNullorUndefined(response) || isNullorUndefined(response.response) || isNullorUndefined(response.response.content) || isNullorUndefined(response.response.content.fareMasterPricerTravelBoardSearchReply) || isNullorUndefined(response.response.content.fareMasterPricerTravelBoardSearchReply.flightIndex)) {
                if (!isNullorUndefined(response) && !isNullorUndefined(response.response) && !isNullorUndefined(response.response.content) && !isNullorUndefined(response.response.content.error) && !isNullorUndefined(response.response.content.error)) {
                    this.errMsg = response.response.content.error.message;
                }
            } else if (response.response.content.fareMasterPricerTravelBoardSearchReply.flightIndex != null) {
                this.hideAllLoaders = true;
                this.hasError = false;
                response.response.content.fareMasterPricerTravelBoardSearchReply.flightIndex.forEach(fi => {
                    fi.selectCredential = response.response.selectCredential;
                    this.allFlightRes.push(fi);
                });
                // this.allFlightRes.push(response.response.content.fareMasterPricerTravelBoardSearchReply.flightIndex.values());

                this.filteredFlightRes = this.allFlightRes.sort(function (a, b) {
                    return parseFloat(a.fare.amount) - parseFloat(b.fare.amount);
                });

                if(this.tripType.toLowerCase() == 'r') {
                    this.filteredFlightRes = _.filter(this.allFlightRes, function(e){
                        return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarDepartureDate 
                        && e.groupOfFlights[1].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarReturnDate;
                    });
                } else {
                    this.filteredFlightRes = _.filter(this.allFlightRes, function(e){
                        return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarDepartureDate;
                    });
                }

                this.flightResBind = this.filteredFlightRes.slice(0, this.currentScrollLimit);



                localStorage.access_token = response.response.token;
                localStorage.timer = new Date();

                this.currency = this.allFlightRes[0].fare.currency;
                //Get Filter Data
                // this.resMinPrice = _.min(this.allFlightRes, function (flind) { return flind.fare.amount; }).fare.amount;
                // this.resMaxPrice = _.max(this.allFlightRes, function (flind) { return flind.fare.amount; }).fare.amount;

                var self = this;
                this.$nextTick(function () {
                    if (self.allFlightRes.length > 0) {
                        //Get Filter Data
                        var lowestPrice = Math.min.apply(Math, self.allFlightRes.map(function (o) {
                            return o.fare.amount;
                        }))
                        var highestPrice = Math.max.apply(Math, self.allFlightRes.map(function (o) {
                            return o.fare.amount;
                        }))

                        if (highestPrice == lowestPrice) {
                            lowestPrice = parseFloat(lowestPrice) - 1;
                        }

                        lowestPrice = i18n.n(lowestPrice / self.CurrencyMultiplier, 'currency', self.selectedCurrency).match(/\d+(\.\d+)?/g).join('');
                        highestPrice = i18n.n(highestPrice / self.CurrencyMultiplier, 'currency', self.selectedCurrency).match(/\d+(\.\d+)?/g).join('');

                        $("#minPricevalue").text(lowestPrice);
                        $("#MaxPricevalue").text(highestPrice);

                        var slider = document.getElementById("price-slider");

                        if (slider.noUiSlider) {
                            slider.noUiSlider.updateOptions({
                                tooltips: [true, true],
                                start: [parseFloat(lowestPrice), parseFloat(highestPrice)],
                                range: {
                                    'min': parseFloat(lowestPrice),
                                    'max': parseFloat(highestPrice)
                                }
                            });
                        } else {
                            noUiSlider.create(slider, {
                                start: [parseFloat(lowestPrice), parseFloat(highestPrice)],
                                connect: true,
                                tooltips: [true, true],
                                range: {
                                    'min': [parseFloat(lowestPrice)],
                                    'max': [parseFloat(highestPrice)]
                                }
                            })

                            slider.noUiSlider.on('update', function (values) {
                                self.lowestPrice = values[0];
                                self.highestPrice = values[1];
                            });

                        }

                        setTimeout(function () {
                            var owl = $("#slider-carousel");
                            var owlInstance = owl.data('owlCarousel');
                            // if instance is existing
                            if (owlInstance != null) {
                                owlInstance.reinit();
                            } else {
                                owl.owlCarousel({
                                    items: 6,
                                    itemsDesktop: [1024, 4],
                                    itemsDesktopSmall: [768, 4],
                                    itemsTablet: [480, 2],
                                    itemsMobile: false,
                                    pagination: false
                                });
                                $(".next").click(function () {
                                    owl.trigger('owl.next');
                                })
                                $(".prev").click(function () {
                                    owl.trigger('owl.prev');
                                })
                            }

                            $("#hide, #hide1, #hide2, #hide4, #hide5, #hide6, #hide7, #hide8, #hide9, #hide10, #hide11, #hide12, #hide13").click(function () {
                                $(".tabcontent").hide();
                            });

                            $('.tab_content').hide();
                            $('.tab_content:first').show();
                            $('.tabs li').click(function (event) {
                                $(this).toggleClass('active');
                                $(this).siblings().removeClass('active');
                                $('.tab_content').hide();
                                var selectTab = $(this).find('a').attr("href");
                                $(selectTab).fadeIn();
                            });
                        }, 100);
                    }
                    $('[data-toggle="tooltip"]').tooltip();
                });



                //Price Slider
                // var slider = document.getElementById('price-slider')

                // if (self.resMinPrice != self.resMaxPrice) {
                //     noUiSlider.create(slider, {
                //         start: [self.resMinPrice, self.resMaxPrice],
                //         connect: true,
                //         step: 10,
                //         range: {
                //             'min': self.resMinPrice,
                //             'max': self.resMaxPrice
                //         }
                //     });

                //     slider.noUiSlider.on('update', function (values, handle) {
                //         $("#minPricevalue").text(self.$n(parseFloat(values[0] / self.CurrencyMultiplier), 'currency', self.selectedCurrency));
                //         $("#MaxPricevalue").text(self.$n(parseFloat(values[1] / self.CurrencyMultiplier), 'currency', self.selectedCurrency));

                //         if (self.changedMinPrice == 0) {
                //             self.changedMinPrice = parseFloat(values[0]);
                //         }
                //         if (self.changedMaxPrice == 0) {
                //             self.changedMaxPrice = parseFloat(values[1]);
                //         }
                //     });

                //     slider.noUiSlider.on('end', function (values, handle) {
                //         self.changedMinPrice = parseFloat(values[0]);
                //         self.changedMaxPrice = parseFloat(values[1]);
                //     });
                // } else {
                //     self.changedMinPrice = self.resMinPrice;
                //     self.changedMaxPrice = self.resMaxPrice;
                // }

                var calendarDepartureDates = [];
                    
                this.allFlightRes.forEach(fl => {
                    fl.groupOfFlights.forEach(leg => {
                        var calendarData = {};
                        if (fl.groupOfFlights[1]) {
                            calendarData = {
                                depDate: fl.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate,
                                depDateRet: fl.groupOfFlights[1].flightDetails[0].fInfo.dateTime.depDate,
                                fare: fl.fare.amount,
                                description: fl.description,
                                airline: this.getAirLineName(fl.groupOfFlights[1].flightDetails[0].fInfo.companyId.mCarrier)
                            }
                        } else {
                            calendarData = {
                                depDate: fl.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate,
                                fare: fl.fare.amount,
                                description: fl.description,
                                airline: this.getAirLineName(fl.groupOfFlights[0].flightDetails[0].fInfo.companyId.mCarrier)
                            }
                        }
                        calendarDepartureDates.push(calendarData);
                    });
                });

                var calendarDepartureDatesGrouped = _.groupBy(calendarDepartureDates, "depDate");
                var tmpDepCal = [];
                $.each(calendarDepartureDatesGrouped, function(key, value) {
                    var minFare = _.min(value, function(e){ return e.fare; });
                    tmpDepCal.push({
                        depDate: key,
                        fare: minFare.fare,
                        description: minFare.description,
                        airline: minFare.airline,
                    });
                });

                this.processMetrix();
                this.searchingNewDates = false;
                
                $(".highlight").removeClass("highlighted");
                $(".t-header").removeClass("h-highlighted");
                $(".owl-item .item").removeClass("active");
                if(this.tripType.toLowerCase() == 'r') {
                    var date = this.selectedCalendarDepartureDate;
                    var date1 = this.selectedCalendarReturnDate;
                    
                    var newObj= _.groupBy(_.sortBy(calendarDepartureDates, function (a) {
                        return moment(a.depDate, 'DDMMYY').valueOf();
                      }), "depDateRet");
                      var tempArr = [];
                      Object.keys(newObj)
                        .sort(function (a, b) {
                            return moment(a, 'DDMMYY').valueOf() - moment(b, 'DDMMYY').valueOf();
                          }).forEach((a) => {
                        tempArr.push({
                            name: a,
                            value: newObj[a]
                        })});
                        if (tempArr.length == 7) {
                            this.groupedRetDates = tempArr;
                        } else {
                            var newArr = [];
                            for(var x = 0; x < 7; x++) {
                                var data = [];
                                try {
                                    var dt = moment(vm.calendarReturnDate, 'DD|MM|YYYY').subtract(3,'days').add(x, 'days').format('DDMMYY');
                                    var data = _.filter(tempArr, function(e){ return e.name == dt; });
                                } catch (error) {}
                                
                                if (data.length == 0) {
                                    newArr.push({
                                        name: dt,
                                        value: []
                                    })
                                } else {
                                    newArr.push(data[0]);
                                }
                                this.groupedRetDates = newArr;
                            }
                        }
                    setTimeout(() => {
                        $("#cell-"+date1+"-"+date).addClass("highlighted");
                        console.log({date1});
                        console.log({date});
                        $("#r-header-"+date1).addClass("h-highlighted");
                        $("#header-"+date).addClass("h-highlighted");
                    }, 200);
                }
                var date = this.selectedCalendarDepartureDate;
                this.groupedDepDates = tmpDepCal.sort(function (a, b) {
                    return moment(a.depDate, 'DDMMYY').valueOf() - moment(b.depDate, 'DDMMYY').valueOf();
                    });
                    if (tmpDepCal.length == 7) {
                        this.groupedDepDates = tmpDepCal;
                        } else {
                            var newArr = [];
                            for(var x = 0; x < 7; x++) {
                                var data = [];
                                try {
                                    var dt = moment(vm.calendarDepartureDate, 'DD|MM|YYYY').subtract(3,'days').add(x, 'days').format('DDMMYY');
                                    var data = _.filter(tmpDepCal, function(e){ return e.depDate == dt; });
                                } catch (error) {}
                                
                                if (data.length == 0) {
                                    newArr.push({
                                        depDate: "",
                                        fare: "",
                                        description: "",
                                        airline: "NA",
                                    })
                                } else {
                                    newArr.push(data[0]);
                                }
                                this.groupedDepDates = newArr;
                            }
                        }
                // this.groupedDepDates = _.sortBy(tmpDepCal, function(e){ return e.depDate; });
                setTimeout(() => {
                    $("#cell-"+date).addClass("highlighted");
                    $("#header-"+date).addClass("h-highlighted");
                    console.log({date});
                }, 200);

                this.searchForMore = false;
            }
            // else {
            //     if (response.response.content.error.message != null) {
            //         this.hasError = true;
            //         this.errMsg = response.response.content.error.message;
            //     }
            // }
            var end = moment(new Date());
            console.log(moment.duration(end.diff(now)).asSeconds());
        },
        processMetrix: function () {
            //Gen Filter Data
            var tempAirlines = [];
            var tempStops = [];
            var tempDepLoc = [];
            var tempArrLoc = [];
            var multipleCarrier = [];
            var vm = this;

            var tmp = this.allFlightRes;
            if(this.tripType.toLowerCase() == 'r') {
                tmp = _.filter(this.allFlightRes, function(e){
                    return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarDepartureDate 
                    && e.groupOfFlights[1].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarReturnDate;
                });
            } else {
                tmp = _.filter(this.allFlightRes, function(e){
                    return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarDepartureDate;
                });
            }

            tmp.forEach(fl => {
                fl.groupOfFlights.forEach(leg => {
                    tempStops.push(leg.flightDetails.length - 1)
                    tempDepLoc.push(leg.flightDetails[0].fInfo.location.locationFrom)
                    tempArrLoc.push(leg.flightDetails[leg.flightDetails.length - 1].fInfo.location.locationTo)
                    if (!leg.flightDetails.every((val, i, arr) => val.fInfo.companyId.mCarrier === arr[0].fInfo.companyId.mCarrier)) {
                        leg.flightDetails.forEach(seg => {
                            multipleCarrier.push(fl.fare.amount);
                        });
                    }
                    leg.flightDetails.forEach(seg => {
                        if (seg.fInfo.stopQuantity > 0) {
                            tempStops.push(99);
                        }
                    });
                    if (leg.flightDetails.every((val, i, arr) => val.fInfo.companyId.mCarrier === arr[0].fInfo.companyId.mCarrier)) {
                        leg.flightDetails.forEach(seg => {
                            tempAirlines.push({
                                "air": seg.fInfo.companyId.mCarrier,
                                "price": fl.fare.amount
                            });
                        });
                    }
                });
            });

            this.multipleCarrier = multipleCarrier;
            tempAirLine = [];
            airPrice = [];
            var resAirlines1 = _.groupBy(tempAirlines, "air");


            $.each(resAirlines1, function (key, value) {
                var airname = key;
                $.each(value, function (key1, value1) {
                    airPrice.push(value1.price);
                });
                tempAirLine.push({
                    "air": airname,
                    "price": airPrice
                });
                airname = null;
                airPrice = [];

            });
            var airMaxMin = tempAirLine.map(function (air) {
                return {
                    air: air.air,
                    priceMax: Math.max.apply(Math, air.price),
                    price: Math.min.apply(Math, air.price)
                }
            });

            this.resStops = _.sortBy(_.uniq(tempStops));
            this.resAirlines = airMaxMin.sort();
            this.resDepLoc = _.uniq(tempDepLoc);
            this.resArrLoc = _.uniq(tempArrLoc);
            this.totalResCount = tmp.length;

            if (this.multipleCarrier.length > 0) {
                airMaxMin.push({
                    air: "Multi-Airline",
                    priceMax: Math.max.apply(Math, this.multipleCarrier),
                    price: Math.min.apply(Math, this.multipleCarrier),
                });
            }

            setTimeout(function () {
                var owl = $("#slider-carousel");
                var owlInstance = owl.data('owlCarousel');
                // if instance is existing
                if (owlInstance != null) {
                    owlInstance.reinit();
                } else {
                    owl.owlCarousel({
                        items: 6,
                        itemsDesktop: [1024, 4],
                        itemsDesktopSmall: [768, 4],
                        itemsTablet: [480, 2],
                        itemsMobile: false,
                        pagination: false
                    });
                    $(".next").click(function () {
                        owl.trigger('owl.next');
                    })
                    $(".prev").click(function () {
                        owl.trigger('owl.prev');
                    })
                }

                $("#hide, #hide1, #hide2, #hide4, #hide5, #hide6, #hide7, #hide8, #hide9, #hide10, #hide11, #hide12, #hide13").click(function () {
                    $(".tabcontent").hide();
                });

                $('.tab_content').hide();
                $('.tab_content:first').show();
                $('.tabs li').click(function (event) {
                    $(this).toggleClass('active');
                    $(this).siblings().removeClass('active');
                    $('.tab_content').hide();
                    var selectTab = $(this).find('a').attr("href");
                    $(selectTab).fadeIn();
                });
            }, 100);
        },
        unChkMatrix: function (val) {
            if (val === this.checkAirlinesMatrix) {
                this.checkAirlinesMatrix = '';
            }
        },
        getMiniRules: function (flIndex, tabName) {
            var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;
            var airMiniRule = ServiceUrls.hubConnection.hubServices.flights.airMiniRule;
            var currenttoken = localStorage.access_token;

            var self = this;
            self.Nofarerules = '';

            var rq = this.createSelRqMinRq('minirule', flIndex);
            console.log(rq)

            axios.post(hubUrl + airMiniRule, rq, {
                headers: {
                    'Authorization': 'Bearer ' + currenttoken
                }
            }).then(function (response) {
                console.log(response)
                localStorage.access_token = response.headers.access_token;
                localStorage.timer = new Date();

                var miniRuleLists = [];

                var miniRules = response.data.response.content.minirules;
                miniRules.forEach(rule => {

                    var miniRules = [];

                    rule.codes[0].category.forEach(cat => {
                        if (cat.cat == "31" || cat.cat == "33") {
                            var before33Restriappinfo = {
                                action: 0
                            };
                            var before33Moninfo = {
                                amount: 0,
                                currency: ''
                            };
                            var before31Restriappinfo = {
                                action: 0
                            };
                            var before31Moninfo = {
                                amount: 0,
                                currency: ''
                            };

                            var after33Restriappinfo = {
                                action: 0
                            };
                            var after33Moninfo = {
                                amount: 0,
                                currency: ''
                            };
                            var after31Restriappinfo = {
                                action: 0
                            };
                            var after31Moninfo = {
                                amount: 0,
                                currency: ''
                            };

                            if (cat.cat == "31") {
                                try {
                                    before33Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'BDA');
                                } catch (err) {}
                                try {
                                    before33Moninfo = cat.monInfo.find(x => x.typeQualifier == 'BDX');
                                } catch (err) {}
                                try {
                                    after33Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'BDA');
                                } catch (err) {}
                                try {
                                    after33Moninfo = cat.monInfo.find(x => x.typeQualifier == 'BDX');
                                } catch (err) {}

                                miniRules.push({
                                    type: 'Cancellation',
                                    before: ((before33Restriappinfo.action == 1) ? ('Allowed @ ' + self.$n(parseFloat(before33Moninfo.amount / self.CurrencyMultiplier), 'currency', self.selectedCurrency)) : 'Not Allowed'),
                                    after: ((before31Restriappinfo.action == 1) ? ('Allowed @ ' + self.$n(parseFloat(before31Moninfo.amount / self.CurrencyMultiplier), 'currency', self.selectedCurrency)) : 'Not Allowed')
                                });
                            }
                            if (cat.cat == "33") {
                                try {
                                    before31Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'ADA');
                                } catch (err) {}
                                try {
                                    before31Moninfo = cat.monInfo.find(x => x.typeQualifier == 'ADX');
                                } catch (err) {}
                                try {
                                    after31Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'ADA');
                                } catch (err) {}
                                try {
                                    after31Moninfo = cat.monInfo.find(x => x.typeQualifier == 'ADX');
                                } catch (err) {}

                                miniRules.push({
                                    type: 'Changes',
                                    before: ((after33Restriappinfo.action == 1) ? ('Allowed @ ' + self.$n(parseFloat(after33Moninfo.amount / self.CurrencyMultiplier), 'currency', self.selectedCurrency)) : 'Not Allowed'),
                                    after: ((after31Restriappinfo.action == 1) ? ('Allowed @ ' + self.$n(parseFloat(after31Moninfo.amount / self.CurrencyMultiplier), 'currency', self.selectedCurrency)) : 'Not Allowed')
                                });
                            }
                        }
                    });

                    var miniRuleList = {
                        "from": rule.departure,
                        "to": rule.arrival,
                        "miniRules": miniRules
                    };

                    miniRuleLists.push(miniRuleList);
                });

                Vue.set(self.flightResBind[flIndex], 'miniRules', miniRuleLists);
            }).catch(function (error) {
                console.log('Error on Mini Rules - ' + error);
                self.Nofarerules = 'No fare rules found';
                Vue.set(self.flightResBind[flIndex], 'miniRules', null);
            });
        },
        createSelRqMinRq: function (type, flIndex) {
            var searchData = this.getSearchData(0);
            var vm = this;
            var selFlightOption = this.flightResBind[flIndex];
            var fareSourceCode = null;
            var segGroups = [];
            selFlightOption.groupOfFlights.forEach(function (leg, legind) {
                leg.flightDetails.forEach(function (seg, segind) {
                    var vAirline = leg.flightProposal.vAirline;
                    var Seginfo = {
                        "segmentInformation": {
                            "flightDate": {
                                "departureDate": seg.fInfo.dateTime.depDate,
                                "departureTime": seg.fInfo.dateTime.depTime,
                                "arrivalDate": seg.fInfo.dateTime.arrDate,
                                "arrivalTime": seg.fInfo.dateTime.arrTime
                            },
                            "fareId": seg.fInfo.eqpType,
                            "boardPointDetails": {
                                "trueLocationId": seg.fInfo.location.locationFrom
                            },
                            "offPointDetails": {
                                "trueLocationId": seg.fInfo.location.locationTo
                            },
                            "companyDetails": {
                                "marketingCompany": seg.fInfo.companyId.mCarrier,
                                "operatingCompany": seg.fInfo.companyId.oCarrier,
                                "validatingCompany": vAirline,
                            },
                            "flightIdentification": {
                                "flightNumber": seg.fInfo.flightNo,
                                "bookingClass": seg.fInfo.rbd
                            },
                            "flightTypeDetails": {
                                "flightIndicator": String(legind + 1),
                                "itemNumber": segind + 1
                            }
                        }
                    }
                    segGroups.push(Seginfo);

                });
            });

            var Com = "FlightSelectRQ";
            if (type === "minirule") {
                Com = "FlightMiniRulesRQ";
            }

            var supplierSpecific = selFlightOption.supplierSpecific ? selFlightOption.supplierSpecific : {}

            if (selFlightOption.segmentRef.supplier == 1) {
                try {
                    fareSourceCode = selFlightOption.typeRef.type;
                } catch (err) {}
            } else if (selFlightOption.segmentRef.supplier == "64" || selFlightOption.segmentRef.supplier == "45") {
                try {
                    fareSourceCode = selFlightOption.segmentRef.segRef;
                } catch (err) {}
            }

            var rq = {
                "request": {
                    "service": "FlightRQ",
                    "supplierCodes": [selFlightOption.segmentRef.supplier],
                    "content": {
                        "command": Com,
                        "commonRequestFarePricer": {
                            "body": {
                                "airRevalidate": {
                                    // "fareSourceCode": null,
                                    "fareSourceCode": fareSourceCode,
                                    "sessionId": selFlightOption.segmentRef.pSessionId,
                                    "target": "Test",
                                    "adt": searchData.request.content.criteria.passengerTypeQuantity.adt,
                                    "chd": searchData.request.content.criteria.passengerTypeQuantity.chd,
                                    "inf": searchData.request.content.criteria.passengerTypeQuantity.inf,
                                    "paymentCardType": null,
                                    "segmentGroup": segGroups,
                                    "tripType": searchData.request.content.criteria.tripType,
                                    "from": selFlightOption.groupOfFlights[0].flightDetails[0].fInfo.location.locationFrom,
                                    "to": vm.tripType.toLowerCase() == 'r' ?  selFlightOption.groupOfFlights[selFlightOption.groupOfFlights.length - 1].flightDetails[0].fInfo.location.locationFrom :  selFlightOption.groupOfFlights[selFlightOption.groupOfFlights.length - 1].flightDetails[selFlightOption.groupOfFlights[selFlightOption.groupOfFlights.length - 1].flightDetails.length - 1].fInfo.location.locationTo,
                                    "dealCode": selFlightOption.dealCode
                                }
                            },
                            "supplierAgencyDetails": null
                        },
                        "supplierSpecific": supplierSpecific
                    },
                    "selectCredential": selFlightOption.selectCredential
                }
            }

            return rq;
        },
        selectFlight: function (ev, flIndex) {
            var selectFlightRq = this.createSelRqMinRq('select', flIndex);
            var selectedFlightInfo = this.flightResBind[flIndex];
            localStorage.selectedFlight = JSON.stringify({
                "selectFlightRq": selectFlightRq,
                "selectedFlightInfo": selectedFlightInfo
            });
            window.location.href = '/Flights/flight-booking.html';
        },
        selectFareRule: function (flIndex, tabName) {
            try {
                var self = this;
                var huburl = ServiceUrls.hubConnection.baseUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var requrl = ServiceUrls.hubConnection.hubServices.flights.airSelect;
                var selectFlightRq = this.createSelRqMinRq('select', flIndex);
                axios.post(huburl + portno + requrl, selectFlightRq, axiosConfigHeader()).then((response) => {
                    var Details = response.data.response.content.fareInformationWithoutPnrReply.airFareRule;
                    Vue.set(self.flightResBind[flIndex], 'details', Details);
                    var Summary = response.data.response.content.fareInformationWithoutPnrReply.minirules;

                    var SummaryLists = [];
                    Summary.forEach(rule => {
                        SummaryList = _.groupBy(rule.category, "cat");
                        SummaryLists.push(SummaryList);
                    });
                    Vue.set(self.flightResBind[flIndex], 'summary', SummaryLists);

                }).catch((err) => {

                });
            } catch (err) {}

        },
        rules: function (item) {
            var self = this;
            var rules;
            var before = item.find(x => x.situation == 'Before departure');
            if (before.allowed) {
                if (before.amount > 0) {
                    rules = '<p>' + before.cat + '</p><p class="allowed">Allowed @ ' + self.$n(parseFloat(before.amount / self.CurrencyMultiplier), 'currency', self.selectedCurrency) + '</p>'

                } else {
                    rules = '<p>' + before.cat + '</p><p class="allowed">Allowed</p>';
                }

            } else {
                rules = '<p>' + before.cat + '</p><p class="not-allowed">Not Allowed</p>';
            }

            var after = item.find(x => x.situation == 'After departure');
            if (after.allowed) {
                if (after.amount > 0) {
                    rules = rules + '<p class="allowed">Allowed @ ' + self.$n(parseFloat(after.amount / self.CurrencyMultiplier), 'currency', self.selectedCurrency) + '</p>'

                } else {
                    rules = rules + '<p class="allowed">Allowed</p>';
                }

            } else {
                rules = rules + '<p class="not-allowed">Not Allowed</p>';
            }
            return rules;

        },
        getElapseTime: function (elapStr) {
            return elapStr.substring(0, 2) + 'h ' + elapStr.substring(2, 4) + 'm ';

            //var elap = elapStr.substring(0, 2) + '.' + elapStr.substring(2, 4);
            // var hour = parseFloat(elapStr.substring(0, 2));
            // var min = parseFloat(elapStr.substring(2, 4));
            // var hpm = hour + '.' + min;
            // var hm = Math.round(hpm * 100) / 100;
            // var dur = moment.duration(hm, 'hours');
            // var hours = Math.floor(dur.asHours());
            // var mins = Math.floor(dur.asMinutes()) - hours * 60;
            // return hours + "h " + mins + "m ";

            // var elapInt = parseInt(elapStr);
            // var dur = moment.duration(elap, 'hours');
            // return dur.format('HH:mm:ss');
        },
        getArrCityTime: function () {
            var d = new Date();
            var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
            var nd = new Date(utc + (3600000 * this.timeOfsetCity));
            this.arrCityTime = moment(nd).format('hh:mm a');
        },
        hideFlDet: function () {
            $(".tabcontent").hide();
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + commonAgencyCode + '/Template/Flight Result/Flight Result/Flight Result.ftl';
                axios.get(aboutUsPage, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (response.data.area_List.length > 0) {
                        var mainComp = response.data.area_List[0].main;
                        self.Class = self.pluckcom('Class', mainComp.component);
                        self.Traveller = self.pluckcom('Traveller', mainComp.component);
                        self.Modify_Search = self.pluckcom('Modify_Search', mainComp.component);
                        self.From_City = self.pluckcom('From_City', mainComp.component);
                        self.To_City = self.pluckcom('To_City', mainComp.component);
                        self.Departure_Date = self.pluckcom('Departure_Date', mainComp.component);
                        self.Return_Date = self.pluckcom('Return_Date', mainComp.component);
                        self.Travellers = self.pluckcom('Travellers', mainComp.component);
                        self.Adults = self.pluckcom('Adults', mainComp.component);
                        self.Children = self.pluckcom('Children', mainComp.component);
                        self.Infant = self.pluckcom('Infant', mainComp.component);
                        self.Economy = self.pluckcom('Economy', mainComp.component);
                        self.Business = self.pluckcom('Business', mainComp.component);
                        self.First_Class = self.pluckcom('First_Class', mainComp.component);
                        self.Search = self.pluckcom('Search', mainComp.component);
                        self.Weather = self.pluckcom('Weather', mainComp.component);
                        self.Budget = self.pluckcom('Budget', mainComp.component);
                        self.Stops = self.pluckcom('Stops', mainComp.component);
                        self.Airlines = self.pluckcom('Airlines', mainComp.component);
                        self.Departure_Time = self.pluckcom('Departure_Time', mainComp.component);
                        self.Arrival_Time = self.pluckcom('Arrival_Time', mainComp.component);
                        self.Sort_by = self.pluckcom('Sort_by', mainComp.component);
                        self.Price = self.pluckcom('Price', mainComp.component);
                        self.Duration = self.pluckcom('Duration', mainComp.component);
                        self.Flights_Found = self.pluckcom('Flights_Found', mainComp.component);
                        self.Adult = self.pluckcom('Adult', mainComp.component);
                        self.Child = self.pluckcom('Child', mainComp.component);
                        self.Departure = self.pluckcom('Departure', mainComp.component);
                        self.Return = self.pluckcom('Return', mainComp.component);
                        self.Flight_Details = self.pluckcom('Flight_Details', mainComp.component);
                        self.Fare_Rules = self.pluckcom('Fare_Rules', mainComp.component);
                        self.Flight_Itinerary = self.pluckcom('Flight_Itinerary', mainComp.component);
                        self.Baggage_Information = self.pluckcom('Baggage_Information', mainComp.component);
                        self.Fare_Breakup = self.pluckcom('Fare_Breakup', mainComp.component);
                        self.Flight_From = self.pluckcom('Flight_From', mainComp.component);
                        self.To = self.pluckcom('To', mainComp.component);
                        self.Layover_at = self.pluckcom('Layover_at', mainComp.component);
                        self.Check_In = self.pluckcom('Check_In', mainComp.component);
                        self.Cabin = self.pluckcom('Cabin', mainComp.component);
                        self.Fare = self.pluckcom('Fare', mainComp.component);
                        self.Taxes_and_Fees = self.pluckcom('Taxes_and_Fees', mainComp.component);
                        self.Total = self.pluckcom('Total', mainComp.component);
                        self.Book_Now = self.pluckcom('Book_Now', mainComp.component);
                        self.Refundable = self.pluckcom('Refundable', mainComp.component);
                        self.Infants = self.pluckcom('Infants', mainComp.component);
                        self.Non_Refundable = self.pluckcom('Non_Refundable', mainComp.component);
                        self.Refundable_with_Penalty = self.pluckcom('Refundable_with_Penalty', mainComp.component);
                        self.Terminal = self.pluckcom('Terminal', mainComp.component);
                        self.Free = self.pluckcom('Free', mainComp.component);
                        self.Before_Departure = self.pluckcom('Before_Departure', mainComp.component);
                        self.After_Departure = self.pluckcom('After_Departure', mainComp.component);
                        self.Change_Search_Query = self.pluckcom('Change_Search_Query', mainComp.component);
                        self.Search_Again = self.pluckcom('Search_Again', mainComp.component);
                        self.Sorry_no_results = self.pluckcom('Sorry_no_results', mainComp.component);
                        self.Via = self.pluckcom('Via', mainComp.component);
                        self.From = self.pluckcom('From', mainComp.component);
                        self.Dates = self.pluckcom('Dates', mainComp.component);
                        self.Oneway = self.pluckcom('Oneway', mainComp.component);
                        self.RoundTrip = self.pluckcom('RoundTrip', mainComp.component);
                        self.MultiCity = self.pluckcom('MultiCity', mainComp.component);
                        self.Stop = self.pluckcom('Stop', mainComp.component);
                        self.Non_Stop = self.pluckcom('Non_Stop', mainComp.component);
                        try {
                            oneway.Search = self.Search;
                        } catch (err) {}
                        try {
                            oneway.Departure_Date = self.Departure_Date;
                        } catch (err) {}
                        try {
                            oneway.Return_Date = self.Return_Date;
                        } catch (err) {}
                        try {
                            oneway.placeholderfrom = self.From_City;
                        } catch (err) {}
                        try {
                            oneway.placeholderTo = self.To_City;
                        } catch (err) {}
                        try {
                            roundtrip.Search = self.Search;
                        } catch (err) {}
                        try {
                            roundtrip.Departure_Date = self.Departure_Date;
                        } catch (err) {}
                        try {
                            roundtrip.Return_Date = self.Return_Date;
                        } catch (err) {}
                        try {
                            roundtrip.placeholderfrom = self.From_City;
                        } catch (err) {}
                        try {
                            roundtrip.placeholderTo = self.To_City;
                        } catch (err) {}
                        try {
                            multicity.Search = self.Search;
                        } catch (err) {}
                        try {
                            multicity.Departure_Date = self.Departure_Date;
                        } catch (err) {}
                        try {
                            multicity.Return_Date = self.Return_Date;
                        } catch (err) {}
                        try {
                            multicity.placeholderfrom = self.From_City;
                        } catch (err) {}
                        try {
                            multicity.placeholderTo = self.To_City;
                        } catch (err) {}

                        self.sortValues = [{
                                type: "1",
                                isActive: true,
                                sortBy: self.Price,
                                isAscending: false
                            },
                            {
                                type: "2",
                                isActive: false,
                                sortBy: self.Duration,
                                isAscending: false
                            },
                            {
                                type: "3",
                                isActive: false,
                                sortBy: self.Departure_Time,
                                isAscending: false
                            },
                            {
                                type: "4",
                                isActive: false,
                                sortBy: self.Arrival_Time,
                                isAscending: false
                            }
                        ]
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        getmoreinfo(url) {

            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url = "/Nirvana/book-now.html?page=" + url;
            } else {
                url = "#";
            }
            return url;
        },
        sortResults: function (event) {
            var index = _.findIndex(this.sortValues, function (e) {
                return e.type == event.currentTarget.id;
            })
            var item = this.sortValues[index];
            this.sortValues = [{
                    type: "1",
                    isActive: false,
                    sortBy: this.Price,
                    isAscending: true
                },
                {
                    type: "2",
                    isActive: false,
                    sortBy: this.Duration,
                    isAscending: true
                },
                {
                    type: "3",
                    isActive: false,
                    sortBy: this.Departure_Time,
                    isAscending: true
                },
                {
                    type: "4",
                    isActive: false,
                    sortBy: this.Arrival_Time,
                    isAscending: true
                }
            ];
            this.$set(this.sortValues, index, {
                type: item.type,
                isActive: true,
                sortBy: item.sortBy,
                isAscending: !item.isAscending
            });

            switch (event.currentTarget.id) {
                case "1":
                    this.filteredFlightRes.sort(function (a, b) {
                        return item.isAscending ? parseFloat(a.fare.amount) - parseFloat(b.fare.amount) : parseFloat(b.fare.amount) - parseFloat(a.fare.amount);
                    });
                    break;
                case "2":
                    this.filteredFlightRes.sort(function (a, b) {
                        return item.isAscending ? parseInt(a.groupOfFlights[0].flightProposal.elapse) - parseInt(b.groupOfFlights[0].flightProposal.elapse) : parseInt(b.groupOfFlights[0].flightProposal.elapse) - parseInt(a.groupOfFlights[0].flightProposal.elapse);
                    });
                    break;
                case "3":
                    this.filteredFlightRes.sort(function (a, b) {
                        return item.isAscending ? moment(a.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm') : moment(b.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm') - new moment(a.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm');
                    });
                    break;
                case "4":
                    this.filteredFlightRes.sort(function (a, b) {
                        return item.isAscending ? moment(a.groupOfFlights[0].flightDetails[a.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[b.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm') : moment(b.groupOfFlights[0].flightDetails[b.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm') - new moment(a.groupOfFlights[0].flightDetails[a.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm');
                    });
                    break;

                default:
                    break;
            }

            this.flightResBind = this.filteredFlightRes.slice(0, this.currentScrollLimit);
        },
        scroll: function () {
            window.onscroll = () => {
                var bottomOfWindow = document.documentElement.scrollTop + window.innerHeight > document.documentElement.offsetHeight - 1500;
                if (bottomOfWindow) {
                    if (this.filteredFlightRes.length > 10) {
                        for (var index = this.currentScrollLimit; index < this.currentScrollLimit + 10; index++) {
                            if (this.filteredFlightRes[index]) {
                                this.flightResBind.push(this.filteredFlightRes[index]);
                            }
                        }
                        this.currentScrollLimit += 10;
                    }
                }
                $('[data-toggle="tooltip"]').tooltip();
            };
        },
        getSeatCount: function (flightDet) {
            var svgIc = '<svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="44.119675mm" height="48.810017mm" viewBox="0 0 44.119675 48.810017" version="1.1" id="svg8" inkscape:version="0.92.3 (2405546, 2018-03-11)"> <defs id="defs2" /> <sodipodi:namedview id="base" bordercolor="#666666" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="2.8" inkscape:cx="73.414515" inkscape:cy="109.95125" inkscape:document-units="mm" inkscape:current-layer="layer1" showgrid="false" inkscape:window-width="1920" inkscape:window-height="1017" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" fit-margin-top="0" fit-margin-left="0" fit-margin-right="0" fit-margin-bottom="0" /> <metadata id="metadata5"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(-81.116032,-108.43448)"> <path style="opacity:1;fill-opacity:1;stroke:none;stroke-width:0.99999994;stroke-opacity:1" d="m 331.50195,409.83398 c -3.38257,-0.0635 -12.32813,0.93359 -19.35937,1.79297 -8.03572,0.98215 -5,8.03516 -5,8.03516 l 19.28515,57.85742 c 0,0 1.34097,3.39202 4.64454,12.05274 3.30355,8.66071 3.39257,15.98242 3.39257,15.98242 0,0 -0.71455,44.19782 -0.35742,57.41211 C 334.46457,576.18108 345,575.55469 345,575.55469 c 0,0 112.50055,-0.0879 121.33984,-0.0879 8.8393,0 6.78516,-8.92969 6.78516,-8.92969 0,0 -2.58927,-17.14177 -3.75,-25.44531 -1.16069,-8.30355 -8.21484,-8.66211 -8.21484,-8.66211 0,0 -6.33958,-1.24889 -12.23243,-2.32031 -5.89285,-1.07146 -8.30273,-7.32227 -8.30273,-7.32227 0,0 -7.41014,-15.89174 -12.41016,-27.32031 -4.99997,-11.42858 -14.82226,-10.89453 -14.82226,-10.89453 0,0 -29.99916,0.002 -39.55274,0.26953 -9.55356,0.26785 -12.76757,-9.55469 -12.76757,-9.55469 0,0 -21.42941,-54.99971 -22.94727,-59.01758 -1.51786,-4.01786 -3.66016,-5.8934 -5.53516,-6.33984 -0.23437,-0.0558 -0.60466,-0.0866 -1.08789,-0.0957 z m 19.02149,1.91797 c -5.93464,0.37879 -6.94532,0.25196 -6.94532,0.25196 l 16.91993,43.18359 6.1875,-2.9043 c 0,0 4.92372,-0.37834 2.01953,-8.20703 -2.90419,-7.82868 -9.5957,-26.26367 -9.5957,-26.26367 0,0 -2.65129,-6.43937 -8.58594,-6.06055 z m 86.61914,86.61914 6.0625,13.63672 h 16.28711 c 0,0 2.65294,0.50691 2.40039,-2.90234 -0.25247,-3.40929 -0.25196,-8.20899 -0.25196,-8.20899 0,0 0.25165,-2.27216 -3.03124,-2.39843 -3.28302,-0.12624 -21.46485,-0.12696 -21.46485,-0.12696 z m 28.30469,82.8086 -119.375,0.53711 -0.35743,-0.35742 v 7.41015 c 0,6.07143 5.35743,5.44727 5.35743,5.44727 0,0 103.48104,-0.0905 109.19531,0.0879 5.7143,0.17858 5.17969,-4.28516 5.17969,-4.28516 z" transform="scale(0.26458333)" id="path826" inkscape:connector-curvature="0" /> </g> </svg>';
            var numSeats = Math.min.apply(Math, flightDet.map(function (o) {
                return o.fInfo.avlStatus;
            }))
            if (numSeats > 0) {
                if (numSeats <= 2) {
                    return svgIc + ' <p style="color:red;">' + numSeats + ' Seats left' + '</p>';
                }
                if (numSeats > 2 && numSeats <= 4) {
                    return svgIc + ' <p style="color:orange;">' + numSeats + ' Seats left' + '</p>';
                } else {
                    return svgIc + ' <p style="color:green;">' + numSeats + ' Seats left' + '</p>';
                }
            }
        },
        showOnly: function (type, val) {
            switch (type) {
                case 1:
                    this.checkStops = [val];
                    break;
                case 2:
                    this.checkAirlines = [val];
                    if (val.air == "Multi-Airline") {
                        this.filterType = 2;
                    } else {
                        this.filterType = 1;
                    }
                    break;
                default:
                    break;
            }
        },
        showAll: function (type) {
            switch (type) {
                case 1:
                    this.checkStops = [];
                    break;
                case 2:
                    this.checkAirlines = [];
                    break;
                case 3:
                    this.selDeptLoc = [];
                    break;
                case 4:
                    this.selArrtLoc = [];
                    break;
                default:
                    break;
            }
        },
        checkRedEye: function (flightDet) {
            var redE = false;
            for (let index = 0; index < flightDet.length; index++) {
                var fromD = flightDet[index].fInfo.dateTime.depTime.substring(0, 2);
                if (fromD >= 00 && fromD <= 05) {
                    redE = true;
                    break
                }
            }
            return redE;
        },
    },
    mounted: function () {
        var vm = this;

        this.$nextTick(function () {
            vm.scroll();
        });

        this.$watch(function (vmWatch) {
                return vmWatch.lowestPrice, vmWatch.highestPrice, vmWatch.checkStops, vmWatch.checkAirlines, vmWatch.checkAirlinesMatrix, vmWatch.selDeptLoc, vmWatch.selArrtLoc, Date.now();
            },
            function () {
                var temp = [];
                if (this.allFlightRes.length > 0) {
                    temp = this.allFlightRes;

                    // if (this.resMinPrice != this.changedMinPrice || this.resMaxPrice != this.changedMaxPrice) {
                    //     var tempchangedMinPrice = this.changedMinPrice;
                    //     var changedMaxPrice = this.changedMaxPrice;

                    //     temp = temp.filter(function (res) {
                    //         var takeLeg = false;
                    //         if (res.fare.amount >= tempchangedMinPrice && res.fare.amount <= changedMaxPrice) {
                    //             takeLeg = true;
                    //         }
                    //         return takeLeg;
                    //     })
                    // }
                    if(this.tripType.toLowerCase() == 'r') {
                        temp = _.filter(this.allFlightRes, function(e){
                            return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarDepartureDate 
                            && e.groupOfFlights[1].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarReturnDate;
                        });
                    } else {
                        temp = _.filter(this.allFlightRes, function(e){
                            return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarDepartureDate;
                        });
                    }
                    temp = temp.filter(function (res) {
                        return parseFloat(res.fare.amount) + 10 >= parseInt(i18n.n(parseInt(vm.lowestPrice) * vm.CurrencyMultiplier, 'currency', vm.selectedCurrency).match(/\d+(\.\d+)?/g).join('')) &&
                            parseInt(res.fare.amount) <= parseFloat(i18n.n(vm.highestPrice * vm.CurrencyMultiplier, 'currency', vm.selectedCurrency).match(/\d+(\.\d+)?/g).join('')) + 10
                    });

                    if (this.checkStops.length > 0) {
                        vm.showStopReset = true
                        temp = temp.filter(function (res) {
                            var takeLeg = true;
                            res.groupOfFlights.forEach(leg => {
                                if (vm.checkStops.length == 1 && vm.checkStops[0] != 99) {
                                    leg.flightDetails.forEach(seg => {
                                        if (vm.checkStops.indexOf(leg.flightDetails.length - 1) == -1 || seg.fInfo.stopQuantity > 0) {
                                            takeLeg = false;
                                        }
                                    });
                                }else if (vm.checkStops.length == 1 && vm.checkStops[0] == 99) {
                                    leg.flightDetails.forEach(seg => {
                                        if (seg.fInfo.stopQuantity == 0) {
                                            takeLeg = false;
                                        }
                                    });
                                } else if (vm.checkStops.length > 1) {
                                    leg.flightDetails.forEach(seg => {
                                        if (vm.checkStops.indexOf(leg.flightDetails.length - 1) == -1 || 
                                        (vm.checkStops.indexOf(99) == -1 && seg.fInfo.stopQuantity)
                                        ) {
                                            takeLeg = false;
                                        }
                                    });
                                }
                            });
                            return takeLeg;
                        })
                    } else {
                        vm.showStopReset = false
                    }

                    if (this.checkAirlines.length > 0) {
                        if (this.checkAirlines[0].air == "Multi-Airline") {
                            this.filterType = 2;
                        } else {
                            this.filterType = 1;
                        }
                        this.checkAirlinesMatrix = "";
                        vm.showAirReset = true
                        var selAirlines = this.checkAirlines;
                        temp = temp.filter(function (res) {
                            var takeLeg = false;
                            res.groupOfFlights.forEach(leg => {
                                if (vm.filterType == 1) {
                                    selAirlines.forEach(air => {
                                        if (leg.flightDetails.every((val, i, arr) => val.fInfo.companyId.mCarrier === arr[0].fInfo.companyId.mCarrier)) {
                                            if (air.air === leg.flightDetails[0].fInfo.companyId.mCarrier) {
                                                takeLeg = true;
                                            }
                                        }
                                    });
                                } else {
                                    if (!leg.flightDetails.every((val, i, arr) => val.fInfo.companyId.mCarrier === arr[0].fInfo.companyId.mCarrier)) {
                                        takeLeg = true;
                                    }
                                }
                            });
                            return takeLeg;
                        })
                    } else {
                        vm.showAirReset = false
                    }

                    if (this.checkAirlinesMatrix.length > 0) {

                        if (this.checkAirlinesMatrix == "Multi-Airline") {
                            this.filterType = 2;
                        } else {
                            this.filterType = 1;
                        }
                        var selAirlines = this.checkAirlinesMatrix;
                        temp = temp.filter(function (res) {
                            var takeLeg = false;
                            res.groupOfFlights.forEach(leg => {
                                if (vm.filterType == 1) {
                                    if (leg.flightDetails.every((val, i, arr) => val.fInfo.companyId.mCarrier === arr[0].fInfo.companyId.mCarrier)) {
                                        if (selAirlines === leg.flightDetails[0].fInfo.companyId.mCarrier) {
                                            takeLeg = true;
                                        }
                                    }
                                } else {
                                    if (!leg.flightDetails.every((val, i, arr) => val.fInfo.companyId.mCarrier === arr[0].fInfo.companyId.mCarrier)) {
                                        takeLeg = true;
                                    }
                                }
                            });
                            return takeLeg;
                        })
                    }

                    if (this.selDeptLoc.length > 0) {
                        vm.showDepLocReset = true
                        var selDeptLoc = this.selDeptLoc;
                        temp = temp.filter(function (res) {
                            var takeLeg = false;
                            res.groupOfFlights.forEach(leg => {
                                selDeptLoc.forEach(deploc => {
                                    var depLocSplit = deploc.split('-');
                                    if (depLocSplit[0] === leg.flightDetails[0].fInfo.location.locationFrom) {
                                        var dTime = parseInt(leg.flightDetails[0].fInfo.dateTime.depTime.substring(0, 2))
                                        if (dTime >= parseInt(depLocSplit[1]) && dTime <= parseInt(depLocSplit[2])) {
                                            takeLeg = true;
                                        }
                                    }
                                });
                            });
                            return takeLeg;
                        })
                    } else {
                        vm.showDepLocReset = false
                    }

                    if (this.selArrtLoc.length > 0) {
                        vm.showArrLocReset = true
                        var selArrtLoc = this.selArrtLoc;
                        temp = temp.filter(function (res) {
                            var takeLeg = false;
                            res.groupOfFlights.forEach(leg => {
                                selArrtLoc.forEach(arrloc => {
                                    var arrLocSplit = arrloc.split('-');
                                    if (arrLocSplit[0] === leg.flightDetails[leg.flightDetails.length - 1].fInfo.location.locationTo) {
                                        var arrTime = parseInt(leg.flightDetails[leg.flightDetails.length - 1].fInfo.dateTime.arrTime.substring(0, 2))
                                        if (arrTime >= parseInt(arrLocSplit[1]) && arrTime <= parseInt(arrLocSplit[2])) {
                                            takeLeg = true;
                                        }
                                    }
                                });
                            });
                            return takeLeg;
                        })
                    } else {
                        vm.showArrLocReset = false
                    }

                    this.totalResCount = temp.length;
                    //window.scrollTo(0, 0);
                }
                setTimeout(() => {
                    $("html, body").animate({ scrollTop: $("#airline_matrix").offset().top }, "slow");
                }, 300);
                vm.filteredFlightRes = temp;
                vm.flightResBind = vm.filteredFlightRes.slice(0, vm.currentScrollLimit);
            }
        )
    },
    computed: {
        // flightResBind: function () {
        //     var temp = [];
        //     if (this.allFlightRes.length > 0) {
        //         temp = this.allFlightRes;

        //         if (this.resMinPrice != this.changedMinPrice || this.resMaxPrice != this.changedMaxPrice) {
        //             var tempchangedMinPrice = this.changedMinPrice;
        //             var changedMaxPrice = this.changedMaxPrice;

        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 if (res.fare.amount >= tempchangedMinPrice && res.fare.amount <= changedMaxPrice) {
        //                     takeLeg = true;
        //                 }
        //                 return takeLeg;
        //             })
        //         }

        //         if (this.checkStops.length > 0) {
        //             var selStops = this.checkStops;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     selStops.forEach(stop => {
        //                         if (stop === leg.flightDetails.length - 1) {
        //                             takeLeg = true;
        //                         }
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //         }

        //         if (this.checkAirlines.length > 0) {
        //             var selAirlines = this.checkAirlines;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     leg.flightDetails.forEach(seg => {
        //                         selAirlines.forEach(air => {
        //                             if (air.air === seg.fInfo.companyId.mCarrier) {
        //                                 takeLeg = true;
        //                             }
        //                         });
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //         }

        //         if (this.checkAirlinesMatrix.length > 0) {
        //             var selAirlines = this.checkAirlinesMatrix;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     leg.flightDetails.forEach(seg => {
        //                         if (selAirlines === seg.fInfo.companyId.mCarrier) {
        //                             takeLeg = true;
        //                         }
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //         }

        //         if (this.selDeptLoc.length > 0 && this.deptLocFiltrChanged != 2) {
        //             var selDeptLoc = this.selDeptLoc;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     selDeptLoc.forEach(deploc => {
        //                         var depLocSplit = deploc.split('-');
        //                         if (depLocSplit[0] === leg.flightDetails[0].fInfo.location.locationFrom) {
        //                             var dTime = parseInt(leg.flightDetails[0].fInfo.dateTime.depTime.substring(0, 2))
        //                             if (dTime >= parseInt(depLocSplit[1]) && dTime <= parseInt(depLocSplit[2])) {
        //                                 takeLeg = true;
        //                             }
        //                         }
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //             this.deptLocFiltrChanged = 0;
        //         }

        //         if (this.selArrtLoc.length > 0 && this.arrLocFiltrChanged != 2) {
        //             var selArrtLoc = this.selArrtLoc;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     selArrtLoc.forEach(arrloc => {
        //                         var arrLocSplit = arrloc.split('-');
        //                         if (arrLocSplit[0] === leg.flightDetails[leg.flightDetails.length - 1].fInfo.location.locationTo) {
        //                             var arrTime = parseInt(leg.flightDetails[leg.flightDetails.length - 1].fInfo.dateTime.arrTime.substring(0, 2))
        //                             if (arrTime >= parseInt(arrLocSplit[1]) && arrTime <= parseInt(arrLocSplit[2])) {
        //                                 takeLeg = true;
        //                             }
        //                         }
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //             this.arrLocFiltrChanged = 0;
        //         }

        //         if (this.SorPriL) {
        //             temp.sort(function (a, b) {
        //                 return parseFloat(a.fare.amount) - parseFloat(b.fare.amount);
        //             });
        //             //temp = _.sortBy(temp, 'fare.amount');
        //         }

        //         if (this.SorPriH) {
        //             temp.sort(function (a, b) {
        //                 return parseFloat(b.fare.amount) - parseFloat(a.fare.amount);
        //             });
        //             //temp = _.sortBy(temp, 'fare.amount').reverse();
        //         }

        //         if (this.SorDuraL) {
        //             temp.sort(function (a, b) {
        //                 return parseInt(a.groupOfFlights[0].flightProposal.elapse) - parseInt(b.groupOfFlights[0].flightProposal.elapse)
        //                 //return moment(a.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm')
        //             });
        //         }

        //         if (this.SorDuraH) {
        //             temp.sort(function (a, b) {
        //                 return parseInt(a.groupOfFlights[0].flightProposal.elapse) - parseInt(b.groupOfFlights[0].flightProposal.elapse)
        //                 //return moment(a.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm')
        //             }).reverse();
        //         }

        //         if (this.SorDepL) {
        //             temp.sort(function (a, b) {
        //                 return moment(a.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm')
        //             });
        //         }

        //         if (this.SorDepH) {
        //             temp.sort(function (a, b) {
        //                 return moment(a.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm')
        //             }).reverse();
        //         }

        //         if (this.SorArrL) {
        //             temp.sort(function (a, b) {
        //                 return moment(a.groupOfFlights[0].flightDetails[a.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[b.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm')
        //             });
        //         }

        //         if (this.SorArrH) {
        //             temp.sort(function (a, b) {
        //                 return moment(a.groupOfFlights[0].flightDetails[a.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[b.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm')
        //             }).reverse();
        //         }

        //         this.totalResCount = temp.length;
        //         //window.scrollTo(0, 0);
        //     }

        //     return temp
        // }
    },
    filters: {
        momCommon: function (date, frmFormat, toFormat) {
            return moment(date, frmFormat).format(toFormat);
        }
    },
    watch: {
        "CurrencyMultiplier": function () {

            var vm = this;
            vm.forCurrencyUpdatesOnly = true;

            var slider = document.getElementById("price-slider");

            var lowestPrice = Math.min.apply(Math, vm.allFlightRes.map(function (o) {
                return o.fare.amount;
            }))
            var highestPrice = Math.max.apply(Math, vm.allFlightRes.map(function (o) {
                return o.fare.amount;
            }))

            lowestPrice = i18n.n(lowestPrice / this.CurrencyMultiplier, 'currency', this.selectedCurrency).match(/\d+(\.\d+)?/g).join('');
            highestPrice = i18n.n(highestPrice / this.CurrencyMultiplier, 'currency', this.selectedCurrency).match(/\d+(\.\d+)?/g).join('');
            if (lowestPrice == highestPrice) {
                highestPrice = highestPrice + 1;
            }
            vm.filteredFlightRes = vm.allFlightRes;
            vm.flightResBind = vm.filteredFlightRes;


            $("#minPricevalue").text(lowestPrice);
            $("#MaxPricevalue").text(highestPrice);

            slider.noUiSlider.updateOptions({
                tooltips: [true, true],
                start: [parseFloat(lowestPrice), parseFloat(highestPrice)],
                range: {
                    'min': parseFloat(lowestPrice),
                    'max': parseFloat(highestPrice)
                }
            });
        },
    },
});

var searchHeader = new Vue({
    i18n,
    el: '#vueHeader',
    name: 'flResultHeader',
    data: {
        Class: '',
        Traveller: '',
        Modify_Search: '',
        From_City: '',
        To_City: '',
        Departure_Date: '',
        Return_Date: '',
        Travellers: '',
        Adults: '',
        Children: '',
        Infant: '',
        Economy: '',
        Business: '',
        First_Class: '',
        Search: '',
        Weather: '',
        Budget: '',
        Stops: '',
        Airlines: '',
        Departure_Time: '',
        Arrival_Time: '',
        Sort_by: '',
        Price: '',
        Duration: '',
        Flights_Found: '',
        Adult: '',
        Child: '',
        Departure: '',
        Return: '',
        Flight_Details: '',
        Fare_Rules: '',
        Flight_Itinerary: '',
        Baggage_Information: '',
        Fare_Breakup: '',
        Flight_From: '',
        To: '',
        Layover_at: '',
        Check_In: '',
        Cabin: '',
        Fare: '',
        Taxes_and_Fees: '',
        Total: '',
        Book_Now: '',
        Refundable: '',
        Infants: '',
        Non_Refundable: '',
        Refundable_with_Penalty: '',
        Terminal: '',
        Free: '',
        Before_Departure: '',
        After_Departure: '',
        Change_Search_Query: '',
        Search_Again: '',
        Sorry_no_results: '',
        Via: '',
        From: '',
        Dates: '',
        Oneway: '',
        RoundTrip: '',
        MultiCity: '',
        Stop: '',
        Non_Stop: ''
    },
    created() {
        this.getPagecontent();
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + commonAgencyCode + '/Template/Flight Result/Flight Result/Flight Result.ftl';
                axios.get(aboutUsPage, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (response.data.area_List.length > 0) {
                        var mainComp = response.data.area_List[0].main;
                        self.Class = self.pluckcom('Class', mainComp.component);
                        self.Traveller = self.pluckcom('Traveller', mainComp.component);
                        self.Modify_Search = self.pluckcom('Modify_Search', mainComp.component);
                        self.From_City = self.pluckcom('From_City', mainComp.component);
                        self.To_City = self.pluckcom('To_City', mainComp.component);
                        self.Departure_Date = self.pluckcom('Departure_Date', mainComp.component);
                        self.Return_Date = self.pluckcom('Return_Date', mainComp.component);
                        self.Travellers = self.pluckcom('Travellers', mainComp.component);
                        self.Adults = self.pluckcom('Adults', mainComp.component);
                        self.Children = self.pluckcom('Children', mainComp.component);
                        self.Infant = self.pluckcom('Infant', mainComp.component);
                        self.Economy = self.pluckcom('Economy', mainComp.component);
                        self.Business = self.pluckcom('Business', mainComp.component);
                        self.First_Class = self.pluckcom('First_Class', mainComp.component);
                        self.Search = self.pluckcom('Search', mainComp.component);
                        self.Weather = self.pluckcom('Weather', mainComp.component);
                        self.Budget = self.pluckcom('Budget', mainComp.component);
                        self.Stops = self.pluckcom('Stops', mainComp.component);
                        self.Airlines = self.pluckcom('Airlines', mainComp.component);
                        self.Departure_Time = self.pluckcom('Departure_Time', mainComp.component);
                        self.Arrival_Time = self.pluckcom('Arrival_Time', mainComp.component);
                        self.Sort_by = self.pluckcom('Sort_by', mainComp.component);
                        self.Price = self.pluckcom('Price', mainComp.component);
                        self.Duration = self.pluckcom('Duration', mainComp.component);
                        self.Flights_Found = self.pluckcom('Flights_Found', mainComp.component);
                        self.Adult = self.pluckcom('Adult', mainComp.component);
                        self.Child = self.pluckcom('Child', mainComp.component);
                        self.Departure = self.pluckcom('Departure', mainComp.component);
                        self.Return = self.pluckcom('Return', mainComp.component);
                        self.Flight_Details = self.pluckcom('Flight_Details', mainComp.component);
                        self.Fare_Rules = self.pluckcom('Fare_Rules', mainComp.component);
                        self.Flight_Itinerary = self.pluckcom('Flight_Itinerary', mainComp.component);
                        self.Baggage_Information = self.pluckcom('Baggage_Information', mainComp.component);
                        self.Fare_Breakup = self.pluckcom('Fare_Breakup', mainComp.component);
                        self.Flight_From = self.pluckcom('Flight_From', mainComp.component);
                        self.To = self.pluckcom('To', mainComp.component);
                        self.Layover_at = self.pluckcom('Layover_at', mainComp.component);
                        self.Check_In = self.pluckcom('Check_In', mainComp.component);
                        self.Cabin = self.pluckcom('Cabin', mainComp.component);
                        self.Fare = self.pluckcom('Fare', mainComp.component);
                        self.Taxes_and_Fees = self.pluckcom('Taxes_and_Fees', mainComp.component);
                        self.Total = self.pluckcom('Total', mainComp.component);
                        self.Book_Now = self.pluckcom('Book_Now', mainComp.component);
                        self.Refundable = self.pluckcom('Refundable', mainComp.component);
                        self.Infants = self.pluckcom('Infants', mainComp.component);
                        self.Non_Refundable = self.pluckcom('Non_Refundable', mainComp.component);
                        self.Refundable_with_Penalty = self.pluckcom('Refundable_with_Penalty', mainComp.component);
                        self.Terminal = self.pluckcom('Terminal', mainComp.component);
                        self.Free = self.pluckcom('Free', mainComp.component);
                        self.Before_Departure = self.pluckcom('Before_Departure', mainComp.component);
                        self.After_Departure = self.pluckcom('After_Departure', mainComp.component);
                        self.Change_Search_Query = self.pluckcom('Change_Search_Query', mainComp.component);
                        self.Search_Again = self.pluckcom('Search_Again', mainComp.component);
                        self.Sorry_no_results = self.pluckcom('Sorry_no_results', mainComp.component);
                        self.Via = self.pluckcom('Via', mainComp.component);
                        self.From = self.pluckcom('From', mainComp.component);
                        self.Dates = self.pluckcom('Dates', mainComp.component);
                        self.Oneway = self.pluckcom('Oneway', mainComp.component);
                        self.RoundTrip = self.pluckcom('RoundTrip', mainComp.component);
                        self.MultiCity = self.pluckcom('MultiCity', mainComp.component);
                        self.Stop = self.pluckcom('Stop', mainComp.component);
                        self.Non_Stop = self.pluckcom('Non_Stop', mainComp.component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        getmoreinfo(url) {

            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url = "/Nirvana/book-now.html?page=" + url;
            } else {
                url = "#";
            }
            return url;
        }
    },
    computed: {
        // flightResBind: function () {
        //     var temp = [];
        //     if (this.allFlightRes.length > 0) {
        //         temp = this.allFlightRes;

        //         if (this.resMinPrice != this.changedMinPrice || this.resMaxPrice != this.changedMaxPrice) {
        //             var tempchangedMinPrice = this.changedMinPrice;
        //             var changedMaxPrice = this.changedMaxPrice;

        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 if (res.fare.amount >= tempchangedMinPrice && res.fare.amount <= changedMaxPrice) {
        //                     takeLeg = true;
        //                 }
        //                 return takeLeg;
        //             })
        //         }

        //         if (this.checkStops.length > 0) {
        //             var selStops = this.checkStops;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     selStops.forEach(stop => {
        //                         if (stop === leg.flightDetails.length - 1) {
        //                             takeLeg = true;
        //                         }
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //         }

        //         if (this.checkAirlines.length > 0) {
        //             var selAirlines = this.checkAirlines;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     leg.flightDetails.forEach(seg => {
        //                         selAirlines.forEach(air => {
        //                             if (air.air === seg.fInfo.companyId.mCarrier) {
        //                                 takeLeg = true;
        //                             }
        //                         });
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //         }

        //         if (this.checkAirlinesMatrix.length > 0) {
        //             var selAirlines = this.checkAirlinesMatrix;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     leg.flightDetails.forEach(seg => {
        //                         if (selAirlines === seg.fInfo.companyId.mCarrier) {
        //                             takeLeg = true;
        //                         }
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //         }

        //         if (this.selDeptLoc.length > 0 && this.deptLocFiltrChanged != 2) {
        //             var selDeptLoc = this.selDeptLoc;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     selDeptLoc.forEach(deploc => {
        //                         var depLocSplit = deploc.split('-');
        //                         if (depLocSplit[0] === leg.flightDetails[0].fInfo.location.locationFrom) {
        //                             var dTime = parseInt(leg.flightDetails[0].fInfo.dateTime.depTime.substring(0, 2))
        //                             if (dTime >= parseInt(depLocSplit[1]) && dTime <= parseInt(depLocSplit[2])) {
        //                                 takeLeg = true;
        //                             }
        //                         }
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //             this.deptLocFiltrChanged = 0;
        //         }

        //         if (this.selArrtLoc.length > 0 && this.arrLocFiltrChanged != 2) {
        //             var selArrtLoc = this.selArrtLoc;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     selArrtLoc.forEach(arrloc => {
        //                         var arrLocSplit = arrloc.split('-');
        //                         if (arrLocSplit[0] === leg.flightDetails[leg.flightDetails.length - 1].fInfo.location.locationTo) {
        //                             var arrTime = parseInt(leg.flightDetails[leg.flightDetails.length - 1].fInfo.dateTime.arrTime.substring(0, 2))
        //                             if (arrTime >= parseInt(arrLocSplit[1]) && arrTime <= parseInt(arrLocSplit[2])) {
        //                                 takeLeg = true;
        //                             }
        //                         }
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //             this.arrLocFiltrChanged = 0;
        //         }

        //         if (this.SorPriL) {
        //             temp.sort(function (a, b) {
        //                 return parseFloat(a.fare.amount) - parseFloat(b.fare.amount);
        //             });
        //             //temp = _.sortBy(temp, 'fare.amount');
        //         }

        //         if (this.SorPriH) {
        //             temp.sort(function (a, b) {
        //                 return parseFloat(b.fare.amount) - parseFloat(a.fare.amount);
        //             });
        //             //temp = _.sortBy(temp, 'fare.amount').reverse();
        //         }

        //         if (this.SorDuraL) {
        //             temp.sort(function (a, b) {
        //                 return parseInt(a.groupOfFlights[0].flightProposal.elapse) - parseInt(b.groupOfFlights[0].flightProposal.elapse)
        //                 //return moment(a.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm')
        //             });
        //         }

        //         if (this.SorDuraH) {
        //             temp.sort(function (a, b) {
        //                 return parseInt(a.groupOfFlights[0].flightProposal.elapse) - parseInt(b.groupOfFlights[0].flightProposal.elapse)
        //                 //return moment(a.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm')
        //             }).reverse();
        //         }

        //         if (this.SorDepL) {
        //             temp.sort(function (a, b) {
        //                 return moment(a.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm')
        //             });
        //         }

        //         if (this.SorDepH) {
        //             temp.sort(function (a, b) {
        //                 return moment(a.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm')
        //             }).reverse();
        //         }

        //         if (this.SorArrL) {
        //             temp.sort(function (a, b) {
        //                 return moment(a.groupOfFlights[0].flightDetails[a.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[b.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm')
        //             });
        //         }

        //         if (this.SorArrH) {
        //             temp.sort(function (a, b) {
        //                 return moment(a.groupOfFlights[0].flightDetails[a.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[b.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm')
        //             }).reverse();
        //         }

        //         this.totalResCount = temp.length;
        //         //window.scrollTo(0, 0);
        //     }

        //     return temp
        // }
    },
    filters: {
        momCommon: function (date, frmFormat, toFormat) {
            return moment(date, frmFormat).format(toFormat);
        }
    }
});

function getSearchData() {
    var urldata = getUrlVars().flight;
    if (urldata == undefined) {
        window.location.href = 'index.html';
    }
    return createflightSearchRequest(urldata.substring(1, urldata.length).split('/'));
}

function getCabinClassName(cabinCode) {
    switch (cabinCode.toUpperCase()) {
        case 'F':
            return "First Class"
            break;
        case 'C':
            return "Business"
            break;
        default:
            return "Economy"
            break;
    }
}

function createflightSearchRequest(tripLeg) {
    var originDestinationInformationArr = [];
    for (var i = 0; i < tripLeg.length - 1; i++) {
        var Trip = tripLeg[i];
        var TripFrom = Trip.split('-')[0].toUpperCase();;
        var TripTo = Trip.split('-')[1].toUpperCase();;
        var TripDate = Trip.split('-')[2];
        var TripArray = {
            departureDate: moment(TripDate, 'DD|MM|YYYY').format("DD-MM-YYYY"),
            originLocation: TripFrom,
            destinationLocation: TripTo,
            radiusInformation: {
                _FromValue: '0',
                _ToValue: '250'
            }
        }
        originDestinationInformationArr.push(TripArray);
    }
    var triptypes = tripLeg[tripLeg.length - 1];
    var totalADT = triptypes.split('-')[0];
    var totalCHD = triptypes.split('-')[1];
    var totalINF = triptypes.split('-')[2];
    var tripClass = triptypes.split('-')[3];
    if (tripClass.toLowerCase() == 'all') {
        tripClass = 'All';
    } else {
        tripClass = tripClass.toUpperCase();
    }
    var tripTyppee = triptypes.split('-')[7];
    var totalpax = parseInt(totalADT) + parseInt(totalCHD) + parseInt(totalINF);
    var supplierCode = triptypes.split('-')[4];
    if (supplierCode == 'all') {
        supplierCode = [];
    } else {
        supplierCode = supplierCode.split('|');
    }
    nmberOfRec = triptypes.split('-')[5];
    var filter = false;
    var isfiltered = triptypes.split('-')[6];
    if (isfiltered == 'T') {
        filter = true;
    }
    var stopQuantity = triptypes.split('-')[10];
    if (stopQuantity.toLowerCase() == "df") {
        stopQuantity = "Direct";
    } else {
        stopQuantity = "All";
    }
    var preferAirline = triptypes.split('-')[9];
    if (preferAirline == "" || preferAirline == undefined) {
        preferAirline = [];
    } else {
        preferAirline = preferAirline.split('|');
    }

    var airProv = [];
    var UserData = JSON.parse(localStorage.User);
    var supDetails = [];
    if (UserData.loginNode.servicesList != undefined) {
        supDetails = UserData.loginNode.servicesList.filter(function (service) {
            return service.name.toLowerCase().includes('air')
        });
        supDetails[0].provider.forEach(function (data) {
            airProv.push(data.id);
        });
    }


    var flightSearchRQ = {
        "request": {
            "service": "FlightRQ",
            "token": localStorage.access_token,
            "supplierCodes": airProv,
            "content": {
                "command": "FlightSearchRQ",
                "criteria": {
                    "criteriaType": "Air",
                    "commonRequestSearch": {
                        "numberOfUnits": parseInt(totalpax),
                        "typeOfUnit": "PX",
                        "numberOfRec": nmberOfRec,
                        "isFiltered": filter
                    },
                    "originDestinationInformation": originDestinationInformationArr,
                    "preferredAirline": preferAirline,
                    "nonStop": false,
                    cabin: tripClass,
                    "isRefundable": false,
                    "maxStopQuantity": stopQuantity,
                    "tripType": tripTyppee,
                    "preferenceLevel": "Preferred",
                    "target": "Test",
                    "passengerTypeQuantity": {
                        adt: parseInt(totalADT),
                        chd: parseInt(totalCHD),
                        inf: parseInt(totalINF)
                    },
                    "requestOption": null,
                    "pricingSource": null,
                    "dealcode": null
                },
                "sort": [{
                    "field": "price",
                    "order": "asc",
                    "sequence": 1
                }],
                "filter": {
                    "noOfResults": "250",
                    "from": 1,
                    "to": 100,
                    "sequence": 1
                },
                "supplierSpecific": {
                    "test": "test"
                }
            }
        }
    }

    return flightSearchRQ;
}

function axiosConfigHeader() {
    var axiosConfig = {
        headers: {
            'Authorization': "Bearer " + localStorage.access_token
        }
    };
    return axiosConfig;
}