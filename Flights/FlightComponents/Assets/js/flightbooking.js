﻿﻿var defaultCountry = 'AE';
var AirlinesDatasSorted = AirlinesDatas;
var commonAgencyCode = 'AGY435';
const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
function SortByName(a, b) {
    var aName = a.A.toLowerCase();
    var bName = b.A.toLowerCase();
    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}
AirlinesDatasSorted.sort(SortByName);

function axiosConfigHeader() {
    var axiosConfig = {
        headers: {
            'Authorization': "Bearer " + localStorage.access_token
        }
    };
    return axiosConfig;
}

var currentUser = getUser();
var currentSuppliers = getSuppliers('Air');
var currentPayGateways = getPayGateways();
var totalFare = 0;
var selectFlightFromSession = localStorage.selectedFlight;
var selectFlightFromSessionObj = JSON.parse(selectFlightFromSession);
var oldTotalfare = selectFlightFromSessionObj.selectedFlightInfo.fare.amount;
$.each(selectFlightFromSessionObj.selectedFlightInfo.groupOfFlights, function (grpindex, groupOfFlight) {
    groupOfFlight['segDirectionID'] = grpindex;
    if (groupOfFlight.flightDetails.length > 1) {
        groupOfFlight['stopCount'] = groupOfFlight.flightDetails.length;
        groupOfFlight['stopCountText'] = groupOfFlight.flightDetails.length.toString() + ' ';
    } else {
        groupOfFlight['stopCount'] = 0;
        groupOfFlight['stopCountText'] = 'Non-';
    }
    $.each(groupOfFlight.flightDetails, function (fltdetindex, flightDetail) {
        flightDetail.fInfo.location['origin'] = getAirport(flightDetail.fInfo.location.locationFrom);
        flightDetail.fInfo.location['fromTerminalValue'] = isNullorUndefined(flightDetail.fInfo.location.fromTerminal) ? 'Terminal N/A' : 'Terminal ' + flightDetail.fInfo.location.fromTerminal;
        flightDetail.fInfo.location['toTerminalValue'] = isNullorUndefined(flightDetail.fInfo.location.toTerminal) ? 'Terminal N/A' : 'Terminal ' + flightDetail.fInfo.location.toTerminal;
        flightDetail.fInfo.location['departure'] = getAirport(flightDetail.fInfo.location.locationTo);
        flightDetail.fInfo.dateTime['dateTimeFormatedDepDate'] = getdatetimeformated(flightDetail.fInfo.dateTime.depDate, 'DDMMYY', 'ddd, DD MMM YYYY');
        flightDetail.fInfo.dateTime['dateTimeFormatedDepTime'] = gettimeformated(flightDetail.fInfo.elapsedTime, 'HHmm', 'HH', 'h ', 'mm', 'm');
        flightDetail.fInfo.dateTime['dateTimeFormatedDepDateTimeDisplay'] = getdatetimeformated(flightDetail.fInfo.dateTime.depTime + flightDetail.fInfo.dateTime.depDate, 'HHmmDDMMYY', 'DD MMM, HH:mm');
        flightDetail.fInfo.dateTime['dateTimeFormatedArrDate'] = getdatetimeformated(flightDetail.fInfo.dateTime.arrDate, 'DDMMYY', 'ddd, DD MMM YYYY');
        flightDetail.fInfo.dateTime['dateTimeFormatedArrTime'] = gettimeformated(flightDetail.fInfo.elapsedTime, 'HHmm', 'HH', 'h ', 'mm', 'm');
        flightDetail.fInfo.dateTime['dateTimeFormatedArrDateTimeDisplay'] = getdatetimeformated(flightDetail.fInfo.dateTime.arrTime + flightDetail.fInfo.dateTime.arrDate, 'HHmmDDMMYY', 'DD MMM, HH:mm');
        flightDetail.fInfo.dateTime['calculateDatetimeDiff'] = calculateDatetimeDiff(flightDetail.fInfo.dateTime.depDate + flightDetail.fInfo.dateTime.depTime, 'DDMMYYHHmm', flightDetail.fInfo.dateTime.arrDate + flightDetail.fInfo.dateTime.arrTime, 'DDMMYYHHmm')
    });
});

selectFlightFromSessionObj.paxList = [];
var totalPaxUnites = 0;
var travllerIndex = 1;
var airRevalidate = selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate;
var adtcount = airRevalidate.adt;
var chdcount = airRevalidate.chd;
var infcount = airRevalidate.inf;
setPaxList('ADT', adtcount);
setPaxList('CHD', chdcount);
setPaxList('INF', infcount);
function setPaxList(paxType, count) {
    defaultCountry = JSON.parse(localStorage.User).loginNode.country.code;
    if (selectFlightFromSessionObj.paxList == undefined) {
        selectFlightFromSessionObj.paxList = [];
    }
    var paxAvailbleTitles = paxTitles.filter(function (title) { return jQuery.inArray(paxType.toUpperCase(), title.paxType) != -1 ? true : false; });
    var titleOptions = getTitleOptions(paxAvailbleTitles);
    for (var paxIndex = 1; paxIndex <= count;) {
        selectFlightFromSessionObj.paxList.push({
            paxType: getPaxType(paxType, 1),
            paxTypeCode: paxType,
            paxIndex: paxIndex++,
            travllerIndex: travllerIndex++,
            titleOptions: titleOptions,
            selected: titleOptions[0],
            nationalitycountry: {
                selected: defaultCountry,
                countries: countryList
            },
            issuecountry: {
                selected: defaultCountry,
                countries: countryList
            },
            AirlinesDatas: AirlinesDatasSorted,
            airlineselected: '',
        });
    }
}

var selectResponseObj = [];

function getdatetimeformated(datetime, inputFormat, outputFormat) {
    return moment(datetime, inputFormat).format(outputFormat);
}

function gettimeformated(time, inputFormat, houroutputFormat, houraddl, minoutputFormat, minaddl) {
    var formattedtime=null;
    if(time!=null){
    var hour = moment(time, inputFormat).format(houroutputFormat).toString();
    var min = moment(time, inputFormat).format(minoutputFormat).toString();
     formattedtime = hour + houraddl + min + minaddl;
    }
    return formattedtime;
}

function calculateDatetimeDiff(startDate, startDateFormat, endDate, endDateFormat) {
    var start_date = moment(startDate, startDateFormat);
    var end_date = moment(endDate, endDateFormat);
    var duration = moment.duration(end_date.diff(start_date));
    var hours = duration.hours();
    var mins = duration.minutes();
    return hours + 'h ' + mins + 'm';
}

function stringIsNullorEmpty(value) {
    var status = false;
    if (value == undefined || value == null || value == '') { status = true; }
    return status;
}

function isValidEmail(emailID) {
    var status = false;
    var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    var matchArray = emailID.match(emailPat);
    if (matchArray != null) { status = true; }
    return status;
}

function isNullorUndefined(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) { status = true; }
    return status;
}

function isNumber(value) {
    var status = false;
    if (Math.floor(value) == value && $.isNumeric(value)) { status = true }
    return status;
}

function isNullorEmpty(value) {
    var status = false;
    if (value == null || value == undefined || value == "undefined") { status = true; }
    if (!status && $.trim(value) == '') { status = true; }
    return status;
}

function isNullorEmptyToBlank(value, optval) {
    return isNullorEmpty(value) ? (isNullorEmpty(optval) ? '' : optval) : value;
}

function getAirport(airportCode) {
    var airport = null;
    try {
        airport = $.grep(this.AirlinesTimezone, function (value) {
            return (value.I.toUpperCase() == airportCode.toUpperCase());
        });
    } catch (err) { }
    if (isNullorUndefined(airport)) {
        airport = { label: airportCode.toUpperCase(), N: airportCode.toUpperCase(), C: airportCode.toUpperCase(), CN: airportCode.toUpperCase(), I: airportCode.toUpperCase(), T: "A", TZ: 0 };
    } else {
        airport = airport[0];
    }
    return airport;
}

function getAirline(airlineCode) {
    var airline;
    try {
        airline = $.grep(this.AirlinesDatas, function (value) {
            return (value.C.toUpperCase() == airlineCode.toUpperCase());
        });
    } catch (err) { }
    if (airline == null || airline == undefined || airline.length == 0) {
        airline = { "C": airlineCode.toUpperCase(), "A": airlineCode.toUpperCase(), "T": airlineCode.toUpperCase(), "S": "true" };
    } else {
        airline = airline[0];
    }
    return airline;
}

function getPaxType(paxType, paxCount) {
    var paxTypeText = 'Adult';
    switch (paxType) {
        case 'ADT':
            paxTypeText = 'Adult' + ((paxCount > 1) ? 's' : '');
            break;
        case 'CHD':
            paxTypeText = 'Child' + ((paxCount > 1) ? 'ren' : '');
            break;
        case 'INF':
            paxTypeText = 'Infant' + ((paxCount > 1) ? 's' : '');
            break;
    }
    return paxTypeText;
}

function getUnit(unit) {
    unit = unit.toLowerCase();
    var unitTypeText = 'KG';
    switch (unit) {
        case 'k':
        case 'kg':
        case 'w':
        case '700':
            unitTypeText = 'KG';
            break;
        case 'pc':
        case 'p':
        case 'n':
            unitTypeText = 'Piece';
            break;
        default:
            unitTypeText = 'KG';
            break;
    }
    return unitTypeText;
}

function getTitleOptions(paxAvailbleTitles) {
    var divOptions = [];
    $.each(paxAvailbleTitles, function (index, title) {
        divOptions.push(title.name);
    });
    return divOptions;
}

var Vue_FlightBook = new Vue({
    el: "#flightbookDiv",
    name:"bookinginfo",
    i18n,
    data: {
        content:[],
        AirlinesDatas: AirlinesDatas,
        AirlinesTimezone: AirlinesTimezone,
        selectedFlight: {
            content: {
                fareInformationWithoutPnrReply: {
                    miniRuleList: null,
                    airSegments: null,
                    paxList: [],
                }
            }
        },
        selectFlightFromSession: [],
        selectedCurrency: null,
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        Review_Flight: '',
        Traveller_Details: '',
        Flight_Booking: '',
        Payment: '',
        Confirmation: '',
        Log_out: '',
        Signin_ProcessLabel: '',
        Sign_In: '',
        Email_RequiredLabel: '',
        Password_RequiredLabel: '',
        Account_NotAvailable: '',
        Account_ProcessLabel: '',
        to: '',
        stop: '',
        Fare_Rules: '',
        Baggage: '',
        Refundable: '',
        Non_Refundable: '',
        Refundable_withPenalty: '',
        No_cancellation: '',
        Fare_Rule: '',
        Before_Departure: '',
        After_Departure: '',
        Allowed_for: '',
        Flight_from: '',
        Checked_Baggage: '',
        Free: '',
        Cabin_Baggage: '',
        No_baggage: '',
        Provide_TravellerDetails: '',
        NationalIdPassport_Warning: '',
        Required_field: '',
        Title_Label: '',
        FirstName_Label: '',
        LastName_Label: '',
        DoB_Label: '',
        Nationality_Label: '',
        PassportNumber_Label: '',
        ExpireDate_Label: '',
        IssueDate_Label: '',
        IssueCountry_Label: '',
        Frequentflyer_program: '',
        Frequentflyer_number: '',
        Email: '',
        FlightTicket_RegisteredEmail: '',
        PhoneNumber_Label: '',
        Traveller: '',
        Agree_WhatsApp: '',
        TripSummary_Detail: '',
        Tickets: '',
        Fare: '',
        Taxes_Fees: '',
        Service_Fee: '',
        Total: '',
        Taxesfees_included: '',
        Travellers: '',
        FareChange_Alert: '',
        Updated_Fare: '',
        Original_Fare: '',
        Cancel: '',
        Continue: '',
        Reviewyour_flight: '',
        Continue_ToPayment: '',
        close: '',
        Alert: '',
        Confirm: '',
        Message_UnexpectSupplierIssue: '',
        Provide_Email: '',
        Provide_PhoneNumber: '',
        Thank_Registerwithus: '',
        Email_AlreadyRegistered: '',
        Fields_Mandatory: '',
        Booking_failed: '',
        Max_TwentyChar: '',
        Username_required: '',
        Password_required: '',
        Login_failed: '',
        Password: '',
        FrequentNumber_Required: '',
        Welcome: '',
        Ok: '',
        TravellerName_Duplicated: '',
        GoBack: '',
        AddFrequent: '',
        checkAgrementValidation:'The Terms and Condition and Privacy policy field is required',
        agencyCode: localStorage.AgencyCode,
        fareRuleDiv: "",
        bookType: 0,
        isPaymentOnAgencyActive: false,
        pgList: getPayGateways(),
        pgEnable: false,
        summary:'',
        details:'',
        selectCredentialInsurance: null,
        insuranceData: null,
        toggleInsurance: false,
        selectedPlan: null,
        insuranceDataFiltered: null,
        viewInsurance: null,
        travellersInsurance: [],
        isUserHaveInsuranceBooking: false,
        Payment_Type_Label: '',
        Card_Label: '',
        Hold_Label: '',
        Disclaimer_Label: '',
        Terms_1_Label: '',
        Terms_2_Label: '',
        Terms_3_Label: '',
        Terms_4_Label: '',
        Terms_5_Label: '',
        Proceed_Label: '',
        Continue_Label: '',
        haveSmsNotification: false
    },
    created: function () {
        var userNode = JSON.parse(localStorage.User);
        if (userNode) {
            this.selectedCurrency = (localStorage.selectedCurrency) ? localStorage.selectedCurrency : userNode.loginNode.currency;
        } else {
            this.selectedCurrency = (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD';
        }
        try {
            var self = this;

            var huburl = ServiceUrls.hubConnection.baseUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var requrl = ServiceUrls.hubConnection.hubServices.flights.airSelect;
            this.selectFlightFromSession = selectFlightFromSessionObj;

            var PostData = selectFlightFromSessionObj.selectFlightRq;
            console.log(JSON.stringify(PostData));
            try {
                var b2cRoles = JSON.parse(localStorage.roleList).filter(function (role) { return role.id == 6; })[0] || {};
                if (!_.isEmpty(b2cRoles)){
                    var flightRoles = _.find(b2cRoles.moduleList, function (service) { return service.name.toLowerCase() == 'flight'; }) ||{};
                    if (!_.isEmpty(flightRoles)) {
                        var roleBookInsurance = _.find(flightRoles.functionalityList, function (e) { return e.id == 208 });
                        if (!_.isEmpty(roleBookInsurance)) {
                            this.isUserHaveInsuranceBooking = true;
                        }
                        var roleBookSms = _.find(flightRoles.functionalityList, function (e) { return e.id == 211 });
                        if (!_.isEmpty(roleBookSms)) {
                            this.haveSmsNotification = true;
                        }
                    }
                    if (this.isUserHaveInsuranceBooking) {
                        this.loadInsurance();
                    }
                }
            } catch (e) {}
            axios.post(huburl + portno + requrl, PostData, axiosConfigHeader()).then((response) => {
                localStorage.access_token = response.headers.access_token;
                this.initData(response.data);

           
                Vue_CoTraveller.setCoTravellers();
            }).catch((err) => {
                if (self.Alert == '') {
                    getPagecontent(function () {
                        alertify.alert(self.Alert, self.Message_UnexpectSupplierIssue).setting({ 'closable': false, onclose: function () { window.location = '/'; } }).set('label', self.Ok);
                    });
                } else {
                    alertify.alert(self.Alert, self.Message_UnexpectSupplierIssue).setting({ 'closable': false, onclose: function () { window.location = '/'; } }).set('label', self.Ok);
                }
            });
        } catch (err) {
            if (self.Alert == '') {
                getPagecontent(function () {
                    alertify.alert(self.Alert, self.Message_UnexpectSupplierIssue).setting({ 'closable': false, onclose: function () { window.location = '/'; } }.set('label', self.Ok));
                });
            } else {
                alertify.alert(self.Alert, self.Message_UnexpectSupplierIssue).setting({ 'closable': false, onclose: function () { window.location = '/'; } }.set('label', self.Ok));
            }
        }
    },
    methods: {
        isRefundableFlight: function (refundString) {
            return isRefundableFlight(refundString);
        },
        getFormattedDate: function (datevalue) {
            var datevaluesplit = datevalue.split('/');
            return new Date(datevaluesplit[2], parseInt(datevaluesplit[1]) - 1, datevaluesplit[0]);
        },
        getMaxDate: function (dateObject) {
            var dateObjectsplit = $(dateObject).attr('tag').split('_');
            var endDate = new Date();
            switch (dateObjectsplit[0]) {
                case 'dob':
                    adddays = -1;
                    endDate.setDate(endDate.getDate() + adddays);
                    switch (dateObjectsplit[1]) {
                        case 'ADT':
                            addyears = -12;
                            endDate.setFullYear(endDate.getFullYear() + addyears);
                            break;
                        case 'CHD':
                            // addyears = -2;
                            // endDate.setFullYear(endDate.getFullYear() + addyears);
                            endDate = new Date();
                            break;
                        case 'INF':
                            endDate = new Date();
                            break;
                    }
                    $(dateObject).datepicker('option', 'yearRange', "-100:+0");
                    $(dateObject).datepicker('option', 'maxDate', new Date(endDate));
                   
                   
                    break;
                case 'exd':
                    break;
                case 'isd':
                    adddays = -1;
                    endDate.setDate(endDate.getDate() + adddays);
                    $(dateObject).datepicker('option', 'maxDate', endDate);
                    break;
            }
            return endDate;
        },
        getMinDate: function (dateObject) {
            var dateObjectsplit = $(dateObject).attr('tag').split('_');
            var txtDob = $('#txtDob' + dateObjectsplit[1] + dateObjectsplit[2]);
            var txtExpireDate = $('#txtExpireDate' + dateObjectsplit[1] + dateObjectsplit[2]);
            var txtPassportIssue = $('#txtPassportIssue' + dateObjectsplit[1] + dateObjectsplit[2]);
            var startDate = new Date();
            switch (dateObjectsplit[0]) {
                case 'dob':
                    try {
                        var gLength = selectFlightFromSessionObj.selectedFlightInfo.groupOfFlights.length;
                        var fLength = selectFlightFromSessionObj.selectedFlightInfo.groupOfFlights[gLength - 1].flightDetails.length - 1;
                        startDate = new Date(selectFlightFromSessionObj.selectedFlightInfo.groupOfFlights[gLength - 1].flightDetails[fLength].fInfo.dateTime.dateTimeFormatedArrDate);
                    } catch (error) {
                        startDate = new Date();
                    }
                    switch (dateObjectsplit[1]) {
                        case 'ADT':
                            addyears = -120;
                            break;
                        case 'CHD':
                            addyears = -12;
                            break;
                        case 'INF':
                            addyears = -2;
                            break;
                    }
                    startDate.setFullYear(startDate.getFullYear() + addyears);
                    break;
                case 'exd':
                    switch (dateObjectsplit[1]) {
                        case 'ADT':
                            adddays = 1;
                            startDate.setDate(startDate.getDate() + adddays);
                            break;
                        case 'CHD':
                            adddays = 1;
                            startDate.setDate(startDate.getDate() + adddays);
                            break;
                        case 'INF':
                            adddays = 1;
                            startDate.setDate(startDate.getDate() + adddays);
                            break;
                    }
                    break;
                case 'isd':
                    switch (dateObjectsplit[1]) {
                        case 'ADT':
                            if (isNullorUndefined($(txtDob).val())) {
                                addyears = -100;
                                startDate.setFullYear(startDate.getFullYear() + addyears);
                            } else {
                                startDate = moment($(txtDob).val(), 'DD/MM/YYYY')._d;
                            }
                            break;
                        case 'CHD':
                            if (isNullorUndefined($(txtDob).val())) {
                                addyears = -12;
                                startDate.setFullYear(startDate.getFullYear() + addyears);
                            } else {
                                startDate = moment($(txtDob).val(), 'DD/MM/YYYY')._d;
                            }
                            break;
                        case 'INF':
                            if (isNullorUndefined($(txtDob).val())) {
                                addyears = -2;
                                startDate.setFullYear(startDate.getFullYear() + addyears);
                            } else {
                                startDate = moment($(txtDob).val(), 'DD/MM/YYYY')._d;
                            }
                            break;
                    }
                    break;
            }
            $(dateObject).datepicker('option', 'minDate', startDate);
            return startDate;
        },
        initData: function (response) {
            try {
                var self = this;
                selectResponseObj = response.response;
                totalFare = selectResponseObj.content.fareInformationWithoutPnrReply.flightFareDetails.totalFare;
                summary = selectResponseObj.content.fareInformationWithoutPnrReply.minirules;
                self.details = selectResponseObj.content.fareInformationWithoutPnrReply.airFareRule
                var SummaryLists = [];
                if(summary!=undefined && summary!=null && summary!=[]){
                    summary.forEach(rule => {
                        SummaryList = _.groupBy(rule.category, "cat");
                        SummaryLists.push(SummaryList);
                    });
                }    
                self.summary =SummaryLists;
                if (isNullorUndefined(totalFare)) {
                    alertify.alert(self.Alert, self.Message_UnexpectSupplierIssue).setting({ 'closable': false, onclose: function () { window.location = '/'; } }).set('label', self.Ok);
                }
                Vue_FareUpdate.resetValues();
                if (parseFloat(oldTotalfare) != parseFloat(totalFare)) {
                    selectFlightFromSessionObj.selectedFlightInfo.fare.amount = totalFare;
                    var updatedFare = parseFloat(totalFare);
                    var originalFare = parseFloat(oldTotalfare);
                    var difference = parseFloat(originalFare - updatedFare).toFixed(2);
                    Vue_FareUpdate.getPagecontent(function () {
                        Vue_FareUpdate.currency = localStorage.selectedCurrency;
                        Vue_FareUpdate.updatedFare = updatedFare.toString();
                        Vue_FareUpdate.originalFare = originalFare.toString();
                        Vue_FareUpdate.fareTitleText = (difference > 0) ? Vue_FareUpdate.Fare_Slashed : Vue_FareUpdate.Fare_Hiked;
                        Vue_FareUpdate.fareFooterText = (difference > 0) ? Vue_FareUpdate.You_saved : Vue_FareUpdate.Extra_amount;
                        Vue_FareUpdate.farecss = (difference > 0) ? 'fa fa-level-down' : 'fa fa-level-up';
                        Vue_FareUpdate.difference = ((difference > 0) ? difference : (difference * -1)).toString();
                        var amt = self.$n((Vue_FareUpdate.difference / Vue_FareUpdate.CurrencyMultiplier), 'currency', Vue_FareUpdate.currency);
                        Vue_FareUpdate.FareChanged_Message = (difference > 0) ? Vue_FareUpdate.FareSlashed_Message.replace('[AMT]', amt) : Vue_FareUpdate.FareHiked_Message.replace('[AMT]', amt);
                        $("#fareChangeModal").modal({ backdrop: 'static', keyboard: false });
                    });
                }

                switch (selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.tripType) {
                    case 'o':
                        selectResponseObj.content.fareInformationWithoutPnrReply['tripTypeText'] = 'Oneway';
                        break;
                    case 'r':
                        selectResponseObj.content.fareInformationWithoutPnrReply['tripTypeText'] = 'Round Trip';
                        break;
                    case 'm':
                        selectResponseObj.content.fareInformationWithoutPnrReply['tripTypeText'] = 'Multicity';
                        break;
                }
                selectResponseObj.paxList = [];
                var totalPaxUnites = 0;
                var travllerIndex = 1;
                $.each(selectResponseObj.content.fareInformationWithoutPnrReply.paxFareDetails, function (index, paxFareDetail) {
                    totalPaxUnites += parseInt(paxFareDetail.totalPaxUnites);
                });

                var miniRuleLists = [];
                // $.each(selectResponseObj.content.fareInformationWithoutPnrReply.minirules, function (index, rule) {
                //     var miniRules = [];
                //     rule.codes[0].category.forEach(cat => {
                //         if (cat.cat == "31" || cat.cat == "33") {
                //             var before33Restriappinfo = { action: 0 };
                //             var before33Moninfo = { amount: 0, currency: '' };
                //             var before31Restriappinfo = { action: 0 };
                //             var before31Moninfo = { amount: 0, currency: '' };

                //             var after33Restriappinfo = { action: 0 };
                //             var after33Moninfo = { amount: 0, currency: '' };
                //             var after31Restriappinfo = { action: 0 };
                //             var after31Moninfo = { amount: 0, currency: '' };

                //             if (cat.cat == "31") {
                //                 try { before33Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'BDA'); } catch (err) { }
                //                 try { before33Moninfo = cat.monInfo.find(x => x.typeQualifier == 'BDX'); } catch (err) { }
                //                 try { after33Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'BDA'); } catch (err) { }
                //                 try { after33Moninfo = cat.monInfo.find(x => x.typeQualifier == 'BDX'); } catch (err) { }

                //                 miniRules.push({
                //                     type: 'Cancellation',
                //                     before: ((before33Restriappinfo.action == 1) ? '' : 'Not Allowed'),
                //                     after: ((before31Restriappinfo.action == 1) ? '' : 'Not Allowed'),
                //                     beforeAmount: before33Moninfo.amount,
                //                     afterAmount: before31Moninfo.amount
                //                 });
                //             }
                //             if (cat.cat == "33") {
                //                 try { before31Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'ADA'); } catch (err) { }
                //                 try { before31Moninfo = cat.monInfo.find(x => x.typeQualifier == 'ADX'); } catch (err) { }
                //                 try { after31Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'ADA'); } catch (err) { }
                //                 try { after31Moninfo = cat.monInfo.find(x => x.typeQualifier == 'ADX'); } catch (err) { }

                //                 miniRules.push({
                //                     type: 'Changes',
                //                     before: ((after33Restriappinfo.action == 1) ? '' : 'Not Allowed'),
                //                     after: ((after31Restriappinfo.action == 1) ? '' : 'Not Allowed'),
                //                     beforeAmount: after33Moninfo.amount,
                //                     afterAmount: after31Moninfo.amount
                //                 });
                //             }
                //         }
                //     });

                //     var miniRuleList = {
                //         "from": rule.departure,
                //         "to": rule.arrival,
                //         "fromAirport": getAirport(rule.departure),
                //         "toAirport": getAirport(rule.arrival),
                //         "miniRules": miniRules
                //     };

                //     miniRuleLists.push(miniRuleList);
                // });
                // selectResponseObj.content.fareInformationWithoutPnrReply.miniRuleList = miniRuleLists;

                var fareRuleDiv = '';

                var airFareRules = selectResponseObj.content.fareInformationWithoutPnrReply.airFareRule;
                $.each(airFareRules, function (segIndex, fareRule) {
                    if (hasFareRule(fareRule)) {
                        fareRuleDiv += '<div class="itinerary_head fare-accordion">' +
                            '<h2>' +
                            '<i class="fa fa-plane summary_flight"></i>' +
                            airportLocationFromAirportCode(fareRule.departure) + '(' + fareRule.departure.toUpperCase() + ') to ' +
                            airportLocationFromAirportCode(fareRule.arrival) + '(' + fareRule.arrival + ')' +
                            '</h2>' +
                            '</div><div class="fare-panel">';
                        if (fareRule.fareRuleDetails && fareRule.fareRuleDetails.length > 0) {
                            $.each(fareRule.fareRuleDetails, function (ruleIndex, rule) {
                                if (!stringIsNullorEmpty(rule.ruleHead)) {
                                    fareRuleDiv += '<p><b>' + rule.ruleHead + '</b></p>' +
                                        '<p>' + rule.ruleBody + '</p>';
                                }
                                else {
                                    fareRuleDiv += '<p style="padding-top: 16px;float: left;">No fare rule available for this segment</p>';
                                }
                            });
                        }
                        fareRuleDiv += '</div>';

                    }
                });

                if (fareRuleDiv != '') {
                    this.fareRuleDiv = fareRuleDiv;
                } else {
                    this.fareRuleDiv = '<p>Fare rules not available for this trip !</p>';
                }

                setTimeout(function(){
                    var acc = document.getElementsByClassName("fare-accordion");
                    var i;

                    for (i = 0; i < acc.length; i++) {
                        acc[i].addEventListener("click", function () {
                            /* Toggle between adding and removing the "active" class,
                            to highlight the button that controls the panel */
                            this.classList.toggle("active");

                            /* Toggle between hiding and showing the active panel */
                            var panel = this.nextElementSibling;
                            if (panel.style.display === "block") {
                                panel.style.display = "none";
                            } else {
                                panel.style.display = "block";
                            }
                        });
                    }
                    $('[data-toggle="tooltip"]').tooltip();
                }, 500);


                selectResponseObj.totalPaxUnites = totalPaxUnites;
                var airLegs = [];
                $.each(selectResponseObj.content.fareInformationWithoutPnrReply.airSegments, function (index, airSegment) {
                    var items = $.grep(selectResponseObj.content.fareInformationWithoutPnrReply.airSegments, function (key) {
                        return (key.segDirectionID == airSegment.segDirectionID);
                    });
                    if (items.length > 1) {
                        airSegment.stopCount = items.length;
                        airSegment.stopCountText = items.length.toString() + ' ';
                    } else {
                        airSegment.stopCount = 0;
                        airSegment.stopCountText = 'Non-';
                    }
                    airSegment.origin = getAirport(airSegment.originCity);
                    airSegment.departure = getAirport(airSegment.departureCity);
                    airSegment.boardAirport = getAirport(airSegment.segInfo.flData.boardPoint);
                    airSegment.offAirport = getAirport(airSegment.segInfo.flData.offpoint);
                    var airLegItemIndex = airLegs.findIndex(function (airLegItem) {
                        return airLegItem.legId == airSegment.segDirectionID;
                    });
                    if (airLegItemIndex > -1) {
                        airLegs[airLegItemIndex].departure = getAirport(airSegment.departureCity);
                    } else {
                        var legClass = airSegment.segDirectionID == 1 && selectResponseObj.content.fareInformationWithoutPnrReply.tripType == 'r' ? 'summary-departure summary-arrival' : 'summary-departure';
                        airLegs.push({
                            legClass: legClass,
                            legId: airSegment.segDirectionID,
                            origin: getAirport(airSegment.originCity),
                            departure: getAirport(airSegment.departureCity),
                            segInfo: airSegment.segInfo
                        });
                    }
                });
                selectResponseObj.content.fareInformationWithoutPnrReply.airLegs = airLegs;
                this.selectedFlight = selectResponseObj;
                Vue_FlightFare.selectedFlight = selectResponseObj;
                Vue_FlightFare.selectFlightFromSession = selectFlightFromSessionObj;
                showHideLoader();
                var isUserHaveOfflineIssuance = false;
                try {
                    var b2cRoles = JSON.parse(localStorage.roleList).filter(function (role) { return role.id == 6; })[0] || {};
                    if (!_.isEmpty(b2cRoles)){
                        var flightRoles = _.find(b2cRoles.moduleList, function (service) { return service.name.toLowerCase() == 'flight'; }) ||{};
                        if (!_.isEmpty(flightRoles)) {
                            var role = _.find(flightRoles.functionalityList, function (e) { return e.id == 198; });
                            if (!_.isEmpty(role)) {
                                isUserHaveOfflineIssuance = true;
                            }
                        }
                    }
                } catch (e) {
                    isUserHaveOfflineIssuance = false;
                }

                try {
                    var b2cRoles = JSON.parse(localStorage.roleList).filter(function (role) { return role.id == 6; })[0] || {};
                    if (!_.isEmpty(b2cRoles)){
                        var flightRoles = _.find(b2cRoles.moduleList, function (service) { return service.name.toLowerCase() == 'flight'; }) ||{};
                        if (!_.isEmpty(flightRoles)) {
                            var role = _.find(flightRoles.functionalityList, function (e) { return e.id == 63 || e.id == 193 });
                            if (!_.isEmpty(role)) {
                                self.pgEnable = true;
                            }
                        }
                    }
                } catch (e) {
                    self.pgEnable = false;
                }

                
                var isIntantPurchachase = false;
                if (selectFlightFromSessionObj.selectedFlightInfo.typeRef != undefined && selectFlightFromSessionObj.selectedFlightInfo.typeRef.type) {
                    isIntantPurchachase = selectFlightFromSessionObj.selectedFlightInfo.typeRef.type.toLowerCase() == 'instantpurchase';   
                }

                var valueDate = moment(selectFlightFromSessionObj.selectedFlightInfo.groupOfFlights[0].flightDetails[0].fInfo.dateTime.dateTimeFormatedDepDate).diff(moment(new Date()), 'hours');
                if (_.isEmpty(selectFlightFromSessionObj.selectedFlightInfo.typeRef) &&
                    !isIntantPurchachase &&
                    isUserHaveOfflineIssuance &&
                    valueDate > 24) {
                    this.isPaymentOnAgencyActive = true;
                }
            } catch (err) {
                alertify.alert(self.Alert, self.Message_UnexpectSupplierIssue).setting({ 'closable': false, onclose: function () { window.location = '/'; } }).set('label', self.Ok);
            }
        },
        rules: function (item) {
            var self = this;
            var rules;
            var before = item.find(x => x.situation == 'Before departure');
            if (before.allowed) {
                if (before.amount > 0) {
                    rules = '<p>' + before.cat + '</p><p class="allowed">Allowed @ ' + self.$n(parseFloat(before.amount / self.CurrencyMultiplier), 'currency', self.selectedCurrency) + '</p>'

                } else {
                    rules = '<p>' + before.cat + '</p><p class="allowed">Allowed</p>';
                }

            } else {
                rules = '<p>' + before.cat + '</p><p class="not-allowed">Not Allowed</p>';
            }

            var after = item.find(x => x.situation == 'After departure');
            if (after.allowed) {
                if (after.amount > 0) {
                    rules = rules + '<p class="allowed">Allowed @ ' + self.$n(parseFloat(after.amount / self.CurrencyMultiplier), 'currency', self.selectedCurrency) + '</p>'

                } else {
                    rules = rules + '<p class="allowed">Allowed</p>';
                }

            } else {
                rules = rules + '<p class="not-allowed">Not Allowed</p>';
            }
            return rules;

        },
        getdatetimeformated: function (datetime, inputFormat, outputFormat) {
            return getdatetimeformated(datetime, inputFormat, outputFormat);
        },
        gettimeformated: function (time, inputFormat, houroutputFormat, houraddl, minoutputFormat, minaddl) {
            return gettimeformated(time, inputFormat, houroutputFormat, houraddl, minoutputFormat, minaddl);
        },
        calculateDatetimeDiff: function (startDate, startDateFormat, endDate, endDateFormat) {
            return calculateDatetimeDiff(startDate, startDateFormat, endDate, endDateFormat);
        },
        getAirport: function (airportCode) {
            return getAirport(airportCode);
        },
        getAirline: function (airlineCode) {
            return getAirline(airlineCode);
        },
        getPaxFareTotal: function (pax, fare) {
            return (pax * fare).toFixed(2);
        },
        getPaxType: function (paxType, paxCount) {
            return getPaxType(paxType, paxCount);
        },
        getUnit: function (unit) {
            return getUnit(unit);
        },
        getPaxTypeName: function (ptype) {
            var paxTypeText = 'Person';
            switch (ptype) {
                case 'ADT':
                    paxTypeText = 'Adult';
                    break;
                case 'CHD':
                    paxTypeText = 'Child';
                    break;
                case 'INF':
                    paxTypeText = 'Infant';
                    break;
            }
            return paxTypeText;
        },
        getObjectId: function (name, paxType, number) {
            return name + paxType + number;
        },
        getDirectionClass: function (directionId, trueClass, falseClass) {
            try {
                return (directionId == 1 && this.selectedFlight.content.fareInformationWithoutPnrReply.tripType == 'r') ? trueClass : falseClass;
            } catch (err) {
                return trueClass;
            }
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function (callBackFunction) {
            var self = this;

            getAgencycode(function (response) {
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + commonAgencyCode + '/Template/Booking Details/Booking Details/Booking Details.ftl';
                axios.get(aboutUsPage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (response.data.area_List.length > 0) {
                        var mainComp = response.data.area_List[0].main;
                        self.Review_Flight = self.pluckcom('Review_Flight', mainComp.component);
                        self.Traveller_Details = self.pluckcom('Traveller_Details', mainComp.component);
                        self.Flight_Booking = self.pluckcom('Flight_Booking', mainComp.component);
                        self.Payment = self.pluckcom('Payment', mainComp.component);
                        self.Confirmation = self.pluckcom('Confirmation', mainComp.component);
                        self.Log_out = self.pluckcom('Log_out', mainComp.component);
                        self.Signin_ProcessLabel = self.pluckcom('Signin_ProcessLabel', mainComp.component);
                        self.Sign_In = self.pluckcom('Sign_In', mainComp.component);
                        self.Email_RequiredLabel = self.pluckcom('Email_RequiredLabel', mainComp.component);
                        self.Password_RequiredLabel = self.pluckcom('Password_RequiredLabel', mainComp.component);
                        self.Account_NotAvailable = self.pluckcom('Account_NotAvailable', mainComp.component);
                        self.Account_ProcessLabel = self.pluckcom('Account_ProcessLabel', mainComp.component);
                        self.to = self.pluckcom('to', mainComp.component);
                        self.stop = self.pluckcom('stop', mainComp.component);
                        self.Fare_Rules = self.pluckcom('Fare_Rules', mainComp.component);
                        self.Baggage = self.pluckcom('Baggage', mainComp.component);
                        self.Refundable = self.pluckcom('Refundable', mainComp.component);
                        self.Non_Refundable = self.pluckcom('Non_Refundable', mainComp.component);
                        self.Refundable_withPenalty = self.pluckcom('Refundable_withPenalty', mainComp.component);
                        self.No_cancellation = self.pluckcom('No_cancellation', mainComp.component);
                        self.Fare_Rule = self.pluckcom('Fare_Rule', mainComp.component);
                        self.Before_Departure = self.pluckcom('Before_Departure', mainComp.component);
                        self.After_Departure = self.pluckcom('After_Departure', mainComp.component);
                        self.Allowed_for = self.pluckcom('Allowed_for', mainComp.component);
                        self.Flight_from = self.pluckcom('Flight_from', mainComp.component);
                        self.Checked_Baggage = self.pluckcom('Checked_Baggage', mainComp.component);
                        self.Free = self.pluckcom('Free', mainComp.component);
                        self.Cabin_Baggage = self.pluckcom('Cabin_Baggage', mainComp.component);
                        self.No_baggage = self.pluckcom('No_baggage', mainComp.component);
                        self.Provide_TravellerDetails = self.pluckcom('Provide_TravellerDetails', mainComp.component);
                        self.NationalIdPassport_Warning = self.pluckcom('NationalIdPassport_Warning', mainComp.component);
                        self.Required_field = self.pluckcom('Required_field', mainComp.component);
                        self.Title_Label = self.pluckcom('Title_Label', mainComp.component);
                        self.FirstName_Label = self.pluckcom('FirstName_Label', mainComp.component);
                        self.LastName_Label = self.pluckcom('LastName_Label', mainComp.component);
                        self.DoB_Label = self.pluckcom('DoB_Label', mainComp.component);
                        self.Nationality_Label = self.pluckcom('Nationality_Label', mainComp.component);
                        self.PassportNumber_Label = self.pluckcom('PassportNumber_Label', mainComp.component);
                        self.ExpireDate_Label = self.pluckcom('ExpireDate_Label', mainComp.component);
                        self.IssueDate_Label = self.pluckcom('IssueDate_Label', mainComp.component);
                        self.IssueCountry_Label = self.pluckcom('IssueCountry_Label', mainComp.component);
                        self.Frequentflyer_program = self.pluckcom('Frequentflyer_program', mainComp.component);
                        self.Frequentflyer_number = self.pluckcom('Frequentflyer_number', mainComp.component);
                        self.Email = self.pluckcom('Email', mainComp.component);
                        self.FlightTicket_RegisteredEmail = self.pluckcom('FlightTicket_RegisteredEmail', mainComp.component);
                        self.PhoneNumber_Label = self.pluckcom('PhoneNumber_Label', mainComp.component);
                        self.Traveller = self.pluckcom('Traveller', mainComp.component);
                        self.Agree_WhatsApp = self.pluckcom('Agree_WhatsApp', mainComp.component);
                        self.TripSummary_Detail = self.pluckcom('TripSummary_Detail', mainComp.component);
                        self.Tickets = self.pluckcom('Tickets', mainComp.component);
                        self.Fare = self.pluckcom('Fare', mainComp.component);
                        self.Taxes_Fees = self.pluckcom('Taxes_Fees', mainComp.component);
                        self.Service_Fee = self.pluckcom('Service_Fee', mainComp.component);
                        self.Total = self.pluckcom('Total', mainComp.component);
                        self.Taxesfees_included = self.pluckcom('Taxesfees_included', mainComp.component);
                        self.Travellers = self.pluckcom('Travellers', mainComp.component);
                        self.FareChange_Alert = self.pluckcom('FareChange_Alert', mainComp.component);
                        self.Updated_Fare = self.pluckcom('Updated_Fare', mainComp.component);
                        self.Original_Fare = self.pluckcom('Original_Fare', mainComp.component);
                        self.Cancel = self.pluckcom('Cancel', mainComp.component);
                        self.Continue = self.pluckcom('Continue', mainComp.component);
                        self.Reviewyour_flight = self.pluckcom('Reviewyour_flight', mainComp.component);
                        self.Continue_ToPayment = self.pluckcom('Continue_ToPayment', mainComp.component);
                        self.close = self.pluckcom('close', mainComp.component);
                        self.Alert = self.pluckcom('Alert', mainComp.component);
                        self.Confirm = self.pluckcom('Confirm', mainComp.component);
                        self.Message_UnexpectSupplierIssue = self.pluckcom('Message_UnexpectSupplierIssue', mainComp.component);
                        self.Provide_Email = self.pluckcom('Provide_Email', mainComp.component);
                        self.Provide_PhoneNumber = self.pluckcom('Provide_PhoneNumber', mainComp.component);
                        self.Thank_Registerwithus = self.pluckcom('Thank_Registerwithus', mainComp.component);
                        self.Email_AlreadyRegistered = self.pluckcom('Email_AlreadyRegistered', mainComp.component);
                        self.Fields_Mandatory = self.pluckcom('Fields_Mandatory', mainComp.component);
                        self.Booking_failed = self.pluckcom('Booking_failed', mainComp.component);
                        self.Max_TwentyChar = self.pluckcom('Max_TwentyChar', mainComp.component);
                        self.Username_required = self.pluckcom('Username_required', mainComp.component);
                        self.Password_required = self.pluckcom('Password_required', mainComp.component);
                        self.Login_failed = self.pluckcom('Login_failed', mainComp.component);
                        self.Password = self.pluckcom('Password', mainComp.component);
                        self.FrequentNumber_Required = self.pluckcom('FrequentNumber_Required', mainComp.component);
                        self.Welcome = self.pluckcom('Welcome', mainComp.component);
                        self.Ok = self.pluckcom('Ok', mainComp.component);
                        self.TravellerName_Duplicated = self.pluckcom('TravellerName_Duplicated', mainComp.component);
                        self.checkAgrementValidation = self.pluckcom('Check_agree_alert', mainComp.component);
                        self.GoBack = self.pluckcom('GoBack', mainComp.component);
                        self.AddFrequent = self.pluckcom('AddFrequent', mainComp.component);
                        self.Payment_Type_Label = self.pluckcom('Payment_Type_Label', mainComp.component);
                        self.Card_Label = self.pluckcom('Card_Label', mainComp.component);
                        self.Hold_Label = self.pluckcom('Hold_Label', mainComp.component);
                        self.Disclaimer_Label = self.pluckcom('Disclaimer_Label', mainComp.component);
                        self.Terms_1_Label = self.pluckcom('Terms_1_Label', mainComp.component);
                        self.Terms_2_Label = self.pluckcom('Terms_2_Label', mainComp.component);
                        self.Terms_3_Label = self.pluckcom('Terms_3_Label', mainComp.component);
                        self.Terms_4_Label = self.pluckcom('Terms_4_Label', mainComp.component);
                        self.Terms_5_Label = self.pluckcom('Terms_5_Label', mainComp.component);
                        self.Proceed_Label = self.pluckcom('Proceed_Label', mainComp.component);
                        self.Continue_Label = self.pluckcom('Continue_Label', mainComp.component);
                        try { callBackFunction(); } catch (err) { console.log(''); }
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });
        },
        getmoreinfo(url) {
            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url = "/Nirvana/book-now.html?page=" + url;
            } else {
                url = "#";
            }
            return url;
        },
        loadInsurance: function() {
            var searchRQ = {
                request: {
                    service: "InsuranceRQ",
                    supplierCodes: [24],
                    token: localStorage.access_token,
                    content: {
                        command: "TuneprotectSearchRQ",
                        criteria: {
                            criteriaType: "TuneprotectInsurance",
                            departCountryCode: get2letterCountryCode(selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.from),
                            departStationCode: selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.from,
                            departDateTime: moment(selectFlightFromSessionObj.selectedFlightInfo.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate, "DDMMYY").format("YYYY-MM-DD 00:00:00"),
                            arrivalCountryCode: get2letterCountryCode(selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.to),
                            arrivalStationCode: selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.to,
                            returnDateTime: selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.tripType.toUpperCase() == "R" ? moment(selectFlightFromSessionObj.selectedFlightInfo.groupOfFlights[selectFlightFromSessionObj.selectedFlightInfo.groupOfFlights.length - 1].flightDetails[0].fInfo.dateTime.depDate, "DDMMYY").format("YYYY-MM-DD 00:00:00") : undefined,
                            departAirlineCode: "",
                            totalAdults: selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.adt,
                            totalChild: selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.chd,
                            totalInfants: selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.inf
                        }
                    }
                }
            }
            websocket.connect(); //open websocket connection           

            websocket.sendMessage(JSON.stringify(searchRQ));
        },
        processInsuranceResult: function (response) {
            if (response.response.content.data && response.response.content.data.availableUpsellPlans) {
                this.insuranceData = response.response.content.data;
                this.insuranceDataFiltered = response.response.content.data.availableUpsellPlans;
                // .filter(function(e) {
                //     return ["AE-API-TRAVELEASY-INBOUND-PLATINUM", "AE-API-TRAVELEASY-INBOUND-GOLD", "AE-API-TRAVELEASY-INBOUND-SILVER",
                //         "AE-API-BAGGAGE-OUTBOUND-PLATINUM", "AE-API-BAGGAGE-OUTBOUND-GOLD", "AE-API-BAGGAGE-OUTBOUND-SILVER",
                //     ].indexOf(e.upsellPlans[0].planCode) != -1;
                // });
                this.selectCredentialInsurance = response.response.selectCredential;
                this.toggleInsurance = true;
                this.selectedPlan = this.insuranceDataFiltered[0].upsellPlans[0];
                Vue_FlightFare.insurance.insuranceTotal = this.selectedPlan.totalPremiumAmount;
                Vue_FlightFare.insurance.toggleInsurance = this.toggleInsurance;
                Vue_FlightFare.$forceUpdate();
                setTimeout(function() {
                    $("#insurance").collapse("show");
                }, 150);
            }
        },
        selectInsurance: function (item) {
            this.selectedPlan = item;
            Vue_FlightFare.insurance.insuranceTotal = this.selectedPlan.totalPremiumAmount;
            Vue_FlightFare.insurance.toggleInsurance = this.toggleInsurance;
        },
        viewPlan: function(item) {
            this.viewInsurance = item;
            setTimeout(() => {
                $('#insuranceplan').modal('show');
            }, 50);
        },
        purchase: function () {
            var vm = this;
            var data = {
                request: {
                    service: "InsuranceRQ",
                    supplierCodes: [24],
                    content: {
                        command: "TuneprotectConfirmRQ",
                        isHubOnly: true,
                        bookingRef: null,
                        purchaseInsuranceRQ: {
                            userContactDetails: {
                                countryCode: "AE"
                            },
                            insurancePurchaseRequest: {
                                pnr: moment(new Date()).format("YYYYMMDDHHSSMM"),
                                purchaseDate: moment(new Date()).format("YYYY-MM-DD HH:SS:MM"),
                                startDate: moment(this.selectedFlight.content.fareInformationWithoutPnrReply.airLegs[0].segInfo.dateandTimeDetails.departureDate, "DDMMYY").format("YYYY-MM-DDT00:00:00"),
                                returnDate: selectResponseObj.content.fareInformationWithoutPnrReply.tripType.toUpperCase() == "R" ? moment(this.selectedFlight.content.fareInformationWithoutPnrReply.airLegs[this.selectedFlight.content.fareInformationWithoutPnrReply.airLegs.length - 1].segInfo.dateandTimeDetails.arrivalDate, "DDMMYY").format("YYYY-MM-DDT00:00:00") : undefined,
                                planCode: this.selectedPlan.planCode,
                                policyName: this.selectedPlan.planTitle,
                                planContent: this.selectedPlan.planContent,
                                totalAdults: selectResponseObj.content.fareInformationWithoutPnrReply.passengerTypeQuantity.adt,
                                totalChild: selectResponseObj.content.fareInformationWithoutPnrReply.passengerTypeQuantity.chd,
                                totalInfants: selectResponseObj.content.fareInformationWithoutPnrReply.passengerTypeQuantity.inf,
                                ssrFeeCode: this.selectedPlan.ssrFeeCode,
                                currency: this.selectedPlan.currencyCode,
                                totalPremium: this.selectedPlan.totalPremiumAmount,
                                contactDetails: {
                                    contactPerson: this.username || $('#emailtxt').val(),
                                    emailAddress: $('#emailtxt').val()
                                },
                                flights: [{
                                    departCountryCode: get2letterCountryCode(this.selectedFlight.content.fareInformationWithoutPnrReply.airLegs[0].origin.I),
                                    departStationCode: this.selectedFlight.content.fareInformationWithoutPnrReply.airLegs[0].origin.I,
                                    arrivalCountryCode: get2letterCountryCode(this.selectedFlight.content.fareInformationWithoutPnrReply.airLegs[0].departure.I),
                                    arrivalStationCode: this.selectedFlight.content.fareInformationWithoutPnrReply.airLegs[0].departure.I,
                                    departAirlineCode: "",
                                    departDateTime: moment(this.selectedFlight.content.fareInformationWithoutPnrReply.airLegs[0].segInfo.dateandTimeDetails.departureDate, "DDMMYY").format("YYYY-MM-DD 00:00:00"),
                                    returnDateTime: selectResponseObj.content.fareInformationWithoutPnrReply.tripType.toUpperCase() == "R" ? moment(this.selectedFlight.content.fareInformationWithoutPnrReply.airLegs[this.selectedFlight.content.fareInformationWithoutPnrReply.airLegs.length - 1].segInfo.dateandTimeDetails.arrivalDate, "DDMMYY").format("YYYY-MM-DD 00:00:00") : undefined,
                                }],
                                passengers: this.travellersInsurance.map(function (e, i) {
                                    var passengerPremiumAmount = vm.selectedPlan.planPricingBreakdown[0].premiumAmount;

                                    if (vm.selectedPlan.planPremiumChargeType.toLowerCase() == "perbooking" && i > 0) {
                                        passengerPremiumAmount = 0
                                    }

                                    return {
                                        title: e.namePrefix == "Miss" ? "Ms": e.namePrefix,
                                        paxType: e.passengerType,
                                        isInfant: e.passengerType == "INF" ? "1" : "0",
                                        firstName: e.givenName,
                                        lastName: e.surName,
                                        gender: e.gender.toUpperCase(),
                                        dob: moment(e.birthDate, "DD-MM-YYYY").format("YYYY-MM-DD"),
                                        age: moment().diff(moment(e.birthDate, "DD-MM-YYYY"), 'years'),
                                        identityType: "PASSPORT",
                                        identityNo: e.docID,
                                        isQualified: "false",
                                        nationality: e.nationalityData.twoLetter,
                                        countryOfResidence: e.nationalityData.twoLetter,
                                        selectedPlanCode: vm.selectedPlan.planCode,
                                        selectedSSRFeeCode: vm.selectedPlan.ssrFeeCode,
                                        currencyCode: vm.selectedPlan.currencyCode,
                                        passengerPremiumAmount: e.passengerType == "INF" ? 0 : passengerPremiumAmount
                                    }
                                })
                            },
                            sealed: this.insuranceData.sealed,
                            paymentMode: 6
                        }
                    },
                    selectCredential: this.selectCredentialInsurance
                }
            }
            return data;
        }
    },
    mounted: function () {
        defaultCountry = JSON.parse(localStorage.User).loginNode.country.code;
        var dateFormat = 'dd/mm/yy';
        var noOfMonths = 1;
        $(".calendar").datepicker({
            numberOfMonths: parseInt(noOfMonths),
            changeMonth: true,
            changeYear: true,
            showButtonPanel: false,
            dateFormat: dateFormat,
            beforeShow: function () {
                Vue_FlightBook.getMinDate(this);
                Vue_FlightBook.getMaxDate(this);
            }
            /*,onSelect: function (date) {
                var startDate = $(this).datepicker('getDate');
                sDate = startDate.getTime();
            }*/
        });
        this.getPagecontent();
    },
    watch: {
        toggleInsurance: function () {
            Vue_FlightFare.insurance.toggleInsurance = this.toggleInsurance;
        }
    }
});

var Vue_FlightFare = new Vue({
    el: "#flightfareDiv",
    i18n,
    data: {
        AirlinesDatas: AirlinesDatas,
        AirlinesTimezone: AirlinesTimezone,
        insurance: {
            insuranceTotal: null,
            toggleInsurance: false
        },
        selectedFlight: {
            totalPaxUnites: 0,
            content: {
                fareInformationWithoutPnrReply: {
                    airLegs: [],
                    flightFareDetails: {
                        currency: '',
                        totalFare: 0
                    }
                },
                tripTypeText: '...'
            }
        },
        selectFlightFromSession: [],
        selectedCurrency: null,
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        Review_Flight: '',
        Traveller_Details: '',
        Flight_Booking: '',
        Payment: '',
        Confirmation: '',
        Log_out: '',
        Signin_ProcessLabel: '',
        Sign_In: '',
        Email_RequiredLabel: '',
        Password_RequiredLabel: '',
        Account_NotAvailable: '',
        Account_ProcessLabel: '',
        to: '',
        stop: '',
        Fare_Rules: '',
        Baggage: '',
        Refundable: '',
        Non_Refundable: '',
        Refundable_withPenalty: '',
        No_cancellation: '',
        Fare_Rule: '',
        Before_Departure: '',
        After_Departure: '',
        Allowed_for: '',
        Flight_from: '',
        Checked_Baggage: '',
        Free: '',
        Cabin_Baggage: '',
        No_baggage: '',
        Provide_TravellerDetails: '',
        NationalIdPassport_Warning: '',
        Required_field: '',
        Title_Label: '',
        FirstName_Label: '',
        LastName_Label: '',
        DoB_Label: '',
        Nationality_Label: '',
        PassportNumber_Label: '',
        ExpireDate_Label: '',
        IssueDate_Label: '',
        IssueCountry_Label: '',
        Frequentflyer_program: '',
        Frequentflyer_number: '',
        Email: '',
        FlightTicket_RegisteredEmail: '',
        PhoneNumber_Label: '',
        Traveller: '',
        Agree_WhatsApp: '',
        TripSummary_Detail: '',
        Tickets: '',
        Fare: '',
        Taxes_Fees: '',
        Service_Fee: '',
        Total: '',
        Taxesfees_included: '',
        Travellers: '',
        FareChange_Alert: '',
        Updated_Fare: '',
        Original_Fare: '',
        Cancel: '',
        Continue: '',
        Reviewyour_flight: '',
        Continue_ToPayment: '',
        close: '',
        Alert: '',
        Confirm: '',
        Message_UnexpectSupplierIssue: '',
        Provide_Email: '',
        Provide_PhoneNumber: '',
        Thank_Registerwithus: '',
        Email_AlreadyRegistered: '',
        Fields_Mandatory: '',
        Booking_failed: '',
        Max_TwentyChar: '',
        Username_required: '',
        Password_required: '',
        Login_failed: '',
        Password: '',
        FrequentNumber_Required: '',
        Welcome: '',
        Ok: '',
        GoBack: '',
        AddFrequent: ''
    },
    mounted() {
        var userNode = JSON.parse(localStorage.User);
        if (userNode) {
            this.selectedCurrency = (localStorage.selectedCurrency) ? localStorage.selectedCurrency : userNode.loginNode.currency;
        } else {
            this.selectedCurrency = (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD';
        }
        autoFillTravellers();
        this.getPagecontent();
    },
    created: function () {
    },
    methods: {
        getdatetimeformated: function (datetime, inputFormat, outputFormat) {
            return moment(datetime, inputFormat).format(outputFormat);
        },
        gettimeformated: function (time, inputFormat, houroutputFormat, houraddl, minoutputFormat, minaddl) {
            var hour = moment(time, inputFormat).format(houroutputFormat).toString();
            var min = moment(time, inputFormat).format(minoutputFormat).toString();
            var formattedtime = hour + houraddl + min + minaddl;
            return formattedtime;
        },
        calculateDatetimeDiff: function (startDate, startDateFormat, endDate, endDateFormat) {
            var start_date = moment(startDate, startDateFormat);
            var end_date = moment(endDate, endDateFormat);
            var duration = moment.duration(end_date.diff(start_date));
            var hours = duration.hours();
            var mins = duration.minutes();
            return hours + 'h ' + mins + 'm';
        },
        getPaxFareTotal: function (pax, fare) {
            return (pax * fare).toFixed(2);
        },
        getPaxType: function (paxType, paxCount) {
            return getPaxType(paxType, paxCount);
        },
        getObjectId: function (name, paxType, number) {
            return name + paxType + number;
        },
        getDirectionClass: function (directionId, trueClass, falseClass) {
            return '';//try { return (directionId == 1 && this.selectedFlight.content.fareInformationWithoutPnrReply.tripType == 'r') ? trueClass : falseClass; } catch (err) { return trueClass; }
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + commonAgencyCode + '/Template/Booking Details/Booking Details/Booking Details.ftl';
                axios.get(aboutUsPage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (response.data.area_List.length > 0) {
                        var mainComp = response.data.area_List[0].main;
                        self.Review_Flight = self.pluckcom('Review_Flight', mainComp.component);
                        self.Traveller_Details = self.pluckcom('Traveller_Details', mainComp.component);
                        self.Flight_Booking = self.pluckcom('Flight_Booking', mainComp.component);
                        self.Payment = self.pluckcom('Payment', mainComp.component);
                        self.Confirmation = self.pluckcom('Confirmation', mainComp.component);
                        self.Log_out = self.pluckcom('Log_out', mainComp.component);
                        self.Signin_ProcessLabel = self.pluckcom('Signin_ProcessLabel', mainComp.component);
                        self.Sign_In = self.pluckcom('Sign_In', mainComp.component);
                        self.Email_RequiredLabel = self.pluckcom('Email_RequiredLabel', mainComp.component);
                        self.Password_RequiredLabel = self.pluckcom('Password_RequiredLabel', mainComp.component);
                        self.Account_NotAvailable = self.pluckcom('Account_NotAvailable', mainComp.component);
                        self.Account_ProcessLabel = self.pluckcom('Account_ProcessLabel', mainComp.component);
                        self.to = self.pluckcom('to', mainComp.component);
                        self.stop = self.pluckcom('stop', mainComp.component);
                        self.Fare_Rules = self.pluckcom('Fare_Rules', mainComp.component);
                        self.Baggage = self.pluckcom('Baggage', mainComp.component);
                        self.Refundable = self.pluckcom('Refundable', mainComp.component);
                        self.Non_Refundable = self.pluckcom('Non_Refundable', mainComp.component);
                        self.Refundable_withPenalty = self.pluckcom('Refundable_withPenalty', mainComp.component);
                        self.No_cancellation = self.pluckcom('No_cancellation', mainComp.component);
                        self.Fare_Rule = self.pluckcom('Fare_Rule', mainComp.component);
                        self.Before_Departure = self.pluckcom('Before_Departure', mainComp.component);
                        self.After_Departure = self.pluckcom('After_Departure', mainComp.component);
                        self.Allowed_for = self.pluckcom('Allowed_for', mainComp.component);
                        self.Flight_from = self.pluckcom('Flight_from', mainComp.component);
                        self.Checked_Baggage = self.pluckcom('Checked_Baggage', mainComp.component);
                        self.Free = self.pluckcom('Free', mainComp.component);
                        self.Cabin_Baggage = self.pluckcom('Cabin_Baggage', mainComp.component);
                        self.No_baggage = self.pluckcom('No_baggage', mainComp.component);
                        self.Provide_TravellerDetails = self.pluckcom('Provide_TravellerDetails', mainComp.component);
                        self.NationalIdPassport_Warning = self.pluckcom('NationalIdPassport_Warning', mainComp.component);
                        self.Required_field = self.pluckcom('Required_field', mainComp.component);
                        self.Title_Label = self.pluckcom('Title_Label', mainComp.component);
                        self.FirstName_Label = self.pluckcom('FirstName_Label', mainComp.component);
                        self.LastName_Label = self.pluckcom('LastName_Label', mainComp.component);
                        self.DoB_Label = self.pluckcom('DoB_Label', mainComp.component);
                        self.Nationality_Label = self.pluckcom('Nationality_Label', mainComp.component);
                        self.PassportNumber_Label = self.pluckcom('PassportNumber_Label', mainComp.component);
                        self.ExpireDate_Label = self.pluckcom('ExpireDate_Label', mainComp.component);
                        self.IssueDate_Label = self.pluckcom('IssueDate_Label', mainComp.component);
                        self.IssueCountry_Label = self.pluckcom('IssueCountry_Label', mainComp.component);
                        self.Frequentflyer_program = self.pluckcom('Frequentflyer_program', mainComp.component);
                        self.Frequentflyer_number = self.pluckcom('Frequentflyer_number', mainComp.component);
                        self.Email = self.pluckcom('Email', mainComp.component);
                        self.FlightTicket_RegisteredEmail = self.pluckcom('FlightTicket_RegisteredEmail', mainComp.component);
                        self.PhoneNumber_Label = self.pluckcom('PhoneNumber_Label', mainComp.component);
                        self.Traveller = self.pluckcom('Traveller', mainComp.component);
                        self.Agree_WhatsApp = self.pluckcom('Agree_WhatsApp', mainComp.component);
                        self.TripSummary_Detail = self.pluckcom('TripSummary_Detail', mainComp.component);
                        self.Tickets = self.pluckcom('Tickets', mainComp.component);
                        self.Fare = self.pluckcom('Fare', mainComp.component);
                        self.Taxes_Fees = self.pluckcom('Taxes_Fees', mainComp.component);
                        self.Service_Fee = self.pluckcom('Service_Fee', mainComp.component);
                        self.Total = self.pluckcom('Total', mainComp.component);
                        self.Taxesfees_included = self.pluckcom('Taxesfees_included', mainComp.component);
                        self.Travellers = self.pluckcom('Travellers', mainComp.component);
                        self.FareChange_Alert = self.pluckcom('FareChange_Alert', mainComp.component);
                        self.Updated_Fare = self.pluckcom('Updated_Fare', mainComp.component);
                        self.Original_Fare = self.pluckcom('Original_Fare', mainComp.component);
                        self.Cancel = self.pluckcom('Cancel', mainComp.component);
                        self.Continue = self.pluckcom('Continue', mainComp.component);
                        self.Reviewyour_flight = self.pluckcom('Reviewyour_flight', mainComp.component);
                        self.Continue_ToPayment = self.pluckcom('Continue_ToPayment', mainComp.component);
                        self.close = self.pluckcom('close', mainComp.component);
                        self.Alert = self.pluckcom('Alert', mainComp.component);
                        self.Confirm = self.pluckcom('Confirm', mainComp.component);
                        self.Message_UnexpectSupplierIssue = self.pluckcom('Message_UnexpectSupplierIssue', mainComp.component);
                        self.Provide_Email = self.pluckcom('Provide_Email', mainComp.component);
                        self.Provide_PhoneNumber = self.pluckcom('Provide_PhoneNumber', mainComp.component);
                        self.Thank_Registerwithus = self.pluckcom('Thank_Registerwithus', mainComp.component);
                        self.Email_AlreadyRegistered = self.pluckcom('Email_AlreadyRegistered', mainComp.component);
                        self.Fields_Mandatory = self.pluckcom('Fields_Mandatory', mainComp.component);
                        self.Booking_failed = self.pluckcom('Booking_failed', mainComp.component);
                        self.Max_TwentyChar = self.pluckcom('Max_TwentyChar', mainComp.component);
                        self.Username_required = self.pluckcom('Username_required', mainComp.component);
                        self.Password_required = self.pluckcom('Password_required', mainComp.component);
                        self.Login_failed = self.pluckcom('Login_failed', mainComp.component);
                        self.Password = self.pluckcom('Password', mainComp.component);
                        self.FrequentNumber_Required = self.pluckcom('FrequentNumber_Required', mainComp.component);
                        self.Welcome = self.pluckcom('Welcome', mainComp.component);
                        self.Ok = self.pluckcom('Ok', mainComp.component);
                        self.GoBack = self.pluckcom('GoBack', mainComp.component);
                        self.AddFrequent = self.pluckcom('AddFrequent', mainComp.component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        getmoreinfo(url) {
            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url = "/Nirvana/book-now.html?page=" + url;
            } else {
                url = "#";
            }
            return url;
        }
    }
});

function togglePaymentButton(toggle, processHtml) {
    showHideConfirmLoader(toggle);
    if (toggle) {
        $('#btnbookpay').attr("rel", $('#btnbookpay').attr("href"));
        $('#btnbookpay').attr("href", "javascript:;");
        $('#btnbookpay').html(!isNullorUndefined(processHtml) ? processHtml : '<i class="fa fa-circle-o-notch fa-spin"></i> Processing flight booking');
    } else {
        $('#btnbookpay').attr("href", $('#btnbookpay').attr("rel"));
        $('#btnbookpay').removeAttr("rel");
        $('#btnbookpay').html(!isNullorUndefined(processHtml) ? processHtml : 'Continue To Payment');
    }
}

var isDuplicateName = false;
var DuplicateNames = [];

function addTravelerInfo() {
    var details = passengerDetails();
    
    if (details.length > 0) {
        alertify.confirm("Information", "Do you want to add/update traveller details to your profile?"
            , function () {
                axios.all(details.map(function (req) { return axios.post(req.url, req.data, req.axiosConfig); }))
                    .then(axios.spread(function () {
                        console.log(arguments)
                        bookFlightFun();
                    }))
            }
            , function () {
                bookFlightFun();
            }).set({
                'closable': false, 'labels': { ok: "Yes", cancel: "No" },
                onclose: function () {
                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                }
            });
    } else {
        bookFlightFun();
    }
}

function continueBooking() {
    if (localStorage.IsLogin === "true") { 
        // bookFlightFun();
        addTravelerInfo()
    } else {
        var userEm = $('#emailtxt').val();
        registerUser(userEm, 'Guest', 'User', 'Mr', function (response) {
            if (response == true) {
                //alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.Thank_Registerwithus).set('closable', false).set('label', .Ok);
                // alert(Vue_FlightBook.Thank_Registerwithus);
                // bookFlightFun();
                addTravelerInfo()
            } else {
                alertify.confirm(Vue_FlightBook.Confirm, Vue_FlightBook.Email_AlreadyRegistered
                    , function () {
                        $('#sign-popup').addClass('in');
                        signArea.username = $('#emailtxt').val();
                        $('[name="password"]').focus();
                        $('html, body').animate({ scrollTop: 0 }, 'fast');
                        $('.loading-overlay').hide();
                    }
                    , function () {
                        /*$('#sign-popup').removeClass('in');
                        signArea.username = '';
                        togglePaymentButton(false);*/
                        commonlogin(bookFlightFun);
                    }).set({
                        'closable': false, 'labels': { ok: "Yes", cancel: "Continue as guest" },
                        onclose: function () {
                            $('html, body').animate({ scrollTop: 0 }, 'fast');
                        }
                    });
            }
        });
    }
}

function bookFlight() {
    $('.loading-overlay').show();
    var self = this;
    isDuplicateName = false;
    DuplicateNames = [];
    togglePaymentButton(true);
    toggleClassHide(('.border_danger'), 'border_danger');
    var isValidData = validateEntries();
    var isAgree = validateagrement();
    var isAgreeInsurance = validateagrementInsurance();
    sessionStorage.setItem("paymentVisit", true);
    setTimeout(function () {
        var self = this;
        if (isDuplicateName) {
            togglePaymentButton(false);
            DuplicateNames = $.distinct(DuplicateNames);
            //alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.TravellerName_Duplicated + ' - ' + DuplicateNames.join(',')).set('closable', false).set('label', Vue_FlightBook.Ok);
            alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.TravellerName_Duplicated).set('closable', false).set('label', Vue_FlightBook.Ok);
            $('.loading-overlay').hide();
        } else if(!isValidData) {
            togglePaymentButton(false);
            // alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.Fields_Mandatory).set({'closable': false,'label': Vue_FlightBook.Ok, 
            // onclose:function(){ 

                
                // setTimeout(() => {
                //    // $('html, body').animate({ scrollTop: 0 }, 'fast');
                //     $('input[type="text"].border_danger').each(function () {
                        
                        
                //         $('html, body').animate({scrollTop: $(this).offset().top }, 200);
                //         $(this).focus();
                //         return false;
                //     });
                //    // 
                // }, 100);
               
            // } });
            // toggleClassHide('.border_danger', 'border_danger');
            $('.loading-overlay').hide();
        } else if (isAgree) {
            togglePaymentButton(false);
             $(".loading-overlay").hide();
            alertify.alert(Vue_FlightBook.Alert,Vue_FlightBook.checkAgrementValidation).set('closable', false).set('label', Vue_FlightBook.Ok);
        } else if (isAgreeInsurance) {
            togglePaymentButton(false);
            $(".loading-overlay").hide();
            // alertify.alert(Vue_FlightBook.Alert,Vue_FlightBook.checkAgrementValidation).set('closable', false).set('label', Vue_FlightBook.Ok);
            alertify.alert("Please agree with inusrance agreement condition.").set('closable', false).set('label', Vue_FlightBook.Ok);
        } else if (isValidData) {
            continueBooking();
        } 
    }, 100);
}

function bookFlightFun() {
    var bookRq = generatingAirBookingRequest();
    console.log(bookRq);
    if (Vue_FlightBook.toggleInsurance) {
        sessionStorage.insuranceRQ = JSON.stringify(Vue_FlightBook.purchase());
    }
    if (isNullorUndefined(bookRq)) {
        togglePaymentButton(false);
        return;
    }
    var paymentDetails = { "totalAmount": -188.88, "customerEmail": "test@test.com", "bookingReference": "testRef", "cartID": "" };
    var returnURL = window.location.origin + "/Flights/flight-confirmation.html";
    var huburl = ServiceUrls.hubConnection.baseUrl;
    var portno = ServiceUrls.hubConnection.ipAddress;
    var requrl = ServiceUrls.hubConnection.hubServices.flights.airBooking;
    $.ajax({
        headers: {
            'Authorization': 'Bearer ' + localStorage.access_token
        },
        type: "POST",
        url: huburl + portno + requrl,
        data: JSON.stringify(bookRq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            
            try {
                var bookData = {
                    BkngRefID: response.response.content.bookFlightResult.bookingRefId,
                    emailId: '',
                    redirectFrom: 'booking',
                    isMailsend: false,
                    passengerTypeQuantity: {
                        adt: selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.adt,
                        chd: selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.chd,
                        inf: selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.inf
                    }
                };
                window.sessionStorage.setItem("selectCredential", JSON.stringify( response.response.selectCredential));
                var status = '101';
                switch (response.response.content.bookFlightResult.status) {
                    case 'HK':
                        status = '101';
                        break;
                    case 'NF':
                        status = '102';
                        break;
                    default:
                        status = '100';
                        break;
                }
                localStorage.bookData = JSON.stringify(bookData);
                localStorage.access_token = response.response.token;
                console.log(JSON.stringify(response));
                if (status == '101') {
                    paymentDetails.bookingReference = bookData.BkngRefID;
                    if (Vue_FlightBook.toggleInsurance) {
                        paymentDetails.totalAmount = parseFloat(totalFare) + parseFloat(Vue_FlightBook.selectedPlan.totalPremiumAmount);
                    } else {
                        paymentDetails.totalAmount = totalFare;
                    }
                    paymentDetails.customerEmail = $('#emailtxt').val();
                    paymentDetails.currentPayGateways = currentPayGateways;
                    paymentDetails.currency = currentUser.loginNode.currency ? currentUser.loginNode.currency : 'USD';
                    paymentDetails.phoneNum = $('#phonetxt').val();

                    sessionStorage.currentUrl = window.location.href;
                    localStorage.cartEmailId = $('#emailtxt').val();
                    localStorage.bkRefNo = bookData.BkngRefID;
                    localStorage.serviceId = 1;
                    localStorage.bookType = Vue_FlightBook.bookType;

                    if (Vue_FlightBook.toggleInsurance) { 
                        var tpGetCurrencyCode = ServiceUrls.hubConnection.hubServices.insurance.purchase;

                        axios.post(huburl + portno + tpGetCurrencyCode, JSON.parse(sessionStorage.insuranceRQ), {
                            headers: {
                                Authorization: "Bearer " + localStorage.access_token
                            }
                        }).then(function (insRes) {
                            console.log(insRes.data.response.content.bookingRef);
                            sessionStorage.insResbookingRef = insRes.data.response.content.bookingRef;
                            paymentDetails.insuranceBookRef = insRes.data.response.content.bookingRef.split("-")[1];
                            if (Vue_FlightBook.bookType == 1) {
                                window.location.href = "/Flights/flight-confirmation.html?status=101";
                            } else {
                                //Un COmment for merchant start
                                if (currentPayGateways != undefined) {
                                    if (currentPayGateways.length > 0) {
                                        if (currentPayGateways[0].id == 9) {
                                            //Merch Integration
                                            localStorage.bkRefNo = bookData.BkngRefID;
                                            localStorage.serviceId = 1;
                                            window.location.href = '/pay.html';
                                        } else {
                                            //Redirection
                                            if (currentPayGateways.length > 1) {
                                                sessionStorage.pgDetails = JSON.stringify({
                                                    pg: paymentDetails,
                                                    retUrl: returnURL
                                                });
                                                window.location.href = '/select-payment.html';
                                            } else {
                                                paymentDetails.currentPayGateways = currentPayGateways;
                                            }
                                            var status = paymentManager(paymentDetails, returnURL, window);
                                        }
                                    } else {
                                        alertify.alert(Vue_FlightBook.Alert, 'Payment Gateway Not Available.').setting({ 'closable': false, onclose: function () { window.location = '/Flights/flight-confirmation.html?status=102'; } }).set('label', Vue_FlightBook.Ok);
                                    }
                                }
                                else {
                                    alertify.alert(Vue_FlightBook.Alert, 'Payment Gateway Not Available.').setting({ 'closable': false, onclose: function () { window.location = '/Flights/flight-confirmation.html?status=102'; } }).set('label', Vue_FlightBook.Ok);
                                }
                                //Un COmment for merchant End
                            }
                        }).catch(function (error) {});
                    } else {
                        if (Vue_FlightBook.bookType == 1) {
                            window.location.href = "/Flights/flight-confirmation.html?status=101";
                        } else {
                            //Un COmment for merchant start
                            if (currentPayGateways != undefined) {
                                if (currentPayGateways.length > 0) {
                                    if (currentPayGateways[0].id == 9) {
                                        //Merch Integration
                                        localStorage.bkRefNo = bookData.BkngRefID;
                                        localStorage.serviceId = 1;
                                        window.location.href = '/pay.html';
                                    } else {
                                        //Redirection
                                        if (currentPayGateways.length > 1) {
                                            sessionStorage.pgDetails = JSON.stringify({
                                                pg: paymentDetails,
                                                retUrl: returnURL
                                            });
                                            window.location.href = '/select-payment.html';
                                        } else {
                                            paymentDetails.currentPayGateways = currentPayGateways;
                                        }
                                        var status = paymentManager(paymentDetails, returnURL, window);
                                    }
                                } else {
                                    alertify.alert(Vue_FlightBook.Alert, 'Payment Gateway Not Available.').setting({ 'closable': false, onclose: function () { window.location = '/Flights/flight-confirmation.html?status=102'; } }).set('label', Vue_FlightBook.Ok);
                                }
                            }
                            else {
                                alertify.alert(Vue_FlightBook.Alert, 'Payment Gateway Not Available.').setting({ 'closable': false, onclose: function () { window.location = '/Flights/flight-confirmation.html?status=102'; } }).set('label', Vue_FlightBook.Ok);
                            }
                            //Un COmment for merchant End
                        }
                    }


                } else if (status == '102') {
                    window.location.href = "/Flights/flight-confirmation.html?status=102";
                } else {
                    showHideConfirmLoader(false);
                    alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.Message_UnexpectSupplierIssue).setting({ 'closable': false, onclose: function () { window.location = '/'; } }).set('label', Vue_FlightBook.Ok);
                }
            } catch (err) {
                showHideConfirmLoader(false);
                alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.Booking_failed).setting({ 'closable': false, onclose: function () { window.location = '/'; } }).set('label', Vue_FlightBook.Ok);
            }
        },
        failure: function (response) {
            console.log(JSON.stringify(response));
        },
        statusCode: {
            500: function () {
            }
        }
    });
}

$("#phonetxt").intlTelInput({
    initialCountry: currentUser.loginNode.country.code || "AE",
    geoIpLookup: function (callback) {
        $.get('https://ipinfo.io', function () { }, "jsonp").always(function (resp) {
            var countryCode = (resp && resp.country) ? resp.country : "";
            (countryCode);
        });
    },
    separateDialCode: true,
    autoPlaceholder: "off",
    preferredCountries: ['ae', 'sa', 'om', 'qa'],
    utilsScript: "../assets/js/intlTelInput.js?v=201903150000" // just for formatting/placeholders etc
});

function validateEntries() {
    var isValidData = true;
    if (isNullorUndefined($('#emailtxt').val()) || !isValidEmail($('#emailtxt').val())) {
        toggleClassShow(('#emailtxt'), 'border_danger');
        alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.Fields_Mandatory).set("label", Vue_FlightBook.Ok);
        isValidData = false;
    }
    if (isNullorUndefined($('#phonetxt').val())) {
        toggleClassShow("#phonetxt", "border_danger");
        alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.Fields_Mandatory).set("label", Vue_FlightBook.Ok);
        isValidData = false;
    }
    var arrname = [];
    $("input.fname").each(function () {
        var value = $(this).val();
        var id = this.id;
        value = isNullorEmpty(value) ? makeid(6, 2) : value.trim().toLowerCase();
        arrname.push({
            fname: value,
            fnameid: '#' + id
        });
    });
    var lnameindex = 0;
    $("input.lname").each(function () {
        var value = $(this).val();
        var id = this.id;
        value = isNullorEmpty(value) ? makeid(6, 2) : value.trim().toLowerCase();
        arrname[lnameindex]['lname'] = value;
        arrname[lnameindex]['lnameid'] = '#' + id;
        lnameindex++;
    });
    for (i = 0; i < arrname.length - 1; i++) {
         
        for (j = i + 1; j < arrname.length; j++) {
             
            if ((arrname[i]['fname'] == arrname[j]['fname']) && (arrname[i]['lname'] == arrname[j]['lname'])) {
                 
                isDuplicateName = true;
                DuplicateNames.push(i + 1);
                DuplicateNames.push(j + 1);
                toggleClassShow((arrname[i]['fnameid']), 'border_danger');
                toggleClassShow((arrname[i]['lnameid']), 'border_danger');
                toggleClassShow((arrname[j]['fnameid']), 'border_danger');
                toggleClassShow((arrname[j]['lnameid']), 'border_danger');
            }
        }
    }
    if (isDuplicateName) {
        return;
    }
    // if (!$("#chkAgree").is(':checked')) {
    //     toggleClassShow(('[name="checkbox-list-09"]'), 'border_danger');
    //     isValidData = false;
    // }
    if (isValidData) {
        var airSupplierResponse = selectResponseObj.content;
        var passengerTypeQuantity = airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity;
        var adult = passengerTypeQuantity.adt;
        var child = passengerTypeQuantity.chd;
        var infant = passengerTypeQuantity.inf;
        var totalPassengers = parseInt(adult) + parseInt(child) + parseInt(infant);
        var adultIndex = 1;
        var childIndex = 1;
        var infantIndex = 1;
        var currentIndex = 1;
        for (var i = 1; i <= totalPassengers; i++) {
            var paxType = (i <= parseInt(adult) ? 'Adult' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'Child' : 'Infant'));
            var paxTypeCode = (i <= parseInt(adult) ? 'ADT' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'CHD' : 'INF'));
            switch (paxTypeCode) {
                case 'ADT':
                    currentIndex = adultIndex;
                    adultIndex++;
                    break;
                case 'CHD':
                    currentIndex = childIndex;
                    childIndex++;
                    break;
                case 'INF':
                    currentIndex = infantIndex;
                    infantIndex++;
                    break;
            }
            var paxTypeIndex = paxTypeCode + currentIndex;
            if (isNullorEmptyToBlank($('#txtFname' + paxTypeIndex).val()) == '') {
                toggleClassShow(('#txtFname' + paxTypeIndex), 'border_danger');
                alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.Fields_Mandatory).set("label", Vue_FlightBook.Ok);
                isValidData = false;
            }
            if (isNullorEmptyToBlank($('#txtSname' + paxTypeIndex).val()) == '') {
                toggleClassShow(('#txtSname' + paxTypeIndex), 'border_danger');
                alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.Fields_Mandatory).set("label", Vue_FlightBook.Ok);
                isValidData = false;
            }
            var txtSname = $('#txtSname' + paxTypeIndex).val();
            if (txtSname.length < 2) {
                toggleClassShow(('#txtSname' + paxTypeIndex), 'border_danger');
                alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.Min_TwoLastName).set('label', Vue_FlightBook.Ok);
                isValidData = false;
            }
            if (/\s\s+/gi.test(txtSname)) {
                toggleClassShow(('#txtSname' + paxTypeIndex), 'border_danger');
                alertify.alert("Invalid", "Please enter single space character for last name").set("label", Vue_FlightBook.Ok);
                isValidData = false;
            }
             if (/\s\s+/gi.test($('#txtFname' + paxTypeIndex).val())) {
                toggleClassShow("#txtFname" + paxTypeIndex, "border_danger");
               alertify.alert("Invalid", "Please enter single space character for first name").set("label", Vue_FlightBook.Ok);
               isValidData = false;
             }
            if (isNullorEmptyToBlank($('#txtDob' + paxTypeIndex).val()) == '') {
                toggleClassShow(('#txtDob' + paxTypeIndex), 'border_danger');
                alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.Fields_Mandatory).set("label", Vue_FlightBook.Ok);
                isValidData = false;
            }
            if (isNullorEmptyToBlank($('#txtPassport' + paxTypeIndex).val()) == '') {
                toggleClassShow(('#txtPassport' + paxTypeIndex), 'border_danger');
                alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.Fields_Mandatory).set("label", Vue_FlightBook.Ok);
                isValidData = false;
            }
            var txtPn = $('#txtPassport' + paxTypeIndex).val();
            if (txtPn.length > 20) {
                toggleClassShow(('#txtPassport' + paxTypeIndex), 'border_danger');
                alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.Max_TwentyChar).set('label', Vue_FlightBook.Ok);
                isValidData = false;
            }
            if (isNullorEmptyToBlank($('#txtPassportIssue' + paxTypeIndex).val()) == '') {
                alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.Fields_Mandatory).set("label", Vue_FlightBook.Ok);
                toggleClassShow(('#txtPassportIssue' + paxTypeIndex), 'border_danger');
                isValidData = false;
            }
            if (isNullorEmptyToBlank($('#txtExpireDate' + paxTypeIndex).val()) == '') {
                toggleClassShow("#txtExpireDate" + paxTypeIndex, "border_danger");
                alertify.alert(Vue_FlightBook.Alert, Vue_FlightBook.Fields_Mandatory).set("label", Vue_FlightBook.Ok);
                isValidData = false;
            }
        }
    }
    return isValidData;
}
function validateagrement() {
    var agreee = false;
    if (!$("#chkAgree").is(':checked')) {
        toggleClassShow(('[name="checkbox-list-09"]'), 'border_danger');
        agreee = true;
    }
    return agreee;
}
function validateagrementInsurance() {
    var agreee = false;
    if (Vue_FlightBook.toggleInsurance) {
        if (!$("#chkAgree-insurance").is(':checked')) {
            toggleClassShow(('[name="checkbox-list-09"]'), 'border_danger');
            agreee = true;
        }
    }
    
    return agreee;
}


function passengerDetails() {
    var airSupplierResponse = selectResponseObj.content;
    var passengerTypeQuantity = airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity;
    var dateFormatFrom = 'DD/MM/YYYY';
    var adult = passengerTypeQuantity.adt;
    var child = passengerTypeQuantity.chd;
    var infant = passengerTypeQuantity.inf;
    var totalPassengers = parseInt(adult) + parseInt(child) + parseInt(infant);
    var adultIndex = 1;
    var childIndex = 1;
    var infantIndex = 1;
    var currentIndex = 1;
    var travellers = [];
    adultIndex = 1;
    childIndex = 1;
    infantIndex = 1;
    currentIndex = 1;
    var details = [];
    
    for (var i = 1; i <= totalPassengers; i++) {
        var paxType = (i <= parseInt(adult) ? 'Adult' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'Child' : 'Infant'));
        var paxTypeCode = (i <= parseInt(adult) ? 'ADT' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'CHD' : 'INF'));
        switch (paxTypeCode) {
            case 'ADT':
                currentIndex = adultIndex;
                adultIndex++;
                break;
            case 'CHD':
                currentIndex = childIndex;
                childIndex++;
                break;
            case 'INF':
                currentIndex = infantIndex;
                infantIndex++;
                break;
        }
        var paxTypeIndex = paxTypeCode + currentIndex;
        var paxTitle = $('#ddltitle' + paxTypeIndex).val();

        var frequentFlayerNumber = $('#txtFreqFlyNumber' + paxTypeIndex).val();
        var frequentFlayerAirlineCode = $('#ddlFreqFlyAirLine' + paxTypeIndex).val();

       var isLead =  Vue_CoTraveller.coTravellers.filter(function (e) {
            return e.lead == true;
        });
        var datas = {
            title: paxTitle.toLowerCase() == "mr" ?
                1 : paxTitle.toLowerCase() == "miss" ?
                    2 : paxTitle.toLowerCase() == "ms" ?
                        4 : paxTitle.toLowerCase() == "mrs" ?
                            3 : paxTitle.toLowerCase() == "mstr" ? 5 : 1,
            firstName: $('#txtFname' + paxTypeIndex).val().trim(),
            lastName: $('#txtSname' + paxTypeIndex).val().trim(),
            nationality: $('#txtnationality' + paxTypeIndex).val(),
            paxType: paxTypeCode,
            dob: moment($('#txtDob' + paxTypeIndex).val(), dateFormatFrom).format('YYYY-MM-DD'),
            passportNumber: $('#txtPassport' + paxTypeIndex).val(),
            issuingCountry: $('#ddlPassportIssue' + paxTypeIndex).val(),
            passportIssueDate: moment($('#txtPassportIssue' + paxTypeIndex).val(), dateFormatFrom).format('YYYY-MM-DD'),
            passportExpDate: moment($('#txtExpireDate' + paxTypeIndex).val(), dateFormatFrom).format('YYYY-MM-DD'),
            lead: isLead.length>=1? 0:1,
            frequentFlyerList: [{
                frequentFlyerCode: (frequentFlayerNumber == undefined || frequentFlayerNumber == '') ? null : frequentFlayerNumber,
                airLine: {
                    code:(frequentFlayerAirlineCode == undefined || frequentFlayerAirlineCode == '') ? null : frequentFlayerAirlineCode
                }
            }
            ]
        };

        var newCotraveler = Vue_CoTraveller.coTravellers.map(function(e){
            return {
                id: e.id,
                data: {
                    title: e.title.toLowerCase() == "mr" ?
                        1 : e.title.toLowerCase() == "miss" ?
                            2 : e.title.toLowerCase() == "ms" ?
                                4 : e.title.toLowerCase() == "mrs" ?
                                    3 : e.title.toLowerCase() == "mstr" ? 5 : 1,
                    firstName: e.firstName,
                    lastName: e.lastName,
                    nationality: e.nationality,
                    paxType: e.paxType,
                    dob: e.dob,
                    passportNumber: e.passportNumber,
                    issuingCountry: e.issuingCountry,
                    passportIssueDate: e.passportIssueDate,
                    passportExpDate: e.passportExpDate,
                    lead: isLead.length>=1? 0:1,
                    frequentFlyerList: [{
                        frequentFlyerCode: (frequentFlayerNumber == undefined || frequentFlayerNumber == '') ? null : frequentFlayerNumber,
                        airLine: {
                            code:(frequentFlayerAirlineCode == undefined || frequentFlayerAirlineCode == '') ? null : frequentFlayerAirlineCode
                        }
                    }]
                }
            }
        })
        // console.log(newCotraveler);
        var addPax = true;
        for (var z = 0; z < newCotraveler.length; z++) {
            var name = newCotraveler[z].data.firstName + newCotraveler[z].data.lastName;
            if(JSON.stringify(newCotraveler[z].data) != JSON.stringify(datas)&&(name.toLowerCase() == (datas.firstName + datas.lastName).toLowerCase())){
                datas.id = newCotraveler[z].id;
                break;
            } else if (JSON.stringify(newCotraveler[z].data) == JSON.stringify(datas) && (name.toLowerCase() == (datas.firstName + datas.lastName).toLowerCase())){
                addPax = false;
                break;
            }
        }

        if (addPax) {     
            var axiosConfig = {
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8',
                    'Authorization': "Bearer " + localStorage.access_token
                }
            };
            var huburl = ServiceUrls.hubConnection.baseUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var requrl = ServiceUrls.hubConnection.hubServices.addCoTraveller;

            details.push({ url: huburl + portno + requrl, data: datas, axiosConfig});
        }

    }

    return details;
}

function generatingAirBookingRequest() {
    var airSupplierResponse = selectResponseObj.content;
    var dateFormatFrom = 'DD/MM/YYYY';
    var paxFares = airSupplierResponse.fareInformationWithoutPnrReply.paxFareDetails;
    var airResposneLegs = airSupplierResponse.fareInformationWithoutPnrReply.airSegments;
    var airSectoridies = _.pluck(airResposneLegs, 'segDirectionID'); airSectoridies = _.uniq(airSectoridies);

    //var flagCountryCode = $("#txtPhoneNumber").intlTelInput("getSelectedCountryData").iso2;
    var flagCountryCode = $('.selected-flag>.iti-flag').attr('class').split(' ')[1];
    // var flagCountryCode = $(".selected-dial-code").text().substring(1) || "";
    var telcode = flagCountryCode.toUpperCase();
    var phno = $('#phonetxt').val();

    var flightid = "";
    if (airSupplierResponse.fareInformationWithoutPnrReply.fSupplier == "18") {
        flightid = window.localStorage.getItem("flightID");
    }

    var passengerTypeQuantity = airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity;
    var adult = passengerTypeQuantity.adt;
    var child = passengerTypeQuantity.chd;
    var infant = passengerTypeQuantity.inf;
    var totalPassengers = parseInt(adult) + parseInt(child) + parseInt(infant);
    var adultIndex = 1;
    var childIndex = 1;
    var infantIndex = 1;
    var currentIndex = 1;
    var isValidData = true;

    var airLineLegs = [];
    for (var i = 0; i < airSectoridies.length; i++) {
        var segments = airResposneLegs.filter(function (segment) { return segment.segDirectionID == airSectoridies[i] });
        var airLineSegments = [];
        var flightDetails = selectFlightFromSessionObj.selectedFlightInfo.groupOfFlights[i].flightDetails;

        $.each(segments, function (segIndex, segment) {
            var terminalFrom = '';
            var terminalTo = '';
            try { terminalFrom = isNullorUndefined(segment.segInfo.flData.boardTerminal) ? flightDetails[segIndex].fInfo.location.fromTerminal : segment.segInfo.flData.boardTerminal; } catch (err) { }
            try { terminalTo = isNullorUndefined(segment.segInfo.flData.offTerminal) ? flightDetails[segIndex].fInfo.location.toTerminal : segment.segInfo.flData.offTerminal; } catch (err) { }
            var airLineSegment = {
                fareBasis: stringIsNullorEmpty(segment.fareId) ? null : segment.fareId,
                departureDate: moment(segment.segInfo.dateandTimeDetails.departureDate, 'DDMMYY').format('DD-MM-YYYY'),
                departureTime: segment.segInfo.dateandTimeDetails.departureTime,
                arrivalDate: moment(segment.segInfo.dateandTimeDetails.arrivalDate, 'DDMMYYYY').format('DD-MM-YYYY'),
                arrivalTime: segment.segInfo.dateandTimeDetails.arrivalTime,
                departureFrom: segment.segInfo.flData.boardPoint,
                departureTo: segment.segInfo.flData.offpoint,
                marketingCompany: segment.segInfo.flData.marketingCompany,
                operatingCompany: segment.segInfo.flData.operatingCompany,
                validatingCompany: segment.segInfo.flData.validatingCompany,
                flightNumber: segment.segInfo.flData.flightNumber,
                bookingClass: !stringIsNullorEmpty(segment.segInfo.flData.bookingClass) ? segment.segInfo.flData.bookingClass : segment.segInfo.flData.rbd,
                rbd: !stringIsNullorEmpty(segment.segInfo.flData.rbd) ? segment.segInfo.flData.rbd : segment.segInfo.flData.bookingClass,
                terminalTo: terminalTo,
                terminalFrom: terminalFrom,
                flightEquip: segment.segInfo.flData.equiptype
            }
            airLineSegments.push(airLineSegment);
        });

        var airLineLeg = {
            elapsedTime: null,
            from: segments[0].originCity,
            to: segments[segments.length - 1].departureCity,
            segments: airLineSegments
        }
        airLineLegs.push(airLineLeg);
    }

    var travellers = [];
    adultIndex = 1;
    childIndex = 1;
    infantIndex = 1;
    currentIndex = 1;
    for (var i = 1; i <= totalPassengers; i++) {
        var paxType = (i <= parseInt(adult) ? 'Adult' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'Child' : 'Infant'));
        var paxTypeCode = (i <= parseInt(adult) ? 'ADT' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'CHD' : 'INF'));
        switch (paxTypeCode) {
            case 'ADT':
                currentIndex = adultIndex;
                adultIndex++;
                break;
            case 'CHD':
                currentIndex = childIndex;
                childIndex++;
                break;
            case 'INF':
                currentIndex = infantIndex;
                infantIndex++;
                break;
        }
        var paxTypeIndex = paxTypeCode + currentIndex;
        var frequentFlayerNumber = $('#txtFreqFlyNumber' + paxTypeIndex).val();
        var frequentFlayerAirlineCode = $('#ddlFreqFlyAirLine' + paxTypeIndex).val();
        var paxTitle = $('#ddltitle' + paxTypeIndex).val();
        var paxGender = (paxTitle == 'Mr' || paxTitle == 'Mstr') ? 'Male' : 'Female';

        var traveller = {
            passengerType: paxTypeCode,
            gender: paxGender,
            givenName: $('#txtFname' + paxTypeIndex).val().trim(),
            namePrefix: paxTitle,
            surName: $('#txtSname' + paxTypeIndex).val().trim(),
            quantity: 1,
            birthDate: moment($('#txtDob' + paxTypeIndex).val(), dateFormatFrom).format('DD-MM-YYYY'),
            docType: '1',
            docID: $('#txtPassport' + paxTypeIndex).val(),
            issuanceDate: moment($('#txtPassportIssue' + paxTypeIndex).val(), dateFormatFrom).format('DD-MM-YYYY'),
            docIssueCountry: $('#ddlPassportIssue' + paxTypeIndex).val(),
            expireDate: moment($('#txtExpireDate' + paxTypeIndex).val(), dateFormatFrom).format('DD-MM-YYYY'),
            issuanceCity: 'India',
            nationalityData: {
                twoLetter: $('#txtnationality' + paxTypeIndex).val(),
                threeLetter: $('#txtnationality' + paxTypeIndex + '>option:selected').data('letter3')
            },
            frequentFlyerNumber: (frequentFlayerNumber == undefined || frequentFlayerNumber == '') ? null : frequentFlayerNumber,
            ffAirlineCode: (frequentFlayerAirlineCode == undefined || frequentFlayerAirlineCode == '') ? null : frequentFlayerAirlineCode
        }

        var contact = null;
        if (paxTypeIndex === 'ADT1') {

            contact = {
                phoneList: [{
                    number: $('#phonetxt').val(),
                    country: {
                        code: ($('.iti-flag').attr('class').split(" ")[1]||"").toUpperCase(),
                        telephonecode: $('.selected-dial-code').text().replace('+', '')
                    },
                    phoneType: { id: 1 }
                }],
                emailList: [{
                    emailId: $('#emailtxt').val(),
                    emailType: { id: 1 }
                }]
            };

            contact.phoneList = contact.phoneList;
            contact.emailList = contact.emailList;
        }

        traveller['contact'] = contact;
        travellers.push(traveller);
        
    }
    Vue_FlightBook.travellersInsurance = travellers.slice();

    var airSegments = airSupplierResponse.fareInformationWithoutPnrReply.airSegments;
    var airLineBaggages = [];

    $.each(airSegments, function (index, airSegment) {
        $.each(airSegment.baggageInformations, function (bagIndex, baggage) {
            var airLineBaggage = {
                paxType: baggage.bagPaxType,
                cabinBaggageQuantity: '7',
                cabinBaggageUnit: 'KG',
                checkinBaggageQuantity: baggage.baggage,
                checkinBaggageUnit: baggage.unit,
                fromSeg: airSegment.segInfo.flData.boardPoint,
                toSeg: airSegment.segInfo.flData.offpoint
            }
            airLineBaggages.push(airLineBaggage);
        });
    });

    var airFareRules = [];
    var miniRules = [];
    try { miniRules = airSupplierResponse.fareInformationWithoutPnrReply.minirules } catch (err) { miniRules = []; }
    if (isNullorUndefined(miniRules)) {
        miniRules = [];
    }

    $.each(airSupplierResponse.fareInformationWithoutPnrReply.airFareRule, function (index, fareRule) {
        $.each(fareRule.fareRuleDetails, function (ruleIndex, rule) {
            if (!stringIsNullorEmpty(rule.ruleBody) && !stringIsNullorEmpty(rule.rulehead)) {
                var airFareRule = {
                    fareRule: rule.ruleBody,
                    fareRef: rule.rulehead,
                    segment: fareRule.departure + '-' + fareRule.arrival,
                    filingAirline: null,
                    marketingAirline: fareRule.airline
                };
                airFareRules.push(airFareRule);
            }
        });

    });
    var supplierSpecific = airSupplierResponse.supplierSpecific ? airSupplierResponse.supplierSpecific : {}
    var userNode = JSON.parse(localStorage.User);
    var smsNotification = {
        countryCode: $('.selected-dial-code').text() || "",
        number: $('#phonetxt').val() || "",
        paxName: $('#txtFnameADT1').val().trim().toLowerCase().replace(/(^.)/, m => m.toUpperCase()) + " " + 
        $('#txtSnameADT1').val().trim().toLowerCase().replace(/(^.)/, m => m.toUpperCase())
    }
    localStorage.smsNotification = JSON.stringify(smsNotification);
    var airBookingRequest = {
        request: {
            service: 'FlightRQ',
            supplierCodes: [airSupplierResponse.fareInformationWithoutPnrReply.fSupplier],
            content: {
                command: 'FlightBookRQ',
                supplierSpecific: supplierSpecific,
                bookFlightRQ: {
                    airBagDetails: airLineBaggages,
                    smsNotification: $('#phonetxt').val() && Vue_FlightBook.haveSmsNotification ? smsNotification : undefined,
                    bookFlightEntity: {
                        bookingRefId: null,
                        bookFlight: {
                            dealCode: airSupplierResponse.fareInformationWithoutPnrReply.dealCode,
                            supplierId: airSupplierResponse.fareInformationWithoutPnrReply.fSupplier,
                            supplierRemarks: airSupplierResponse.fareInformationWithoutPnrReply.fareFareSourceCode,
                            otherDetails: null,
                            reference: null,
                            comments: airSupplierResponse.fareInformationWithoutPnrReply.fareAgencyCode,
                            supplierSpecific: supplierSpecific,
                            tripType: airSupplierResponse.fareInformationWithoutPnrReply.tripType.toUpperCase(),
                            faresourceCode:airSupplierResponse.fareInformationWithoutPnrReply.fSupplier==1? (airSupplierResponse.fareInformationWithoutPnrReply.airSegments[0].fareSourceCode?airSupplierResponse.fareInformationWithoutPnrReply.airSegments[0].fareSourceCode:null):airSupplierResponse.fareInformationWithoutPnrReply.fareFareSourceCode,
                            sessionId: airSupplierResponse.fareInformationWithoutPnrReply.fareSessionId,
                            abc: airSupplierResponse.fareInformationWithoutPnrReply.fareFareSourceCode,
                            addressLine: [
                                "Abc",
                                "Xyz"],
                            fdetails: {
                                flightId: '',
                                clientIp: '',
                                confirmedCurrency: currentUser.loginNode.currency
                            },
                            travelerInfo: travellers,
                            adt: airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity.adt,
                            chd: airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity.chd,
                            inf: airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity.inf,
                            flLegGroup: airLineLegs
                        }
                    },
                    paymentMode: Vue_FlightBook.bookType == 1 ? 7 : 6,
                    totalfaregroup: {
                        totalBaseNet: parseFloat(airSupplierResponse.fareInformationWithoutPnrReply.flightFareDetails.basefare),
                        actualBaseNet: 0,
                        totalTaxNet: parseFloat(airSupplierResponse.fareInformationWithoutPnrReply.flightFareDetails.taxFare),
                        actualTaxNet: 0,
                        netCurrency: airSupplierResponse.fareInformationWithoutPnrReply.flightFareDetails.currency,
                        paidDate: null,
                        paidAmount: parseFloat(airSupplierResponse.fareInformationWithoutPnrReply.flightFareDetails.totalFare),
                        paymentTypeId: 1
                    },
                    fareRuleSeg: airFareRules,
                    miniRules: miniRules,
                    sealed: airSupplierResponse.fareInformationWithoutPnrReply.sealed,
                    userContactDetails: {
                        countryCode: telcode,
                        contactNumber: phno
                    }
                }
            },
            selectCredential:selectResponseObj.selectCredential,
            hqCode:userNode.loginNode.solutionId ? userNode.loginNode.solutionId :"",

        }
    }

    return airBookingRequest;
}

function toggleClassShow(obj, className) {
    // $('html, body').animate({
    //     scrollTop: $(obj).offset().top
    // }, 20);
    $(obj).addClass(className);
}

function toggleClassHide(obj, className) {
    //setTimeout(function () {
    $(obj).removeClass(className);
    //}, 4000);
}

function autoFillTravellers() {
    var airRevalidate = selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate;
    var adult = airRevalidate.adt;
    var child = airRevalidate.chd;
    var infant = airRevalidate.inf;
    var totalPassengers = parseInt(adult) + parseInt(child) + parseInt(infant);
    var url = window.location.href.toLowerCase();
    if (url.includes('?autofill')) {
        var adultIndex = 1;
        var childIndex = 1;
        var infantIndex = 1;
        var currentIndex = 1;
        for (var i = 1; i <= totalPassengers; i++) {
            var paxType = (i <= parseInt(adult) ? 'Adult' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'Child' : 'Infant'));
            var paxTypeCode = (i <= parseInt(adult) ? 'ADT' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'CHD' : 'INF'));
            switch (paxTypeCode) {
                case 'ADT':
                    currentIndex = adultIndex;
                    adultIndex++;
                    break;
                case 'CHD':
                    currentIndex = childIndex;
                    childIndex++;
                    break;
                case 'INF':
                    currentIndex = infantIndex;
                    infantIndex++;
                    break;
            }
            var paxTypeIndex = paxTypeCode + currentIndex;
            $('#txtPassport' + paxTypeIndex).val(paxTypeIndex + makeid(5, 3) + i);
            $('#txtFname' + paxTypeIndex).val('Name' + makeid(6, 2));
            $('#txtSname' + paxTypeIndex).val('Sur' + makeid(6, 2));
            var todaydate = new Date();
            var new_date = todaydate.getDate() + '-' + (todaydate.getMonth() + 1).toString().padStart(2, '0') + '-' + todaydate.getFullYear();
            $('#txtExpireDate' + paxTypeIndex).val(moment(new_date, "DD-MM-YYYY").add('year', 10).format("DD/MM/YYYY"));
            $('#txtPassportIssue' + paxTypeIndex).val(moment(new_date, "DD-MM-YYYY").add('year', -1).format("DD/MM/YYYY"));
            if (paxTypeCode == 'ADT') {
                $('#txtDob' + paxTypeIndex).val(moment(new_date, "DD-MM-YYYY").add('year', -25).format("DD/MM/YYYY"));
            } else if (paxTypeCode == 'CHD') {
                $('#txtDob' + paxTypeIndex).val(moment(new_date, "DD-MM-YYYY").add('year', -11).format("DD/MM/YYYY"));
            } else {
                $('#txtDob' + paxTypeIndex).val(moment(new_date, "DD-MM-YYYY").add('year', -1).format("DD/MM/YYYY"));
            }
        }
    }
}

function makeid(length, idtype) {
    var text = "";
    var possibleAlpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    var possibleNumeric = "0123456789";
    var possible = '';
    switch (idtype) {
        case 1:
            possible = possibleAlpha + possibleNumeric;
            break;
        case 0:
        case 2:
            possible = possibleAlpha;
            break;
        case 3:
            possible = possibleNumeric;
            break;
    }

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function showHideLoader(isShow) {
    if (isNullorUndefined(isShow)) isShow = false;
    if (isShow) {
        $('.load-anim').show();
    } else {
        $('.load-anim').hide();
    }
}

function getUser() {
    if (localStorage.IsLogin) {
        var user = JSON.parse(localStorage.User);
        $('#emailtxt').val(user.emailId);
        $('#phonetxt').val(user.contactNumber);
        return user;
    }
    return null;
}

function checkLoginLocal() {
    if (localStorage.IsLogin === "true") {
        return true;
    }
    else {
        return false;
    }
}

function getSuppliers(serviceName) {
    var suppliers = [];
    if (!isNullorUndefined(serviceName)) {
        $.each(currentUser.loginNode.servicesList.filter(function (service) { return service.name === serviceName; })[0].provider, function (providerindex, provider) {
            suppliers.push(provider.id);
        });
    }
    return suppliers;
}

function getPayGateways() {
    if (currentUser.loginNode.paymentGateways != null) {
        return currentUser.loginNode.paymentGateways;
    }
}

var signArea = new Vue({
    el: '#sign-section',
    name: 'SignArea',
    data: {
        username: '',
        password: '',
        userLogined: checkLoginLocal(),
        userinfo: [],
        Welcome: 'Welcome',
        welcomeUser: '',
        Review_Flight: '',
        Traveller_Details: '',
        Flight_Booking: '',
        Payment: '',
        Confirmation: '',
        Log_out: '',
        Signin_ProcessLabel: '',
        Sign_In: '',
        Email_RequiredLabel: '',
        Password_RequiredLabel: '',
        Account_NotAvailable: '',
        Account_ProcessLabel: '',
        to: '',
        stop: '',
        Fare_Rules: '',
        Baggage: '',
        Refundable: '',
        Non_Refundable: '',
        Refundable_withPenalty: '',
        No_cancellation: '',
        Fare_Rule: '',
        Before_Departure: '',
        After_Departure: '',
        Allowed_for: '',
        Flight_from: '',
        Checked_Baggage: '',
        Free: '',
        Cabin_Baggage: '',
        No_baggage: '',
        Provide_TravellerDetails: '',
        NationalIdPassport_Warning: '',
        Required_field: '',
        Title_Label: '',
        FirstName_Label: '',
        LastName_Label: '',
        DoB_Label: '',
        Nationality_Label: '',
        PassportNumber_Label: '',
        ExpireDate_Label: '',
        IssueDate_Label: '',
        IssueCountry_Label: '',
        Frequentflyer_program: '',
        Frequentflyer_number: '',
        Email: '',
        FlightTicket_RegisteredEmail: '',
        PhoneNumber_Label: '',
        Traveller: '',
        Agree_WhatsApp: '',
        TripSummary_Detail: '',
        Tickets: '',
        Fare: '',
        Taxes_Fees: '',
        Service_Fee: '',
        Total: '',
        Taxesfees_included: '',
        Travellers: '',
        FareChange_Alert: '',
        Updated_Fare: '',
        Original_Fare: '',
        Cancel: '',
        Continue: '',
        Reviewyour_flight: '',
        Continue_ToPayment: '',
        close: '',
        Alert: '',
        Confirm: '',
        Message_UnexpectSupplierIssue: '',
        Provide_Email: '',
        Provide_PhoneNumber: '',
        Thank_Registerwithus: '',
        Email_AlreadyRegistered: '',
        Fields_Mandatory: '',
        Booking_failed: '',
        Max_TwentyChar: '',
        Username_required: '',
        Password_required: '',
        Login_failed: '',
        Password: '',
        FrequentNumber_Required: '',
        Welcome: '',
        Ok: '',
        GoBack: '',
        AddFrequent: ''
    },
    mounted() {
        try {
            var payvisitCHeck = sessionStorage.getItem("payvisit");
            if (payvisitCHeck==="true") {
                window.location.href = "/index.html"
            }
        } catch (error) {
            console.log(error)
        }
        this.getPagecontent();
        if (localStorage.IsLogin === "true") {
            $('#emailtxt').val(currentUser.emailId);
            $('#phonetxt').val(currentUser.contactNumber);
        }
    },
    methods: {
        headerLogin: function (userDetails) {
            this.username = userDetails.userName;
            this.password = userDetails.password;
            this.signInbook();
        },
        signInbook: function () {
            var self = this;

            if (!this.username) {
                alertify.alert(self.Alert, self.Username_required).set('closable', false).set('label', self.Ok);
            }
            else if (!this.password) {
                alertify.alert(self.Alert, self.Password_required).set('closable', false).set('label', self.Ok);
            }
            else {
                login(this.username, this.password, function (response) {
                    if (response == false && self.userLogined == false) {
                        self.userLogined = false;
                        self.logout();
                        alertify.alert(self.Alert, self.Login_failed).set('closable', false).set('label', self.Ok);
                    } else {
                        response = JSON.parse(localStorage.User);
                        console.log(response);
                        self.userLogined = true;
                        self.userinfo = response;
                        self.welcomeUser = self.Welcome + '!' + response.firstName + ' ' + response.lastName + ' <span>(' + response.emailId + ')</span>';
                        $('#bind-login-info').show();
                        $('#sign-bt-area').hide();
                        $('#modal_retrieve').hide();
                        $('#bind-login-info span').html(response.firstName + ' ' + response.lastName);
                        // $('#bind-login-info').html('<input type="checkbox" id="profile2"> <img src="https://cdn0.iconfinder.com/data/icons/avatars-3/512/avatar_hipster_guy-512.png"> <span>' + response.firstName + ' ' + response.lastName + '</span> <label for="profile2"><i class="fa fa-angle-down" aria-hidden="true"></i></label> <ul> <li><a href="/customer-profile.html"><i class="fa fa-th-large" aria-hidden="true"></i> My Dashboard</a></li> <li><a href="/my-profile.html"><i class="fa fa-user" aria-hidden="true"></i> My Profile</a></li> <li><a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i> My Bookings</a></li> <li><a href="#" v-on:click="logout"><i class="fa fa-power-off" aria-hidden="true"></i> Logout</a></li> </ul>')
                        $('#emailtxt').val(response.emailId);
                        $('#phonetxt').val(response.contactNumber);
                        Vue_CoTraveller.setCoTravellers();
                    }
                });
            }
        },
        logout: function () {
            this.userLogined = false;
            this.userinfo = [];
            commonlogin();
            Vue_CoTraveller.resetValues();
            $('#bind-login-info').hide();
            $('#sign-bt-area').show();
            $('#modal_retrieve').show();
            $('#emailtxt').val('');
            $('#phonetxt').val('');
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            sessionStorage.setItem("payvisit", false);
            var self = this;
            getAgencycode(function (response) {
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + commonAgencyCode + '/Template/Booking Details/Booking Details/Booking Details.ftl';
                axios.get(aboutUsPage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (response.data.area_List.length > 0) {
                        var mainComp = response.data.area_List[0].main;
                        self.Review_Flight = self.pluckcom('Review_Flight', mainComp.component);
                        self.Traveller_Details = self.pluckcom('Traveller_Details', mainComp.component);
                        self.Flight_Booking = self.pluckcom('Flight_Booking', mainComp.component);
                        self.Payment = self.pluckcom('Payment', mainComp.component);
                        self.Confirmation = self.pluckcom('Confirmation', mainComp.component);
                        self.Log_out = self.pluckcom('Log_out', mainComp.component);
                        self.Signin_ProcessLabel = self.pluckcom('Signin_ProcessLabel', mainComp.component);
                        self.Sign_In = self.pluckcom('Sign_In', mainComp.component);
                        self.Email_RequiredLabel = self.pluckcom('Email_RequiredLabel', mainComp.component);
                        self.Password_RequiredLabel = self.pluckcom('Password_RequiredLabel', mainComp.component);
                        self.Account_NotAvailable = self.pluckcom('Account_NotAvailable', mainComp.component);
                        self.Account_ProcessLabel = self.pluckcom('Account_ProcessLabel', mainComp.component);
                        self.to = self.pluckcom('to', mainComp.component);
                        self.stop = self.pluckcom('stop', mainComp.component);
                        self.Fare_Rules = self.pluckcom('Fare_Rules', mainComp.component);
                        self.Baggage = self.pluckcom('Baggage', mainComp.component);
                        self.Refundable = self.pluckcom('Refundable', mainComp.component);
                        self.Non_Refundable = self.pluckcom('Non_Refundable', mainComp.component);
                        self.Refundable_withPenalty = self.pluckcom('Refundable_withPenalty', mainComp.component);
                        self.No_cancellation = self.pluckcom('No_cancellation', mainComp.component);
                        self.Fare_Rule = self.pluckcom('Fare_Rule', mainComp.component);
                        self.Before_Departure = self.pluckcom('Before_Departure', mainComp.component);
                        self.After_Departure = self.pluckcom('After_Departure', mainComp.component);
                        self.Allowed_for = self.pluckcom('Allowed_for', mainComp.component);
                        self.Flight_from = self.pluckcom('Flight_from', mainComp.component);
                        self.Checked_Baggage = self.pluckcom('Checked_Baggage', mainComp.component);
                        self.Free = self.pluckcom('Free', mainComp.component);
                        self.Cabin_Baggage = self.pluckcom('Cabin_Baggage', mainComp.component);
                        self.No_baggage = self.pluckcom('No_baggage', mainComp.component);
                        self.Provide_TravellerDetails = self.pluckcom('Provide_TravellerDetails', mainComp.component);
                        self.NationalIdPassport_Warning = self.pluckcom('NationalIdPassport_Warning', mainComp.component);
                        self.Required_field = self.pluckcom('Required_field', mainComp.component);
                        self.Title_Label = self.pluckcom('Title_Label', mainComp.component);
                        self.FirstName_Label = self.pluckcom('FirstName_Label', mainComp.component);
                        self.LastName_Label = self.pluckcom('LastName_Label', mainComp.component);
                        self.DoB_Label = self.pluckcom('DoB_Label', mainComp.component);
                        self.Nationality_Label = self.pluckcom('Nationality_Label', mainComp.component);
                        self.PassportNumber_Label = self.pluckcom('PassportNumber_Label', mainComp.component);
                        self.ExpireDate_Label = self.pluckcom('ExpireDate_Label', mainComp.component);
                        self.IssueDate_Label = self.pluckcom('IssueDate_Label', mainComp.component);
                        self.IssueCountry_Label = self.pluckcom('IssueCountry_Label', mainComp.component);
                        self.Frequentflyer_program = self.pluckcom('Frequentflyer_program', mainComp.component);
                        self.Frequentflyer_number = self.pluckcom('Frequentflyer_number', mainComp.component);
                        self.Email = self.pluckcom('Email', mainComp.component);
                        self.FlightTicket_RegisteredEmail = self.pluckcom('FlightTicket_RegisteredEmail', mainComp.component);
                        self.PhoneNumber_Label = self.pluckcom('PhoneNumber_Label', mainComp.component);
                        self.Traveller = self.pluckcom('Traveller', mainComp.component);
                        self.Agree_WhatsApp = self.pluckcom('Agree_WhatsApp', mainComp.component);
                        self.TripSummary_Detail = self.pluckcom('TripSummary_Detail', mainComp.component);
                        self.Tickets = self.pluckcom('Tickets', mainComp.component);
                        self.Fare = self.pluckcom('Fare', mainComp.component);
                        self.Taxes_Fees = self.pluckcom('Taxes_Fees', mainComp.component);
                        self.Service_Fee = self.pluckcom('Service_Fee', mainComp.component);
                        self.Total = self.pluckcom('Total', mainComp.component);
                        self.Taxesfees_included = self.pluckcom('Taxesfees_included', mainComp.component);
                        self.Travellers = self.pluckcom('Travellers', mainComp.component);
                        self.FareChange_Alert = self.pluckcom('FareChange_Alert', mainComp.component);
                        self.Updated_Fare = self.pluckcom('Updated_Fare', mainComp.component);
                        self.Original_Fare = self.pluckcom('Original_Fare', mainComp.component);
                        self.Cancel = self.pluckcom('Cancel', mainComp.component);
                        self.Continue = self.pluckcom('Continue', mainComp.component);
                        self.Reviewyour_flight = self.pluckcom('Reviewyour_flight', mainComp.component);
                        self.Continue_ToPayment = self.pluckcom('Continue_ToPayment', mainComp.component);
                        self.close = self.pluckcom('close', mainComp.component);
                        self.Alert = self.pluckcom('Alert', mainComp.component);
                        self.Confirm = self.pluckcom('Confirm', mainComp.component);
                        self.Message_UnexpectSupplierIssue = self.pluckcom('Message_UnexpectSupplierIssue', mainComp.component);
                        self.Provide_Email = self.pluckcom('Provide_Email', mainComp.component);
                        self.Provide_PhoneNumber = self.pluckcom('Provide_PhoneNumber', mainComp.component);
                        self.Thank_Registerwithus = self.pluckcom('Thank_Registerwithus', mainComp.component);
                        self.Email_AlreadyRegistered = self.pluckcom('Email_AlreadyRegistered', mainComp.component);
                        self.Fields_Mandatory = self.pluckcom('Fields_Mandatory', mainComp.component);
                        self.Booking_failed = self.pluckcom('Booking_failed', mainComp.component);
                        self.Max_TwentyChar = self.pluckcom('Max_TwentyChar', mainComp.component);
                        self.Username_required = self.pluckcom('Username_required', mainComp.component);
                        self.Password_required = self.pluckcom('Password_required', mainComp.component);
                        self.Login_failed = self.pluckcom('Login_failed', mainComp.component);
                        self.Password = self.pluckcom('Password', mainComp.component);
                        self.FrequentNumber_Required = self.pluckcom('FrequentNumber_Required', mainComp.component);
                        self.Welcome = self.pluckcom('Welcome', mainComp.component);
                        self.Ok = self.pluckcom('Ok', mainComp.component);
                        self.GoBack = self.pluckcom('GoBack', mainComp.component);
                        self.AddFrequent = self.pluckcom('AddFrequent', mainComp.component);
                        self.welcomeUser = self.Welcome + '! ' + currentUser.firstName + ' ' + currentUser.lastName + ' <span>(' + currentUser.emailId + ')</span>';
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        getmoreinfo(url) {
            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url = "/Nirvana/book-now.html?page=" + url;
            } else {
                url = "#";
            }
            return url;
        }
    }
});

var Vue_FareUpdate = new Vue({
    el: "#fareChangeModal",
    i18n,
    data: {
        fareChangedText: '',
        fareTitleText: '',
        currency: '',
        farecss: '',
        updatedFare: '',
        originalFare: '',
        difference: '',
        fareFooterText: '',
        selectedCurrency: null,
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        Review_Flight: '',
        Traveller_Details: '',
        Flight_Booking: '',
        Payment: '',
        Confirmation: '',
        Log_out: '',
        Signin_ProcessLabel: '',
        Sign_In: '',
        Email_RequiredLabel: '',
        Password_RequiredLabel: '',
        Account_NotAvailable: '',
        Account_ProcessLabel: '',
        to: '',
        stop: '',
        Fare_Rules: '',
        Baggage: '',
        Refundable: '',
        Non_Refundable: '',
        Refundable_withPenalty: '',
        No_cancellation: '',
        Fare_Rule: '',
        Before_Departure: '',
        After_Departure: '',
        Allowed_for: '',
        Flight_from: '',
        Checked_Baggage: '',
        Free: '',
        Cabin_Baggage: '',
        No_baggage: '',
        Provide_TravellerDetails: '',
        NationalIdPassport_Warning: '',
        Required_field: '',
        Title_Label: '',
        FirstName_Label: '',
        LastName_Label: '',
        DoB_Label: '',
        Nationality_Label: '',
        PassportNumber_Label: '',
        ExpireDate_Label: '',
        IssueDate_Label: '',
        IssueCountry_Label: '',
        Frequentflyer_program: '',
        Frequentflyer_number: '',
        Email: '',
        FlightTicket_RegisteredEmail: '',
        PhoneNumber_Label: '',
        Traveller: '',
        Agree_WhatsApp: '',
        TripSummary_Detail: '',
        Tickets: '',
        Fare: '',
        Taxes_Fees: '',
        Service_Fee: '',
        Total: '',
        Taxesfees_included: '',
        Travellers: '',
        FareChange_Alert: '',
        Updated_Fare: '',
        Original_Fare: '',
        Cancel: '',
        Continue: '',
        Reviewyour_flight: '',
        Continue_ToPayment: '',
        close: '',
        Alert: '',
        Confirm: '',
        Message_UnexpectSupplierIssue: '',
        Provide_Email: '',
        Provide_PhoneNumber: '',
        Thank_Registerwithus: '',
        Email_AlreadyRegistered: '',
        Fields_Mandatory: '',
        Booking_failed: '',
        Max_TwentyChar: '',
        Username_required: '',
        Password_required: '',
        Login_failed: '',
        Password: '',
        FrequentNumber_Required: '',
        Welcome: '',
        Ok: '',
        Fare_Slashed: '',
        Fare_Hiked: '',
        FareSlashed_Message: '',
        FareHiked_Message: '',
        FareChanged_Message: '',
        You_saved: '',
        Extra_amount: '',
        GoBack: '',
        AddFrequent: ''
    },
    mounted() {
        var userNode = JSON.parse(localStorage.User);
        if (userNode) {
            this.selectedCurrency = (localStorage.selectedCurrency) ? localStorage.selectedCurrency : userNode.loginNode.currency;
        } else {
            this.selectedCurrency = (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD';
        }
        this.getPagecontent();
    },
    methods: {
        resetValues: function () {
            this.fareChangedText = '';
            this.fareTitleText = '';
            this.currency = '';
            this.farecss = '';
            this.updatedFare = 0;
            this.originalFare = 0;
            this.difference = 0;
            this.fareFooterText = '';
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function (callBackFunction) {
            var self = this;

            getAgencycode(function (response) {
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + commonAgencyCode + '/Template/Booking Details/Booking Details/Booking Details.ftl';
                axios.get(aboutUsPage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (response.data.area_List.length > 0) {
                        var mainComp = response.data.area_List[0].main;
                        self.Review_Flight = self.pluckcom('Review_Flight', mainComp.component);
                        self.Traveller_Details = self.pluckcom('Traveller_Details', mainComp.component);
                        self.Flight_Booking = self.pluckcom('Flight_Booking', mainComp.component);
                        self.Payment = self.pluckcom('Payment', mainComp.component);
                        self.Confirmation = self.pluckcom('Confirmation', mainComp.component);
                        self.Log_out = self.pluckcom('Log_out', mainComp.component);
                        self.Signin_ProcessLabel = self.pluckcom('Signin_ProcessLabel', mainComp.component);
                        self.Sign_In = self.pluckcom('Sign_In', mainComp.component);
                        self.Email_RequiredLabel = self.pluckcom('Email_RequiredLabel', mainComp.component);
                        self.Password_RequiredLabel = self.pluckcom('Password_RequiredLabel', mainComp.component);
                        self.Account_NotAvailable = self.pluckcom('Account_NotAvailable', mainComp.component);
                        self.Account_ProcessLabel = self.pluckcom('Account_ProcessLabel', mainComp.component);
                        self.to = self.pluckcom('to', mainComp.component);
                        self.stop = self.pluckcom('stop', mainComp.component);
                        self.Fare_Rules = self.pluckcom('Fare_Rules', mainComp.component);
                        self.Baggage = self.pluckcom('Baggage', mainComp.component);
                        self.Refundable = self.pluckcom('Refundable', mainComp.component);
                        self.Non_Refundable = self.pluckcom('Non_Refundable', mainComp.component);
                        self.Refundable_withPenalty = self.pluckcom('Refundable_withPenalty', mainComp.component);
                        self.No_cancellation = self.pluckcom('No_cancellation', mainComp.component);
                        self.Fare_Rule = self.pluckcom('Fare_Rule', mainComp.component);
                        self.Before_Departure = self.pluckcom('Before_Departure', mainComp.component);
                        self.After_Departure = self.pluckcom('After_Departure', mainComp.component);
                        self.Allowed_for = self.pluckcom('Allowed_for', mainComp.component);
                        self.Flight_from = self.pluckcom('Flight_from', mainComp.component);
                        self.Checked_Baggage = self.pluckcom('Checked_Baggage', mainComp.component);
                        self.Free = self.pluckcom('Free', mainComp.component);
                        self.Cabin_Baggage = self.pluckcom('Cabin_Baggage', mainComp.component);
                        self.No_baggage = self.pluckcom('No_baggage', mainComp.component);
                        self.Provide_TravellerDetails = self.pluckcom('Provide_TravellerDetails', mainComp.component);
                        self.NationalIdPassport_Warning = self.pluckcom('NationalIdPassport_Warning', mainComp.component);
                        self.Required_field = self.pluckcom('Required_field', mainComp.component);
                        self.Title_Label = self.pluckcom('Title_Label', mainComp.component);
                        self.FirstName_Label = self.pluckcom('FirstName_Label', mainComp.component);
                        self.LastName_Label = self.pluckcom('LastName_Label', mainComp.component);
                        self.DoB_Label = self.pluckcom('DoB_Label', mainComp.component);
                        self.Nationality_Label = self.pluckcom('Nationality_Label', mainComp.component);
                        self.PassportNumber_Label = self.pluckcom('PassportNumber_Label', mainComp.component);
                        self.ExpireDate_Label = self.pluckcom('ExpireDate_Label', mainComp.component);
                        self.IssueDate_Label = self.pluckcom('IssueDate_Label', mainComp.component);
                        self.IssueCountry_Label = self.pluckcom('IssueCountry_Label', mainComp.component);
                        self.Frequentflyer_program = self.pluckcom('Frequentflyer_program', mainComp.component);
                        self.Frequentflyer_number = self.pluckcom('Frequentflyer_number', mainComp.component);
                        self.Email = self.pluckcom('Email', mainComp.component);
                        self.FlightTicket_RegisteredEmail = self.pluckcom('FlightTicket_RegisteredEmail', mainComp.component);
                        self.PhoneNumber_Label = self.pluckcom('PhoneNumber_Label', mainComp.component);
                        self.Traveller = self.pluckcom('Traveller', mainComp.component);
                        self.Agree_WhatsApp = self.pluckcom('Agree_WhatsApp', mainComp.component);
                        self.TripSummary_Detail = self.pluckcom('TripSummary_Detail', mainComp.component);
                        self.Tickets = self.pluckcom('Tickets', mainComp.component);
                        self.Fare = self.pluckcom('Fare', mainComp.component);
                        self.Taxes_Fees = self.pluckcom('Taxes_Fees', mainComp.component);
                        self.Service_Fee = self.pluckcom('Service_Fee', mainComp.component);
                        self.Total = self.pluckcom('Total', mainComp.component);
                        self.Taxesfees_included = self.pluckcom('Taxesfees_included', mainComp.component);
                        self.Travellers = self.pluckcom('Travellers', mainComp.component);
                        self.FareChange_Alert = self.pluckcom('FareChange_Alert', mainComp.component);
                        self.Updated_Fare = self.pluckcom('Updated_Fare', mainComp.component);
                        self.Original_Fare = self.pluckcom('Original_Fare', mainComp.component);
                        self.Cancel = self.pluckcom('Cancel', mainComp.component);
                        self.Continue = self.pluckcom('Continue', mainComp.component);
                        self.Reviewyour_flight = self.pluckcom('Reviewyour_flight', mainComp.component);
                        self.Continue_ToPayment = self.pluckcom('Continue_ToPayment', mainComp.component);
                        self.close = self.pluckcom('close', mainComp.component);
                        self.Alert = self.pluckcom('Alert', mainComp.component);
                        self.Confirm = self.pluckcom('Confirm', mainComp.component);
                        self.Message_UnexpectSupplierIssue = self.pluckcom('Message_UnexpectSupplierIssue', mainComp.component);
                        self.Provide_Email = self.pluckcom('Provide_Email', mainComp.component);
                        self.Provide_PhoneNumber = self.pluckcom('Provide_PhoneNumber', mainComp.component);
                        self.Thank_Registerwithus = self.pluckcom('Thank_Registerwithus', mainComp.component);
                        self.Email_AlreadyRegistered = self.pluckcom('Email_AlreadyRegistered', mainComp.component);
                        self.Fields_Mandatory = self.pluckcom('Fields_Mandatory', mainComp.component);
                        self.Booking_failed = self.pluckcom('Booking_failed', mainComp.component);
                        self.Max_TwentyChar = self.pluckcom('Max_TwentyChar', mainComp.component);
                        self.Username_required = self.pluckcom('Username_required', mainComp.component);
                        self.Password_required = self.pluckcom('Password_required', mainComp.component);
                        self.Login_failed = self.pluckcom('Login_failed', mainComp.component);
                        self.Password = self.pluckcom('Password', mainComp.component);
                        self.FrequentNumber_Required = self.pluckcom('FrequentNumber_Required', mainComp.component);
                        self.Welcome = self.pluckcom('Welcome', mainComp.component);
                        self.Ok = self.pluckcom('Ok', mainComp.component);
                        self.Fare_Slashed = self.pluckcom('Fare_Slashed', mainComp.component);
                        self.Fare_Hiked = self.pluckcom('Fare_Hiked', mainComp.component);
                        self.FareChanged_Message = self.pluckcom('FareChanged_Message', mainComp.component);
                        self.You_saved = self.pluckcom('You_saved', mainComp.component);
                        self.Extra_amount = self.pluckcom('Extra_amount', mainComp.component);
                        self.FareHiked_Message = self.pluckcom('FareHiked_Message', mainComp.component);
                        self.FareSlashed_Message = self.pluckcom('FareSlashed_Message', mainComp.component);
                        self.GoBack = self.pluckcom('GoBack', mainComp.component);
                        self.AddFrequent = self.pluckcom('AddFrequent', mainComp.component);
                        try { callBackFunction(); } catch (err) { }
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });
        },
        getmoreinfo(url) {
            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url = "/Nirvana/book-now.html?page=" + url;
            } else {
                url = "#";
            }
            return url;
        }
    }
});

var Vue_CoTraveller = new Vue({
    el: "#divCoTraveller",
    data: {
        coTravellers: [],
        coTravellerByPaxType: [],
        currentPaxType: '',
        Review_Flight: '',
        Traveller_Details: '',
        Flight_Booking: '',
        Payment: '',
        Confirmation: '',
        Log_out: '',
        Signin_ProcessLabel: '',
        Sign_In: '',
        Email_RequiredLabel: '',
        Password_RequiredLabel: '',
        Account_NotAvailable: '',
        Account_ProcessLabel: '',
        to: '',
        stop: '',
        Fare_Rules: '',
        Baggage: '',
        Refundable: '',
        Non_Refundable: '',
        Refundable_withPenalty: '',
        No_cancellation: '',
        Fare_Rule: '',
        Before_Departure: '',
        After_Departure: '',
        Allowed_for: '',
        Flight_from: '',
        Checked_Baggage: '',
        Free: '',
        Cabin_Baggage: '',
        No_baggage: '',
        Provide_TravellerDetails: '',
        NationalIdPassport_Warning: '',
        Required_field: '',
        Title_Label: '',
        FirstName_Label: '',
        LastName_Label: '',
        DoB_Label: '',
        Nationality_Label: '',
        PassportNumber_Label: '',
        ExpireDate_Label: '',
        IssueDate_Label: '',
        IssueCountry_Label: '',
        Frequentflyer_program: '',
        Frequentflyer_number: '',
        Email: '',
        FlightTicket_RegisteredEmail: '',
        PhoneNumber_Label: '',
        Traveller: '',
        Agree_WhatsApp: '',
        TripSummary_Detail: '',
        Tickets: '',
        Fare: '',
        Taxes_Fees: '',
        Service_Fee: '',
        Total: '',
        Taxesfees_included: '',
        Travellers: '',
        FareChange_Alert: '',
        Updated_Fare: '',
        Original_Fare: '',
        Cancel: '',
        Continue: '',
        Reviewyour_flight: '',
        Continue_ToPayment: '',
        close: '',
        Alert: '',
        Confirm: '',
        Message_UnexpectSupplierIssue: '',
        Provide_Email: '',
        Provide_PhoneNumber: '',
        Thank_Registerwithus: '',
        Email_AlreadyRegistered: '',
        Fields_Mandatory: '',
        Booking_failed: '',
        Max_TwentyChar: '',
        Username_required: '',
        Password_required: '',
        Login_failed: '',
        Password: '',
        FrequentNumber_Required: '',
        Welcome: '',
        Ok: '',
        GoBack: '',
        AddFrequent: '',
        selectedCotraveler: []
    },
    mounted() {
        this.getPagecontent();
    },
    created: function () {
        this.setCoTravellers();
    },
    methods: {
        setCoTravellers: function (callback) {
            var self = this;
            try {
                if (localStorage.IsLogin == "true") {
                    var huburl = ServiceUrls.hubConnection.baseUrl;
                    var portno = ServiceUrls.hubConnection.ipAddress;
                    var requrl = ServiceUrls.hubConnection.hubServices.getCoTravellers;

                    axios.get(huburl + portno + requrl, axiosConfigHeader()).then((response) => {
                        localStorage.access_token = response.headers.access_token;
                        this.initData(response.data);
                        if (typeof callback =="function") {
                            callback();
                        }
                    }).catch((err) => {
                        console.log(err);
                        $('#divCoTraveller').hide();
                        // alertify.alert(self.Alert, self.Message_UnexpectSupplierIssue).setting({ 'closable': false, onclose: function () { window.location = '/'; } }).set('label', self.Ok);
                    });
                }
            } catch (err) {
                console.log(err);
                $('#divCoTraveller').hide();
                // alertify.alert(self.Alert, self.Message_UnexpectSupplierIssue).setting({ 'closable': false, onclose: function () { window.location = '/'; } }).set('label', self.Ok);
            }
        },
        initData: function (response) {
            this.coTravellers = response;
        },
        resetValues: function () {
            this.coTravellers = [];
            this.coTravellerByPaxType = [];
            this.currentPaxType = '';
        },
        setPaxTypeList: function (paxType, paxIndex) {
            $('#selTrvlrId').data('type', paxType);
            $('#selTrvlrId').val(paxIndex);
            this.coTravellerByPaxType = [];
            this.coTravellerByPaxType = this.coTravellers.filter(function (coTraveller) { return coTraveller.paxType.toLowerCase() == paxType.toLowerCase() });
            this.currentPaxType = getPaxType(paxType, 1);
            return this.coTravellerByPaxType;
        },
        selectTraveller: function (id) {
            var vm = this;
            try {
                this.setCoTravellers(function(){
                    var coTraveller = Vue_CoTraveller.coTravellers.filter(function (coTraveller) { return coTraveller.id == id });
                    if (coTraveller.length==0) {
                        alertify.alert(self.Alert, "Selected companion is not existing. It might have been deleted in My Companion page. Please verify.").setting({ 'closable': false, onclose: function () { } }).set('label', self.Ok);
                    }else {
                        coTraveller = coTraveller[0];
                        vm.selectedCotraveler.push(JSON.parse(JSON.stringify(coTraveller)));
                        var paxType = $('#selTrvlrId').data('type');
                        var paxIndex = $('#selTrvlrId').val();
                        var paxTypeIndex = paxType + paxIndex;
                        var paxtitle = '';
                        if ((coTraveller.title == '1' || coTraveller.title == '5') && (paxType == 'CHD' || paxType == 'INF')) {
                            paxtitle = 'Mstr';
                        } else if (coTraveller.title == '4' && (paxType == 'CHD' || paxType == 'INF')) {
                            paxtitle = 'Miss';
                        } else if (coTraveller.title == '1' && paxType == 'ADT') {
                            paxtitle = 'Mr';
                        } else if (coTraveller.title == '2' && paxType == 'ADT') {
                            paxtitle = 'Mrs';
                        } else if (coTraveller.title == '3' && paxType == 'ADT') {
                            paxtitle = 'Ms';
                        }else {
                            paxtitle = coTraveller.title;
                        }
                        
                        $('#ddltitle' + paxTypeIndex).val(paxtitle);
                        $('#txtFname' + paxTypeIndex).val(coTraveller.firstName);
                        $('#txtSname' + paxTypeIndex).val(coTraveller.lastName);
                        $('#txtDob' + paxTypeIndex).val(moment(coTraveller.dob, 'YYYY-MM-DD').format('DD/MM/YYYY'));
                        $('#txtnationality' + paxTypeIndex).val(coTraveller.nationality);
                        $('#txtPassport' + paxTypeIndex).val(coTraveller.passportNumber);
                        $('#txtExpireDate' + paxTypeIndex).val(moment(coTraveller.passportExpDate, 'YYYY-MM-DD').format('DD/MM/YYYY'));
                        $('#txtPassportIssue' + paxTypeIndex).val(moment(coTraveller.passportIssueDate, 'YYYY-MM-DD').format('DD/MM/YYYY'));
                        $('#ddlPassportIssue' + paxTypeIndex).val(coTraveller.issuingCountry);
                    }
                    $('#divCoTraveller').hide();
                });

            } catch (err) {
                console.log(err);
            }
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + commonAgencyCode + '/Template/Booking Details/Booking Details/Booking Details.ftl';
                axios.get(aboutUsPage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (response.data.area_List.length > 0) {
                        var mainComp = response.data.area_List[0].main;
                        self.Review_Flight = self.pluckcom('Review_Flight', mainComp.component);
                        self.Traveller_Details = self.pluckcom('Traveller_Details', mainComp.component);
                        self.Flight_Booking = self.pluckcom('Flight_Booking', mainComp.component);
                        self.Payment = self.pluckcom('Payment', mainComp.component);
                        self.Confirmation = self.pluckcom('Confirmation', mainComp.component);
                        self.Log_out = self.pluckcom('Log_out', mainComp.component);
                        self.Signin_ProcessLabel = self.pluckcom('Signin_ProcessLabel', mainComp.component);
                        self.Sign_In = self.pluckcom('Sign_In', mainComp.component);
                        self.Email_RequiredLabel = self.pluckcom('Email_RequiredLabel', mainComp.component);
                        self.Password_RequiredLabel = self.pluckcom('Password_RequiredLabel', mainComp.component);
                        self.Account_NotAvailable = self.pluckcom('Account_NotAvailable', mainComp.component);
                        self.Account_ProcessLabel = self.pluckcom('Account_ProcessLabel', mainComp.component);
                        self.to = self.pluckcom('to', mainComp.component);
                        self.stop = self.pluckcom('stop', mainComp.component);
                        self.Fare_Rules = self.pluckcom('Fare_Rules', mainComp.component);
                        self.Baggage = self.pluckcom('Baggage', mainComp.component);
                        self.Refundable = self.pluckcom('Refundable', mainComp.component);
                        self.Non_Refundable = self.pluckcom('Non_Refundable', mainComp.component);
                        self.Refundable_withPenalty = self.pluckcom('Refundable_withPenalty', mainComp.component);
                        self.No_cancellation = self.pluckcom('No_cancellation', mainComp.component);
                        self.Fare_Rule = self.pluckcom('Fare_Rule', mainComp.component);
                        self.Before_Departure = self.pluckcom('Before_Departure', mainComp.component);
                        self.After_Departure = self.pluckcom('After_Departure', mainComp.component);
                        self.Allowed_for = self.pluckcom('Allowed_for', mainComp.component);
                        self.Flight_from = self.pluckcom('Flight_from', mainComp.component);
                        self.Checked_Baggage = self.pluckcom('Checked_Baggage', mainComp.component);
                        self.Free = self.pluckcom('Free', mainComp.component);
                        self.Cabin_Baggage = self.pluckcom('Cabin_Baggage', mainComp.component);
                        self.No_baggage = self.pluckcom('No_baggage', mainComp.component);
                        self.Provide_TravellerDetails = self.pluckcom('Provide_TravellerDetails', mainComp.component);
                        self.NationalIdPassport_Warning = self.pluckcom('NationalIdPassport_Warning', mainComp.component);
                        self.Required_field = self.pluckcom('Required_field', mainComp.component);
                        self.Title_Label = self.pluckcom('Title_Label', mainComp.component);
                        self.FirstName_Label = self.pluckcom('FirstName_Label', mainComp.component);
                        self.LastName_Label = self.pluckcom('LastName_Label', mainComp.component);
                        self.DoB_Label = self.pluckcom('DoB_Label', mainComp.component);
                        self.Nationality_Label = self.pluckcom('Nationality_Label', mainComp.component);
                        self.PassportNumber_Label = self.pluckcom('PassportNumber_Label', mainComp.component);
                        self.ExpireDate_Label = self.pluckcom('ExpireDate_Label', mainComp.component);
                        self.IssueDate_Label = self.pluckcom('IssueDate_Label', mainComp.component);
                        self.IssueCountry_Label = self.pluckcom('IssueCountry_Label', mainComp.component);
                        self.Frequentflyer_program = self.pluckcom('Frequentflyer_program', mainComp.component);
                        self.Frequentflyer_number = self.pluckcom('Frequentflyer_number', mainComp.component);
                        self.Email = self.pluckcom('Email', mainComp.component);
                        self.FlightTicket_RegisteredEmail = self.pluckcom('FlightTicket_RegisteredEmail', mainComp.component);
                        self.PhoneNumber_Label = self.pluckcom('PhoneNumber_Label', mainComp.component);
                        self.Traveller = self.pluckcom('Traveller', mainComp.component);
                        self.Agree_WhatsApp = self.pluckcom('Agree_WhatsApp', mainComp.component);
                        self.TripSummary_Detail = self.pluckcom('TripSummary_Detail', mainComp.component);
                        self.Tickets = self.pluckcom('Tickets', mainComp.component);
                        self.Fare = self.pluckcom('Fare', mainComp.component);
                        self.Taxes_Fees = self.pluckcom('Taxes_Fees', mainComp.component);
                        self.Service_Fee = self.pluckcom('Service_Fee', mainComp.component);
                        self.Total = self.pluckcom('Total', mainComp.component);
                        self.Taxesfees_included = self.pluckcom('Taxesfees_included', mainComp.component);
                        self.Travellers = self.pluckcom('Travellers', mainComp.component);
                        self.FareChange_Alert = self.pluckcom('FareChange_Alert', mainComp.component);
                        self.Updated_Fare = self.pluckcom('Updated_Fare', mainComp.component);
                        self.Original_Fare = self.pluckcom('Original_Fare', mainComp.component);
                        self.Cancel = self.pluckcom('Cancel', mainComp.component);
                        self.Continue = self.pluckcom('Continue', mainComp.component);
                        self.Reviewyour_flight = self.pluckcom('Reviewyour_flight', mainComp.component);
                        self.Continue_ToPayment = self.pluckcom('Continue_ToPayment', mainComp.component);
                        self.close = self.pluckcom('close', mainComp.component);
                        self.Alert = self.pluckcom('Alert', mainComp.component);
                        self.Confirm = self.pluckcom('Confirm', mainComp.component);
                        self.Message_UnexpectSupplierIssue = self.pluckcom('Message_UnexpectSupplierIssue', mainComp.component);
                        self.Provide_Email = self.pluckcom('Provide_Email', mainComp.component);
                        self.Provide_PhoneNumber = self.pluckcom('Provide_PhoneNumber', mainComp.component);
                        self.Thank_Registerwithus = self.pluckcom('Thank_Registerwithus', mainComp.component);
                        self.Email_AlreadyRegistered = self.pluckcom('Email_AlreadyRegistered', mainComp.component);
                        self.Fields_Mandatory = self.pluckcom('Fields_Mandatory', mainComp.component);
                        self.Booking_failed = self.pluckcom('Booking_failed', mainComp.component);
                        self.Max_TwentyChar = self.pluckcom('Max_TwentyChar', mainComp.component);
                        self.Username_required = self.pluckcom('Username_required', mainComp.component);
                        self.Password_required = self.pluckcom('Password_required', mainComp.component);
                        self.Login_failed = self.pluckcom('Login_failed', mainComp.component);
                        self.Password = self.pluckcom('Password', mainComp.component);
                        self.FrequentNumber_Required = self.pluckcom('FrequentNumber_Required', mainComp.component);
                        self.Welcome = self.pluckcom('Welcome', mainComp.component);
                        self.Ok = self.pluckcom('Ok', mainComp.component);
                        self.GoBack = self.pluckcom('GoBack', mainComp.component);
                        self.AddFrequent = self.pluckcom('AddFrequent', mainComp.component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        getmoreinfo(url) {
            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url = "/Nirvana/book-now.html?page=" + url;
            } else {
                url = "#";
            }
            return url;
        }
    }
});

var Vue_FareHead = new Vue({
    el: "#fareHead",
    i18n,
    data: {
        Review_Flight: '',
        Traveller_Details: '',
        Flight_Booking: '',
        Payment: '',
        Confirmation: '',
        Log_out: '',
        Signin_ProcessLabel: '',
        Sign_In: '',
        Email_RequiredLabel: '',
        Password_RequiredLabel: '',
        Account_NotAvailable: '',
        Account_ProcessLabel: '',
        to: '',
        stop: '',
        Fare_Rules: '',
        Baggage: '',
        Refundable: '',
        Non_Refundable: '',
        Refundable_withPenalty: '',
        No_cancellation: '',
        Fare_Rule: '',
        Before_Departure: '',
        After_Departure: '',
        Allowed_for: '',
        Flight_from: '',
        Checked_Baggage: '',
        Free: '',
        Cabin_Baggage: '',
        No_baggage: '',
        Provide_TravellerDetails: '',
        NationalIdPassport_Warning: '',
        Required_field: '',
        Title_Label: '',
        FirstName_Label: '',
        LastName_Label: '',
        DoB_Label: '',
        Nationality_Label: '',
        PassportNumber_Label: '',
        ExpireDate_Label: '',
        IssueDate_Label: '',
        IssueCountry_Label: '',
        Frequentflyer_program: '',
        Frequentflyer_number: '',
        Email: '',
        FlightTicket_RegisteredEmail: '',
        PhoneNumber_Label: '',
        Traveller: '',
        Agree_WhatsApp: '',
        TripSummary_Detail: '',
        Tickets: '',
        Fare: '',
        Taxes_Fees: '',
        Service_Fee: '',
        Total: '',
        Taxesfees_included: '',
        Travellers: '',
        FareChange_Alert: '',
        Updated_Fare: '',
        Original_Fare: '',
        Cancel: '',
        Continue: '',
        Reviewyour_flight: '',
        Continue_ToPayment: '',
        close: '',
        Alert: '',
        Confirm: '',
        Message_UnexpectSupplierIssue: '',
        Provide_Email: '',
        Provide_PhoneNumber: '',
        Thank_Registerwithus: '',
        Email_AlreadyRegistered: '',
        Fields_Mandatory: '',
        Booking_failed: '',
        Max_TwentyChar: '',
        Username_required: '',
        Password_required: '',
        Login_failed: '',
        Password: '',
        FrequentNumber_Required: '',
        Welcome: '',
        Ok: '',
        GoBack: '',
        AddFrequent: ''
    },
    mounted() {
        this.getPagecontent();
    },
    methods: {
        resetValues: function () {
            this.fareChangedText = '';
            this.fareTitleText = '';
            this.currency = '';
            this.farecss = '';
            this.updatedFare = 0;
            this.originalFare = 0;
            this.difference = 0;
            this.fareFooterText = '';
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + commonAgencyCode + '/Template/Booking Details/Booking Details/Booking Details.ftl';
                axios.get(aboutUsPage, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    if (response.data.area_List.length > 0) {
                        var mainComp = response.data.area_List[0].main;
                        self.Review_Flight = self.pluckcom('Review_Flight', mainComp.component);
                        self.Traveller_Details = self.pluckcom('Traveller_Details', mainComp.component);
                        self.Flight_Booking = self.pluckcom('Flight_Booking', mainComp.component);
                        self.Payment = self.pluckcom('Payment', mainComp.component);
                        self.Confirmation = self.pluckcom('Confirmation', mainComp.component);
                        self.Log_out = self.pluckcom('Log_out', mainComp.component);
                        self.Signin_ProcessLabel = self.pluckcom('Signin_ProcessLabel', mainComp.component);
                        self.Sign_In = self.pluckcom('Sign_In', mainComp.component);
                        self.Email_RequiredLabel = self.pluckcom('Email_RequiredLabel', mainComp.component);
                        self.Password_RequiredLabel = self.pluckcom('Password_RequiredLabel', mainComp.component);
                        self.Account_NotAvailable = self.pluckcom('Account_NotAvailable', mainComp.component);
                        self.Account_ProcessLabel = self.pluckcom('Account_ProcessLabel', mainComp.component);
                        self.to = self.pluckcom('to', mainComp.component);
                        self.stop = self.pluckcom('stop', mainComp.component);
                        self.Fare_Rules = self.pluckcom('Fare_Rules', mainComp.component);
                        self.Baggage = self.pluckcom('Baggage', mainComp.component);
                        self.Refundable = self.pluckcom('Refundable', mainComp.component);
                        self.Non_Refundable = self.pluckcom('Non_Refundable', mainComp.component);
                        self.Refundable_withPenalty = self.pluckcom('Refundable_withPenalty', mainComp.component);
                        self.No_cancellation = self.pluckcom('No_cancellation', mainComp.component);
                        self.Fare_Rule = self.pluckcom('Fare_Rule', mainComp.component);
                        self.Before_Departure = self.pluckcom('Before_Departure', mainComp.component);
                        self.After_Departure = self.pluckcom('After_Departure', mainComp.component);
                        self.Allowed_for = self.pluckcom('Allowed_for', mainComp.component);
                        self.Flight_from = self.pluckcom('Flight_from', mainComp.component);
                        self.Checked_Baggage = self.pluckcom('Checked_Baggage', mainComp.component);
                        self.Free = self.pluckcom('Free', mainComp.component);
                        self.Cabin_Baggage = self.pluckcom('Cabin_Baggage', mainComp.component);
                        self.No_baggage = self.pluckcom('No_baggage', mainComp.component);
                        self.Provide_TravellerDetails = self.pluckcom('Provide_TravellerDetails', mainComp.component);
                        self.NationalIdPassport_Warning = self.pluckcom('NationalIdPassport_Warning', mainComp.component);
                        self.Required_field = self.pluckcom('Required_field', mainComp.component);
                        self.Title_Label = self.pluckcom('Title_Label', mainComp.component);
                        self.FirstName_Label = self.pluckcom('FirstName_Label', mainComp.component);
                        self.LastName_Label = self.pluckcom('LastName_Label', mainComp.component);
                        self.DoB_Label = self.pluckcom('DoB_Label', mainComp.component);
                        self.Nationality_Label = self.pluckcom('Nationality_Label', mainComp.component);
                        self.PassportNumber_Label = self.pluckcom('PassportNumber_Label', mainComp.component);
                        self.ExpireDate_Label = self.pluckcom('ExpireDate_Label', mainComp.component);
                        self.IssueDate_Label = self.pluckcom('IssueDate_Label', mainComp.component);
                        self.IssueCountry_Label = self.pluckcom('IssueCountry_Label', mainComp.component);
                        self.Frequentflyer_program = self.pluckcom('Frequentflyer_program', mainComp.component);
                        self.Frequentflyer_number = self.pluckcom('Frequentflyer_number', mainComp.component);
                        self.Email = self.pluckcom('Email', mainComp.component);
                        self.FlightTicket_RegisteredEmail = self.pluckcom('FlightTicket_RegisteredEmail', mainComp.component);
                        self.PhoneNumber_Label = self.pluckcom('PhoneNumber_Label', mainComp.component);
                        self.Traveller = self.pluckcom('Traveller', mainComp.component);
                        self.Agree_WhatsApp = self.pluckcom('Agree_WhatsApp', mainComp.component);
                        self.TripSummary_Detail = self.pluckcom('TripSummary_Detail', mainComp.component);
                        self.Tickets = self.pluckcom('Tickets', mainComp.component);
                        self.Fare = self.pluckcom('Fare', mainComp.component);
                        self.Taxes_Fees = self.pluckcom('Taxes_Fees', mainComp.component);
                        self.Service_Fee = self.pluckcom('Service_Fee', mainComp.component);
                        self.Total = self.pluckcom('Total', mainComp.component);
                        self.Taxesfees_included = self.pluckcom('Taxesfees_included', mainComp.component);
                        self.Travellers = self.pluckcom('Travellers', mainComp.component);
                        self.FareChange_Alert = self.pluckcom('FareChange_Alert', mainComp.component);
                        self.Updated_Fare = self.pluckcom('Updated_Fare', mainComp.component);
                        self.Original_Fare = self.pluckcom('Original_Fare', mainComp.component);
                        self.Cancel = self.pluckcom('Cancel', mainComp.component);
                        self.Continue = self.pluckcom('Continue', mainComp.component);
                        self.Reviewyour_flight = self.pluckcom('Reviewyour_flight', mainComp.component);
                        self.Continue_ToPayment = self.pluckcom('Continue_ToPayment', mainComp.component);
                        self.close = self.pluckcom('close', mainComp.component);
                        self.Alert = self.pluckcom('Alert', mainComp.component);
                        self.Confirm = self.pluckcom('Confirm', mainComp.component);
                        self.Message_UnexpectSupplierIssue = self.pluckcom('Message_UnexpectSupplierIssue', mainComp.component);
                        self.Provide_Email = self.pluckcom('Provide_Email', mainComp.component);
                        self.Provide_PhoneNumber = self.pluckcom('Provide_PhoneNumber', mainComp.component);
                        self.Thank_Registerwithus = self.pluckcom('Thank_Registerwithus', mainComp.component);
                        self.Email_AlreadyRegistered = self.pluckcom('Email_AlreadyRegistered', mainComp.component);
                        self.Fields_Mandatory = self.pluckcom('Fields_Mandatory', mainComp.component);
                        self.Booking_failed = self.pluckcom('Booking_failed', mainComp.component);
                        self.Max_TwentyChar = self.pluckcom('Max_TwentyChar', mainComp.component);
                        self.Username_required = self.pluckcom('Username_required', mainComp.component);
                        self.Password_required = self.pluckcom('Password_required', mainComp.component);
                        self.Login_failed = self.pluckcom('Login_failed', mainComp.component);
                        self.Password = self.pluckcom('Password', mainComp.component);
                        self.FrequentNumber_Required = self.pluckcom('FrequentNumber_Required', mainComp.component);
                        self.Welcome = self.pluckcom('Welcome', mainComp.component);
                        self.Ok = self.pluckcom('Ok', mainComp.component);
                        self.GoBack = self.pluckcom('GoBack', mainComp.component);
                        self.AddFrequent = self.pluckcom('AddFrequent', mainComp.component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        getmoreinfo(url) {
            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url = "/Nirvana/book-now.html?page=" + url;
            } else {
                url = "#";
            }
            return url;
        }
    }
});

$('body').on('focus', '.sel_co_trvlr_new', function () {
    if (!isNullorUndefined(Vue_CoTraveller.coTravellers) && Vue_CoTraveller.coTravellers.length > 0) {
        Vue_CoTraveller.setPaxTypeList($(this).data('paxtype'), $(this).data('paxindex'));
        if (Vue_CoTraveller.coTravellerByPaxType.length > 0) {
            $('#divCoTraveller').show();
            //$("#divCoTraveller").css('top', ($(this).parent().parent().position().top) + 'px');
            $("#divCoTraveller").css('top', ($(this).parent().position().top + 90) + 'px');
            $("#divCoTraveller").css('left', ($(this).parent().position().left + 15) + 'px');
            $("#divCoTraveller").css('width', ($(this).width() + 21) + 'px');
        }
    }
});

$(document).on('click', function (e) {
    if ($(e.target).closest("#divCoTraveller").length === 0 && $(e.target).closest(".sel_co_trvlr_new").length === 0) {
        $("#divCoTraveller").hide();
    }
});

$('body').on('keypress', '.only_alpha', function (e) {
    if (e.ctrlKey || e.altKey) {
        e.preventDefault();
    } else {
        var key = e.keyCode;
        if (!((key == 32) || (key == 96) || (key >= 65 && key <= 90) || (key >= 97 && key <= 122))) {
            e.preventDefault();
        }
    }
});

$('body').on('keypress', '.only_number', function (e) {
    if (e.ctrlKey || e.altKey) {
        e.preventDefault();
    } else {
        var key = e.keyCode;
        if (!((key >= 48 && key <= 57))) {
            e.preventDefault();
        }
    }
});

$('.only_alpha, .only_number, .sel_co_trvlr, .sel_co_trvlr_new').bind('copy paste cut', function (e) {
    e.preventDefault(); //disable cut,copy,paste
});

$.extend({
    distinct: function (anArray) {
        var result = [];
        $.each(anArray, function (i, v) {
            if ($.inArray(v, result) == -1) result.push(v);
        });
        return result;
    }
});

function backPage() {
    window.history.back();
}
var vuePagecontent = new Vue({
    el: "#cmsdiv",
    name: "terms and privacy",
    data: {
        privacyHead: 'Privacy Policy',
        privacyContent: '',
        termsHead: 'Terms And Conditions',
        termsContent: '',
        getterms: false,
        getprivacy: false
    },
    mounted() {
        this.getPagecontent();
    },
    methods: {

        getPagecontent: function () {
            var self = this;
            var huburl = ServiceUrls.hubConnection.baseUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var contact = ServiceUrls.hubConnection.hubServices.contact;
            axios.get(huburl + portno + contact, {
                headers: {
                    Authorization: "Bearer " + localStorage.access_token
                }
            }).then(function (response) {

                try {
                    var Result = response.data.siteList[0].contentType;
                    var privacy = Result.filter(row => row.name === 'FlightBookingPrivacyPolicy');
                    if (privacy.length > 0) {
                        self.getprivacy = true;
                        self.privacyContent = privacy[0].value;
                    }
                    else {
                        privacy = Result.filter(row => row.name === 'CommonPrivacyPolicy');
                        if (privacy.length > 0) {
                            self.getprivacy = true;
                            self.privacyContent = privacy[0].value;
                        }
                    }
                    var terms = Result.filter(row => row.name === 'FlightBookingTermsNConditions');
                    if (terms.length > 0) {
                        self.getterms = true;
                        self.termsContent = terms[0].value;
                    }
                    else {
                        terms = Result.filter(row => row.name === 'CommonTermsNConditions');
                        if (terms.length > 0) {
                            self.getterms = true;
                            self.termsContent = terms[0].value;
                        }
                    }
                    console.log(Result);
                } catch (error) {

                }
            }).catch(function (error) {
                console.log(error);
            });

        },
    }
});

function hasFareRule(airFareRules) {
    var hasFareRule = false;
    if (airFareRules != undefined && airFareRules != null && airFareRules != "" && airFareRules != "NA" && airFareRules != "NIL") {
        if (airFareRules.departure != "" && airFareRules.departure != "NA" && airFareRules.departure != null && airFareRules.departure != undefined && airFareRules.departure != "NIL") {
            hasFareRule = true;
        }
    }
    return hasFareRule;
}