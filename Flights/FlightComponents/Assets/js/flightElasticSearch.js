async function getFlightResultUsingElasticSearch(keywordEntered) {
 var searchResult=[];
    var cityName = keywordEntered;

    var lowercaseLetter = cityName.toLowerCase();
    var query = {
      size: 25,
      query: {
        bool: {
          should: [
            {
              term: {
                iata_code: {
                  value: lowercaseLetter,
                  boost: 10,
                },
              },
            },
            {
              wildcard: {
                name: {
                  value: lowercaseLetter + "*",
                  boost: 5,
                },
              },
            },
            {
              wildcard: {
                name: {
                  value: "*" + lowercaseLetter + "*",
                  boost: 3,
                },
              },
            },
            {
              wildcard: {
                name_arabic: {
                  value: localStorage.Languagecode == "ar" ? "*" + lowercaseLetter : lowercaseLetter + "*",
                  boost: 5,
                },
              },
            },
            {
              wildcard: {
                name_arabic: {
                  value: "*" + lowercaseLetter + "*",
                  boost: 3,
                },
              },
            },
            {
              wildcard: {
                country_name: {
                  value: lowercaseLetter + "*",
                  boost: 1.5,
                },
              },
            },
            {
              wildcard: {
                country_name: {
                  value: "*" + lowercaseLetter + "*",
                  boost: 1.5,
                },
              },
            },
            {
              wildcard: {
                country_name_arabic: {
                  value: localStorage.Languagecode == "ar" ? "*" + lowercaseLetter : lowercaseLetter + "*",
                  boost: 1.5,
                },
              },
            },
            {
              wildcard: {
                country_name_arabic: {
                  value: "*" + lowercaseLetter + "*",
                  boost: 1.5,
                },
              },
            },
            {
              wildcard: {
                municipality: lowercaseLetter + "*",
              },
            },
            {
              wildcard: {
                municipality: "*" + lowercaseLetter + "*",
              },
            },
            {
              wildcard: {
                municipality_arabic: localStorage.Languagecode == "ar" ? "*" + lowercaseLetter : lowercaseLetter + "*",
              },
            },
            {
              wildcard: {
                municipality_arabic: "*" + lowercaseLetter + "*",
              },
            },
          ],
          must: [
            {
              bool: {
                should: [
                  {
                    term: {
                      type: {
                        value: "major_airport",
                        boost: 7,
                      },
                    },
                  },
                  {
                    term: {
                      type: {
                        value: "large_airport",
                        boost: 5,
                      },
                    },
                  },
                  {
                    term: {
                      type: {
                        value: "medium_airport",
                        boost: 2,
                      },
                    },
                  },
                  {
                    term: {
                      type: {
                        value: "small_airport",
                        boost: 1,
                      },
                    },
                  },
                ],
                minimum_should_match: 1,
              },
            },
          ],
          minimum_should_match: 1,
        },
      },
    };

    await axios.post(ServiceUrls.elasticSearch.url, query, {
      headers: {
        "Content-Type": "application/json"
      }
      }).then(async function (resp) {
        finalResult = [];
        var hits = resp.data.hits.hits;
        var Citymap = new Map();
        for (var i = 0; i < hits.length; i++) {
            Citymap.set(hits[i]._source.iata_code, hits[i]._source);
        }
        var get_values = Citymap.values();
        var Cityvalues = [];
        // for (var ele of get_values) {
        //   Cityvalues.push(ele);
        // }
        for (var ele of get_values) {
            Cityvalues.push(ele);
        }
        
        var results = SortInputFirstFlight(cityName, Cityvalues);
        for (var i = 0; i < results.length; i++) {
            if (localStorage.Languagecode == "ar") {
                finalResult.push({
                    "code": results[i].iata_code,
                    "label": (results[i].name_arabic || results[i].name ) + ", " + (results[i].country_name_arabic || results[i].country_name) + '(' + results[i].iata_code + ')',
                    "municipality": results[i].municipality_arabic || results[i].municipality,
                    "country": results[i].iso_country
                });
            } else {
              finalResult.push({
                "code": results[i].iata_code,
                "label": results[i].name + ", " + results[i].country_name + '(' + results[i].iata_code + ')',
                "municipality": results[i].municipality,
                "country": results[i].iso_country
            });
            }
        }
        // var newData = [];
        // finalResult.forEach(function (item, index) {
        //   // if (item.label.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0 || item.code.toLowerCase().includes(keywordEntered.toLowerCase()) || item.municipality.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
        //   newData.push(item);
        //   // }
        // });
        searchResult = finalResult;
        // self.autoCompleteProgress = false;
    }).catch(function (error) {
          console.log(error)
      });
  //   var client = new elasticsearch.Client({
  //       host: [{
  //           host: ServiceUrls.elasticSearch.elasticsearchHost,
  //           auth: ServiceUrls.elasticSearch.auth,
  //           protocol: ServiceUrls.elasticSearch.protocol,
  //           port: ServiceUrls.elasticSearch.port,
  //           requestTimeout: 60000
  //       }],
  //       log: 'trace'
  //   });
  //  await client.search({
  //       index: 'airport_info',
  //       size: 25,
  //       timeout: "3000ms",
  //       body: query
  //   }).then(async function (resp) {
  //       finalResult = [];
  //       var hits = resp.hits.hits;
  //       var Citymap = new Map();
  //       for (var i = 0; i < hits.length; i++) {
  //           Citymap.set(hits[i]._source.iata_code, hits[i]._source);
  //       }
  //       var get_values = Citymap.values();
  //       var Cityvalues = [];
  //       // for (var ele of get_values) {
  //       //   Cityvalues.push(ele);
  //       // }
  //       for (var ele of get_values) {
  //           Cityvalues.push(ele);
  //       }
        
  //       var results = SortInputFirstFlight(cityName, Cityvalues);
  //       for (var i = 0; i < results.length; i++) {
  //           if (localStorage.Languagecode == "ar") {
  //               finalResult.push({
  //                   "code": results[i].iata_code,
  //                   "label": (results[i].name_arabic || results[i].name ) + ", " + (results[i].country_name_arabic || results[i].country_name) + '(' + results[i].iata_code + ')',
  //                   "municipality": results[i].municipality_arabic || results[i].municipality,
  //                   "country": results[i].iso_country
  //               });
  //           } else {
  //             finalResult.push({
  //               "code": results[i].iata_code,
  //               "label": results[i].name + ", " + results[i].country_name + '(' + results[i].iata_code + ')',
  //               "municipality": results[i].municipality,
  //               "country": results[i].iso_country
  //           });
  //           }
  //       }
  //       // var newData = [];
  //       // finalResult.forEach(function (item, index) {
  //       //   // if (item.label.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0 || item.code.toLowerCase().includes(keywordEntered.toLowerCase()) || item.municipality.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
  //       //   newData.push(item);
  //       //   // }
  //       // });
  //       searchResult = finalResult;
  //       // self.autoCompleteProgress = false;
  //   })
    console.log('test',searchResult);
    // localStorage.elasticSearchResults = JSON.stringify(searchResult);
    return searchResult;
}

async function getAirportUsingElasticSearch(airportCode) {
    var searchResult=[];
    var query = {
        "size": 1,
        "query": {
            "bool": {
                "filter": [
                    {
                    "match_phrase": {
                        "iata_code": airportCode
                    }
                    }
                ]
            }
        }
    }
    await axios.post(ServiceUrls.elasticSearch.url, query, {
      headers: {
        "Content-Type": "application/json"
      }
      }).then(async function (resp) {
        finalResult = [];
        var hits = resp.data.hits.hits;
        var Citymap = new Map();
        for (var i = 0; i < hits.length; i++) {
            Citymap.set(hits[i]._source.iata_code, hits[i]._source);
        }
        var get_values = Citymap.values();
        var Cityvalues = [];
        // for (var ele of get_values) {
        //   Cityvalues.push(ele);
        // }
        for (var ele of get_values) {
            Cityvalues.push(ele);
        }

        var results = SortInputFirstFlight(airportCode, Cityvalues);
        for (var i = 0; i < results.length; i++) {
            if (localStorage.Languagecode == "ar") {
              finalResult.push({
                  "code": results[i].iata_code,
                  "label": (results[i].name_arabic || results[i].name ) + '(' + results[i].iata_code + ')',
                  "municipality": results[i].municipality_arabic || results[i].municipality,
                  "country": results[i].iso_country
              });
            } else {
                finalResult.push({
                  "code": results[i].iata_code,
                  "label": results[i].name + '(' + results[i].iata_code + ')',
                  "municipality": results[i].municipality,
                  "country": results[i].iso_country
              });
            }
            if (localStorage.airportDetailsFromElastic) {
              var airportDetailsFromElastic = JSON.parse(localStorage.airportDetailsFromElastic);
              var hasDataValue = _.find(airportDetailsFromElastic, function(airport){ return airport.I == results[i].iata_code; });
              if (hasDataValue == undefined) {
                airportDetailsFromElastic.push({
                  C: results[i].municipality_arabic,
                  CN: results[i].country_name_arabic,
                  I: results[i].iata_code,
                  N: results[i].name_arabic
                });
              }
              localStorage.airportDetailsFromElastic = JSON.stringify(airportDetailsFromElastic);
            } else {
              localStorage.airportDetailsFromElastic = JSON.stringify([{
                C: results[i].municipality_arabic,
                CN: results[i].country_name_arabic,
                I: results[i].iata_code,
                N: results[i].name_arabic
              }]);

            }
        }
        searchResult = finalResult;
      }).catch(function (error) {
          console.log(error)
      });
    // var client = new elasticsearch.Client({
    //     host: [{
    //         host: ServiceUrls.elasticSearch.elasticsearchHost,
    //         auth: ServiceUrls.elasticSearch.auth,
    //         protocol: ServiceUrls.elasticSearch.protocol,
    //         port: ServiceUrls.elasticSearch.port,
    //         requestTimeout: 60000
    //     }],
    //     log: 'trace'
    // });
    // await client.search({
    //     index: 'airport_info',
    //     size: 25,
    //     timeout: "3000ms",
    //     body: query
    // }).then(async function (resp) {
    //     finalResult = [];
    //     var hits = resp.hits.hits;
    //     var Citymap = new Map();
    //     for (var i = 0; i < hits.length; i++) {
    //         Citymap.set(hits[i]._source.iata_code, hits[i]._source);
    //     }
    //     var get_values = Citymap.values();
    //     var Cityvalues = [];
    //     // for (var ele of get_values) {
    //     //   Cityvalues.push(ele);
    //     // }
    //     for (var ele of get_values) {
    //         Cityvalues.push(ele);
    //     }

    //     var results = SortInputFirstFlight(airportCode, Cityvalues);
    //     for (var i = 0; i < results.length; i++) {
    //         if (localStorage.Languagecode == "ar") {
    //           finalResult.push({
    //               "code": results[i].iata_code,
    //               "label": (results[i].name_arabic || results[i].name ) + '(' + results[i].iata_code + ')',
    //               "municipality": results[i].municipality_arabic || results[i].municipality,
    //               "country": results[i].iso_country
    //           });
    //         } else {
    //             finalResult.push({
    //               "code": results[i].iata_code,
    //               "label": results[i].name + '(' + results[i].iata_code + ')',
    //               "municipality": results[i].municipality,
    //               "country": results[i].iso_country
    //           });
    //         }
    //         if (localStorage.airportDetailsFromElastic) {
    //           var airportDetailsFromElastic = JSON.parse(localStorage.airportDetailsFromElastic);
    //           var hasDataValue = _.find(airportDetailsFromElastic, function(airport){ return airport.I == results[i].iata_code; });
    //           if (hasDataValue == undefined) {
    //             airportDetailsFromElastic.push({
    //               C: results[i].municipality_arabic,
    //               CN: results[i].country_name_arabic,
    //               I: results[i].iata_code,
    //               N: results[i].name_arabic
    //             });
    //           }
    //           localStorage.airportDetailsFromElastic = JSON.stringify(airportDetailsFromElastic);
    //         } else {
    //           localStorage.airportDetailsFromElastic = JSON.stringify([{
    //             C: results[i].municipality_arabic,
    //             CN: results[i].country_name_arabic,
    //             I: results[i].iata_code,
    //             N: results[i].name_arabic
    //           }]);

    //         }
    //     }
    //     searchResult = finalResult;
    // })
    console.log(searchResult)
    return searchResult;
}

/**Sorting Function for AutoCompelte Start**/
function SortInputFirstFlight(input, data) {

    var output = [];
    for (var i = 0; i < data.length; i++) {
        data[i].municipality = isNullorEmptyToBlank(data[i].municipality);
        output.push(data[i]);
    }
    return output;
}
/**Sorting Function for AutoCompelte End**/

function isNullorEmpty(value) {
    var status = false;
    if (value == null || value == undefined || value == "undefined") {
        status = true;
    }
    if (!status && $.trim(value) == '') {
        status = true;
    }
    return status;
}

function isNullorEmptyToBlank(value, optval) {
    return isNullorEmpty(value) ? (isNullorEmpty(optval) ? '' : optval) : value;
}

function getSuppliers(legDetails, callback) {
  debugger;
  var airProv = [];
  var UserData = JSON.parse(localStorage.User);
  var supDetails = [];
  if (UserData.loginNode.servicesList != undefined) {
      supDetails = UserData.loginNode.servicesList.filter(function (service) {
          return service.name.toLowerCase().includes('air')
      });
      if (supDetails.length > 0) {
        supDetails[0].provider.forEach(function (data) {
            airProv.push(data.id.toString());
        });
      } else {
        airProv = [];
      }
  }

  var PostData = {
    Leg: legDetails,
    SupplierCode: airProv
  };

  axios
    .post(ServiceUrls.elasticSearch.airFilter, PostData, {
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then(async function (response) {
      if (response.data) {
        var tempSupp = [];
        for (let index = 0; index < response.data.data.length; index++) {
          tempSupp.push(response.data.data[index].SupplierCode);
        }
        var tmpArr = airProv.filter(function(e) { return ([1, 45, 55, 64, 42].indexOf(e) != -1) || (["1", "45", "55", "64", "42"].indexOf(e) != -1)});
        var newArr = tempSupp.concat(tmpArr);
        callback(newArr.join("|"));
      } else {
        callback("all");
      }
    })
    .catch(function (error) {
      console.log(error);
    });
}