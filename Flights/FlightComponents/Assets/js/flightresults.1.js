
var searchSummary = new Vue({
    el: '#vueSummary',
    name: 'flSummary',
    data: {
        searchData: getSearchData(),
        tripType: getSearchData().request.content.criteria.triptype
    },
    methods: {
        moment: function (date) {
            return moment(date);
        },
        getCabName: function (cab) {
            return getCabinClassName(cab);
        },
        airportFromAirportCode: function (airport) {
            return airportFromAirportCode(airport);
        },
        getTripType: function () {
            var urldata = getUrlVars().flight;
            if (urldata == undefined) {
                window.location.href = 'index.html';
            }
            var urld = urldata.substring(1, urldata.length).split('/')
            var triptypes = urld[urld.length - 1];
            var tripTyppee = triptypes.split('-')[7];
            this.tripType = tripTyppee;
            if (tripTyppee === 'o') {
                return 1
            }
            else if (tripTyppee === 'r') {
                return 2
            }
            else {
                return 3
            }
        },
        getViaCities: function (destinations) {
            var outStr = '';
            destinations.forEach(function (dest, idx) {
                if (idx !== destinations.length - 1) {
                    if (outStr === '') {
                        outStr += airportLocationFromAirportCode(dest.destinationLocation);
                    } else {
                        outStr += ', ' + airportLocationFromAirportCode(dest.destinationLocation);
                    }
                }
            });
            return outStr;
        },
    },
    filters: {
        momDToFull: function (date) {
            return moment(date, 'DD-MM-YYYY').format('ddd, DD MMMM YYYY');
        }
    }
});

const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})

var searchResults = new Vue({
    i18n,
    el: '#vueResults',
    name: 'flResult',
    data: {
        allFlightRes: [],
        flightResBind: [],
        tripType: getSearchData().request.content.criteria.tripType,
        // cdnDomain: cdnDomain,
        weatherImg: 'http://openweathermap.org/img/w/',
        weatherCity: airportData(getSearchData().request.content.criteria.originDestinationInformation[0].destinationLocation)[0].C,
        timeOfsetCity: airportData(getSearchData().request.content.criteria.originDestinationInformation[0].destinationLocation)[0].TZ,
        arrCityTime: '',
        totalResCount: 0,
        resStops: [],
        checkStops: [],
        resAirlines: [],
        checkAirlines: [],
        checkAirlinesMatrix: '',
        currency: '',
        resMinPrice: 0,
        resMaxPrice: 0,
        changedMinPrice: 0,
        changedMaxPrice: 0,
        resDepLoc: [],
        selDeptLoc: [],
        deptLocFiltrChanged: 0,
        resArrLoc: [],
        selArrtLoc: [],
        arrLocFiltrChanged: 0,
        weatherData: null,
        SorPriL: false,
        SorPriH: false,
        SorDuraL: false,
        SorDuraH: false,
        SorDepL: false,
        SorDepH: false,
        SorArrL: false,
        SorArrH: false,
        hideAllLoaders: false,
        hasError: false,
        errMsg: '',
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        priceSortOrder: true,
        durationSortOrder: true,
        deptSortOrder: true,
        ArrSortOrder: true

    },
    created() {
        var rq = getSearchData();
        websocket.connect();
        var self = this;

        setTimeout(function () {
            try {
                self.hasError = false;
                websocket.sendMessage(JSON.stringify(rq)); //send request to web socket server 
            } catch (error) {
                setTimeout(function () {
                    try {
                        self.hasError = false;
                        websocket.sendMessage(JSON.stringify(rq));
                    } catch (error) {
                        self.errMsg = "Our systems seem to be experiencing an issue. Please refresh the page. " + error;
                        self.hideAllLoaders = true;
                        self.hasError = true;
                    }
                }, 2000);
            }
        }, 1000);

        this.getWeather(function (resp) {
            self.weatherData = resp;
        });
        this.getArrCityTime();
    },
    methods: {
        openFlightData(event, tabName, fIndex) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(tabName).style.display = "block";
            event.currentTarget.className += " active";

            if (tabName.split('_')[0] === "Fare-Rules") {
                if (event.currentTarget.getAttribute('isOpened') === "0") {
                    event.currentTarget.setAttribute('isOpened', "1");
                    this.getMiniRules(fIndex, tabName);
                }
            }
        },
        getAirLineName: function (airlinecode) {
            return getAirLineName(airlinecode);
        },
        airportLocationFromAirportCode: function (airlinecode) {
            return airportLocationFromAirportCode(airlinecode);
        },
        airportFromAirportCode: function (airport) {
            return airportFromAirportCode(airport);
        },
        airportData: function (airlinecode) {
            return airportData(airlinecode);
        },
        calcLayoverTime: function (arrDate, arrTime, depDate, depTime) {
            return calcLayoverTime(arrDate, arrTime, depDate, depTime);
        },
        getBaggageUnitString: function (baggageValue) {
            return getBaggageUnitString(baggageValue);
        },
        isRefundableFlight: function (refundString) {
            return isRefundableFlight(refundString);
        },
        rDeptLocChange: function (e) {
            if (this.selDeptLoc.indexOf(e) !== -1) {
                this.selDeptLoc.splice(this.selDeptLoc.indexOf(e), 1);
            }
            else {
                var splited = e.split('-');
                var index = this.selDeptLoc.findIndex(function (element) {
                    return element.split('-')[0] === splited[0];
                })
                if (index !== -1) {
                    this.selDeptLoc[index] = e;
                }
                else {
                    this.selDeptLoc.push(e);
                }
            }
            this.deptLocFiltrChanged = 1;
            this.flightResBindFn();
        },
        rArrLocChange: function (e) {
            if (this.selArrtLoc.indexOf(e) !== -1) {
                this.selArrtLoc.splice(this.selArrtLoc.indexOf(e), 1);
            }
            else {
                var splited = e.split('-');
                var index = this.selArrtLoc.findIndex(function (element) {
                    return element.split('-')[0] === splited[0];
                })
                if (index !== -1) {
                    this.selArrtLoc[index] = e;
                }
                else {
                    this.selArrtLoc.push(e);
                }
            }
            this.arrLocFiltrChanged = 1;
            this.flightResBindFn();
        },
        getAirLineNameTrim: function (airlinecode, limit) {
            var airN = getAirLineName(airlinecode);
            return airN.length < limit ? airN : airN.substring(0, limit) + "...";
        },
        sortRes: function (sortType, by) {

            switch (sortType) {
                case 'price':
                    this.priceSortOrder = !this.priceSortOrder;
                    this.durationSortOrder = true;
                    this.deptSortOrder = true;
                    this.ArrSortOrder = true;
                    if (this.priceSortOrder) {
                        // if (by == 1) {
                        this.SorPriL = true;
                        this.SorPriH = false;
                        this.SorDuraL = false;
                        this.SorDuraH = false;
                        this.SorDepL = false;
                        this.SorDepH = false;
                        this.SorArrL = false;
                        this.SorArrH = false;
                    } else {
                        this.SorPriH = true;
                        this.SorPriL = false;
                        this.SorDuraL = false;
                        this.SorDuraH = false;
                        this.SorDepL = false;
                        this.SorDepH = false;
                        this.SorArrL = false;
                        this.SorArrH = false;
                    }
                    break;
                case 'dura':
                    this.durationSortOrder = !this.durationSortOrder;
                    this.priceSortOrder = true;
                    this.deptSortOrder = true;
                    this.ArrSortOrder = true;
                    if (this.durationSortOrder) {
                        //if (by == 1) {
                        this.SorDuraL = true;
                        this.SorDuraH = false;
                        this.SorPriL = false;
                        this.SorPriH = false;
                        this.SorDepL = false;
                        this.SorDepH = false;
                        this.SorArrL = false;
                        this.SorArrH = false;
                    } else {
                        this.SorDuraH = true;
                        this.SorDuraL = false;
                        this.SorPriL = false;
                        this.SorPriH = false;
                        this.SorDepL = false;
                        this.SorDepH = false;
                        this.SorArrL = false;
                        this.SorArrH = false;
                    }
                    break;
                case 'dept':
                    this.deptSortOrder = !this.deptSortOrder;
                    this.priceSortOrder = true;
                    this.durationSortOrder = true;
                    this.ArrSortOrder = true;
                    if (this.deptSortOrder) {
                        // if (by == 1) {
                        this.SorDepL = true;
                        this.SorDepH = false;
                        this.SorPriL = false;
                        this.SorPriH = false;
                        this.SorDuraL = false;
                        this.SorDuraH = false;
                        this.SorArrL = false;
                        this.SorArrH = false;
                    } else {
                        this.SorDepH = true;
                        this.SorDepL = false;
                        this.SorPriL = false;
                        this.SorPriH = false;
                        this.SorDuraL = false;
                        this.SorDuraH = false;
                        this.SorArrL = false;
                        this.SorArrH = false;
                    }
                    break;
                case 'arri':
                    this.ArrSortOrder = !this.ArrSortOrder;
                    this.priceSortOrder = true;
                    this.durationSortOrder = true;
                    this.deptSortOrder = true;
                    if (this.ArrSortOrder) {
                        // if (by == 1) {
                        this.SorArrL = true;
                        this.SorArrH = false;
                        this.SorPriL = false;
                        this.SorPriH = false;
                        this.SorDuraL = false;
                        this.SorDuraH = false;
                        this.SorDepL = false;
                        this.SorDepH = false;
                    } else {
                        this.SorArrH = true;
                        this.SorArrL = false;
                        this.SorPriL = false;
                        this.SorPriH = false;
                        this.SorDuraL = false;
                        this.SorDuraH = false;
                        this.SorDepL = false;
                        this.SorDepH = false;
                    }
                    break;

                default:
                    break;
            }

            this.flightResBindFn();
        },
        getWeather: function (callback) {
            var weatCity = this.weatherCity;
            axios.get('https://api.openweathermap.org/data/2.5/weather?q=' + weatCity + '&units=metric&appid=7c0ff0999be50513159abbf786d93a0a', {
                headers: { 'content-type': 'application/json' }
            }).then(function (response) {
                console.log(response);
                callback(response);
            });
        },
        momGetDate: function (toFormat) {
            return moment().format(toFormat);
        },
        processFlightResult: function (response) {
            console.log(response)
            this.hideAllLoaders = true;
            if (response.response.content.fareMasterPricerTravelBoardSearchReply.flightIndex != null) {
                this.hasError = false;
                this.allFlightRes = response.response.content.fareMasterPricerTravelBoardSearchReply.flightIndex;
                this.flightResBind = response.response.content.fareMasterPricerTravelBoardSearchReply.flightIndex;
                this.totalResCount = response.response.content.fareMasterPricerTravelBoardSearchReply.flightIndex.length;
                localStorage.access_token = response.response.token;
                localStorage.timer = new Date();

                this.currency = this.allFlightRes[0].fare.currency;
                //Get Filter Data
                this.resMinPrice = _.min(this.allFlightRes, function (flind) { return flind.fare.amount; }).fare.amount;
                this.resMaxPrice = _.max(this.allFlightRes, function (flind) { return flind.fare.amount; }).fare.amount;

                var tempAirlines = [];
                var tempStops = [];
                var tempDepLoc = [];
                var tempArrLoc = [];

                this.allFlightRes.forEach(fl => {
                    fl.groupOfFlights.forEach(leg => {
                        tempStops.push(leg.flightDetails.length - 1)
                        tempDepLoc.push(leg.flightDetails[0].fInfo.location.locationFrom)
                        tempArrLoc.push(leg.flightDetails[leg.flightDetails.length - 1].fInfo.location.locationTo)
                        leg.flightDetails.forEach(seg => {
                            tempAirlines.push({ "air": seg.fInfo.companyId.mCarrier, "price": fl.fare.amount });
                        });
                    });
                });

                this.resStops = _.sortBy(_.uniq(tempStops));
                this.resAirlines = _.chain(tempAirlines).sortBy(['air', 'price'], ['asc', 'desc']).uniq('air').value();
                this.resDepLoc = _.uniq(tempDepLoc);
                this.resArrLoc = _.uniq(tempArrLoc);

                var self = this;
                //Price Slider
                var slider = document.getElementById('price-slider')

                if (self.resMinPrice != self.resMaxPrice) {
                    noUiSlider.create(slider, {
                        start: [self.resMinPrice, self.resMaxPrice],
                        connect: true,
                        step: 5,
                        range: {
                            'min': self.resMinPrice,
                            'max': self.resMaxPrice
                        }
                    });

                    slider.noUiSlider.on('update', function (values, handle) {
                        $("#minPricevalue").text(self.$n(parseFloat(values[0] / self.CurrencyMultiplier), 'currency', self.selectedCurrency));
                        $("#MaxPricevalue").text(self.$n(parseFloat(values[1] / self.CurrencyMultiplier), 'currency', self.selectedCurrency));

                        if (self.changedMinPrice == 0) {
                            self.changedMinPrice = parseFloat(values[0]);
                        }
                        if (self.changedMaxPrice == 0) {
                            self.changedMaxPrice = parseFloat(values[1]);
                        }
                    });

                    slider.noUiSlider.on('end', function (values, handle) {
                        self.changedMinPrice = parseFloat(values[0]);
                        self.changedMaxPrice = parseFloat(values[1]);
                        self.flightResBindFn();
                    });
                }
                else {
                    self.changedMinPrice = self.resMinPrice;
                    self.changedMaxPrice = self.resMaxPrice;
                }

                Vue.nextTick(function () {
                    var owl = $("#slider-carousel");
                    owl.owlCarousel({
                        items: 5,
                        itemsDesktop: [1024, 4],
                        itemsDesktopSmall: [768, 4],
                        itemsTablet: [480, 2],
                        itemsMobile: false,
                        pagination: false
                    });
                    $(".next").click(function () {
                        owl.trigger('owl.next');
                    })
                    $(".prev").click(function () {
                        owl.trigger('owl.prev');
                    })

                    $("#hide, #hide1, #hide2, #hide4, #hide5, #hide6, #hide7, #hide8, #hide9, #hide10, #hide11, #hide12, #hide13").click(function () {
                        $(".tabcontent").hide();
                    });

                    $('.tab_content').hide();
                    $('.tab_content:first').show();
                    $('.tabs li').click(function (event) {
                        $(this).toggleClass('active');
                        $(this).siblings().removeClass('active');
                        $('.tab_content').hide();
                        var selectTab = $(this).find('a').attr("href");
                        $(selectTab).fadeIn();
                    });

                }.bind(self));
            } else {
                if (response.response.content.error.message != null) {
                    this.hasError = true;
                    this.errMsg = response.response.content.error.message;
                }
            }
        },
        unChkMatrix: function (val) {
            if (val === this.checkAirlinesMatrix) {
                this.checkAirlinesMatrix = '';
                this.flightResBindFn();
            }
        },
        getMiniRules: function (flIndex, tabName) {
            var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;
            var airMiniRule = ServiceUrls.hubConnection.hubServices.flights.airMiniRule;
            var currenttoken = localStorage.access_token;

            var self = this;

            var rq = this.createSelRqMinRq('minirule', flIndex);
            console.log(rq)

            axios.post(hubUrl + airMiniRule, rq, {
                headers: { 'Authorization': 'Bearer ' + currenttoken }
            }).then(function (response) {
                console.log(response)
                localStorage.access_token = response.headers.access_token;
                localStorage.timer = new Date();

                var miniRuleLists = [];

                var miniRules = response.data.response.content.minirules;
                miniRules.forEach(rule => {

                    var miniRules = [];

                    rule.codes[0].category.forEach(cat => {
                        if (cat.cat == "31" || cat.cat == "33") {
                            var before33Restriappinfo = { action: 0 };
                            var before33Moninfo = { amount: 0, currency: '' };
                            var before31Restriappinfo = { action: 0 };
                            var before31Moninfo = { amount: 0, currency: '' };

                            var after33Restriappinfo = { action: 0 };
                            var after33Moninfo = { amount: 0, currency: '' };
                            var after31Restriappinfo = { action: 0 };
                            var after31Moninfo = { amount: 0, currency: '' };

                            if (cat.cat == "31") {
                                try { before33Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'BDA'); } catch (err) { }
                                try { before33Moninfo = cat.monInfo.find(x => x.typeQualifier == 'BDX'); } catch (err) { }
                                try { after33Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'BDA'); } catch (err) { }
                                try { after33Moninfo = cat.monInfo.find(x => x.typeQualifier == 'BDX'); } catch (err) { }

                                miniRules.push({
                                    type: 'Cancellation',
                                    before: ((before33Restriappinfo.action == 1) ? ('Allowed @ ' + before33Moninfo.currency + ' ' + before33Moninfo.amount) : 'Not Allowed'),
                                    after: ((before31Restriappinfo.action == 1) ? ('Allowed @ ' + before31Moninfo.currency + ' ' + before31Moninfo.amount) : 'Not Allowed')
                                });
                            }
                            if (cat.cat == "33") {
                                try { before31Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'ADA'); } catch (err) { }
                                try { before31Moninfo = cat.monInfo.find(x => x.typeQualifier == 'ADX'); } catch (err) { }
                                try { after31Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'ADA'); } catch (err) { }
                                try { after31Moninfo = cat.monInfo.find(x => x.typeQualifier == 'ADX'); } catch (err) { }

                                miniRules.push({
                                    type: 'Changes',
                                    before: ((after33Restriappinfo.action == 1) ? ('Allowed @ ' + after33Moninfo.currency + ' ' + after33Moninfo.amount) : 'Not Allowed'),
                                    after: ((after31Restriappinfo.action == 1) ? ('Allowed @ ' + after31Moninfo.currency + ' ' + after31Moninfo.amount) : 'Not Allowed')
                                });
                            }
                        }
                    });

                    var miniRuleList = {
                        "from": rule.departure,
                        "to": rule.arrival,
                        "miniRules": miniRules
                    };

                    miniRuleLists.push(miniRuleList);
                });

                Vue.set(self.flightResBind[flIndex], 'miniRules', miniRuleLists);
            }).catch(function (error) {
                console.log('Error on Mini Rules - ' + error);
            });
        },
        createSelRqMinRq: function (type, flIndex) {
            var searchData = getSearchData();
            var selFlightOption = this.flightResBind[flIndex];

            var segGroups = [];
            selFlightOption.groupOfFlights.forEach(function (leg, legind) {
                leg.flightDetails.forEach(function (seg, segind) {

                    var Seginfo = {
                        "segmentInformation": {
                            "flightDate": {
                                "departureDate": seg.fInfo.dateTime.depDate,
                                "departureTime": seg.fInfo.dateTime.depTime,
                                "arrivalDate": seg.fInfo.dateTime.arrDate,
                                "arrivalTime": seg.fInfo.dateTime.arrTime
                            },
                            "fareId": seg.fInfo.eqpType,
                            "boardPointDetails": {
                                "trueLocationId": seg.fInfo.location.locationFrom
                            },
                            "offPointDetails": {
                                "trueLocationId": seg.fInfo.location.locationTo
                            },
                            "companyDetails": {
                                "marketingCompany": seg.fInfo.companyId.mCarrier,
                                "operatingCompany": seg.fInfo.companyId.mCarrier,
                                "validatingCompany": seg.fInfo.companyId.mCarrier,
                            },
                            "flightIdentification": {
                                "flightNumber": seg.fInfo.flightNo,
                                "bookingClass": seg.fInfo.rbd
                            },
                            "flightTypeDetails": {
                                "flightIndicator": String(legind + 1),
                                "itemNumber": segind + 1
                            }
                        }
                    }
                    segGroups.push(Seginfo);

                });
            });

            var Com = "FlightSelectRQ";
            if (type === "minirule") {
                Com = "FlightMiniRulesRQ";
            }

            var rq = {
                "request": {
                    "service": "FlightRQ",
                    "supplierCodes": [
                        1
                    ],
                    "content": {
                        "command": Com,
                        "commonRequestFarePricer": {
                            "body": {
                                "airRevalidate": {
                                    "fareSourceCode": null,
                                    "sessionId": null,
                                    "target": "Test",
                                    "adt": searchData.request.content.criteria.passengerTypeQuantity.adt,
                                    "chd": searchData.request.content.criteria.passengerTypeQuantity.chd,
                                    "inf": searchData.request.content.criteria.passengerTypeQuantity.inf,
                                    "paymentCardType": null,
                                    "segmentGroup": segGroups,
                                    "tripType": searchData.request.content.criteria.tripType,
                                    "from": selFlightOption.groupOfFlights[0].flightDetails[0].fInfo.location.locationFrom,
                                    "to": selFlightOption.groupOfFlights[selFlightOption.groupOfFlights.length - 1].flightDetails[selFlightOption.groupOfFlights[selFlightOption.groupOfFlights.length - 1].flightDetails.length - 1].fInfo.location.locationTo,
                                    "dealcode": []
                                }
                            },
                            "supplierAgencyDetails": null
                        },
                        "supplierSpecific": {
                            "test": "test"
                        }
                    }
                }
            }

            return rq;
        },
        selectFlight: function (ev, flIndex) {
            var selectFlightRq = this.createSelRqMinRq('select', flIndex);
            var selectedFlightInfo = this.flightResBind[flIndex];
            localStorage.selectedFlight = JSON.stringify({ "selectFlightRq": selectFlightRq, "selectedFlightInfo": selectedFlightInfo });
            window.location.href = '/Flights/flight-booking.html';
        },
        getElapseTime: function (elapStr) {
            return elapStr.substring(0, 2) + 'h ' + elapStr.substring(2, 4) + 'm ';

            //var elap = elapStr.substring(0, 2) + '.' + elapStr.substring(2, 4);
            // var hour = parseFloat(elapStr.substring(0, 2));
            // var min = parseFloat(elapStr.substring(2, 4));
            // var hpm = hour + '.' + min;
            // var hm = Math.round(hpm * 100) / 100;
            // var dur = moment.duration(hm, 'hours');
            // var hours = Math.floor(dur.asHours());
            // var mins = Math.floor(dur.asMinutes()) - hours * 60;
            // return hours + "h " + mins + "m ";

            // var elapInt = parseInt(elapStr);
            // var dur = moment.duration(elap, 'hours');
            // return dur.format('HH:mm:ss');
        },
        getArrCityTime: function () {
            var d = new Date();
            var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
            var nd = new Date(utc + (3600000 * this.timeOfsetCity));
            this.arrCityTime = moment(nd).format('hh:mm a');
        },
        hideFlDet: function () {
            $(".tabcontent").hide();
        },
        flightResBindFn: function () {
            var temp = [];
            if (this.allFlightRes.length > 0) {
                temp = this.allFlightRes;

                if (this.resMinPrice != this.changedMinPrice || this.resMaxPrice != this.changedMaxPrice) {
                    var tempchangedMinPrice = this.changedMinPrice;
                    var changedMaxPrice = this.changedMaxPrice;

                    temp = temp.filter(function (res) {
                        var takeLeg = false;
                        if (res.fare.amount >= tempchangedMinPrice && res.fare.amount <= changedMaxPrice) {
                            takeLeg = true;
                        }
                        return takeLeg;
                    })
                }

                if (this.checkStops.length > 0) {
                    var selStops = this.checkStops;
                    temp = temp.filter(function (res) {
                        var takeLeg = false;
                        res.groupOfFlights.forEach(leg => {
                            selStops.forEach(stop => {
                                if (stop === leg.flightDetails.length - 1) {
                                    takeLeg = true;
                                }
                            });
                        });
                        return takeLeg;
                    })
                }

                if (this.checkAirlines.length > 0) {
                    var selAirlines = this.checkAirlines;
                    temp = temp.filter(function (res) {
                        var takeLeg = false;
                        res.groupOfFlights.forEach(leg => {
                            leg.flightDetails.forEach(seg => {
                                selAirlines.forEach(air => {
                                    if (air.air === seg.fInfo.companyId.mCarrier) {
                                        takeLeg = true;
                                    }
                                });
                            });
                        });
                        return takeLeg;
                    })
                }

                if (this.checkAirlinesMatrix.length > 0) {
                    var selAirlines = this.checkAirlinesMatrix;
                    temp = temp.filter(function (res) {
                        var takeLeg = false;
                        res.groupOfFlights.forEach(leg => {
                            leg.flightDetails.forEach(seg => {
                                if (selAirlines === seg.fInfo.companyId.mCarrier) {
                                    takeLeg = true;
                                }
                            });
                        });
                        return takeLeg;
                    })
                }

                if (this.selDeptLoc.length > 0 && this.deptLocFiltrChanged != 2) {
                    var selDeptLoc = this.selDeptLoc;
                    temp = temp.filter(function (res) {
                        var takeLeg = false;
                        res.groupOfFlights.forEach(leg => {
                            selDeptLoc.forEach(deploc => {
                                var depLocSplit = deploc.split('-');
                                if (depLocSplit[0] === leg.flightDetails[0].fInfo.location.locationFrom) {
                                    var dTime = parseInt(leg.flightDetails[0].fInfo.dateTime.depTime.substring(0, 2))
                                    if (dTime >= parseInt(depLocSplit[1]) && dTime <= parseInt(depLocSplit[2])) {
                                        takeLeg = true;
                                    }
                                }
                            });
                        });
                        return takeLeg;
                    })
                    this.deptLocFiltrChanged = 0;
                }

                if (this.selArrtLoc.length > 0 && this.arrLocFiltrChanged != 2) {
                    var selArrtLoc = this.selArrtLoc;
                    temp = temp.filter(function (res) {
                        var takeLeg = false;
                        res.groupOfFlights.forEach(leg => {
                            selArrtLoc.forEach(arrloc => {
                                var arrLocSplit = arrloc.split('-');
                                if (arrLocSplit[0] === leg.flightDetails[leg.flightDetails.length - 1].fInfo.location.locationTo) {
                                    var arrTime = parseInt(leg.flightDetails[leg.flightDetails.length - 1].fInfo.dateTime.arrTime.substring(0, 2))
                                    if (arrTime >= parseInt(arrLocSplit[1]) && arrTime <= parseInt(arrLocSplit[2])) {
                                        takeLeg = true;
                                    }
                                }
                            });
                        });
                        return takeLeg;
                    })
                    this.arrLocFiltrChanged = 0;
                }

                if (this.SorPriL) {
                    temp.sort(function (a, b) {
                        return parseFloat(a.fare.amount) - parseFloat(b.fare.amount);
                    });
                    //temp = _.sortBy(temp, 'fare.amount');
                }

                if (this.SorPriH) {
                    temp.sort(function (a, b) {
                        return parseFloat(b.fare.amount) - parseFloat(a.fare.amount);
                    });
                    //temp = _.sortBy(temp, 'fare.amount').reverse();
                }

                if (this.SorDuraL) {
                    temp.sort(function (a, b) {
                        return parseInt(a.groupOfFlights[0].flightProposal.elapse) - parseInt(b.groupOfFlights[0].flightProposal.elapse)
                        //return moment(a.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm')
                    });
                }

                if (this.SorDuraH) {
                    temp.sort(function (a, b) {
                        return parseInt(a.groupOfFlights[0].flightProposal.elapse) - parseInt(b.groupOfFlights[0].flightProposal.elapse)
                        //return moment(a.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm')
                    }).reverse();
                }

                if (this.SorDepL) {
                    temp.sort(function (a, b) {
                        return moment(a.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm')
                    });
                }

                if (this.SorDepH) {
                    temp.sort(function (a, b) {
                        return moment(a.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm')
                    }).reverse();
                }

                if (this.SorArrL) {
                    temp.sort(function (a, b) {
                        return moment(a.groupOfFlights[0].flightDetails[a.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[b.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm')
                    });
                }

                if (this.SorArrH) {
                    temp.sort(function (a, b) {
                        return moment(a.groupOfFlights[0].flightDetails[a.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[b.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm')
                    }).reverse();
                }

                this.totalResCount = temp.length;
                window.scrollTo(0, 0);
            }

            this.flightResBind = temp;
        }
    },
    computed: {
        // flightResBind: function () {
        //     var temp = [];
        //     if (this.allFlightRes.length > 0) {
        //         temp = this.allFlightRes;

        //         if (this.resMinPrice != this.changedMinPrice || this.resMaxPrice != this.changedMaxPrice) {
        //             var tempchangedMinPrice = this.changedMinPrice;
        //             var changedMaxPrice = this.changedMaxPrice;

        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 if (res.fare.amount >= tempchangedMinPrice && res.fare.amount <= changedMaxPrice) {
        //                     takeLeg = true;
        //                 }
        //                 return takeLeg;
        //             })
        //         }

        //         if (this.checkStops.length > 0) {
        //             var selStops = this.checkStops;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     selStops.forEach(stop => {
        //                         if (stop === leg.flightDetails.length - 1) {
        //                             takeLeg = true;
        //                         }
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //         }

        //         if (this.checkAirlines.length > 0) {
        //             var selAirlines = this.checkAirlines;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     leg.flightDetails.forEach(seg => {
        //                         selAirlines.forEach(air => {
        //                             if (air.air === seg.fInfo.companyId.mCarrier) {
        //                                 takeLeg = true;
        //                             }
        //                         });
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //         }

        //         if (this.checkAirlinesMatrix.length > 0) {
        //             var selAirlines = this.checkAirlinesMatrix;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     leg.flightDetails.forEach(seg => {
        //                         if (selAirlines === seg.fInfo.companyId.mCarrier) {
        //                             takeLeg = true;
        //                         }
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //         }

        //         if (this.selDeptLoc.length > 0 && this.deptLocFiltrChanged != 2) {
        //             var selDeptLoc = this.selDeptLoc;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     selDeptLoc.forEach(deploc => {
        //                         var depLocSplit = deploc.split('-');
        //                         if (depLocSplit[0] === leg.flightDetails[0].fInfo.location.locationFrom) {
        //                             var dTime = parseInt(leg.flightDetails[0].fInfo.dateTime.depTime.substring(0, 2))
        //                             if (dTime >= parseInt(depLocSplit[1]) && dTime <= parseInt(depLocSplit[2])) {
        //                                 takeLeg = true;
        //                             }
        //                         }
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //             this.deptLocFiltrChanged = 0;
        //         }

        //         if (this.selArrtLoc.length > 0 && this.arrLocFiltrChanged != 2) {
        //             var selArrtLoc = this.selArrtLoc;
        //             temp = temp.filter(function (res) {
        //                 var takeLeg = false;
        //                 res.groupOfFlights.forEach(leg => {
        //                     selArrtLoc.forEach(arrloc => {
        //                         var arrLocSplit = arrloc.split('-');
        //                         if (arrLocSplit[0] === leg.flightDetails[leg.flightDetails.length - 1].fInfo.location.locationTo) {
        //                             var arrTime = parseInt(leg.flightDetails[leg.flightDetails.length - 1].fInfo.dateTime.arrTime.substring(0, 2))
        //                             if (arrTime >= parseInt(arrLocSplit[1]) && arrTime <= parseInt(arrLocSplit[2])) {
        //                                 takeLeg = true;
        //                             }
        //                         }
        //                     });
        //                 });
        //                 return takeLeg;
        //             })
        //             this.arrLocFiltrChanged = 0;
        //         }

        //         if (this.SorPriL) {
        //             temp.sort(function (a, b) {
        //                 return parseFloat(a.fare.amount) - parseFloat(b.fare.amount);
        //             });
        //             //temp = _.sortBy(temp, 'fare.amount');
        //         }

        //         if (this.SorPriH) {
        //             temp.sort(function (a, b) {
        //                 return parseFloat(b.fare.amount) - parseFloat(a.fare.amount);
        //             });
        //             //temp = _.sortBy(temp, 'fare.amount').reverse();
        //         }

        //         if (this.SorDuraL) {
        //             temp.sort(function (a, b) {
        //                 return parseInt(a.groupOfFlights[0].flightProposal.elapse) - parseInt(b.groupOfFlights[0].flightProposal.elapse)
        //                 //return moment(a.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm')
        //             });
        //         }

        //         if (this.SorDuraH) {
        //             temp.sort(function (a, b) {
        //                 return parseInt(a.groupOfFlights[0].flightProposal.elapse) - parseInt(b.groupOfFlights[0].flightProposal.elapse)
        //                 //return moment(a.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightProposal.elapse, 'HHmm').format('HHmm')
        //             }).reverse();
        //         }

        //         if (this.SorDepL) {
        //             temp.sort(function (a, b) {
        //                 return moment(a.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm')
        //             });
        //         }

        //         if (this.SorDepH) {
        //             temp.sort(function (a, b) {
        //                 return moment(a.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm')
        //             }).reverse();
        //         }

        //         if (this.SorArrL) {
        //             temp.sort(function (a, b) {
        //                 return moment(a.groupOfFlights[0].flightDetails[a.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[b.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm')
        //             });
        //         }

        //         if (this.SorArrH) {
        //             temp.sort(function (a, b) {
        //                 return moment(a.groupOfFlights[0].flightDetails[a.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[b.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm')
        //             }).reverse();
        //         }

        //         this.totalResCount = temp.length;
        //         window.scrollTo(0, 0);
        //     }

        //     return temp
        // }
    },
    filters: {
        momCommon: function (date, frmFormat, toFormat) {
            return moment(date, frmFormat).format(toFormat);
        }
    }
});

function getSearchData() {
    var urldata = getUrlVars().flight;
    if (urldata == undefined) {
        window.location.href = 'index.html';
    }
    return createflightSearchRequest(urldata.substring(1, urldata.length).split('/'));
}

function getCabinClassName(cabinCode) {
    switch (cabinCode.toUpperCase()) {
        case 'F':
            return "First Class"
            break;
        case 'C':
            return "Business"
            break;
        default:
            return "Economy"
            break;
    }
}

function createflightSearchRequest(tripLeg) {
    var originDestinationInformationArr = [];
    for (var i = 0; i < tripLeg.length - 1; i++) {
        var Trip = tripLeg[i];
        var TripFrom = Trip.split('-')[0].toUpperCase();;
        var TripTo = Trip.split('-')[1].toUpperCase();;
        var TripDate = Trip.split('-')[2];
        var TripArray = {
            departureDate: moment(TripDate, 'DD|MM|YYYY').format("DD-MM-YYYY"),
            originLocation: TripFrom,
            destinationLocation: TripTo,
            radiusInformation: {
                _FromValue: '0',
                _ToValue: '250'
            }
        }
        originDestinationInformationArr.push(TripArray);
    }
    var triptypes = tripLeg[tripLeg.length - 1];
    var totalADT = triptypes.split('-')[0];
    var totalCHD = triptypes.split('-')[1];
    var totalINF = triptypes.split('-')[2];
    var tripClass = triptypes.split('-')[3];
    if (tripClass.toLowerCase() == 'all') {
        tripClass = 'All';
    } else {
        tripClass = tripClass.toUpperCase();
    }
    var tripTyppee = triptypes.split('-')[7];
    var totalpax = parseInt(totalADT) + parseInt(totalCHD) + parseInt(totalINF);
    var supplierCode = triptypes.split('-')[4];
    if (supplierCode == 'all') {
        supplierCode = [];
    } else {
        supplierCode = supplierCode.split('|');
    }
    nmberOfRec = triptypes.split('-')[5];
    var filter = false;
    var isfiltered = triptypes.split('-')[6];
    if (isfiltered == 'T') {
        filter = true;
    }
    var stopQuantity = triptypes.split('-')[10];
    if (stopQuantity == "df") {
        stopQuantity = "Direct";
    }
    else {
        stopQuantity = "All";
    }
    var preferAirline = [triptypes.split('-')[9]];
    if (preferAirline == "" || preferAirline == undefined) {
        preferAirline = [];
    }

    var flightSearchRQ = {
        "request": {
            "service": "FlightRQ",
            "token": localStorage.access_token,
            "supplierCodes": [1],
            "content": {
                "command": "FlightSearchRQ",
                "criteria": {
                    "criteriaType": "Air",
                    "commonRequestSearch": {
                        "numberOfUnits": parseInt(totalpax),
                        "typeOfUnit": "PX",
                        "numberOfRec": nmberOfRec,
                        "isFiltered": filter
                    },
                    "originDestinationInformation": originDestinationInformationArr,
                    "preferredAirline": preferAirline,
                    "nonStop": false,
                    cabin: tripClass,
                    "isRefundable": false,
                    "maxStopQuantity": stopQuantity,
                    "tripType": tripTyppee,
                    "preferenceLevel": "Preferred",
                    "target": "Test",
                    "passengerTypeQuantity": {
                        adt: parseInt(totalADT),
                        chd: parseInt(totalCHD),
                        inf: parseInt(totalINF)
                    },
                    "requestOption": null,
                    "pricingSource": null,
                    "dealcode": null
                },
                "sort": [
                    {
                        "field": "price",
                        "order": "asc",
                        "sequence": 1
                    }
                ],
                "filter": {
                    "noOfResults": "250",
                    "from": 1,
                    "to": 100,
                    "sequence": 1
                },
                "supplierSpecific": {
                    "test": "test"
                }
            }
        }
    }

    return flightSearchRQ;
}