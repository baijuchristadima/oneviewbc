const printing = window.matchMedia("print");
var provider = [];
const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})

function isNullorUndefind(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == []) {
        status = true;
    }
    return status;
}
var bookinginfo_vue = new Vue({
    i18n,
    el: "#div_bookinginfo",

    data: {
        content: null,
        active: false,
        bookDetailsSeen: true,
        bookinginfo: {
            "data": {
                "response": {
                    "userID": 0,
                    "agencyCode": "",
                    "bookingRefId": "",
                    "bookingStatus": "",
                    "bookingStatusName": "",
                    "paymentStatus": false,
                    "pnrNum": "",
                    "bookingDate": "",
                    "ticketLimit": "",
                    "totalFareGroup": {
                        "totalBaseNet": "",
                        "totalTaxNet": "",
                        "markupValue": 0,
                        "sellAmount": "",
                        "sellCurrency": "",
                        "additionalServiceFee": "",
                        "cancellationAmount": "",
                        "paidAmount": "",
                        "totalCouponDiscount": 0
                    },
                    "travelerInfo": [{
                        "passengerType": "",
                        "givenName": "",
                        "namePrefix": "",
                        "surname": "",
                        "birthDate": "",
                        "docType": "",
                        "documentNumber": "",
                        "docIssueCountry": "",
                        "expireDate": "",
                        "airTicketNo": null,
                        "extras": []
                    }],
                    "adt": 0,
                    "chd": 0,
                    "inf": 0,
                    "costBreakuppax": [],
                    "miniRules": [],
                    "flLegGroup": [],
                    "paymentMode": ""
                }
            },
        },
        user: {
            "id": "",
            "loginId": "",
            "title": {
                "id": "",
                "name": ""
            },
            "status": "",
            "emailId": "",
            "firstName": "",
            "lastName": "",
            "usercontactNumber": {
                "number": ""
            },
            "userAdress": {
                "city": ""
            },
            "contactNumber": "",
            "loginNode": {
                "code": "",
                "name": "",
                "currency": "",
                "city": "",
                "cityName": "",
                "country": {
                    "code": "",
                    "telephonecode": ""
                },
                "url": "",
                "logo": "",
                "homePageURL": "",
                "loginUrl": "",
                "address": "",
                "zip": "",
                "email": "",
                "phoneList": [{
                        "type": "",
                        "nodeContactNumber": {
                            "number": ""
                        },
                        "number": ""
                    },
                    {
                        "type": "",
                        "nodeContactNumber": {
                            "number": ""
                        },
                        "number": ""
                    },
                    {
                        "type": "",
                        "nodeContactNumber": {
                            "number": ""
                        },
                        "number": ""
                    }
                ],
                "nodetype": "",
                "solutionId": "",
                "lookNfeel": {
                    "primary": "",
                    "secondary": "",
                    "tertiary": ""
                },
                "tenant": {
                    "id": "",
                    "name": "",
                    "status": "",
                    "solutionTypeList": [{
                        "id": "",
                        "name": ""
                    }]
                },
                "site": {
                    "title": "",
                    "fevIcon": "",
                    "url": ""
                },
                "servicesList": [{
                    "id": "",
                    "name": "",
                    "provider": []
                }]
            },
            "roleList": []

        },
        supDetails: [],
        bookData: {
            BkngRefID: '',
            emailId: '',
            redirectFrom: '',
            isMailsend: false
        },
        divBkngSucces: false,
        divBkngFail: false,
        flightMailDiv: true,
        axiosConfig: {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                'Authorization': "Bearer " + localStorage.access_token
            }
        },
        huburl: ServiceUrls.hubConnection.baseUrl,
        portno: ServiceUrls.hubConnection.ipAddress,
        selectedCurrency: null,
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        VoucherLogoUrl: '',
        isUserHaveOfflineIssuance: false,
        cmscontent: {
            Confirmation_message: '',
            Cancelled_message: '',
            Book_reference_no: '',
            Pnr_label: '',
            Ticket_mail_message: '',
            Flight_details_label: '',
            Flight_to_label: '',
            Traveler_detail_label: '',
            Passenger_name: '',
            Type: '',
            Ticket_no_label: '',
            Baggage_detail_label: '',
            Flight_from: '',
            Cabin_baggage_label: '',
            Free_label: '',
            Checked_baggage_label: '',
            Payment_details_label: '',
            Adult_label: '',
            Child_label: '',
            Infant_label: '',
            Fare_label: '',
            Taxes_and_Fees_label: '',
            Total_label: '',
            Customer_Support_label: '',
            Address_label: '',
            Phone_label: '',
            Email_label: '',
            PrintVoucher_Label: '',
            DownloadVoucher_Label: ''

        },
        showSpinnerHtml: false,
        emailOption: "yes",
        insuranceDetails: null,
        haveSmsNotification: false
    },
    created: function () {
        var userNode = JSON.parse(localStorage.User);
        if (userNode) {
            this.selectedCurrency = (localStorage.selectedCurrency) ? localStorage.selectedCurrency : userNode.loginNode.currency;
        } else {
            this.selectedCurrency = (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD';
        }
        try {
            var b2cRoles = JSON.parse(localStorage.roleList).filter(function (role) {
                return role.id == 6;
            })[0] || {};
            if (!_.isEmpty(b2cRoles)) {
                var flightRoles = _.find(b2cRoles.moduleList, function (service) {
                    return service.name.toLowerCase() == 'flight';
                }) || {};
                if (!_.isEmpty(flightRoles)) {
                    var roleBookOnly = _.find(flightRoles.functionalityList, function (e) {
                        return e.id == 193
                    });
                    var roleBookOnHold = _.find(flightRoles.functionalityList, function (e) {
                        return e.id == 198;
                    });
                    var bookType = JSON.parse(localStorage.bookType);
                    if (!_.isEmpty(roleBookOnHold) && bookType == 1) {
                        this.isUserHaveOfflineIssuance = true;
                    } else if (!_.isEmpty(roleBookOnly)) {
                        this.isUserHaveOfflineIssuance = true;
                    }
                    var roleBookSms = _.find(flightRoles.functionalityList, function (e) { return e.id == 211 });
                        if (!_.isEmpty(roleBookSms)) {
                            this.haveSmsNotification = true;
                        }
                }
            }
        } catch (e) {
            this.isUserHaveOfflineIssuance = false;
        }

        showHideConfirmLoader(true);
        this.bookData = JSON.parse(localStorage.bookData);
        this.user = JSON.parse(localStorage.User);
        this.supDetails = this.user.loginNode.servicesList.filter(function (service) {
            return service.name.toLowerCase().includes('air')
        });
        this.supDetails[0].provider.forEach(function (data) {
            provider.push(data.id);
        });
        const urlParams = new URLSearchParams(window.location.search);
        const paymentStatus = urlParams.get('status');
        // this.VoucherLogoUrl = ServiceUrls.hubConnection.logoBaseUrl + this.user.loginNode.logo + '.xhtml?ln=logo';
        this.VoucherLogoUrl = this.user.loginNode.logo;

        if (paymentStatus != null) {
            if (performance.navigation.type == 0) {
                if (paymentStatus == "101") {
                    if (this.isUserHaveOfflineIssuance) {
                        this.bookData.isMailsend = true;
                        this.tripDetails();
                    } else {
                        var PostData = "";
                        var selectCred = null;
                        try {
                          selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"));
                        } catch (err) {}

                        PostData = {
                          request: {
                            service: "FlightRQ",
                            content: {
                              command: "FlightTripDetailsRQ",
                              supplierSpecific: {},
                              tripDetailRQ: {
                                bookingRefId: this.bookData.BkngRefID,
                              },
                            },
                            selectCredential: selectCred,
                            supplierCodes: null,
                            hqCode:this.user.loginNode.solutionId ? this.user.loginNode.solutionId :"",
                          },
                        };
                        console.log("TripDetailReq", JSON.stringify(PostData));
                        var requrl = ServiceUrls.hubConnection.hubServices.flights.airTripDetails;
                        axios
                          .post(this.huburl + this.portno + requrl, PostData, this.axiosConfig)
                          .then(res => {
                            console.log("TripDetails Response: ", res);
                            this.issueTicket(res.data.response.content.supplierSpecific ? res.data.response.content.supplierSpecific : {});
                          })
                          .catch(err => {
                            console.log("TripDetails Error: ", err);
                            showHideConfirmLoader(false);
                          });
                    }
                } else if (paymentStatus == "102") {
                    this.cancelPNR();
                } else {
                    showHideConfirmLoader(false);
                    alertify.alert('Error', 'Server error!');
                }
            } else {
                this.tripDetails();
            }
        } else if (this.bookData != null && this.bookData.redirectFrom == 'retrievebooking' ||
            this.bookData.redirectFrom == 'myBookings') {
            this.tripDetails()
        } else {
            alertify.alert('Error', 'Server error !');
        }
    },
    mounted: function () {
        this.getpagecontent();

    },
    methods: {
        getTerminal: function (terminal) {
            return isNullorUndefind(terminal) ? 'Terminal N/A' : 'Terminal ' + terminal;
        },
        issueTicket: function (supplierSpecific) {
            var vm = this;
            var selectCred = null;
            var smsNotification = JSON.parse(localStorage.smsNotification)
            try {
                selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"))
            } catch (err) {}
            var PostData = {
                request: {
                    service: "FlightRQ",
                    content: {
                        command: "FlightIssueTicketRQ",
                        supplierSpecific: supplierSpecific,
                        issueTicketRQ: {
                            smsNotification: vm.haveSmsNotification ? smsNotification : undefined,
                            bookingRefId: vm.bookData.BkngRefID,
                            passengerTypeQuantity: vm.bookData.passengerTypeQuantity
                        }
                    },
                    selectCredential: selectCred,
                    supplierCodes: provider,
                    hqCode:vm.user.loginNode.solutionId ? vm.user.loginNode.solutionId :"",
                }
            };
            console.log('IssueTicketReq', JSON.stringify(PostData));
            var requrl = ServiceUrls.hubConnection.hubServices.flights.airIssueTicket;
            axios.post(vm.huburl + vm.portno + requrl, PostData, vm.axiosConfig)
                .then((res) => {
                    console.log("IssueTicket Response: ", res);
                    localStorage.access_token = res.headers.access_token;
                    vm.bookData.isMailsend = res.data.response.content.issueTicketRS.success;
                    if (res.data.response.content.issueTicketRS.success) {
                        if (sessionStorage.insuranceRQ) {
                            var insuranceData = JSON.parse(sessionStorage.insuranceRQ);
                            insuranceData.request.content.bookingRef = sessionStorage.insResbookingRef;
                            insuranceData.request.content.isHubOnly = false;

                            var tpGetCurrencyCode = ServiceUrls.hubConnection.hubServices.insurance.purchase;

                            axios.post(vm.huburl + vm.portno + tpGetCurrencyCode, insuranceData, {
                                headers: {
                                    Authorization: "Bearer " + localStorage.access_token
                                }
                            }).then(function (response) {
                                var insertBookingIds = ServiceUrls.hubConnection.hubServices.cart;
                                console.log(response.data.response.content.bookingRef);
                                axios.post(vm.huburl + vm.portno + insertBookingIds, [res.data.response.content.issueTicketRS.bookingRefId, response.data.response.content.bookingRef], {
                                    headers: {
                                        Authorization: "Bearer " + localStorage.access_token
                                    }
                                }).then(function (cartRes) {
                                    console.log(cartRes.data.response.data);
                                    vm.tripDetails("success");
                                }).catch(function (error) {
                                    vm.tripDetails("success");
                                });
                            }).catch(function (error) {
                                vm.tripDetails("success");
                            });
                        } else {
                            vm.tripDetails("success");
                        }
                    } else {
                        vm.tripDetails("cancel");
                    }
                })
                .catch((err) => {
                    console.log("IssueTicket Error: ", err);
                    showHideConfirmLoader(false);
                    alertify.alert('Error', 'Server error !');
                })
        },
        cancelPNR: function () {
            this.divBkngSucces = false;
            this.divBkngFail = true;
            this.flightMailDiv = false;
            var selectCred = null;
            try {
                selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"))
            } catch (err) {}
            var PostData = {
                request: {
                    service: "FlightRQ",
                    content: {
                        command: "FlightCancelPnrRQ",
                        supplierSpecific: {},
                        cancelPnrRQ: {
                            bookingRefId: this.bookData.BkngRefID
                        }
                    },
                    selectCredential: selectCred,
                    supplierCodes: provider
                }
            };
            console.log('CancelPNRReq', JSON.stringify(PostData));
            var requrl = ServiceUrls.hubConnection.hubServices.flights.AircancelPnr;
            axios.post(this.huburl + this.portno + requrl, PostData, this.axiosConfig)
                .then((res) => {
                    console.log("CancelPnr Response: ", res);
                    localStorage.access_token = res.headers.access_token;
                    console.log("Cancelled Status ", res.data.response.content.cancelPnrRS.success);
                    this.bookData.isMailsend = true;
                    this.tripDetails("cancel");
                })
                .catch((err) => {
                    console.log("Cancelled Error: ", err);
                    showHideConfirmLoader(false);
                    alertify.alert('Error', 'Server error !');

                })
        },
        tripDetails: function (status) {
            var PostData = '';
            var selectCred = null;
            try {
                selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"))
            } catch (err) {}
            if (this.bookData.redirectFrom == 'retrievebooking') {
                PostData = {
                    request: {
                        service: "FlightRQ",
                        content: {
                            command: "FlightTripDetailsRQ",
                            supplierSpecific: {},
                            tripDetailRQ: {
                                bookingRefId: this.bookData.BkngRefID,
                                emailId: this.bookData.emailId
                            }
                        },
                        selectCredential: selectCred,
                        supplierCodes: null,
                        hqCode:this.user.loginNode.solutionId ? this.user.loginNode.solutionId :"",
                    }

                };
            } else {
                PostData = {
                    request: {
                        service: "FlightRQ",
                        content: {
                            command: "FlightTripDetailsRQ",
                            supplierSpecific: {},
                            tripDetailRQ: {
                                bookingRefId: this.bookData.BkngRefID
                            }
                        },
                        selectCredential: selectCred,
                        supplierCodes: null,
                        hqCode:this.user.loginNode.solutionId ? this.user.loginNode.solutionId :"",
                    }
                };
            }
            if (performance.navigation.type != 0) {
                this.bookData.isMailsend = false;
            }
            var vm = this;
            var isUserHaveInsuranceBooking = false;
            try {
                var b2cRoles = JSON.parse(localStorage.roleList).filter(function (role) { return role.id == 6; })[0] || {};
                if (!_.isEmpty(b2cRoles)){
                    var flightRoles = _.find(b2cRoles.moduleList, function (service) { return service.name.toLowerCase() == 'flight'; }) ||{};
                    if (!_.isEmpty(flightRoles)) {
                        var roleBookInsurance = _.find(flightRoles.functionalityList, function (e) { return e.id == 208 });
                        if (!_.isEmpty(roleBookInsurance)) {
                            isUserHaveInsuranceBooking = true;
                        }
                        
                    }
                }
            } catch (e) {}
            console.log('TripDetailReq', JSON.stringify(PostData));
            var requrl = ServiceUrls.hubConnection.hubServices.flights.airTripDetails;
            axios.post(this.huburl + this.portno + requrl, PostData, this.axiosConfig)
                .then((res) => {
                    console.log("TripDetails Response: ", res);
                    if (isUserHaveInsuranceBooking) {
                        var getBookingIds = ServiceUrls.hubConnection.hubServices.cart;
                        axios.get(vm.huburl + vm.portno + getBookingIds + "/" + vm.bookData.BkngRefID, {
                            headers: {
                                Authorization: "Bearer " + localStorage.access_token
                            }
                        }).then(function (cartRes) {
                            if (cartRes.data.length > 0) {
                                var getInsurancDetails = ServiceUrls.hubConnection.hubServices.insurance.tuneprotectDetail;
                                var cartIns = cartRes.data.filter(function (e) {
                                    return e.service.id == 7;
                                })[0];
                                var dataRequest = {
                                    request: {
                                        service: "InsuranceRQ",
                                        content: {
                                            command: "TuneprotectDetailsRQ",
                                            supplierSpecific: {},
                                            policyDetails: {
                                                bookingRef: cartIns.bookingRef
                                            }
                                        },
                                        selectCredential: cartIns.selectCredential,
                                        supplierCodes: null
                                    }
                                }
                                axios.post(vm.huburl + vm.portno + getInsurancDetails, dataRequest, {
                                    headers: {
                                        Authorization: "Bearer " + localStorage.access_token
                                    }
                                }).then(function (ins) {
                                    vm.insuranceDetails = ins.data.response.content.booking;
                                    localStorage.access_token = res.headers.access_token;
                                    vm.bookinginfo.data.response = res.data.response.content.tripDetailRS.tripDetailsUiData.response;
                                    if (vm.bookinginfo.data.response.bookingStatus == "XX" ||
                                        vm.bookinginfo.data.response.bookingStatus == "RF" ||
                                        vm.bookinginfo.data.response.bookingStatus == "TF") {
                                        vm.divBkngSucces = false;
                                        vm.divBkngFail = true;
                                        vm.flightMailDiv = false;
                                    } else {
                                        vm.divBkngSucces = true;
                                    }
                                    vm.bookDetailsSeen = true;

                                    if (vm.isUserHaveOfflineIssuance && vm.bookData.isMailsend) {
                                        vm.sendMail(vm.bookinginfo.data.response, vm.user.loginNode.parentEmailId, "admin", status);
                                    } else {
                                        if (vm.insuranceDetails && vm.insuranceDetails.success.policyNum == undefined) {
                                            vm.sendMail(vm.bookinginfo.data.response, vm.user.loginNode.parentEmailId, "admin", status);
                                        }
                                    }
                                    if (vm.bookData.isMailsend) {
                                        vm.sendMail(vm.bookinginfo.data.response, localStorage.cartEmailId, "user", status);
                                    }
                                    if (!bookinginfo_vue.bookData.isMailsend) {
                                        showHideConfirmLoader(false);
                                    } 
                                    setTimeout(() => {
                                        $('[data-toggle="tooltip"]').tooltip();
                                    }, 100);
                                }).catch(function (error) {});
                            } else {
                                localStorage.access_token = res.headers.access_token;
                                vm.bookinginfo.data.response = res.data.response.content.tripDetailRS.tripDetailsUiData.response;
                                if (vm.bookinginfo.data.response.bookingStatus == "XX" ||
                                    vm.bookinginfo.data.response.bookingStatus == "RF" ||
                                    vm.bookinginfo.data.response.bookingStatus == "TF") {
                                    vm.divBkngSucces = false;
                                    vm.divBkngFail = true;
                                    vm.flightMailDiv = false;
                                } else {
                                    vm.divBkngSucces = true;
                                }
                                vm.bookDetailsSeen = true;
                                if (vm.isUserHaveOfflineIssuance && vm.bookData.isMailsend) {
                                    vm.sendMail(vm.bookinginfo.data.response, vm.user.loginNode.parentEmailId, "admin", status);
                                }
                                if (vm.bookData.isMailsend) {
                                    vm.sendMail(vm.bookinginfo.data.response, localStorage.cartEmailId, "user", status);
                                }
                                if (!bookinginfo_vue.bookData.isMailsend) {
                                    showHideConfirmLoader(false);
                                } 
                                setTimeout(() => {
                                    $('[data-toggle="tooltip"]').tooltip();
                                }, 100);
                            }

                        }).catch(function (error) {});
                    } else {
                        localStorage.access_token = res.headers.access_token;
                        this.bookinginfo.data.response = res.data.response.content.tripDetailRS.tripDetailsUiData.response;
                        if (this.bookinginfo.data.response.bookingStatus == "XX" ||
                            this.bookinginfo.data.response.bookingStatus == "RF" ||
                            this.bookinginfo.data.response.bookingStatus == "TF") {
                            this.divBkngSucces = false;
                            this.divBkngFail = true;
                            this.flightMailDiv = false;
                        } else {
                            this.divBkngSucces = true;
                        }
                        this.bookDetailsSeen = true;
                        if (vm.isUserHaveOfflineIssuance && this.bookData.isMailsend) {
                            vm.sendMail(vm.bookinginfo.data.response, vm.user.loginNode.parentEmailId, "admin", status);
                        }
                        if (this.bookData.isMailsend) {
                            this.sendMail(this.bookinginfo.data.response, localStorage.cartEmailId, "user", status);
                        }
                        if (!bookinginfo_vue.bookData.isMailsend) {
                            showHideConfirmLoader(false);
                        } 
                    }

                })
                .catch((err) => {
                    console.log("TripDetails Error: ", err);
                    showHideConfirmLoader(false);
                    this.bookDetailsSeen = false;
                    try {
                        if (err.response.data.message == 'Booking details not found!') {
                            alertify.alert('Error', err.response.data.message);
                        } else {
                            alertify.alert('Error', 'Server error !');
                        }
                    } catch (err) {
                        alertify.alert('Error', 'Server error !');
                    }

                })
        },
        sendMail: function (tripData, toEmail, toWhom, status) {
            var Host = window.location.hostname;
            var tripName = this.getTripName(tripData);
            var self = this;
            $.getJSON('/Resources/HubUrls/AgencyCredentials.json', function (json) {
                let agency = json.Agencies.find(o => o.Domain.toLowerCase() === Host.toLowerCase());
                if (typeof agency !== "undefined") {
                    try {
                        var travellers = [];
                        tripData.travelerInfo.forEach(function (trveler) {
                            var travelerType = '';
                            travelerType = trveler.passengerType;
                            var passenger = {
                                paxName: trveler.namePrefix + ' ' + trveler.givenName + ' ' + trveler.surname,
                                type: travelerType,
                                ticketNumber: isNullorUndefind(trveler.airTicketNo) ? 'NA' : trveler.airTicketNo,
                            }
                            travellers.push(passenger);
                        });
                        var flightDetails = [];
                        var baggageinfo = [];
                        tripData.flLegGroup.forEach(function (legGroup) {
                            var stops = bookinginfo_vue.getNoOfStop(legGroup.segments);
                            legGroup.segments.forEach(function (flight) {
                                // var termnlFrom=flight.terminalFrom ? flight.terminalFrom : 'N/A';
                                // var termnlTo=flight.terminalTo ? flight.terminalTo : 'N/A';
                                var destn = airportLocationFromAirportCode(flight.departureFrom) + ' - ' + airportLocationFromAirportCode(flight.departureTo);
                                var sgmnts = {
                                    airlineName: bookinginfo_vue.getAirLineName(flight.operatingCompany),
                                    airlineCode: flight.operatingCompany,
                                    flightNumber: flight.flightNumber,
                                    departureAirport: airportFromAirportCode(flight.departureFrom),
                                    departureCity: bookinginfo_vue.airportLocationFromAirportCode(flight.departureFrom),
                                    arrivalAirport: airportFromAirportCode(flight.departureTo),
                                    arrivalCity: bookinginfo_vue.airportLocationFromAirportCode(flight.departureTo),
                                    deparureTime: bookinginfo_vue.moment(flight.departureDate).format('HH:mm'),
                                    arrivalTime: bookinginfo_vue.moment(flight.arrivalDate).format('HH:mm'),
                                    arrivalDate: bookinginfo_vue.moment(flight.arrivalDate).format("DD MMM' YY, ddd"),
                                    deparureDate: bookinginfo_vue.moment(flight.departureDate).format("DD MMM' YY, ddd"),
                                    airlineLogoUrl: window.location.origin + "/Flights/assets/images/AirLines/" + flight.operatingCompany + ".gif",
                                    bookingClass: bookinginfo_vue.getCabinName(flight.bookingClass) || "",
                                    flightDuration: bookinginfo_vue.GetFlightdurationtime(flight.departureFrom, flight.departureTo, flight.departureDate, flight.arrivalDate),
                                }
                                flightDetails.push(sgmnts);
                                var cabinBaggage = '';
                                var checkinBaggage = '';
                                flight.airBagDetails.forEach(function (bagDetail) {

                                    if (bagDetail.cabinBaggageQuantity != undefined) {

                                        cabinBaggage = bagDetail.cabinBaggageQuantity.replace(".0", "") + ' ' + bagDetail.cabinBaggageUnit + '/Person';
                                    }
                                    if (bagDetail.checkinBaggageQuantity != undefined) {
                                        var unit = '';
                                        if ((bagDetail.checkinBaggageUnit).toLowerCase() == 'piece' ||
                                            (bagDetail.checkinBaggageUnit).toLowerCase() == 'p' ||
                                            (bagDetail.checkinBaggageUnit).toLowerCase() == 'n' ||
                                            (bagDetail.checkinBaggageUnit).toLowerCase() == 'pc') {
                                            unit = 'Piece'
                                        } else if ((bagDetail.checkinBaggageUnit).toLowerCase() == 'k' ||
                                            (bagDetail.checkinBaggageUnit).toLowerCase() == 'kg' ||
                                            (bagDetail.checkinBaggageUnit).toLowerCase() == 'w' ||
                                            (bagDetail.checkinBaggageUnit).toLowerCase() == '700') {

                                            unit = 'KG';
                                        }
                                        checkinBaggage = bagDetail.checkinBaggageQuantity.replace(".0", "") + ' ' + unit + '/Person';
                                    }
                                });
                                var bagDetail = {
                                    cabinBaggage: cabinBaggage,
                                    checkedBaggage: checkinBaggage,
                                    destination: 'Flight From ' + destn
                                }
                                baggageinfo.push(bagDetail);
                            });
                        });
                        var phoneNumber = bookinginfo_vue.user.loginNode.phoneList.filter(function (phones) {
                            return phones.type.toLowerCase().includes('telephone')
                        });
                        var frommail = isNullorUndefind(bookinginfo_vue.user.loginNode.parentEmailId) ? 'uaecrt@gmail.com' : bookinginfo_vue.user.loginNode.parentEmailId;
                        var ccEmails = _.filter(bookinginfo_vue.user.loginNode.emailList, function (o) {
                            return o.emailTypeId == 5 && o.emailType.toLowerCase() == 'air issuance';
                        });
                        ccEmails = _.map(ccEmails, function (o) {
                            return o.emailId
                        });

                        var messageContent = "";

                        if (status && status == "cancel" && toWhom == "admin") {
                            messageContent = "Failed Booking Notification.";
                        } else if (status && status == "cancel" && toWhom == "user") {
                            messageContent = "Your flight booking has failed.";
                        } else if (status && status == "success" && toWhom == "user") {
                            messageContent = "Congratulations! Your booking has been confirmed.";
                        } else if (toWhom == "admin") {
                            messageContent = "The booking is confirmed and awaiting issuance.";
                        } else if (toWhom == "user") {
                            messageContent = "Congratulations! Your booking has been confirmed. Your e-ticket will be sent shortly.";
                        }
                        var insuranceMessage = undefined;

                        if (self.insuranceDetails) {
                            if (self.insuranceDetails.success.policyNum == undefined) {
                                if (toWhom == "user") {
                                    insuranceMessage = "Your insurance booking with reference " + self.insuranceDetails.bookingRef + " is pending and will be sent to your email.";
                                } else if (toWhom == "admin") {
                                    insuranceMessage = "The insurance booking with reference " + self.insuranceDetails.bookingRef + " is failed and is pending for your action.";
                                }
                            }
                        }

                        var guestName = bookinginfo_vue.user.title.name + ' ' + bookinginfo_vue.user.firstName + ' ' + bookinginfo_vue.user.lastName;
                        if (toWhom == "admin") {
                            guestName = "Admin";
                        } else {
                            if (bookinginfo_vue.bookinginfo.data.response.travelerInfo[0].givenName || bookinginfo_vue.bookinginfo.data.response.travelerInfo[0].surname) {
                                guestName = bookinginfo_vue.bookinginfo.data.response.travelerInfo[0].namePrefix + ' ' +
                                    bookinginfo_vue.bookinginfo.data.response.travelerInfo[0].givenName + ' ' + bookinginfo_vue.bookinginfo.data.response.travelerInfo[0].surname;
                            }
                        }


                        // var postData = {
                        //     type: "FlightBookingVoucherV2",
                        //     fromEmail: frommail,
                        //     toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                        //     // toEmail: bookinginfo_vue.user.emailId,
                        //     ccEmails: ccEmails,
                        //     bccEmails: null,
                        //     primaryColor: null,
                        //     secondaryColor: null,
                        //     bookingStatusPic: status && status == "cancel" ? "https://s3.ap-south-1.amazonaws.com/images.oneviewitsolutions.com/Email-Template/images/flight-cancel.png" : "https://s3.ap-south-1.amazonaws.com/images.oneviewitsolutions.com/Email-Template/images/con-flight.png",
                        //     messageContent: messageContent,
                        //     displayTicketNumber: bookinginfo_vue.isUserHaveOfflineIssuance ? "none" : "block",
                        //     //toEmail: 'pavan@oneviewit.com',
                        //     // logo:protocol + '//' + hostName +'/assets/images/logo/logo.png',
                        //     //logo:'https://s3.amazonaws.com/ovit-fileupload/B2B/AdminPanel/CMS/AGY435/Images/a2z-logo.png',
                        //     logo: ServiceUrls.hubConnection.logoBaseUrl + bookinginfo_vue.user.loginNode.logo + '.xhtml?ln=logo',
                        //     agencyPhone: isNullorUndefind(phoneNumber[0].nodeContactNumber.number) ? 'NA' : '+' + bookinginfo_vue.user.loginNode.country.telephonecode +""+ phoneNumber[0].nodeContactNumber.number,
                        //     bookingRefNumber: tripData.bookingRefId,
                        //     refundable: "",
                        //     flightDetails: flightDetails,
                        //     passagerDetails: travellers,
                        //     cancellationPolicy: [
                        //         {
                        //             cancellationPolicyTitle: "",
                        //             cancellationPolicyDesc: "",
                        //             cancellationPolicyDetails: ""
                        //         }
                        //     ],
                        //     paymentDetails: {
                        //         adult: tripData.adt.toString(),
                        //         child: tripData.chd.toString(),
                        //         infant: tripData.inf.toString(),
                        //         // fare: tripData.totalFareGroup.sellCurrency + ' ' + tripData.totalFareGroup.totalBaseNet,
                        //         // taxesAndFees: tripData.totalFareGroup.sellCurrency + ' ' + tripData.totalFareGroup.totalTaxNet,
                        //         // serviceFee: "",
                        //         // total: tripData.totalFareGroup.sellCurrency + ' ' + tripData.totalFareGroup.sellAmount,
                        //         fare: bookinginfo_vue.$n((tripData.totalFareGroup.totalBaseNet / bookinginfo_vue.CurrencyMultiplier), 'currency', bookinginfo_vue.selectedCurrency),
                        //         taxesAndFees: bookinginfo_vue.$n((tripData.totalFareGroup.totalTaxNet / bookinginfo_vue.CurrencyMultiplier), 'currency', bookinginfo_vue.selectedCurrency),
                        //         serviceFee: "",
                        //         total: bookinginfo_vue.$n((tripData.totalFareGroup.sellAmount / bookinginfo_vue.CurrencyMultiplier), 'currency', bookinginfo_vue.selectedCurrency),
                        //     },
                        //     customerSupport: {                                
                        //         address: isNullorUndefind(bookinginfo_vue.user.loginNode.address)?'NA' : bookinginfo_vue.user.loginNode.address,
                        //         phone: isNullorUndefind(phoneNumber[0].nodeContactNumber.number) ? 'NA' : '+' +bookinginfo_vue.user.loginNode.country.telephonecode  +""+ phoneNumber[0].nodeContactNumber.number,
                        //         email: isNullorUndefind(bookinginfo_vue.user.loginNode.email)?'NA' : bookinginfo_vue.user.loginNode.email,
                        //     },
                        //     guestName: guestName,
                        //     pnr: isNullorUndefind(tripData.pnrNum) ? '' : tripData.pnrNum,
                        //     baggageDetails: baggageinfo
                        // };
                        // console.log('BkngMailReq: ', JSON.stringify(postData));
                        // var emailApi = ServiceUrls.emailServices.emailApi;
                        // sendMailService(emailApi, postData);
                        // bookinginfo_vue.bookData.isMailsend = false;

                        var postData = {
                            type: "",
                            toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                            fromEmail: frommail,
                            ccEmails: ccEmails,
                            bccEmails: null,
                            primaryColor: null,
                            secondaryColor: null,
                            subject: "",
                            // logoUrl: ServiceUrls.hubConnection.logoBaseUrl + bookinginfo_vue.user.loginNode.logo + '.xhtml?ln=logo',
                            logoUrl: bookinginfo_vue.user.loginNode.logo,
                            agencyName: bookinginfo_vue.user.loginNode.name,
                            agencyAddress: isNullorUndefind(bookinginfo_vue.user.loginNode.address) ? 'NA' : bookinginfo_vue.user.loginNode.address,
                            year: new Date().getFullYear().toString(),
                            agencyNumber: isNullorUndefind(phoneNumber[0].nodeContactNumber.number) ? 'NA' : '+' + bookinginfo_vue.user.loginNode.country.telephonecode + "" + phoneNumber[0].nodeContactNumber.number,
                            agencyEmail: isNullorUndefind(bookinginfo_vue.user.loginNode.email) ? 'NA' : bookinginfo_vue.user.loginNode.email,
                            fullName: guestName,
                            tripName: "",
                            displayStatus: messageContent,
                            status: "",
                            bookingRefId: tripData.bookingRefId,
                            pnrNumber: isNullorUndefind(tripData.pnrNum) ? '' : tripData.pnrNum,
                            totalFare: "",
                            segments: flightDetails,
                            paxList: travellers,
                            leadPaxDetails: "",
                            baggage: "",
                            fareBreakup: "",
                            ticketInfo: "e-ticket",
                            withAttachment: status == "success" ? true : false,
                            insuranceMessage: self.insuranceDetails && self.insuranceDetails.success.policyNum ? undefined : insuranceMessage

                        };
                        self.processingEmail = true;
                        self.getTemplate("FlightEmailTemplate").then(function (templateResponse) {
                            var data = templateResponse.data.data;
                            var emailTemplate = "";
                            if (data.length > 0) {
                                for (var x = 0; x < data.length; x++) {
                                    if (data[x].enabled == true && data[x].type == "FlightEmailTemplate") {
                                        emailTemplate = data[x].content;
                                        break;
                                    }
                                }
                            };
                            var htmlGenerate = ServiceUrls.emailServices.htmlGenerate
                            var emailData = {
                                template: emailTemplate,
                                content: postData
                            };
                            axios.post(htmlGenerate, emailData)
                                .then(function (htmlResponse) {
                                    if (status == "success") {
                                        self.getPdf().then(function (response) {
                                            var emailPostData = {
                                                type: "AttachmentRequest",
                                                toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                                                fromEmail: frommail,
                                                ccEmails: ccEmails.length == 0 ? null : ccEmails,
                                                bccEmails: null,
                                                subject: "Flight Confirmation - " + tripData.bookingRefId,
                                                attachmentPath: response.data,
                                                html: htmlResponse.data.data
                                            };
                                            var mailUrl = ServiceUrls.emailServices.emailApiWithAttachment;
                                            sendMailServiceWithCallBack(mailUrl, emailPostData, function () {
                                                showHideConfirmLoader(false);
                                            });
                                            self.processingEmail = false;
                                        });
                                    } else {
                                        var emailPostData = {
                                            type: "AttachmentRequest",
                                            toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                                            fromEmail: frommail,
                                            ccEmails: ccEmails.length == 0 ? null : ccEmails,
                                            bccEmails: null,
                                            subject: "Flight Confirmation - " + tripData.bookingRefId,
                                            attachmentPath: "",
                                            html: htmlResponse.data.data
                                        };
                                        var mailUrl = ServiceUrls.emailServices.emailApiWithAttachment;
                                        sendMailServiceWithCallBack(mailUrl, emailPostData, function () {
                                            showHideConfirmLoader(false);
                                        });
                                        self.processingEmail = false;
                                    }
                                    bookinginfo_vue.bookData.isMailsend = false;
                                }).catch(function (error) {
                                    self.showLoader = false;
                                    showHideConfirmLoader(false);
                                    return false;
                                })
                        });
                    } catch (err) {
                        showHideConfirmLoader(false);
                        console.log('SendMail Error: ', err)
                    }
                }
            });
        },
        printVoucher() {
            var self = this;
            var URL = "/Flights/flight-bookMailTemplate.html";
            axios.get(URL)
                .then(response => {
                    var printBody = self.generateVoucher(response.data);
                    var id = (new Date()).getTime();
                    var myWindow = window.open(window.location.href + '?printerFriendly=true', id, "toolbar=1,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=800,height=600,left = 240,top = 212");
                    myWindow.document.write(printBody);
                    myWindow.focus();
                    setTimeout(
                        function () {
                            myWindow.print();
                        }, 1000);
                }).catch(function (error) {
                    console.log('Error');
                    this.content = [];
                });
        },
        getTemplate(template) {
            var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
            var url = ServiceUrls.emailServices.getTemplate + "AGY75/" + template;
            // var url = ServiceUrls.emailServices.getTemplate + LoggedUser.loginNode.code + "/" + template;
            return axios.get(url);
        },
        getPdf(button) {

            if (button) {
                this.showSpinnerHtml = true;
            }
            var vm = this;
            var bookInfo = this.bookinginfo.data.response;

            var bookFlightStatusImg = window.location.origin + '/assets/images/con-flight.png';
            if (bookInfo.bookingStatus == "XX" ||
                this.bookinginfo.data.response.bookingStatus == "RF" ||
                this.bookinginfo.data.response.bookingStatus == "TF") {
                bookFlightStatusImg = window.location.origin + '/assets/images/con-flight-error.png';
            }
            var guestName = bookinginfo_vue.bookinginfo.data.response.travelerInfo[0].namePrefix + ' ' +
                bookinginfo_vue.bookinginfo.data.response.travelerInfo[0].givenName + ' ' + bookinginfo_vue.bookinginfo.data.response.travelerInfo[0].surname;
            var phoneNumber = bookinginfo_vue.user.loginNode.phoneList.filter(function (phones) {
                return phones.type.toLowerCase().includes('telephone')
            });
            var flightDetails = [];
            var baggageinfo = [];
            var travellers = [];
            vm.bookinginfo.data.response.travelerInfo.forEach(function (trveler) {
                var travelerType = '';
                travelerType = trveler.passengerType;
                var passenger = {
                    paxName: trveler.namePrefix + ' ' + trveler.givenName + ' ' + trveler.surname,
                    paxType: travelerType,
                    ticketNo: isNullorUndefind(trveler.airTicketNo) ? 'NA' : trveler.airTicketNo,
                }
                travellers.push(passenger);
            });
            vm.bookinginfo.data.response.flLegGroup.forEach(function (legGroup) {
                var stops = bookinginfo_vue.getNoOfStop(legGroup.segments);
                legGroup.segments.forEach(function (flight) {
                    var termnlFrom = flight.terminalFrom ? flight.terminalFrom : 'N/A';
                    var termnlTo = flight.terminalTo ? flight.terminalTo : 'N/A';
                    var sgmnts = {
                        fromCity: airportLocationFromAirportCode(flight.departureFrom),
                        toCity: airportLocationFromAirportCode(flight.departureTo),
                        departureDateHeader: bookinginfo_vue.moment(flight.departureDate).format('ddd, DD MMM YYYY'),
                        duration: vm.GetFlightdurationtime(flight.departureFrom, flight.departureTo, flight.departureDate, flight.arrivalDate),
                        airlineLogo: window.location.origin + '/Flights/assets/images/AirLines/' + flight.marketingCompany + '.gif',
                        airlineName: bookinginfo_vue.getAirLineName(flight.marketingCompany),
                        companyCode: flight.marketingCompany + ' - ' + flight.flightNumber,
                        departureDate: bookinginfo_vue.moment(flight.departureDate).format('DD MMM, HH:mm'),
                        fromAirport: vm.airportFromAirportCodeSimple(flight.departureFrom),
                        fromCityCode: flight.departureFrom,
                        terminalFrom: 'Terminal ' + termnlFrom,
                        arrivalDate: bookinginfo_vue.moment(flight.arrivalDate).format('DD MMM, HH:mm'),
                        toAirport: vm.airportFromAirportCodeSimple(flight.departureTo),
                        toCityCode: flight.departureTo,
                        terminalTo: 'Terminal ' + termnlTo,
                        cabinClass: bookinginfo_vue.getCabinName(flight.bookingClass),
                        stops: "Non stop"
                    }

                    flightDetails.push(sgmnts);
                    var cabinBaggage = '';
                    var checkinBaggage = '';
                    flight.airBagDetails.forEach(function (bagDetail) {

                        if (bagDetail.cabinBaggageQuantity != undefined) {

                            cabinBaggage = bagDetail.cabinBaggageQuantity.replace(".0", "") + ' ' + bagDetail.cabinBaggageUnit + '/Person';
                        }
                        if (bagDetail.checkinBaggageQuantity != undefined) {
                            var unit = '';
                            if ((bagDetail.checkinBaggageUnit).toLowerCase() == 'piece' ||
                                (bagDetail.checkinBaggageUnit).toLowerCase() == 'p' ||
                                (bagDetail.checkinBaggageUnit).toLowerCase() == 'n' ||
                                (bagDetail.checkinBaggageUnit).toLowerCase() == 'pc') {
                                unit = 'Piece'
                            } else if ((bagDetail.checkinBaggageUnit).toLowerCase() == 'k' ||
                                (bagDetail.checkinBaggageUnit).toLowerCase() == 'kg' ||
                                (bagDetail.checkinBaggageUnit).toLowerCase() == 'w' ||
                                (bagDetail.checkinBaggageUnit).toLowerCase() == '700') {

                                unit = 'KG';
                            }
                            checkinBaggage = bagDetail.checkinBaggageQuantity.replace(".0", "") + ' ' + unit + '/Person';
                        }
                    });
                    var bagDetail = {
                        cabin: cabinBaggage,
                        checkIn: checkinBaggage,
                        to: airportLocationFromAirportCode(flight.departureTo),
                        from: airportLocationFromAirportCode(flight.departureFrom)
                    }
                    baggageinfo.push(bagDetail);
                });
            });
            var totalAmount = bookinginfo_vue.$n((bookinginfo_vue.bookinginfo.data.response.totalFareGroup.sellAmount / bookinginfo_vue.CurrencyMultiplier), 'currency', bookinginfo_vue.selectedCurrency);
            if (vm.insuranceDetails) {
                totalAmount = bookinginfo_vue.$n((parseFloat(bookinginfo_vue.bookinginfo.data.response.totalFareGroup.sellAmount) + parseFloat(vm.insuranceDetails.totalFare)) / bookinginfo_vue.CurrencyMultiplier, 'currency', bookinginfo_vue.selectedCurrency);
            }
            var postData = {
                logoUrl: bookinginfo_vue.user.loginNode.logo,
                agencyPhone: isNullorUndefind(phoneNumber[0].nodeContactNumber.number) ? 'NA' : '+' + bookinginfo_vue.user.loginNode.country.telephonecode + "" + phoneNumber[0].nodeContactNumber.number,
                pnr: vm.bookinginfo.data.response.pnrNum ? vm.bookinginfo.data.response.pnrNum : "NA",
                bookFlightStatusImg: bookFlightStatusImg,
                leadPaxName: guestName,
                bookFlightStatus: vm.divBkngFail ? vm.cmscontent.Cancelled_message : vm.cmscontent.Confirmation_message,
                refNumber: vm.bookinginfo.data.response.bookingRefId,
                itinerary: flightDetails,
                paxDetails: travellers,
                baggage: baggageinfo,
                totalFare: bookinginfo_vue.$n((bookinginfo_vue.bookinginfo.data.response.totalFareGroup.totalBaseNet / bookinginfo_vue.CurrencyMultiplier), 'currency', bookinginfo_vue.selectedCurrency),
                totalTax: bookinginfo_vue.$n((bookinginfo_vue.bookinginfo.data.response.totalFareGroup.totalTaxNet / bookinginfo_vue.CurrencyMultiplier), 'currency', bookinginfo_vue.selectedCurrency),
                totalAmount: totalAmount,
                agencyAddress: isNullorUndefind(bookinginfo_vue.user.loginNode.address) ? 'NA' : bookinginfo_vue.user.loginNode.address,
                agencyContact: isNullorUndefind(phoneNumber[0].nodeContactNumber.number) ? 'NA' : '+' + bookinginfo_vue.user.loginNode.country.telephonecode + "" + phoneNumber[0].nodeContactNumber.number,
                agencyEmail: isNullorUndefind(bookinginfo_vue.user.loginNode.email) ? 'NA' : bookinginfo_vue.user.loginNode.email,
                insuranceTotal: vm.insuranceDetails ? bookinginfo_vue.$n(vm.insuranceDetails.totalFare / bookinginfo_vue.CurrencyMultiplier, 'currency', bookinginfo_vue.selectedCurrency) : undefined,
                insunraceBookRef: vm.insuranceDetails ? vm.insuranceDetails.success.policyNum : undefined
            };
            return vm.getTemplate("B2CFlightPDFTemplate").then(function (response) {
                var data = response.data.data;
                var pdfTemplateId = "";
                var pdfFileName = "";
                if (data.length > 0) {
                    for (var x = 0; x < data.length; x++) {
                        if (data[x].enabled == true && data[x].type == "B2CFlightPDFTemplate") {
                            pdfTemplateId = data[x].id;
                            pdfFileName = data[x].nodeCode;
                        }
                    }
                };
                var generatePdf = ServiceUrls.emailServices.generatePdf
                var pdfData = {
                    templateID: pdfTemplateId,
                    filename: pdfFileName,
                    content: postData
                };
                return axios.post(generatePdf, pdfData)
                    .then(function (response) {
                        if (button) {

                            vm.showSpinnerHtml = false;
                            vm.downloadPdf(response.data)
                        } else {
                            return response.data;
                        }
                    }).catch(function (error) {
                        if (button) {
                            vm.showSpinnerHtml = false;
                        } else {
                            return false;
                        }
                    })
            }).catch(function (error) {
                if (button) {
                    vm.showSpinnerHtml = false;
                } else {
                    return false;
                }
            });
        },
        downloadPdf: function (response) {
            try {
                var link = document.createElement('a');
                link.href = response.data;
                link.download = response.data.split('/').pop();
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
                this.processingPdf = false;
            } catch (error) {
                alertify.alert("Error", "Error in generating pdf file.").set('label', 'Ok');
                this.processingPdf = false;
            }
        },
        GetFlightdurationtime: function (FromGo, ToGo, DepTimeGo, ArriTimeGo) {
            return GetFlightdurationtime(FromGo, ToGo, DepTimeGo, ArriTimeGo);
        },
        airportFromAirportCodeSimple: function (airPortCode) {
            return airportFromAirportCodeSimple(airPortCode);
        },
        generateVoucher: function (printBody) {
            var flightDetails = '';
            var travellerDetails = '<div style="width:100%;float:left;background:#efefef;border-bottom:1px solid #ddd;display:flex">' +
                '<div style="width:57%;float:left;padding:1%;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#333;text-align:left;font-weight:600;border-right:1px solid #ddd">Passenger Names</div>' +
                '<div style="width:18%;float:left;padding:1%;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#333;text-align:left;font-weight:600;border-right:1px solid #ddd">Type</div>' +
                '<div style="width:18%;float:left;padding:1%;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#333;text-align:left;font-weight:600">Ticket No.</div>' +
                '</div>';
            var baggageDetails = '';
            var bookInfo = this.bookinginfo.data.response;
            bookInfo.flLegGroup.forEach(function (legGroup) {
                var stops = bookinginfo_vue.getNoOfStop(legGroup.segments);
                legGroup.segments.forEach(function (flight) {
                    var termnlFrom = flight.terminalFrom ? flight.terminalFrom : 'N/A';
                    var termnlTo = flight.terminalTo ? flight.terminalTo : 'N/A';
                    flightDetails += '<div style="width:100%;float:left">' +
                        '<div style="width:100%;float:left;background:#efefef;margin-bottom:5px">' +
                        '<div style="width:50%;float:left;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:18px;color:#464646;text-align:left;line-height:25px;border-right:1px solid #b2b2b2">' +
                        '<img src="https://s3.ap-south-1.amazonaws.com/images.oneviewitsolutions.com/Email-Template/images/flight.png" style="padding:3px;float:left" class="CToWUd">' +
                        airportLocationFromAirportCode(flight.departureFrom) + ' To ' + airportLocationFromAirportCode(flight.departureTo) +
                        '</div>' +
                        '<div style="width:24%;float:left;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:13px;color:#464646;text-align:left;line-height:25px;border-right:1px solid #b2b2b2">' +
                        '<img src="https://s3.ap-south-1.amazonaws.com/images.oneviewitsolutions.com/Email-Template/images/calendar.png" style="padding:3px;float:left" class="CToWUd">' +
                        bookinginfo_vue.moment(flight.departureDate).format('ddd, DD MMM YYYY') +
                        '</div>' +
                        '<div style="width:25%;float:left;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:13px;color:#464646;text-align:left;line-height:25px">' +
                        stops +
                        '</div>' +
                        '</div>' +
                        '<div style="width:100%;float:left;padding:10px 0px">' +
                        '<div style="width:25%;float:left">' +
                        '<div style="width:20%;float:left;padding-left:9%"><img src="/Flights/assets/images/AirLines/' + flight.marketingCompany + '.gif" style="width:100%" class="CToWUd"></div>' +
                        '<div style="width:67%;float:left;padding-left:3%">' +
                        '<div style="width:100%;float:left;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#464646;text-align:left">' + bookinginfo_vue.getAirLineName(flight.marketingCompany) + '</div>' +
                        '<div style="width:100%;float:left;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:13px;color:#464646;text-align:left">' + flight.marketingCompany + ' - ' + flight.flightNumber + '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div style="width:52%;float:left;padding:0 2%">' +
                        '<div style="width:25%;float:left">' +
                        '<div style="width:100%;float:left;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#000;text-align:left;font-weight:600">' + bookinginfo_vue.moment(flight.departureDate).format('DD MMM, HH:mm') + '</div>' +
                        '<div style="width:100%;float:left;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:13px;color:#3a7fc1;text-align:left;font-weight:600">' +
                        airportLocationFromAirportCode(flight.departureFrom) +
                        '<div style="width:100%;font-weight:normal;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:12px;color:#8e8e8e">' + flight.departureFrom + '</div>' +
                        '<div style="width:100%;font-weight:normal;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:12px;color:#8e8e8e">' + 'Terminal ' + termnlFrom + '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div style="width:42%;float:left;padding:0 4%">' +
                        '<div style="width:100%;float:left"><img src="https://s3.ap-south-1.amazonaws.com/images.oneviewitsolutions.com/Email-Template/images/flight-arrow.png" style="width:100%" class="CToWUd"></div>' +
                        '</div>' +
                        '<div style="width:25%;float:left">' +
                        '<div style="width:100%;float:left;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#000;text-align:left;font-weight:600">' + bookinginfo_vue.moment(flight.arrivalDate).format('DD MMM, HH:mm') + '</div>' +
                        '<div style="width:100%;float:left;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:13px;color:#3a7fc1;text-align:left;font-weight:600">' +
                        airportLocationFromAirportCode(flight.departureTo) +
                        '<div style="width:100%;font-weight:normal;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:12px;color:#8e8e8e">' + flight.departureTo + '</div>'
                        // + '<div style="width:100%;font-weight:normal;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:12px;color:#8e8e8e">' + 'Terminal ' + legGroup.segments[0].terminalTo + '</div>'
                        +
                        '<div style="width:100%;font-weight:normal;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:12px;color:#8e8e8e">' + 'Terminal ' + termnlTo + '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div style="width:15%;float:left;border-left:1px dashed #cccccc;padding-left:2%">' +
                        '<div style="width:100%;float:left;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:13px;color:#444;text-align:left;font-weight:600"></div>' +
                        '<div style="width:100%;float:left;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:13px;color:#444;text-align:left;font-weight:600">' + bookinginfo_vue.getCabinName(flight.bookingClass) + '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';

                    var cabinBaggage = '';
                    var checkinBaggage = '';
                    flight.airBagDetails.forEach(function (bagDetail) {

                        if (bagDetail.cabinBaggageQuantity != undefined) {

                            cabinBaggage = bagDetail.cabinBaggageQuantity.replace(".0", "") + ' ' + bagDetail.cabinBaggageUnit + '/Person';
                        }
                        if (bagDetail.checkinBaggageQuantity != undefined) {
                            var unit = '';
                            if ((bagDetail.checkinBaggageUnit).toLowerCase() == 'piece' ||
                                (bagDetail.checkinBaggageUnit).toLowerCase() == 'p' ||
                                (bagDetail.checkinBaggageUnit).toLowerCase() == 'n' ||
                                (bagDetail.checkinBaggageUnit).toLowerCase() == 'pc') {
                                unit = 'Piece'
                            } else if ((bagDetail.checkinBaggageUnit).toLowerCase() == 'k' ||
                                (bagDetail.checkinBaggageUnit).toLowerCase() == 'kg' ||
                                (bagDetail.checkinBaggageUnit).toLowerCase() == 'w' ||
                                (bagDetail.checkinBaggageUnit).toLowerCase() == '700') {

                                unit = 'KG';
                            }
                            checkinBaggage = bagDetail.checkinBaggageQuantity.replace(".0", "") + ' ' + unit + '/Person';
                        }

                    });
                    baggageDetails += '<div style="width:100%;float:left;background:#efefef;border-bottom:1px solid #ddd;display:flex">' +
                        '<div style="width:98%;float:left;padding:1%;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#333;text-align:left;font-weight:600">Flight From ' + airportLocationFromAirportCode(flight.departureFrom) + ' To ' + airportLocationFromAirportCode(flight.departureTo) + '</div>' +
                        '</div>' +
                        '<div style="width:100%;float:left;border-bottom:1px dashed #ddd;display:flex">' +
                        '<div style="width:33%;float:left;padding:1%;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#333;text-align:left;font-weight:300">Cabin Baggage</div>' +
                        '<div style="width:33%;float:left;padding:1%;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#333;text-align:center;font-weight:300">' + cabinBaggage + '</div>' +
                        '<div style="width:33%;float:left;padding:1%;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#333;text-align:right;font-weight:300">Free</div>' +
                        '</div>' +
                        '<div style="width:100%;float:left;display:flex">' +
                        '<div style="width:33%;float:left;padding:1%;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#333;text-align:left;font-weight:300">Checked Baggage</div>' +
                        '<div style="width:33%;float:left;padding:1%;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#333;text-align:center;font-weight:300">' + checkinBaggage + '</div>' +
                        '<div style="width:33%;float:left;padding:1%;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#333;text-align:right;font-weight:300">Free</div>' +
                        '</div>';

                });
            });
            bookInfo.travelerInfo.forEach(function (trveler) {
                var airTktNo = isNullorUndefind(trveler.airTicketNo) ? 'NA' : trveler.airTicketNo;
                travellerDetails += '<div style="width:100%;float:left;border-bottom:1px solid #ddd;display:flex">' +
                    '<div style="width:57%;float:left;padding:1%;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#333;text-align:left;font-weight:300;border-right:1px solid #ddd">' + trveler.namePrefix + ' ' + trveler.givenName + ' ' + trveler.surname + '</div>' +
                    '<div style="width:18%;float:left;padding:1%;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#333;text-align:left;font-weight:300;border-right:1px solid #ddd">' + trveler.passengerType + '</div>' +
                    '<div style="width:18%;float:left;padding:1%;font-family:' + 'Gill Sans' + ',' + 'Gill Sans MT' + ',' + 'Calibri,' + 'Trebuchet MS' + ',sans-serif;font-size:14px;color:#333;text-align:left;font-weight:300">' + airTktNo + '</div>' +
                    '</div>';
            });
            var logoimg = '<img src="' + "/" + localStorage.AgencyFolderName + "/website-informations/logo/logo.png" + '"></img>';
            var successLogo = '<img src="../assets/images/con-flight.png" class="CToWUd"></img>';
            if (bookInfo.bookingStatus == "XX" ||
                this.bookinginfo.data.response.bookingStatus == "RF" ||
                this.bookinginfo.data.response.bookingStatus == "TF") {
                successLogo = '<img src="../assets/images/con-flight-error.png" class="CToWUd"></img>';
            }
            printBody = printBody.replace('#logo#', logoimg);
            printBody = printBody.replace('#flightSuccesslogo#', successLogo);
            printBody = printBody.replace('#Phone#', $('#phone').html());
            printBody = printBody.replace('#PNR#', $('#pnrNum').html());
            printBody = printBody.replace('#UserName#', $('#userName').html());
            printBody = printBody.replace('#BookorCancelMessage#', $('#bookorCancelMessage').html());
            printBody = printBody.replace('#BkngRefNumber#', 'Booking Reference Number - ' + $('#bkngRefNumber').html());
            printBody = printBody.replace('#FlightDetails#', flightDetails);
            printBody = printBody.replace('#TravellerDetails#', travellerDetails);
            printBody = printBody.replace('#BaggageDetails#', baggageDetails);
            printBody = printBody.replace('#paxCount#', bookInfo.adt + ' Adult ' + bookInfo.chd + ' Child ' + bookInfo.inf + ' Infant');
            printBody = printBody.replace('#Fare#', $('#fare').html());
            printBody = printBody.replace('#TaxesandFees#', $('#taxesandFees').html());
            printBody = printBody.replace('#Total#', $('#total').html());
            printBody = printBody.replace('#Address#', $('#address').html());
            printBody = printBody.replace('#Phone#', $('#phoneNo').html());
            printBody = printBody.replace('#Email#', $('#email').html());
            return printBody;
        },
        moment: function (date) {
            return moment(date);
        },
        getCabinName: function (cabin) {
            return getCabinName(cabin);
        },
        getAirLineName: function (airlinecode) {
            return getAirLineName(airlinecode);
        },
        airportLocationFromAirportCode: function (airlinecode) {
            return airportLocationFromAirportCode(airlinecode);
        },
        airportFromAirportCode: function (airlinecode) {
            return airportFromAirportCode(airlinecode);
        },
        getNoOfStop: function (segment) {
            return ((segment.length - 1 > 0) ? (segment.length - 1) + ' stop' : 'Nonstop');
        },
        getDuration: function (deptdate, arrdate) {
            var ms = moment(deptdate).diff(moment(arrdate));
            // var s=Math.abs((deptdate.getTime() - arrdate.getTime()) / 3600000);
            var d = moment.duration(ms);
            var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
            return s;
        },
        getBagDetails: function (airBagDetails, baginfo) {
            var res = '';
            var bag = [];
            var unit = '';
            var paxtype = [];
            var tooltip = '';
            if (airBagDetails.length > 0) {
                airBagDetails.forEach(function (bagDet) {
                    if (baginfo == 'cabin') {
                        if (bagDet.cabinBaggageQuantity != undefined) {
                            bag.push(bagDet.cabinBaggageQuantity.replace(".0", ""));
                            unit = bagDet.cabinBaggageUnit;
                            paxtype.push(bagDet.paxType);
                        }
                    } else {
                        if (bagDet.checkinBaggageQuantity != undefined) {
                            if ((bagDet.checkinBaggageUnit).toLowerCase() == 'piece' || (bagDet.checkinBaggageUnit).toLowerCase() == 'p' || (bagDet.checkinBaggageUnit).toLowerCase() == 'n' || (bagDet.checkinBaggageUnit).toLowerCase() == 'pc') {
                                tooltip = 'Airline typically permit 23kg baggage weight per piece';
                                unit = 'Piece'
                            } else if ((bagDet.checkinBaggageUnit).toLowerCase() == 'k' ||
                                (bagDet.checkinBaggageUnit).toLowerCase() == 'kg' ||
                                (bagDet.checkinBaggageUnit).toLowerCase() == 'w' ||
                                (bagDet.checkinBaggageUnit).toLowerCase() == '700') {
                                // unit = bagDet.checkinBaggageUnit;
                                unit = 'KG';
                            }
                            bag.push(bagDet.checkinBaggageQuantity.replace(".0", ""));
                            paxtype.push(bagDet.paxType);
                        }
                    }
                })

                try {
                    if (bag.length > 1) {
                        if (bag.length > 2) {

                            if (bag[0] == bag[1] && bag[0] == bag[2]) {

                                res = '<div v-b-tooltip.hover="" title="' + tooltip + '">' + bag[0] + unit + '/Person' + '</div>';

                            } else {
                                res = '<div v-b-tooltip.hover="" title="' + tooltip + '">' + bag[0] + unit + '(' + paxtype[0] + ')' + '</div>' +
                                    '<div v-b-tooltip.hover="" title="' + tooltip + '">' + bag[1] + unit + '(' + paxtype[1] + ')' + '</div>' +
                                    '<div v-b-tooltip.hover="" title="' + tooltip + '">' + bag[2] + unit + '(' + paxtype[2] + ')' + '</div>'
                            }
                        } else {
                            if (bag[0] == bag[1]) {
                                res = '<div v-b-tooltip.hover="" title="' + tooltip + '">' + bag[0] + unit + '/Person' + '</div>';
                            } else {
                                res = '<div v-b-tooltip.hover="" title="' + tooltip + '">' + bag[0] + unit + '(' + paxtype[0] + ')' + '</div>' +
                                    '<div v-b-tooltip.hover="" title="' + tooltip + '">' + bag[1] + unit + '(' + paxtype[1] + ')' + '</div>'
                            }
                        }

                    } else {

                        res = '<div v-b-tooltip.hover="" title="' + tooltip + '">' + bag[0] + unit + '/Person' + '</div>';
                        //res = bag[0] + unit + '/Person' + tooltip;

                    }
                } catch (err) {}

            }

            return res;
        },
        getAirportNameWithTooltip: function (airlinecode) {
            return '<span v-b-tooltip.hover="" title="' + airportFromAirportCode(airlinecode) + '">' + airportLocationFromAirportCode(airlinecode) + '</span>';
        },
        getAirportCodeWithTooltip: function (airlinecode) {
            return '<span v-b-tooltip.hover="" title="' + airportFromAirportCode(airlinecode) + '">' + airlinecode + '</span>';
        },
        getTripName: function (tripData) {
            var tripname = ''
            if (tripData.flLegGroup != null && tripData.flLegGroup != undefined) {
                tripData.flLegGroup.forEach(function (fiLegGroup) {
                    tripname += ' - ' + airportLocationFromAirportCode(fiLegGroup.from) + ' to ' +
                        airportLocationFromAirportCode(fiLegGroup.to);
                });
            }
            return tripname;
        },
        getpagecontent: function () {

            var self = this;
            getAgencycode(function (response) {
                var Agencycode = 'AGY435';
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Flight+Confirmation/Flight+Confirmation/Flight+Confirmation.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    self.content = response.data;
                    if (response.data.area_List.length > 0) {
                        var mainComp = response.data.area_List[0].main;
                        self.cmscontent.Confirmation_message = self.pluckcom('Confirmation_message', mainComp.component);
                        self.cmscontent.Cancelled_message = self.pluckcom('Cancelled_message', mainComp.component);
                        self.cmscontent.Book_reference_no = self.pluckcom('Book_reference_no', mainComp.component);
                        self.cmscontent.Pnr_label = self.pluckcom('Pnr_label', mainComp.component);
                        self.cmscontent.Ticket_mail_message = self.pluckcom('Ticket_mail_message', mainComp.component);
                        self.cmscontent.Flight_details_label = self.pluckcom('Flight_details_label', mainComp.component);
                        self.cmscontent.Flight_to_label = self.pluckcom('Flight_to_label', mainComp.component);
                        self.cmscontent.Traveler_detail_label = self.pluckcom('Traveler_detail_label', mainComp.component);
                        self.cmscontent.Passenger_name = self.pluckcom('Passenger_name', mainComp.component);
                        self.cmscontent.Type = self.pluckcom('Type', mainComp.component);
                        self.cmscontent.Ticket_no_label = self.pluckcom('Ticket_no_label', mainComp.component);
                        self.cmscontent.Baggage_detail_label = self.pluckcom('Baggage_detail_label', mainComp.component);
                        self.cmscontent.Flight_from = self.pluckcom('Flight_from', mainComp.component);
                        self.cmscontent.Cabin_baggage_label = self.pluckcom('Cabin_baggage_label', mainComp.component);
                        self.cmscontent.Free_label = self.pluckcom('Free_label', mainComp.component);
                        self.cmscontent.Checked_baggage_label = self.pluckcom('Checked_baggage_label', mainComp.component);
                        self.cmscontent.Payment_details_label = self.pluckcom('Payment_details_label', mainComp.component);
                        self.cmscontent.Adult_label = self.pluckcom('Adult_label', mainComp.component);
                        self.cmscontent.Child_label = self.pluckcom('Child_label', mainComp.component);
                        self.cmscontent.Infant_label = self.pluckcom('Infant_label', mainComp.component);
                        self.cmscontent.Fare_label = self.pluckcom('Fare_label', mainComp.component);
                        self.cmscontent.Taxes_and_Fees_label = self.pluckcom('Taxes_and_Fees_label', mainComp.component);
                        self.cmscontent.Total_label = self.pluckcom('Total_label', mainComp.component);
                        self.cmscontent.Customer_Support_label = self.pluckcom('Customer_Support_label', mainComp.component);
                        self.cmscontent.Address_label = self.pluckcom('Address_label', mainComp.component);
                        self.cmscontent.Phone_label = self.pluckcom('Phone_label', mainComp.component);
                        self.cmscontent.Email_label = self.pluckcom('Email_label', mainComp.component);
                        self.cmscontent.PrintVoucher_Label = self.pluckcom('PrintVoucher_Label', mainComp.component);
                        self.cmscontent.DownloadVoucher_Label = self.pluckcom('DownloadVoucher_Label', mainComp.component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                    this.content = [];
                });

            });
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);

                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        }
    },
    computed: {
        paymentStatus: function () {
            var status = "Pending";
            if (this.bookinginfo.data.response.paymentStatus == true) {
                status = "Success";
            } else if (this.bookinginfo.data.response.paymentStatus == false) {
                status = "Failed";
            }
            return status;
        }
    }
});