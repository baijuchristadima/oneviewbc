﻿var cResults = httpVueLoader("/Flights/components/templates/flightresults.vue");
var cTravellerDetails = httpVueLoader("/Flights/components/templates/travellerdetails.vue");
var cBookingInfo = httpVueLoader("/Flights/components/templates/bookinginfo.vue");

const routes = [
    { path: "/", component: cResults },
    { path: "/Results", component: cResults },
    { path: "/TravellerDetails", component: cTravellerDetails },
    { path: "/BookingInfo", component: cBookingInfo }
];

const router = new VueRouter({
    routes // short for 'routes: routes'
});

var vm = new Vue({
    el: "#el_flightDetail",
    router,
    data: {
        headerFooterVisible: true,
        result: null
    }
});