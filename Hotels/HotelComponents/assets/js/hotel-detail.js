module.exports = {
  data: function () {
    return {
      hotelDetails: {},
      roomDetails: {},
      bundledResult: [],
      optionalResult: [],
      selectedRooms: [],
      bookingDetails: {
        cInDate: {
          fulldate: null
        },
        cOutDate: {
          fulldate: null
        },
        numberOfNights: 0,
        childCount: 0,
        adultCount: 0,
        bookingPaxDetails: "",
        roomSummaryDetails: []
      },
      imageCarousel: [{
        url: "",
        caption: ""
      }],
      hotelAmenities: [],
      roomAmenities: [],
      filteredroomAmenities: [],
      filteredhotelAmenities: [],
      hotelDescription: "",
      minimumHotelPrice: 0,
      tweenedMinimumHotelPrice: 0,
      bookNowIsClicked: false,
      sharedStateHotel: store.state,
      prebookRQData: null,
      roomRQData: null,
      hDetailRQData: null,
      roomRQCount: 0,
      hDetailRQCount: 0,
      prebookRQCount: 0,
      hideHotelAmenities: true,
      hideRoomAmenities: true,
      SelectRoomButton_Label: "",
      AdultCount_Label: "",
      ChildCount_Label: "",
      ChildrenCount_Label: "",
      NightsCount_Label: "",
      NoMapAvailable_Label: "",
      ChildrenAgesSearch_Label: "",
      ViewCancellationPolicy_Label: "",
      BookNow_Label: "",
      HotelAmenities_Label: "",
      RoomAmenities_Label: "",
      AllAmenitiesLink_Label: "",
      CancellationPolicyHeader_Label: "",
      CancellationNotes_Label: "",
      Rooms_Label: "",
      AboutTheHotel_Label: "",
      LoadingCancellation_Label: "",
      CancellationTextA1_Label: "",
      CancellationTextA2_Label: "",
      CancellationTextA3_Label: "",
      CancellationTextB1_Label: "",
      CancellationTextC1_Label: "",
      CancellationTextC2_Label: "",
      GeneralDescription_Label: "",
      CancellationWarning_Message: "",
      WarningNotes_Message: "",
      UnableToProceedError_Message: "",
      ProblemWithRoomSelection_Message: "",
      ErrorPopUp_Label: "",
      PleaseNotePopUp_Label: "",
      SelectButton_Label: "",
      SelectedButton_Label: "",
      showMoreRooms_Label: "",
      showLessRooms_Label: "",
      ConfirmationHeader_Label: "",
      PaymentHeader_Label: "",
      TravellerDetailsHeader_Label: "",
      ReviewHotelHeader_Label: "",
      currentLanguage: localStorage.direction,
      bookNowIsClickedIndex:null,
      suppliersWithHotelWiseCancellation: globalConfigs.services.hotels.suppliersWithHotelWiseCancellation,
      stickyzIndexCount: 99999

    };
  },
  watch: {
    minimumHotelPrice: function () {
      TweenLite.to(this.$data, 0.5, {
        tweenedMinimumHotelPrice: this.minimumHotelPrice
      });

    }
  },
  computed: {
    getMinimumHotelPrice: function () {

      return this.tweenedMinimumHotelPrice.toFixed(2);
    }
  },
  methods: {
    unescapeHtml: function(unsafe) {
      return unsafe
        .replace(/&amp;/g, "&")
        .replace(/&lt;/g, "<")
        .replace(/&gt;/g, ">")
        .replace(/&quot;/g, "\"")
        .replace(/&#039;/g, "'");
    },
    imgUrlAlt(event) {

        event.target.src = "/assets/images/no-image.png";
    },
    imgUrlAltList(event) {
      event.target.src = "/assets/images/no-image.png"
    },
    scrollFix: function (hash) {
      setTimeout(() =>
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 1000), 1)
    },
    selectRoom: function (searchRoomIndex, optionalRoomId) {
      var rooms = this.optionalResult[searchRoomIndex - 1].rooms;

      for (var index = 0; index < rooms.length; index++) {
        if (rooms[index].isSelected == true) {
          rooms[index].isSelected = false;
        }
        if (rooms[index].optionalRoomId == optionalRoomId) {
          rooms[index].isSelected = true;
        }
      }

      if (this.optionalResult.length != 1) {
        this.minimumHotelPrice = Number.POSITIVE_INFINITY;
      }
    },
    bookNowForSingleOptionalRoom: function (index, searchRoomIndex, optionalRoomId){
      this.selectRoom(searchRoomIndex, optionalRoomId);
      this.bookNowIsClickedIndex = index;
      this.bookNowIsClicked = true;
      this.bookNow(null, true);

    },
    bookNow: function (bundleId, isOptional) {
      this.bookNowIsClicked = true;

      if (isOptional) {
        var selectedRooms = [];
        for (var searchRoomIndex = 0; searchRoomIndex < this.optionalResult.length; searchRoomIndex++) {
          var rooms = _.cloneDeep(this.optionalResult[searchRoomIndex].rooms);
          for (var roomIndex = 0; roomIndex < rooms.length; roomIndex++) {
            if (rooms[roomIndex].isSelected) {
              rooms[roomIndex].roomId = this.optionalResult[searchRoomIndex].searchRoomIndex;
              rooms[roomIndex].guests = this.hotelDetails.content.supplierSpecific.criteria.rooms[this.optionalResult[searchRoomIndex].searchRoomIndex - 1].guests

              selectedRooms.push({
                searchRoomIndex: this.optionalResult[searchRoomIndex].searchRoomIndex,
                room: rooms[roomIndex]
              });
              break;
            }
          }
        }
        this.selectedRooms = selectedRooms;
      } else {
        var selectedRooms = [];

        for (var searchRoomIndex = 0; searchRoomIndex < this.bundledResult.length; searchRoomIndex++) {
          var rooms = _.cloneDeep(this.bundledResult[searchRoomIndex].details);
          if (this.bundledResult[searchRoomIndex].bundleId == bundleId) {
            for (var roomIndex = 0; roomIndex < rooms.length; roomIndex++) {
              rooms[roomIndex].room.roomId = rooms[roomIndex].searchRoomIndex;
              rooms[roomIndex].room.guests = this.hotelDetails.content.supplierSpecific.criteria.rooms[rooms[roomIndex].searchRoomIndex - 1].guests
              selectedRooms.push(_.cloneDeep(rooms[roomIndex]));
            }
            break;
          }
        }
        this.selectedRooms = selectedRooms;

      }
      //Update global store
      store.updateState("selectedRooms", this.selectedRooms);

      this.$router.push('/hotelBooking');
    },
    getCancellationPolicy: function (room, prebookRoomId, UiRoomId, isOptional) {
      var labels = {
        CancellationTextA1_Label: this.CancellationTextA1_Label,
        CancellationTextA2_Label: this.CancellationTextA2_Label,
        CancellationTextA3_Label: this.CancellationTextA3_Label,
        CancellationTextB1_Label: this.CancellationTextB1_Label,
        CancellationTextC1_Label: this.CancellationTextC1_Label,
        CancellationTextC2_Label: this.CancellationTextC2_Label

      }
     
      if ((room.rates[0].cancellationPoliciesParsed.length == 0 && room.rates[0].isLoading == true) || room.rates[0].language != localStorage.direction) {

        // var supplierCode = this.hotelDetails.supplierCode;
        if (room.rates[0].isLoading == true) {
          //Check if supplier needs to send prebook request
          if (room.rates[0].cancellationPolicies == undefined) {
            var rooms = [];
            rooms.push({
              name: room.roomName,
              roomId: prebookRoomId,
              paxes: [], // from pax page?
              guests: this.hotelDetails.content.supplierSpecific.criteria.rooms[prebookRoomId - 1].guests, //from search 
              rate: {
                bookable: true,
                cancellationPolicies: room.rates[0].cancellationPolicies,
                rateSpecific: room.rates[0].roomRateSpecific,
                pricing: {
                  perNightPrice: room.rates[0].pricing.perNightPrice,
                  totalPrice: room.rates[0].pricing.totalPrice
                },
                roomRateReferences: room.rates[0].roomRateSpecific ? room.rates[0].roomRateSpecific.roomRateReferences : undefined
              },
              roomReferences: room.roomSpecific ? room.roomSpecific.roomReferences : undefined

            });
            var PrebookRQ = {
              service: "HotelRQ",
              node: this.hotelDetails.node,
              token: this.hotelDetails.token,
              hubUuid: this.hotelDetails.hubUuid,
              supplierCodes: ["" + this.hotelDetails.supplierCode],
              credentials: [],
              content: {
                command: "PrebookRQ",
                hotel: {
                  cityCode: this.hotelDetails.content.searchResponse.hotels[0].location.cityCode,
                  hotelCode: this.hotelDetails.content.searchResponse.hotels[0].hotelCode,
                  totalAmount: room.rates[0].pricing.totalPrice.price,
                  sealCode: room.rates[0].pricing.totalPrice.sealCode,
                  nationality: this.hotelDetails.content.searchResponse.nationality,
                  residentOf: this.hotelDetails.content.searchResponse.residentOf,
                  rooms: rooms,
                  hotelSpecific: this.hotelDetails.content.searchResponse.hotels[0].hotelSpecific
                },
                paxes: [], // from pax page?
                stay: this.hotelDetails.content.supplierSpecific.criteria.stay,
                supplierSpecific: {
                  uuid: localStorage.getItem("hotelUUID"), // from session storage
                  requestType: "cancellationPolicy",
                  searchReferences: this.hotelDetails.content.searchResponse.supplierSpecific ? this.hotelDetails.content.searchResponse.supplierSpecific.searchReferences : undefined,
                  searchHotelReferences: this.hotelDetails.content.searchResponse.hotels[0].hotelSpecific ? this.hotelDetails.content.searchResponse.hotels[0].hotelSpecific.searchHotelReferences : undefined,
                  roomsReferences: this.roomDetails.roomSpecific ? this.roomDetails.roomSpecific.roomReferences : undefined
                }
              }
            }
            this.roomRQData = {
              request: PrebookRQ,
              roomId: UiRoomId,
              isOptional: isOptional,
              fromBooking: false,
              callback: null

            };
            this.sendPrebook({
              request: PrebookRQ,
              roomId: UiRoomId,
              isOptional: isOptional,
              fromBooking: false,
              callback: null

            });

          } else {
            if (room.rates[0].cancellationPoliciesParsed.length == 0) {
              room.rates[0].cancellationPoliciesParsed = ParseCancellationPolicyWithI18n(room.rates[0].cancellationPolicies, room.rates[0].pricing.totalPrice.price, labels).cancellation;
            }
          }
        }
        room.rates[0].isLoading = false;

      } else {
        try {
          if (room.rates[0].cancellationPoliciesParsed.length == 0) {
            room.rates[0].cancellationPoliciesParsed = ParseCancellationPolicyWithI18n(room.rates[0].cancellationPolicies, room.rates[0].pricing.totalPrice.price, labels).cancellation;
          }
        } catch (error) {
          //is cancellation sent
        }

      }
    },
    sendPrebook: function (data) {
      var labels = {
        CancellationTextA1_Label: this.CancellationTextA1_Label,
        CancellationTextA2_Label: this.CancellationTextA2_Label,
        CancellationTextA3_Label: this.CancellationTextA3_Label,
        CancellationTextB1_Label: this.CancellationTextB1_Label,
        CancellationTextC1_Label: this.CancellationTextC1_Label,
        CancellationTextC2_Label: this.CancellationTextC2_Label

      }
      this.prebookRQCount++;
      var vm = this;

      var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;

      axios({
        method: "post",
        url: hubUrl + "/preBook",
        data: {
          request: data.request
        },
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.access_token
        }
      }).then(response => {
        console.log(response.data);
        var roomIsBookable = true;
        var showCancellationMessage = false;
       
        var hotelDetails = JSON.parse(window.sessionStorage.getItem("hotelInfo"));

          if (response.data.response.content.prebookResponse.bookable) {
            var hotel = response.data.response.content.prebookResponse.hotel;
            //Check each room here
           
            if (data.fromBooking) {
              for (var roomIndex = 0; roomIndex < hotel.rooms.length; roomIndex++) {
                var room = hotel.rooms[roomIndex];
                if (typeof room.rate.bookable != "undefined" && !room.rate.bookable) {
                  roomIsBookable = false;
                  vm.bookNowIsClicked = false;
                }
              }
              if (roomIsBookable) {

                for (var roomIndex = 0; roomIndex < vm.selectedRooms.length; roomIndex++) {
                  var room = vm.selectedRooms[roomIndex].room || vm.selectedRooms[roomIndex];

                  // Check for room wise or hotel wise cancellation
                  if (hotel.hotelCancellationPolicies != undefined && hotel.hotelCancellationPolicies.length > 0) {

                    var getCancellation = ParseCancellationPolicy(hotel.hotelCancellationPolicies, response.data.response.content.prebookResponse.currentPrice);

                    room.rates[0].cancellationPolicies = hotel.hotelCancellationPolicies;
                    room.rates[0].isCharged = getCancellation.isCharged;
                    room.rates[0].cancellationPoliciesParsed = getCancellation.cancellation;

                    room.isPrebookSent = true;
                  } else {
                    var prebookRooms = hotel.rooms;
                    for (var prebookRoomIndex = 0; prebookRoomIndex < prebookRooms.length; prebookRoomIndex++) {
                      var prebookRoom = prebookRooms[prebookRoomIndex];
                      if (room.roomId == prebookRoom.roomId) {


                        var getCancellation = ParseCancellationPolicy(prebookRoom.rate.cancellationPolicies, prebookRoom.rate.pricing.totalPrice.price);

                        room.rates[0].cancellationPolicies = prebookRoom.rate.cancellationPolicies;
                        room.rates[0].isCharged = getCancellation.isCharged;
                        room.rates[0].cancellationPoliciesParsed = getCancellation.cancellation;

                        room.isPrebookSent = true;
                        break;
                      }
                    }
                  }

                  if (room.rates[0].isCharged) {
                    showCancellationMessage = true;
                  }
                }
                //Update global store
                store.updateState("prebookCancellationRS", response.data);
              } else {
                alertify.alert(this.ErrorPopUp_Label, this.UnableToProceedError_Message)
                vm.bookNowIsClicked = false;

              }
              if (localStorage.direction == 'rtl') {
                $(".ajs-modal").addClass("ar_direction1");
              } else {
                $(".ajs-modal").removeClass("ar_direction1");
              }
            } else {
              if (data.isOptional) {
               
                for (var resultIndex = 0; resultIndex < vm.optionalResult.length; resultIndex++) {
                  for (var roomIndex = 0; roomIndex < vm.optionalResult[resultIndex].rooms.length; roomIndex++) {
                    var room = vm.optionalResult[resultIndex].rooms[roomIndex];


                    if (room.optionalRoomId == data.roomId) {

                      // Check for room wise or hotel wise cancellation

                      if (hotel.hotelCancellationPolicies != undefined && hotel.hotelCancellationPolicies.length > 0) {
                        var getCancellation = ParseCancellationPolicyWithI18n(hotel.hotelCancellationPolicies, response.data.response.content.prebookResponse.currentPrice, labels);

                        room.rates[0].cancellationPolicies = hotel.hotelCancellationPolicies;
                        room.rates[0].isCharged = getCancellation.isCharged;
                        room.rates[0].cancellationPoliciesParsed = getCancellation.cancellation;

                      } else {
                        var getCancellation = ParseCancellationPolicyWithI18n(hotel.rooms[0].rate.cancellationPolicies, hotel.rooms[0].rate.pricing.totalPrice.price, labels);

                        room.rates[0].cancellationPolicies = hotel.rooms[0].rate.cancellationPolicies;
                        room.rates[0].isCharged = getCancellation.isCharged;
                        room.rates[0].cancellationPoliciesParsed = getCancellation.cancellation;

                      }
                      room.rates[0].isLoading = true;
                      room.rates[0].language = localStorage.direction;

                      room.isPrebookSent = true;
                      break;
                    }
                  }
                }
              } else {
                for (var bundleIndex = 0; bundleIndex < vm.bundledResult.length; bundleIndex++) {
                  for (var roomIndex = 0; roomIndex < vm.bundledResult[bundleIndex].details.length; roomIndex++) {

                    var room = vm.bundledResult[bundleIndex].details[roomIndex].room;
                    if (room.bundledRoomId == data.roomId) {

                      if (hotel.hotelCancellationPolicies.length > 0) {
                        var getCancellation = ParseCancellationPolicyWithI18n(hotel.hotelCancellationPolicies, hotel.currentPrice, labels);

                        room.rates[0].cancellationPolicies = hotel.hotelCancellationPolicies;
                        room.rates[0].isCharged = getCancellation.isCharged;
                        room.rates[0].cancellationPoliciesParsed = getCancellation.cancellation;

                      } else {
                        var getCancellation = ParseCancellationPolicyWithI18n(hotel.rooms[0].rate.cancellationPolicies, hotel.rooms[0].rate.pricing.totalPrice.price, labels);

                        room.rates[0].cancellationPolicies = hotel.rooms[0].rate.cancellationPolicies;
                        room.rates[0].isCharged = getCancellation.isCharged;
                        room.rates[0].cancellationPoliciesParsed = getCancellation.cancellation;
                      }
                      room.isPrebookSent = true;
                      room.rates[0].isLoading = true;
                      room.rates[0].language = localStorage.direction;

                      break;

                    }
                  }
                }
              }
            }

            if (roomIsBookable) {

              if (data.callback) {

                var session = JSON.parse(window.sessionStorage.getItem("hotelInfo"));
                session.content.searchResponse.hotels[0].roomResponse = {};
                session.content.searchResponse.hotels[0].roomResponse.selectedRooms = vm.selectedRooms;
                session.content.searchResponse.hotels[0].roomResponse.selectedRooms.roomSpecific = "" // ??
                session.content.searchResponse.hotels[0].roomReferences = vm.roomDetails.roomSpecific ? vm.roomDetails.roomSpecific.roomReferences : undefined;
                session.content.searchResponse.hotels[0].prebookResponse = response.data.response.content.prebookResponse;
                session.content.searchResponse.hotels[0].imageUrl = hotelDetails.content.searchResponse.hotels[0].imageUrl || vm.imageCarousel[0].url ||'/assets/images/no-image.png'
                window.sessionStorage.setItem("hotelInfo", JSON.stringify(session));

                if (showCancellationMessage) {
                  alertify.confirm(vm.PleaseNotePopUp_Label, vm.CancellationWarning_Message + "</br></br>" + vm.WarningNotes_Message,
                    function () { data.callback(); },
                    function () { vm.bookNowIsClicked = false; });
                } else {
                  data.callback();
                }
              }
            } else {
              alertify.alert(vm.ErrorPopUp_Label, vm.UnableToProceedError_Message)
              vm.bookNowIsClicked = false;
            }
          } else {
            alertify.alert(vm.ErrorPopUp_Label, vm.UnableToProceedError_Message)
            vm.bookNowIsClicked = false;
          }
          if (localStorage.direction == 'rtl') {
            $(".ajs-modal").addClass("ar_direction1");
          } else {
            $(".ajs-modal").removeClass("ar_direction1");
          }
      }).catch(error => {

        var self = this;
        commonlogin(function (response) {

          if (self.prebookRQCount <= 2) {
            self.sendPrebook(self.prebookRQData);
          } else {
            if (error.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              alertify.alert(this.ErrorPopUp_Label, error.response.data.message)
              console.log("Server error: ", error.response.data);
            } else if (error.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log("No response received: ", error.request);
            } else {
              // Something happened in setting up the request that triggered an Error
              console.log("Error", error.message);
            }
          }

        });
        if (localStorage.direction == 'rtl') {
          $(".ajs-modal").addClass("ar_direction1");
        } else {
          $(".ajs-modal").removeClass("ar_direction1");
        }
      })
        .finally(() => {
          console.log("Finally");
        });

    },
    sendRoomsRQ: function (roomRQ) {
      this.roomRQCount++;
      var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;


      axios({
        method: "post",
        url: hubUrl + "/rooms",
        data: {
          request: roomRQ
        },
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.access_token
        }
      }).then(rooms => {

        console.log(rooms.data);
        if (rooms.data) {

          var hotelDetails = JSON.parse(window.sessionStorage.getItem("hotelInfo"));


          try {
            //rooms binding
            var roomsArray = rooms.data.response.content.roomResponse.rooms;
            var bundleList = rooms.data.response.content.roomResponse.bundleList;
            var optionalList = rooms.data.response.content.roomResponse.optionalList

            var bundledResult = [];
            var optionalResult = [];
            var optionalRoomId = 0;
            var bundledRoomId = 0;
            for (var roomIndex = 0; roomIndex < roomsArray.length; roomIndex++) {

              var roomDetailsArray = roomsArray[roomIndex].roomsDetails.sort((a, b) => a.rates[0].pricing.totalPrice.price - b.rates[0].pricing.totalPrice.price);
              var optionalRoom = [];

              for (var roomDetailsIndex = 0; roomDetailsIndex < roomDetailsArray.length; roomDetailsIndex++) {

                var roomRatesArray = roomDetailsArray[roomDetailsIndex].rates;

                for (var rateIndex = 0; rateIndex < roomRatesArray.length; rateIndex++) {

                  var rates = roomRatesArray[rateIndex];
                  var roomDetails = _.cloneDeep(roomDetailsArray[roomDetailsIndex]);
                  roomDetails.rates = [rates];
                  roomDetails.isPrebookSent = false;
                  roomDetails.rates[0].cancellationPoliciesParsed = [];
                  roomDetails.rates[0].isLoading = true;
                  roomDetails.rates[0].language = "ltr";


                  if (_.includes(bundleList, rates.bundleId)) {

                    var bundleIndex = _.findIndex(bundledResult, {
                      bundleId: rates.bundleId
                    });
                    roomDetails.bundledRoomId = bundledRoomId++;
                    if (bundleIndex == -1) {
                      bundledResult.push({
                        bundleId: rates.bundleId,
                        roomsToggleShowMore: true,
                        details: [{
                          searchRoomIndex: roomsArray[roomIndex].searchRoomIndex,
                          room: roomDetails
                        }]
                      });
                    } else {

                      bundledResult[bundleIndex].details.push({
                        searchRoomIndex: roomsArray[roomIndex].searchRoomIndex,
                        room: roomDetails
                      });
                    }

                  } else if (_.includes(optionalList, rates.bundleId)) {
                    roomDetails.isSelected = false;
                    roomDetails.optionalRoomId = optionalRoomId++;
                    optionalRoom.push(roomDetails);
                  }

                }

              }

              if (optionalRoom.length > 0) {
                optionalResult.push({
                  searchRoomIndex: roomsArray[roomIndex].searchRoomIndex,
                  roomsToggleShowMore: true,
                  rooms: optionalRoom
                });
              }
            }

            if (optionalResult.length > 0) {
              for (var roomIndex = 0; roomIndex < optionalResult.length; roomIndex++) {
                var index = _.indexOf(optionalResult[roomIndex].rooms,
                  _.minBy(optionalResult[roomIndex].rooms, function (room) {
                    return room.rates[0].pricing.totalPrice.price;
                  }))
                optionalResult[roomIndex].rooms[index].isSelected = true;
              }
            }

            console.log(bundledResult);
            console.log(optionalResult);

            this.bundledResult = bundledResult;
            this.optionalResult = optionalResult;

            this.roomDetails.roomsReferences = rooms.data.response.content.roomResponse.roomSpecific ? rooms.data.response.content.roomResponse.roomSpecific.roomReferences : undefined;

            //Update global store
            store.updateState("roomsRS", rooms.data);
            var hotelSearchResponse = this.hotelDetails.content.searchResponse;
            var hotelRQ = {
              service: "HotelRQ",
              node: hotelDetails.node,
              token: hotelDetails.token,
              hubUuid: hotelDetails.hubUuid,
              supplierCodes: ["" + hotelDetails.supplierCode],
              credentials: [],
              content: {
                command: "HotelDetailsRQ",
                hotel: {
                  oneviewId: hotelSearchResponse.hotels[0].oneviewId,
                  hotelCode: hotelSearchResponse.hotels[0].hotelCode,
                  nationality: hotelSearchResponse.nationality,
                  residentOf: hotelSearchResponse.residentOf,
                  location: hotelSearchResponse.hotels[0].location,
                  rooms: hotelDetails.content.supplierSpecific.criteria.rooms,
                  stay: hotelDetails.content.supplierSpecific.criteria.stay,
                  hotelSpecific: {
                    test: "test"
                  }
                },
                supplierSpecific: {
                  uuid: localStorage.getItem("hotelUUID"),
                  searchReferences: hotelSearchResponse.supplierSpecific ? hotelSearchResponse.supplierSpecific.searchReferences : undefined,
                  searchHotelReferences: hotelSearchResponse.hotels[0].hotelSpecific ? hotelSearchResponse.hotels[0].hotelSpecific.searchHotelReferences : undefined,
                  roomsReferences: this.roomDetails.roomSpecific ? this.roomDetails.roomSpecific.roomReferences : undefined
                }
              }
            };
            this.hDetailRQData = hotelRQ;
            this.sendHDetailsRQ(hotelRQ);

          } catch (error) {
            alertify.alert(this.ErrorPopUp_Label, this.ProblemWithRoomSelection_Message);
            if (localStorage.direction == 'rtl') {
              $(".ajs-modal").addClass("ar_direction1");
            } else {
              $(".ajs-modal").removeClass("ar_direction1");
            }
          }
        }
      }).catch(error => {

        var self = this;
        commonlogin(function (response) {

          if (self.roomRQCount <= 2) {
            self.sendRoomsRQ(self.roomRQData);
          } else {
            if (error.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              alertify.alert(this.ErrorPopUp_Label, error.response.data.message)
              console.log("Server error: ", error.response.data);
            } else if (error.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log("No response received: ", error.request);
            } else {
              // Something happened in setting up the request that triggered an Error
              console.log("Error", error.message);
            }
          }
          if (localStorage.direction == 'rtl') {
            $(".ajs-modal").addClass("ar_direction1");
          } else {
            $(".ajs-modal").removeClass("ar_direction1");
          }
        });

      }).finally(() => {
        console.log("Finally");
      });
    },
    sendHDetailsRQ: function (hotelRQ) {
      this.hDetailRQCount++;
      var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;


      axios({
        method: "post",
        url: hubUrl + "/hotelDetails",
        data: {
          request: hotelRQ
        },
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.access_token
        }
      }).then(hotel => {

        console.log(hotel.data);

        if (hotel.data) {
          //hotel binding

          var hotelDetails = JSON.parse(window.sessionStorage.getItem("hotelInfo"));


          try {

            this.imageCarousel = hotel.data.response.content.hotelDetailsResponse.hotel.specification[0].images.filter(function (img) {
              return !_.isEmpty(img) 
              // && img.url.toLowerCase().match(/\.(jpg|png|gif)/g)
            });
           
            if (this.imageCarousel.length == 1 && this.imageCarousel[0].url == '') {
              this.imageCarousel = [];
            }
            this.hotelDescription = this.unescapeHtml(hotel.data.response.content.hotelDetailsResponse.hotel.specification[0].description);
          } catch (error) {
            this.imageCarousel = [];
            this.hotelDescription = "";
          }

          var hotelAmenities = hotel.data.response.content.hotelDetailsResponse.hotel.hotelAmenities || [];
          var filteredhotelAmenities = _.filter(hotelAmenities, function (e) { return e.oneviewCode != 'NA'; });

          if (filteredhotelAmenities.length == 0) {
            filteredhotelAmenities = hotelAmenities.slice(0, 9);
          } else if (filteredhotelAmenities.length>5){
            filteredhotelAmenities = filteredhotelAmenities.slice(0, 5);
          }

          this.filteredhotelAmenities = filteredhotelAmenities;
          this.hotelAmenities = hotelAmenities;

          var roomAmenities = hotel.data.response.content.hotelDetailsResponse.hotel.roomAmenities || [];
          var filteredroomAmenities = _.filter(roomAmenities, function (e) { return e.oneviewCode != 'NA'; });
          if (filteredroomAmenities.length == 0) {
            filteredroomAmenities = roomAmenities.slice(0, 9);
          }
          
          this.roomAmenities = roomAmenities;
          this.filteredroomAmenities = filteredroomAmenities;

          if (this.hotelAmenities.length == 0) {
            this.hideHotelAmenities = false;
          }

          if (this.roomAmenities.length == 0) {
            this.hideRoomAmenities = false;

          }

          if (this.optionalResult.length == 1) {
            var totalPrice = parseFloat(0);
            for (var roomIndex = 0; roomIndex < this.optionalResult.length; roomIndex++) {
              var rooms = this.optionalResult[roomIndex].rooms
              for (var rateIindex = 0; rateIindex < rooms.length; rateIindex++) {
                var rates = rooms[rateIindex];
                if (rates.isSelected) {
                  totalPrice += parseFloat(rates.rates[0].pricing.totalPrice.price);
                }
              }
            }
            this.minimumHotelPrice = totalPrice;
          }else {
            this.minimumHotelPrice = Number.POSITIVE_INFINITY;
          }

          var session = JSON.parse(window.sessionStorage.getItem("hotelInfo"));
          session.content.searchResponse.hotels[0].hotelDetailsResponse = {};
          session.content.searchResponse.hotels[0].hotelDetailsResponse = hotel.data;
          window.sessionStorage.setItem("hotelInfo", JSON.stringify(session));

          //Update global store
          store.updateState("hotelRS", hotel.data);

          this.$nextTick(function () {
            // var script = document.createElement('script');
            // script.src = "/assets/pluggins/sliderengine/initslider-1.js";
            // document.getElementsByTagName('head')[0].appendChild(script);

            //Don't remove scrollbar when showing alertify pop up.
            alertify.defaults.preventBodyShift = true;
            //Don't focus on top.
            alertify.defaults.maintainFocus = false;


              $(document).ready(function () {
                $('#imageGallery').lightSlider({
                  gallery: true,
                  item: 1,
                  auto: true,
                  loop: true, 
                  thumbItem: 9,
                  slideMargin: 0,
                  adaptiveHeight:true,
                  pauseOnHover: true,
                  enableDrag: false,
                  currentPagerPosition: 'left',
                  onSliderLoad: function (el) {

                    var maxHeight = 0,
                      container = $(el),
                      children = container.children();

                    children.each(function () {
                      var childHeight = $(this).height();
                      if (childHeight > maxHeight) {
                        maxHeight = childHeight;
                      }
                    });
                    container.height(maxHeight);

                    el.lightGallery({
                      selector: '#imageGallery .lslide',
                      thumbnail: true
                    });
                  }
                });
              });

          $(function () {
            $('.filter .facilities ul li').each(function () {
              if ($.trim($(this).text()).length == 0) {
                $(this).hide();

                console.log('li number ' + ($(this).index() + 1) + ' is hidden');
              }
            });
          });

          });

        } else {

        }
        if (localStorage.direction == 'rtl') {
          $(".ar_direction").addClass("ar_direction1");
        } else {
          $(".ar_direction").removeClass("ar_direction1");
        }

      }).catch(error => {

        var self = this;
        commonlogin(function (response) {

          if (self.hDetailRQCount <= 2) {
            self.sendHDetailsRQ(self.hDetailRQData);
          } else {
            if (error.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              alertify.alert(this.ErrorPopUp_Label, error.response.data.message)
              console.log("Server error: ", error.response.data);
            } else if (error.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log("No response received: ", error.request);
            } else {
              // Something happened in setting up the request that triggered an Error
              console.log("Error", error.message);
            }
          }

        });
      }).finally(() => {
        console.log("Finally");
      });

    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;

      getAgencycode(function (response) {
        //var Agencycode = response;
        var Agencycode = 'AGY435';
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        self.dir = langauage == "ar" ? "rtl" : "ltr";
        var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Hotel Details/Hotel Details/Hotel Details.ftl';
        axios.get(aboutUsPage, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          self.getdata = false;
          if (response.data.area_List.length > 0) {
            var mainComp = response.data.area_List[0].main;

            self.SelectRoomButton_Label = self.pluckcom('SelectRoomButton_Label', mainComp.component);
            self.AdultCount_Label = self.pluckcom('AdultCount_Label', mainComp.component);
            self.ChildCount_Label = self.pluckcom('ChildCount_Label', mainComp.component);
            self.ChildrenCount_Label = self.pluckcom('ChildrenCount_Label', mainComp.component);
            self.NightsCount_Label = self.pluckcom('NightsCount_Label', mainComp.component);
            self.NoMapAvailable_Label = self.pluckcom('NoMapAvailable_Label', mainComp.component);
            self.ChildrenAgesSearch_Label = self.pluckcom('ChildrenAgesSearch_Label', mainComp.component);
            self.ViewCancellationPolicy_Label = self.pluckcom('ViewCancellationPolicy_Label', mainComp.component);
            self.BookNow_Label = self.pluckcom('BookNow_Label', mainComp.component);
            self.HotelAmenities_Label = self.pluckcom('HotelAmenities_Label', mainComp.component);
            self.RoomAmenities_Label = self.pluckcom('RoomAmenities_Label', mainComp.component);
            self.AllAmenitiesLink_Label = self.pluckcom('AllAmenitiesLink_Label', mainComp.component);
            self.CancellationPolicyHeader_Label = self.pluckcom('CancellationPolicyHeader_Label', mainComp.component);
            self.CancellationNotes_Label = self.pluckcom('CancellationNotes_Label', mainComp.component);
            self.Rooms_Label = self.pluckcom('Rooms_Label', mainComp.component);

            self.AboutTheHotel_Label = self.pluckcom('AboutTheHotel_Label', mainComp.component);
            self.LoadingCancellation_Label = self.pluckcom('LoadingCancellation_Label', mainComp.component);
            self.CancellationTextA1_Label = self.pluckcom('CancellationTextA1_Label', mainComp.component);
            self.CancellationTextA2_Label = self.pluckcom('CancellationTextA2_Label', mainComp.component);
            self.CancellationTextA3_Label = self.pluckcom('CancellationTextA3_Label', mainComp.component);
            self.CancellationTextB1_Label = self.pluckcom('CancellationTextB1_Label', mainComp.component);
            self.CancellationTextC1_Label = self.pluckcom('CancellationTextC1_Label', mainComp.component);
            self.CancellationTextC2_Label = self.pluckcom('CancellationTextC2_Label', mainComp.component);
            self.GeneralDescription_Label = self.pluckcom('GeneralDescription_Label', mainComp.component);

            self.CancellationWarning_Message = self.pluckcom('CancellationWarning_Message', mainComp.component);
            self.WarningNotes_Message = self.pluckcom('WarningNotes_Message', mainComp.component);
            self.UnableToProceedError_Message = self.pluckcom('UnableToProceedError_Message', mainComp.component);
            self.ProblemWithRoomSelection_Message = self.pluckcom('ProblemWithRoomSelection_Message', mainComp.component);
            self.ErrorPopUp_Label = self.pluckcom('ErrorPopUp_Label', mainComp.component);
            self.PleaseNotePopUp_Label = self.pluckcom('PleaseNotePopUp_Label', mainComp.component);


            self.ConfirmationHeader_Label = self.pluckcom('ConfirmationHeader_Label', mainComp.component);
            self.PaymentHeader_Label = self.pluckcom('PaymentHeader_Label', mainComp.component);
            self.TravellerDetailsHeader_Label = self.pluckcom('TravellerDetailsHeader_Label', mainComp.component);
            self.ReviewHotelHeader_Label = self.pluckcom('ReviewHotelHeader_Label', mainComp.component);

            self.SelectButton_Label = self.pluckcom('SelectButton_Label', mainComp.component);
            self.SelectedButton_Label = self.pluckcom('SelectedButton_Label', mainComp.component);


            self.showMoreRooms_Label = self.pluckcom('showMoreRooms_Label', mainComp.component);
            self.showLessRooms_Label = self.pluckcom('showLessRooms_Label', mainComp.component);
            self.currentLanguage = localStorage.direction;
            self.bookingDetails.bookingPaxDetails = self.bookingDetails.adultCount + " " + self.AdultCount_Label + (self.bookingDetails.childCount != 0 ? (", " + self.bookingDetails.childCount + (self.bookingDetails.childCount == 1 ? " " + self.ChildCount_Label : " " + self.ChildrenCount_Label)) : '') + ", " + (self.bookingDetails.numberOfNights + " " + self.NightsCount_Label);
            if (localStorage.direction == 'rtl') {
              alertify.defaults.glossary.title = 'أليرتفاي جي اس';
              alertify.defaults.glossary.ok = 'موافق';
              alertify.defaults.glossary.cancel = 'إلغاء';
              alertify.confirm().set('labels', { ok: 'موافق', cancel: 'إلغاء' });
              alertify.alert().set('label', 'موافق');


            } else {
              alertify.defaults.glossary.title = 'AlertifyJS';
              alertify.defaults.glossary.ok = 'Ok';
              alertify.defaults.glossary.cancel = 'Cancel';
              alertify.confirm().set('labels', { ok: 'Ok', cancel: 'Cancel' });
              alertify.alert().set('label', 'Ok');

            }
          }
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
      });

    },
    toggleShowMoreRoom: function (item) {
      item.roomsToggleShowMore = !item.roomsToggleShowMore;
      setTimeout(function () {
        $('html, body').animate({
          scrollTop: $("#select-room").offset().top - 100
        }, 300, 'linear');
      }, 50);

    },
    getMealType: function(room){
      var mealType = "";
      if (room.rates[0].boards.length > 0 && room.rates[0].boards[0].value) {
        mealType = '<li class="green-color"><i class="fa fa-check" aria-hidden="true"></i>'+
          room.rates[0].boards[0].value.charAt(0).toUpperCase() + _.unescape(room.rates[0].boards[0].value.slice(1).toLowerCase())+
          '</li>';
      }
      return mealType;
    }
  },
  created() {
     
    this.getPagecontent();
    // Get hotel data in session storage.
    // var hotelDetails = JSON.parse('{"response":{"service":"HotelRS","supplierCode":"22","node":{"currency":"AED","agencyCode":"AGY75"},"hubUuid":"495f23a5-041e-4f8a-b993-73bf7e25491d","token":"eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vaHViLm1hdHJpeC5ARj75bE64Vdc70ABIXlv-fwo","content":{"command":"HotelSearchRS","searchResponse":{"hotels":[{"index":0,"oneviewId":"ONV1001","name":"CROWNE PLAZA DUBAI","hotelCode": "CHA1","preferredHotel":false,"starRating":"5.0","imageUrl":"https://cloud1.nowworld.com/cpfv3/images/?image=aHR0cHM6Ly9jbG91ZDEubm93d29ybGQuY29tLy9wcm9qZWN0X2ZvbGRlci9uaXJ2YW5hbGxjL3VwbG9hZHMvaG90ZWxfaW1hZ2VzL3VwbG9hZC8xNDMzOTI0NjkzX21haW5fMTMyNTEzMzQuanBn","location":{"address":"SHEIKH ZAYED AL NAHYAN ROAD PO BOX 23215 ","cityCode":"71649","cityName":"Dubai","countryCode":"AE","latitude":"25.220303","longitude":"55.280315"},"hotelAmenity":[{"oneviewCode":"AC","code":"6GA","name":"Air Conditioning","category":"","price":2}],"roomAmenity":[{"oneviewCode":"AC","code":"AC","name":"Air Conditioning","category":"","price":2}],"pointOfInterest":[{"name":"Beach","distance":"0.9","distanceUnit":"M","description":"One of the best tourist destination"},{"name":"Grand Mosque","distance":"5.3","distanceUnit":"KM","description":"One of the best tourist destination"}],"boards":[{"oneviewCode":0,"supplierCode":"SA","value":"BED AND BREAKFAST","included":true,"price":10},{"oneviewCode":0,"supplierCode":"SA","value":"ROOM ONLY","included":false,"price":25}],"tripAdvisorInfo":{"averageRating":0,"noOfReviews":0,"ratingImage":"","review":[],"reviewId":"","reviewsPageURL":"","writeReviewURL":""},"policies":[{"code":"NPA","description":"No Pets Allowed"}],"rate":"562.5","searchHotelReferences":{"ratePlanCode":"45821420190311250","hotelId":"5c8a3a143e0ad5c1e00f9579"},"hotelSpecific":{"bundleStatus":false}}],"searchSpecific":{"hotelCount":240},"nationality":"AE","residentOf":"AE","supplierPreference":2,"searchReferences":{"searchReferenceId":"45821420190311250185642714567"}},"error":{"message":"","code":"","type":"","supplierErrorCode":""},"supplierSpecific":{"criteria":{"criteriaType":"Hotel","location":{"cityCode":"17593","cityName":"Dubai City","countryCode":"AE","latitude":"25.09538","longitude":"55.16171","radius":""},"stay":{"checkIn":"12/04/2019","checkOut":"13/04/2019"},"rooms":[{"roomId":1,"guests":{"adult":1,"children":[{"age":4,"sequence":1},{"age":6,"sequence":2}]}},{"roomId":2,"guests":{"adult":1,"children":[{"age":4,"sequence":1},{"age":6,"sequence":2}]}}],"advanceSearch":{"hotelId":0,"hotelName":"","maxAmount":0,"minAmount":0,"starRatings":[1,5],"hotelChain":"Hayat"}}}}}}');
    // window.sessionStorage.setItem("hotelInfo", '{"service":"HotelRS","content":{"command":"HotelSearchRS","supplierSpecific":{"criteria":{"criteriaType":"Hotel","location":{"cityCode":"17593","cityName":"Dubai City","countryCode":"AE","latitude":"25.09538","longitude":"55.16171"},"stay":{"checkIn":"26/04/2019","checkOut":"27/04/2019"},"rooms":[{"roomId":0,"guests":{"adult":1,"children":[{"age":2,"sequence":1},{"age":3,"sequence":2}]}},{"roomId":0,"guests":{"adult":2,"children":[{"age":2,"sequence":1}]}}],"advanceSearch":{"hotelId":0,"hotelName":"aaaa","maxAmount":0,"minAmount":0,"hotelChain":"dfdf"}},"freeCancellation":false},"searchResponse":{"hotels":[{"index":0,"name":"Fortune Hotel Deira~","preferredHotel":"false","starRating":"3","imageUrl":"http://www.eetglobal.com/images/upload_p//EETGlobalV2/HotelsImages/UAE/Deira/FortuneHotelDeira/Fortune_Hotel_Deira_View.png","hotelCode":"JP042956","location":{"cityCode":"17593","cityName":"Dubai City","latitude":"25.2744550088463","longitude":"55.3152561932802","address":"Omer Bin Al Khatab Rd , Deira , Deira, 49185 Dubai, United Arab Emirates"},"rate":"148.39","searchHotelReferences":{"ratePlanCode":"4eCmH85Qy/IlM9gORayW86UkaBCs+W1P125mz6+vhlyJ22zMOzgq2XM4Th+ob9XHU6FZHNKUCs6G9l7XPw6ikBUshWerWOKozM+5+mBg8p2/LnekNToXRt6fJSiBEbEu50xTssaUmsLncwwmVhDorOy6GLyh5loGLtNATLqvmLkQ8QRLT2NH3F9yAGdflS6trP0x/t0pL9h+81+etRXqnQ5nq6AjSFTXDV/d0V6QpnJkWFC2Uz7ao8BKRJw6mqS7FIhQMZmPCEIn/qUkxUIjPdvxUn/NTAo2bd4YTsoATKUIs8L3Zq+K1HfzLbOqfZj0kxqzjW54pDcdjX0mbr0L/aiH9qQmt3lfCp39jLWN9XGlM6mkMiRD4XwBeJqBqiELicBvzpBz0RZiRUl9BpKk92ocN5u4z5kGkiTgXAcEf2VUiNsRw8eBqz/WjlJibDvt47FAoj12xNVyM1tE9EtzDu7py54pxZcv4DiHo6euzd+9M6JQpt0SDURgMgDFAAv6aLXD/k7cIQ6ou1C1iFHkSa0moiHaFf3R9+LEfRQovZ8yRyfi2OXTyxUe4AQ8Zg8nV/Ib2ErhjfI3tckA7lgdd3Y6QG8FuXyjr7JpZoJjNDWBzGSdrjFwrhxMBi0/HGKRzGDlpgQFm+Mv3I0jzC0P3+eRDz9x9VrqVkqAxQhasZjWo6AKYggGOYybkJt5eZUw","hotelId":"JP042956"},"supplierCode":22,"currency":"AED","cityCode":"17593","cityName":"Dubai City","countryCode":"AE","searchReferences":{}}],"searchSpecific":{"hotelCount":425},"nationality":"AE","residentOf":"AE","searchReferences":{}},"searchReferences":{}},"token":"eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vaHViLm1hdHJpeC5jb20vIiwic3ViIjoiNzA5IiwiZXhwIjoxNTU1MTQ3NDE1fQ.97vfCcHiF8ElL49hXIeob6X8Iqlcq6ieh1DWP7dUedw","hubUuid":"9837c435","node":{"currency":"AED","agencyCode":"AGY436"},"supplierCode":22}');
    var hotelDetails = JSON.parse(window.sessionStorage.getItem("hotelInfo"));

    this.hotelDetails = hotelDetails;

    var stay = this.hotelDetails.content.supplierSpecific.criteria.stay;

    var checkOut = moment(stay.checkOut, 'DD/MM/YYYY');
    var checkIn = moment(stay.checkIn, 'DD/MM/YYYY');

    this.bookingDetails.cInDate = {
      fullDate: checkIn.format("DD/MM/YYYY")
    };
    this.bookingDetails.cOutDate = {
      fullDate: checkOut.format("DD/MM/YYYY")
    };

    var bookingPaxDetails = hotelDetails.content.supplierSpecific.criteria.rooms;

    var childCount = 0;
    var adultCount = 0;
    for (var roomIndex = 0; roomIndex < bookingPaxDetails.length; roomIndex++) {
      var roomDetails = bookingPaxDetails[roomIndex].guests;
      adultCount += roomDetails.adult;
      if (roomDetails.children != undefined && roomDetails.children.length > 0) {
        childCount += roomDetails.children.length;
      }
    }


    this.bookingDetails.childCount = childCount;
    this.bookingDetails.adultCount = adultCount;
    this.bookingDetails.numberOfNights = moment.duration(moment(checkOut).diff(moment(checkIn))).asDays();
    this.bookingDetails.bookingPaxDetails = adultCount + " " + this.AdultCount_Label + (childCount != 0 ? (", " + childCount + (childCount == 1 ? " " + this.ChildCount_Label : " " + this.ChildrenCount_Label)) : '') + ", " + (this.bookingDetails.numberOfNights + " " + this.NightsCount_Label);

    var hotelSearchResponse = this.hotelDetails.content.searchResponse;
    var roomRQ = {
      service: "HotelRQ",
      node: hotelDetails.node,
      token: hotelDetails.token,
      hubUuid: hotelDetails.hubUuid,
      supplierCodes: ["" + hotelDetails.supplierCode],
      credentials: [],
      content: {
        command: "HotelRoomRQ",
        criteria: {
          criteriaType: "HotelRoom",
          hotel: {
            hotelName: hotelSearchResponse.hotels[0].name,
            rooms: hotelDetails.content.supplierSpecific.criteria.rooms,
            location: hotelSearchResponse.hotels[0].location,
            searchHotelReferences: hotelSearchResponse.hotels[0].hotelSpecific ? hotelSearchResponse.hotels[0].hotelSpecific.searchHotelReferences : undefined
          },
          stay: hotelDetails.content.supplierSpecific.criteria.stay,
          nationality: hotelSearchResponse.nationality,
          residentOf: hotelSearchResponse.residentOf
        },
        supplierSpecific: {
          uuid: localStorage.getItem("hotelUUID"), // from session storage
          searchReferences: hotelSearchResponse.supplierSpecific ? hotelSearchResponse.supplierSpecific.searchReferences : undefined
        }
      }
    }

    //Update global store
    store.updateState("roomsRQ", roomRQ);
    this.roomRQData = roomRQ;
    this.sendRoomsRQ(roomRQ);

  },
  beforeRouteLeave(to, from, next) {
    // called when the route that renders this component is about to
    // be navigated away from.
    // has access to `this` component instance.

    console.log("leaving...")

    var rooms = [];
    for (var index = 0; index < this.selectedRooms.length; index++) {

      var room = this.selectedRooms[index].room || this.selectedRooms[index];
      rooms.push({
        name: room.roomName,
        roomId: room.roomId,
        paxes: [], // from pax page
        guests: room.guests, // from search 
        rate: {
          bookable: true,
          cancellationPolicies: room.rates[0].cancellationPolicies,
          rateSpecific: room.rates[0].roomRateSpecific,
          pricing: {
            perNightPrice: room.rates[0].pricing.perNightPrice,
            totalPrice: room.rates[0].pricing.totalPrice
          },
          roomRateReferences: room.rates[0].roomRateSpecific ? room.rates[0].roomRateSpecific.roomRateReferences : undefined
        },
        roomReferences: room.roomSpecific ? room.roomSpecific.roomReferences : undefined

      });
    }

    var PrebookRQ = {
      service: "HotelRQ",
      node: this.hotelDetails.node,
      token: this.hotelDetails.token,
      hubUuid: this.hotelDetails.hubUuid,
      supplierCodes: ["" + this.hotelDetails.supplierCode],
      credentials: [],
      content: {
        command: "PrebookRQ",
        hotel: {
          cityCode: this.hotelDetails.content.searchResponse.hotels[0].location.cityCode,
          hotelCode: this.hotelDetails.content.searchResponse.hotels[0].hotelCode,
          totalAmount: 0, // total for hotel?
          sealCode: this.hotelDetails.content.searchResponse.hotels[0].sealCode, // what sealcode to use?
          nationality: this.hotelDetails.content.searchResponse.nationality,
          residentOf: this.hotelDetails.content.searchResponse.residentOf,
          rooms: rooms,
          hotelSpecific: this.hotelDetails.content.searchResponse.hotels[0].hotelSpecific
        },
        paxes: [], // from pax page?
        stay: this.hotelDetails.content.supplierSpecific.criteria.stay,
        supplierSpecific: {
          uuid: localStorage.getItem("hotelUUID"), // from session storage
          requestType: "cancellationPolicy",
          searchReferences: this.hotelDetails.content.searchResponse.supplierSpecific ? this.hotelDetails.content.searchResponse.supplierSpecific.searchReferences : undefined,
          searchHotelReferences: this.hotelDetails.content.searchResponse.hotels[0].hotelSpecific ? this.hotelDetails.content.searchResponse.hotels[0].hotelSpecific.searchHotelReferences : undefined,
          roomsReferences: this.roomDetails.roomSpecific ? this.roomDetails.roomSpecific.roomReferences : undefined
        }
      }
    }

    //Update global store
    store.updateState("prebookCancellationRQ", PrebookRQ);
    this.prebookRQData = {
      request: PrebookRQ,
      roomId: null,
      isOptional: false,
      fromBooking: true,
      callback: function () {
        next();
      }
    };
    this.sendPrebook({
      request: PrebookRQ,
      roomId: null,
      isOptional: false,
      fromBooking: true,
      callback: function () {
        next();
      }
    });

    console.log(rooms);
  }
};

Vue.component("booking-price", {
  props: {
    stickyIndex: Number,
    roomDetails: Array,
    revertBooking: Boolean,
    isOptional: Boolean,
    bookNow: Function,
    bundleId: Number,
    bookDetails: String,
    currency: String,
    minimumHotelPrice: Function,
    sharedStateHotel: Object,
    bookNowLabel: String
  },
  data: function () {
    return {
      totalPrice: 0,
      tweenedPrice: 0,
      bookNowIsClicked: false,
      buttonString:  this.bookNowLabel + (this.bookNowIsClicked && this.revertBooking ? '&nbsp;&nbsp;<i class="fa fa-hourglass-o fa-spin" aria-hidden="true"></i>' : '')
    };
  },
  watch: {
    totalPrice: function () {
      TweenLite.to(this.$data, 0.5, {
        tweenedPrice: this.totalPrice
      });

    },
    
    revertBooking: {
      immediate: true,
      handler(val, oldVal) {
        this.buttonString = this.bookNowLabel + (this.bookNowIsClicked && this.revertBooking ? '&nbsp;&nbsp;<i class="fa fa-hourglass-o fa-spin" aria-hidden="true"></i>' : '');
      }
    }

  },
  computed: {
    getTotalPrice: function () {
      var totalPrice = parseFloat(0);
      if (this.isOptional) {
        for (var roomIndex = 0; roomIndex < this.roomDetails.length; roomIndex++) {
          var rooms = this.roomDetails[roomIndex].rooms
          for (var rateIindex = 0; rateIindex < rooms.length; rateIindex++) {
            var rates = rooms[rateIindex];
            if (rates.isSelected) {
              totalPrice += parseFloat(rates.rates[0].pricing.totalPrice.price);
            }
          }
        }
      } else {
        for (var index = 0; index < this.roomDetails.length; index++) {
          totalPrice += parseFloat(this.roomDetails[index].room.rates[0].pricing.totalPrice.price);
        }
      }
      this.totalPrice = totalPrice;
      this.$emit("minimum-hotel-price", this.totalPrice);
      return this.tweenedPrice.toFixed(2);
    }
  },
  methods: {
    bookNowIsClickedFunc() {
     
      this.bookNowIsClicked = true;
      this.$emit('book-now', this.bundleId, this.isOptional);
    }
  },
  template: `
    <div class="col-sm-4 col-xs-12 col-md-4" :style="isOptional?'':'z-index:'+stickyIndex" :class="isOptional?'':'sticky'">
      <div class="price-section" :class="isOptional?'sticky':''">
        <div class="price">
          {{ $n((getTotalPrice/sharedStateHotel.currencyMultiplier), 'currency', sharedStateHotel.selectedCurrency) }}
        </div>
        <p class="adult-night">{{bookDetails}}</p>
        <a class="select-room" @click.prevent="bookNowIsClickedFunc" v-html="buttonString"></a>
        </div>
        <slot></slot>
    </div>
  `
});

function ParseCancellationPolicy(cancellationPolicy, roomPrice) {

  var parsedCancellationPolicies = [];
  var isCharged = false;

  for (var i = 0; i < cancellationPolicy.length; i++) {
    var to = moment(cancellationPolicy[i].to, "DD/MM/YYYY").format("MMM DD YYYY, dddd");
    var from = moment(cancellationPolicy[i].from, "DD/MM/YYYY").format("MMM DD YYYY, dddd");
    var amount = i18n.n((cancellationPolicy[i].amount / store.state.currencyMultiplier), 'currency', store.state.selectedCurrency);
    if (parseInt(cancellationPolicy[i].amount) == 0) {
      parsedCancellationPolicies.push('<p style="color:green;">Free cancellation on or before <b>' + to + '</b></p>');
    } else if (roomPrice == cancellationPolicy[i].amount && (to == "" && from == "")) {
        isCharged = true;
        parsedCancellationPolicies.push('<p style="color:red;">Non refundable booking with cancellation charges <b>' + amount + '</b></p>');
    } else if (to != "" && from != "") {
      parsedCancellationPolicies.push("<p>Cancellation between <b>" + from + "</b> and <b>" + to + "</b> will be charged <b>" + amount + "</b></p>");
      if (moment(from, 'MMM DD YYYY, dddd').isSameOrBefore(moment().toDate(), "day")) {
        isCharged = true;
      }
    }

  }
  return { cancellation: parsedCancellationPolicies, isCharged: isCharged };

}

function ParseCancellationPolicyWithI18n(cancellationPolicy, roomPrice, labels) {

  var parsedCancellationPolicies = [];
  var isCharged = false;

  for (var i = 0; i < cancellationPolicy.length; i++) {
    var to = moment(cancellationPolicy[i].to, "DD/MM/YYYY").format("MMM DD YYYY, dddd");
    var from = moment(cancellationPolicy[i].from, "DD/MM/YYYY").format("MMM DD YYYY, dddd");
    var amount = i18n.n((cancellationPolicy[i].amount / store.state.currencyMultiplier), 'currency', store.state.selectedCurrency);
    if (parseInt(cancellationPolicy[i].amount) == 0) {
      parsedCancellationPolicies.push('<p style="color:green;">' + labels.CancellationTextC1_Label + ' <b>' + to + '</b></p>');
    } else if (roomPrice == cancellationPolicy[i].amount && (to == "" && from == "")) {
        isCharged = true;
        parsedCancellationPolicies.push('<p style="color:red;">' + labels.CancellationTextB1_Label + ' <b>' + amount + '</b></p>');
    } else if (to != "" && from != "") {
      parsedCancellationPolicies.push("<p>" + labels.CancellationTextA1_Label + " <b>" + from + "</b> " + labels.CancellationTextA2_Label + " <b>" + to + "</b> " + labels.CancellationTextA3_Label + " <b>" + amount + "</b></p>");
      if (moment(from, 'MMM DD YYYY, dddd').isSameOrBefore(moment().toDate(), "day")) {
        isCharged = true;
      }
    }

  }
  return { cancellation: parsedCancellationPolicies, isCharged: isCharged };

}