// Price slider
var startValue = 1010;
var endValue = 500000;
var minValue = 1000;
var maxValue = 500000;
$(document).ready(function () {

    $("#slider-container").slider({
        range: true,
        min: minValue,
        max: maxValue,
        values: [startValue, endValue],
        create: function () {
            $("#amount-from").val(startValue);
            $("#amount-to").val(endValue);
        },
        slide: function (event, ui) {
            $("#amount-from").val(ui.values[0]);
            $("#amount-to").val(ui.values[1]);
            var from = $("#amount-from").val();
            var to = $("#amount-to").val();
            console.log(from + " --- " + to);
        }
    });

});


/**
 * Vue instance for Hotel listing
 */
var Vue_HotelListing = new Vue({
    el: "#hotelListDiv",
    data: {
        access_token: '',
        hotelList: [], //shows hotel results
        total_result: [], //hold all supplier results
        supplierMap: new Map(), //hold supplier wise results
        startLimit: 0,
        limit: 10,
        sortOrder: false,
        isFiltered: false,
        filter_result: [],
        isPriceActive: true,
        isNameActive: false,
        isStarActive: false,
        hotelSearchKeyName: '',
        hotelSearchKeyLocation: '',
        supplierList: supplierList,
        supplierFilterCategories: [],
        hotelRatingCategories: [],
        lowestPrice: Number.POSITIVE_INFINITY,
        highestPrice: Number.NEGATIVE_INFINITY
    },
    created: function () {


         
        var session_url = 'https://localhost:9443/authenticate/agy-436';

        axios.get(session_url, {
            headers: { 'Authorization': 'Basic bmlydmFuYUxpdmU6NllobjhJayw=' }
        }).then(function (response) {
             
            console.log(response);
            this.access_token = response.headers.access_token;


            // this.access_token
            searchRequest = {
                "request": {
                    "command": "searchRQ",
                    "supplierCodes": [
                        40
                    ],
                    "content": {
                        "criteria": {
                            "location": {
                                "cityCode": "5239",
                                "countryCode": "AE",
                                "radius": "5"
                            },
                            "stay": {
                                "checkIn": "29/04/2019",
                                "checkOut": "30/04/2019"
                            },
                            "rooms": [
                                {
                                    "roomId": 0,
                                    "guests": {
                                        "adult": 1,
                                        "children": [
                                            {
                                                "age": 2,
                                                "sequence": 1
                                            },
                                            {
                                                "age": 3,
                                                "sequence": 2
                                            }
                                        ]
                                    }
                                }
                            ],
                            "advanceSearch": {
                                "hotelId": 0,
                                "maxAmount": 0,
                                "minAmount": 0,
                                "starRatings": [
                                    1,
                                    5
                                ],
                                "hotelChain": "Hayat"
                            }
                        },
                        "sort": [
                            {
                                "field": "price",
                                "order": "asc",
                                "sequence": 1
                            }
                        ],
                        "filter": {
                            "noOfResults": "250",
                            "from": 1,
                            "to": 100,
                            "sequence": 1
                        },
                        "token": this.access_token,
                        "uuid": "495f23a5-041e-4f8a-b993-73bf7e25491d",
                        "serviceId": 2
                    }
                }
            }



            websocket.connect(); //open websocket connection
             
            var urlpath = window.location.search;
            var queryString = urlpath.split('?q=');
            console.log("queryString");

            console.log(queryString);

            setTimeout(function () {
                websocket.sendMessage(JSON.stringify(searchRequest)); //send request to web socket server
            }, 1000);

        }).catch(function (error) {
            console.log('Error on Authentication');
        });








    },
    methods: {
        /**
         * used for binding hotel response to html
         * @param {*hotel response} data 
         */
        fetchHotelList: function (data) {
             
            var tmp;
            if (data.data.response.hotels != null) {
                if (data.data.response.hotels.length > 0) {
                    for (let i = 0; i < data.data.response.hotels.length; i++) {
                        this.total_result.push(data.data.response.hotels[i]);
                        tmp = parseFloat(data.data.response.hotels[i].rate);
                        if (tmp < this.lowestPrice) this.lowestPrice = tmp;
                        if (tmp > this.highestPrice) this.highestPrice = tmp;
                    }
                }
            }
            console.log("highest, lowest");
            console.log(this.highestPrice, this.lowestPrice);
            this.supplierList.forEach(supplier_element => {

                var temp_supplierList = [];
                this.total_result.forEach(element => {
                    if (element.supplierCode == supplier_element.id) {
                        temp_supplierList.push(element);
                    }
                });
                this.supplierMap.set(supplier_element.id, temp_supplierList);
                supplier_element.count = temp_supplierList.length;
            });


            if (this.hotelList.length == 0) {

                if (this.total_result.length > 0) {
                    tot_limit = this.total_result.length;
                    if (tot_limit <= 10) {
                        for (let index = this.startLimit; index < tot_limit; index++) {
                            this.hotelList.push(this.total_result[index]);
                        }
                    } else {
                        for (let index = this.startLimit; index < this.limit; index++) {
                            this.hotelList.push(this.total_result[index]);
                        }
                        this.startLimit = this.limit;
                        this.limit = this.limit + 10;
                    }
                }

            }

            console.log("Total:" + this.total_result.length);
        },
        sort: function (sortType) {
            this.isFiltered = true;
            this.sortOrder = !this.sortOrder;
            if (Vue_HotelListing.hotelList.length > 0) {
                Vue_HotelListing.filter_result = Vue_HotelListing.total_result;
                switch (sortType) {
                    case 'price':
                        Vue_HotelListing.filter_result.sort(comparePrice);
                        this.isPriceActive = true;
                        this.isNameActive = false;
                        this.isStarActive = false;
                        break;
                    case 'name':
                        Vue_HotelListing.filter_result.sort(compareName);
                        this.isPriceActive = false;
                        this.isNameActive = true;
                        this.isStarActive = false;
                        break;
                    case 'star':
                        Vue_HotelListing.filter_result.sort(compareStar);
                        this.isPriceActive = false;
                        this.isNameActive = false;
                        this.isStarActive = true;
                        break;
                }
                fetchFilterResults();
            }
        },
        hotelSearch(event) {
            // console.log("Key:");
            // console.log(this.hotelSearchKeyName);
            this.filter_result = [];
            this.hotelList = [];
            this.startLimit = 0;
            this.limit = 10;

            if (this.hotelSearchKeyName.length > 0) {

                this.isFiltered = true;
                if (this.hotelSearchKeyLocation.length > 0) {
                    for (var i = 0; i < this.total_result.length; i++) {
                        if (this.total_result[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation) || this.total_result[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation)) {
                            this.filter_result.push(this.total_result[i]);
                        }
                    }
                }
                if (this.hotelRatingCategories.length > 0) {
                    var tempFilterArray = this.filter_result;
                    this.filter_result = [];
                    this.hotelRatingCategories.forEach(rating_element => {
                        for (var i = 0; i < tempFilterArray.length; i++) {
                            if (parseInt(tempFilterArray[i].starRating) == parseInt(rating_element)) {
                                this.filter_result.push(tempFilterArray[i]);
                            }
                        }
                    });
                    tempFilterArray = [];
                }
                if (this.hotelSearchKeyName.length > 0) {
                    var tempFilterArray = this.filter_result;
                    this.filter_result = [];
                    for (var i = 0; i < this.total_result.length; i++) {
                        if (this.total_result[i].name.toLowerCase().includes(this.hotelSearchKeyName) || this.total_result[i].name.toUpperCase().includes(this.hotelSearchKeyName)) {
                            this.filter_result.push(this.total_result[i]);
                        }
                    } 0

                    tempFilterArray = [];

                }
                /*
                for (var i = 0; i < this.total_result.length; i++) {
                    if (this.total_result[i].name.toLowerCase().includes(this.hotelSearchKeyName) || this.total_result[i].name.toUpperCase().includes(this.hotelSearchKeyName)) {
                        this.filter_result.push(this.total_result[i]);
                    }
                }
                */

                fetchSearchResults();
                console.log(this.filter_result.length);

            } else {
                // console.log("No values");
                if (this.hotelSearchKeyLocation.length > 0) {
                    for (var i = 0; i < this.total_result.length; i++) {
                        if (this.total_result[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation) || this.total_result[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation)) {
                            this.filter_result.push(this.total_result[i]);
                        }
                    }
                }
                if (this.hotelRatingCategories.length > 0) {
                    var tempFilterArray = this.filter_result;
                    this.filter_result = [];
                    this.hotelRatingCategories.forEach(rating_element => {
                        for (var i = 0; i < tempFilterArray.length; i++) {
                            if (parseInt(tempFilterArray[i].starRating) == parseInt(rating_element)) {
                                this.filter_result.push(tempFilterArray[i]);
                            }
                        }
                    });
                    tempFilterArray = [];
                }

                if (this.filter_result.length > 0) {
                    this.isFiltered = true;
                    fetchSearchResults();
                    console.log(this.filter_result.length);

                } else {
                    this.isFiltered = false;
                     
                    fetchAllResults();
                }

            }

        },
        searchByLocation(event) {
            //console.log("Key:");
            //console.log(this.hotelSearchKeyLocation);
            this.filter_result = [];
            this.hotelList = [];
            this.startLimit = 0;
            this.limit = 10;
            if (this.hotelSearchKeyLocation.length > 0) {
                this.isFiltered = true;
                for (var i = 0; i < this.total_result.length; i++) {
                    if (this.total_result[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation) || this.total_result[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation)) {
                        this.filter_result.push(this.total_result[i]);
                    }
                }
                fetchSearchResults();
                console.log(this.filter_result.length);

            }
            else {
                this.isFiltered = false;
                 
                fetchAllResults();
            }
        },
        filterByRating: function (e) {
             
            //console.log(this.hotelRatingCategories);
            this.filter_result = [];
            this.hotelList = [];
            this.startLimit = 0;
            this.limit = 10;
            if (this.hotelRatingCategories.length > 0) {
                this.isFiltered = true;
                if (this.hotelSearchKeyName.length > 0 || this.hotelSearchKeyLocation.length > 0) {
                    filterHotelNameAndLocation();
                    var temp_filterList = this.filter_result;
                    this.filter_result = [];
                    this.hotelRatingCategories.forEach(rating_element => {
                        for (var i = 0; i < temp_filterList.length; i++) {
                            if (parseInt(temp_filterList[i].starRating) == parseInt(rating_element)) {
                                this.filter_result.push(temp_filterList[i]);
                            }
                        }
                    });

                } else {
                    this.hotelRatingCategories.forEach(rating_element => {
                        for (var i = 0; i < this.total_result.length; i++) { //get data from all results
                            if (parseInt(this.total_result[i].starRating) == parseInt(rating_element)) {
                                this.filter_result.push(this.total_result[i]);
                            }
                        }
                    });


                }
                fetchSearchResults();
                console.log(this.filter_result.length);
            } else { //Rating Empty
                if (this.hotelSearchKeyName.length > 0 || this.hotelSearchKeyLocation.length > 0) {
                    filterHotelNameAndLocation();
                    fetchSearchResults();
                    console.log(this.filter_result.length);
                } else {

                    this.isFiltered = false;
                    fetchAllResults();
                }
            }
        }
    }
});
function fetchFilterResults() {
    Vue_HotelListing.hotelList = [];
    if (Vue_HotelListing.filter_result.length > 0) {

        tot_limit = Vue_HotelListing.filter_result.length;
        if (tot_limit >= 10) {
            for (let index = 0; index < 10; index++) {
                Vue_HotelListing.hotelList.push(Vue_HotelListing.filter_result[index]);
            }
        } else if (tot_limit < 10) {
            for (let index = 0; index < tot_limit; index++) {
                Vue_HotelListing.hotelList.push(Vue_HotelListing.filter_result[index]);
            }
        }
    }
}
/**
 * used for binding hotel results according to the search criteria
 */
function fetchSearchResults() {
    if (Vue_HotelListing.filter_result.length > 0) {

        tot_limit = Vue_HotelListing.filter_result.length;
        if (tot_limit >= 10) {
            for (let index = Vue_HotelListing.startLimit; index < Vue_HotelListing.limit; index++) {
                Vue_HotelListing.hotelList.push(Vue_HotelListing.filter_result[index]);
            }
            Vue_HotelListing.startLimit = Vue_HotelListing.limit;
            Vue_HotelListing.limit = Vue_HotelListing.limit + 10;
        } else if (tot_limit < 10) {
            for (let index = Vue_HotelListing.startLimit; index < tot_limit; index++) {
                Vue_HotelListing.hotelList.push(Vue_HotelListing.filter_result[index]);
            }
        }
    }
}

function filterHotelNameAndLocation() {

    if (Vue_HotelListing.hotelSearchKeyName.length > 0) {

        for (var i = 0; i < Vue_HotelListing.total_result.length; i++) {
            if (Vue_HotelListing.total_result[i].name.toLowerCase().includes(Vue_HotelListing.hotelSearchKeyName) || Vue_HotelListing.total_result[i].name.toUpperCase().includes(Vue_HotelListing.hotelSearchKeyName)) {
                Vue_HotelListing.filter_result.push(Vue_HotelListing.total_result[i]);
            }
        }
    }
    if (Vue_HotelListing.hotelSearchKeyLocation.length > 0) {

        if (Vue_HotelListing.hotelSearchKeyName.length > 0) {
            var tempList = Vue_HotelListing.filter_result;
            for (var i = 0; i < tempList.length; i++) {
                if (tempList[i].location.address.toLowerCase().includes(Vue_HotelListing.hotelSearchKeyLocation) || tempList[i].location.address.toUpperCase().includes(Vue_HotelListing.hotelSearchKeyLocation)) {
                    Vue_HotelListing.filter_result.push(tempList[i]);
                }
            }
        } else {
            for (var i = 0; i < Vue_HotelListing.total_result.length; i++) {
                if (Vue_HotelListing.total_result[i].location.address.toLowerCase().includes(Vue_HotelListing.hotelSearchKeyLocation) || Vue_HotelListing.total_result[i].location.address.toUpperCase().includes(Vue_HotelListing.hotelSearchKeyLocation)) {
                    Vue_HotelListing.filter_result.push(Vue_HotelListing.total_result[i]);
                }
            }
        }

    }

}

/**
 * Used for binding hotel results without any search criteria
 */
function fetchAllResults() {
    if (Vue_HotelListing.total_result.length > 0) {
        tot_limit = Vue_HotelListing.total_result.length;
        if (tot_limit <= 10) {
            for (let index = Vue_HotelListing.startLimit; index < tot_limit; index++) {
                Vue_HotelListing.hotelList.push(Vue_HotelListing.total_result[index]);
            }
        } else {
            for (let index = Vue_HotelListing.startLimit; index < Vue_HotelListing.limit; index++) {
                Vue_HotelListing.hotelList.push(Vue_HotelListing.total_result[index]);
            }
            Vue_HotelListing.startLimit = Vue_HotelListing.limit;
            Vue_HotelListing.limit = Vue_HotelListing.limit + 10;
        }
    }
}
/**
 * Sort By Price
 */
function comparePrice(a, b) {

    if (Vue_HotelListing.sortOrder) { //ASC
        if (parseFloat(a.rate) < parseFloat(b.rate))
            return -1;
        if (parseFloat(a.rate) > parseFloat(b.rate))
            return 1;
        return 0;
    } else { //DSC
        if (parseFloat(a.rate) > parseFloat(b.rate))
            return -1;
        if (parseFloat(a.rate) < parseFloat(b.rate))
            return 1;
        return 0;
    }

}
/**
 * Sort By Name
 */
function compareName(a, b) {

    if (Vue_HotelListing.sortOrder) { //ASC

        var nameA = a.name.toUpperCase(); // ignore upper and lowercase
        var nameB = b.name.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }

        // names must be equal
        return 0;

    } else { //DSC
        var nameA = a.name.toUpperCase(); // ignore upper and lowercase
        var nameB = b.name.toUpperCase(); // ignore upper and lowercase
        if (nameA > nameB) {
            return -1;
        }
        if (nameA < nameB) {
            return 1;
        }

        // names must be equal
        return 0;
    }

}
/**
 * Sort By Star
 */
function compareStar(a, b) {

    if (Vue_HotelListing.sortOrder) { //ASC
        if (parseInt(a.starRating) < parseInt(b.starRating))
            return -1;
        if (parseInt(a.starRating) > parseInt(b.starRating))
            return 1;
        return 0;
    } else { //DSC
        if (parseInt(a.starRating) > parseInt(b.starRating))
            return -1;
        if (parseInt(a.starRating) < parseInt(b.starRating))
            return 1;
        return 0;
    }

}
$(window).scroll(function () {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {

        if (!Vue_HotelListing.isFiltered) {

            if (Vue_HotelListing.limit < Vue_HotelListing.total_result.length) {

                for (let index = Vue_HotelListing.startLimit; index < Vue_HotelListing.limit; index++) {
                    Vue_HotelListing.hotelList.push(Vue_HotelListing.total_result[index]);
                }
                Vue_HotelListing.startLimit = Vue_HotelListing.limit;
                Vue_HotelListing.limit = Vue_HotelListing.limit + 10;

            } else if (Vue_HotelListing.hotelList.length < Vue_HotelListing.total_result.length) {
                Vue_HotelListing.limit = Vue_HotelListing.total_result.length;
                for (let index = Vue_HotelListing.startLimit; index < Vue_HotelListing.limit; index++) {
                    Vue_HotelListing.hotelList.push(Vue_HotelListing.total_result[index]);
                }
            }

        } else {
             
            if (Vue_HotelListing.limit < Vue_HotelListing.filter_result.length) {
                for (let index = Vue_HotelListing.startLimit; index < Vue_HotelListing.limit; index++) {
                    Vue_HotelListing.hotelList.push(Vue_HotelListing.filter_result[index]);
                }
                Vue_HotelListing.startLimit = Vue_HotelListing.limit;
                Vue_HotelListing.limit = Vue_HotelListing.limit + 10;

            } else if (Vue_HotelListing.hotelList.length < Vue_HotelListing.filter_result.length) {
                Vue_HotelListing.limit = Vue_HotelListing.filter_result.length;
                for (let index = Vue_HotelListing.startLimit; index < Vue_HotelListing.limit; index++) {
                    Vue_HotelListing.hotelList.push(Vue_HotelListing.filter_result[index]);
                }
            }
            console.log(Vue_HotelListing.hotelList.length);
            console.log(Vue_HotelListing.filter_result.length);
        }

    }
});