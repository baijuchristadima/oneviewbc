Vue.component("services-links", {
  props: ["currentTab"],
  template: "#services-links-template"
})

Vue.component("flight-search", {
  mounted: function () {
    pack();
    JQueryEvents();
  },
  template: "#flight-search-template"
})

Vue.component("hotel-search", {
  mounted: function () {
    pack();
  },
  data() {
    return {
      checkInDate: {
        to: new Date(new Date().setDate(new Date().getDate() - 1))
      },
      checkOutDate: {
        to: new Date(new Date().setDate(new Date().getDate() - 1))
      }
    };
  },
  components: {
    vuejsDatepicker
  },
  template: "#hotel-search-template"
})

Vue.component("flight-hotel-search", {
  mounted: function () {
    pack();
    JQueryEvents();
  },
  template: "#flight-hotel-search-template"
})

Vue.component("holiday-search", {
  mounted: function () {
    pack();
    JQueryEvents();
  },
  template: "#holiday-search-template"
})

const vm = new Vue({
  el: '#searchdiv',
  data: {
    currentTab: 'flight'
  },
  computed: {
    currentTabComponent: function () {
      return this.currentTab.toLowerCase() + '-search'
    }
  },
  methods: {
    updateDiv: function (tab) {
      this.currentTab = tab;
    }
  }


})