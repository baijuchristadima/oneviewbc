var finalResult = [];
var roomsSelected = '&room1=ADT_1,CHD_0';
//var elasticsearchHost = '03d68b6206bd4dca96b7ea9639f281cb.eu-west-1.aws.found.io';
// var requestStatus = false;

// $(document).ready(function () {
//     $(".showHideButton6").click(function () {
//         $("#showHide6").slideToggle(500);
//         if ($(this).text() == 'Show') {
//             $(this).text('Hide');
//         } else {
//             $(this).text('Show');
//         }
//     });
// });+1
/**Auto Complete Component Start* */

var citySearch = Vue.component('citySearch', {
  data() {
    return {
      isCompleted: false,
      keywordSearch: "",
      autoCompleteResult: []
    }
  },
  methods: {
    onKeyUpAutoCompleteEvent:
      _.debounce(function (keywordEntered) {
        var self = this;
        if (keywordEntered.length > 2) {
          this.isCompleted = true;
          //for supplier list
          var supcitycodes = [];
          var agencyNode = window.localStorage.getItem("User");
          if (agencyNode) {
            agencyNode = JSON.parse(agencyNode);
            var servicesList = agencyNode.loginNode.servicesList;
            for (var i = 0; i < servicesList.length; i++) {
              if (servicesList[i].name == "Hotel" && servicesList[i].provider.length > 0) {
                for (var j = 0; j < servicesList[i].provider.length; j++) {
                  supcitycodes.push(servicesList[i].provider[j].id);
                }
                break;
              } else if (servicesList[i].name == "Hotel" && servicesList[i].provider.length == 0) {
                // no supplier
              }
            }
          }
          var cityName = keywordEntered;
          var cityarray = cityName.split(' ');
          var uppercaseFirstLetter = "";
          for (var k = 0; k < cityarray.length; k++) {
            uppercaseFirstLetter += cityarray[k].charAt(0).toUpperCase() + cityarray[k].slice(1).toLowerCase()
            if (k < cityarray.length - 1) { uppercaseFirstLetter += " "; }
          }
          uppercaseFirstLetter = "*" + uppercaseFirstLetter + "*";
          var lowercaseLetter = "*" + cityName.toLowerCase() + "*";
          var uppercaseLetter = "*" + cityName.toUpperCase() + "*";

          var supplier_type = ["Test", "Both"];
          var should =
          {
            bool: {
              must: [
                {
                  terms: { supplier_id: supcitycodes }
                },
                {
                  terms: { supplier_type: supplier_type }
                }
              ]
            }
          };
          var query =
          {
            query: {
              bool: {
                must: [
                  {
                    bool: {
                      should: [
                        {
                          wildcard: { supplier_city_name: uppercaseFirstLetter }
                        },
                        {
                          wildcard: { supplier_city_name: uppercaseLetter }
                        },
                        {
                          wildcard: { supplier_city_name: lowercaseLetter }
                        }
                      ]
                    }
                  },
                  {
                    bool:
                    {
                      should
                    }
                  }

                ]
              }
            }
          };

          var client = new elasticsearch.Client({
            host: [
              {
                host: ServiceUrls.elasticSearch.elasticsearchHost,
                auth: ServiceUrls.elasticSearch.auth,
                protocol: ServiceUrls.elasticSearch.protocol,
                port: ServiceUrls.elasticSearch.port
              }
            ]
            , log: 'trace'
          });

          client.search({ index: 'city_map', size: 150, body: query }).then(function (resp) {
            finalResult = [];
            var hits = resp.hits.hits;
            var Citymap = new Map();
            for (var i = 0; i < hits.length; i++) {
              Citymap.set(hits[i]._source.oneview_city_id, hits[i]._source);
            }
            var get_values = Citymap.values();
            var Cityvalues = [];
            for (var ele of get_values) {
              Cityvalues.push(ele);
            }
            var results = SortInputFirst(cityName, Cityvalues);
            for (var i = 0; i < results.length; i++) {
              finalResult.push({
                "code": results[i].oneview_city_id,
                "label": results[i].supplier_city_name + ", " + results[i].oneview_country_name,
                "location": results[i].location,
                "countryCode": results[i].oneview_country_code
              });
            }

            var newData = [];
            finalResult.forEach(function (item, index) {
              if (item.label.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {

                newData.push(item);
              }
            });
            console.log(newData);
            self.isCompleted = false;
            self.autoCompleteResult = newData;
            if (hits.length === 0) {
              Vue_HotelListing.city_code = "";
            }
          })

        } else {
          this.isCompleted = false;
          this.autoCompleteResult = [];
        }
      }, 100),
    updateResults: function (label, code, location, countryCode) {
      this.keywordSearch = label;
      this.autoCompleteResult = [];
      this.$emit('city-search-completed', code, label);
      window.sessionStorage.setItem("cityLocation", JSON.stringify(location));
      window.sessionStorage.setItem("countryCode", countryCode);

    }
  },
  props: {
    cityLabel: String,
    cityValue: String
  },
  mounted: function () {
    this.keywordSearch = this.cityValue;
  },
  template: `
      <div class="autocomplete">
      <input type="text" id="txtCitySearch" :placeholder="cityLabel" v-model="keywordSearch" class="form-control" :class="{ 'loading-circle' : (keywordSearch && keywordSearch.length > 2), 'hide-loading-circle': autoCompleteResult.length > 0 || autoCompleteResult.length == 0 && !isCompleted  }" @keyup="onKeyUpAutoCompleteEvent(keywordSearch)" autocomplete="off"/>
      <ul class="autocomplete-results" v-if="autoCompleteResult.length > 0">
          <li class="autocomplete-result" v-for="(item,index) in autoCompleteResult" :key="index" @click="updateResults(item.label, item.code, item.location, item.countryCode)">
              {{ item.label }}
          </li>
      </ul>
      </div>
  `
});
/**Auto Complete Component End* */

/**Room Iteration Component Start* */
var roomsIteration = Vue.component('todo-item', {

  props: {
    value: {
      default: ''
    },
    guestPerRoom: Array,
    roomLabel: String,
    adultLabel: String,
    childLabel: String,
    childrenLabel: String,
    yearsLabel: String

  },
  created() {
    this.selected_adult = this.guestPerRoom ? this.guestPerRoom[0] : 1;
    this.selected_child = (this.guestPerRoom ? this.guestPerRoom[1] : []).filter(function (e) {
      return e != 0;
    }).length;
    if (this.selected_child > 0) {
      this.okage = true;
      this.ok = true;
      this.selected_childAge1 = this.guestPerRoom[1][0];
      if (this.selected_child == 2) {
        this.okplus = true;
        this.selected_childAge2 = this.guestPerRoom[1][1];
      }
    }
  },

  template: `<div><div class="travelDropdown"> 
      <div class="section-pass"> 
      <div class="room-title">{{roomLabel}} {{value}}</div> <fieldset> 
      <label>{{adultLabel}}</label> <div class="trvFld"><span class="custom-select-v3 sm">
       <select :id="'adult_' + value"  class="elmAdult _apxSelection" v-model="selected_adult"><option   v-for="adult in adults" :value="adult.value">{{adult.text}}</option> </select></span></div> 
       <span class="ageRange">(12+ {{yearsLabel}})</span> </fieldset> </div> <div class="section-pass"> <fieldset> <label>{{childLabel}}</label> <div  class="trvFld">
       <span class="custom-select-v3 sm"> <select :id="'child_' + value" class="elmChild _apxSelection"  v-on:change="onChange" v-model="selected_child"><option   v-for="child in children"  :value="child.value">{{child.text}}</option> </select> </span></div> 
       <span class="ageRange">(2-12 {{yearsLabel}})</span> </fieldset> </div> <div class="section-pass"> <fieldset>
        <label v-show="okage">{{childrenLabel}}</label> <div  class="trvFld" v-show="ok"><span class="custom-select-v3 sm"> <select :id="'childage_' + value +'_1'" class="elmChild _apxSelection" v-model="selected_childAge1"><option   v-for="childAge1 in childrenAges1" :value="childAge1.value">{{childAge1.text}}</option> </select> </span></div> 
        <div  class="trvFld" v-show="okplus"><span class="custom-select-v3 sm"> <select :id="'childage_' + value +'_2'" class="elmChild _apxSelection" v-model="selected_childAge2"><option   v-for="childAge2 in childrenAges2" :value="childAge2.value">{{childAge2.text}}</option> </select> </span>
        </div> </fieldset> </div> </div> </div>`,
  data: function () {
    return {
      adults: [

        { 'value': 1, 'text': '1' },
        { 'value': 2, 'text': '2' },
        { 'value': 3, 'text': '3' },
        { 'value': 4, 'text': '4' },
        { 'value': 5, 'text': '5' },
        { 'value': 6, 'text': '6' },
        { 'value': 7, 'text': '7' }
      ],
      children: [
        { 'value': 0, 'text': '0' },
        { 'value': 1, 'text': '1' },
        { 'value': 2, 'text': '2' },

      ],
      childrenAges1: [
        { 'value': 1, 'text': '1' },
        { 'value': 2, 'text': '2' },
        { 'value': 3, 'text': '3' },
        { 'value': 4, 'text': '4' },
        { 'value': 5, 'text': '5' },
        { 'value': 6, 'text': '6' },
        { 'value': 7, 'text': '7' },
        { 'value': 8, 'text': '8' },
        { 'value': 9, 'text': '9' },
        { 'value': 10, 'text': '10' },
        { 'value': 11, 'text': '11' },
        { 'value': 12, 'text': '12' }
      ],
      childrenAges2: [
        { 'value': 1, 'text': '1' },
        { 'value': 2, 'text': '2' },
        { 'value': 3, 'text': '3' },
        { 'value': 4, 'text': '4' },
        { 'value': 5, 'text': '5' },
        { 'value': 6, 'text': '6' },
        { 'value': 7, 'text': '7' },
        { 'value': 8, 'text': '8' },
        { 'value': 9, 'text': '9' },
        { 'value': 10, 'text': '10' },
        { 'value': 11, 'text': '11' },
        { 'value': 12, 'text': '12' }
      ],
      selected_adult: 1,
      selected_child: 0,
      selected_childAge1: 1,
      selected_childAge2: 1,
      ok: false,
      okplus: false,
      okage: false

    }

  },

  methods: {
    onChange: function (event) {
      if (event.target.value == 1) { this.ok = true; this.okplus = false; this.okage = true; }
      else if (event.target.value == 2) { this.okplus = true; this.okage = true; this.ok = true; }
      else { this.okplus = false; this.ok = false; this.okage = false; }
    }
  },

})
/**Room Iteration Component End* */

const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})

Vue.prototype.$eventHub = new Vue({
  data: {
    selectedHotel: {}
  }
});

var autoCompleteMap = Vue.component('map-autocomplete', {
  props: {
    latitude: Number,
    longitude: Number,
    directive: String,
    locationName: String,
    directionsLabel: String
  },

  data() {
    return {
      search: "",
      results: [],
      isOpen: false,
      isLoading: false
    };
  },

  methods: {
    onChange: _.debounce(function () {
      this.isLoading = true;
      this.isOpen = true;
      var rqUrl = ServiceUrls.hubConnection.proxyUrl + "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + this.search + "&location=" + this.latitude + "," + this.longitude + "&radius=50000&strictbounds&key=AIzaSyBoOq8WuZ48t8uO4_XK_ACLJyIsOLX0F58";
      axios.get(rqUrl, {
        headers: { 'content-type': 'application/json' }
      }).then(response => {
        console.log(response.data);
        this.isLoading = false;

        this.results = _.map(response.data.predictions, function (results) { return { name: results.structured_formatting.main_text, id: results.place_id } });
      })

    }, 500),
    setResult: function (result) {
      var vm = this;
      vm.search = result.name;
      vm.isOpen = false;
      var rqUrl = ServiceUrls.hubConnection.proxyUrl + "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + result.id + "&fields=geometry&key=AIzaSyBoOq8WuZ48t8uO4_XK_ACLJyIsOLX0F58";

      axios.get(rqUrl, {
        headers: { 'content-type': 'application/json' }
      }).then(response => {
        console.log(response.data);
        var directions = {
          location: {
            latitude: response.data.result.geometry.location.lat,
            longitude: response.data.result.geometry.location.lng
          },
          locationName: result.name,
          directive: vm.directive
        }
        vm.$eventHub.$emit('select-directions', directions);


      })

    },
    handleClickOutside: function (evt) {
      if (!this.$el.contains(evt.target)) {
        this.isOpen = false;
      }
    }
  },
  watch: {
    locationName: function () {
      this.search = this.locationName;
    }
  },
  mounted() {
    document.addEventListener('click', this.handleClickOutside)
  },
  destroyed() {
    document.removeEventListener('click', this.handleClickOutside)
  },
  template: `
    <div class="autocomplete">
      <input type="text" class="txt_direction1 desti_place" :placeholder="directionsLabel" 
      @input="onChange" v-model="search"/>
      <ul id="autocomplete-results" v-show="isOpen" class="autocomplete-results">
        <li class="loading" v-if="isLoading">Loading results...</li>
        <li v-else v-for="(result, i) in results" :key="i" @click="setResult(result)" 
          class="autocomplete-result">
          {{ result.name }}
        </li>
      </ul>
    </div>
  `
});

var infoWindowButton = Vue.extend({
  i18n,
  methods: {
    infoWindowButtonClicked: function () {
      this.$eventHub.$emit('select-hotel-map', this.hotelCode);
    },
    imgUrlAlt: function (event) {
      event.target.src = "/assets/images/no-image.png"
    },
    directionsFrom: function (location, locationName) {
      var directions = {
        location: location,
        locationName: locationName,
        directive: "from"
      }
      this.$eventHub.$emit('select-directions', directions);
    },
    directionsTo: function (location, locationName) {
      var directions = {
        location: location,
        locationName: locationName,
        directive: "to"
      }
      this.$eventHub.$emit('select-directions', directions);
    }
  },
  props: {
    imageUrl: String,
    hotelName: String,
    currency: String,
    rate: String,
    starRating: String,
    hotelCode: String,
    location: Object,
    selectedCurrency: String,
    currencyMultiplier: Number,
    hideDirectioBtn: Boolean
  },
  template: `
    <div class="map_details">
      <div class="hotel_img"><img :src="imageUrl||'/assets/images/no-image.png'" alt="noimage" @error="imgUrlAlt"></div>
        <div class="hotel_dis">
        <h3>{{hotelName}}
            <span class="float-star">
              <span
                v-for="(item, index) in 5"
                :key="index"
                :class="{checked : index < parseInt(starRating)}"
                class="stars fa fa-star"
              ></span>
            </span>
        </h3>
        <div class="hotel_location"><i class="fa fa-map-marker sp01" aria-hidden="true"></i>
        <span>{{location.address}}</span></div>
        <div class="direction_btn">
           <button v-show="hideDirectioBtn" class="direction_btn1" @click="directionsFrom(location, hotelName)">Directions From</button>
           <button v-show="hideDirectioBtn" class="direction_btn2" @click="directionsTo(location, hotelName)">Directions To</button>
        </div>
        <div class="clearfix"></div>
        <div>
          <div class="map_rate"><span>{{ $n((rate/currencyMultiplier), 'currency', selectedCurrency) }}</span></div>
          <button class="map_hotel_select_btn" @click="infoWindowButtonClicked">Select</button>
        </div>
      </div>
    </div>
  `
});

var hotelListMap = Vue.extend({
  i18n,
  methods: {
    hotelMapDetailsEnter: function (marker) {
      marker.setAnimation(google.maps.Animation.BOUNCE);
    },
    hotelMapDetailLeave: function (markers, index) {
      markers[index].marker.setAnimation(null);
    },
    hotelMapDetailsButton: function (hotel, latitude, longitude) {
      var vm = this;

      var marker;
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: new google.maps.LatLng(latitude, longitude),
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      var infowindow = new google.maps.InfoWindow();
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(latitude, longitude),
        icon: "/Hotels/HotelComponents/assets/images/marker_icon.png",
        map: map
      });

      // google.maps.event.addListener(marker, 'click', (function () {
      //   return function () {
      //     vm.$eventHub.$emit('select-hotel-map', hotel.hotelCode);
      //   };
      // })(marker));



      google.maps.event.addListener(marker, 'mouseover', (function (marker) {
        return function () {
          infowindow.setContent(new infoWindowButton({
            propsData: {
              imageUrl: hotel.imageUrl,
              hotelName: hotel.name,
              currency: hotel.currency,
              rate: hotel.rate,
              starRating: hotel.starRating,
              hotelCode: hotel.hotelCode,
              location: hotel.location,
              selectedCurrency: vm.selectedCurrency,
              currencyMultiplier: vm.currencyMultiplier,
              hideDirectioBtn: false
            }
          }).$mount().$el);
          infowindow.open(map, marker);
        };
      })(marker));

      // assuming you also want to hide the infowindow when user mouses-out

      // google.maps.event.addListener(marker, 'mouseout', (function () {
      //   return function () {
      //     infowindow.close();
      //   };
      // })(marker));

      vm.$eventHub.$emit('select-poi', {
        hotel: hotel.name,
        latitude: latitude,
        longitude: longitude
      });

      var directions = {
        location: hotel.location,
        locationName: hotel.name,
        directive: "from"
      }
      vm.$eventHub.$emit('select-directions', directions);
    },
    imgUrlAlt: function (event) {
      event.target.src = "/assets/images/no-image.png"
    }
  },
  props: {
    hotels: Array,
    selectedCurrency: String,
    currencyMultiplier: Number,
    markers: Array
  },
  template: `
  <div id="map_list_parent">
    <div id="map_list"> 
      <ul class="content">
        <li class="hotel_view_area_map" v-for="(item, index) in hotels" :key="index" @mouseleave="hotelMapDetailLeave(markers, index)" @mouseenter="hotelMapDetailsEnter(markers[index].marker)" @click="hotelMapDetailsButton(item, item.location.latitude, item.location.longitude)">
          <div class="list_htlImg"><img :src="item.imageUrl||'/assets/images/no-image.png'" alt="hotels" @error="imgUrlAlt"></div>
          <h2>{{item.name}}</h2><h3><span>{{ $n((item.rate/currencyMultiplier), 'currency', selectedCurrency) }}</span></h3>
          <div style="color:#FEB810;">
            <h6>
              <span class="float-star">
                <span
                  v-for="(itemStar, index) in 5"
                  :key="index"
                  :class="{checked : index < parseInt(item.starRating)}"
                  class="stars fa fa-star"
                ></span>
              </span>
            </h6>
          </div>
        </li>
      </ul>
    </div>
  </div>
  `
});


var hotelsMap = Vue.component("hotels-map", {
  components: {
    hotelListMap
  },
  props: {
    hotels: Array,
    hideMap: Function,
    selectedCurrency: String,
    currencyMultiplier: Number,
    mapFilterByHotelName: String,
    mapGetDirection: String,
    mapDirectionFrom: String,
    mapDirectionTo: String,
    mapSelectButton: String
  },
  mounted() {
    this.getMap();
  },
  methods: {
    getMap: function () {
      var vm = this;
      var lat = "90";
      var lon = "-90";
      try {
        lat = this.citySearchLocation.lat;
        lon = this.citySearchLocation.lon;
      } catch (error) { }

      var element = document.getElementById("map");
      var options = {
        zoom: 11,
        center: new google.maps.LatLng(lat, lon),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false
      };
      var map = new google.maps.Map(element, options);

      var infowindow = new google.maps.InfoWindow();
      var markers = [];
      var mapInfoHtml = [];
      for (var i = 0; i < this.filteredHotel.length; i++) {

        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(this.filteredHotel[i].location.latitude, this.filteredHotel[i].location.longitude),
          icon: "/Hotels/HotelComponents/assets/images/marker_icon.png",
          map: map
        });
        markers.push({
          marker: marker,
          index: i
        });
        mapInfoHtml[i] = new infoWindowButton({
          propsData: {
            imageUrl: this.filteredHotel[i].imageUrl,
            hotelName: this.filteredHotel[i].name,
            currency: this.filteredHotel[i].currency,
            rate: this.filteredHotel[i].rate,
            starRating: this.filteredHotel[i].starRating,
            hotelCode: this.filteredHotel[i].hotelCode,
            location: this.filteredHotel[i].location,
            selectedCurrency: this.selectedCurrency,
            currencyMultiplier: this.currencyMultiplier,
            hideDirectioBtn: true
          }
        }).$mount().$el;

        // google.maps.event.addListener(marker, 'click', (function (marker, i) {
        //   return function () {
        //     vm.$eventHub.$emit('select-hotel-map', vm.filteredHotel[i].hotelCode);
        //   };
        // })(marker, i));

        // google.maps.event.addListener(marker, 'click', (function (marker, i) {
        //   return function () {
        //     infowindow.setContent(mapInfoHtml[i]);
        //     infowindow.open(map, marker);
        //   };
        // })(marker, i));

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
          return function () {
            infowindow.setContent(mapInfoHtml[i]);
            infowindow.open(map, marker);
          };
        })(marker, i));

        // // assuming you also want to hide the infowindow when user mouses-out

        // google.maps.event.addListener(marker, 'mouseout', (function (marker, i) {
        //   return function () {
        //     infowindow.close();
        //   };
        // })(marker, i));
      }

      this.gmapMarkers = markers;
      this.isSingleHotelOnMap = false;
      this.showPOI = false;
      this.showPrintMap = false;
      this.showDirections = false;
      this.showFilter = true;
      this.showDirectionsList = false;

    },
    selectDirections: function (directions) {
      if (directions.directive == "to") {
        this.directionTo.locationName = directions.locationName;
        this.directionTo.latitude = parseFloat(directions.location.latitude);
        this.directionTo.longitude = parseFloat(directions.location.longitude);
      } else {
        this.directionFrom.locationName = directions.locationName;
        this.directionFrom.latitude = parseFloat(directions.location.latitude);
        this.directionFrom.longitude = parseFloat(directions.location.longitude);
      }
      this.showDirections = true;
    },
    getDirections: function () {
      var vm = this;
      this.$nextTick(function () {
        // Code that will run only after the
        // entire view has been rendered
        var directionsDisplay = new google.maps.DirectionsRenderer({ preserveViewport: true });
        var directionsService = new google.maps.DirectionsService;
        var start = new google.maps.LatLng(this.directionFrom.latitude, this.directionFrom.longitude);
        var end = new google.maps.LatLng(this.directionTo.latitude, this.directionTo.longitude);

        var mapOptions = {
          mapTypeControl: false,
          zoom: 14,
          center: new google.maps.LatLng(this.directionFrom.latitude, this.directionFrom.longitude)
        };

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);
        directionsDisplay.setMap(map);

        var tempComponent = Vue.extend({
          mounted: function () {
            this.$nextTick(function () {
              directionsDisplay.setPanel(document.getElementById('tempHtml'));
            })
          },
          template: '<div id="tempHtml"></div>'
        })

        var htmlTemp = new tempComponent().$mount();

        var node = document.getElementById('directions_list');
        while (node.hasChildNodes()) {
          node.removeChild(node.firstChild);
        }
        node.appendChild(htmlTemp.$el);

        this.showPrintMap = true;
        this.showDirectionsList = true;

        var request = {
          origin: start,
          destination: end,
          // Note that Javascript allows us to access the constant
          // using square brackets and a string value as its
          // "property."
          travelMode: google.maps.TravelMode[this.selectedMode]
        };
        directionsService.route(request, function (response, status) {

          if (status == 'OK') {
            directionsDisplay.setDirections(response);
            vm.showFilter = false;
          }
        });

      });
    },
    printMap: function () {

      const $body = $('body');
      const $mapContainer = $('.map-container');
      var divToPrint = document.getElementById('directions_list');
      const $mapContainerParent = $mapContainer.parent();
      const $printContainer = $('<div style="position:relative;">' + divToPrint.innerHTML);

      $printContainer.height($mapContainer.height()).prepend($mapContainer).prependTo($body);

      const $content = $body.children().not($printContainer).not('script').detach();

      /**
       * Needed for those who use Bootstrap 3.x, because some of
       * its `@media print` styles ain't play nicely when printing.
       */
      const $patchedStyle = $('<style media="print">')
        .text('img { max-width: none !important; } a[href]:after { content: ""; }')
        .appendTo('head');

      window.print();

      $body.prepend($content);
      $mapContainerParent.prepend($mapContainer);

      $printContainer.remove();
      $patchedStyle.remove();
    },
    selectPoi: function (data) {
      this.isSingleHotelOnMap = true;
      this.poiData = data;
    },
    getMapPOI: function () {
      vm = this;
      vm.showFilter = false;
      vm.showPOI = true;
      vm.showDirectionsList = false;

      var locations = ['airport', 'restaurant', 'shopping_mall', 'amusement_park', 'department_store'];
      for (var j = 0; j < locations.length; j++) {

        var center = new google.maps.LatLng(vm.poiData.latitude, vm.poiData.longitude);

        var map = new google.maps.Map(document.getElementById('mapdummy'), {
          center: center,
          mapTypeControl: false,
          zoom: 15
        });

        var request = {
          location: center,
          radius: 500,
          type: [locations[j]]
        };

        var service = new google.maps.places.PlacesService(map);

        service.nearbySearch(request, function (results, status, pagination) {
          if (status == google.maps.places.PlacesServiceStatus.OK) {

            var tempComponent = Vue.extend({
              data: function () {
                return {
                  poiService: null,
                  airport: results.filter(function (e) { return e.types[0] == 'airport' }),
                  restaurant: results.filter(function (e) { return e.types[0] == 'restaurant' }),
                  establishment: results.filter(function (e) { return e.types[0] == 'establishment' || e.types[0] == 'department_store' }),
                  amusementPark: results.filter(function (e) { return e.types[0] == 'amusement_park' }),
                  shoppingMall: results.filter(function (e) { return e.types[0] == 'shopping_mall' })
                }
              },
              methods: {
                showCertainPOI: function (service) {
                  if (this.poiService == service) {
                    this.poiService = null;
                  } else {
                    this.poiService = service;
                  }
                },
                selectPoi: function (place) {
                  var directions = {
                    location: {
                      latitude: place.geometry.location.lat(),
                      longitude: place.geometry.location.lng()
                    },
                    locationName: place.name,
                    directive: "to"
                  }
                  this.$eventHub.$emit('select-directions', directions);
                }
              },
              template: `
              <ul id="nav">
                <li v-if="airport.length>0">
                  <a href="#" @click.prevent="showCertainPOI('airport')">Airports<i class="fa fa-angle-down"></i></a>
                  <transition name="slide">
                    <ul v-show="poiService=='airport'">
                      <li v-for="(place, index) in airport" :key="index"><a href="#" @click.prevent="selectPoi(place)">{{place.name}}</a></li>
                    </ul>
                  </transition>
                </li>
                <li v-if="restaurant.length>0">
                  <a href="#" @click.prevent="showCertainPOI('locality')">Restaurant<i class="fa fa-angle-down"></i></a>
                  <transition name="slide">
                    <ul v-show="poiService=='locality'">
                      <li v-for="(place, index) in restaurant" :key="index"><a href="#" @click.prevent="selectPoi(place)">{{place.name}}</a></li>
                    </ul>
                    </transition>
                </li>
                <li v-if="establishment.length>0">
                  <a href="#" @click.prevent="showCertainPOI('premise')">Establishment<i class="fa fa-angle-down"></i></a>
                  <transition name="slide">
                    <ul v-show="poiService=='premise'">
                      <li v-for="(place, index) in establishment" :key="index"><a href="#" @click.prevent="selectPoi(place)">{{place.name}}</a></li>
                    </ul>
                  </transition>
                </li>
                <li v-if="amusementPark.length>0">
                  <a href="#" @click.prevent="showCertainPOI('islands')">Attractions<i class="fa fa-angle-down"></i></a>
                  <transition name="slide">
                    <ul v-show="poiService=='islands'">
                      <li v-for="(place, index) in amusementPark" :key="index"><a href="#" @click.prevent="selectPoi(place)">{{place.name}}</a></li>
                    </ul>
                  </transition>
                </li>
                <li v-if="shoppingMall.length>0">
                  <a href="#" @click.prevent="showCertainPOI('shoppingMalls')">Shopping Malls<i class="fa fa-angle-down"></i></a>
                  <transition name="slide">
                    <ul v-show="poiService=='shoppingMalls'">
                      <li v-for="(place, index) in shoppingMall" :key="index"><a href="#" @click.prevent="selectPoi(place)">{{place.name}}</a></li>
                    </ul>
                  </transition>
                </li>
              </ul>
              `
            })

            var htmlTemp = new tempComponent().$mount();

            var poi_mapNode = document.getElementById('poi_map');
            while (poi_mapNode.hasChildNodes()) {
              poi_mapNode.removeChild(poi_mapNode.firstChild);
            }
            poi_mapNode.appendChild(htmlTemp.$el);
          }
        });
      }


    }
  },
  data: function () {
    return {
      searchHotel: null,
      filteredHotel: this.hotels.filter(function (e) { return e.location.latitude != undefined; }),
      showList: true,
      showFilter: true,
      showPOI: false,
      showPrintMap: false,
      poiData: {
        hotel: null,
        latitude: JSON.parse(window.sessionStorage.getItem("cityLocation")).lat,
        longitude: JSON.parse(window.sessionStorage.getItem("cityLocation")).lon
      },
      showDirections: false,
      showDirectionsList: false,
      isSingleHotelOnMap: false,
      directionTo: {
        locationName: null,
        latitude: null,
        longitude: null
      },
      directionFrom: {
        locationName: null,
        latitude: null,
        longitude: null
      },
      selectedMode: "DRIVING",
      citySearchLocation: JSON.parse(window.sessionStorage.getItem("cityLocation")),
      gmapMarkers: null
    };
  },
  watch: {
    selectedCurrency: function () {
      this.getMap();
    },
    searchHotel: _.debounce(function (newVal, old) {
      this.filteredHotel = this.hotels.filter(hotel => {
        return hotel.name.toLowerCase().includes((newVal || "").toLowerCase())
      });

      this.filteredHotel = this.filteredHotel.filter(function (e) { return e.location.latitude != undefined; });

      this.getMap();

    }, 200)
  },
  created: function () {
    this.$eventHub.$on('select-directions', this.selectDirections);
    this.$eventHub.$on('select-poi', this.selectPoi);
  },
  beforeDestroy() {
    this.$eventHub.$off('select-directions');
    this.$eventHub.$off('select-poi');
  },
  template: `
  <!--Map view-->
  <div class="container-fluid">
    <div class="row">
      <div id="fullviewmap" class="view_map">
        <div class="htl_list">
          <div class="htl_view" @click="showList=false" v-if="showList"><i id="sp_icon" class="fa fa-caret-left"></i></div>
          <div class="htl_view" @click="showList=true" v-if="!showList"><i id="sp_icon" class="fa fa-caret-right"></i></div>
          <div class="web_view" @click="getMap" title="Map"><i id="sp_icon" class="fa fa-bed"></i></div>
          <div class="globe_view" @click="getMapPOI" title="POIs"><i id="sp_icon3" class="fa fa-globe"></i></div>
          <div class="print_view" @click="printMap" v-if="showPrintMap" title="Print"><i id="sp_icon2" class="fa fa-print"></i></div>
          <transition name="hide_list01">
            <div class="htl_list01" v-show="showList">
                <div class="map_filter">
                  <input :disabled="!showFilter" type="text" class="srch_filter" v-model="searchHotel" :placeholder="showFilter?mapFilterByHotelName:''" />
                  <transition name="get_direction">
                  <div class="get_direction">
                    <div class="get_direction_navs">
                      <div class="mapbtn">
                        <input type="radio" id="radio-btn-1" value="DRIVING" checked="checked" name="radio-btns-1" v-model="selectedMode">
                        <label for="radio-btn-1" class="btn"><i class="fa fa-car"></i></label>
                      </div>
                      <div class="mapbtn">
                        <input type="radio" id="radio-btn-2" value="TRANSIT" name="radio-btns-2" v-model="selectedMode">
                        <label for="radio-btn-2" class="btn"><i class="fa fa-bus"></i></label>
                      </div>
                      <div class="mapbtn">
                        <input type="radio" id="radio-btn-3" value="WALKING" name="radio-btns-3" v-model="selectedMode">
                        <label for="radio-btn-3" class="btn"><i class="fa fa-male"></i></label>
                      </div>
                      <!--<a class="hide_direction" @click="showDirections=false"><i class="fa fa-close"></i></a>-->
                      <a @click="getDirections">{{mapGetDirection}}</a>
                    </div>
                    <div class="get_direction_txt">
                      <div class="fm_direction">
                        <map-autocomplete :directions-label="mapDirectionFrom" :latitude="citySearchLocation.lat" :longitude="citySearchLocation.lon" :directive="'from'" :locationName="directionFrom.locationName"></map-autocomplete>
                        <!--<input :value="directionFrom.locationName" type="text" class="txt_direction1 desti_place" placeholder="Directions From" />-->
                        <i class="fa fa-street-view"></i>
                      </div>
                      <div class="line_direction">
                        <i class="fa fa-ellipsis-v"></i>
                      </div>
                      <div class="to_direction">
                        <map-autocomplete :directions-label="mapDirectionTo" :latitude="citySearchLocation.lat" :longitude="citySearchLocation.lon" :directive="'to'" :locationName="directionTo.locationName"></map-autocomplete>
                        <!--<input type="text" :value="directionTo.locationName" class="txt_direction2 desti_place" placeholder="Directions To" />-->
                        <i class="fa fa-map-marker"></i>
                      </div>
                    </div>
                  </div>
                </transition>
                <div class="mode_change" title="Hotels" @click="$emit('hide-map')"><i class="fa fa-list"></i></div>
                <div class="hotel_map_list hotel_map_list_shrink" id="vbar_list" v-if="!showPOI&&!showDirectionsList">
                  <hotel-list-map :hotels="filteredHotel" :selected-currency="selectedCurrency" :currency-multiplier="currencyMultiplier" :markers="gmapMarkers"></hotel-list-map>
                </div>
                <div id="directions_list" class="point_int hotel_map_list_shrink" v-show="showDirectionsList"></div>
                <div class="point_int hotel_map_list_shrink" id="poi_map" v-show="showPOI&&!showDirectionsList"></div>
              </div>
            </div>
          </transition>
        </div>
        <div class="htl_map_view">
          <div id="map" class="map-container"></div>
          <div id="mapdummy" style="display:none"></div>
        </div>
      </div>
    </div>
  </div>
  <!--Map view close-->
  `
});

var Vue_HotelListing = new Vue({
  i18n,
  el: "#hotelListDiv",
  components: {
    hotelsMap: hotelsMap
  },
  data: {
    // cityCode: '',
    //   cityName: '',
    occupancyDates: {
      checkIn: null,
      checkOut: null
    },
    roomDetails: {

    },
    numberOfNights: 0,
    numberOfRooms: 1,
    dspl: false,
    totalcount: "1 Guests,1 Room",
    count: 1,
    todos: [
    ],
    values: {},
    validationMessage: '',
    access_token: '',
    hotelList: [], //shows hotel results
    total_result: [], //hold all supplier results
    supplierMap: new Map(), //hold supplier wise results
    startLimit: 0,
    limit: 10,
    sortOrder: true,
    priceSortOrder: true,
    nameSortOrder: true,
    starSortOrder: true,
    isFiltered: false,
    filter_result: [],
    isPriceActive: true,
    isNameActive: false,
    isStarActive: false,
    hotelSearchKeyName: '',
    hotelSearchKeyLocation: '',
    supplierList: supplierList,
    supplierFilterCategories: [],
    hotelRatingCategories: [],
    lowestPrice: Number.POSITIVE_INFINITY,
    highestPrice: Number.NEGATIVE_INFINITY,
    startPriceValue: 0,
    endPriceValue: 0,
    nationality: '',
    country_of_residence: '',
    countries: [
      { code: "AF", name: "Afghanistan" },
      { code: "AL", name: "Albania" },
      { code: "DZ", name: "Algeria" },
      { code: "AS", name: "American Samoa" },
      { code: "AD", name: "Andorra" },
      { code: "AO", name: "Angola" },
      { code: "AI", name: "Anguilla" },
      { code: "AQ", name: "Antartica" },
      { code: "AG", name: "Antigua And Barbuda" },
      { code: "AR", name: "Argentina" },
      { code: "AM", name: "Armenia" },
      { code: "AW", name: "Aruba" },
      { code: "AU", name: "Australia" },
      { code: "AT", name: "Austria" },
      { code: "AZ", name: "Azerbaijan" },
      { code: "BS", name: "Bahamas" },
      { code: "BH", name: "Bahrain" },
      { code: "BD", name: "Bangladesh" },
      { code: "BB", name: "Barbados" },
      { code: "BY", name: "Belarus" },
      { code: "BE", name: "Belgium" },
      { code: "BZ", name: "Belize" },
      { code: "BJ", name: "Benin" },
      { code: "BM", name: "Bermuda" },
      { code: "BT", name: "Bhutan" },
      { code: "BO", name: "Bolivia" },
      { code: "BQ", name: "Bonaire St Eustatius And Saba " },
      { code: "BA", name: "Bosnia-Herzegovina" },
      { code: "BW", name: "Botswana" },
      { code: "BR", name: "Brazil" },
      { code: "IO", name: "British Indian Ocean Territory" },
      { code: "BN", name: "Brunei Darussalam" },
      { code: "BG", name: "Bulgaria" },
      { code: "BF", name: "Burkina Faso" },
      { code: "BI", name: "Burundi" },
      { code: "KH", name: "Cambodia" },
      { code: "CM", name: "Cameroon-Republic Of" },
      { code: "CB", name: "Canada Buffer" },
      { code: "CA", name: "Canada" },
      { code: "CV", name: "Cape Verde-Republic Of" },
      { code: "KY", name: "Cayman Islands" },
      { code: "CF", name: "Central African Republic" },
      { code: "TD", name: "Chad" },
      { code: "CL", name: "Chile" },
      { code: "CN", name: "China" },
      { code: "CX", name: "Christmas Island" },
      { code: "CC", name: "Cocos Islands" },
      { code: "CO", name: "Colombia" },
      { code: "KM", name: "Comoros" },
      { code: "CG", name: "Congo Brazzaville" },
      { code: "CD", name: "Congo The Democratic Rep Of" },
      { code: "CK", name: "Cook Islands" },
      { code: "CR", name: "Costa Rica" },
      { code: "CI", name: "Cote D Ivoire" },
      { code: "HR", name: "Croatia" },
      { code: "CU", name: "Cuba" },
      { code: "CW", name: "Curacao" },
      { code: "CY", name: "Cyprus" },
      { code: "CZ", name: "Czech Republic" },
      { code: "DK", name: "Denmark" },
      { code: "DJ", name: "Djibouti" },
      { code: "DM", name: "Dominica" },
      { code: "DO", name: "Dominican Republic" },
      { code: "TP", name: "East Timor Former Code)" },
      { code: "EC", name: "Ecuador" },
      { code: "EG", name: "Egypt" },
      { code: "SV", name: "El Salvador" },
      { code: "EU", name: "Emu European Monetary Union" },
      { code: "GQ", name: "Equatorial Guinea" },
      { code: "ER", name: "Eritrea" },
      { code: "EE", name: "Estonia" },
      { code: "ET", name: "Ethiopia" },
      { code: "FK", name: "Falkland Islands" },
      { code: "FO", name: "Faroe Islands" },
      { code: "ZZ", name: "Fictitious Points" },
      { code: "FJ", name: "Fiji" },
      { code: "FI", name: "Finland" },
      { code: "FR", name: "France" },
      { code: "GF", name: "French Guiana" },
      { code: "PF", name: "French Polynesia" },
      { code: "GA", name: "Gabon" },
      { code: "GM", name: "Gambia" },
      { code: "GE", name: "Georgia" },
      { code: "DE", name: "Germany" },
      { code: "GH", name: "Ghana" },
      { code: "GI", name: "Gibraltar" },
      { code: "GR", name: "Greece" },
      { code: "GL", name: "Greenland" },
      { code: "GD", name: "Grenada" },
      { code: "GP", name: "Guadeloupe" },
      { code: "GU", name: "Guam" },
      { code: "GT", name: "Guatemala" },
      { code: "GW", name: "Guinea Bissau" },
      { code: "GN", name: "Guinea" },
      { code: "GY", name: "Guyana" },
      { code: "HT", name: "Haiti" },
      { code: "HN", name: "Honduras" },
      { code: "HK", name: "Hong Kong" },
      { code: "HU", name: "Hungary" },
      { code: "IS", name: "Iceland" },
      { code: "IN", name: "India" },
      { code: "ID", name: "Indonesia" },
      { code: "IR", name: "Iran" },
      { code: "IQ", name: "Iraq" },
      { code: "IE", name: "Ireland-Republic Of" },
      { code: "IL", name: "Israel" },
      { code: "IT", name: "Italy" },
      { code: "JM", name: "Jamaica" },
      { code: "JP", name: "Japan" },
      { code: "JO", name: "Jordan" },
      { code: "KZ", name: "Kazakhstan" },
      { code: "KE", name: "Kenya" },
      { code: "KI", name: "Kiribati" },
      { code: "KP", name: "Korea Dem Peoples Rep Of" },
      { code: "KR", name: "Korea Republic Of" },
      { code: "KW", name: "Kuwait" },
      { code: "KG", name: "Kyrgyzstan" },
      { code: "LA", name: "Lao Peoples Dem Republic" },
      { code: "LV", name: "Latvia" },
      { code: "LB", name: "Lebanon" },
      { code: "LS", name: "Lesotho" },
      { code: "LR", name: "Liberia" },
      { code: "LY", name: "Libya" },
      { code: "LI", name: "Liechtenstein" },
      { code: "LT", name: "Lithuania" },
      { code: "LU", name: "Luxembourg" },
      { code: "MO", name: "Macao -Sar Of China-" },
      { code: "MK", name: "Macedonia -Fyrom-" },
      { code: "MG", name: "Madagascar" },
      { code: "MW", name: "Malawi" },
      { code: "MY", name: "Malaysia" },
      { code: "MV", name: "Maldives Island" },
      { code: "ML", name: "Mali" },
      { code: "MT", name: "Malta" },
      { code: "MH", name: "Marshall Islands" },
      { code: "MQ", name: "Martinique" },
      { code: "MR", name: "Mauritania" },
      { code: "MU", name: "Mauritius Island" },
      { code: "YT", name: "Mayotte" },
      { code: "MB", name: "Mexico Buffer" },
      { code: "MX", name: "Mexico" },
      { code: "FM", name: "Micronesia" },
      { code: "MD", name: "Moldova" },
      { code: "MC", name: "Monaco" },
      { code: "MN", name: "Mongolia" },
      { code: "ME", name: "Montenegro" },
      { code: "MS", name: "Montserrat" },
      { code: "MA", name: "Morocco" },
      { code: "MZ", name: "Mozambique" },
      { code: "MM", name: "Myanmar" },
      { code: "NA", name: "Namibia" },
      { code: "NR", name: "Nauru" },
      { code: "NP", name: "Nepal" },
      { code: "AN", name: "Netherlands Antilles" },
      { code: "NL", name: "Netherlands" },
      { code: "NC", name: "New Caledonia" },
      { code: "NZ", name: "New Zealand" },
      { code: "NI", name: "Nicaragua" },
      { code: "NE", name: "Niger" },
      { code: "NG", name: "Nigeria" },
      { code: "NU", name: "Niue" },
      { code: "NF", name: "Norfolk Island" },
      { code: "MP", name: "Northern Mariana Islands" },
      { code: "NO", name: "Norway" },
      { code: "OM", name: "Oman" },
      { code: "PK", name: "Pakistan" },
      { code: "PW", name: "Palau Islands" },
      { code: "PS", name: "Palestine - State Of" },
      { code: "PA", name: "Panama" },
      { code: "PG", name: "Papua New Guinea" },
      { code: "PY", name: "Paraguay" },
      { code: "PE", name: "Peru" },
      { code: "PH", name: "Philippines" },
      { code: "PL", name: "Poland" },
      { code: "PT", name: "Portugal" },
      { code: "PR", name: "Puerto Rico" },
      { code: "QA", name: "Qatar" },
      { code: "RE", name: "Reunion Island" },
      { code: "RO", name: "Romania" },
      { code: "RU", name: "Russia" },
      { code: "XU", name: "Russia" },
      { code: "RW", name: "Rwanda" },
      { code: "WS", name: "Samoa-Independent State Of" },
      { code: "SM", name: "San Marino" },
      { code: "ST", name: "Sao Tome And Principe Islands " },
      { code: "SA", name: "Saudi Arabia" },
      { code: "SN", name: "Senegal" },
      { code: "RS", name: "Serbia" },
      { code: "SC", name: "Seychelles Islands" },
      { code: "SL", name: "Sierra Leone" },
      { code: "SG", name: "Singapore" },
      { code: "SX", name: "Sint Maarten" },
      { code: "SK", name: "Slovakia" },
      { code: "SI", name: "Slovenia" },
      { code: "SB", name: "Solomon Islands" },
      { code: "SO", name: "Somalia" },
      { code: "ZA", name: "South Africa" },
      { code: "SS", name: "South Sudan" },
      { code: "ES", name: "Spain" },
      { code: "LK", name: "Sri Lanka" },
      { code: "SH", name: "St. Helena Island" },
      { code: "KN", name: "St. Kitts" },
      { code: "LC", name: "St. Lucia" },
      { code: "PM", name: "St. Pierre And Miquelon" },
      { code: "VC", name: "St. Vincent" },
      { code: "SD", name: "Sudan" },
      { code: "SR", name: "Suriname" },
      { code: "SZ", name: "Swaziland" },
      { code: "SE", name: "Sweden" },
      { code: "CH", name: "Switzerland" },
      { code: "SY", name: "Syrian Arab Republic" },
      { code: "TW", name: "Taiwan" },
      { code: "TJ", name: "Tajikistan" },
      { code: "TZ", name: "Tanzania-United Republic" },
      { code: "TH", name: "Thailand" },
      { code: "TL", name: "Timor Leste" },
      { code: "TG", name: "Togo" },
      { code: "TK", name: "Tokelau" },
      { code: "TO", name: "Tonga" },
      { code: "TT", name: "Trinidad And Tobago" },
      { code: "TN", name: "Tunisia" },
      { code: "TR", name: "Turkey" },
      { code: "TM", name: "Turkmenistan" },
      { code: "TC", name: "Turks And Caicos Islands" },
      { code: "TV", name: "Tuvalu" },
      { code: "UM", name: "U.S. Minor Outlying Islands" },
      { code: "UG", name: "Uganda" },
      { code: "UA", name: "Ukraine" },
      { code: "AE", name: "United Arab Emirates" },
      { code: "GB", name: "United Kingdom" },
      { code: "US", name: "United States Of America" },
      { code: "UY", name: "Uruguay" },
      { code: "UZ", name: "Uzbekistan" },
      { code: "VU", name: "Vanuatu" },
      { code: "VA", name: "Vatican" },
      { code: "VE", name: "Venezuela" },
      { code: "VN", name: "Vietnam" },
      { code: "VG", name: "Virgin Islands-British" },
      { code: "VI", name: "Virgin Islands-United States" },
      { code: "WF", name: "Wallis And Futuna Islands" },
      { code: "EH", name: "Western Sahara" },
      { code: "YE", name: "Yemen Republic" },
      { code: "ZM", name: "Zambia" },
      { code: "ZW", name: "Zimbabwe" },
    ],
    city: '',
    city_code: '',
    city_Display: '',
    cin: '',
    cout: '',
    night: '',
    num_room: '',
    guestCount: '',
    guestNumber: '',
    adtCount: '',
    chdCount: '',
    rangeValue: '',
    requestStatus: false,
    isFilterApplied: false,
    NoResult: false,
    NoResultHead: '',
    NoResultDesc: '',
    uuid: '',
    totalResult_Count: 0,
    session_data: [],
    loadingStatus: true,
    weatherData: null,
    weatherImg: 'https://openweathermap.org/img/w/',
    numOfGuestPerRoom: [],
    showMap: false,
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
    CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    CityNameSearch_Label: "",
    LocationRange_Label: "",
    LocationRangeUnit_Label: "",
    RoomSearch_Label: "",
    AdultSearch_Label: "",
    ChildrenSearch_Label: "",
    ChildrenAgesSearch_Label: "",
    NumberOfYears_Label: "",
    RemoveRoomButton_Label: "",
    AddRoomButton_Label: "",
    DoneSearchButton_Label: "",
    Guest_Label: "",
    Rooms_Label: "",
    SearchButton_Label: "",
    Nights_Label: "",
    Dates_Label: "",
    ModifySearch_Label: "",
    HotelsFound_Label: "",
    PriceSorting_Label: "",
    NameSorting_Label: "",
    StarSorting_Label: "",
    SortBy_Label: "",
    Weather_Label: "",
    HotelNameFilter_Label: "",
    PriceFilter_Label: "",
    LocationFilter_Label: "",
    StarFilter_Label: "",
    HotelNameFilter_Placeholder: "",
    LocationFilter_Placeholder: "",
    SelectRoomButton_Label: "",
    AdultCount_Label: "",
    ChildCount_Label: "",
    ChildrenCount_Label: "",
    NightsCount_Label: "",
    NoResultsFound_Label: "",
    NoResultsDesc_Label: "",
    SearchAgainButton_Label: "",
    MapFilterByHotelName_Label: "",
    MapGetDirection_Label: "",
    MapDirectionFrom_Label: "",
    MapDirectionTo_Label: "",
    MapSelectButton_Label: "",
    toggleShowAllStar: false,
    searchRequest: "",
    hasCORandNat: false,
    countValidSupplierResponse: 0,
    requestCount: 1,

  },
  mounted: function () {

    var dateFormat = generalInformation.systemSettings.systemDateFormat;
    var noOfMonths = generalInformation.systemSettings.calendarDisplay;

    if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
      noOfMonths = 1;
    } else if (parseInt($(window).width()) > 999) {
      noOfMonths = 2;
    }


    var sDate = new Date(moment(this.cin, "DD/MM/YYYY")).getTime();
    var eDate = new Date(moment(this.cout, "DD/MM/YYYY")).getTime();


    // if (localStorage.direction == 'rtl') {
    //   $.datepicker.setDefaults($.datepicker.regional["ar"]);
    // } else {
    //   $.datepicker.setDefaults($.datepicker.regional["en-GB"]);
    // }

    //Checkin Date
    $("#checkInId").datepicker({
      dateFormat: dateFormat,
      minDate: 0, // 0 for search
      maxDate: "360d",
      firstDay: 0,
      numberOfMonths: parseInt(noOfMonths),
      onSelect: function (date) {
        var dt2 = $('#CheckOutId');
        var startDate = $(this).datepicker('getDate');
        var minDate = $(this).datepicker('getDate');
        sDate = startDate.getTime();
        startDate.setDate(startDate.getDate() + 1);
        dt2.datepicker('option', 'minDate', startDate);
        dt2.datepicker('setDate', minDate);
        $('.ui-datepicker-current-day').click();
      }, beforeShowDay: function (date) {

        if (date.getTime() == eDate) {
          return [true, 'event event-selected event-selected-right', ''];
        }
        else if (date.getTime() <= eDate && date.getTime() >= $(this).datepicker('getDate').getTime()) {
          if (date.getTime() == $(this).datepicker('getDate').getTime()) {
            return [true, 'event-selected event-selected-left', ''];
          } else {
            return [true, 'event-selected', ''];
          }
        }
        else {
          return [true, '', ''];
        }
      }

    });

    // $('#checkInId').val(moment(startDate).format('DD MMM YYYY,dd'));
    // $("#checkInId").val(moment(this.cin, 'DD/MM/YYYY').format('DD MMM YY,ddd'));
    $("#checkInId").datepicker({ dateFormat: 'DD/MM/YYYY' }).datepicker("setDate", new Date(sDate));

    //Checkout Date
    var vm = this;
    $('#CheckOutId').datepicker({
      dateFormat: dateFormat,
      minDate: new Date(moment(vm.cin, "DD/MM/YYYY").add(1, 'days')),
      maxDate: "360d",
      firstDay: 0,
      numberOfMonths: parseInt(noOfMonths),
      onSelect: function (date) {
        var a = $("#checkInId").datepicker('getDate').getTime();
        var b = $("#CheckOutId").datepicker('getDate').getTime();
        var c = 24 * 60 * 60 * 1000;
        var diffDays = Math.round(Math.abs((a - b) / c));
        if (diffDays > 90) {
          alert('Maximum Nights', 'Maximum 90 nights allowed.');

        } else {

          eDate = $(this).datepicker('getDate').getTime();
        }
      }, beforeShowDay: function (date) {
        if (date.getTime() == sDate) {
          return [true, 'event event-selected event-selected-left', ''];
        }
        else if (date.getTime() >= sDate && date.getTime() <= $(this).datepicker('getDate').getTime()) {
          if (date.getTime() == $(this).datepicker('getDate').getTime()) {
            return [true, 'event-selected event-selected-right', ''];
          } else {
            return [true, 'event-selected', ''];
          }
        }
        else {
          return [true, '', ''];
        }
      }
    });

    // $('#CheckOutId').val(moment(new Date(), 'DD MMM YYYY,dd').add(2, 'days').format('DD MMM YYYY,dd'));
    // $("#CheckOutId").val(moment(this.cout, 'DD/MM/YYYY').format('DD MMM YY,ddd'));
    $("#CheckOutId").datepicker({ dateFormat: 'DD/MM/YYYY' }).datepicker("setDate", new Date(eDate));



    var div = $('.autocomplete');
    div.find(':input').val(this.city);

    //Slider for filtering hotel price
    var slider = document.getElementById('ageSlider');
    noUiSlider.create(slider, {
      start: [1, 1000],
      connect: true,
      step: 1,
      range: {
        'min': 0,
        'max': 1000
      }
    });
    $("#minPricevalue").text(1);
    $("#MaxPricevalue").text(1000);

    //Slider for changing KM radius
    var self = this;
    self.rangeValue = GetParameterValues("KMrange") == "NODATA" ? 10 : GetParameterValues("KMrange");
    var stepSlider = document.getElementById('rangeSlider');

    noUiSlider.create(stepSlider, {
      start: [self.rangeValue],
      step: 1,
      range: {
        'min': [1],
        'max': [10]
      }
    });
    var stepSliderValueElement = document.getElementById('slider-step-value');
    stepSlider.noUiSlider.on('update', function (values, handle) {
      stepSliderValueElement.firstChild.innerText = parseInt(values[handle]);
      self.rangeValue = parseInt(values[handle]);
    });


  },
  created: function () {
    this.getPagecontent();

    this.requestStatus = false;
    this.nationality = GetParameterValues("nationality");
    this.country_of_residence = GetParameterValues("country_of_residence");
    this.city_Display = GetParameterValues("city").replace(/%20/g, " ");
    this.city = GetParameterValues("city").replace(/%20/g, " ");
    this.city_code = GetParameterValues("city_code");
    this.cin = GetParameterValues("cin");
    this.cout = GetParameterValues("cout");
    this.night = GetParameterValues("night");
    this.num_room = GetParameterValues("num_room");
    this.guestCount = GetParameterValues("guest");
    this.adtCount = GetParameterValues("adtCount");
    this.chdCount = GetParameterValues("chdCount");
    this.uuid = GetParameterValues("uuid");
    this.rangeValue = GetParameterValues("KMrange");
    this.hasCORandNat = GetParameterValues("hasCORandNat") == "NODATA" || GetParameterValues("hasCORandNat") == "false" ? false : true;

    this.count = this.num_room;
    var roomCount = this.count;
    for (var index = 1; index <= roomCount; index++) {
      id = 'div_Room_' + index;

      this.todos.push({
        id,
        value: index,
      });

    }
    this.$set(this.values, id, '')

    var numOfGuestPerRoom = [];

    for (var irt = 1; irt <= this.num_room; irt++) {
      var guest = GetParameterValues("room" + irt).split(",");
      var adult = parseInt(guest[0].split("_")[1]);
      var child = guest[1].split("_");
      var child1 = 0;
      var child2 = 0;
      if (child[1] == 1) {
        child1 = parseInt(child[2]);
      }
      if (child[1] == 2) {
        child1 = parseInt(child[2]);
        child2 = parseInt(child[3]);
      }
      numOfGuestPerRoom.push([adult, [child1, child2]]);
    }

    this.numOfGuestPerRoom = numOfGuestPerRoom;

    if (this.numOfGuestPerRoom.length > 1) {
      this.dspl = true;
    }

    this.totalcount = (parseInt(this.chdCount) + parseInt(this.adtCount)) + " " + this.Guest_Label + ", " + this.num_room + " " + this.Rooms_Label;

    this.access_token = localStorage.access_token;

    websocket.connect(); //open websocket connection           
    var urlpath = window.location.search;

    var queryString = urlpath.split('?q=');
    console.log("queryString");

    roomsPax = "";
    var roomList = [];
    var roomItem = [];

    //Parse rooms
    for (var irt = 1; irt <= parseInt(this.num_room); irt++) {

      roomItem = [];
      roomsPax = GetParameterValues("room" + irt);
      paxDetails = roomsPax.split(',');
      ADTCount = (paxDetails[0].split('_'))[1];
      if (paxDetails.length > 1) {
        child = paxDetails[1].split('_');
        childCount = child[1];
        C_Count = 0;
        for (var i = 2; i < child.length; i++) {

          C_Count = C_Count + 1;
          age = child[i];
          console.log(age);
          if (childCount == C_Count) {
            roomItem.push({
              "age": age,
              "sequence": C_Count
            });
          }
          else if (childCount > 1) {
            roomItem.push({
              "age": age,
              "sequence": C_Count
            });
          } else {
            roomItem.push({
              "age": age,
              "sequence": C_Count
            });
          }
        }
        roomList.push({
          "roomId": irt,
          "guests": {
            "adult": ADTCount,
            "children": roomItem
          }
        });
      }

    }
    // console.log(JSON.stringify(roomList));

    //for supplier list
    var supplierListFromAgencyNode = [];
    var agencyNode = window.localStorage.getItem("User");
    if (agencyNode) {
      agencyNode = JSON.parse(agencyNode);
      var servicesList = agencyNode.loginNode.servicesList;
      for (var i = 0; i < servicesList.length; i++) {
        if (servicesList[i].name == "Hotel" && servicesList[i].provider.length > 0) {
          for (var j = 0; j < servicesList[i].provider.length; j++) {
            supplierListFromAgencyNode.push(servicesList[i].provider[j].id);
          }
          break;
        } else if (servicesList[i].name == "Hotel" && servicesList[i].provider.length == 0) {
          // no supplier
        }
      }
    }
    searchRequest = {
      "request": {
        "service": "HotelRQ",
        "content": {
          "command": "HotelSearchRQ",
          "criteria": {
            "criteriaType": "Hotel",
            "location": {
              "cityCode": this.city_code,
              "countryCode": window.sessionStorage.getItem("countryCode") ? window.sessionStorage.getItem("countryCode") : this.nationality,
              "radius": this.rangeValue
            },
            "stay": {
              "checkIn": this.cin,
              "checkOut": this.cout
            },
            "nationality": this.nationality,
            "residentOf": this.country_of_residence,
            "rooms": roomList,
            "advanceSearch": {
              "hotelId": 0,
              "hotelName": "aaaa",
              "maxAmount": 0,
              "minAmount": 0,
              "hotelChain": "dfdf"
            }
          },
          "sort": [
            {
              "order": "Asc",
              "sequence": 1
            }
          ],
          "filter": {
            "noOfResults": "100",
            "from": 1,
            "to": 100
          },
          "supplierSpecific": {
            "uuid": this.uuid
          }
        },
        "token": this.access_token,
        "supplierCodes": supplierListFromAgencyNode,
      }
    }
    localStorage.setItem("hotelUUID", this.uuid);

    this.searchRequest = searchRequest;
    console.log(JSON.stringify(searchRequest));
    websocket.sendMessage(JSON.stringify(searchRequest)); //send request to web socket server
    // setTimeout(function () {
    //   websocket.sendMessage(JSON.stringify(searchRequest)); //send request to web socket server  
    //   Vue_HotelListing.requestStatus = false;
    // }, 1000);

    // var self = this;

    // setTimeout(function () {
    //   try {
    //     //self.hasError = false;
    //     console.log("Trying to connect first");
    //     websocket.sendMessage(JSON.stringify(searchRequest)); //send request to web socket server 
    //     self.requestStatus = false;
    //   } catch (error) {
    //     setTimeout(function () {
    //       try {            
    //         console.log("Trying to connect Second");
    //         websocket.sendMessage(JSON.stringify(searchRequest)); //send request to web socket server 
    //         self.requestStatus = false;
    //       } catch (error) {           
    //         self.NoResult = true;
    //         self.NoResultHead = 'NO HOTELS FOUND!';
    //         self.NoResultDesc = "Our systems seems to be experiencing an issue. Please refresh the page.";
    //       }
    //     }, 2000);
    //   }
    // }, 1000);

    this.hotelRatingCategories = ["5", "4", "3", "2", "1", "0"];
    var vm = this;
    setTimeout(function () {
      websocket.disconnect();
      if (!Vue_HotelListing.requestStatus) {
        Vue_HotelListing.loadingStatus = false;
        Vue_HotelListing.NoResult = true;
        Vue_HotelListing.NoResultHead = Vue_HotelListing.NoResultsFound_Label;
        Vue_HotelListing.NoResultDesc = Vue_HotelListing.NoResultsDesc_Label + ' (SUP - ' + Vue_HotelListing.uuid + ')';
        // Vue_HotelListing.NoResultHead = 'NO HOTELS FOUND!';
        // Vue_HotelListing.NoResultDesc = 'Please try again with different travel dates or guest details. Error : (SUP -' + Vue_HotelListing.uuid + ')';
      }
      vm.countValidSupplierResponse = vm.requestCount;
    }, 180000);
    this.$eventHub.$on('select-hotel-map', this.selectHotelMap);
  },
  beforeDestroy() {
    this.$eventHub.$off('select-hotel-map');
  },
  methods: {
    selectHotelMap: function (hotelCode) {
      this.selectHotel(hotelCode);
    },
    moment: function (date) {
      return moment(date);
    },
    fetchHotelList: function (data) {

      if (data.requestDetails) {
        this.requestCount = parseInt(data.requestDetails.count);

      } else {
        this.countValidSupplierResponse = ++this.countValidSupplierResponse;

      this.session_data = [];
      this.session_data = JSON.parse(JSON.stringify(data));
      this.session_data.response.content.searchResponse.hotels = [];
      this.session_data.response.content.searchResponse.supplierSpecific = {};
      // this.session_data.response.content.searchResponse.supplierSpecific.searchReferences = [];
      this.session_data.response.content.searchResponse.nationality = this.nationality;
      this.session_data.response.content.searchResponse.residentOf = this.country_of_residence;

      if (data.response.content.searchResponse.hotels != null) {
        if (data.response.content.searchResponse.hotels.length > 0) {

          for (let i = 0; i < data.response.content.searchResponse.hotels.length; i++) {
            data.response.content.searchResponse.hotels[i].supplierCode = data.response.supplierCode; //supplier code
            data.response.content.searchResponse.hotels[i].currency = data.response.node.currency;
            data.response.content.searchResponse.hotels[i].cityCode = data.response.content.supplierSpecific.criteria.location.cityCode;
            data.response.content.searchResponse.hotels[i].cityName = data.response.content.supplierSpecific.criteria.location.cityName;
            data.response.content.searchResponse.hotels[i].countryCode = data.response.content.supplierSpecific.criteria.location.countryCode;
            
            if (parseInt(data.response.content.searchResponse.hotels[i].starRating) < 1 || parseInt(data.response.content.searchResponse.hotels[i].starRating) > 5) {
              data.response.content.searchResponse.hotels[i].starRating = "0";
            }

            if (data.response.content.supplierSpecific) {
              data.response.content.searchResponse.hotels[i].supplierSpecific = {};
              data.response.content.searchResponse.hotels[i].supplierSpecific.searchReferences = data.response.content.supplierSpecific.searchReferences;
            }

            //Check if same hotel already exists
            const found = this.total_result.some(el => el.hotelCode === data.response.content.searchResponse.hotels[i].hotelCode && el.supplierCode === data.response.supplierCode)

            if (!found) this.total_result.push(data.response.content.searchResponse.hotels[i]); //Fetch total results                     

            Vue_HotelListing.requestStatus = true;
          }
          this.total_result.sort((a, b) => a.rate - b.rate)
          this.totalResult_Count = this.total_result.length;
          console.log("Total Result:" + this.total_result.length);

        }

      }

      if (this.hotelList.length == 0) {

        if (this.total_result.length > 0) {

          tot_limit = this.total_result.length;
          if (tot_limit <= 10) {

            for (let index = this.startLimit; index < tot_limit; index++) {

              this.hotelList.push(this.total_result[index]);
              this.startLimit = tot_limit;
              // this.limit = tot_limit + 10;
            }
          } else {
            for (let index = this.startLimit; index < this.limit; index++) {
              this.hotelList.push(this.total_result[index]);
            }
            this.startLimit = this.limit;
            this.limit = this.limit + 10;
          }
        }

      }
      else if (this.hotelList.length > 0) {
        tot_limit = this.total_result.length;
        if (tot_limit <= 10) {
          for (let index = this.startLimit; index < tot_limit; index++) {

            this.hotelList.push(this.total_result[index]);
            this.startLimit = tot_limit;
            // this.limit = tot_limit + 10;
          }
        }


      }

      if (this.total_result.length > 0) {

        var self = this;

        this.lowestPrice = Math.min.apply(Math, this.total_result.map(function (o) { return o.rate; }))
        this.highestPrice = Math.max.apply(Math, this.total_result.map(function (o) { return o.rate; }))

        if (this.highestPrice == this.lowestPrice) {
          this.lowestPrice = 0;
        }
        this.startPriceValue = this.lowestPrice;
        this.endPriceValue = this.highestPrice;

        $("#minPricevalue").text(this.lowestPrice);
        $("#MaxPricevalue").text(this.highestPrice);

        var slider = document.getElementById('ageSlider');
        slider.noUiSlider.set([parseFloat(this.lowestPrice), parseFloat(this.highestPrice)]);
        slider.noUiSlider.updateOptions({
          range: {
            'min': parseFloat(this.lowestPrice),
            'max': parseFloat(this.highestPrice)
          }
        });
        var slider = document.getElementById('ageSlider');
        slider.noUiSlider.set([parseFloat(this.lowestPrice), parseFloat(this.highestPrice)]);
        slider.noUiSlider.updateOptions({
          range: {
            'min': parseFloat(this.lowestPrice),
            'max': parseFloat(this.highestPrice)
          }
        });


        slider.noUiSlider.on('update', function (values, handle) {
          $("#minPricevalue").text(self.$n(parseFloat(values[0] / self.CurrencyMultiplier), 'currency', self.selectedCurrency));
          $("#MaxPricevalue").text(self.$n(parseFloat(values[1] / self.CurrencyMultiplier), 'currency', self.selectedCurrency));
          Vue_HotelListing.priceFilter(values[0], values[1]);
          Vue_HotelListing.sort('price')

        });


        Vue_HotelListing.loadingStatus = false;
        this.getWeather(function (resp) {
          Vue_HotelListing.weatherData = resp;

        });
      }

        if (this.requestCount == this.countValidSupplierResponse) {
          websocket.disconnect();
          if (this.hotelList.length == 0) {
            this.loadingStatus = false;
            this.NoResult = true;
            this.NoResultHead = this.NoResultsFound_Label;
            this.NoResultDesc = this.NoResultsDesc_Label + ' (SUP - ' + Vue_HotelListing.uuid + ')';
          }
          this.countValidSupplierResponse = this.requestCount;
        }
      }
    },
    sort: function (sortType) {

      // this.isFiltered = true;
      this.sortOrder = !this.sortOrder;
      this.startLimit = 10;
      this.limit = 20;
      if (Vue_HotelListing.hotelList.length > 0) {
        //Vue_HotelListing.filter_result = Vue_HotelListing.total_result;
        switch (sortType) {
          case 'price':
            this.priceSortOrder = this.sortOrder;
            this.starSortOrder = true;
            this.nameSortOrder = true;

            if (this.filter_result.length == 0) {
              this.filter_result = Vue_HotelListing.total_result;
            }
            this.filter_result.sort(comparePrice);
            this.isPriceActive = true;
            this.isNameActive = false;
            this.isStarActive = false;
            break;
          case 'name':
            this.nameSortOrder = this.sortOrder;
            this.priceSortOrder = true;
            this.starSortOrder = true;

            if (this.filter_result.length == 0) {
              this.filter_result = Vue_HotelListing.total_result;
            }
            this.filter_result.sort(compareName);
            this.isPriceActive = false;
            this.isNameActive = true;
            this.isStarActive = false;
            break;
          case 'star':
            this.starSortOrder = this.sortOrder;
            this.priceSortOrder = true;
            this.nameSortOrder = true;

            if (this.filter_result.length == 0) {
              this.filter_result = Vue_HotelListing.total_result;
            }
            this.filter_result.sort(compareStar);
            this.isPriceActive = false;
            this.isNameActive = false;
            this.isStarActive = true;
            break;
        }
        fetchSortResults();
      }
    },
    /**************************************Hotel Name************************************************/
    hotelSearch(event) {
      this.filter_result = [];
      this.hotelList = [];
      this.startLimit = 0;
      this.limit = 10;
      var tempList = [];
      this.isFilterApplied = false;
      if (this.hotelSearchKeyName.length > 0) { //Search by Name
        this.filter_result = [];
        this.isFiltered = true;
        if (this.hotelSearchKeyLocation.length > 0) { //Search by Location  
          this.isFilterApplied = true;
          for (var i = 0; i < this.total_result.length; i++) {
            if (this.total_result[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation.toLowerCase()) || this.total_result[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation.toUpperCase())) {
              this.filter_result.push(this.total_result[i]);
            }
          }
        }

        if (parseFloat(this.startPriceValue) != parseFloat(this.lowestPrice) || parseFloat(this.endPriceValue) != parseFloat(this.highestPrice)) { //price filter
          this.isFiltered = true;
          //  this.isFilterApplied = true;
          //if (this.filter_result.length > 0) {
          if (this.isFilterApplied) {
            tempList = [];
            tempList = this.filter_result;
            this.filter_result = [];
            for (var i = 0; i < tempList.length; i++) {
              if (tempList[i].rate >= parseFloat(this.startPriceValue) && tempList[i].rate <= parseFloat(this.endPriceValue)) {
                this.filter_result.push(tempList[i]);
              }
            }

          } else {
            for (var i = 0; i < this.total_result.length; i++) {
              if (this.total_result[i].rate >= parseFloat(this.startPriceValue) && this.total_result[i].rate <= parseFloat(this.endPriceValue)) {
                this.filter_result.push(this.total_result[i]);
              }
            }
          }
          this.isFilterApplied = true;
        }
        if (this.hotelRatingCategories.length > 0) { //Search by Rating
          this.isFiltered = true;
          this.isFilterApplied = true;
          if (this.filter_result.length > 0) {
            tempList = this.filter_result;
            this.filter_result = [];
            this.hotelRatingCategories.forEach(rating_element => {
              for (var i = 0; i < tempList.length; i++) {
                if (parseInt(tempList[i].starRating) == parseInt(rating_element)) {
                  this.filter_result.push(tempList[i]);
                }
              }
            });
          } else {
            this.hotelRatingCategories.forEach(rating_element => {
              for (var i = 0; i < this.total_result.length; i++) {
                if (parseInt(this.total_result[i].starRating) == parseInt(rating_element)) {
                  this.filter_result.push(this.total_result[i]);
                }
              }
            });
          }
        }
        if (this.filter_result.length > 0) {
          tempList = [];
          tempList = this.filter_result;
          this.filter_result = [];

          for (var i = 0; i < tempList.length; i++) {
            if (tempList[i].name.toLowerCase().includes(this.hotelSearchKeyName.toLowerCase()) || tempList[i].name.toUpperCase().includes(this.hotelSearchKeyName.toUpperCase())) {
              this.filter_result.push(tempList[i]);
            }
          }

        }
        else {
          if (!this.isFilterApplied) {
            for (var i = 0; i < this.total_result.length; i++) {
              if (this.total_result[i].name.toLowerCase().includes(this.hotelSearchKeyName.toLowerCase()) || this.total_result[i].name.toUpperCase().includes(this.hotelSearchKeyName.toUpperCase())) {
                this.filter_result.push(this.total_result[i]);
              }
            }
          }
        }



        fetchFilterResults();
        console.log(this.filter_result.length);

      } else if (this.hotelSearchKeyLocation.length > 0 || this.hotelRatingCategories.length > 0 || parseFloat(this.startPriceValue) != parseFloat(this.lowestPrice) || parseFloat(this.endPriceValue) != parseFloat(this.highestPrice)) { //Name search NULL
        this.isFiltered = true;
        if (this.hotelSearchKeyLocation.length > 0) {
          this.isFilterApplied = true;
          for (var i = 0; i < this.total_result.length; i++) {
            if (this.total_result[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation.toLowerCase()) || this.total_result[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation.toUpperCase())) {
              this.filter_result.push(this.total_result[i]);
            }
          }
        }
        if (this.hotelRatingCategories.length > 0) {

          this.isFiltered = true;
          this.isFilterApplied = true;
          if (this.filter_result.length > 0) {
            tempList = this.filter_result;
            this.filter_result = [];
            this.hotelRatingCategories.forEach(rating_element => {
              for (var i = 0; i < tempList.length; i++) {
                if (parseInt(tempList[i].starRating) == parseInt(rating_element)) {
                  this.filter_result.push(tempList[i]);
                }
              }
            });
          } else {
            this.hotelRatingCategories.forEach(rating_element => {
              for (var i = 0; i < this.total_result.length; i++) {
                if (parseInt(this.total_result[i].starRating) == parseInt(rating_element)) {
                  this.filter_result.push(this.total_result[i]);
                }
              }
            });
          }

        }
        if (parseFloat(this.startPriceValue) != parseFloat(this.lowestPrice) || parseFloat(this.endPriceValue) != parseFloat(this.highestPrice)) { //price filter

          this.isFiltered = true;

          // if (this.filter_result.length) {
          if (this.isFilterApplied) {
            tempList = [];
            tempList = this.filter_result;
            this.filter_result = [];
            for (var i = 0; i < tempList.length; i++) {
              if (tempList[i].rate >= parseFloat(this.startPriceValue) && tempList[i].rate <= parseFloat(this.endPriceValue)) {
                this.filter_result.push(tempList[i]);
              }
            }

          } else {
            for (var i = 0; i < this.total_result.length; i++) {
              if (this.total_result[i].rate >= parseFloat(this.startPriceValue) && this.total_result[i].rate <= parseFloat(this.endPriceValue)) {
                this.filter_result.push(this.total_result[i]);
              }
            }
          }
          this.isFilterApplied = true;
        }

        fetchFilterResults();
        console.log(this.filter_result.length);
      }
      else {

        this.isFiltered = false;
        fetchAllResults();
      }

    },
    /**************************************Hotel Location********************************************/
    searchByLocation(event) {

      this.filter_result = [];
      this.hotelList = [];
      this.startLimit = 0;
      this.limit = 10;
      var tempList = [];
      this.isFilterApplied = false;
      if (this.hotelSearchKeyLocation.length > 0) { //Search by Location             

        this.isFiltered = true;

        if (this.hotelSearchKeyName.length > 0) {
          this.isFilterApplied = true;
          for (var i = 0; i < this.total_result.length; i++) {
            if (this.total_result[i].name.toLowerCase().includes(this.hotelSearchKeyName.toLowerCase()) || this.total_result[i].name.toUpperCase().includes(this.hotelSearchKeyName.toUpperCase())) {
              this.filter_result.push(this.total_result[i]);
            }
          }
        }
        if (this.hotelRatingCategories.length > 0) { //Rating
          this.isFiltered = true;
          this.isFilterApplied = true;
          if (this.filter_result.length > 0) {
            tempList = this.filter_result;
            this.filter_result = [];
            this.hotelRatingCategories.forEach(rating_element => {
              for (var i = 0; i < tempList.length; i++) {
                if (parseInt(tempList[i].starRating) == parseInt(rating_element)) {
                  this.filter_result.push(tempList[i]);
                }
              }
            });
          } else {
            this.hotelRatingCategories.forEach(rating_element => {
              for (var i = 0; i < this.total_result.length; i++) {
                if (parseInt(this.total_result[i].starRating) == parseInt(rating_element)) {
                  this.filter_result.push(this.total_result[i]);
                }
              }
            });
          }

        }

        if (parseFloat(this.startPriceValue) != parseFloat(this.lowestPrice) || parseFloat(this.endPriceValue) != parseFloat(this.highestPrice)) { //price filter
          this.isFiltered = true;
          // this.isFilterApplied = true;
          //if (this.filter_result.length) {
          if (this.isFilterApplied) {
            tempList = [];
            tempList = this.filter_result;
            this.filter_result = [];
            for (var i = 0; i < tempList.length; i++) {
              if (tempList[i].rate >= parseFloat(this.startPriceValue) && tempList[i].rate <= parseFloat(this.endPriceValue)) {
                this.filter_result.push(tempList[i]);
              }
            }

          } else {
            for (var i = 0; i < this.total_result.length; i++) {
              if (this.total_result[i].rate >= parseFloat(this.startPriceValue) && this.total_result[i].rate <= parseFloat(this.endPriceValue)) {
                this.filter_result.push(this.total_result[i]);
              }
            }
          }
          this.isFilterApplied = true;
        }

        if (this.filter_result.length > 0) {
          tempList = this.filter_result;
          this.filter_result = [];
          for (var i = 0; i < tempList.length; i++) {
            if (tempList[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation.toLowerCase()) || tempList[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation.toUpperCase())) {
              this.filter_result.push(tempList[i]);
            }
          }
        } else {
          if (!this.isFilterApplied) {
            for (var i = 0; i < this.total_result.length; i++) {
              if (this.total_result[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation.toLowerCase()) || this.total_result[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation.toUpperCase())) {
                this.filter_result.push(this.total_result[i]);
              }
            }
          }
        }
        fetchFilterResults();
        console.log(this.filter_result.length);

      } else if (this.hotelSearchKeyName.length > 0 || this.hotelRatingCategories.length > 0 || parseFloat(this.startPriceValue) != parseFloat(this.lowestPrice) || parseFloat(this.endPriceValue) != parseFloat(this.highestPrice)) { //Search Location NULL
        this.isFiltered = true;
        if (this.hotelSearchKeyName.length > 0) {
          this.isFilterApplied = true;
          for (var i = 0; i < this.total_result.length; i++) {
            if (this.total_result[i].name.toLowerCase().includes(this.hotelSearchKeyName.toLowerCase()) || this.total_result[i].name.toUpperCase().includes(this.hotelSearchKeyName.toUpperCase())) {
              this.filter_result.push(this.total_result[i]);
            }
          }
        }
        if (this.hotelRatingCategories.length > 0) {
          this.isFiltered = true;
          this.isFilterApplied = true;
          if (this.filter_result.length > 0) {
            tempList = this.filter_result;
            this.filter_result = [];
            this.hotelRatingCategories.forEach(rating_element => {
              for (var i = 0; i < tempList.length; i++) {
                if (parseInt(tempList[i].starRating) == parseInt(rating_element)) {
                  this.filter_result.push(tempList[i]);
                }
              }
            });
          } else {
            this.hotelRatingCategories.forEach(rating_element => {
              for (var i = 0; i < this.total_result.length; i++) {
                if (parseInt(this.total_result[i].starRating) == parseInt(rating_element)) {
                  this.filter_result.push(this.total_result[i]);
                }
              }
            });
          }

        }

        if (parseFloat(this.startPriceValue) != parseFloat(this.lowestPrice) || parseFloat(this.endPriceValue) != parseFloat(this.highestPrice)) { //price filter

          this.isFiltered = true;
          // if (this.filter_result.length) {
          if (this.isFilterApplied) {
            tempList = [];
            tempList = this.filter_result;
            this.filter_result = [];
            for (var i = 0; i < tempList.length; i++) {
              if (tempList[i].rate >= parseFloat(this.startPriceValue) && tempList[i].rate <= parseFloat(this.endPriceValue)) {
                this.filter_result.push(tempList[i]);
              }
            }

          } else {
            for (var i = 0; i < this.total_result.length; i++) {
              if (this.total_result[i].rate >= parseFloat(this.startPriceValue) && this.total_result[i].rate <= parseFloat(this.endPriceValue)) {
                this.filter_result.push(this.total_result[i]);
              }
            }
          }
          this.isFilterApplied = true;
        }


        fetchFilterResults();
        console.log(this.filter_result.length);

      } else {
        this.isFiltered = false;
        fetchAllResults();
      }
    },
    /**************************************Show all/only for star**********************************************/
    showStarOnly: function (star) {
      this.hotelRatingCategories = [star.toString()];
      this.filterByRating();
      this.toggleShowAllStar = true;
    },
    starShowAll: function () {
      this.hotelRatingCategories = ["5", "4", "3", "2", "1", "0"];
      this.filterByRating();
    },
    shoHideShowAllStar: function () {
      if (this.hotelRatingCategories.length < 5) {
        this.toggleShowAllStar = !this.toggleShowAllStar
      } else {
        this.toggleShowAllStar = false;
      }
    },
    /**************************************Hotel rating**********************************************/
    filterByRating: function (e) {
      this.toggleShowAllStar = true;
      this.filter_result = [];
      this.hotelList = [];
      this.startLimit = 0;
      this.limit = 10;
      var tempList = [];
      this.isFilterApplied = false;
      if (this.hotelRatingCategories.length > 0) { //Search by Rating              

        this.isFiltered = true;

        if (this.hotelSearchKeyName.length > 0) {
          this.isFilterApplied = true;
          for (var i = 0; i < this.total_result.length; i++) {
            if (this.total_result[i].name.toLowerCase().includes(this.hotelSearchKeyName) || this.total_result[i].name.toUpperCase().includes(this.hotelSearchKeyName)) {
              this.filter_result.push(this.total_result[i]);
            }
          }
        }
        if (this.hotelSearchKeyLocation.length > 0) { //Search by Location
          this.isFiltered = true;
          //this.isFilterApplied = true;
          //if (this.filter_result.length > 0) {
          if (this.isFilterApplied) {

            tempList = [];
            tempList = this.filter_result;
            this.filter_result = [];
            for (var i = 0; i < tempList.length; i++) {
              if (tempList[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation) || tempList[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation)) {
                this.filter_result.push(tempList[i]);
              }
            }
          } else {
            for (var i = 0; i < this.total_result.length; i++) {
              if (this.total_result[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation) || this.total_result[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation)) {
                this.filter_result.push(this.total_result[i]);
              }
            }
          }
          this.isFilterApplied = true;
        }

        if (parseFloat(this.startPriceValue) != parseFloat(this.lowestPrice) || parseFloat(this.endPriceValue) != parseFloat(this.highestPrice)) { //price filter
          this.isFiltered = true;
          // this.isFilterApplied = true;
          // if (this.filter_result.length) {
          if (this.isFilterApplied) {
            tempList = [];
            tempList = this.filter_result;
            this.filter_result = [];
            for (var i = 0; i < tempList.length; i++) {
              if (tempList[i].rate >= parseFloat(this.startPriceValue) && tempList[i].rate <= parseFloat(this.endPriceValue)) {
                this.filter_result.push(tempList[i]);
              }
            }

          } else {
            for (var i = 0; i < this.total_result.length; i++) {
              if (this.total_result[i].rate >= parseFloat(this.startPriceValue) && this.total_result[i].rate <= parseFloat(this.endPriceValue)) {
                this.filter_result.push(this.total_result[i]);
              }
            }
          }
          this.isFilterApplied = true;
        }

        if (this.filter_result.length > 0) {
          tempList = [];
          tempList = this.filter_result;
          this.filter_result = [];
          // console.log(tempList);
          this.hotelRatingCategories.forEach(rating_element => {
            for (var i = 0; i < tempList.length; i++) { //get data from all results

              if (parseInt(tempList[i].starRating) == parseInt(rating_element)) {

                this.filter_result.push(tempList[i]);
              }
            }
          });
        } else {
          if (!this.isFilterApplied) {
            this.hotelRatingCategories.forEach(rating_element => {
              for (var i = 0; i < this.total_result.length; i++) { //get data from all results
                if (parseInt(this.total_result[i].starRating) == parseInt(rating_element)) {
                  this.filter_result.push(this.total_result[i]);
                }
              }
            });
          }
        }

        fetchFilterResults();
        console.log(this.filter_result.length);
      } else if (this.hotelSearchKeyName.length > 0 || this.hotelSearchKeyLocation.length > 0 || parseFloat(this.startPriceValue) != parseFloat(this.lowestPrice) || parseFloat(this.endPriceValue) != parseFloat(this.highestPrice)) { //Search by rating NULL

        this.isFiltered = true;
        if (this.hotelSearchKeyName.length > 0) {
          this.isFilterApplied = true;
          for (var i = 0; i < this.total_result.length; i++) {
            if (this.total_result[i].name.toLowerCase().includes(this.hotelSearchKeyName) || this.total_result[i].name.toUpperCase().includes(this.hotelSearchKeyName)) {
              this.filter_result.push(this.total_result[i]);
            }
          }
        }
        if (this.hotelSearchKeyLocation.length > 0) {
          this.isFiltered = true;
          // this.isFilterApplied = true;
          // if (this.filter_result.length > 0) {
          if (this.isFilterApplied) {
            tempList = [];
            tempList = this.filter_result;
            this.filter_result = [];
            for (var i = 0; i < tempList.length; i++) {
              if (tempList[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation) || tempList[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation)) {
                this.filter_result.push(tempList[i]);
              }
            }
          } else {
            for (var i = 0; i < this.total_result.length; i++) {
              if (this.total_result[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation) || this.total_result[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation)) {
                this.filter_result.push(this.total_result[i]);
              }
            }
          }
          this.isFilterApplied = true;
        }

        if (parseFloat(this.startPriceValue) != parseFloat(this.lowestPrice) || parseFloat(this.endPriceValue) != parseFloat(this.highestPrice)) { //price filter

          this.isFiltered = true;
          // if (this.filter_result.length) {
          if (this.isFilterApplied) {
            tempList = [];
            tempList = this.filter_result;
            this.filter_result = [];
            for (var i = 0; i < tempList.length; i++) {
              if (tempList[i].rate >= parseFloat(this.startPriceValue) && tempList[i].rate <= parseFloat(this.endPriceValue)) {
                this.filter_result.push(tempList[i]);
              }
            }

          } else {
            for (var i = 0; i < this.total_result.length; i++) {
              if (this.total_result[i].rate >= parseFloat(this.startPriceValue) && this.total_result[i].rate <= parseFloat(this.endPriceValue)) {
                this.filter_result.push(this.total_result[i]);
              }
            }
          }
          this.isFilterApplied = true;

        }


        fetchFilterResults();
        console.log(this.filter_result.length);

      } else {
        this.isFiltered = false;
        // fetchAllResults();
        Vue_HotelListing.totalResult_Count = Vue_HotelListing.filter_result.length;
      }
    },
    /**************************************Hotel Price filter****************************************/
    priceFilter(startPrice, endPrice) {
      this.filter_result = [];
      this.hotelList = [];
      this.startLimit = 0;
      this.limit = 10;
      var tempList = [];
      this.startPriceValue = startPrice;
      this.endPriceValue = endPrice;
      this.isFilterApplied = false;
      if (parseFloat(startPrice) != parseFloat(this.lowestPrice) || parseFloat(endPrice) != parseFloat(this.highestPrice)) {
        // this.startPriceValue = startPrice;
        // this.endPriceValue = endPrice;


        this.isFiltered = true;
        if (this.hotelSearchKeyName.length > 0) {
          this.isFilterApplied = true;
          for (var i = 0; i < this.total_result.length; i++) { //Name
            if (this.total_result[i].name.toLowerCase().includes(this.hotelSearchKeyName) || this.total_result[i].name.toUpperCase().includes(this.hotelSearchKeyName)) {
              this.filter_result.push(this.total_result[i]);
            }
          }
        }
        if (this.hotelSearchKeyLocation.length > 0) { //Search by Location
          this.isFiltered = true;
          // this.isFilterApplied = true;
          // if (this.filter_result.length > 0) {
          if (this.isFilterApplied) {
            tempList = [];
            tempList = this.filter_result;
            this.filter_result = [];
            for (var i = 0; i < tempList.length; i++) {
              if (tempList[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation) || tempList[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation)) {
                this.filter_result.push(tempList[i]);
              }
            }
          } else {
            for (var i = 0; i < this.total_result.length; i++) {
              if (this.total_result[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation) || this.total_result[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation)) {
                this.filter_result.push(this.total_result[i]);
              }
            }
          }
          this.isFilterApplied = true;
        }
        if (this.hotelRatingCategories.length > 0) { //Search by Rating
          this.isFiltered = true;
          this.isFilterApplied = true;
          if (this.filter_result.length > 0) {
            tempList = [];
            tempList = this.filter_result;
            this.filter_result = [];
            this.hotelRatingCategories.forEach(rating_element => {
              for (var i = 0; i < tempList.length; i++) {
                if (parseInt(tempList[i].starRating) == parseInt(rating_element)) {
                  this.filter_result.push(tempList[i]);
                }
              }
            });
          } else {
            this.hotelRatingCategories.forEach(rating_element => {
              for (var i = 0; i < this.total_result.length; i++) {
                if (parseInt(this.total_result[i].starRating) == parseInt(rating_element)) {
                  this.filter_result.push(this.total_result[i]);
                }
              }
            });
          }
        }
        if (this.filter_result.length > 0) {
          tempList = [];
          tempList = this.filter_result;
          this.filter_result = [];
          for (var i = 0; i < tempList.length; i++) {
            if (tempList[i].rate >= parseFloat(startPrice) && tempList[i].rate <= parseFloat(endPrice)) {
              this.filter_result.push(tempList[i]);
            }
          }
        } else {
          if (!this.isFilterApplied) {
            for (var i = 0; i < this.total_result.length; i++) {
              if (this.total_result[i].rate >= parseFloat(startPrice) && this.total_result[i].rate <= parseFloat(endPrice)) {
                this.filter_result.push(this.total_result[i]);
              }
            }
          }
        }
        fetchFilterResults();
        console.log(this.filter_result.length);

      } else if (this.hotelSearchKeyName.length > 0 || this.hotelSearchKeyLocation.length > 0 || this.hotelRatingCategories.length > 0) {
        this.isFiltered = true;
        if (this.hotelSearchKeyName.length > 0) {
          this.isFilterApplied = true;
          for (var i = 0; i < this.total_result.length; i++) {
            if (this.total_result[i].name.toLowerCase().includes(this.hotelSearchKeyName) || this.total_result[i].name.toUpperCase().includes(this.hotelSearchKeyName)) {
              this.filter_result.push(this.total_result[i]);
            }
          }
        }
        if (this.hotelSearchKeyLocation.length > 0) { //Search by Location
          this.isFiltered = true;
          // this.isFilterApplied = true;
          // if (this.filter_result.length > 0) {
          if (this.isFilterApplied) {
            tempList = [];
            tempList = this.filter_result;
            this.filter_result = [];
            for (var i = 0; i < tempList.length; i++) {
              if (tempList[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation) || tempList[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation)) {
                this.filter_result.push(tempList[i]);
              }
            }
          } else {
            for (var i = 0; i < this.total_result.length; i++) {
              if (this.total_result[i].location.address.toLowerCase().includes(this.hotelSearchKeyLocation) || this.total_result[i].location.address.toUpperCase().includes(this.hotelSearchKeyLocation)) {
                this.filter_result.push(this.total_result[i]);
              }
            }
          }
          this.isFilterApplied = true;
        }

        if (this.hotelRatingCategories.length > 0) { //Search by Rating
          if (this.filter_result.length > 0) {
            tempList = [];
            tempList = this.filter_result;
            this.filter_result = [];
            this.hotelRatingCategories.forEach(rating_element => {
              for (var i = 0; i < tempList.length; i++) {
                if (parseInt(tempList[i].starRating) == parseInt(rating_element)) {
                  this.filter_result.push(tempList[i]);
                }
              }
            });
          } else {
            this.hotelRatingCategories.forEach(rating_element => {
              for (var i = 0; i < this.total_result.length; i++) {
                if (parseInt(this.total_result[i].starRating) == parseInt(rating_element)) {
                  this.filter_result.push(this.total_result[i]);
                }
              }
            });
          }
        }

        fetchFilterResults();
        console.log(this.filter_result.length);

      }
      else {
        this.isFiltered = false;
        fetchAllResults();
      }

    },
    citySearchCompleted(cityCode, cityName) {

      //  this.cityCode = cityCode;
      // this.cityName = cityName;
      this.city_code = cityCode;
      this.city = cityName;

    },
    /**Add Room Click Start**/
    addNewTodo: function () {
      if (this.todos.length < 9) {
        var roomCount = ++this.count;
        id = 'div_Room_' + roomCount;
        this.dspl = true;
        this.todos.push({
          id,
          value: roomCount,


        });
        this.$set(this.values, id, '')
      }
    },
    /**Add Room Click End**/
    /**Remove Room Click Start**/
    RemoveNewTodo: function (todo) {
      var roomCount = --this.count;
      if (roomCount == 1) { this.dspl = false; }
      var index = this.todos.indexOf(todo);
      this.todos.splice(index, 1);
      var noOfRooms = this.todos.length;
      var totalguest = 0;
      for (ipax = 1; ipax <= parseInt(noOfRooms); ipax++) {
        roomsSelected += '&room' + ipax + '=ADT_' + $('#adult_' + ipax).find(":selected").val() +
          ',CHD_' + $('#child_' + ipax).find(":selected").val() + '';
        for (ichild = 1; ichild <= parseInt($('#child_' + ipax).find(":selected").val()); ichild++) {

          roomsSelected += '_' + $('#childage_' + ipax + '_' + ichild).find(":selected").val();
        }

        totalguest += parseInt($('#adult_' + ipax).find(":selected").val()) + parseInt($('#child_' + ipax).find(":selected").val());
      }

      this.numberOfRooms = noOfRooms;

      this.totalcount = totalguest + " " + this.Guest_Label + ", " + noOfRooms + " " + this.Rooms_Label;

    },
    /**Remove Room Click End**/
    /**Room Final Selection Click Start**/
    done: function () {
      roomsSelected = '';
      console.log(this.values);
      var noOfRooms = this.todos.length;
      var totalguest = 0;
      var adt_c = 0;
      var chd_c = 0;
      for (ipax = 1; ipax <= parseInt(noOfRooms); ipax++) {
        roomsSelected += '&room' + ipax + '=ADT_' + $('#adult_' + ipax).find(":selected").val() +
          ',CHD_' + $('#child_' + ipax).find(":selected").val() + '';
        for (ichild = 1; ichild <= parseInt($('#child_' + ipax).find(":selected").val()); ichild++) {

          roomsSelected += '_' + $('#childage_' + ipax + '_' + ichild).find(":selected").val();
        }
        adt_c += parseInt($('#adult_' + ipax).find(":selected").val());
        chd_c += parseInt($('#child_' + ipax).find(":selected").val());
        totalguest += parseInt($('#adult_' + ipax).find(":selected").val()) + parseInt($('#child_' + ipax).find(":selected").val());
      }

      this.numberOfRooms = noOfRooms;
      this.guestCount = totalguest;
      this.guestNumber = this.guestCount;
      this.adtCount = adt_c;
      this.chdCount = chd_c;
      this.totalcount = totalguest + " " + this.Guest_Label + ", " + noOfRooms + " " + this.Rooms_Label;


    },
    /**Room Final Selection Click End**/
    /**Search Click Start**/
    search: function () {
      // var cityCode = this.cityCode;
      this.done();
      var checkIn = $('#checkInId').datepicker('getDate');
      checkIn = moment(checkIn, 'DD MMM YY,ddd').format('DD/MM/YYYY');
      var checkOut = $('#CheckOutId').datepicker('getDate');
      checkOut = moment(checkOut, 'DD MMM YY,ddd').format('DD/MM/YYYY');
      var uuidcode = uuidv4();
      //var supplers = 'QTECH';          
      var roomDetails = roomsSelected;
      if (!this.city_code) {
        this.validationMessage = "Please select city !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = "";
        }, 1500);
        document.getElementById("txtCitySearch").focus();
        return false;
      }

      if (!checkIn) {
        this.validationMessage = "Please select checkindate !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = "";
        }, 1500);
        return false;
      }
      if (!checkOut) {
        this.validationMessage = "Please select checkoutdate !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = "";
        }, 1500);
        return false;
      }
      if (!this.totalcount) {
        this.validationMessage = "Please select guests !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = "";
        }, 1500);
        return false;
      }
      if (this.guestNumber > 9) {
        this.validationMessage = "Maximum occupancy in a single booking is 9.";
        var self = this;
        setTimeout(function () {
          self.validationMessage = "";
        }, 1500);
        return false;
      }

      this.validationMessage = "";
      console.log(this.city_code);
      console.log(this.city);

      roomsSelected = '';
      console.log(this.values);
      var noOfRooms = this.todos.length;
      var totalguest = 0;
      var adt_c = 0;
      var chd_c = 0;

      if (parseInt(noOfRooms) > 0) {

        for (ipax = 1; ipax <= parseInt(noOfRooms); ipax++) {
          roomsSelected += '&room' + ipax + '=ADT_' + $('#adult_' + ipax).find(":selected").val() +
            ',CHD_' + $('#child_' + ipax).find(":selected").val() + '';
          for (ichild = 1; ichild <= parseInt($('#child_' + ipax).find(":selected").val()); ichild++) {

            roomsSelected += '_' + $('#childage_' + ipax + '_' + ichild).find(":selected").val();
          }
          adt_c += parseInt($('#adult_' + ipax).find(":selected").val());
          chd_c += parseInt($('#child_' + ipax).find(":selected").val());
          totalguest += parseInt($('#adult_' + ipax).find(":selected").val()) + parseInt($('#child_' + ipax).find(":selected").val());
        }
        this.numberOfRooms = noOfRooms;

        this.guestCount = totalguest;
        this.guestNumber = this.guestCount;
        this.adtCount = adt_c;
        this.chdCount = chd_c;

      } else {
        this.adtCount = (this.adtCount == 0) ? this.adtCount = 1 : this.adtCount = this.adtCount;
        this.guestCount = (this.guestCount == 0) ? this.guestCount = 1 : this.guestCount = this.guestCount;
      }

      this.totalcount = totalguest + " " + this.Guest_Label + ", " + noOfRooms + " " + this.Rooms_Label;

      var div = $('.autocomplete');
      div.find(':input').val(this.cityName);
      this.validationMessage = "";
      // console.log(roomDetails);



      var CORandNat = this.hasCORandNat ?
        "nationality=" + this.nationality + "&country_of_residence=" + this.country_of_residence : "nationality=AE&country_of_residence=AE";
      var searchCriteria = CORandNat +
        "&city_code=" + this.city_code +
        "&city=" + this.city +
        "&cin=" + checkIn +
        "&cout=" + checkOut +
        "&night=" + moment.duration(moment(checkOut, 'DD/MM/YYYY').diff(moment(checkIn, 'DD/MM/YYYY'))).asDays() +
        "&adtCount=" + this.adtCount +
        "&chdCount=" + this.chdCount +
        "&guest=" + this.guestCount +
        "&num_room=" + this.numberOfRooms +
        roomDetails +
        "&KMrange=" + this.rangeValue + "&sort=" + "price-a" +
        (this.hasCORandNat ? "&hasCORandNat=true" : "") +
        "&uuid=" + uuidcode;
      var uri = '../Hotels/hotel-listing.html?' + searchCriteria;
      window.location.href = uri

    },
    /**Search Click End**/
    /**Select Hotel */
    selectHotel: function (code) {
      const result = this.total_result.find(hotel => hotel.hotelCode === code);
      this.session_data.response.supplierCode = result.supplierCode;
      this.session_data.response.node.currency = result.currency;
      if (result.supplierSpecific) {
        this.session_data.response.content.searchResponse.supplierSpecific.searchReferences = result.supplierSpecific.searchReferences;
      }
      this.session_data.response.content.supplierSpecific.criteria.location.cityCode = result.cityCode;
      this.session_data.response.content.supplierSpecific.criteria.location.cityName = result.cityName;
      this.session_data.response.content.supplierSpecific.criteria.location.countryCode = result.countryCode;
      this.session_data.response.content.searchResponse.hotels = [];
      this.session_data.response.content.searchResponse.hotels.push(result);
      console.log(JSON.stringify(this.session_data));
      sessionStorage.removeItem("hotelInfo");
      sessionStorage.setItem("hotelInfo", JSON.stringify(this.session_data.response));
      // websocket.disconnect();
      window.open('hotel-detail.html', '_blank');
    },
    getWeather: function (callback) {

      var weatCity = this.city;
      axios.get('https://api.openweathermap.org/data/2.5/weather?lat=' + JSON.parse(window.sessionStorage.getItem("cityLocation")).lat + '&lon=' + JSON.parse(window.sessionStorage.getItem("cityLocation")).lon + '&units=metric&appid=7c0ff0999be50513159abbf786d93a0a', {
        headers: { 'content-type': 'application/json' }
      }).then(function (response) {
        console.log(response);
        callback(response);
      });
    },
    momGetDate: function (toFormat) {
      return moment().format(toFormat);
    },
    imgUrlAlt(event) {
      event.target.src = "/assets/images/no-image.png"
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;

      getAgencycode(function (response) {
        //var Agencycode = response;
        var Agencycode = 'AGY435';
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        self.dir = langauage == "ar" ? "rtl" : "ltr";
        var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Hotel Results/Hotel Results/Hotel Results.ftl';
        axios.get(aboutUsPage, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          self.getdata = false;
          if (response.data.area_List.length > 0) {
            var mainComp = response.data.area_List[0].main;

            self.CityNameSearch_Label = self.pluckcom('CityNameSearch_Label', mainComp.component);
            self.LocationRange_Label = self.pluckcom('LocationRange_Label', mainComp.component);
            self.LocationRangeUnit_Label = self.pluckcom('LocationRangeUnit_Label', mainComp.component);
            self.RoomSearch_Label = self.pluckcom('RoomSearch_Label', mainComp.component);
            self.AdultSearch_Label = self.pluckcom('AdultSearch_Label', mainComp.component);
            self.ChildrenSearch_Label = self.pluckcom('ChildrenSearch_Label', mainComp.component);
            self.ChildrenAgesSearch_Label = self.pluckcom('ChildrenAgesSearch_Label', mainComp.component);
            self.NumberOfYears_Label = self.pluckcom('NumberOfYears_Label', mainComp.component);
            self.RemoveRoomButton_Label = self.pluckcom('RemoveRoomButton_Label', mainComp.component);
            self.AddRoomButton_Label = self.pluckcom('AddRoomButton_Label', mainComp.component);
            self.DoneSearchButton_Label = self.pluckcom('DoneSearchButton_Label', mainComp.component);
            self.Guest_Label = self.pluckcom('Guest_Label', mainComp.component);
            self.Rooms_Label = self.pluckcom('Rooms_Label', mainComp.component);
            self.SearchButton_Label = self.pluckcom('SearchButton_Label', mainComp.component);
            self.Nights_Label = self.pluckcom('Nights_Label', mainComp.component);
            self.Dates_Label = self.pluckcom('Dates_Label', mainComp.component);
            self.ModifySearch_Label = self.pluckcom('ModifySearch_Label', mainComp.component);
            self.HotelsFound_Label = self.pluckcom('HotelsFound_Label', mainComp.component);
            self.PriceSorting_Label = self.pluckcom('PriceSorting_Label', mainComp.component);
            self.StarSorting_Label = self.pluckcom('StarSorting_Label', mainComp.component);
            self.NameSorting_Label = self.pluckcom('NameSorting_Label', mainComp.component);
            self.SortBy_Label = self.pluckcom('SortBy_Label', mainComp.component);
            self.Weather_Label = self.pluckcom('Weather_Label', mainComp.component);
            self.HotelNameFilter_Label = self.pluckcom('HotelNameFilter_Label', mainComp.component);
            self.PriceFilter_Label = self.pluckcom('PriceFilter_Label', mainComp.component);
            self.LocationFilter_Label = self.pluckcom('LocationFilter_Label', mainComp.component);
            self.StarFilter_Label = self.pluckcom('StarFilter_Label', mainComp.component);
            self.HotelNameFilter_Placeholder = self.pluckcom('HotelNameFilter_Placeholder', mainComp.component);
            self.LocationFilter_Placeholder = self.pluckcom('LocationFilter_Placeholder', mainComp.component);
            self.SelectRoomButton_Label = self.pluckcom('SelectRoomButton_Label', mainComp.component);
            self.AdultCount_Label = self.pluckcom('AdultCount_Label', mainComp.component);
            self.ChildCount_Label = self.pluckcom('ChildCount_Label', mainComp.component);
            self.ChildrenCount_Label = self.pluckcom('ChildrenCount_Label', mainComp.component);
            self.NightsCount_Label = self.pluckcom('NightsCount_Label', mainComp.component);
            self.NoResultsFound_Label = self.pluckcom('NoResultsFound_Label', mainComp.component);
            self.NoResultsDesc_Label = self.pluckcom('NoResultsDesc_Label', mainComp.component);
            self.SearchAgainButton_Label = self.pluckcom('SearchAgainButton_Label', mainComp.component);
            self.MapFilterByHotelName_Label = self.pluckcom('MapFilterByHotelName_Label', mainComp.component);
            self.MapGetDirection_Label = self.pluckcom('MapGetDirection_Label', mainComp.component);
            self.MapDirectionFrom_Label = self.pluckcom('MapDirectionFrom_Label', mainComp.component);
            self.MapDirectionTo_Label = self.pluckcom('MapDirectionTo_Label', mainComp.component);
            self.MapSelectButton_Label = self.pluckcom('MapSelectButton_Label', mainComp.component);


            var tmp1 = self.totalcount.split(",");
            var tmp2 = $.grep(tmp1[0].split(" "), Boolean);
            var tmp3 = $.grep(tmp1[1].split(" "), Boolean);
            self.totalcount = tmp2[0] + " " + self.Guest_Label + ", " + tmp3[0] + " " + self.Rooms_Label;

           

            if (localStorage.direction == 'rtl') {
              alertify.defaults.glossary.title = 'أليرتفاي جي اس';
              alertify.defaults.glossary.ok = 'موافق';
              alertify.defaults.glossary.cancel = 'إلغاء';
              alertify.confirm().set('labels', { ok: 'موافق', cancel: 'إلغاء' });
              alertify.alert().set('label', 'موافق');

            } else {
              alertify.defaults.glossary.title = 'AlertifyJS';
              alertify.defaults.glossary.ok = 'Ok';
              alertify.defaults.glossary.cancel = 'Cancel';
              alertify.confirm().set('labels', { ok: 'Ok', cancel: 'Cancel' });
              alertify.alert().set('label', 'Ok');

            }

            // if (localStorage.direction == 'rtl') {
            //   $.datepicker.setDefaults($.datepicker.regional["ar"]);
            // } else {
            //   $.datepicker.setDefaults($.datepicker.regional["en-GB"]);
            // }
          }
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
      });

    },

  },
  filters: {
    momDToFull: function (date) {
      return moment(date, 'DD-MM-YYYY').format('ddd, DD MMMM YYYY');
    }
  }

});
/**
 * Sort By Price
 */
function comparePrice(a, b) {

  if (Vue_HotelListing.sortOrder) { //ASC
    if (parseFloat(a.rate) < parseFloat(b.rate))
      return -1;
    if (parseFloat(a.rate) > parseFloat(b.rate))
      return 1;
    return 0;
  } else { //DSC
    if (parseFloat(a.rate) > parseFloat(b.rate))
      return -1;
    if (parseFloat(a.rate) < parseFloat(b.rate))
      return 1;
    return 0;
  }

}
/**
 * Sort By Name
 */
function compareName(a, b) {

  if (Vue_HotelListing.sortOrder) { //ASC

    var nameA = a.name.toUpperCase(); // ignore upper and lowercase
    var nameB = b.name.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }

    // names must be equal
    return 0;

  } else { //DSC
    var nameA = a.name.toUpperCase(); // ignore upper and lowercase
    var nameB = b.name.toUpperCase(); // ignore upper and lowercase
    if (nameA > nameB) {
      return -1;
    }
    if (nameA < nameB) {
      return 1;
    }

    // names must be equal
    return 0;
  }

}
/**
 * Sort By Star
 */
function compareStar(a, b) {

  if (Vue_HotelListing.sortOrder) { //ASC
    if (parseInt(a.starRating) < parseInt(b.starRating))
      return -1;
    if (parseInt(a.starRating) > parseInt(b.starRating))
      return 1;
    return 0;
  } else { //DSC
    if (parseInt(a.starRating) > parseInt(b.starRating))
      return -1;
    if (parseInt(a.starRating) < parseInt(b.starRating))
      return 1;
    return 0;
  }

}
function fetchSortResults() {

  Vue_HotelListing.hotelList = [];
  if (Vue_HotelListing.filter_result.length > 0) {

    tot_limit = Vue_HotelListing.filter_result.length;
    if (tot_limit >= 10) {
      for (let index = 0; index < 10; index++) {
        Vue_HotelListing.hotelList.push(Vue_HotelListing.filter_result[index]);
      }
    } else if (tot_limit < 10) {
      for (let index = 0; index < tot_limit; index++) {
        Vue_HotelListing.hotelList.push(Vue_HotelListing.filter_result[index]);

      }
    }
  }
}
/**
 * used for binding hotel results according to the search criteria
 */
function fetchFilterResults() {

  Vue_HotelListing.NoResult = false;
  if (Vue_HotelListing.filter_result.length > 0) {

    tot_limit = Vue_HotelListing.filter_result.length;
    if (tot_limit >= 10) {
      for (let index = Vue_HotelListing.startLimit; index < Vue_HotelListing.limit; index++) {
        Vue_HotelListing.hotelList.push(Vue_HotelListing.filter_result[index]);
      }
      Vue_HotelListing.startLimit = Vue_HotelListing.limit;
      Vue_HotelListing.limit = Vue_HotelListing.limit + 10;
    } else if (tot_limit < 10) {
      for (let index = Vue_HotelListing.startLimit; index < tot_limit; index++) {
        Vue_HotelListing.hotelList.push(Vue_HotelListing.filter_result[index]);
      }
    }
  } else {
    Vue_HotelListing.NoResultHead = 'No results found for the provided search conditions';
    Vue_HotelListing.NoResultDesc = "";
    Vue_HotelListing.NoResult = true;

  }

  Vue_HotelListing.totalResult_Count = Vue_HotelListing.filter_result.length;
}
/**
 * Used for binding hotel results without any Filter criteria
 */
function fetchAllResults() {

  Vue_HotelListing.NoResult = false;
  if (Vue_HotelListing.total_result.length > 0) {
    tot_limit = Vue_HotelListing.total_result.length;
    if (tot_limit <= 10) {
      for (let index = Vue_HotelListing.startLimit; index < tot_limit; index++) {
        Vue_HotelListing.hotelList.push(Vue_HotelListing.total_result[index]);
        Vue_HotelListing.startLimit = tot_limit;
      }
    } else {
      for (let index = Vue_HotelListing.startLimit; index < Vue_HotelListing.limit; index++) {
        Vue_HotelListing.hotelList.push(Vue_HotelListing.total_result[index]);
      }
      Vue_HotelListing.startLimit = Vue_HotelListing.limit;
      Vue_HotelListing.limit = Vue_HotelListing.limit + 10;
    }
  } else {
    Vue_HotelListing.NoResultHead = 'No results found for the provided search conditions';
    Vue_HotelListing.NoResultDesc = "";
    Vue_HotelListing.NoResult = true;

  }
  Vue_HotelListing.totalResult_Count = Vue_HotelListing.total_result.length;

  console.log("All results:" + Vue_HotelListing.total_result.length);
}
function GetParameterValues(param) {
  var QData = 'NODATA';
  var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  for (var i = 0; i < url.length; i++) {
    var urlparam = url[i].split('=');
    if (urlparam[0] == param) {
      //QData = urlparam[1].replace('#', '');
      QData = urlparam[1].replace(/\#$/, '');
      QData.replace('%20', ' ');
      return QData;
    }
  }
  return QData;
}
/**
 * Load data while scrolling down
 */

$(window).scroll(function () {
  if (Math.floor(document.documentElement.scrollTop) + window.innerHeight >= document.documentElement.offsetHeight - 10) {


    if (!Vue_HotelListing.isFiltered) {

      if (Vue_HotelListing.limit < Vue_HotelListing.total_result.length) {

        for (let index = Vue_HotelListing.startLimit; index < Vue_HotelListing.limit; index++) {
          Vue_HotelListing.hotelList.push(Vue_HotelListing.total_result[index]);
        }
        Vue_HotelListing.startLimit = Vue_HotelListing.limit;
        Vue_HotelListing.limit = Vue_HotelListing.limit + 10;

      } else if (Vue_HotelListing.hotelList.length < Vue_HotelListing.total_result.length) {
        Vue_HotelListing.limit = Vue_HotelListing.total_result.length;
        for (let index = Vue_HotelListing.startLimit; index < Vue_HotelListing.limit; index++) {
          Vue_HotelListing.hotelList.push(Vue_HotelListing.total_result[index]);
        }
      }

      console.log(Vue_HotelListing.hotelList.length);
      console.log(Vue_HotelListing.total_result.length);
    } else {

      if (Vue_HotelListing.limit < Vue_HotelListing.filter_result.length) {
        for (let index = Vue_HotelListing.startLimit; index < Vue_HotelListing.limit; index++) {
          Vue_HotelListing.hotelList.push(Vue_HotelListing.filter_result[index]);
        }
        Vue_HotelListing.startLimit = Vue_HotelListing.limit;
        Vue_HotelListing.limit = Vue_HotelListing.limit + 10;

      } else if (Vue_HotelListing.hotelList.length < Vue_HotelListing.filter_result.length) {
        Vue_HotelListing.limit = Vue_HotelListing.filter_result.length;
        for (let index = Vue_HotelListing.startLimit; index < Vue_HotelListing.limit; index++) {
          Vue_HotelListing.hotelList.push(Vue_HotelListing.filter_result[index]);
        }
      }
      console.log(Vue_HotelListing.hotelList.length);
      console.log(Vue_HotelListing.filter_result.length);
    }

  }
});

/**Sorting Function for AutoCompelte Start**/
function SortInputFirst(input, data) {
  var first = [];
  var others = [];
  for (var i = 0; i < data.length; i++) {
    if (data[i].supplier_city_name.toLowerCase().startsWith(input.toLowerCase())) {
      first.push(data[i]);
    } else {

      others.push(data[i]);
    }
  }
  first.sort(function (a, b) {
    var nameA = a.supplier_city_name.toLowerCase(), nameB = b.supplier_city_name.toLowerCase()
    if (nameA < nameB) //sort string ascending
      return -1
    if (nameA > nameB)
      return 1
    return 0 //default return value (no sorting)
  })
  others.sort(function (a, b) {
    var nameA = a.supplier_city_name.toLowerCase(), nameB = b.supplier_city_name.toLowerCase()
    if (nameA < nameB) //sort string ascending
      return -1
    if (nameA > nameB)
      return 1
    return 0 //default return value (no sorting)
  })
  return (first.concat(others));
}

/**Sorting Function for AutoCompelte End**/
/**UUID Generation Start**/
function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}
  /**UUID Generation End**/


