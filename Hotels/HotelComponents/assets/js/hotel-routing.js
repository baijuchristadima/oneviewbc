var cHotelDetail = httpVueLoader("/Hotels/HotelComponents/hotel-detail.vue");
var cHotelBooking = httpVueLoader("/Hotels/HotelComponents/hotel-booking.vue");
var cHotelPayment = httpVueLoader("/Hotels/HotelComponents/hotel-payment.vue");
var cHotelConfirmation = httpVueLoader(
  "/Hotels/HotelComponents/hotel-confirmation.vue"
);

const routes = [
  { path: "/", component: cHotelDetail },
  { path: "/hotelBooking", component: cHotelBooking },
  { path: "/hotelPayment", component: cHotelPayment },
  { path: "/hotelConfirmation", component: cHotelConfirmation },
  { path: '*', component: cHotelDetail }
];
const router = new VueRouter({
  routes, // short for `routes: routes`
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
});

const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})

var el_hotelDetail = new Vue({
  i18n,
  el: "#el_hotelDetail",
  router,
  data: {
    headerFooterVisible: true
  },
  mounted: function () {
    if (localStorage.direction == 'rtl') {
      alertify.defaults.glossary.title = 'أليرتفاي جي اس';
      alertify.defaults.glossary.ok = 'موافق';
      alertify.defaults.glossary.cancel = 'إلغاء';
      alertify.confirm().set('labels', { ok: 'موافق', cancel: 'إلغاء' });
      alertify.alert().set('label', 'موافق');

    } else {
      alertify.defaults.glossary.title = 'AlertifyJS';
      alertify.defaults.glossary.ok = 'Ok';
      alertify.defaults.glossary.cancel = 'Cancel';
      alertify.confirm().set('labels', { ok: 'Ok', cancel: 'Cancel' });
      alertify.alert().set('label', 'Ok');

    }
  }
});


var store = {
  state: {
    roomsRQ: {}, // done
    roomsRS: {}, // done
    hotelDetailsRQ: {},
    hotelDetailsRS: {},
    prebookCancellationRQ: {}, //done
    prebookCancellationRS: {}, // done
    prebookAvailabilityRQ: {}, //done
    prebookAvailabilityRS: {},  //done
    bookRQ: {},
    bookRS: {},
    selectedHotel: {},
    selectedRooms: {}, // done
    paxDetails: {}, // done
    nodeDetails: {},
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
    currencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1)
  },
  updateState(key, data) {
    console.log(key + " updates...")
    this.state[key] = _.cloneDeep(data);
  }
}