module.exports = {
  mixins: [{
    methods: {
      getCancellationPolicy: function (hotelBookingInformation) {
        var hotelCancellationPolicy = hotelBookingInformation.booking.hotels[0].cancellationPolicyList;
        var cancellationOutput = [];
        var rooms = hotelBookingInformation.booking.hotels[0].rooms;

        if (hotelCancellationPolicy.length > 0) {
          var cancellationOutputHotel = [];
          for (var iP = 0; iP < rooms.length; iP++) {
            var xPolicy = rooms[iP].roomQuotes;
            for (var iarr = 0; iarr < xPolicy.length; iarr++) {
              for (var i = 0; i < xPolicy[iarr].cancellationPolicyList.length; i++) {
                var totalRoomPrice = + xPolicy[iarr].debitDetails.sellAmount;
              }
            }
          }

          for (var i = 0; i < hotelCancellationPolicy.length; i++) {
            var from = moment(hotelCancellationPolicy[i].fromDate).format("MMM DD YYYY, dddd");
            var to = moment(hotelCancellationPolicy[i].toDate).format("MMM DD YYYY, dddd");
            var amount = i18n.n((hotelCancellationPolicy[i].totalCost / store.state.currencyMultiplier), 'currency', store.state.selectedCurrency);

            if (parseInt(hotelCancellationPolicy[i].totalCost) == 0) {
              cancellationOutputHotel.push('Free cancellation on or before ' + to);
            } else if (totalRoomPrice == amount) {
              cancellationOutputHotel.push('Non refundable booking with cancellation charges ' + amount);
            } else if (to != "" && from != "") {
              cancellationOutputHotel.push("Cancellation between " + from + " and " + to + " will be charged " + amount);
            }
          }

          cancellationOutput.push(cancellationOutputHotel);
        } else {
          for (var iP = 0; iP < rooms.length; iP++) {
            var xPolicy = rooms[iP].roomQuotes;
            var cancellationOutputPerRoom = [];
            for (var iarr = 0; iarr < xPolicy.length; iarr++) {
              for (var i = 0; i < xPolicy[iarr].cancellationPolicyList.length; i++) {
                var from = moment(xPolicy[iarr].cancellationPolicyList[i].fromDate).format("MMM DD YYYY, dddd");
                var to = moment(xPolicy[iarr].cancellationPolicyList[i].toDate).format("MMM DD YYYY, dddd");
                var amount = i18n.n((xPolicy[iarr].cancellationPolicyList[i].totalCost / store.state.currencyMultiplier), 'currency', store.state.selectedCurrency);
                var roomPrice = xPolicy[iarr].debitDetails.sellAmount;

                if (parseInt(xPolicy[iarr].cancellationPolicyList[i].totalCost) == 0) {
                  cancellationOutputPerRoom.push('Free cancellation on or before ' + to);
                } else if (roomPrice == amount) {
                  cancellationOutputPerRoom.push('Non refundable booking with cancellation charges ' + amount);
                } else if (to != "" && from != "") {
                  cancellationOutputPerRoom.push("Cancellation between " + from + " and " + to + " will be charged " + amount);
                }
              }
            }
            cancellationOutput.push(cancellationOutputPerRoom);
          }
        }
        return cancellationOutput;
      },
      getCancellationPolicyWithI18n: function (hotelBookingInformation, labels) {

        var hotelCancellationPolicy = hotelBookingInformation.booking.hotels[0].cancellationPolicyList;
        var cancellationOutput = [];
        var rooms = hotelBookingInformation.booking.hotels[0].rooms;

        if (hotelCancellationPolicy.length > 0) {
          var cancellationOutputHotel = [];
          for (var iP = 0; iP < rooms.length; iP++) {
            var xPolicy = rooms[iP].roomQuotes;
            for (var iarr = 0; iarr < xPolicy.length; iarr++) {
              for (var i = 0; i < xPolicy[iarr].cancellationPolicyList.length; i++) {
                var totalRoomPrice = + xPolicy[iarr].debitDetails.sellAmount;
              }
            }
          }

          for (var i = 0; i < hotelCancellationPolicy.length; i++) {
            var from = moment(hotelCancellationPolicy[i].fromDate).format("MMM DD YYYY, dddd");
            var to = moment(hotelCancellationPolicy[i].toDate).format("MMM DD YYYY, dddd");
            var amount = i18n.n((hotelCancellationPolicy[i].totalCost / store.state.currencyMultiplier), 'currency', store.state.selectedCurrency);

            if (parseInt(hotelCancellationPolicy[i].totalCost) == 0) {
              cancellationOutputHotel.push(labels.CancellationTextC1_Label + ' ' + to);
            } else if (totalRoomPrice == amount) {
              cancellationOutputHotel.push(labels.CancellationTextB1_Label + ' ' + amount);
            } else if (to != "" && from != "") {
              cancellationOutputHotel.push(labels.CancellationTextA1_Label + " " + from + " " + labels.CancellationTextA2_Label + " " + to + " " + labels.CancellationTextA3_Label + " " + amount);
            }
          }

          cancellationOutput.push(cancellationOutputHotel);
        } else {
          for (var iP = 0; iP < rooms.length; iP++) {
            var xPolicy = rooms[iP].roomQuotes;
            var cancellationOutputPerRoom = [];
            for (var iarr = 0; iarr < xPolicy.length; iarr++) {
              for (var i = 0; i < xPolicy[iarr].cancellationPolicyList.length; i++) {
                var from = moment(xPolicy[iarr].cancellationPolicyList[i].fromDate).format("MMM DD YYYY, dddd");
                var to = moment(xPolicy[iarr].cancellationPolicyList[i].toDate).format("MMM DD YYYY, dddd");
                var amount = i18n.n((xPolicy[iarr].cancellationPolicyList[i].totalCost / store.state.currencyMultiplier), 'currency', store.state.selectedCurrency);
                var roomPrice = xPolicy[iarr].debitDetails.sellAmount;

                if (parseInt(xPolicy[iarr].cancellationPolicyList[i].totalCost) == 0) {
                  cancellationOutputPerRoom.push(labels.CancellationTextC1_Label + ' ' + to);
                } else if (roomPrice == amount) {
                  cancellationOutputPerRoom.push(labels.CancellationTextB1_Label + ' ' + amount);
                } else if (to != "" && from != "") {
                  cancellationOutputPerRoom.push(labels.CancellationTextA1_Label + " " + from + " " + labels.CancellationTextA2_Label + " " + to + " " + labels.CancellationTextA3_Label + " " + amount);
                }
              }
            }
            cancellationOutput.push(cancellationOutputPerRoom);
          }
        }
        return cancellationOutput;
      }
    }
  }],
  data: function () {
    return {
      bookingInformation: {},
      header: {
        leadTravellerName: null,
        bookingStatus: null,
        agencyReference: null,
        bookingStatusCode: null
      },
      hotelDetails: {
        supplierReferenceNumber: null,
        hotelName: null,
        starRating: null,
        hotelAddress: null,
        hotelPhoneNo: null,
        cInDate: {
          day: null,
          fulldate: null
        },
        cOutDate: {
          day: null,
          fulldate: null
        },
        bookDate: null,
        voucherPaxDetails: null
      },
      roomDetails: [{
        roomType: null,
        meal: null,
        traveller: null,
        guestName: null,
        inclusions: null
      }],
      guestDetails: {
        leadTravellerName: null,
        guestEmail: null,
        title: "Mr"
      },
      paymentDetails: {
        roomCharges: null,
        vat: null,
        totalAmount: null
      },

      cancellationDetails: {
        cancellationPolicy: null,
        cancellationParsed: null,
        cancellationParsedEmail: null,
        amendmentPolicy: null
      },
      bookingNotes: "",
      supportDetails: {
        address: "",
        phone: "",
        email: ""
      },
      pageControls: {
        callToActionButtons: [{
          show: false,
          label: "Cancel",
          action: this.cancelBooking
        },
        {
          show: false,
          label: "Request Cancellation",
          action: this.requestOfflineCancellation
        },
        {
          show: false,
          label: "Download Voucher",
          action: this.downloadVoucher
        },
        {
          show: false,
          label: "Email Voucher",
          action: this.emailVoucher
        },
        {
          show: false,
          label: "Edit Voucher",
          action: this.editVoucher
        }
        ]
      },
      isRetrieveBooking: false,
      divHtlBkngFail: false,
      divHtlBkngSucces: false,
      sharedStateHotel: store.state,
      sendVoucher: false,
      logoUrl: null,
      phoneNumber: null,
      DearHeader_Label: "",
      BookingSuccessHeader: "",
      BookingFailureHeader: "",
      BookingReferenceNumber_Label: "",
      BookingEmailNotes_Label: "",
      SupplierReferenceNumber_Label: "",
      CheckOut_Label: "",
      CheckIn_Label: "",
      HotelDetails_Label: "",
      GuestName_Label: "",
      Guests_Label: "",
      RoomType_Label: "",
      RoomNumber_Label: "",
      GuestDetails_Label: "",
      PaymentDetails_Label: "",
      TotalRoomCharges_Label: "",
      Tax_Label: "",
      TotalPrice_Label: "",
      CancellationPolicy_Label: "",
      DetailedCancellationPolicy_Label: "",
      BookingNotes_Label: "",
      CustomerSupport_Label: "",
      Email_Label: "",
      Phone_Label: "",
      Address_Label: "",
      CancellationTextA1_Label: "",
      CancellationTextA2_Label: "",
      CancellationTextA3_Label: "",
      CancellationTextB1_Label: "",
      CancellationTextC1_Label: "",
      CancellationTextC2_Label: "",
      CancellationNotes_Label: "",
      Room_Label: "",
      NoInformation_Label: "",
      Adults_Label: "",
      Child_Label: "",
      Children_Label: "",
      ReconfirmedStatus_Label: "",
      RquestedStatus_Label: "",
      FailedStatus_Label: "",
      currentLanguage: localStorage.direction,
      showButtons: true,
      showSpinnerHtml: false
    };
  },
  computed: {
    activeCalltoActionButtons: function () {
      this.callToActionButtons.filter(function (item, index) {

      });
    }
  },
  watch: {
    "sharedStateHotel.selectedCurrency": function () {
      var labels = {
        CancellationTextA1_Label: this.CancellationTextA1_Label,
        CancellationTextA2_Label: this.CancellationTextA2_Label,
        CancellationTextA3_Label: this.CancellationTextA3_Label,
        CancellationTextB1_Label: this.CancellationTextB1_Label,
        CancellationTextC1_Label: this.CancellationTextC1_Label,
        CancellationTextC2_Label: this.CancellationTextC2_Label

      }
      this.cancellationDetails.cancellationParsed = this.getCancellationPolicyWithI18n(this.cancellationDetails.cancellationPolicy, labels);
    },
    currentLanguage: function () {
      var labels = {
        CancellationTextA1_Label: this.CancellationTextA1_Label,
        CancellationTextA2_Label: this.CancellationTextA2_Label,
        CancellationTextA3_Label: this.CancellationTextA3_Label,
        CancellationTextB1_Label: this.CancellationTextB1_Label,
        CancellationTextC1_Label: this.CancellationTextC1_Label,
        CancellationTextC2_Label: this.CancellationTextC2_Label

      }
      this.cancellationDetails.cancellationParsed = this.getCancellationPolicyWithI18n(this.cancellationDetails.cancellationPolicy, labels);
    }
  },
  created() {
     
    this.getPagecontent();
    //after pg redirect
    var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
    this.phoneNumber = LoggedUser.loginNode.phoneList[0].number;
    // this.logoUrl = ServiceUrls.hubConnection.logoBaseUrl + LoggedUser.loginNode.logo + '.xhtml?ln=logo';
    this.logoUrl = LoggedUser.loginNode.logo;
    console.log("after pg redirect", localStorage.access_token);

    // Get data from session storage
    var userAction = window.sessionStorage.getItem("userAction");

    //Check if payment gateway or credit
    var session = JSON.parse(window.sessionStorage.getItem("hotelInfo"));

    var continueBooking = false;
    if (userAction == "7" || userAction == "6") {
      
      if (userAction == "6" && GetParameterByName("status") == 101) {
        continueBooking = true;
      } else if (userAction == "7") {
        continueBooking = true;
      }
    }
    if (continueBooking) {
      // Get data from session storage

      var searchCriteria = session.content.supplierSpecific.criteria;
      var selectedRooms = session.content.searchResponse.hotels[0].roomResponse.selectedRooms;
      var hotelDetails = session.content.searchResponse.hotels[0];

      var paxDetails = [];
      var paxPerRoom = [];
      var paxId = 1;
      for (var roomIndex = 0; roomIndex < searchCriteria.rooms.length; roomIndex++) {
        var guests = searchCriteria.rooms[roomIndex].guests;
        var paxes = [];
        for (var guestIndex = 0; guestIndex < guests.adult; guestIndex++) {
          paxes.push(paxId);
          paxId++;

        }
        if (guests.children != undefined && guests.children.length > 0) {

          for (var guestIndex = 0; guestIndex < guests.children.length; guestIndex++) {
            paxes.push(paxId);
            paxId++;

          }
          paxDetails.push({
            adult: guests.adult,
            child: guests.children.length
          });
        } else {
          paxDetails.push({
            adult: guests.adult,
            child: 0
          });

        }
        paxPerRoom.push(paxes);
      }

      var rooms = [];

      for (var index = 0; index < selectedRooms.length; index++) {
        var room = selectedRooms[index].room;
        rooms.push({
          name: room.roomName,
          roomId: room.roomId,
          paxes: paxPerRoom[index], // from pax page
          guests: room.guests, // from search
          rate: {
            bookable: true,
            cancellationPolicies: room.rates[0].cancellationPolicies,
            rateSpecific: room.rates[0].roomRateSpecific,
            pricing: {
              perNightPrice: room.rates[0].pricing.perNightPrice,
              totalPrice: room.rates[0].pricing.totalPrice
            },
            roomRateReferences: room.rates[0].roomRateSpecific ? room.rates[0].roomRateSpecific.roomRateReferences : undefined
          },
          roomReferences: room.roomSpecific ? room.roomSpecific.roomReferences : undefined,
          roomPrebookReference: hotelDetails.prebookResponse.hotel.rooms[index].roomSpecific ? hotelDetails.prebookResponse.hotel.rooms[index].roomSpecific.roomPrebookReference : undefined
        });
      }

      var paxDetails1 = JSON.parse(window.sessionStorage.getItem("paxDetails"));
      var vm = this;
      var paxes = [];
      for (var paxIndex = 0; paxIndex < paxDetails1.pax.length; paxIndex++) {
        for (var roomIndex = 0; roomIndex < paxDetails1.pax[paxIndex].paxPerRoom.length; roomIndex++) {
          var pax = _.cloneDeep(paxDetails1.pax[paxIndex].paxPerRoom[roomIndex]);
          pax.email = paxDetails1.contactDetails.emailID;
          pax.phoneNumber = paxDetails1.contactDetails.phoneNumber;
          delete pax.label;
          paxes.push(pax);
        }
      }

      var BookRQ = {
        service: "HotelRQ",
        node: session.node,
        token: localStorage.access_token,
        hubUuid: session.hubUuid,
        supplierCodes: ["" + session.supplierCode],
        credentials: [],
        content: {
          command: "BookRQ",
          hotel: {
            cityCode: hotelDetails.location.cityCode,
            hotelCode: hotelDetails.hotelCode,
            totalAmount: parseFloat(hotelDetails.prebookResponse.currentPrice), // total for hotel?
            sealCode: hotelDetails.prebookResponse.sealCode, //hotelDetails.hotel.sealCode, // what sealcode to use?
            nationality: session.content.searchResponse.nationality,
            residentOf: session.content.searchResponse.residentOf,
            rooms: rooms,
            hotelCancellationPolicies: hotelDetails.prebookResponse.hotel.hotelCancellationPolicies, // from prebook
            hotelSpecific: hotelDetails.hotelSpecific
          },
          paxes: paxes,
          stay: session.content.supplierSpecific.criteria.stay,
          ovBookingReference: session.content.searchResponse.bookingId,
          supplierSpecific: {
            uuid: localStorage.getItem("hotelUUID"),
            searchReferences: session.content.searchResponse.supplierSpecific ? session.content.searchResponse.supplierSpecific.searchReferences : undefined,
            searchHotelReferences: hotelDetails.hotelSpecific ? hotelDetails.hotelSpecific.searchHotelReferences : undefined,
            roomsReferences: hotelDetails.roomSpecific ? hotelDetails.roomSpecific.roomReferences : undefined,
            prebookReferences: hotelDetails.prebookResponse.supplierSpecific ? hotelDetails.prebookResponse.supplierSpecific.prebookReferences : undefined
          }
        }
      };
      var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;
      var vm = this;
      axios({
        method: "post",
        url: hubUrl + "/hotelBook",
        data: {
          request: BookRQ
        },
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.access_token
        }
      })
        .then(response => {
          console.log(response.data);

          // if (response.data.response.content.bookResponse.hotel.statusCode == "HK") {

            //Update Status
            this.bookingStatusUpdate(response.data.response.content.bookResponse.bookingReferences.oneviewReference, response.data.response.content.bookResponse.hotel.statusCode);

          // } else {
            //Show booking failed
            // this.getHotelBookingInformation(response.data.response.content.bookResponse.bookingReferences.oneviewReference);
          // }
          //Clear booking identifier
          window.sessionStorage.setItem("userAction", response.data.response.content.bookResponse.bookingReferences.oneviewReference);

        })
        .catch(error => {
          if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            vm.bookingStatusUpdate(session.content.searchResponse.bookingId, "X");
            alertify.alert("Error", error.response.data.message)
            console.log("Server error: ", error.response.data);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log("No response received: ", error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log("Error", error.message);
          }
        })
        .finally(() => {
          console.log("Finally");
        });

    } else {
      this.isRetrieveBooking = true;
      if (userAction == "6" && GetParameterByName("status") == 102) {
        this.bookingStatusUpdate(session == undefined || (session && session.content.searchResponse.bookingId == undefined) ? userAction : session.content.searchResponse.bookingId, "X");
      }else {
        this.getHotelBookingInformation(session == undefined || (session && session.content.searchResponse.bookingId == undefined) ? userAction : session.content.searchResponse.bookingId);
      }
    }

  },
  methods: {
    emailBookingDetails: function (to) {
      var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
      try {
        var main = this.pluckcom('main', JSON.parse(window.localStorage.getItem("cmsContent")).area_List);
        this.logoUrl = this.pluckcom('Logo', main.component);
      }
      catch (error) {
        // this.logoUrl = ServiceUrls.hubConnection.logoBaseUrl + LoggedUser.loginNode.logo + '.xhtml?ln=logo';
        this.logoUrl = LoggedUser.loginNode.logo;
      }
      var vm = this;
      var totalRooms = vm.roomDetails.length;
      var totalChildren = vm.roomDetails.reduce(function(a,c){return a + c.children}, 0);
      var totalAdults = vm.roomDetails.reduce(function(a,c){return a + c.adult}, 0);
      var postData = {
        agencyPhone: vm.supportDetails.phone || "",
        agencyEmail: vm.supportDetails.email || "",
        logoUrl: vm.logoUrl,
        fullName: vm.header.leadTravellerName,
        bookingRefId: vm.header.agencyReference,
        hotelRef: vm.hotelDetails.supplierReferenceNumber || "Not Available",
        hotelName: vm.hotelDetails.hotelName,
        hotelAddress: decodeURI(vm.hotelDetails.hotelAddress),
        nights: vm.hotelDetails.bookingInformation.booking.hotels[0].minimumNights + (vm.hotelDetails.bookingInformation.booking.hotels[0].minimumNights > 1 ? ' Nights': ' Night'),
        adults: totalAdults + (totalAdults > 1 ? ' Adults' : ' Adult'),
        child: totalChildren + (totalChildren > 1 ? ' Children' : ' Child'),
        room: totalRooms + (totalRooms > 1 ? ' Rooms' : ' Room'),
        checkIn: vm.hotelDetails.cInDate.fullDate,
        checkOut: vm.hotelDetails.cOutDate.fullDate,
        roomDetails: vm.roomDetails.map(function(e, i) {
          return {
            name: e.roomType,
            pax: e.allNames
          }
        }),
      }
      vm.getTemplate("HotelEmailTemplate").then(function (templateResponse) {
        var data = templateResponse.data.data;
        var emailTemplate = "";
        if (data.length > 0) {
            for (var x = 0; x < data.length; x++) {
                if (data[x].enabled == true && data[x].type == "HotelEmailTemplate") {
                    emailTemplate = data[x].content;
                    break;
                }
            }
        };
        var htmlGenerate =  ServiceUrls.emailServices.htmlGenerate
        var emailData = {
            template: emailTemplate,
            content: postData
        };
        var ccEmails = _.filter(LoggedUser.loginNode.emailList, function (o) { return o.emailTypeId == 8 && o.emailType.toLowerCase() == 'hotel confirmation'; });
        ccEmails = _.map(ccEmails, function (o) { return o.emailId});
    
          var toEmails = "";
          if (to) {
            toEmails = to;
          } else {
            toEmails = Array.isArray(vm.guestDetails.guestEmail) ? vm.guestDetails.guestEmail : [vm.guestDetails.guestEmail];
          }
        axios.post(htmlGenerate, emailData)
        .then(function (htmlResponse) {
            vm.getPdf().then(function(response){
                var emailPostData = {
                    type: "AttachmentRequest",
                    toEmails: toEmails,
                    fromEmail: LoggedUser.loginNode.email || "itsolutionsoneview@gmail.com",
                    ccEmails: ccEmails,
                    bccEmails: null,
                    subject: "Hotel Confirmation - " + vm.header.agencyReference,
                    attachmentPath: response.data,
                    html: htmlResponse.data.data
                };
                var mailUrl = ServiceUrls.emailServices.emailApiWithAttachment;
                sendMailService(mailUrl, emailPostData);
                if (to) {
                  //
                }
            });
            
        }).catch(function (error) {
            return false;
        })
    });
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getTemplate(template){
      var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
        var url = ServiceUrls.emailServices.getTemplate + "AGY75/" + template;
        // var url = ServiceUrls.emailServices.getTemplate + LoggedUser.loginNode.code + "/" + template;
        return axios.get(url);
    },
    getPdf(button) {
      if (button) {
        this.showSpinnerHtml = true;
      }
      var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
      try {
        var main = this.pluckcom('main', JSON.parse(window.localStorage.getItem("cmsContent")).area_List);
        this.logoUrl = this.pluckcom('Logo', main.component);
      }
      catch (error) {
        // this.logoUrl = ServiceUrls.hubConnection.logoBaseUrl + LoggedUser.loginNode.logo + '.xhtml?ln=logo';
        this.logoUrl =  LoggedUser.loginNode.logo ;
      }
      var vm = this;
      var totalRooms = vm.roomDetails.length;
      var totalChildren = vm.roomDetails.reduce(function(a,c){return a + c.children}, 0);
      var totalAdults = vm.roomDetails.reduce(function(a,c){return a + c.adult}, 0);
      var postData = {
        logoUrl: vm.logoUrl,
        agencyPhone: vm.supportDetails.phone || "",
        agencyAddress: vm.supportDetails.address || "",
        agencyEmail:vm.supportDetails.email || "",
        bookStatusImg: window.location.origin + (vm.divHtlBkngSucces ? "/assets/images/con-hotel.png" : "/assets/images/con-hote-errorl.png"),
        leadPaxName: vm.header.leadTravellerName,
        statusText: vm.divHtlBkngSucces ? "Congratulations! Your hotel booking is confirmed" : "Sorry! Your hotel booking failed",
        bookRef: vm.header.agencyReference,
        hotelRef: vm.hotelDetails.supplierReferenceNumber || "Not Available",
        hotelName: vm.hotelDetails.hotelName,
        star: Array(vm.hotelDetails.starRating).fill(0),
        noStar: Array(5 - vm.hotelDetails.starRating).fill(0),
        hotelAddress: decodeURI(vm.hotelDetails.hotelAddress),
        checkInDay: vm.hotelDetails.cInDate.day,
        checkIndate: vm.hotelDetails.cInDate.fullDate,
        checkOutDay: vm.hotelDetails.cOutDate.day,
        checkOutDate: vm.hotelDetails.cOutDate.fullDate,
        nights: vm.hotelDetails.bookingInformation.booking.hotels[0].minimumNights + (vm.hotelDetails.bookingInformation.booking.hotels[0].minimumNights > 1 ? ' Nights': ' Night'),
        totalAdults: totalAdults + (totalAdults > 1 ? ' Adults' : ' Adult'),
        totalChildren: totalChildren + (totalChildren > 1 ? ' Children' : ' Child'),
        totalRooms: totalRooms + (totalRooms > 1 ? ' Rooms' : ' Room'),
        rooms: vm.roomDetails.map(function(e, i) {
          return {
            id: "Room " + (i + 1),
            name: e.roomType,
            pax: e.allNames
          }
        }),
        roomCharge: i18n.n(vm.paymentDetails.roomCharges / vm.sharedStateHotel.currencyMultiplier, 'currency', vm.sharedStateHotel.selectedCurrency),
        tax: i18n.n(0 / vm.sharedStateHotel.currencyMultiplier, 'currency', vm.sharedStateHotel.selectedCurrency),
        totalPrice: i18n.n(vm.paymentDetails.totalAmount / vm.sharedStateHotel.currencyMultiplier, 'currency', vm.sharedStateHotel.selectedCurrency),
        cancellationPolicy: vm.cancellationDetails.cancellationParsedEmail.map(function(e, i) {
          return {
            id: vm.cancellationDetails.cancellationParsedEmail.length > 1 ? "Room " + (i + 1) : undefined,
            cancellation: e
          }
        }),
        supplierRemarks: vm.bookingNotes
      }
      return vm.getTemplate("HotelPDFTemplate").then(function(response){
        var data = response.data.data;
        var pdfTemplateId = "";
        var pdfFileName = "";
        if (data.length > 0) {
            for (var x = 0; x < data.length; x++) {
                if (data[x].enabled == true && data[x].type == "HotelPDFTemplate") {
                    pdfTemplateId = data[x].id;
                    pdfFileName = data[x].nodeCode;
                }
            }
        };
        var generatePdf = ServiceUrls.emailServices.generatePdf
        var pdfData = {
            templateID: pdfTemplateId,
            filename: pdfFileName,
            content:  postData
        };
        return axios.post(generatePdf, pdfData)
        .then(function (response) {
          if (button) {

            vm.showSpinnerHtml = false;
            vm.downloadPdf(response.data)
          } else {
            return response.data;
          }
        }).catch(function (error) {
          if (button) {
            vm.showSpinnerHtml = false;
          } else {
            return false;
          }
        })
      }).catch(function (error) {
        if (button) {
          vm.showSpinnerHtml = false;
        } else {
          return false;
        }
      });
    },
    downloadPdf: function(response){
        try {
            var link = document.createElement('a');
            link.href = response.data;
            link.download = response.data.split('/').pop();
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            this.processingPdf = false;
        } catch (error) {
            alertify.alert("Error", "Error in generating pdf file.").set('label', 'Ok');
            this.processingPdf = false;
        }
    },
    cancelBooking() {
      var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;

      axios.get(hubUrl + "/api/values")
        .then(response => {
          console.log(response.data);
        })
        .catch(error => {
          if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            alertify.alert("Error", error.response.data.message)
            console.log("Server error: ", error.response.data);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log("No response received: ", error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log("Error", error.message);
          }
        })
        .finally(() => {
          console.log("Finally");
        });
    },
    requestOfflineCancellation() {
      var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;

      axios.get(hubUrl + "/api/values")
        .then(response => {
          console.log(response.data);
        })
        .catch(error => {
          if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            alertify.alert("Error", error.response.data.message)
            console.log("Server error: ", error.response.data);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log("No response received: ", error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log("Error", error.message);
          }
        })
        .finally(() => {
          console.log("Finally");
        });
    },
    emailVoucher() {
      var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;

      axios.get(hubUrl + "/api/values")
        .then(response => {
          console.log(response.data);
        })
        .catch(error => {
          if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            alertify.alert("Error", error.response.data.message)
            console.log("Server error: ", error.response.data);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log("No response received: ", error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log("Error", error.message);
          }
        })
        .finally(() => {
          console.log("Finally");
        });
    },
    editVoucher() {
      var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;

      axios.get(hubUrl + "/api/values")
        .then(response => {
          console.log(response.data);
        })
        .catch(error => {
          if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            alertify.alert("Error", error.response.data.message)
            console.log("Server error: ", error.response.data);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log("No response received: ", error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log("Error", error.message);
          }
        })
        .finally(() => {
          console.log("Finally");
        });
    },
    printVoucher() {
     
      var vm = this;
      var URL = "/Hotels/EmailTemplates/booking/printVoucher.html";
      axios.get(URL)
        .then(response => {
          var printBody = response.data;
          var tableDetails = "";
          for (var i = 0; i < vm.roomDetails.length; i++) {
            tableDetails += '<tr>';
            tableDetails += '<td style="color: #555555;padding: 8px;line-height: 1.42857143;vertical-align: top;">' + (i + 1) + '</td>';
            tableDetails += '<td style="color: #555555;max-width: 220px;padding: 8px;line-height: 1.42857143;vertical-align: top;">' + vm.roomDetails[i].roomType + ' - ' + vm.roomDetails[i].meal + '</td>';
            tableDetails += '<td style="color: #555555;padding: 8px;line-height: 1.42857143;vertical-align: top;">' + vm.roomDetails[i].traveller + '</td>';
            tableDetails += '<td style="color: #555555;padding: 8px;line-height: 1.42857143;vertical-align: top;">' + vm.roomDetails[i].guestName + '</td>';
            tableDetails += '</tr>';
          }

          var cancellationPolicy = "";

          for (var j = 0; j < vm.cancellationDetails.cancellationParsedEmail.length; j++) {
            cancellationPolicy += '<div>';
            if (vm.cancellationDetails.cancellationParsedEmail.length > 1) {
              cancellationPolicy += '<p style="display: block;font-size: 13px;color: #444;font-weight: 300;padding: 3px 0 0;line-height: 18px;margin:0;"><b>Room ' + (j + 1) + '</b></p>';
            }
            for (var k = 0; k < vm.cancellationDetails.cancellationParsedEmail[j].length; k++) {
              cancellationPolicy += '<p style="display: block;font-size: 13px;color: #444;font-weight: 300;padding: 3px 0 0;line-height: 18px;margin:0;">' + vm.cancellationDetails.cancellationParsedEmail[j][k]; +'</p>';
            }
            cancellationPolicy += '</div>';
          }

          cancellationPolicy += "<div>*Cancellation policies are based on date and time of destination.</div>";

          var bookingNotes = "";
          if (vm.bookingNotes != '') {
            bookingNotes = '<div style="width:100%;float:left;border:1px solid #ddd;margin-bottom:10px;border-bottom:none">' +
              '<div style="width:100%;float:left;background:#efefef;border-bottom:1px solid #ddd;display:flex">' +
              '<div style="width:100%;float:left;padding:1%;font-family:Gill Sans,Gill Sans MT,Calibri,Trebuchet MS,sans-serif;font-size:14px;color:#333;text-align:left;font-weight:600;border-right:1px solid #ddd">Booking Notes</div>' +
              '</div>' +
              '<div style="width:100%;float:left;border-bottom:1px solid #ddd;display:flex">' +
              '<div style="width:100%;float:left;padding:1%;font-family:Gill Sans,Gill Sans MT,Calibri,Trebuchet MS,sans-serif;font-size:14px;color:#333;text-align:left;font-weight:300;border-right:1px solid #ddd">' +
              (vm.bookingNotes || '').replace('<br>', '') + '</div>' +
              '</div>' +
              '</div>';
          }


          var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
          // var logoUrl = ServiceUrls.hubConnection.logoBaseUrl + LoggedUser.loginNode.logo + '.xhtml?ln=logo';
          var logoUrl = LoggedUser.loginNode.logo;

          printBody = printBody.replace('#LOGO#', logoUrl);
          printBody = printBody.replace('#PHONE#', vm.phoneNumber);
          printBody = printBody.replace('#STATUSICON#', vm.divHtlBkngSucces ? "../assets/images/con-hotel.png" : "../assets/images/con-hote-errorl.png");
          printBody = printBody.replace(/#LEADPAXNAME#/g, vm.header.leadTravellerName);
          printBody = printBody.replace('#BOOKINGSTATUSMESSAGE#', (vm.divHtlBkngSucces ? "Congratulations! Your hotel booking is " : "Sorry! Your hotel booking ") + vm.header.bookingStatus);
          printBody = printBody.replace('#BOOKINGREFNUMBER#', vm.header.agencyReference);
          printBody = printBody.replace('#SUPPLIERREFNUMBER#', vm.hotelDetails.supplierReferenceNumber || "No Information Available");
          printBody = printBody.replace('#HOTELNAME#', vm.hotelDetails.hotelName);
          printBody = printBody.replace('#HOTELADDRESS#', decodeURI(vm.hotelDetails.hotelAddress));
          printBody = printBody.replace('#CHECKINDAY#', vm.hotelDetails.cInDate.day);
          printBody = printBody.replace('#CHECKINDATE#', vm.hotelDetails.cInDate.fullDate);
          printBody = printBody.replace('#CHECKOUTDAY#', vm.hotelDetails.cOutDate.day);
          printBody = printBody.replace('#CHECKOUTDATE#', vm.hotelDetails.cOutDate.fullDate);
          printBody = printBody.replace('#TABLECONTENTS#', tableDetails);
          printBody = printBody.replace('#ROOMCHARGES#', i18n.n((vm.paymentDetails.roomCharges / vm.sharedStateHotel.currencyMultiplier), 'currency', vm.sharedStateHotel.selectedCurrency));
          printBody = printBody.replace('#TAX#', i18n.n((vm.paymentDetails.vat / vm.sharedStateHotel.currencyMultiplier), 'currency', vm.sharedStateHotel.selectedCurrency));
          printBody = printBody.replace('#TOTAL#', i18n.n((vm.paymentDetails.totalAmount / vm.sharedStateHotel.currencyMultiplier), 'currency', vm.sharedStateHotel.selectedCurrency));
          printBody = printBody.replace('#CANCELLATIONPOLICY#', cancellationPolicy);
          printBody = printBody.replace('#BOOKINGNOTES#', bookingNotes);
          printBody = printBody.replace('#ADDRESS#', vm.supportDetails.address);
          printBody = printBody.replace('#SUPORTPHONE#', vm.supportDetails.phone);
          printBody = printBody.replace('#EMAIL#', vm.supportDetails.email);

          var id = (new Date()).getTime();
          var myWindow = window.open(window.location.href + '?printerFriendly=true', id, "toolbar=1,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=800,height=600,left = 240,top = 212");
          myWindow.document.write(printBody);
          myWindow.focus();
          setTimeout(
            function () {
              myWindow.print();
            }, 1000);
        }).catch(function (error) {
          console.log('Error');
          this.content = [];
        });
    },
    bookingStatusUpdate(ovBookingReference, statusCode) {
      var vm = this;
      var statusToBeSent = "";
      switch (statusCode) {
        case "HK":
          statusToBeSent = "RR";
          break;
        case "CP":
          statusToBeSent = "CP";
          break;
        case "XP":
          statusToBeSent = "XP";
          break;
        case "X":
        case "ER":
        case "RJ":
          statusToBeSent = "RF";
          break;
        default:
          statusToBeSent = "RF";
          break;
      }

      var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;

      axios({
        method: "post",
        url: hubUrl + "/hotelBook/booking/changeStatus",
        data: {
          referenceNumber: ovBookingReference,
          code: statusToBeSent
        },
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.access_token
        }
      }).then(response => {
        console.log("Status update", response.data);
        var session = JSON.parse(window.sessionStorage.getItem("hotelInfo"));
          if (response.data) {
            //bind booking details
            vm.getHotelBookingInformation(ovBookingReference);
          } else {
            //show update status error
          }
      }).catch(error => {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          alertify.alert("Error", error.response.data.message)
          console.log("Server error: ", error.response.data);
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log("No response received: ", error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log("Error", error.message);
        }
      }).finally(() => {
        console.log("Finally");
      });
    },
    getHotelBookingInformation(ovBookingReference) {
      // response.data = JSON.parse(
      //   '{"booking":{"refId":"AGY75-10972","hotels":[{"rooms":[{"roomQuotes":[{"guests":[{"titleId":0,"titleName":"Mr","firstName":"Adult","surName":"One","isLead":true,"paxTypeCode":"ADT","email":"mahesh@oneviewit.com","nationalityCountryCode":"AE","nationalityCountry":"United Arab Emirates","countryOfResidenceCode":"AE","countryOfResidence":"United Arab Emirates"}],"serialNumber":1,"numberOfAdults":1,"status":"Reconfirmed","statusCode":"RR","checkInDate":"2019-04-17T00:00:00.000+0000","checkOutDate":"2019-04-18T00:00:00.000+0000","nights":1,"costPerNight":"78.90","totalCost":"78.90","debitDetails":{"baseAmount":"78.90","netAmount":"225.32","sellAmount":"339.82","totalCouponDiscount":"64.49","sellCurrency":"AED","netCurrency":"USD","totalMarkup":114.490774,"conversionRate":3.6732},"amendmentPolicyList":[],"cancellationPolicyList":[{"fromDate":"2019-03-27T00:00:00.000+0000","toDate":"2019-04-16T00:00:00.000+0000","totalCost":"78.90","finalCost":"404.31","remarks":"true"}],"supplierReferenceNumber":"D7HFAG","reservationId":""}],"roomTypeId":0,"roomType":"Double","category":"1 King Standard Room Nonsmoking(Room Only)","isExtraBed":false,"isBabyCot":false,"supplierRoomType":"1 King Standard Room Nonsmoking(Room Only)"}],"supplierCode":"25","name":"Holiday Inn Abu Dhabi","cityCode":"100765","cityName":"Abu Dhabi, United Arab Emirates","telephone":"56454545","starCategory":4,"payableBy":"25","supplierReferenceNumber":"D7HFAG","emergencyNumber":"556646545","minimumNights":1,"tariffRemarks":"www","address":" 31st street, between Muroor Airport, P.O. Box 46668, Abu Dhabi, 46668, United Arab Emirates,46668.","amendmentPolicyList":[],"cancellationPolicyList":[]}],"serviceId":2,"serviceName":"Hotel","supplierId":25,"supplierName":"TBO","reference":"3215454","supplierRemarks":"Early check out will attract full cancellation charge unless otherwise specified.","otherDetails":"OtherDetails","bookDate":"2019-03-27T07:41:46.000+0000","deadlineDate":"2019-03-27T00:00:00.000+0000","totalCost":"339.82","bookingStatusList":[{"entryDate":"2019-03-27T07:41:46.000+0000","bookingStatus":"Requested","userName":"Nancy Felix"},{"entryDate":"2019-03-27T07:42:12.000+0000","bookingStatus":"Booking Success/ Confirmed","userName":"Nancy Felix"},{"entryDate":"2019-03-27T07:42:12.000+0000","bookingStatus":"Reconfirmed","userName":"Nancy Felix"}],"paymentStatus":false,"paymentMode":"Payment Gateway"}}'
      // );
      window.sessionStorage.removeItem("hotelInfo")
      var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;

      axios({
        method: "get",
        url: hubUrl + "/hotelBook/bookingbyref/" + ovBookingReference,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.access_token
        }
      }).then(response => {

        console.log(response.data);
        var roomDetails = response.data.booking.hotels[0].rooms[0].roomQuotes[0];
        this.bookingInformation = response.data;
        this.header.leadTravellerName =
          roomDetails.guests[0].firstName + " " + roomDetails.guests[0].surName;

        if (roomDetails.statusCode == "RR") {
          this.header.bookingStatus = this.ReconfirmedStatus_Label;
        } else if (roomDetails.statusCode == "RQ") {
          this.header.bookingStatus = this.RquestedStatus_Label;
        } else if (roomDetails.statusCode == "X") {
          this.header.bookingStatus = this.FailedStatus_Label;
        } else {
          this.header.bookingStatus = roomDetails.status;
        }

        this.header.bookingStatusCode = roomDetails.statusCode;

        if (roomDetails.statusCode == "HK" || roomDetails.statusCode == "RR") {
          this.divHtlBkngSucces = true;
          this.sendVoucher = true;
        }
        else {
          this.divHtlBkngFail = true;
          this.showButtons = false;

        }

        //  this.divHtlBkngSucces = true;
        //  this.divHtlBkngFail = false;
        this.header.agencyReference = response.data.booking.refId;

        this.hotelDetails.supplierReferenceNumber =
          roomDetails.supplierReferenceNumber;
        this.hotelDetails.bookingInformation = response.data;
        this.hotelDetails.hotelName = response.data.booking.hotels[0].name;
        this.hotelDetails.starRating = response.data.booking.hotels[0].starCategory;
        this.hotelDetails.hotelAddress = response.data.booking.hotels[0].address;
        this.hotelDetails.hotelPhoneNo = response.data.booking.hotels[0].telephone;

        this.hotelDetails.bookDate = moment(response.data.booking.bookDate).format("Do MMMM YYYY");

        var voucherPax = "";
        var paxData = response.data.booking.hotels[0].rooms;
        for (var iPax = 0; iPax < paxData.length; iPax++) {
          for (var iQ = 0; iQ < paxData[iPax].roomQuotes.length; iQ++) {
            var val = parseInt(iQ) + parseInt(1);
            var Passname = paxData[iPax].roomQuotes[iQ].guests[0].firstName + " " + paxData[iPax].roomQuotes[iQ].guests[0].surName;
            var rooms = paxData[iPax].category;

            var vochurage = parseInt(0);
            var childAgeArr = [];
            for (var ichd = 0; ichd < paxData[iPax].roomQuotes[iQ].guests.length; ichd++) {
              var chdsplit = paxData[iPax].roomQuotes[iQ].guests[ichd].paxTypeCode;
              if (chdsplit == "CHD") {
                vochurage = vochurage + 1;
                var ageChild = paxData[iPax].roomQuotes[iQ].guests[ichd].age;
                childAgeArr.push(ageChild == 1 ? ageChild + " yr" : ageChild + " yrs");
              }
            }

            if (vochurage > 0) {
              var childTable = '<table style="width:100%;border: 0px solid #cecece;border-collapse: collapse;"><tr><th style="border: 1px solid #cecece;border-width: 0 1px 1px 0;text-align:center;">Count</th><th style="border: 1px solid #cecece;border-width: 0 0 1px 0;text-align:center;">Age</th></tr>';
              childTable += '<tr><td style="width: 40%;border: 1px solid #cecece;border-width: 0 1px 0 0;">' + vochurage + '</td><td>';
              childTable += childAgeArr.length > 1 ? childAgeArr.join(" & ") : childAgeArr;
              childTable += '</td></tr></table>';
            } else {
              childTable = vochurage;
            }
            var Adult = paxData[iPax].roomQuotes[iQ].numberOfAdults;

            voucherPax += '<tr>' +
              '<td style="border: 1px solid #cecece; border-collapse: collapse; padding: 10px; text-align:center;">' + val + '</td>' +
              '<td colspan="2" style="border: 1px solid #cecece; border-collapse: collapse; padding: 10px; text-align:center;">' + rooms + '</td>' +
              '<td style="border: 1px solid #cecece; border-collapse: collapse; padding: 10px; text-align:center;">' + Adult + '</td>' +
              '<td style="border: 1px solid #cecece; border-collapse: collapse; text-align:center;">' + childTable + '</td>' +
              '<td colspan="1" style="border: 1px solid #cecece; border-collapse: collapse; padding: 10px; text-align:center;">' + Passname + '</td>' +
              '</tr>';
          }
        }


        this.hotelDetails.voucherPaxDetails = voucherPax;
        this.hotelDetails.cInDate = {
          day: moment(roomDetails.checkInDate).format("dddd"),
          fullDate: moment(roomDetails.checkInDate).format("Do MMMM YYYY")
        };
        this.hotelDetails.cOutDate = {
          day: moment(roomDetails.checkOutDate).format("dddd"),
          fullDate: moment(roomDetails.checkOutDate).format("Do MMMM YYYY")
        };

        this.roomDetails = this.getRoomDetails(response.data);


        this.guestDetails.leadTravellerName =
          roomDetails.guests[0].firstName + " " + roomDetails.guests[0].surName;
        this.guestDetails.title = roomDetails.guests[0].titleName;
        this.guestDetails.guestEmail = response.data.booking.hotels[0].rooms[0].roomQuotes[0].guests[0].email;
        this.paymentDetails.roomCharges = response.data.booking.totalCost;
        this.paymentDetails.vat = 0;
        this.paymentDetails.totalAmount = response.data.booking.totalCost;


        this.cancellationDetails.cancellationPolicy = response.data;

        this.cancellationDetails.cancellationParsedEmail = this.getCancellationPolicy(response.data);

        var labels = {
          CancellationTextA1_Label: this.CancellationTextA1_Label,
          CancellationTextA2_Label: this.CancellationTextA2_Label,
          CancellationTextA3_Label: this.CancellationTextA3_Label,
          CancellationTextB1_Label: this.CancellationTextB1_Label,
          CancellationTextC1_Label: this.CancellationTextC1_Label,
          CancellationTextC2_Label: this.CancellationTextC2_Label

        }

        this.cancellationDetails.cancellationParsed = this.getCancellationPolicyWithI18n(response.data, labels);
        this.cancellationDetails.amendmentPolicy = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.";

        this.bookingNotes = response.data.booking.supplierRemarks || "";

        var user = JSON.parse(window.localStorage.getItem("User"));

        this.supportDetails = {

          address: user.loginNode.address||"",
          phone: user.loginNode.phoneList.filter((x) => {
            return x.type == "Telephone";
          })[0].number||"",
          email: user.loginNode.email||""
        };

        if (!this.isRetrieveBooking) {
          if (this.divHtlBkngSucces) {
            var vm = this;
            setTimeout(function () {
              vm.emailBookingDetails(); // generate booking email
            }, 100);
          }
        }

      }).catch(error => {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          if (error.response.data.message.indexOf("Web Token") > -1) {
            alertify.alert("Error", "Session has expired.");
          } else {
            alertify.alert("Error", error.response.data.message);
            console.log("Server error: ", error.response.data);
          }
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log("No response received: ", error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log("Error", error.message);
        }
      })
        .finally(() => {
          console.log("Finally");
        });

    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;

      getAgencycode(function (response) {
        //var Agencycode = response;
        var Agencycode = 'AGY435';
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        self.dir = langauage == "ar" ? "rtl" : "ltr";
        var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Hotel Confirmation/Hotel Confirmation/Hotel Confirmation.ftl';
        axios.get(aboutUsPage, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          self.getdata = false;
          if (response.data.area_List.length > 0) {
            var mainComp = response.data.area_List[0].main;

            self.SelectRoomButton_Label = self.pluckcom('SelectRoomButton_Label', mainComp.component);

            self.DearHeader_Label = self.pluckcom('DearHeader_Label', mainComp.component);
            self.BookingSuccessHeader = self.pluckcom('BookingSuccessHeader', mainComp.component);
            self.BookingFailureHeader = self.pluckcom('BookingFailureHeader', mainComp.component);
            self.BookingReferenceNumber_Label = self.pluckcom('BookingReferenceNumber_Label', mainComp.component);
            self.BookingEmailNotes_Label = self.pluckcom('BookingEmailNotes_Label', mainComp.component);
            self.SupplierReferenceNumber_Label = self.pluckcom('SupplierReferenceNumber_Label', mainComp.component);
            self.CheckOut_Label = self.pluckcom('CheckOut_Label', mainComp.component);
            self.CheckIn_Label = self.pluckcom('CheckIn_Label', mainComp.component);
            self.HotelDetails_Label = self.pluckcom('HotelDetails_Label', mainComp.component);
            self.GuestName_Label = self.pluckcom('GuestName_Label', mainComp.component);
            self.Guests_Label = self.pluckcom('Guests_Label', mainComp.component);
            self.RoomType_Label = self.pluckcom('RoomType_Label', mainComp.component);
            self.RoomNumber_Label = self.pluckcom('RoomNumber_Label', mainComp.component);
            self.GuestDetails_Label = self.pluckcom('GuestDetails_Label', mainComp.component);
            self.PaymentDetails_Label = self.pluckcom('PaymentDetails_Label', mainComp.component);
            self.TotalRoomCharges_Label = self.pluckcom('TotalRoomCharges_Label', mainComp.component);
            self.Tax_Label = self.pluckcom('Tax_Label', mainComp.component);
            self.TotalPrice_Label = self.pluckcom('TotalPrice_Label', mainComp.component);
            self.CancellationPolicy_Label = self.pluckcom('CancellationPolicy_Label', mainComp.component);
            self.DetailedCancellationPolicy_Label = self.pluckcom('DetailedCancellationPolicy_Label', mainComp.component);
            self.BookingNotes_Label = self.pluckcom('BookingNotes_Label', mainComp.component);
            self.CustomerSupport_Label = self.pluckcom('CustomerSupport_Label', mainComp.component);
            self.Email_Label = self.pluckcom('Email_Label', mainComp.component);
            self.Phone_Label = self.pluckcom('Phone_Label', mainComp.component);
            self.Address_Label = self.pluckcom('Address_Label', mainComp.component);
            self.CancellationTextA1_Label = self.pluckcom('CancellationTextA1_Label', mainComp.component);
            self.CancellationTextA2_Label = self.pluckcom('CancellationTextA2_Label', mainComp.component);
            self.CancellationTextA3_Label = self.pluckcom('CancellationTextA3_Label', mainComp.component);
            self.CancellationTextB1_Label = self.pluckcom('CancellationTextB1_Label', mainComp.component);
            self.CancellationTextC1_Label = self.pluckcom('CancellationTextC1_Label', mainComp.component);
            self.CancellationTextC2_Label = self.pluckcom('CancellationTextC2_Label', mainComp.component);
            self.CancellationNotes_Label = self.pluckcom('CancellationNotes_Label', mainComp.component);
            self.Room_Label = self.pluckcom('Room_Label', mainComp.component);
            self.NoInformation_Label = self.pluckcom('NoInformation_Label', mainComp.component);

            self.Adults_Label = self.pluckcom('Adults_Label', mainComp.component);
            self.Child_Label = self.pluckcom('Child_Label', mainComp.component);
            self.Children_Label = self.pluckcom('Children_Label', mainComp.component);
            self.ReconfirmedStatus_Label = self.pluckcom('ReconfirmedStatus_Label', mainComp.component);
            self.RquestedStatus_Label = self.pluckcom('RquestedStatus_Label', mainComp.component);
            self.FailedStatus_Label = self.pluckcom('FailedStatus_Label', mainComp.component);


            self.currentLanguage = localStorage.direction;
            if (self.hotelDetails.bookingInformation) {
              self.roomDetails = self.getRoomDetails(self.hotelDetails.bookingInformation);
            }

            if (self.header.bookingStatusCode == "RR") {
              self.header.bookingStatus = self.ReconfirmedStatus_Label;
            } else if (self.header.bookingStatusCode == "RQ") {
              self.header.bookingStatus = self.RquestedStatus_Label;
            } else if (self.header.bookingStatusCode == "X") {
              self.header.bookingStatus = self.FailedStatus_Label;
            }


            if (localStorage.direction == 'rtl') {
              alertify.defaults.glossary.title = 'أليرتفاي جي اس';
              alertify.defaults.glossary.ok = 'موافق';
              alertify.defaults.glossary.cancel = 'إلغاء';
              alertify.confirm().set('labels', { ok: 'موافق', cancel: 'إلغاء' });
              alertify.alert().set('label', 'موافق');
            } else {
              alertify.defaults.glossary.title = 'AlertifyJS';
              alertify.defaults.glossary.ok = 'Ok';
              alertify.defaults.glossary.cancel = 'Cancel';
              alertify.confirm().set('labels', { ok: 'Ok', cancel: 'Cancel' });
              alertify.alert().set('label', 'Ok');

            }
          }
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
      });

    },
    getRoomDetails: function (response) {
      var roomDetailsTable = [];
      for (var iPax = 0; iPax < response.booking.hotels[0].rooms.length; iPax++) {

        for (var iQ = 0; iQ < response.booking.hotels[0].rooms[iPax].roomQuotes.length; iQ++) {
          var details = {};
          var paxData = response.booking.hotels[0].rooms[iPax].roomQuotes[iQ].guests;
          details.roomType = _.unescape(response.booking.hotels[0].rooms[iPax].category);
          details.meal = _.unescape(response.booking.hotels[0].rooms[iPax].meal);
          var countChild = 0;
          var countAdult = 0;
          var childAge = [];
          for (var ifb = 0; ifb < paxData.length; ifb++) {
            if (paxData[ifb].isLead == true) {
              details.guestName = paxData[ifb].firstName + " " + paxData[ifb].surName;
            }

            if (paxData[ifb].paxTypeCode == "CHD") {
              countChild++;
              childAge.push(paxData[ifb].age ? paxData[ifb].age : "");
            }
            if (paxData[ifb].paxTypeCode == "ADT") {
              countAdult++;
            }
            details.roomNumber = iPax + 1;
          }
          var allNames = paxData.map(function(e) {
            return e.firstName + " " + e.surName
          });
          details.allNames = allNames.join(", ");
          details.adult = countAdult;
          details.children = countChild;
          details.childAge = childAge.length > 0 ? childAge.join(" & ") : "0";
          details.traveller = countAdult + " " + this.Adults_Label + (countChild != 0 ? (" " + countChild + " " + (countChild == 1 ? this.Child_Label : this.Children_Label)) : "");

          details.inclusions = "Breakfast";

        }
        roomDetailsTable.push(details);
      }
      return roomDetailsTable;
    }
  }
};

function GetParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
