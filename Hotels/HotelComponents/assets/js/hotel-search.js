var finalResult = [];
var roomsSelected = '&room1=ADT_1,CHD_0';
var AgencyCode = 'DXBTRZ001';
//var elasticsearchHost = '03d68b6206bd4dca96b7ea9639f281cb.eu-west-1.aws.found.io';

/**Auto Complete Component Start* */

var citySearchcom = Vue.component('city-search', {
  data() {
    return {
      isCompleted: false,
      keywordSearch: "",
      autoCompleteResult: [],
      highlightIndex:0
    }
  },

  template: `
	<div class="autocomplete">
    <input id="txtCitySearch" type="text" placeholder="City name" v-model="keywordSearch" class="form-control" 
    :class="{ 'loading-circle' : (keywordSearch && keywordSearch.length > 2 && autoCompleteResult.length!=0), 'hide-loading-circle': autoCompleteResult.length > 0 || autoCompleteResult.length == 0 && !isCompleted  }" 
    @input="onKeyUpAutoCompleteEvent($event)" autocomplete="off"
    @keydown.down="down"
    @keydown.up="up"
    @keydown.tab="isCompleted=false"
    @keydown.esc="isCompleted=false"
    @keydown.enter="updateResults(autoCompleteResult[highlightIndex].label,autoCompleteResult[highlightIndex].code,autoCompleteResult[highlightIndex].location)"/>
    <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteResult.length > 0">
        <li ref="options" :class="{'autocomplete-result-active' : index == highlightIndex}" class="autocomplete-result" v-for="(item,index) in autoCompleteResult" :key="index" @click="updateResults(item.label, item.code, item.location)">
            {{ item.label }}
        </li>
    </ul>
	</div>
`,
  methods: {
    up: function () {
      if (this.isCompleted) {
        if (this.highlightIndex > 0) {
          this.highlightIndex--
        }
      } else {
        this.isCompleted = true;
      }
      this.fixScrolling();
    },
    down: function () {
      if (this.isCompleted) {
        if (this.highlightIndex < this.autoCompleteResult.length - 1) {
          this.highlightIndex++
        } else if (this.highlightIndex == this.autoCompleteResult.length - 1) {
          this.highlightIndex = 0;
        }
      } else {
        this.isCompleted = true;
      }
      this.fixScrolling();
    },
    fixScrolling: function () {
      if (this.$refs.options[this.highlightIndex]) {
        var liH = this.$refs.options[this.highlightIndex].clientHeight;
        if (liH == 50) {
          liH = 32;
        }
        if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
          this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
        }
      }
    },
    onKeyUpAutoCompleteEvent:
      _.debounce(function (event) {
        var self = this;
        var keywordEntered=event.target.value;
        if (keywordEntered.length > 2) {
          this.isCompleted = true;
          //for supplier list
          var supcitycodes = [];
          var agencyNode = window.localStorage.getItem("User");
          if (agencyNode) {
            agencyNode = JSON.parse(agencyNode);
            var servicesList = agencyNode.loginNode.servicesList;
            for (var i = 0; i < servicesList.length; i++) {
              if (servicesList[i].name == "Hotel" && servicesList[i].provider.length > 0) {
                for (var j = 0; j < servicesList[i].provider.length; j++) {
                  supcitycodes.push(servicesList[i].provider[j].id);
                }
                break;
              } else if (servicesList[i].name == "Hotel" && servicesList[i].provider.length == 0) {
                // no supplier
              }
            }
          }
          var cityName = keywordEntered;
          var cityarray = cityName.split(' ');
          var uppercaseFirstLetter = "";
          for (var k = 0; k < cityarray.length; k++) {
            uppercaseFirstLetter += cityarray[k].charAt(0).toUpperCase() + cityarray[k].slice(1).toLowerCase()
            if (k < cityarray.length - 1) { uppercaseFirstLetter += " "; }
          }
          uppercaseFirstLetter = "*" + uppercaseFirstLetter + "*";
          var lowercaseLetter = "*" + cityName.toLowerCase() + "*";
          var uppercaseLetter = "*" + cityName.toUpperCase() + "*";

          var supplier_type = ["Test", "Both"];
          var should =
          {
            bool: {
              must: [
                {
                  terms: { supplier_id: supcitycodes }
                },
                {
                  terms: { supplier_type: supplier_type }
                }
              ]
            }
          };
          var query =
          {
            query: {
              bool: {
                must: [
                  {
                    bool: {
                      should: [
                        {
                          wildcard: { supplier_city_name: uppercaseFirstLetter }
                        },
                        {
                          wildcard: { supplier_city_name: uppercaseLetter }
                        },
                        {
                          wildcard: { supplier_city_name: lowercaseLetter }
                        }
                      ]
                    }
                  },
                  {
                    bool:
                    {
                      should
                    }
                  }

                ]
              }
            }
          };

          var client = new elasticsearch.Client({
            host: [
              {
                host: ServiceUrls.elasticSearch.elasticsearchHost,
                auth: ServiceUrls.elasticSearch.auth,
                protocol: ServiceUrls.elasticSearch.protocol,
                port: ServiceUrls.elasticSearch.port
              }
            ]
            , log: 'trace'
          });

          client.search({ index: 'city_map', size: 150, body: query }).then(function (resp) {
            finalResult = [];
            var hits = resp.hits.hits;
            var Citymap = new Map();
            for (var i = 0; i < hits.length; i++) {
              Citymap.set(hits[i]._source.oneview_city_id, hits[i]._source);
            }
            var get_values = Citymap.values();
            var Cityvalues = [];
            for (var ele of get_values) {
              Cityvalues.push(ele);
            }
            var results = SortInputFirst(cityName, Cityvalues);
            for (var i = 0; i < results.length; i++) {
              finalResult.push({
                "code": results[i].oneview_city_id,
                "label": results[i].supplier_city_name + ", " + results[i].oneview_country_name,
                "location": results[i].location

              });
            }
            var newData = [];
            finalResult.forEach(function (item, index) {
              if (item.label.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {

                newData.push(item);
              }
            });
            console.log(newData);
            self.isCompleted = false;
            self.autoCompleteResult = newData;
          })

        }
        else {
          this.isCompleted = false;
          this.autoCompleteResult = [];
        }
      }, 100),
    updateResults: function (label, code, location) {
      this.keywordSearch = label;
      this.autoCompleteResult = [];
      this.$emit('city-search-completed', code, label);
      // $("#checkInId").focus();
      window.sessionStorage.setItem("cityLocation", JSON.stringify(location));
    }
  }
});
/**Auto Complete Component End* */
/**Room Iteration Component Start* */
var roomsIteration = Vue.component('todo-item', {

  props: {
    value: {
      default: ''
    }


  },


  template: `<div><div class="travelDropdown"> 
    <div class="section-pass"> 
    <div class="room-title">Room {{value}}</div> <fieldset> 
    <label>Adult(s)</label> <div class="trvFld"><span class="custom-select-v3 sm">
     <select :id="'adult_' + value"  class="elmAdult _apxSelection" v-model="selected_adult"><option   v-for="adult in adults" :value="adult.value">{{adult.text}}</option> </select></span></div> 
     <span class="ageRange">(12+ yrs)</span> </fieldset> </div> <div class="section-pass"> <fieldset> <label>Children</label> <div  class="trvFld">
     <span class="custom-select-v3 sm"> <select :id="'child_' + value" class="elmChild _apxSelection"  v-on:change="onChange" v-model="selected_child"><option   v-for="child in children"  :value="child.value">{{child.text}}</option> </select> </span></div> 
     <span class="ageRange">(2-12 yrs)</span> </fieldset> </div> <div class="section-pass"> <fieldset>
      <label v-show="okage">Children ages</label> <div  class="trvFld" v-show="ok"><span class="custom-select-v3 sm"> <select :id="'childage_' + value +'_1'" class="elmChild _apxSelection" v-model="selected_childAge1"><option   v-for="childAge1 in childrenAges1" :value="childAge1.value">{{childAge1.text}}</option> </select> </span></div> 
      <div  class="trvFld" v-show="okplus"><span class="custom-select-v3 sm"> <select :id="'childage_' + value +'_2'" class="elmChild _apxSelection" v-model="selected_childAge2"><option   v-for="childAge2 in childrenAges2" :value="childAge2.value">{{childAge2.text}}</option> </select> </span>
      </div> </fieldset> </div> </div> </div>`,
  data: function () {
    return {
      adults: [

        { 'value': 1, 'text': '1' },
        { 'value': 2, 'text': '2' },
        { 'value': 3, 'text': '3' },
        { 'value': 4, 'text': '4' },
        { 'value': 5, 'text': '5' },
        { 'value': 6, 'text': '6' },
        { 'value': 7, 'text': '7' }
      ],
      children: [
        { 'value': 0, 'text': '0' },
        { 'value': 1, 'text': '1' },
        { 'value': 2, 'text': '2' },

      ],
      childrenAges1: [
        { 'value': 1, 'text': '1' },
        { 'value': 2, 'text': '2' },
        { 'value': 3, 'text': '3' },
        { 'value': 4, 'text': '4' },
        { 'value': 5, 'text': '5' },
        { 'value': 6, 'text': '6' },
        { 'value': 7, 'text': '7' },
        { 'value': 8, 'text': '8' },
        { 'value': 9, 'text': '9' },
        { 'value': 10, 'text': '10' },
        { 'value': 11, 'text': '11' },
        { 'value': 12, 'text': '12' }
      ],
      childrenAges2: [
        { 'value': 1, 'text': '1' },
        { 'value': 2, 'text': '2' },
        { 'value': 3, 'text': '3' },
        { 'value': 4, 'text': '4' },
        { 'value': 5, 'text': '5' },
        { 'value': 6, 'text': '6' },
        { 'value': 7, 'text': '7' },
        { 'value': 8, 'text': '8' },
        { 'value': 9, 'text': '9' },
        { 'value': 10, 'text': '10' },
        { 'value': 11, 'text': '11' },
        { 'value': 12, 'text': '12' }
      ],
      selected_adult: 1,
      selected_child: 0,
      selected_childAge1: 1,
      selected_childAge2: 1,
      ok: false,
      okplus: false,
      okage: false

    }

  },

  methods: {
    onChange: function (event) {
      if (event.target.value == 1) { this.ok = true; this.okplus = false; this.okage = true; }
      else if (event.target.value == 2) { this.okplus = true; this.okage = true; this.ok = true; }
      else { this.okplus = false; this.ok = false; this.okage = false; }
    }
  },

})
/**Room Iteration Component End* */

var HoteSearchMainComponent = new Vue({
  el: '#hotels',
  data: {
    KMrange: 5,
    city: {
      cityCode: null,
      cityName: null
    },
    occupancyDates: {
      checkIn: null,
      checkOut: null
    },
    roomDetails: {

    },
    numberOfNights: 0,
    numberOfRooms: 1,
    suppliers: null,
    nationality: null,
    countryOfResidence: null,
    sorting: null,
    uuid: null,

    access_token: '',
    countryCode: '',
    dspl: false,
    totalcount: "1 Guests,1 Room",
    guestCount: 0,
    adtCount: 0,
    chdCount: 0,
    count: 1,
    todos: [
    ],

    values: {},
    validationMessage: ''


  },

  /**Calender Start**/
  mounted: function () {

    var dateFormat = generalInformation.systemSettings.systemDateFormat;
    var noOfMonths = generalInformation.systemSettings.calendarDisplay;
    var startDate = new Date();
    var endDate = new Date();
    var sDate = new Date(moment(startDate, "DD/MM/YYYY")).getTime();
    var eDate = new Date(moment(endDate, "DD/MM/YYYY")).getTime();

    //Checkin Date
    $("#checkInId").datepicker({
      dateFormat: dateFormat,
      minDate: 0, // 0 for search
      maxDate: "360d",
      numberOfMonths: parseInt(noOfMonths),
      onSelect: function (date) {
        var dt2 = $('#CheckOutId');
        var startDate = $(this).datepicker('getDate');
        var minDate = $(this).datepicker('getDate');
        sDate = startDate.getTime();
        startDate.setDate(startDate.getDate() + 1);
        dt2.datepicker('option', 'minDate', startDate);
        dt2.datepicker('setDate', minDate);


      },

    });
    $('#checkInId').val(moment(startDate).format('DD MMM YYYY,dd'));
    //  $("#checkInId").datepicker({ dateFormat: dateFormat }).datepicker("setDate", new Date().getDay + 1);

    //Checkout Date
    $('#CheckOutId').datepicker({
      dateFormat: dateFormat,
      minDate: +2, // 2 for search
      maxDate: "360d",
      numberOfMonths: parseInt(noOfMonths),
      onSelect: function (date) {
        var a = $("#checkInId").datepicker('getDate').getTime();
        var b = $("#CheckOutId").datepicker('getDate').getTime();
        var c = 24 * 60 * 60 * 1000;
        var diffDays = Math.round(Math.abs((a - b) / c));
        if (diffDays > 90) {
          alert('Maximum Nights', 'Maximum 90 nights allowed.');

        } else {

          eDate = $(this).datepicker('getDate').getTime();
        }
      },

    });

    $('#CheckOutId').val(moment(new Date(), 'DD MMM YYYY,dd').add(2, 'days').format('DD MMM YYYY,dd'));
    //$("#CheckOutId").datepicker({ dateFormat: dateFormat }).datepicker("setDate", new Date().getDay + 2);

    var stepSlider = document.getElementById('rangeSlider');

    noUiSlider.create(stepSlider, {
      start: [5],
      step: 1,
      range: {
        'min': [1],
        'max': [10]
      }
    });

    var stepSliderValueElement = document.getElementById('slider-step-value');
    var self = this;
    stepSlider.noUiSlider.on('update', function (values, handle) {
      stepSliderValueElement.innerHTML = parseInt(values[handle]) + " KM";
      self.KMrange = parseInt(values[handle]);
    });



  },
  /**Calender End**/
  /**Default Single Room Bind Start**/
  created: function () {


    var roomCount = this.count;
    id = 'div_Room_' + roomCount;

    this.todos.push({
      id,
      value: roomCount,


    });
    this.$set(this.values, id, '')



  },
  /**Default Single Room Bind End**/
  methods: {
    citySearchCompleted(cityCode, cityName) {
      this.city.cityCode = cityCode;
      this.city.cityName = cityName;
    },

    /**Add Room Click Start**/
    addNewTodo: function () {


      if (this.todos.length < 9) {

        var roomCount = ++this.count;
        id = 'div_Room_' + roomCount;
        this.dspl = true;
        this.todos.push({
          id,
          value: roomCount,


        });
        this.$set(this.values, id, '')
      }
    },
    /**Add Room Click End**/
    /**Remove Room Click Start**/
    RemoveNewTodo: function (todo) {
      var roomCount = --this.count;
      if (roomCount == 1) { this.dspl = false; }
      var index = this.todos.indexOf(todo);
      this.todos.splice(index, 1);
      var noOfRooms = this.todos.length;
      var totalguest = 0;
      for (ipax = 1; ipax <= parseInt(noOfRooms); ipax++) {
        roomsSelected += '&room' + ipax + '=ADT_' + $('#adult_' + ipax).find(":selected").val() +
          ',CHD_' + $('#child_' + ipax).find(":selected").val() + '';
        for (ichild = 1; ichild <= parseInt($('#child_' + ipax).find(":selected").val()); ichild++) {

          roomsSelected += '_' + $('#childage_' + ipax + '_' + ichild).find(":selected").val();
        }

        totalguest += parseInt($('#adult_' + ipax).find(":selected").val()) + parseInt($('#child_' + ipax).find(":selected").val());
      }

      this.numberOfRooms = noOfRooms;
      this.totalcount = totalguest + " Guests," + noOfRooms + " Rooms"

    },
    /**Remove Room Click End**/

    /**Room Final Selection Click Start**/
    done: function () {
      roomsSelected = '';
      console.log(this.values);
      var noOfRooms = this.todos.length;
      var totalguest = 0;
      var adt_c = 0;
      var chd_c = 0;
      for (ipax = 1; ipax <= parseInt(noOfRooms); ipax++) {
        roomsSelected += '&room' + ipax + '=ADT_' + $('#adult_' + ipax).find(":selected").val() +
          ',CHD_' + $('#child_' + ipax).find(":selected").val() + '';
        for (ichild = 1; ichild <= parseInt($('#child_' + ipax).find(":selected").val()); ichild++) {

          roomsSelected += '_' + $('#childage_' + ipax + '_' + ichild).find(":selected").val();
        }
        adt_c += parseInt($('#adult_' + ipax).find(":selected").val());
        chd_c += parseInt($('#child_' + ipax).find(":selected").val());
        totalguest += parseInt($('#adult_' + ipax).find(":selected").val()) + parseInt($('#child_' + ipax).find(":selected").val());
      }


      console.log(adt_c, chd_c);

      this.numberOfRooms = noOfRooms;

      this.guestCount = totalguest;
      this.adtCount = adt_c;
      this.chdCount = chd_c;
      this.totalcount = totalguest + " Guests," + noOfRooms + " Rooms"

    },
    /**Room Final Selection Click End**/
    /**Search Click Start**/
    search: function () {



      var cityCode = this.city.cityCode;
      var checkIn = $('#checkInId').val() == "" ? "" : $('#checkInId').datepicker('getDate');
      var checkOut = $('#CheckOutId').val() == "" ? "" : $('#CheckOutId').datepicker('getDate');
      var uuidcode = uuidv4();
      var supplers = 'QTECH';

      var roomDetails = roomsSelected;
      if (!this.city.cityCode) {
        this.validationMessage = "Please select city !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = "";
        }, 1500);
        document.getElementById("txtCitySearch").focus();
        return false;
      }

      if (!checkIn) {
        this.validationMessage = "Please select checkindate !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = "";
        }, 1500);
        return false;
      }
      if (!checkOut) {
        this.validationMessage = "Please select checkoutdate !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = "";
        }, 1500);
        return false;
      }
      if (!this.totalcount) {
        this.validationMessage = "Please select guests !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = "";
        }, 1500);
        return false;
      }
      if (this.guestCount > 9) {
        this.validationMessage = "Maximum occupancy in a single booking is 9.";
        var self = this;
        setTimeout(function () {
          self.validationMessage = "";
        }, 1500);
        return false;
      }

      this.validationMessage = "";
      //  console.log(searchRequest);
      //   var query = JSON.stringify(searchRequest);
      //   console.log(query); 

      console.log(roomDetails);
      this.adtCount = (this.adtCount == 0) ? this.adtCount = 1 : this.adtCount = this.adtCount;
      this.guestCount = (this.guestCount == 0) ? this.guestCount = 1 : this.guestCount = this.guestCount;
      var searchCriteria = "nationality=AE&country_of_residence=AE" +
        "&city_code=" + this.city.cityCode +
        "&city=" + this.city.cityName +
        "&cin=" + moment(checkIn).format('DD/MM/YYYY') +
        "&cout=" + moment(checkOut).format('DD/MM/YYYY') +
        "&night=" + moment.duration(moment(checkOut).diff(moment(checkIn))).asDays() +
        "&adtCount=" + this.adtCount +
        "&chdCount=" + this.chdCount +
        "&guest=" + this.guestCount +
        "&num_room=" + this.numberOfRooms +
        roomDetails +
        "&KMrange=" + this.KMrange +
        "&sort=" + "price-a" +
        "&uuid=" + uuidcode;
      var uri = '../Hotels/hotel-listing.html?' + searchCriteria;
       
      window.location.href = uri
    }
  }
})
/**Search Click End**/

/**UUID Generation Start**/
function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}
/**UUID Generation End**/


/**Sorting Function for AutoCompelte Start**/
function SortInputFirst(input, data) {
  var first = [];
  var others = [];
  for (var i = 0; i < data.length; i++) {
    if (data[i].supplier_city_name.toLowerCase().startsWith(input.toLowerCase())) {
      first.push(data[i]);
    } else {

      others.push(data[i]);
    }
  }
  first.sort(function (a, b) {
    var nameA = a.supplier_city_name.toLowerCase(), nameB = b.supplier_city_name.toLowerCase()
    if (nameA < nameB) //sort string ascending
      return -1
    if (nameA > nameB)
      return 1
    return 0 //default return value (no sorting)
  })
  others.sort(function (a, b) {
    var nameA = a.supplier_city_name.toLowerCase(), nameB = b.supplier_city_name.toLowerCase()
    if (nameA < nameB) //sort string ascending
      return -1
    if (nameA > nameB)
      return 1
    return 0 //default return value (no sorting)
  })
  return (first.concat(others));
}

/**Sorting Function for AutoCompelte End**/

/** content management start */

/**content management end */


