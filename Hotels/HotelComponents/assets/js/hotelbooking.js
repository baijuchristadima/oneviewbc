new Vue({
    el: '#div_hotelBooking',
    data() {
      return {
        hotelbooking: {
          
            "hotelname": "Emirates Grand Hotel Apartments",
            "imgsrc": "../assets/images/hotel-img2.jpg",
            "address":"Business Bay, 71847 Dubai, United Arab Emirates",
             "starcount":"5",
         
    }
}
},
  
    computed: {
      courses() {
        return Object.values(this.hotelbooking).map(({
            hotelname,
            imgsrc,
            address,
            starcount
        }) => ({
            hotelname,
            imgsrc,
            address,
            starcount
        }))
      }
    }
  })