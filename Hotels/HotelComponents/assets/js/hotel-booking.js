module.exports = {
  mixins: [],
  components: {
    VeeValidate
  },
  data: function () {

    return {
      hotelDetails: {
        location: {
          address: ""
        }
      },
      searchCriteria: {},
      selectedRooms: {},
      bookingDetails: {
        cInDate: {
          time: null,
          day: null,
          month: null,
          fulldate: null
        },
        cOutDate: {
          time: null,
          day: null,
          month: null,
          fulldate: null
        },
        numberOfNights: 0,
        bookingPaxDetails: "",
        roomSummaryDetails: [],
        amenities: [],
        hotelInformation: "",
        cancellationDetails: {
          cancellationPolicy: [],
          amendmentPolicy: null
        },
        totalCost: null,
        hotelNorms: []
      },
      paxDetails: {
        contactDetails: {
          emailID: null,
          phoneNumber: null,
          optedForWhatsApp: false
        },
        pax: [],
        paxPerRoom: []
      },
      showModal: false,
      finalConfirmation: "",
      priceHasChanged: false,
      sharedStateHotel: store.state,
      paymentType: 6,
      paymentForm: "",
      paymentDetails: {
        "totalAmount": -188.88,
        "customerEmail": "test@test.com",
        "bookingReference": "testRef",
        "cartID": "",
        "supplier": "",
        "uuid": ""
      },
      returnURL: window.location.origin + "/Hotels/hotel-detail.html#/hotelConfirmation/",
      username: '',
      password: '',
      userLogined: checkLoginLocal(),
      userinfo: [],
      confirmBookingIsClicked: false,
      welcomeUser: (this.WelcomeNotes_Label || 'Welcome') + ', ' + currentUser.firstName + ' ' + currentUser.lastName + '! <span>(' + currentUser.emailId + ')</span>',
      currentTotalCost: null,
      isHotelWiseCancellation: false,
      updatedCancellationPolicy: [],
      ConfirmationHeader_Label: "",
      PaymentHeader_Label: "",
      TravellerDetailsHeader_Label: "",
      ReviewHotelHeader_Label: "",
      SummaryDetails_Label: "",
      Nights_Label: "",
      Adults_Label: "",
      Child_Label: "",
      Children_Label: "",
      TotalPrice_Label: "",
      SidebarNotes_Label: "",
      SignInButton_Label: "",
      LogoutButton_Label: "",
      SignInNotes_Label: "",
      PasswordNotes_Label: "",
      EmailNotes_Label: "",
      Password_Placeholder: "",
      Email_Placeholder: "",
      AccountCreationQuestion_Label: "",
      AccountCreationNotes_Label: "",
      ReviewYourBooking_Label: "",
      CheckOut_Label: "",
      CheckIn_Label: "",
      HotelInformation_Label: "",
      HotelNorms_Label: "",
      NoInformation_Label: "",
      CancellationPolicy_Label: "",
      CancellationTextA1_Label: "",
      CancellationTextA2_Label: "",
      CancellationTextA3_Label: "",
      CancellationTextB1_Label: "",
      CancellationTextC1_Label: "",
      CancellationTextC2_Label: "",
      CancellationNotes_Label: "",
      GuestDetails_Label: "",
      LeadPax_Label: "",
      LastName_Label: "",
      FirstName_Label: "",
      Title_Label: "",
      PaxNotes_Label: "",
      ContactDetails_Label: "",
      EmailId_Label: "",
      PhoneNumber_Label: "",
      WhatsAppNotes_Label: "",
      ContinueToPaymentButton_Label: "",
      ValidationPopUp_Label: "",
      ValidationRequiredFields_Label: "",
      ValidationNotBookable_Label: "",
      WelcomeNotes_Label: "",
      FinalPrice_Label: "",
      FinalUpdatedPrice_Label: "",
      OkButton_Label: "",
      CancelButton_Label: "",
      Room_Label: "",
      AdultTitle_Label: "",
      ChildTitle_Label: "",
      Infant_Label: "",
      Pax_Label: "",
      Age_Label: "",
      privacyPolicy: "No information available.",
      termsAndConditions: "No information available.",
      ppAndTcAccepted: false,
      TermsAndPolicy1_Label: "",
      TermsAndPolicy2_Label: "",
      TermsAndPolicy3_Label: "",
      TermsAndPolicy4_Label: "",
      TermsAndPolicy5_Label:"",
      currentUser: "",
      currentPayGateways: "",
      Required_Message: "",
      AlphaSpaces_Message: "",
      Email_Message: "",
      Numeric_Message: "",
      MaximumLength_Message: "", 
      Regex_Message: "",
      TermsAndPolicy_Message: ""

    };
  },
  created() {
     
    this.getPagecontent();
    this.getTermsnConditions();
    this.veeValidateLocaleUpdate();

    if (localStorage.IsLogin === "true") {

      var htlStorage = JSON.parse(localStorage.User);
      this.paxDetails.contactDetails.emailID = htlStorage.emailId;
    }
    this.$eventHub.$on('logged-in', this.headerLogin);
    this.$eventHub.$on('logged-out', this.logout);
  },
  beforeDestroy() {
    this.$eventHub.$off('logged-in');
    this.$eventHub.$off('logged-out');
  },
  watch: {
    "sharedStateHotel.selectedCurrency": function () {

      var labels = {
        CancellationTextA1_Label: this.CancellationTextA1_Label,
        CancellationTextA2_Label: this.CancellationTextA2_Label,
        CancellationTextA3_Label: this.CancellationTextA3_Label,
        CancellationTextB1_Label: this.CancellationTextB1_Label,
        CancellationTextC1_Label: this.CancellationTextC1_Label,
        CancellationTextC2_Label: this.CancellationTextC2_Label

      }

      var details = JSON.parse(window.sessionStorage.getItem("hotelInfo"));

      var cancellationParsed = [];
      if (details.content.searchResponse.hotels[0].prebookResponse.hotel.hotelCancellationPolicies) {
        var isHotelWiseCancellation = details.content.searchResponse.hotels[0].prebookResponse.hotel.hotelCancellationPolicies.length > 0;
      }

      if (isHotelWiseCancellation) {
        cancellationParsed.push(ParseCancellationPolicyWithI18n(this.selectedRooms[0].room.rates[0].cancellationPolicies, this.bookingDetails.totalCost, labels));

      } else {
        for (var index = 0; index < this.selectedRooms.length; index++) {
          cancellationParsed.push(ParseCancellationPolicyWithI18n(this.selectedRooms[index].room.rates[0].cancellationPolicies, this.selectedRooms[index].room.rates[0].pricing.totalPrice.price, labels));
        }
      }
      this.bookingDetails.cancellationDetails.cancellationPolicy = cancellationParsed;
    }
  },
  mounted() {
    if (localStorage.IsLogin === "true") {
      $('#emailtxt').val(currentUser.emailId);
      $('#phonetxt').val(currentUser.contactNumber);
    }

    this.$nextTick(function(){
      this.currentUser = this.getUser();
      this.currentPayGateways = this.getPayGateways();     
    })
  },
  methods: {
    headerLogin(userDetails){
      this.username = userDetails.userName;
      this.password = userDetails.password;
      this.signInbook();
    },
    close() {
      this.showModal = false;
      this.priceHasChanged = false;
      this.bookingDetails.totalCost = this.currentTotalCost;
    },
    imgUrlAlt(event) {

      this.hotelDetails.imageUrl = "/assets/images/no-image.png";
    },
    confirmBooking() {
      //var response = false;
      var self = this;
      this.$validator.validateAll().then((result) => {
        if (result) {

          if (localStorage.IsLogin === "true") {
            this.bookingHandler();
          } else {

            registerUser(this.paxDetails.contactDetails.emailID, 'Guest', 'User', 'Mr', function (response) {
              if (response === true) {
                response = JSON.parse(localStorage.User);
                self.userLogined = true;
                self.userinfo = response;
                self.welcomeUser = (self.WelcomeNotes_Label || 'Welcome') + ', ' + 'Guest' + ' ' + 'User' + '! <span>(' + self.paxDetails.contactDetails.emailID + ')</span>';
                $('#bind-login-info').show();
                $('#sign-bt-area').hide();
                $('#modal_retrieve').hide();
                $('#bind-login-info span').html(response.firstName + ' ' + response.lastName);
                // $('#bind-login-info').html('<input type="checkbox" id="profile2"> <img src="https://cdn0.iconfinder.com/data/icons/avatars-3/512/avatar_hipster_guy-512.png"> <span>' + 'Guest' + ' ' + 'User' + '</span> <label for="profile2"><i class="fa fa-angle-down" aria-hidden="true"></i></label> <ul> <li><a href="/customer-profile.html"><i class="fa fa-th-large" aria-hidden="true"></i> My Dashboard</a></li> <li><a href="/my-profile.html"><i class="fa fa-user" aria-hidden="true"></i> My Profile</a></li> <li><a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i> My Bookings</a></li> <li><a href="#" v-on:click="logout"><i class="fa fa-power-off" aria-hidden="true"></i> Logout</a></li> </ul>')
                self.bookingHandler();

              } else {

                alertify.confirm("Confirm", "Email already registered. Do you want to login with it?"
                  , function () {
                    self.username = self.paxDetails.contactDetails.emailID;
                    $('#sign-popup').addClass('in');
                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                    self.$nextTick(() => self.$refs.password.focus())
                    //self.$refs.username.$el.focus();
                  }
                  , function () {
                    /*$('#sign-popup').removeClass('in');
                    signArea.username = '';
                    togglePaymentButton(false);*/
                    commonlogin(self.bookingHandler);
                  }).set({
                    'closable': false, 'labels': { ok: "Yes", cancel: "Continue as guest" },
                    onclose: function () {
                      $('html, body').animate({ scrollTop: 0 }, 'fast');
                    }
                  });
                

              }
            });
          }
        } else {
          if (this.$validator.errors.collect('terms').length==1 && this.$validator.errors.all().length==1) {
            alertify.alert(this.ValidationPopUp_Label, this.$validator.errors.collect('terms')[0]);
          }else {
            alertify.alert(this.ValidationPopUp_Label, this.ValidationRequiredFields_Label);
          }
        }
      });
    },
    bookingHandler() {


      this.confirmBookingIsClicked = true;
      //Update global store
      store.updateState("paxDetails", {
        paxDetails: this.paxDetails.pax,
        paxPerRoom: this.paxDetails.paxPerRoom
      });
      var totalAmount = 0;

      var rooms = [];
      for (var index = 0; index < this.selectedRooms.length; index++) {

        var room = this.selectedRooms[index].room;
        rooms.push({
          name: room.roomName,
          roomId: room.roomId,
          paxes: this.paxDetails.paxPerRoom[index], // from pax page
          guests: room.guests, // from search 
          rate: {
            bookable: true,
            cancellationPolicies: room.rates[0].cancellationPolicies,
            rateSpecific: room.rates[0].roomRateSpecific,
            pricing: {
              perNightPrice: room.rates[0].pricing.perNightPrice,
              totalPrice: room.rates[0].pricing.totalPrice
            },
            roomRateReferences: room.rates[0].roomRateSpecific ? room.rates[0].roomRateSpecific.roomRateReferences : undefined
          },
          roomReferences: room.roomSpecific ? room.roomSpecific.roomReferences : undefined
        });
        totalAmount += room.rates[0].pricing.totalPrice.price;
        sealCode = room.rates[0].pricing.totalPrice.sealCode;
      }


      var details = JSON.parse(window.sessionStorage.getItem("hotelInfo"));

      var paxes = []
      for (var paxIndex = 0; paxIndex < this.paxDetails.pax.length; paxIndex++) {
        for (var roomIndex = 0; roomIndex < this.paxDetails.pax[paxIndex].paxPerRoom.length; roomIndex++) {
          var pax = _.cloneDeep(this.paxDetails.pax[paxIndex].paxPerRoom[roomIndex]);
          delete pax.label;
          paxes.push(pax);
        }
      }
      window.sessionStorage.setItem("paxDetails", JSON.stringify(this.paxDetails));
      var PrebookRQ = {
        service: "HotelRQ",
        node: details.node,
        token: details.token,
        hubUuid: details.hubUuid,
        supplierCodes: ["" + details.supplierCode],
        credentials: [],
        content: {
          command: "PrebookRQ",
          hotel: {
            cityCode: this.hotelDetails.location.cityCode,
            hotelCode: this.hotelDetails.hotelCode,
            totalAmount: totalAmount, // total for hotel?
            sealCode: sealCode, // what sealcode to use?
            nationality: details.content.searchResponse.nationality,
            residentOf: details.content.searchResponse.residentOf,
            rooms: rooms,
            hotelSpecific: this.hotelDetails.hotelSpecific
          },
          paxes: paxes, // from pax page?
          stay: details.content.supplierSpecific.criteria.stay,
          supplierSpecific: {
            uuid: localStorage.getItem("hotelUUID"), // from session storage
            requestType: "priceRecheck",
            searchReferences: details.content.searchResponse.supplierSpecific ? details.content.searchResponse.supplierSpecific.searchReferences : undefined,
            searchHotelReferences: this.hotelDetails.hotelSpecific ? this.hotelDetails.hotelSpecific.searchHotelReferences : undefined,
            roomsReferences: this.hotelDetails.roomSpecific ? this.hotelDetails.roomSpecific.roomReferences : undefined
          }
        }
      }

      //Update global store
      store.updateState("prebookAvailabilityRQ", PrebookRQ);

      this.sendPrebook({
        request: PrebookRQ,
        roomId: null,
        isOptional: false,
        fromBooking: true
      });

      console.log(rooms);


    },
    sendPrebook(data) {
      var vm = this;

      var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;

      axios({
        method: "post",
        url: hubUrl + "/preBook",
        data: {
          request: data.request
        },
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          'Authorization': 'Bearer ' + localStorage.access_token
        }
      }).then(response => {
        console.log(response.data);
        this.confirmBookingIsClicked = false;
        var details = JSON.parse(window.sessionStorage.getItem("hotelInfo"));

       

        var hotel = response.data.response.content.prebookResponse.hotel;
        this.currentTotalCost = this.bookingDetails.totalCost;
        var roomIsBookable = true;
        if (response.data.response.content.prebookResponse.bookable) {

          for (var roomIndex = 0; roomIndex < hotel.rooms.length; roomIndex++) {
            var room = hotel.rooms[roomIndex];
            if (typeof room.rate.bookable != "undefined" && !room.rate.bookable) {
              roomIsBookable = false;
            }
          }
          if (roomIsBookable) {

            // Check updated price
            if (this.bookingDetails.totalCost != response.data.response.content.prebookResponse.currentPrice) {


              this.bookingDetails.totalCost = response.data.response.content.prebookResponse.currentPrice;
              var prebookRooms = response.data.response.content.prebookResponse.hotel.rooms;

              for (var travellerIndex = 0; travellerIndex < this.bookingDetails.roomSummaryDetails.length; travellerIndex++) {
                for (var prebookRoomIndex = 0; prebookRoomIndex < prebookRooms.length; prebookRoomIndex++) {
                  var prebookRoom = prebookRooms[prebookRoomIndex];
                  if (this.bookingDetails.roomSummaryDetails[travellerIndex].roomId == prebookRoom.roomId) {
                    this.bookingDetails.roomSummaryDetails[travellerIndex].roomPrice = prebookRoom.rate.pricing.totalPrice.price;
                    break;
                  }
                }
              }

              for (var roomIndex = 0; roomIndex < vm.selectedRooms.length; roomIndex++) {
                var room = vm.selectedRooms[roomIndex].room;
                // Check for room wise or hotel wise cancellation

                if (hotel.hotelCancellationPolicies != undefined && hotel.hotelCancellationPolicies.length > 0) {

                  room.rates[0].cancellationPolicies = hotel.hotelCancellationPolicies;
                  room.rates[0].cancellationPoliciesParsed = ParseCancellationPolicy(hotel.hotelCancellationPolicies, response.data.response.content.prebookResponse.currentPrice);

                  room.isPrebookSent = true;
                } else {
                  for (var prebookRoomIndex = 0; prebookRoomIndex < prebookRooms.length; prebookRoomIndex++) {
                    var prebookRoom = prebookRooms[prebookRoomIndex];
                    if (room.roomId == prebookRoom.roomId) {

                      room.isPrebookSent = true;
                      room.rates[0].cancellationPolicies = prebookRoom.rate.cancellationPolicies;

                      room.rates[0].cancellationPoliciesParsed = ParseCancellationPolicy(prebookRoom.rate.cancellationPolicies, prebookRoom.rate.pricing.totalPrice.price);
                      break;
                    }
                  }
                }
                for (var prebookRoomIndex = 0; prebookRoomIndex < prebookRooms.length; prebookRoomIndex++) {
                  var prebookRoom = prebookRooms[prebookRoomIndex];
                  if (room.roomId == prebookRoom.roomId) {
                    room.rates[0].pricing = prebookRooms[prebookRoomIndex].rate.pricing;
                    break;
                  }
                }
              }

              var details = JSON.parse(window.sessionStorage.getItem("hotelInfo"));

              var cancellationParsed = [];

              if (details.content.searchResponse.hotels[0].prebookResponse.hotel.hotelCancellationPolicies) {
                this.isHotelWiseCancellation = details.content.searchResponse.hotels[0].prebookResponse.hotel.hotelCancellationPolicies.length > 0;
              }

              if (this.isHotelWiseCancellation) {
                cancellationParsed.push(ParseCancellationPolicy(this.selectedRooms[0].room.rates[0].cancellationPolicies, this.bookingDetails.totalCost));

              } else {
                for (var index = 0; index < this.selectedRooms.length; index++) {
                  cancellationParsed.push(ParseCancellationPolicy(this.selectedRooms[index].room.rates[0].cancellationPolicies, this.selectedRooms[index].room.rates[0].pricing.totalPrice.price));
                }
              }
              this.updatedCancellationPolicy = cancellationParsed;

              this.priceHasChanged = true;
            }
            var hotelPolicies = response.data.response.content.prebookResponse.policies;
            var policyBindHtml = "";
            if (hotelPolicies) {

              for (var index = 0; index < hotelPolicies.length; index++) {
                if (hotelPolicies[index].description != undefined) {
                  policyBindHtml += "<p>" + hotelPolicies[index].description + "</p>";
                }
                // else {
                //   policyBindHtml += "<p>" + this.NoInformation_Label + "</p>";
                // }
              }
              this.bookingDetails.hotelInformation = policyBindHtml;
            }

          }
        } else {
          roomIsBookable = false;
        }

        if (roomIsBookable) {
          this.finalConfirmation = this.bookingDetails.hotelInformation;
          this.showModal = true;


          //Update global store
          store.updateState("prebookAvailabilityRS", response.data);

          var session = JSON.parse(window.sessionStorage.getItem("hotelInfo"));
          session.content.searchResponse.hotels[0].prebookResponse = response.data.response.content.prebookResponse;
          session.content.searchResponse.hotels[0].prebookResponse.supplierSpecific = response.data.response.content.supplierSpecific;
          session.content.searchResponse.hotels[0].roomResponse.selectedRooms = _.cloneDeep(vm.selectedRooms);
          window.sessionStorage.setItem("hotelInfo", JSON.stringify(session));
          console.log(_.cloneDeep(vm.selectedRooms));
        } else {
          alertify.alert(this.ValidationPopUp_Label, this.ValidationNotBookable_Label);

        }
      }).catch(error => {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          alertify.alert("Error", error.response.data.message)
          console.log("Server error: ", error.response.data);
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log("No response received: ", error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log("Error", error.message);
        }
      })
        .finally(() => {
          this.confirmBookingIsClicked = false;
          console.log("Finally");
        });

    },
    continueBooking() {
      var vm = this;
      this.confirmBookingIsClicked = true;

      // Get data from session storage
      var session = JSON.parse(window.sessionStorage.getItem("hotelInfo"));

      var selectedRooms = session.content.searchResponse.hotels[0].roomResponse.selectedRooms;
      var hotelDetails = session.content.searchResponse.hotels[0];

      var paxId = 1;

      var paxesPerRoom = [];
      // vm.paxDetails.contactDetails.phoneNumber = $(".selected-dial-code").html() + vm.paxDetails.contactDetails.phoneNumber;

      window.sessionStorage.setItem("paxDetails", JSON.stringify(this.paxDetails));
      for (var index = 0; index < this.paxDetails.pax.length; index++) {

        var paxes = _.map(this.paxDetails.pax[index].paxPerRoom, function (e) {
          return {
            serialNumber: paxId++,
            firstName: e.name,
            surName: e.surName,
            age: e.age,
            isLead: e.leadPax,
            titleId: e.title == "Mr" ? 1 : e.title == "Mrs" ? 3 : e.title == "Ms" ? 4 : e.title == "Master" ? 5 : 2,
            paxTypeCode: e.type == "Adult" ? "ADT" : "CHD",
            email: vm.paxDetails.contactDetails.emailID,
            phone: $(".selected-dial-code").html() + vm.paxDetails.contactDetails.phoneNumber,
            nationalityCode: session.content.searchResponse.nationality,
            countryOfResidenceCode: session.content.searchResponse.residentOf
          }
        })
        paxesPerRoom.push(paxes);
      }

      var rooms = [];
      for (var index = 0; index < selectedRooms.length; index++) {
        var room = selectedRooms[index].room;
        var cancellation = [];
        if (!vm.isHotelWiseCancellation) {
          cancellation = _.map(room.rates[0].cancellationPolicies, function (e) {
            return {
              fromDate: moment(e.from, 'DD/MM/YYYY').format("YYYY-MM-DDTHH:mm:ss.000+0000"),
              toDate: moment(e.to, 'DD/MM/YYYY').format("YYYY-MM-DDTHH:mm:ss.000+0000"),
              totalCost: e.amount,
              remarks: true
            };
          });
        }

        rooms.push({
          roomQuotes: [{
            cancellationPolicy: cancellation,
            guests: paxesPerRoom[index],
            serialNumber: index + 1,
            date: moment(new Date()).format("YYYY-MM-DDTHH:mm:ss.000+0000"),
            numberOfAdults: this.paxDetails.pax[index].adult,
            status: "pending",
            checkInDate: moment(session.content.supplierSpecific.criteria.stay.checkIn, 'DD/MM/YYYY').format("YYYY-MM-DDTHH:mm:ss.000+0000"),
            checkOutDate: moment(session.content.supplierSpecific.criteria.stay.checkOut, 'DD/MM/YYYY').format("YYYY-MM-DDTHH:mm:ss.000+0000"),
            nights: moment.duration(moment(session.content.supplierSpecific.criteria.stay.checkOut, 'DD/MM/YYYY').diff(moment(session.content.supplierSpecific.criteria.stay.checkIn, 'DD/MM/YYYY'))).asDays(),
            costPerNight: 0.0,
            totalCost: 0.0
          }],
          pricing: {
            perNightPrice: room.rates[0].pricing.perNightPrice,
            totalPrice: room.rates[0].pricing.totalPrice
          },
          roomTypeId: 1,
          category: room.roomName,
          meal: vm.getMealType(room),
          isExtraBed: false,
          isBabyCot: false,
          supplierRoomType: room.roomName,
          roomDetail: null
        });
      }

      if (vm.isHotelWiseCancellation) {
        var hotelWiseCancellation = _.map(vm.hotelDetails.prebookResponse.hotel.hotelCancellationPolicies, function (e) {
          return {
            fromDate: moment(e.from, 'DD/MM/YYYY').format("YYYY-MM-DDTHH:mm:ss.000+0000"),
            toDate: moment(e.to, 'DD/MM/YYYY').format("YYYY-MM-DDTHH:mm:ss.000+0000"),
            totalCost: e.amount,
            remarks: true
          }
        });
      };

      var hubBookRQ = {
        booking: {
          hotels: [{
            rooms: rooms,
            cancellationPolicy: hotelWiseCancellation,
            serialNumber: 1,
            supplierCode: session.supplierCode,
            name: hotelDetails.name,
            address: hotelDetails.location.address,
            cityCode: hotelDetails.location.cityCode,
            cityName: session.content.supplierSpecific.criteria.location.cityName,
            telephone: "556646545",
            starCategory: parseFloat(hotelDetails.starRating),
            payableBy: "GTA",
            emergencyNumber: "556646545",
            minimumNights: 1,
            tariffRemarks: "www"
          }],
          serviceId: 2,
          supplierId: session.supplierCode,
          reference: localStorage.getItem("hotelUUID"),
          comments: "comments",
          bookPosition: 1,
          supplierRemarks: this.bookingDetails.hotelInformation,
          otherDetails: "otherDetails",
          bookDate: moment(new Date()).format("YYYY-MM-DDTHH:mm:ss.000+0000"),
          deadlineDate: "2018-09-16T00:00:00.000+0000" // ??
        },
        sealedData: hotelDetails.prebookResponse.sealedData,
        paymentMode: 6,
        couponUsedIds: []
      }

      var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;

      axios({
        method: "post",
        url: hubUrl + "/hotelBook/booking",
        data: hubBookRQ,
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          'Authorization': 'Bearer ' + localStorage.access_token
        }
      })
        .then(response => {
          console.log(response.data);
          var details = JSON.parse(window.sessionStorage.getItem("hotelInfo"));

          if (response.data) {
            var session = JSON.parse(window.sessionStorage.getItem("hotelInfo"));
            session.content.searchResponse.bookingId = response.data.referenceNumber;
            session.content.searchResponse.cartID = response.data.cartId;

            window.sessionStorage.setItem("hotelInfo", JSON.stringify(session));

            //Make booking session variable
            window.sessionStorage.setItem("userAction", vm.paymentType);


            if (vm.paymentType == 7) {
              vm.$router.push('/hotelConfirmation');
            } else {

              vm.paymentDetails.bookingReference = session.content.searchResponse.bookingId;
              vm.paymentDetails.totalAmount = vm.bookingDetails.totalCost.toString().indexOf(".") > 0 ? vm.bookingDetails.totalCost : parseFloat(vm.bookingDetails.totalCost).toFixed(2);
              vm.paymentDetails.customerEmail = vm.paxDetails.contactDetails.emailID;
              vm.paymentDetails.cartID = session.content.searchResponse.cartID;
              vm.paymentDetails.supplier = details.supplierCode;
              vm.paymentDetails.currentPayGateways = vm.currentPayGateways;
              vm.paymentDetails.currency = vm.currentUser.loginNode.currency ? vm.currentUser.loginNode.currency : 'USD';

              sessionStorage.currentUrl = window.location.href;
              //before pg
              console.log("before pg", localStorage.access_token);
              localStorage.cartEmailId = vm.paxDetails.contactDetails.emailID;

              if (vm.currentPayGateways != undefined) {
                if (vm.currentPayGateways.length > 0) {
                  if (vm.currentPayGateways[0].id == 9) {
                    //Merch Integration
                    localStorage.bkRefNo = response.data.referenceNumber;
                    
                    localStorage.serviceId = 2;
                    window.location.href = '/pay.html';
                  } else {
                    //Redirection
                    vm.paymentForm = paymentManager(vm.paymentDetails, vm.returnURL, vm);
                  }
                } else {
                  alertify.alert('Warning', 'Payment Gateway Not Available.');
                }
              }
              else {
                alertify.alert('Warning', 'Payment Gateway Not Available.');
              }

              console.log("before pg form", localStorage.access_token);

              // vm.$router.push('/hotelPayment');
            }

          }
            
        })
        .catch(error => {
          if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            vm.showModal = false;
            alertify.alert("Error", error.response.data.message)
            console.log("Server error: ", error.response.data);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log("No response received: ", error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log("Error", error.message);
          }
        })
        .finally(() => {
          console.log("Finally");
          this.confirmBookingIsClicked = false;
        });

    },
    signInbook: function () {
      var self = this;

      if (!this.username) {
        alert('Username required.');
      } else if (!this.password) {
        alert('Password required.');
      } else {
        login(this.username, this.password, function (response) {

          if (response == false && self.userLogined == false) {
            self.userLogined = false;
            self.logout();
            alert("Invalid username or password.");
          } else {
            response = JSON.parse(localStorage.User);
            console.log(response);
            self.userLogined = true;
            self.userinfo = response;
            self.welcomeUser = (self.WelcomeNotes_Label || 'Welcome') + ', ' + response.firstName + ' ' + response.lastName + '! <span>(' + response.emailId + ')</span>';
            $('#bind-login-info').show();
            $('#sign-bt-area').hide();
            $('#modal_retrieve').hide();
            $('#bind-login-info span').html( response.firstName + ' ' + response.lastName );
            // $('#bind-login-info').html('<input type="checkbox" id="profile2"> <img src="https://cdn0.iconfinder.com/data/icons/avatars-3/512/avatar_hipster_guy-512.png"> <span>' + response.firstName + ' ' + response.lastName + '</span> <label for="profile2"><i class="fa fa-angle-down" aria-hidden="true"></i></label> <ul> <li><a href="/customer-profile.html"><i class="fa fa-th-large" aria-hidden="true"></i> My Dashboard</a></li> <li><a href="/my-profile.html"><i class="fa fa-user" aria-hidden="true"></i> My Profile</a></li> <li><a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i> My Bookings</a></li> <li><a href="#" v-on:click="logout"><i class="fa fa-power-off" aria-hidden="true"></i> Logout</a></li> </ul>')
            $('#emailtxt').val(response.emailId);
            $('#phonetxt').val(response.contactNumber);

            if (localStorage.IsLogin === "true") {
              var htlStorage = JSON.parse(localStorage.User);
              self.paxDetails.contactDetails.emailID = htlStorage.emailId;
              self.paxDetails.contactDetails.phoneNumber = htlStorage.contactNumber;
            }
            // try { this.checklogin(); } catch (err) { console.log(err); }
          }
        });
      }
    },
    logout: function () {
      this.userLogined = false;
      this.userinfo = [];
      var self = this;
      self.welcomeUser = this.SignInNotes_Label || 'Sign in to speed up your booking process';


      commonlogin(function () {

        if (localStorage.IsLogin === "false") {
          $('#bind-login-info').hide();
          $('#sign-bt-area').show();
          $('#modal_retrieve').show();
          $('#emailtxt').val('');
          $('#phonetxt').val('');
          self.paxDetails.contactDetails.emailID = "";
          self.paxDetails.contactDetails.phoneNumber = "";
        }
      });
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;

      getAgencycode(function (response) {
        //var Agencycode = response;
        var Agencycode = 'AGY435';
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        self.dir = langauage == "ar" ? "rtl" : "ltr";
        var aboutUsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Hotel Booking/Hotel Booking/Hotel Booking.ftl';
        axios.get(aboutUsPage, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          self.getdata = false;
          if (response.data.area_List.length > 0) {
            var mainComp = response.data.area_List[0].main;

            self.ConfirmationHeader_Label = self.pluckcom('ConfirmationHeader_Label', mainComp.component);
            self.PaymentHeader_Label = self.pluckcom('PaymentHeader_Label', mainComp.component);
            self.TravellerDetailsHeader_Label = self.pluckcom('TravellerDetailsHeader_Label', mainComp.component);
            self.ReviewHotelHeader_Label = self.pluckcom('ReviewHotelHeader_Label', mainComp.component);
            self.SummaryDetails_Label = self.pluckcom('SummaryDetails_Label', mainComp.component);
            self.Nights_Label = self.pluckcom('Nights_Label', mainComp.component);
            self.Adults_Label = self.pluckcom('Adults_Label', mainComp.component);
            self.Child_Label = self.pluckcom('Child_Label', mainComp.component);
            self.Children_Label = self.pluckcom('Children_Label', mainComp.component);
            self.TotalPrice_Label = self.pluckcom('TotalPrice_Label', mainComp.component);
            self.SidebarNotes_Label = self.pluckcom('SidebarNotes_Label', mainComp.component);
            self.SignInButton_Label = self.pluckcom('SignInButton_Label', mainComp.component);
            self.LogoutButton_Label = self.pluckcom('LogoutButton_Label', mainComp.component);
            self.SignInNotes_Label = self.pluckcom('SignInNotes_Label', mainComp.component);
            self.PasswordNotes_Label = self.pluckcom('PasswordNotes_Label', mainComp.component);
            self.EmailNotes_Label = self.pluckcom('EmailNotes_Label', mainComp.component);
            self.Password_Placeholder = self.pluckcom('Password_Placeholder', mainComp.component);
            self.Email_Placeholder = self.pluckcom('Email_Placeholder', mainComp.component);
            self.AccountCreationQuestion_Label = self.pluckcom('AccountCreationQuestion_Label', mainComp.component);
            self.AccountCreationNotes_Label = self.pluckcom('AccountCreationNotes_Label', mainComp.component);

            self.ReviewYourBooking_Label = self.pluckcom('ReviewYourBooking_Label', mainComp.component);
            self.CheckOut_Label = self.pluckcom('CheckOut_Label', mainComp.component);
            self.CheckIn_Label = self.pluckcom('CheckIn_Label', mainComp.component);
            self.HotelInformation_Label = self.pluckcom('HotelInformation_Label', mainComp.component);
            self.HotelNorms_Label = self.pluckcom('HotelNorms_Label', mainComp.component);
            self.NoInformation_Label = self.pluckcom('NoInformation_Label', mainComp.component);
            self.CancellationPolicy_Label = self.pluckcom('CancellationPolicy_Label', mainComp.component);
            self.CancellationTextA1_Label = self.pluckcom('CancellationTextA1_Label', mainComp.component);
            self.CancellationTextA2_Label = self.pluckcom('CancellationTextA2_Label', mainComp.component);
            self.CancellationTextA3_Label = self.pluckcom('CancellationTextA3_Label', mainComp.component);
            self.CancellationTextB1_Label = self.pluckcom('CancellationTextB1_Label', mainComp.component);
            self.CancellationTextC1_Label = self.pluckcom('CancellationTextC1_Label', mainComp.component);
            self.CancellationTextC2_Label = self.pluckcom('CancellationTextC2_Label', mainComp.component);
            self.CancellationNotes_Label = self.pluckcom('CancellationNotes_Label', mainComp.component);

            self.GuestDetails_Label = self.pluckcom('GuestDetails_Label', mainComp.component);
            self.LeadPax_Label = self.pluckcom('LeadPax_Label', mainComp.component);
            self.LastName_Label = self.pluckcom('LastName_Label', mainComp.component);
            self.FirstName_Label = self.pluckcom('FirstName_Label', mainComp.component);
            self.Title_Label = self.pluckcom('Title_Label', mainComp.component);
            self.PaxNotes_Label = self.pluckcom('PaxNotes_Label', mainComp.component);
            self.ContactDetails_Label = self.pluckcom('ContactDetails_Label', mainComp.component);
            self.EmailId_Label = self.pluckcom('EmailId_Label', mainComp.component);
            self.PhoneNumber_Label = self.pluckcom('PhoneNumber_Label', mainComp.component);
            self.WhatsAppNotes_Label = self.pluckcom('WhatsAppNotes_Label', mainComp.component);
            self.ContinueToPaymentButton_Label = self.pluckcom('ContinueToPaymentButton_Label', mainComp.component);
            self.ValidationPopUp_Label = self.pluckcom('ValidationPopUp_Label', mainComp.component);
            self.ValidationRequiredFields_Label = self.pluckcom('ValidationRequiredFields_Label', mainComp.component);
            self.ValidationNotBookable_Label = self.pluckcom('ValidationNotBookable_Label', mainComp.component);
            self.WelcomeNotes_Label = self.pluckcom('WelcomeNotes_Label', mainComp.component);
            self.FinalPrice_Label = self.pluckcom('FinalPrice_Label', mainComp.component);
            self.FinalUpdatedPrice_Label = self.pluckcom('FinalUpdatedPrice_Label', mainComp.component);
            self.OkButton_Label = self.pluckcom('OkButton_Label', mainComp.component);
            self.CancelButton_Label = self.pluckcom('CancelButton_Label', mainComp.component);

            self.Room_Label = self.pluckcom('Room_Label', mainComp.component);
            self.AdultTitle_Label = self.pluckcom('AdultTitle_Label', mainComp.component);
            self.ChildTitle_Label = self.pluckcom('ChildTitle_Label', mainComp.component);
            self.Infant_Label = self.pluckcom('Infant_Label', mainComp.component);
            self.Pax_Label = self.pluckcom('Pax_Label', mainComp.component);
            self.Age_Label = self.pluckcom('Age_Label', mainComp.component);

            self.TermsAndPolicy1_Label = self.pluckcom('TermsAndPolicy1_Label', mainComp.component);
            self.TermsAndPolicy2_Label = self.pluckcom('TermsAndPolicy2_Label', mainComp.component);
            self.TermsAndPolicy3_Label = self.pluckcom('TermsAndPolicy3_Label', mainComp.component);
            self.TermsAndPolicy4_Label = self.pluckcom('TermsAndPolicy4_Label', mainComp.component);
            self.TermsAndPolicy5_Label = self.pluckcom('TermsAndPolicy5_Label', mainComp.component);

            self.Required_Message = self.pluckcom('Required_Message', mainComp.component);
            self.AlphaSpaces_Message = self.pluckcom('AlphaSpaces_Message', mainComp.component);
            self.Email_Message = self.pluckcom('Email_Message', mainComp.component);
            self.Numeric_Message = self.pluckcom('Numeric_Message', mainComp.component);
            self.MaximumLength_Message = self.pluckcom('MaximumLength_Message', mainComp.component);
            self.Regex_Message = self.pluckcom('Regex_Message', mainComp.component);
            self.TermsAndPolicy_Message = self.pluckcom('TermsAndPolicy_Message', mainComp.component);

            self.welcomeUser = (self.WelcomeNotes_Label || 'Welcome') + ', ' + currentUser.firstName + ' ' + currentUser.lastName + '! <span>(' + currentUser.emailId + ')</span>';

            if (localStorage.direction == 'rtl') {
              alertify.defaults.glossary.title = 'أليرتفاي جي اس';
              alertify.defaults.glossary.ok = 'موافق';
              alertify.defaults.glossary.cancel = 'إلغاء';
              alertify.confirm().set('labels', { ok: 'موافق', cancel: 'إلغاء' });
              alertify.alert().set('label', 'موافق');
            } else {
              alertify.defaults.glossary.title = 'AlertifyJS';
              alertify.defaults.glossary.ok = 'Ok';
              alertify.defaults.glossary.cancel = 'Cancel';
              alertify.confirm().set('labels', { ok: 'Ok', cancel: 'Cancel' });
              alertify.alert().set('label', 'Ok');
            }
           


            if (localStorage.direction == 'rtl') {
              $(".arborder").addClass("arbrder_left");
              $(".ar_direction").addClass("ar_direction1");
              $(".ajs-modal").addClass("ar_direction1");

              // $(".en_dwn").addClass("dwn_icon");
            } else {
              $(".arborder").removeClass("arbrder_left");
              $(".ar_direction").removeClass("ar_direction1");
              $(".ajs-modal").removeClass("ar_direction1");


              //$(".en_dwn").removeClass("dwn_icon");

            }

            self.pageDetails();

            self.veeValidateLocaleUpdate();
          }
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
      });

    },
    pageDetails() {
      var details = JSON.parse(window.sessionStorage.getItem("hotelInfo"));

      this.hotelDetails = details.content.searchResponse.hotels[0];
      this.searchCriteria = details.content.supplierSpecific.criteria;
      this.selectedRooms = details.content.searchResponse.hotels[0].roomResponse.selectedRooms;

      var paxDetails = [];
      var childCount = 0;
      var adultCount = 0;
      var paxPerRoom = [];
      var paxId = 1;
      this.bookingDetails.roomSummaryDetails = [];
      for (var roomIndex = 0; roomIndex < this.searchCriteria.rooms.length; roomIndex++) {
        var perRoomDetails = [];
        var guests = this.searchCriteria.rooms[roomIndex].guests;
        adultCount += guests.adult;
        if (guests.children != undefined && guests.children.length > 0) {
          childCount += guests.children.length;
        }
        var paxes = [];
        for (var guestIndex = 0; guestIndex < guests.adult; guestIndex++) {
          paxes.push(paxId);
          perRoomDetails.push({
            paxId: paxId++,
            leadPax: guestIndex == 0 ? true : false,
            age: null,
            name: "",
            surName: "",
            title: "Mr",
            type: "Adult",
            label: guestIndex == 0 ? this.LeadPax_Label : this.Pax_Label + " " + (guestIndex + 1)
          });
        }
        if (guests.children != undefined && guests.children.length > 0) {
          for (var guestIndex = 0; guestIndex < guests.children.length; guestIndex++) {
            paxes.push(paxId);
            perRoomDetails.push({
              paxId: paxId++,
              leadPax: false,
              age: guests.children[guestIndex].age,
              name: "",
              surName: "",
              title: "Master",
              type: "Child",
              label: guests.children[guestIndex].age < 2 ? (this.Infant_Label + " " + (guestIndex + 1) + ("(" + this.Age_Label + " " + guests.children[guestIndex].age + ")") ): this.Child_Label + " " + (guestIndex + 1) + ("(" + this.Age_Label + " " + guests.children[guestIndex].age + ")")
            });
          }

          this.bookingDetails.roomSummaryDetails.push({
            traveller: guests.adult + " " + this.Adults_Label + ' ' + guests.children.length + ' ' + (guests.children.length > 1 ? this.Child_Label : this.Children_Label),
            roomType: null
          });
          paxDetails.push({
            adult: guests.adult,
            child: guests.children.length,
            paxPerRoom: perRoomDetails
          });
        } else {
          this.bookingDetails.roomSummaryDetails.push({
            traveller: guests.adult + " " + this.Adults_Label,
            roomType: null
          });
          paxDetails.push({
            adult: guests.adult,
            child: 0,
            paxPerRoom: perRoomDetails
          });
        }
        paxPerRoom.push(paxes);
      }


      this.paxDetails.pax = paxDetails;
      this.paxDetails.paxPerRoom = paxPerRoom;
      this.bookingDetails.bookingPaxDetails = adultCount + " " + this.Adults_Label + (childCount != 0 ? (childCount + " " + (childCount == 1 ? this.Child_Label : this.Children_Label)) : '');

      var checkOut = moment(this.searchCriteria.stay.checkOut, 'DD/MM/YYYY');
      var checkIn = moment(this.searchCriteria.stay.checkIn, 'DD/MM/YYYY');
      this.bookingDetails.numberOfNights = moment.duration(moment(checkOut).diff(moment(checkIn))).asDays();

      var cost = parseFloat(0);
      for (var travellerIndex = 0; travellerIndex < this.bookingDetails.roomSummaryDetails.length; travellerIndex++) {
        this.bookingDetails.roomSummaryDetails[travellerIndex].roomPrice = this.selectedRooms[travellerIndex].room.rates[0].pricing.totalPrice.price;
        this.bookingDetails.roomSummaryDetails[travellerIndex].roomId = this.selectedRooms[travellerIndex].room.roomId;
        cost += parseFloat(this.selectedRooms[travellerIndex].room.rates[0].pricing.totalPrice.price);
        this.bookingDetails.roomSummaryDetails[travellerIndex].roomType = "Room " + (travellerIndex + 1) + ": " + this.selectedRooms[travellerIndex].room.roomName + " - " + (this.getMealType(this.selectedRooms[travellerIndex].room));
      }

      this.bookingDetails.totalCost = parseFloat(cost).toFixed(2);
      this.currentTotalCost = this.bookingDetails.totalCost;

      var hotelPolicies = details.content.searchResponse.hotels[0].prebookResponse.policies;
      var policyBindHtml = "";
      if (hotelPolicies) {

        for (var index = 0; index < hotelPolicies.length; index++) {
          if (hotelPolicies[index].description != undefined) {
            policyBindHtml += "<p>" + hotelPolicies[index].description + "</p>";
          }
          // else {
          //   policyBindHtml += "<p>" + this.NoInformation_Label + "</p>";

          // }
        }
        this.bookingDetails.hotelInformation = policyBindHtml;
      }

      var cancellationParsed = [];
      if (details.content.searchResponse.hotels[0].prebookResponse.hotel.hotelCancellationPolicies) {
        this.isHotelWiseCancellation = details.content.searchResponse.hotels[0].prebookResponse.hotel.hotelCancellationPolicies.length > 0;
      }


      var labels = {
        CancellationTextA1_Label: this.CancellationTextA1_Label,
        CancellationTextA2_Label: this.CancellationTextA2_Label,
        CancellationTextA3_Label: this.CancellationTextA3_Label,
        CancellationTextB1_Label: this.CancellationTextB1_Label,
        CancellationTextC1_Label: this.CancellationTextC1_Label,
        CancellationTextC2_Label: this.CancellationTextC2_Label

      }

      if (this.isHotelWiseCancellation) {
        cancellationParsed.push(ParseCancellationPolicyWithI18n(this.selectedRooms[0].room.rates[0].cancellationPolicies, this.bookingDetails.totalCost, labels));

      } else {
        for (var index = 0; index < this.selectedRooms.length; index++) {
          cancellationParsed.push(ParseCancellationPolicyWithI18n(this.selectedRooms[index].room.rates[0].cancellationPolicies, this.selectedRooms[index].room.rates[0].pricing.totalPrice.price, labels));
        }
      }

      this.bookingDetails.cancellationDetails.cancellationPolicy = cancellationParsed;

      var hotelNorms = []
      if (details.content.supplierSpecific.roomReferences && details.content.supplierSpecific.roomReferences.hotelNorms) {
        hotelNorms.push(details.content.supplierSpecific.roomReferences.hotelNorms);
      } else {
        for (var index = 0; index < this.selectedRooms.length; index++) {
          var temp = this.selectedRooms[index].room.rates[0].roomRateSpecific;
          if (temp) {
            if (temp.roomRateReferences) {
              hotelNorms.push(temp.roomRateReferences.hotelNorms);
            }
          }
        }
      }

      this.bookingDetails.hotelNorms = hotelNorms;

      this.bookingDetails.cInDate = {
        time: checkIn.format("hh:mm"),
        day: checkIn.format("D"),
        month: checkIn.format("MMM"),
        fullDate: checkIn.format("DD/MM/YYYY")
      };
      this.bookingDetails.cOutDate = {
        time: checkOut.format("hh:mm"),
        day: checkOut.format("D"),
        month: checkOut.format("MMM"),
        fullDate: checkOut.format("DD/MM/YYYY")
      };

      this.$nextTick(function () {

        //Don't remove scrollbar when showing alertify pop up.
        alertify.defaults.preventBodyShift = true;
        //Don't focus on top.
        alertify.defaults.maintainFocus = false;
        // Code that will run only after the
        // entire view has been rendered
        $(document).ready(function () {
          $('.myselect').select2({
            minimumResultsForSearch: Infinity,
            'width': '100%'
          });
          $('b[role="presentation"]').hide();
          $('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>');
        });

        //reinitialize jquery plugins
        // $(".summary-detail").affix({
        //     offset: {
        //         // Distance of between element and top page
        //         top: function () {
        //             return (this.top = $(".summary-detail").offset().top)
        //         }
        //     }
        // });
        var vm = this;
        $("#phone").intlTelInput({
          initialCountry: vm.currentUser.loginNode.country.code || "AE",
          geoIpLookup: function (callback) {
            $.get('https://ipinfo.io', function () { }, "jsonp").always(function (resp) {
              var countryCode = (resp && resp.country) ? resp.country : "";
              (countryCode);
            });
          },
          separateDialCode: true,
          autoPlaceholder: "off",
          preferredCountries: ['ae', 'sa', 'om', 'qa'],
          utilsScript: "../assets/js/intlTelInput.js?v=201903150000" // just for formatting/placeholders etc
        });
      })
    },
    getTermsnConditions: function () {
      try {
        var vm = this;
        var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;
        var serviceUrl = ServiceUrls.hubConnection.hubServices.contact;
        axios({
          method: "get",
          url: hubUrl + serviceUrl,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + localStorage.access_token
          }
        }).then(function (response) {
         
          var termsNconditions = vm.getContentTypeFromKey('termsnconditions', 'HotelTermsNConditions', response.data.siteList);

          if (stringIsNullorEmpty(termsNconditions)) {
            termsNconditions = vm.getContentTypeFromKey('termsnconditions', 'CommonTermsNConditions', response.data.siteList);
            if (stringIsNullorEmpty(termsNconditions)) {
              termsNconditions = vm.NoInformation_Label;
            }
          }

          var privacyPolicy = vm.getContentTypeFromKey('privacypolicy', 'HotelPrivacyPolicy', response.data.siteList);
          if (stringIsNullorEmpty(privacyPolicy)) {
            privacyPolicy = vm.getContentTypeFromKey('privacypolicy', 'CommonPrivacyPolicy', response.data.siteList);
            if (stringIsNullorEmpty(privacyPolicy)) {
              privacyPolicy = vm.NoInformation_Label;
            }
          }

          vm.privacyPolicy = privacyPolicy;
          vm.termsAndConditions = termsNconditions;

        }).catch(function (error) {
          console.log('Error: Terms n Conditions');
        });
      } catch (err) {
        console.log('Error: Terms n Conditions');
      }
    },
    getContentTypeFromKey: function (contentType, keyValue, siteList) {
      var value = '';
      var siteContents = siteList;
      if (hasArrayData(siteContents)) {
        var contentTypes = siteContents[0].contentType.filter(function (type) {
          return type.description.toLowerCase().split(' ').join('') == contentType.toLowerCase()
            && type.name.toLowerCase().split(' ').join('') == keyValue.toLowerCase()
        });
        if (hasArrayData(contentTypes)) {
          value = contentTypes[0].value;
        }
      }
      return value;
    },
    getUser: function() {
      if (localStorage.IsLogin) {
        var user = JSON.parse(localStorage.User);
        $('#emailtxt').val(user.emailId);
        $('#phonetxt').val(user.contactNumber);
        return user;
      }
      return null;
    },
    getPayGateways:function () {
      if (this.currentUser.loginNode.paymentGateways != null) {
        return this.currentUser.loginNode.paymentGateways;
      }
    },
    getMealType: function (room) {
     
      var mealType = "";
      if (room.rates[0].boards.length > 0 && room.rates[0].boards[0].value) {
        mealType = room.rates[0].boards[0].value.charAt(0).toUpperCase() + _.unescape(room.rates[0].boards[0].value.slice(1).toLowerCase());
      }
      return mealType;
    }, 
    veeValidateLocaleUpdate: function () {
        
      this.$validator.localize(localStorage.Languagecode ? localStorage.Languagecode : "en", {
          messages: {
            required: (field) => '* ' + field + ' ' + this.Required_Message,
            alpha_spaces: () => '* ' + this.AlphaSpaces_Message,
            email: () => '* ' + this.Email_Message,
            numeric: () => '* ' + this.Numeric_Message,
            max: () => '* ' + this.MaximumLength_Message,
            regex: () => '* ' + this.Regex_Message,
          },
          attributes: {
            terms: this.TermsAndPolicy_Message
          }
        })
      this.$validator.reset();

      var vm = this;
      this.$validator.extend('ppandtc', {
        getMessage: () => vm.TermsAndPolicy_Message,
        validate: () => vm.ppAndTcAccepted ? true : false
      });
    }
  },
  directives: {
    select2: {
      inserted(el) {
        $(el).on('select2:select', () => {
          const event = new Event('change', {
            bubbles: true,
            cancelable: true
          });
          el.dispatchEvent(event);
        });

        $(el).on('select2:unselect', () => {
          const event = new Event('change', {
            bubbles: true,
            cancelable: true
          })
          el.dispatchEvent(event)
        })
      },
    }
  }
  // ,
  // beforeRouteLeave(to, from, next) {
  //   // called when the route that renders this component is about to
  //   // be navigated away from.
  //   // has access to `this` component instance.
  //   // ;
  //   console.log("leaving now...")
  //   next();
  // }

};

// register modal component
Vue.component('vue-modal', {
  props: {
    showModal: Boolean,
    OkBtn: String,
    CancelBtn: String
  },
  template: `
  <transition name="modal" v-if="showModal">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container">

          <div class="modal-header">
            <slot name="header">
              default header
            </slot>
          </div>

          <div class="modal-body">
            <slot name="body">
              default body
            </slot>
          </div>

          <div class="modal-footer">
            <slot name="footer">
              <button class="modal-default-button btn btn-default" @click="$emit('continue-booking')">
                {{OkBtn}}
              </button>
              <button class="modal-default-button btn btn-default" @click="$emit('close')">
                {{CancelBtn}}
              </button>
            </slot>
          </div>
        </div>
      </div>
    </div>
  </transition>`
});

var currentUser = getUser();

function getUser() {
  if (localStorage.IsLogin) {
    var user = JSON.parse(localStorage.User);
    $('#emailtxt').val(user.emailId);
    $('#phonetxt').val(user.contactNumber);
    return user;
  }
  return null;
}

function checkLoginLocal() {

  if (localStorage.IsLogin === "true") {
    return true;
  } else {
    return false;
  }
}


function ParseCancellationPolicy(cancellationPolicy, roomPrice) {

  var parsedCancellationPolicies = [];
  for (var i = 0; i < cancellationPolicy.length; i++) {
    var to = moment(cancellationPolicy[i].to, "DD/MM/YYYY").format("MMM DD YYYY, dddd");
    var from = moment(cancellationPolicy[i].from, "DD/MM/YYYY").format("MMM DD YYYY, dddd");
    var amount = i18n.n((cancellationPolicy[i].amount / store.state.currencyMultiplier), 'currency', store.state.selectedCurrency);
    if (parseInt(cancellationPolicy[i].amount) == 0) {
      parsedCancellationPolicies.push('<p style="color:green;">Free cancellation on or before <b>' + to + '</b></p>');
    } else if (roomPrice == cancellationPolicy[i].amount && (to == "" && from == "")) {
      parsedCancellationPolicies.push('<p style="color:red;">Non refundable booking with cancellation charges <b>' + amount + '</b></p>');
    } else if (to != "" && from != "") {
      parsedCancellationPolicies.push("<p>Cancellation between <b>" + from + "</b> and <b>" + to + "</b> will be charged <b>" + amount + "</b></p>");
    }

  }
  return parsedCancellationPolicies;
}


function ParseCancellationPolicyWithI18n(cancellationPolicy, roomPrice, labels) {

  var parsedCancellationPolicies = [];

  for (var i = 0; i < cancellationPolicy.length; i++) {
    var to = moment(cancellationPolicy[i].to, "DD/MM/YYYY").format("MMM DD YYYY, dddd");
    var from = moment(cancellationPolicy[i].from, "DD/MM/YYYY").format("MMM DD YYYY, dddd");
    var amount = i18n.n((cancellationPolicy[i].amount / store.state.currencyMultiplier), 'currency', store.state.selectedCurrency);
    if (parseInt(cancellationPolicy[i].amount) == 0) {
      parsedCancellationPolicies.push('<p style="color:green;">' + labels.CancellationTextC1_Label + ' <b>' + to + '</b></p>');
    } else if (roomPrice == cancellationPolicy[i].amount && (to == "" && from == "")) {
      parsedCancellationPolicies.push('<p style="color:red;">' + labels.CancellationTextB1_Label + ' <b>' + amount + '</b></p>');
    } else if (to != "" && from != "") {
      parsedCancellationPolicies.push("<p>" + labels.CancellationTextA1_Label + " <b>" + from + "</b> " + labels.CancellationTextA2_Label + " <b>" + to + "</b> " + labels.CancellationTextA3_Label + " <b>" + amount + "</b></p>");
    }

  }
  return parsedCancellationPolicies;

}

function hasArrayData(value) {
  var status = false;
  if (value == undefined || value == null || value == '') {
    status = false;
  } else {
    if (value.length > 0) {
      status = true;
    }
  }
  return status;
}

function stringIsNullorEmpty(value) {
  var status = false;
  if (value == undefined || value == null || value == '') { status = true; }
  return status;
}