(function($) { 

/*--Back-to-top--*/ 
    $(document).ready(function(){
	
	//Check to see if the window is top if not then display button
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
	
});

        /* Booking form JS
============================================================== */
$("ul.nav-tabs a").click(function (e) {
  e.preventDefault();  
    $(this).tab('show');
});
    
    
    
$(".forgot-pass").click(function (e) {
  e.preventDefault();  
    $(this).tab('show');
    
    
});
        
    
    
    
   $(document).ready(function () { 
    $('.modal-hd').show();

    $('.forgot-pass').click(function() {
        $('.modal-hd').hide();
    });
    $('.modal-hd').hide(); // hide again once clicked off the tab.
});
    
    
    
    
    
/*--nav-dropdown-hover--*/
$('ul.nav li.dropdown').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).slideDown(300);
    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).slideUp(300);
    });
    
    
    
 /*--------Banner-Slider---------*/  
     $(document).ready(function() {

      $("#owl-demo-1").owlCarousel({
        autoplay:true,
        autoPlay : 8000,
        autoplayHoverPause:true, 
        stopOnHover : false,  
        items : 1,
        margin:10,  
        lazyLoad : true,
        navigation : true,
        itemsDesktop : [1199, 1],
        itemsDesktopSmall : [979, 1],
        itemsTablet : [768, 1],
      });
         
         $( ".owl-prev").html('<i class="fa  fa-chevron-left"></i>');
         $( ".owl-next").html('<i class="fa fa-chevron-right"></i>');
    });

    
     /*--------News-Slider---------*/  
     $(document).ready(function() {

      $("#owl-demo-").owlCarousel({
        autoplay:true,
        autoPlay : 8000,
        autoplayHoverPause:true, 
        stopOnHover : false,  
        items : 1,
        margin:10,  
        lazyLoad : true,
        navigation : true,
        itemsDesktop : [1199, 1],
        itemsDesktopSmall : [979, 1],
        itemsTablet : [768, 1],
          itemsMobile : [500, 1],
      });
         
         $( ".owl-prev").html('<i class="fa  fa-chevron-left"></i>');
         $( ".owl-next").html('<i class="fa fa-chevron-right"></i>');
    });

    
    
    
    
    
/*--select-form-js--*/    
$(document).ready(function(){
	$(function() {
		"use strict";
		
		$( "#departure_date" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#return_date" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#check_out" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#check_in" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#package_start" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#car_start" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#car_end" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#cruise_start" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#adult_count" ).spinner({
			min: 1
		});
		$( "#child_count" ).spinner( {
			min: 1
		});
		$( "#hotel_adult_count" ).spinner( {
			min: 1
		});
		$( "#hotel_child_count" ).spinner( {
			min: 1
		});
		// $('.selectpicker').selectpicker({
		// 	style: 'custom-select-button'
		// });
});
});
    
    /*---select-box-js---*/
    
    jQuery(document).ready(function($){
    $('.myselect').select2({minimumResultsForSearch: Infinity,'width':'100%'});
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa  fa-angle-down"></i>');
});
	    jQuery(document).ready(function($){
    $('.myselect-2').select2({minimumResultsForSearch: Infinity,'width':'100%'});
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('');
});

    
/*--date-picker--*/
     $( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#dept" )
        .datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to" ).datepicker({
        minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
    
    
    /*-- Secon-id--*/
     $( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#from-1" )
        .datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to-1" ).datepicker({
        minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
    
    
    /*-- Third Id --*/
    $( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#from-2" )
        .datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to-2" ).datepicker({
        minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
    
    
    /*-- Forth Id --*/
    $( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#from-3" )
        .datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to-3" ).datepicker({
        minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
    
    /*-- Five Id --*/
    $( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#from-4" )
        .datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to-4" ).datepicker({
        minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
    
    /*-- Six Id --*/
    $( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#from-5" )
        .datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to-5" ).datepicker({
        minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
    
    

 
/*--------logo-Slider---------*/  
    //  $(document).ready(function() {

    //   $("#owl-demo-3").owlCarousel({
    //     autoplay:true,
    //     autoPlay : 8000,
    //     autoplayHoverPause:true, 
    //     stopOnHover : false,  
    //     items : 5,
    //     margin:10,  
    //     lazyLoad : true,
    //     navigation : true,
    //     itemsDesktop : [1199, 4],
    //     itemsDesktopSmall : [979, 3],
    //     itemsTablet : [768, 2],
    //   });
         
    //      $( ".owl-prev").html('<i class="fa  fa-arrow-left"></i>');
    //      $( ".owl-next").html('<i class="fa  fa-arrow-right"></i>');
    // });


})(jQuery);