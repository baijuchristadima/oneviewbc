Vue.directive('click-outside', {
    bind: function (el, binding, vnode) {
        el.clickOutsideEvent = function (event) {
            // here I check that click was outside the el and his childrens
            if (!(el == event.target || el.contains(event.target))) {
                // and if it did, call method provided in attribute value
                if (vnode.context[binding.expression]) {
                    vnode.context[binding.expression](event);
                }
            }
        };
        document.body.addEventListener('click', el.clickOutsideEvent)
    },
    unbind: function (el) {
        document.body.removeEventListener('click', el.clickOutsideEvent)
    },
});


var hotelRooms = Vue.component("hotel-rooms", {
    data: function () {
        return {
            adults: [{
                    'value': 1
                },
                {
                    'value': 2
                },
                {
                    'value': 3
                },
                {
                    'value': 4
                },
                {
                    'value': 5
                },
                {
                    'value': 6
                },
                {
                    'value': 7
                }
            ],
            children: [{
                    'value': 0
                },
                {
                    'value': 1
                },
                {
                    'value': 2
                },

            ],
            childrenAges: [{
                    'value': 1,
                },
                {
                    'value': 2,
                },
                {
                    'value': 3,
                },
                {
                    'value': 4,
                },
                {
                    'value': 5,
                },
                {
                    'value': 6,
                },
                {
                    'value': 7,
                },
                {
                    'value': 8,
                },
                {
                    'value': 9,
                },
                {
                    'value': 10
                },
                {
                    'value': 11
                },
                {
                    'value': 12
                },
                // { 'value': 13 },
                // { 'value': 14 },
                // { 'value': 15 },
                // { 'value': 16 },
                // { 'value': 17 }
            ],
            selectedAdults: 1,
            selectedChildren: 0,
            selectedChildrenAges: [{
                    child1: 1
                },
                {
                    child2: 1
                }
            ]
        };
    },
    props: {
        roomId: Number,
        getRoomDetails: Function,
        hotellabel:Object
    },
    methods:{
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                if (type == 'Adult') {
                    this.selectedAdults = parseInt(event.target.value);
                }
                if (type == 'Child') {
                    this.selectedChildren = parseInt(event.target.value);
                }
                // if (type == 'childAge') {
                //     this.numberOfRooms = parseInt(event.target.value);
                // }
            }
        },
    },
    mounted: function () {
        this.$nextTick(function () {
            if (localStorage.direction == 'rtl') {
                //  this.arabic_dropdown = "dwn_icon";
                $(".en_dwn").addClass("dwn_icon");
            } else {
                //  this.arabic_dropdown = "";
                $(".en_dwn").removeClass("dwn_icon");
            }
            var vm = this;
            $('#adultdrpdwn').on("change", function (e) {
                vm.dropdownChange(e, 'Adult')
            });
            $('#childdrpdwn').on("change", function (e) {
                vm.dropdownChange(e, 'Child')
            });
        })
        this.$watch(function (vm) {
                return vm.selectedAdults, vm.selectedChildren, vm.selectedChildrenAges, Date.now();
            },
            function () {
                // Executes if data above have changed.
                this.$emit("get-room-details", {
                    roomId: this.roomId,
                    selectedAdults: this.selectedAdults,
                    selectedChildren: this.selectedChildren,
                    selectedChildrenAges: this.selectedChildrenAges
                })
                this.$nextTick(function () {
                    if (localStorage.direction == 'rtl') {
                        //  this.arabic_dropdown = "dwn_icon";
                        $(".en_dwn").addClass("dwn_icon");
                    } else {
                        //  this.arabic_dropdown = "";
                        $(".en_dwn").removeClass("dwn_icon");
                    }
                })
            }
        )
    },
    template: `
  <div>
  <div class="col-md-12 col-sm-12 search-col-padding">
  <div class="hotel-icon">
      <i class="fa  fa-hotel"><span>{{hotellabel.Room_Label}} ({{roomId+1}})</span></i> 
  </div>
</div>
  <div class="col-md-3 col-sm-3 search-col-padding">
  <label class="select-label">{{hotellabel.Adult_Label}}</label>
  <select class="form-control" v-model.number="selectedAdults">
    <option v-for="(adult, index) in adults" :value="adult.value">{{adult.value}}</option>
  </select>
</div>
<div class="col-md-3 col-sm-3 search-col-padding">
  <label class="select-label">{{hotellabel.Child_Label}}</label>
  <select class="form-control" v-model.number="selectedChildren">
  <option v-for="(child, index) in children" :value="child.value">{{child.value}}</option>
  </select>
</div>
  
<div v-for="(selectedChild, index) in parseInt(selectedChildren)" :key="index"
    class="col-md-3 col-sm-3 search-col-padding">
    <label class="select-label">{{hotellabel.Child_Age_Label}}</label>
    <select class="form-control" v-model.number="selectedChildrenAges[index]['child'+(index+1)]">
      <option v-for="(childAge, index) in childrenAges" :value="childAge.value">{{childAge.value}}</option>
    </select>
  </div>
  <div class="clearfix"></div>
</div>
  `
});

var hotelSeatch = Vue.component("hotel-search", {
    props: ['hotellabel'],
    data: function () {
        return {
            //city search
            isCompleted: false,
            keywordSearch: "",
            autoCompleteResult: [],
            cityCode: "",
            cityName: "",
            //supplier select
            showSupplierList: false,
            supplierList: [],
            selectedSuppliers: [],
            supplierSearchLabel: "All Suppliers",
            countryOfResidence: "AE",
            nationality: "AE",
            // datepicker
            checkIn: "",
            checkOut: "",
            numberOfNights: 1,
            numberOfRooms: 1,
            roomSelectionDetails: {
                room1: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room2: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room3: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room4: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room5: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                }
            },
            KMrange: 5,
            arabic_dropdown: '',
            highlightIndex: 0,
            content: {},
            searchForm: [],
            Hotel_form_caption: "",
            Hotel_city_placeholder: "",
            childLabel: "",
            childrenLabel: "",
            adultLabel: "",
            adultsLabel: "",
            ageLabel: "",
            agesLabel: "",
            roomLabel: "",
            roomsLabel: "",
            rangeLabel: "",
            searchBtnLabel: "",
            Search_range_label: ""
        }
    },
    created: function () {
        var agencyNode = window.localStorage.getItem("User");
        if (agencyNode) {
            agencyNode = JSON.parse(agencyNode);
            var servicesList = agencyNode.loginNode.servicesList;
            for (var i = 0; i < servicesList.length; i++) {
                if (servicesList[i].name == "Hotel" && servicesList[i].provider.length > 0) {
                    for (var j = 0; j < servicesList[i].provider.length; j++) {
                        this.supplierList.push({
                            id: servicesList[i].provider[j].id,
                            name: servicesList[i].provider[j].name.toUpperCase(),
                            supplierType: servicesList[i].provider[j].supplierType
                        });
                        this.selectedSuppliers = this.supplierList.map(function (supplier) {
                            return supplier.id
                        });
                    }
                    break;
                } else if (servicesList[i].name == "Hotel" && servicesList[i].provider.length == 0) {
                    //setLoginPage("login");
                }
            }
            // this.getPagecontent();

        }
    },
    computed: {
        selectAll: {
            get: function () {
                return this.supplierList ? this.selectedSuppliers.length == this.supplierList.length : false;
            },
            set: function (value) {
                var selectedSuppliers = [];

                if (value) {
                    this.supplierList.forEach(function (supplier) {
                        selectedSuppliers.push(supplier.id);
                    });
                }

                this.selectedSuppliers = selectedSuppliers;
            }
        }
    },
    methods: {
        onClickAutoCompleteEvent: function () {
            // if (this.autoCompleteResult.length > 0) {
            this.isCompleted = true;
            // } else {
            // this.isCompleted = false;
            // }
        },
        onKeyUpAutoCompleteEvent: _.debounce(function (keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
                var cityName = keywordEntered;
                var cityarray = cityName.split(' ');
                var uppercaseFirstLetter = "";
                for (var k = 0; k < cityarray.length; k++) {
                    uppercaseFirstLetter += cityarray[k].charAt(0).toUpperCase() + cityarray[k].slice(1).toLowerCase()
                    if (k < cityarray.length - 1) {
                        uppercaseFirstLetter += " ";
                    }
                }
                uppercaseFirstLetter = "*" + uppercaseFirstLetter + "*";
                var lowercaseLetter = "*" + cityName.toLowerCase() + "*";
                var uppercaseLetter = "*" + cityName.toUpperCase() + "*";


                var should = [];
                var i = 0;
                var supplier_type = "";
                for (var key in self.supplierList) {
                    // check if the property/key is defined in the object itself, not in parent
                    if (self.supplierList.hasOwnProperty(key)) {
                        supplier_type = [self.supplierList[key].supplierType, "Both"];
                        var supplierIndex = self.selectedSuppliers.indexOf(self.supplierList[key].id);
                        if (supplierIndex > -1) {

                            var supplier_id_query = {
                                bool: {
                                    must: [{
                                            match_phrase: {
                                                supplier_id: self.selectedSuppliers[supplierIndex]
                                            }
                                        },
                                        {
                                            terms: {
                                                supplier_type: supplier_type
                                            }
                                        }
                                    ]
                                }
                            };

                            should.push(supplier_id_query);
                            i++;
                        }
                    }
                }

                var query = {
                    query: {
                        bool: {
                            must: [{
                                    bool: {
                                        should: [{
                                                wildcard: {
                                                    supplier_city_name: uppercaseFirstLetter
                                                }
                                            },
                                            {
                                                wildcard: {
                                                    supplier_city_name: uppercaseLetter
                                                }
                                            },
                                            {
                                                wildcard: {
                                                    supplier_city_name: lowercaseLetter
                                                }
                                            }
                                        ]
                                    }
                                },
                                {
                                    bool: {
                                        should
                                    }
                                }
                            ]
                        }
                    }
                };

                var client = new elasticsearch.Client({
                    host: [{
                        host: ServiceUrls.elasticSearch.elasticsearchHost,
                        auth: ServiceUrls.elasticSearch.auth,
                        protocol: ServiceUrls.elasticSearch.protocol,
                        port: ServiceUrls.elasticSearch.port
                    }],
                    log: 'trace'
                });

                client.search({
                    index: 'city_map',
                    size: 150,
                    body: query
                }).then(function (resp) {
                    finalResult = [];
                    var hits = resp.hits.hits;
                    var Citymap = new Map();
                    for (var i = 0; i < hits.length; i++) {
                        Citymap.set(hits[i]._source.oneview_city_id, hits[i]._source);
                    }
                    var get_values = Citymap.values();
                    var Cityvalues = [];
                    for (var ele of get_values) {
                        Cityvalues.push(ele);
                    }
                    var results = SortInputFirst(cityName, Cityvalues);
                    for (var i = 0; i < results.length; i++) {
                        finalResult.push({
                            "code": results[i].oneview_city_id,
                            "label": results[i].supplier_city_name + ", " + results[i].oneview_country_name,
                            "location": results[i].location

                        });
                    }
                    var newData = [];
                    finalResult.forEach(function (item, index) {
                        if (item.label.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {

                            newData.push(item);
                        }
                    });
                    console.log(newData);
                    self.autoCompleteResult = newData;
                    self.isCompleted = true;
                    self.highlightIndex = 0;
                });

            } else {
                this.autoCompleteResult = [];
                this.isCompleted = false;
            }
        }, 100),
        updateResults: function (item) {
            this.keywordSearch = item.label;
            this.autoCompleteResult = [];
            this.cityCode = item.code;
            this.cityName = item.label;
            window.sessionStorage.setItem("cityLocation", JSON.stringify(item.location));
            $('#txtCheckInDate').focus();

        },
        showSupplierListDropdown: function () {
            if (this.supplierList.length != 1) {
                this.showSupplierList = !this.showSupplierList;
            }
            this.isCompleted = false;
        },
        getRoomDetails: function (data) {
            var selectedRoom = this.roomSelectionDetails['room' + (data.roomId + 1)];
            selectedRoom.adult = data.selectedAdults;
            selectedRoom.children = data.selectedChildren;
            if (data.selectedChildren == 0) {
                selectedRoom.childrenAges = [];
            } else if (data.selectedChildren == 1) {
                selectedRoom.childrenAges = [data.selectedChildrenAges[0]];
            } else if (data.selectedChildren == 2) {
                selectedRoom.childrenAges = data.selectedChildrenAges;
            }

            this.roomSelectionDetails['room' + (data.roomId + 1)] = selectedRoom;
        },
        setNumberOfNights: function () {
            var dt2 = $('#txtCheckOutDate');
            var startDate = $('#txtCheckInDate').datepicker('getDate');
            var a = $("#txtCheckInDate").datepicker('getDate').getTime();
            var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
            var c = 24 * 60 * 60 * 1000;
            var diffDates = Math.round(Math.abs((a - b) / c));

            if (event.target.value.match(/[^0-9\.]/gi)) {
                alertify.alert('Validate', 'Only numbers allowed.');
                this.numberOfNights = diffDates;
            } else if (event.target.value.match(/[d*\.]/gi)) {
                alertify.alert('Validate', 'Only whole numbers allowed.');
                this.numberOfNights = diffDates;
            } else if (event.target.value > 90) {
                alertify.alert('Maximum Nights', 'Maximum 90 nights allowed.');
                this.numberOfNights = diffDates;
            } else if (event.target.value == 0) {
                alertify.alert('Validate', 'Minimum one night allowed.');
                this.numberOfNights = diffDates;
            } else if (event.target.value == "") {
                this.numberOfNights = diffDates;
            } else {
                startDate.setDate(startDate.getDate() + this.numberOfNights);
                dt2.datepicker('setDate', startDate);
                eDate = dt2.datepicker('getDate').getTime();
                this.checkOut = $("#txtCheckOutDate").val();
            }
        },
        searchHotels: function () {
            var rooms = "";
            var adultCount = 0;
            var childCount = 0;

            for (var roomIndex = 1; roomIndex <= parseInt(this.numberOfRooms); roomIndex++) {
                var roomSelectionDetails = this.roomSelectionDetails['room' + roomIndex];
                adultCount += roomSelectionDetails.adult;
                childCount += roomSelectionDetails.children;
                rooms += "&room" + roomIndex + "=ADT_" + roomSelectionDetails.adult + ",CHD_" + roomSelectionDetails.children;
                for (var childIndex = 0; childIndex < roomSelectionDetails.childrenAges.length; childIndex++) {
                    rooms += "_" + roomSelectionDetails.childrenAges[childIndex]['child' + (childIndex + 1)];
                }
            }

            if (this.cityName.trim() == "" || this.cityCode == "") {
                alertify.alert(getValidationMsgByCode('M07'), getValidationMsgByCode('M06')).set('closable', false);
            } else if (this.selectedSuppliers.length == 0) {
                alertify.alert('Alert', 'Please select supplier.');
            } else if (this.checkIn == "") {
                alertify.alert('Alert', 'Please select check in date.');
            } else if (this.checkOut == "") {
                alertify.alert('Alert', 'Please select check out date.');
            } else if (this.countryOfResidence == "") {
                alertify.alert('Alert', 'Please select country of residence.');
            } else if (this.nationality == "") {
                alertify.alert('Alert', 'Please select nationality.');
            } else if (this.numberOfNights == "") {
                alertify.alert('Alert', 'Please enter number of nights.');
            } else if (this.numberOfRooms == "") {
                alertify.alert('Alert', 'Please select number of rooms.');
            } else if (this.numberOfNights > 90) {
                alertify.alert('Alert', 'Maximum nights allowed in a single booking is 90.');
            } else if (parseInt(adultCount + childCount) > 9) {
                alertify.alert('Alert', 'Maximum occupancy in a single booking is 9.');
            } else {

                var checkIn = $('#txtCheckInDate').val() == "" ? "" : $('#txtCheckInDate').datepicker('getDate');
                var checkOut = $('#txtCheckOutDate').val() == "" ? "" : $('#txtCheckOutDate').datepicker('getDate');

                var searchCriteria = "nationality=" + this.nationality +
                    "&country_of_residence=" + this.countryOfResidence +
                    "&city_code=" + this.cityCode +
                    "&city=" + this.cityName +
                    "&cin=" + moment(checkIn).format('DD/MM/YYYY') +
                    "&cout=" + moment(checkOut).format('DD/MM/YYYY') +
                    "&night=" + this.numberOfNights +
                    "&adtCount=" + adultCount +
                    "&chdCount=" + childCount +
                    "&guest=" + (adultCount + childCount) +
                    "&num_room=" + this.numberOfRooms + rooms +
                    "&KMrange=" + this.KMrange +
                    // "&supplier=" + this.selectedSuppliers.join(",") +
                    // "&sort=" + "price-a" +
                    "&uuid=" + uuidv4();
                var uri = "/Hotels/hotel-listing.html?" + searchCriteria;

                window.location.href = uri;
            }

        },
        hideSupplier: function () {
            this.showSupplierList = false;
        },
        hideCity: function () {
            this.isCompleted = false;
        },
        up: function () {
            if (this.isCompleted) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.isCompleted = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.isCompleted) {
                if (this.highlightIndex < this.autoCompleteResult.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.autoCompleteResult.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.isCompleted = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {

            var liH = this.$refs.options[this.highlightIndex].clientHeight;
            if (liH == 50) {
                liH = 32;
            }
            if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
            }
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        // getPagecontent: function () {
        //     var self = this;
        //     getAgencycode(function (response) {
        //         var Agencycode = response;
        //         var huburl = ServiceUrls.hubConnection.cmsUrl;
        //         var portno = ServiceUrls.hubConnection.ipAddress;
        //         var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        //         self.dir = langauage == "ar" ? "rtl" : "ltr";
        //         var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Home/Home/Home.ftl';
        //         var cmsurl = huburl + portno + homecms;
        //         axios.get(cmsurl, {
        //             headers: {
        //                 'content-type': 'text/html',
        //                 'Accept': 'text/html',
        //                 'Accept-Language': langauage
        //             }
        //         }).then(function (response) {
        //             self.content = response.data;
        //             self.searchForm = self.pluck('Search_form', self.content.area_List);
        //             self.Hotel_form_caption = self.pluckcom('Hotel_form_caption', self.searchForm[0].component);
        //             self.Hotel_city_placeholder = self.pluckcom('Hotel_city_placeholder', self.searchForm[0].component);
        //             self.Search_range_label = self.pluckcom('Search_range_label', self.searchForm[0].component);

        //             self.childLabel = self.pluckcom('child_label', self.searchForm[0].component);
        //             self.childrenLabel = self.pluckcom('children_label_', self.searchForm[0].component);
        //             self.adultLabel = self.pluckcom('Adult_label', self.searchForm[0].component);
        //             self.adultsLabel = self.pluckcom('adults_label', self.searchForm[0].component);
        //             self.ageLabel = self.pluckcom('Age_Label', self.searchForm[0].component);
        //             self.agesLabel = self.pluckcom('Ages_Label', self.searchForm[0].component);
        //             self.roomLabel = self.pluckcom('Room_label', self.searchForm[0].component);
        //             self.roomsLabel = self.pluckcom('Rooms_label', self.searchForm[0].component);
        //             self.rangeLabel = self.pluckcom('Range_Label', self.searchForm[0].component);
        //             self.searchBtnLabel = self.pluckcom('Search_button_label', self.searchForm[0].component);

        //             self.ValidationPopUpLabel = self.pluckcom('ValidationPopUp_Label', self.searchForm[0].component);
        //             self.ValidationRequiredFieldsLabel = self.pluckcom('ValidationRequiredFields_Label', self.searchForm[0].component);

        //             // if (localStorage.direction == 'rtl') {
        //             //     $.datepicker.setDefaults($.datepicker.regional["ar"]);
        //             // } else {
        //             //     $.datepicker.setDefaults($.datepicker.regional["en-GB"]);
        //             // }

        //             if (localStorage.direction == 'rtl') {
        //                 $(".ajs-modal").addClass("ar_direction1");
        //             } else {
        //                 $(".ajs-modal").removeClass("ar_direction1");
        //             }

        //             if (localStorage.direction == 'rtl') {
        //                 alertify.defaults.glossary.title = 'أليرتفاي جي اس';
        //                 alertify.defaults.glossary.ok = 'موافق';
        //                 alertify.defaults.glossary.cancel = 'إلغاء';
        //                 alertify.confirm().set('labels', {
        //                     ok: 'موافق',
        //                     cancel: 'إلغاء'
        //                 });
        //                 alertify.alert().set('label', 'موافق');
        //             } else {
        //                 alertify.defaults.glossary.title = 'AlertifyJS';
        //                 alertify.defaults.glossary.ok = 'Ok';
        //                 alertify.defaults.glossary.cancel = 'Cancel';
        //                 alertify.confirm().set('labels', {
        //                     ok: 'Ok',
        //                     cancel: 'Cancel'
        //                 });
        //                 alertify.alert().set('label', 'Ok');
        //             }

        //         }).catch(function (error) {
        //             console.log('Error');
        //             self.content = [];
        //         });

        //     });
        // },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                if (type == 'Room') {
                    this.numberOfRooms = parseInt(event.target.value);
                }
                // if (type == 'childAge') {
                //     this.numberOfRooms = parseInt(event.target.value);
                // }
            }
        },
    },
    watch: {
        selectedSuppliers: function () {
            if (this.selectedSuppliers.length == this.supplierList.length && this.supplierList.length != 1) {
                this.supplierSearchLabel = 'All Suppliers';
            } else if (this.selectedSuppliers.length == 0) {
                this.supplierSearchLabel = 'No supplier selected';
            } else if (this.selectedSuppliers.length > 4) {
                this.supplierSearchLabel = this.selectedSuppliers.length + ' supplier selected'
            } else {
                var vm = this;
                this.supplierSearchLabel = this.selectedSuppliers.map(function (id) {
                    return vm.supplierList.filter(function (supplier) {
                        return id == supplier.id;
                    })[0].name;
                }).join(",");
            }
            this.autoCompleteResult = [];
            this.keywordSearch = "";
        }
    },
    mounted: function () {



        if (localStorage.direction == 'rtl') {
            // $("#rangeSlider").removeClass("range_sec ");
            $("#rangeSlider").addClass("range_sec");
            this.arabic_dropdown = "dwn_icon";

            $(".ar_direction").addClass("ar_direction1");
        } else {
            $("#rangeSlider").removeClass("range_sec");

            this.arabic_dropdown = "";
            $(".ar_direction").removeClass("ar_direction1");
        }



        var vm = this;
        this.$nextTick(function () {
            var generalInformation = {
                systemSettings: {
                    systemDateFormat: 'dd M y,D',
                    calendarDisplay: 1,
                    calendarDisplayInMobile: 1
                },
            }

            var setDate = function (days) {
                return moment(new Date).add(days, 'days').format("DD/MM/YYYY");
            };

            var startDate = setDate(1);
            var endDate = setDate(2);

            var dateFormat = generalInformation.systemSettings.systemDateFormat;

            var tempnumberofmonths = 1;
            if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
                tempnumberofmonths = 1;
            } else if (parseInt($(window).width()) > 999) {
                tempnumberofmonths = 1;
            }

            var sDate = new Date(moment(startDate, "DD/MM/YYYY")).getTime();
            var eDate = new Date(moment(endDate, "DD/MM/YYYY")).getTime();

            //Checkin Date
            $("#txtCheckInDate").datepicker({
                dateFormat: dateFormat,
                minDate: 0, // 0 for search
                maxDate: "360d",
                numberOfMonths: parseInt(tempnumberofmonths),
                onSelect: function (date) {

                    var dt2 = $('#txtCheckOutDate');
                    var startDate = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('getDate');
                    sDate = startDate.getTime();
                    startDate.setDate(startDate.getDate() + 1);
                    dt2.datepicker('option', 'minDate', startDate);
                    dt2.datepicker('setDate', minDate);
                    var a = $("#txtCheckInDate").datepicker('getDate').getTime();
                    var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
                    var c = 24 * 60 * 60 * 1000;
                    var diffDays = Math.round(Math.abs((a - b) / c));
                    vm.numberOfNights = diffDays;
                    vm.checkIn = $("#txtCheckInDate").val();
                    $('.ui-datepicker-current-day').click();
                },
                beforeShowDay: function (date) {

                    var container = $(".multi_dropBox");
                    container.hide();

                    if (date.getTime() == eDate) {
                        return [true, 'event event-selected event-selected-right', ''];
                    } else if (date.getTime() <= eDate && date.getTime() >= $(this).datepicker('getDate').getTime()) {
                        if (date.getTime() == $(this).datepicker('getDate').getTime()) {
                            return [true, 'event-selected event-selected-left', ''];
                        } else {
                            return [true, 'event-selected', ''];
                        }
                    } else {
                        return [true, '', ''];
                    }
                }
            });
            $("#txtCheckInDate").datepicker({
                dateFormat: dateFormat
            }).datepicker("setDate", new Date().getDay + 1);
            vm.checkIn = $("#txtCheckInDate").val();

            //Checkout Date
            $('#txtCheckOutDate').datepicker({
                dateFormat: dateFormat,
                minDate: 2, // 2 for search
                maxDate: "360d",
                numberOfMonths: parseInt(tempnumberofmonths),
                onSelect: function (date) {
                    var a = $("#txtCheckInDate").datepicker('getDate').getTime();
                    var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
                    var c = 24 * 60 * 60 * 1000;
                    var diffDays = Math.round(Math.abs((a - b) / c));
                    if (diffDays > 90) {
                        alertify.alert('Maximum Nights', 'Maximum 90 nights allowed.');
                        vm.setNumberOfNights();
                    } else {
                        vm.numberOfNights = diffDays;
                        eDate = $(this).datepicker('getDate').getTime();
                    }
                    vm.checkOut = $("#txtCheckOutDate").val();
                },
                beforeShowDay: function (date) {
                    if (date.getTime() == sDate) {
                        return [true, 'event event-selected event-selected-left', ''];
                    } else if (date.getTime() >= sDate && date.getTime() <= $(this).datepicker('getDate').getTime()) {
                        if (date.getTime() == $(this).datepicker('getDate').getTime()) {
                            return [true, 'event-selected event-selected-right', ''];
                        } else {
                            return [true, 'event-selected', ''];
                        }
                    } else {
                        return [true, '', ''];
                    }
                }
            });

            $("#txtCheckOutDate").datepicker({
                dateFormat: dateFormat
            }).datepicker("setDate", new Date().getDay + 2);
            vm.checkOut = $("#txtCheckOutDate").val();

            //   var stepSlider = document.getElementById('rangeSlider');

            //   if (stepSlider.noUiSlider) {
            //       stepSlider.noUiSlider.updateOptions({
            //           direction: localStorage.direction == 'rtl' ? 'rtl' : 'ltr'
            //       });
            //   } else {
            //       noUiSlider.create(stepSlider, {
            //           start: [5],
            //           step: 1,
            //           direction: localStorage.direction == 'rtl' ? 'rtl' : 'ltr',
            //           range: {
            //               'min': [1],
            //               'max': [10]
            //           }
            //       });
            //   }

            //   stepSlider.noUiSlider.on('update', function (values, handle) {
            //       vm.KMrange = parseInt(values[handle]);
            //   });
            $('#roomsdrp').on("change", function (e) {
                vm.dropdownChange(e, 'Room')
            });
            // $('#agedrpdwn').on("change", function (e) {
            //     vm.dropdownChange(e, 'childAge')
            // });
        });

    },
    template: `
  <form>
  <div class="col-md-12 col-sm-12 search-col-padding">
    <div class="input-group">
      <label class="select-label">{{hotellabel.Destination_Label}}</label>
      <input id="txtCity" type="text" class="form-control ui-autocomplete-input" 
      :placeholder="hotellabel.Enter_City_Placeholder" autocomplete="off" v-model="keywordSearch" 
      @input="onKeyUpAutoCompleteEvent(keywordSearch)"
      @keydown.down="down"
      @keydown.up="up"
      @keydown.tab="isCompleted=false"
      @keydown.esc="isCompleted=false"
      @keydown.enter="updateResults(autoCompleteResult[highlightIndex])"
      @click="onClickAutoCompleteEvent">
      <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
      <ul ref="searchautocomplete" class="autocomplete-results" v-if="isCompleted&&autoCompleteResult.length!=0">
        <li ref="options" :class="{'autocomplete-result-active' : index == highlightIndex}" class="autocomplete-result" v-for="(item,index) in autoCompleteResult" :key="index" @click="updateResults(item)">
            {{ item.label }}
        </li>
      </ul>
      <span class="plane-icon map-marker"><i class="fa fa-map-marker"></i></span> </div>
  </div>
  
  <div class="col-md-6 col-sm-6 search-col-padding">
    <label class="select-label">{{hotellabel.Check_In_Label}}</label>
    <input id="txtCheckInDate" class="calendar" type="text" placeholder="DD/MM/YYYY" v-model="checkIn" readonly="readonly"/>
  </div>
  <div class="col-md-6 col-sm-6 search-col-padding">
    <label class="select-label">{{hotellabel.Check_Out_Label}}</label>
    <input id="txtCheckOutDate" class="calendar" type="text" placeholder="DD/MM/YYYY" readonly="readonly" v-model="checkOut"/>
  </div>
    
  <div class="col-md-12 col-sm-12 search-col-padding">
    <label class="select-label">{{hotellabel.Rooms_Label}}</label>
    <select class="myselect" v-model.number="numberOfRooms" id="roomsdrp">
      <option v-for="(room, index) in 5" :key="index" :value="index+1">{{(index+1)}}</option>
    </select>
  </div>
    
    <!-------Rooms-1------------>
    <hotel-rooms v-for="(room, index) in parseInt(numberOfRooms)" :key="index" :roomId="index" @get-room-details="getRoomDetails" :hotellabel="hotellabel">
    </hotel-rooms>
    <div class="clearfix"></div>
    <div class="clearfix"></div>
  <div class="col-md-4 col-sm-4 col-md-offset-8 search-col-padding col-sm-offset-8">
    <button type="submit" class="search-button" @click.prevent="searchHotels"><i class="fa fa-search"></i>{{hotellabel.Search_Button_Label}} </button>
  </div>
  <div class="clearfix"></div>
</form>
  `
});

//Hotel Search Niravan B2C end

/**Sorting Function for AutoCompelte Start**/
function SortInputFirst(input, data) {
    var first = [];
    var others = [];
    for (var i = 0; i < data.length; i++) {
        if (data[i].supplier_city_name.toLowerCase().startsWith(input.toLowerCase())) {
            first.push(data[i]);
        } else {

            others.push(data[i]);
        }
    }
    first.sort(function (a, b) {
        var nameA = a.supplier_city_name.toLowerCase(),
            nameB = b.supplier_city_name.toLowerCase()
        if (nameA < nameB) //sort string ascending
            return -1
        if (nameA > nameB)
            return 1
        return 0 //default return value (no sorting)
    })
    others.sort(function (a, b) {
        var nameA = a.supplier_city_name.toLowerCase(),
            nameB = b.supplier_city_name.toLowerCase()
        if (nameA < nameB) //sort string ascending
            return -1
        if (nameA > nameB)
            return 1
        return 0 //default return value (no sorting)
    })
    return (first.concat(others));
}
/**Sorting Function for AutoCompelte End**/

/**UUID Generation Start**/
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
/**UUID Generation End**/