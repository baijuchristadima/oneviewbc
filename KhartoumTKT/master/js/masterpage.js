var HeaderComponent = Vue.component('headeritem', {
    template: `  
    <div class="Tkt">
    <div id="header" class="ar_direction">
    <div class="top-nav">
    <div class="container">
        <div class="row">
           
            <div class="col-lg-4 col-md-4 col-sm-7 col-xs-7">
                <div class="header-right">
                    <div class="popup">
                   <a v-show="!userlogined">
                    <button type="button" class="btn-info btn-lg sign-r" data-toggle="modal" data-target="#Signin"><i :class="TopSection.Sign_In_Icon"></i> {{TopSection.Sign_In_Label}}</button>
                    <button id="btn-login" type="button" class="btn-info cr-btn btn-lg sign-r" data-toggle="modal" data-target="#cr-account"><i :class="TopSection.Register_Icon"></i> {{TopSection.Register_Label}}</button>
                   </a> 
                   <li>
                   <!--iflogin-->
                   <label id="bind-login-info" for="profile2" class="profile-dropdowns" v-show="userlogined">
                       <input type="checkbox" id="profile2">
                       <img src="/assets/images/user.png">
                       <span>{{userinfo.firstName}}</span>
                       <label for="profile2"><i class="fa fa-angle-down" aria-hidden="true"></i></label>
                       <ul>
                           <li><a href="/customer-profile.html"><i class="fa fa-th-large" aria-hidden="true"></i>{{TopSection.Dashboard_Label}}</a></li>
                           <li><a href="/my-profile.html"><i class="fa fa-user" aria-hidden="true"></i>{{TopSection.My_Profile_Label}}</a></li>
                           <li><a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i>{{TopSection.My_Booking_Label}}</a></li>
                           <li><a href="#" v-on:click.prevent="logout"><i class="fa fa-power-off" aria-hidden="true"></i>{{TopSection.Log_Out_Label}}</a></li>
                       </ul>
                   </label>
                   <!--ifnotlogin-->
                   </li>

                    
                     <div class="modal modal-hd fade" id="Signin" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        
        </div>
        <div class="modal-body">
            <div class="model-text">
                 <h3>{{TopSection.Login_Box_Heading}}<br><span>{{TopSection.Login_Box_Sub_Heading}}</span></h3>
                          <ul>
                          <li v-for="description in TopSection.Description_List"><i class="fa  fa-check-square-o"></i>{{description.Description}}</li>
                </ul>
            </div>
            
            <div class="model-form">
                <h3>{{TopSection.Sign_In_Label}}</h3>
                <hr>
               <!-- <ul>
                    <li class="fb-model"><a href="#">Facebook</a></li>
                    <li class="google-model"><a href="#">Google</a></li>
                </ul>-->
                
                <form>
                  <div class="validation_sec">
                    <input type="email" id="email" name="email" class="form-control" :placeholder="TopSection.Email_Placeholder" v-model="username">
                    <span class="cd-signin-modal__error sp_validation" v-bind:class="{ 'cd-signin-modal__error--is-visible': usererrormsg.empty||usererrormsg.invalid }">{{usererrormsg.empty?getValidationMsgByCode('M02'):(usererrormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                  </div>
                  <div class="validation_sec">
                    <input type="password" id="password" :placeholder="TopSection.Password_Placeholder" class="form-control" v-model="password" >
                    <span class="cd-signin-modal__error sp_validation" v-bind:class="{ 'cd-signin-modal__error--is-visible': psserrormsg }">
                    {{getValidationMsgByCode('M33')}}</span>
                  </div>
                    <input type="submit" id="submit" v-on:click.prevent="loginaction" :value="TopSection.Sign_In_Label" />
                </form>
                
                <a class="forgot-pass" href="#" data-toggle="modal" data-target="#forgot-pass" data-dismiss="modal">{{TopSection.Forgot_Password_Label}}?</a>
                
                
                
                <p class="cr-ac">{{TopSection.Donot_Have_Account_Label}} <a href="#" data-toggle="modal" data-target="#cr-account" data-dismiss="modal">{{TopSection.Register_Label}}</a></p>
                
            </div>
            
        </div>
        
      </div>
      
    </div>
  </div>
                        
                        
                        <div class="modal fade" id="forgot-pass" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        
        </div>
        <div class="modal-body">
            <div class="model-text">
                 <h3>{{TopSection.Login_Box_Heading}}<br><span> {{TopSection.Login_Box_Sub_Heading}}</span></h3>
                          <ul>
                          <li v-for="description in TopSection.Description_List"><i class="fa  fa-check-square-o"></i>{{description.Description}}</li>
                </ul>
            </div>
            
            <div class="model-form">
                <h3>{{TopSection.Forgot_Password_Label}}</h3>
                <hr>
                <!-- <ul>
                    <li class="fb-model"><a href="#">Facebook</a></li>
                    <li class="google-model"><a href="#">Google</a></li>
                </ul>-->
              <p v-html="TopSection.Forgot_Password_Description"></p>
                <form>
                <div class="validation_sec">
                    
                    <input type="email" id="email" name="email" class="form-control" :placeholder="TopSection.Email_Placeholder" v-model="emailId">
                    <span class="cd-signin-modal__error sp_validation" v-bind:class="{ 'cd-signin-modal__error--is-visible': userforgotErrormsg.empty||userforgotErrormsg.invalid }">{{userforgotErrormsg.empty?getValidationMsgByCode('M02'):(userforgotErrormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                 
                    </div>
                    <input type="submit" id="submit" name="submit" :value="TopSection.Reset_Password_Label" v-on:click.prevent="forgotPassword">
                    <span class="cd-signin-modal__error sp_validation" v-bind:class="{ 'cd-signin-modal__error--is-visible': usererrormsg.empty||usererrormsg.invalid }">{{usererrormsg.empty?getValidationMsgByCode('M02'):(usererrormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                    <a href="#" class="back-login" data-toggle="modal" data-target="#Signin" data-dismiss="modal">{{TopSection.BackTo_Login_Label}} </a>
                    
                </form>
              
               
                
            </div>
            
        </div>
        
      </div>
      
    </div>
  </div>
                    <div class="modal modal-hd  fade" id="cr-account" role="dialog">
                        
                        <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        
        </div>
        <div class="modal-body">
            <div class="model-text">
                 <h3>{{TopSection.Login_Box_Heading}}<br><span> {{TopSection.Login_Box_Sub_Heading}}</span></h3>
                          <ul>
                              <li v-for="description in TopSection.Description_List"><i class="fa  fa-check-square-o"></i>{{description.Description}}</li>
                             
                </ul>
            </div>
            
            <div class="model-form">
                <h3>{{TopSection.Register_Label}}</h3>
                <hr>
                
              <!-- <ul>
                    <li class="fb-model"><a href="#">Facebook</a></li>
                    <li class="google-model"><a href="#">Google</a></li>
                </ul>-->
                
                <form>
                <div class="validation_sec" style="z-index:999;">
                    <select v-model="registerUserData.title">
                        <option v-for="list in TopSection.Title_List">{{list.Title}}</option>
                      
                    </select>
                    </div>
                    <div class="validation_sec" style="z-index:999;">
                    <input type="text" id="text" name="text" class="form-control name-se" :placeholder="TopSection.Full_Name_Placeholder"  v-model="registerUserData.firstName">
                    <span class="cd-signin-modal__error sp_validation" v-bind:class="{ 'cd-signin-modal__error--is-visible': userFirstNameErormsg }">
                    {{getValidationMsgByCode('M31')}}</span>
                    </div>

                    <div class="validation_sec">
                    <input type="text" id="signup-lastname" name="lastname" class="form-control" :placeholder="TopSection.Last_Name_Placeholder"  v-model="registerUserData.lastName">
                    <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userLastNameErrormsg }">{{getValidationMsgByCode('M32')}}</span>
                    </div>
                    <div class="validation_sec">
                    <input type="email" id="email" name="email" class="form-control" :placeholder="TopSection.Email_Placeholder"  v-model="registerUserData.emailId">
                    <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userEmailErormsg.empty||userEmailErormsg.invalid }">{{userEmailErormsg.empty?getValidationMsgByCode('M02'):(userEmailErormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                    </div>
                    <input type="checkbox" id="checkbox" v-model="isChecked"> {{TopSection.Send_Me_Deals_And_Offers_Label}}
                    <input type="submit" id="submit" name="submit" :value="TopSection.Register_Label" v-on:click.prevent="registerUser">
                   
                </form>
                
                
                <p class="cr-ac">{{TopSection.Already_Register_Label}} <a href="#" data-toggle="modal" data-target="#Signin" data-dismiss="modal">{{TopSection.Sign_In_Label}}</a></p>
                
            </div>
            
        </div>
        
      </div>
      
    </div>
                        
                    </div>
                    </div>
  

                    <a :href="'mailto:'+TopSection.Email_Address" class="email-top"><i :class="TopSection.Email_Icon"></i>{{TopSection.Email_Address}}</a>
              
                </div>
                
            </div>
            <div class="col-lg-5 col-md-5 col-sm-3 col-xs-3">
            <nav class="navbar menu_nav navbar-default" id="nav_bar" >
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!--<a class="navbar-brand" href="#">Brand</a>-->
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li :class="{ 'active': activeClass === '/index.html' || activeClass === '/'}"><a  href="/KhartoumTKT/index.html">{{TopSection.Home_Label}}</a></li>
        <li :class="{ 'active': activeClass === '/about.html'}"><a href="/KhartoumTKT/about.html">{{TopSection.About_us_Label}}</a></li>
        <li :class="{ 'active': activeClass === '/promotions.html'}"><a href="/KhartoumTKT/promotions.html">{{TopSection.Promotions_Label}}</a></li>
        <!--<li :class="{ 'active': activeClass === '/terms-condition.html'}"><a href="/KhartoumTKT/terms-condition.html">{{TopSection._Terms_And_Conditions_Label}}</a></li> -->
        <li :class="{ 'active': activeClass === '/contact.html'}"><a href="/KhartoumTKT/contact.html">{{TopSection.Contact_Us_Label}}</a></li>
        <li v-if="checkMobileOrNot"><a href="/KhartoumTKT/news-event.html">{{topSectionLabel.News_Events_Label}}</a></li>
        <li v-if="checkMobileOrNot"><a href="/KhartoumTKT/privacypolicy.html">{{topSectionLabel.Privacy_Policy_Label}}</a></li> 
         
      </ul>

    </div>
    <!-- /.navbar-collapse -->
  <!-- /.container-fluid -->
</nav>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2">
        <div class="list-unstyled list-inline">
        <div class="lang lag"><lang-select @languagechange="getpagecontent"></lang-select></div>
        <div class="currency lang lag">
        <div><currency-select></currency-select></div>
    </div> 
              </div>
      </div>
        </div>
    </div>
        </div>
    
    <!-- Navigation -->
<div class="container">
    <div class="row">
        <div class="col-md-2 col-sm-3 col-xs-4">
                    <div class="logo">
                        <a href="/KhartoumTKT/index.html"><img :src="TopSection.Logo_143x142px" alt="logo"></a>
                    </div>
            </div>
        <div class="col-md-10 col-sm-9 col-xs-8">
            
            <div class="customer-service">
                <div class="customer-text">
                <h4>{{TopSection.Customer_Service_Label}} <br><a :href="'tel:' +TopSection.Customer_Care_Number">{{TopSection.Customer_Care_Number}}</a></h4>
                <p v-html="TopSection.Customer_Service_Description"></p>
                    </div>
                
                <div class="customer-icon">
                    <img :src="TopSection.Service_icon_59x59px" alt="icon">
                </div>
                
            </div>
            
        </div>
    </div>
</div>  
  </div>
  </div>
`,
    data() {
        return {
            language: 'en',
            active_el: (sessionStorage.active_el) ? sessionStorage.active_el : 0,
            isChecked: false,
            TopSection: {},
            validationList: {},
            activeClass: '',
            isLoading: false,
            fullPage: true,
            userlogined: this.checklogin(),
            userlogined: '',
            userinfo: [],
            username: '',
            password: '',
            emailId: '',
            retrieveEmailId: '',
            usererrormsg: {
                empty: false,
                invalid: false
            },
            psserrormsg: false,
            userFirstNameErormsg: false,
            userLastNameErrormsg: false,
            userEmailErormsg: {
                empty: false,
                invalid: false
            },
            userPasswordErormsg: false,
            userVerPasswordErormsg: false,
            userPwdMisMatcherrormsg: false,
            userTerms: false,
            userforgotErrormsg: {
                empty: false,
                invalid: false
            },

            retrieveEmailErormsg: false,
            registerUserData: {
                firstName: '',
                lastName: '',
                emailId: '',
                password: '',
                verifyPassword: '',
                terms: '',
                title: 'Mr'
            },
            sessionids: "",
            Ipaddress: "",
            DeviceInfo: "",
            pageType: "",
            PageName: "",
            activeClass: '',
            topSectionLabel: {}

        }
    },
    created() {
        var self = this;
        var pathArray = window.location.pathname.split('/');
        self.activeClass = '/' + pathArray[pathArray.length - 1];

    },
    computed: {
        checkMobileOrNot: function () {
           
            if( screen.width <= 1020 ) {
                return true;
            }
            else {
                return false;
            }
                
        }
    },
    methods: {
        activate: function (el) {
            sessionStorage.active_el = el;
            this.active_el = el;
            if (el == 1 || el == 2) {
                maininstance.actvetab = el;
            } else {
                maininstance.actvetab = 0;
            }



        },
        getpagecontent: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/01 Master/01 Master/01 Master.ftl';
                axios.get(pageurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    if (response.data.area_List.length) {
                        var headerSection = pluck('Header_Section', response.data.area_List);
                        self.TopSection = getAllMapData(headerSection[0].component);

                        var Footer = pluck('Footer_Section', response.data.area_List);
                        self.topSectionLabel = getAllMapData(Footer[0].component);
                    }
                    self.swapStyle();
                    self.isLoading = false;
                }).catch(function (error) {
                    self.isLoading = false;
                    console.log('Error');
                });
            });
        },
        swapStyle:function(){
            if (localStorage.direction == 'rtl') {
                    document.getElementById("styleid").setAttribute("href", 'css/style-rtl.css');      
                    document.getElementById("mediaid").setAttribute("href", 'css/media-rtl.css');     
                    $(".ar_direction").addClass("ar_direction1");
                    alertify.defaults.glossary.ok = 'موافق';
                    this.getValidationMsgs();
            } else {
                    document.getElementById("styleid").setAttribute("href", 'css/style-ltr.css');
                    document.getElementById("mediaid").setAttribute("href", 'css/media-ltr.css');
                    $(".ar_direction").removeClass("ar_direction1");
                     alertify.defaults.glossary.ok = 'OK';
                      this.getValidationMsgs();
            }
            partnerinstance.key = Math.random();
        },
        getValidationMsgs: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Validation/Validation/Validation.ftl';
                axios.get(pageurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    if (response.data.area_List.length) {
                        var validations = pluck('Validations', response.data.area_List);
                        self.validationList = getAllMapData(validations[0].component);
                        sessionStorage.setItem('validationItems', JSON.stringify(self.validationList));
                    }
                }).catch(function (error) {
                    console.log('Error');
                });
            });
        },
        loginaction: function () {
            if (!this.username.trim()) {
                this.usererrormsg = {
                    empty: true,
                    invalid: false
                };
                return false;
            } else if (!this.validEmail(this.username.trim())) {
                this.usererrormsg = {
                    empty: false,
                    invalid: true
                };
                return false;
            } else {
                this.usererrormsg = {
                    empty: false,
                    invalid: false
                };
            }
            if (!this.password) {
                this.psserrormsg = true;
                return false;

            } else {
               
                this.psserrormsg = false;
                var self = this;
                self.isLoading = true;
                login(this.username, this.password, function (response) {
                    if (response == false) {
                        self.userlogined = false;
                        self.isLoading = false;
                        alertify.alert(this.getValidationMsgByCode('M07'),this.getValidationMsgByCode('M37'));
                    } else {
                        self.userlogined = true;
                        self.userinfo = JSON.parse(localStorage.User);
                        $('#Signin').modal('toggle');


                        try {
                            self.$eventHub.$emit('logged-in', {
                                userName: self.username,
                                password: self.password
                            });
                            signArea.headerLogin({
                                userName: self.username,
                                password: self.password
                            })
                            // self.username ='';
                            // self.password = '';
                            self.isLoading = false;

                        } catch (error) {
                            self.isLoading = false;
                        }
                    }
                });

            }



        },
        validEmail: function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        checklogin: function () {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);
                return true;
            } else {
                this.userlogined = false;
                return false;
            }
        },
        logout: function () {
            this.userlogined = false;
            this.userinfo = [];
            localStorage.profileUpdated = false;
            try {
                this.$eventHub.$emit('logged-out');
                signArea.logout()
            } catch (error) {

            }
            commonlogin();
            // signOut();
            // signOutFb();
        },
        registerUser: function () {
            if (this.registerUserData.firstName.trim() == "") {
                this.userFirstNameErormsg = true;
                return false;
            } else {
                this.userFirstNameErormsg = false;
            }
            if (this.registerUserData.lastName.trim() == "") {
                this.userLastNameErrormsg = true;
                return false;
            } else {
                this.userLastNameErrormsg = false;
            }
            if (this.registerUserData.emailId == "") {
                this.userEmailErormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.registerUserData.emailId)) {
                this.userEmailErormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.userEmailErormsg =  { empty: false, invalid: false };
            }
            var vm = this;
            vm.isLoading = true;
            registerUser(this.registerUserData.emailId, this.registerUserData.firstName, this.registerUserData.lastName, this.registerUserData.title, function (response) {
                if (response.isSuccess == true) {
                    try {
                        if (this.isChecked) {
                            let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                            let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                            let insertSubscibeData = {
                                type: "Subscription",
                                date1: requestedDate,
                                keyword1: this.registerUserData.emailId,
                                nodeCode: agencyCode

                            };
                            let responseObject = cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                            this.registerUserData.firstName = '';
                            this.registerUserData.emailId = '';
                            this.registerUserData.title = '';
                        }
                        vm.isLoading = false;

                    } catch (error) {
                        vm.isLoading = false;
                    }
                    vm.username = response.data.data.user.loginId;
                    vm.password = response.data.data.user.password;
                    login(vm.username, vm.password, function (response) {
                        if (response != false) {
                            $('#Signin').modal('toggle');
                            //  $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                            window.location.href = "/edit-my-profile.html?edit-profile=true";
                        }
                    });
                }
                vm.isLoading = false;
            });
        },

        forgotPassword: function () {
            if (this.emailId.trim() == "") {
                this.userforgotErrormsg = {
                    empty: true,
                    invalid: false
                };
                return false;
            } else if (!this.validEmail(this.emailId.trim())) {
                this.userforgotErrormsg = {
                    empty: false,
                    invalid: true
                };
                return false;
            } else {
                this.userforgotErrormsg = {
                    empty: false,
                    invalid: false
                };
            }
            var self = this;
            self.isLoading = true;
            var datas = {
                emailId: this.emailId,
                agencyCode: localStorage.AgencyCode,
                logoUrl: window.location.origin + "/" + localStorage.AgencyFolderName + "/website-informations/logo/logo.png",
                websiteUrl: window.location.origin,
                resetUri: window.location.origin + "/" + localStorage.AgencyFolderName + "/reset-password.html"
            };
            $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:none;background:grey !important;");

            var huburl = ServiceUrls.hubConnection.baseUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var requrl = ServiceUrls.hubConnection.hubServices.forgotPasswordUrl;
            axios.post(huburl + portno + requrl, datas)
                .then((response) => {
                    if (response.data != "") {
                        alertify.alert(this.getValidationMsgByCode('M07'), response.data);
                        this.emailId = '';
                    } else {
                        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M34'));
                    }
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:var(--primary-color) !important");
                    self.isLoading = false;
                    $('#forgot-pass').modal('toggle');
                })
                .catch((err) => {
                    console.log("FORGOT PASSWORD  ERROR: ", err);
                    if (err.response.data.message == 'No User is registered with this emailId.') {
                        alertify.alert(this.getValidationMsgByCode('M07') , err.response.data.message);
                    } else {
                        alertify.alert(this.getValidationMsgByCode('M07') ,this.getValidationMsgByCode('M36'));
                    }
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:var(--primary-color) !important");
                   
                    self.isLoading = false;
                })
        },
        Getvisit: function () {
            var self = this;
            var currentUrl = window.location.pathname;
            if (currentUrl == "/KhartoumTKT/index.html") {
                self.pageType = "Master";
                self.PageName = "Home"
            }
            else if (currentUrl == "/KhartoumTKT/holiday-detail.html") {
                self.pageType = "Package details";
                self.PageName = packageView.mainContent.Package_Name;
              } 
              else {
                self.PageName = currentUrl;
                self.pageType = "Master";
              }

            var TempDeviceInfo = detect.parse(navigator.userAgent);
            self.DeviceInfo = TempDeviceInfo;
            this.showYourIp();
        },
        showYourIp: function () {
            var self = this;
            var ipUrl = ServiceUrls.externalServiceUrl.ipAddressApi;
            fetch(ipUrl)
                .then(x => x.json())
                .then(({
                    ip
                }) => {
                    let Ipaddress = ip;
                    let sessionids = ip.replace(/\./g, '');
                    this.sendip(Ipaddress,sessionids);
                });
        },
        sendip: async function (Ipaddress,sessionids) {
            var self = this;
            if (self.PageName == null || self.PageName == undefined || self.PageName == "") {
                var pathArray = window.location.pathname.split('/');
                var activePage = pathArray[pathArray.length - 1];


            }
            let agencyCode = JSON.parse(localStorage.User).loginNode.code;
            let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
            let insertVisitData = {
                type: "Visit",
                keyword1: sessionids,
                ip1: Ipaddress,
                keyword2: self.pageType,
                keyword3: self.PageName,
                keyword4: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'AED',
                keyword5: (localStorage.Languagecode) ? localStorage.Languagecode : "en",
                text1: self.DeviceInfo.device.type,
                text2: self.DeviceInfo.os.name,
                text3: self.DeviceInfo.browser.name,
                date1: requestedDate,
                nodeCode: agencyCode
            };
            cmsRequestData("POST", "cms/data", insertVisitData, null);
        },
        getValidationMsgByCode: function (code) {
            if (sessionStorage.validationItems !== undefined) {
              var validationList = JSON.parse(sessionStorage.validationItems);
              for (let validationItem of validationList.Validation_List) {
                if (code === validationItem.Code) {
                  return validationItem.Message;
                }
              }
            }
          }
    },

    mounted: function () {
        $("#dropdown09").addClass("btn-lg sign-r");
        if (localStorage.IsLogin == "true") {
            this.userlogined = true;
            this.userinfo = JSON.parse(localStorage.User);

        } else {
            this.userlogined = false;

        }
        this.getpagecontent();
        this.getValidationMsgs();
        setTimeout(() => {
            this.Getvisit();
        }, 1000);

    },
    watch: {
        updatelogin: function () {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);

            } else {
                this.userlogined = false;

            }

        }

    }
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('footeritem', {
    template: `
    <div class="Tkt ar_direction">
    <div id="footer">
    <div class="container" v-if="!checkMobileOrNot">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-3 col-md-4 col-sm-4">
            <div class="footer-logo">
                <img :src="TopSection.Logo_143x142px" alt="logo">
                <p> <i :class="FooterSection.Address_Icon"></i>{{FooterSection.Address_Description}}</p>
            </div>
        </div>
        
        
        <div class="col-lg-3 col-md-4 col-sm-4">
            <div class="footer-contact">
                <div class="phone-email">
                    <h5>{{FooterSection.Call_Us_Title}}</h5>
                    <a :href="'tel:' +FooterSection.Phone_Number"><i :class="FooterSection.Call_Us_Icon"></i> {{FooterSection.Phone_Number}}</a>
                    <!--<a class="last-number" :href="'tel:' + FooterSection.Phone">{{FooterSection.Phone}}</a>-->
                </div>
                
                <div class="phone-email">
                    <h5>{{FooterSection.Email_Us_Label}}</h5>
                    <a :href="'mailto:'+TopSection.Email_Address"><i :class="TopSection.Email_Icon"></i>{{TopSection.Email_Address}}</a>
                </div>
                
                <ul>
                  <li v-for="social in FooterSection.Social_Media_List"><a :href="social.Path" target="_blank"><i :class="social.Icon"></i></a></li>
                     <li><img :src="FooterSection.Iata_image_46x29px" alt="logo"></li>
                </ul>
            </div>
        </div>
        
        
        
        <div class="col-lg-2 col-md-4 col-sm-4">
            <div class="quick-link">
                <h4>{{FooterSection.Quick_Links_Label}}</h4>
                <ul>
                    <li ><a  href="/KhartoumTKT/index.html">{{TopSection.Home_Label}}</a></li>
                    <li><a href="/KhartoumTKT/about.html">{{TopSection.About_us_Label}}</a></li>
                    <li><a href="/KhartoumTKT/promotions.html">{{TopSection.Promotions_Label}}</a></li>
                    <li><a href="/KhartoumTKT/news-event.html">{{FooterSection.News_Events_Label}}</a></li>
                    <li><a href="/KhartoumTKT/terms-condition.html">{{TopSection._Terms_And_Conditions_Label}}</a></li> 
                    <li class="active"><a href="/KhartoumTKT/contact.html">{{TopSection.Contact_Us_Label}}</a></li> 
                    <li><a href="/KhartoumTKT/privacypolicy.html">{{FooterSection.Privacy_Policy_Label}}</a></li> 
                </ul>
            </div>
        </div>
        
        

      
    </div>
</div>
<section class="all-reserved">
<div class="container">
    <div class="reserved">
        <p v-html="FooterSection.Copyright_Description"></p>
    </div>
    
    <div class="Powered">
        <p>{{FooterSection.Powered_By_Label}}</p>
        <a href="http://www.oneviewit.com/" target="_blank"><img :src="FooterSection.Powered_By_Logo_92x33px" alt="logo"></a>
    </div>
</div>
</section> 
    </div>
    </div>
    `,
    data() {
        return {
            TopSection: {},
            FooterSection: {},
            isLoading: false,
            fullPage: true,
        }
    },
    computed: {
        checkMobileOrNot: function () {
           
            if( screen.width <= 1020 ) {
                return true;
            }
            else {
                return false;
            }
        },
    },
    methods: {
        getpagecontent: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/01 Master/01 Master/01 Master.ftl';
                axios.get(pageurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    if (response.data.area_List.length) {
                        var headerSection = pluck('Header_Section', response.data.area_List);
                        self.TopSection = getAllMapData(headerSection[0].component);

                        var FooterContent = pluck('Footer_Section', response.data.area_List);
                        self.FooterSection = getAllMapData(FooterContent[0].component);

                        self.isLoading = false;

                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });

        }
    },
    mounted: function () {
        this.getpagecontent();
        document.onreadystatechange = () => {
            if (document.readyState == "complete") {
                if (localStorage.direction == 'rtl') {
                    $(".ar_direction").addClass("ar_direction1");
                } else {
                    $(".ar_direction").removeClass("ar_direction1");
                }
            }
        }
    }

})

var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },
});
Vue.component('partneritem', {
    template: `
    <div id="partner">
    <section class="bg-pattern">
    <div class="container">
        <div class="our-partner partner-in">
        <h2> {{PartnersSection.Our_Partners_Label}}</h2>
         <div id="owl-demo-3" class="owl-carousel partner-logo owl-theme">
            <div class="item" v-for="Image in PartnersSection.Slider_Image_List">
                <img :src="Image.Image_150x68px" alt="logo">
            </div>
          </div>
        </div>
    </div>
        </section>
    </div>
    `,
    data() {
        return {
            PartnersSection: {},
            isLoading: false,
            fullPage: true,
        }
    },
    methods: {
        getpagecontent: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/07 Partner/07 Partner/07 Partner.ftl';
                axios.get(pageurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    if (response.data.area_List.length) {
                        var headerSection = pluck('Partners_Section', response.data.area_List);
                        self.PartnersSection = getAllMapData(headerSection[0].component);
                        setTimeout(() => {
                            this.carousel();
                        }, 200);
                        self.isLoading = false;
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });

        }
    },
    mounted: function () {
        this.getpagecontent();
    },

})

var partnerinstance = new Vue({
    el: '#partner',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },
});

function carousel() {
    $("#owl-demo-3").owlCarousel({
        autoplay: true,
        autoPlay: 8000,
        autoplayHoverPause: true,
        stopOnHover: false,
        items: 5,
        margin: 10,
        lazyLoad: true,
        navigation: true,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [768, 2],
    });

    $(".owl-prev").html('<i class="fa  fa-arrow-left"></i>');
    $(".owl-next").html('<i class="fa  fa-arrow-right"></i>');

}
var vsid = "kc235703159f039";

$(function() {
    (function() {
    var vsjs = document.createElement('script'); vsjs.type = 'text/javascript'; vsjs.async = true; vsjs.setAttribute('defer', 'defer');
     vsjs.src = 'https://www.leadchatbot.com/vsa/chat.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(vsjs, s);
    })()
});