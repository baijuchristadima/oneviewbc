const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
Vue.component('vue-multiselect', window.VueMultiselect.default);

var flightserchfromComponent = Vue.component('flightsearch', {
    data() {
        return {
            access_token: '',
            KeywordSearch: '',
            resultItemsarr: [],
            autoCompleteProgress: false,
            highlightIndex: 0
        }
    },
    props: {
        itemText: String,
        itemId: String,
        placeHolderText: String,
        returnValue: Boolean,
        id: {
            type: String,
            default: '',
            required: false
        },
        leg: Number,
        //KeywordSearch:String
    },

    template: `<div class="autocomplete">
        <input type="text" :placeholder="placeHolderText" :id="id" :data-aircode="KeywordSearch" autocomplete="off" 
        v-model="KeywordSearch" class="form-control" 
        :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !autoCompleteProgress  }" 
        @input="onSelectedAutoCompleteEvent(KeywordSearch)"
            @keydown.down="down"
            @keydown.up="up"           
            @keydown.esc="autoCompleteProgress=false"
            @keydown.enter="onSelected(resultItemsarr[highlightIndex])"
            @keydown.tab="tabclick(resultItemsarr[highlightIndex])"/>
        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>        
        <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length>0">
            <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item)">
                {{ item.label }}
            </li>
        </ul>
    </div>`,


    methods: {
        up: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex < this.resultItemsarr.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
            if (this.$refs.options[this.highlightIndex]) {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            }

        },
        onSelectedAutoCompleteEvent: _.debounce(async function (keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
                this.autoCompleteProgress = true;
                self.resultItemsarr = await getFlightResultUsingElasticSearch(keywordEntered);


            } else {
                this.autoCompleteProgress = false;
                this.resultItemsarr = [];

            }
        }, 100),
        onSelected: function (item) {
            this.autoCompleteProgress = false
            this.KeywordSearch = item.label;
            this.resultItemsarr = [];
            var targetWhenClicked = $(event.target).parent().parent().find("input").attr("id");
            if (maininstance.triptype != 'M') {
                if (event.target.id == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
                    $('#Cityto1').focus();
                } else if (event.target.id == "Cityto1" || targetWhenClicked == "Cityto1") {
                    $('#deptDate01').focus();
                }
            } else {
                var eventTarget = event.target.id;
                $(document).ready(function () {
                    if (eventTarget == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
                        $('#Cityto1').focus();
                    } else if (eventTarget == "Cityto1" || targetWhenClicked == "Cityto1") {
                        $('#deptDate01').focus();
                    } else if (eventTarget == "DeparturefromLeg1" || targetWhenClicked == "DeparturefromLeg1") {
                        $('#ArrivalfromLeg1').focus();
                    } else if (eventTarget == "ArrivalfromLeg1" || targetWhenClicked == "ArrivalfromLeg1") {
                        $('#txtLeg1Date').focus();
                    } else if (eventTarget == "DeparturefromLeg2" || targetWhenClicked == "DeparturefromLeg2") {
                        $('#ArrivalfromLeg2').focus();
                    } else if (eventTarget == "ArrivalfromLeg2" || targetWhenClicked == "ArrivalfromLeg2") {
                        $('#txtLeg2Date').focus();
                    } else if (eventTarget == "DeparturefromLeg3" || targetWhenClicked == "DeparturefromLeg3") {
                        $('#ArrivalfromLeg3').focus();
                    } else if (eventTarget == "ArrivalfromLeg3" || targetWhenClicked == "ArrivalfromLeg3") {
                        $('#txtLeg3Date').focus();
                    } else if (eventTarget == "DeparturefromLeg4" || targetWhenClicked == "DeparturefromLeg4") {
                        $('#ArrivalfromLeg4').focus();
                    } else if (eventTarget == "ArrivalfromLeg4" || targetWhenClicked == "ArrivalfromLeg4") {
                        $('#txtLeg4Date').focus();
                    } else if (eventTarget == "DeparturefromLeg5" || targetWhenClicked == "DeparturefromLeg5") {
                        $('#ArrivalfromLeg5').focus();
                    } else if (eventTarget == "ArrivalfromLeg5" || targetWhenClicked == "ArrivalfromLeg5") {
                        $('#txtLeg5Date').focus();
                    } else if (eventTarget == "DeparturefromLeg6" || targetWhenClicked == "DeparturefromLeg6") {
                        $('#ArrivalfromLeg6').focus();
                    } else if (eventTarget == "ArrivalfromLeg6" || targetWhenClicked == "ArrivalfromLeg6") {
                        $('#txtLeg6Date').focus();
                    }
                });

            }

            this.$emit('air-search-completed', item.code, item.label, this.leg);


        },
        tabclick: function (item) {
            if (!item) {

            } else {
                this.onSelected(item);
            }
        }

    },
    watch: {
        returnValue: function () {
            this.KeywordSearch = this.itemText;
        }
    }

});
Vue.component('loading', VueLoading)
var maininstance = new Vue({
    i18n,
    el: '#indexmain',
    name: 'indexmain',
    data: {
        triptype: "O",
        Flight_form_caption: "",
        Oneway: "",
        Round_trip: "",
        Multicity: "",
        Advance_search_label: "",
        preferred_airline_placeholder: "",
        Direct_flight_option_label: "",
        Flight_tab_name: "",
        Hotel_tab_name: "",
        Package_tab_name: "",
        CityFrom: '',
        CityTo: '',
        validationMessage: '',
        adtcount: 1,
        chdcount: 0,
        infcount: 0,
        preferAirline: '',
        returnValue: true,
        legcount: 2,
        legs: [],
        cityList: [],
        adt: "adult",
        adults: [

            {
                'value': 1,
                'text': '1 Adult'
            },
            {
                'value': 2,
                'text': '2 Adults'
            },
            {
                'value': 3,
                'text': '3 Adults'
            },
            {
                'value': 4,
                'text': '4 Adults'
            },
            {
                'value': 5,
                'text': '5 Adults'
            },
            {
                'value': 6,
                'text': '6 Adults'
            },
            {
                'value': 7,
                'text': '7 Adults'
            },
            {
                'value': 8,
                'text': '8 Adults'
            },
            {
                'value': 9,
                'text': '9 Adults'
            }
        ],
        children: [{
                'value': 0,
                'text': 'Children (2-11 yrs)'
            },
            {
                'value': 1,
                'text': '1 Child'
            },
            {
                'value': 2,
                'text': '2 Children'
            },
            {
                'value': 3,
                'text': '3 Children'
            },
            {
                'value': 4,
                'text': '4 Children'
            },
            {
                'value': 5,
                'text': '5 Children'
            },
            {
                'value': 6,
                'text': '6 Children'
            },
            {
                'value': 7,
                'text': '7 Children'
            },
            {
                'value': 8,
                'text': '8 Children'
            }

        ],
        infants: [{
                'value': 0,
                'text': 'Infants (0-1 yr)'
            },
            {
                'value': 1,
                'text': '1 Infant'
            }

        ],
        cabinclass: [{
                'value': 'Y',
                'text': 'Economy'
            },
            {
                'value': 'C',
                'text': 'Business'
            },
            {
                'value': 'F',
                'text': 'First'
            }
        ],
        selected_cabin: 'Y',
        selected_adult: 1,
        selected_child: 0,
        selected_infant: 0,
        totalAllowdPax: 9,
        arabic_dropdown: '',
        childLabel: "",
        childrenLabel: "",
        adultLabel: "",
        adultsLabel: "",
        ageLabel: "",
        agesLabel: "",
        infantLabel: "",
        searchBtnLabel: "",
        addUptoLabel: "",
        tripsLabel: "",
        tripLabel: "",
        Totaltravaller: '',
        travellerdisply: false,
        child: 0,
        flightSearchCityName: {
            cityFrom1: '',
            cityTo1: '',
            cityFrom2: '',
            cityTo2: '',
            cityFrom3: '',
            cityTo3: '',
            cityFrom4: '',
            cityTo4: '',
            cityFrom5: '',
            cityTo5: '',
            cityFrom6: '',
            cityTo6: ''
        },
        advncedsearch: false,
        isLoading: false,
        direct_flight: false,
        airlineList: AirlinesDatas,
        Airlineresults: [],
        selectedAirline: [],
        adultrange: '',
        childrange: '',
        infantrange: '',
        donelabel: '',
        classlabel: '',
        bnrcaption: false,
        hotelInit: Math.random(), //hotel init
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        banner: {},
        HomeMain: {},
        aboutDetails: {},
        newsEvent:[]

    },
    filters: {
        aboutStr: function (string) {
            if (string == null) {
                return "";
            }
            string = string.replace(/<\/?[^>]+(>|$)/g, "");
            if (string.length > 100)
                return string.substr(0, 350) + '...';
            else
                return string;
        },
        newsStr: function (string) {
            if (string == null) {
                return "";
            }
            string = string.replace(/<\/?[^>]+(>|$)/g, "");
            if (string.length > 100)
                return string.substr(0, 111) + '...';
            else
                return string;
        }
    },
    methods: {
        pageContent: function () {
            var self = this;
            self.banner.Class_List=[];
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homePageURL = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/02 Home/02 Home/02 Home.ftl';
                var aboutUsurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/03 About Us/03 About Us/03 About Us.ftl';
                axios.get(homePageURL, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    if (response.data.area_List.length) {
                        var bannerDetails = pluck('Banner_Section', response.data.area_List);
                        self.banner = getAllMapData(bannerDetails[0].component);

                        var Main = pluck('Home_Main_Area', response.data.area_List);
                        self.HomeMain = getAllMapData(Main[0].component);

                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.stopLoader();
                });

                axios.get(aboutUsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    if (response.data.area_List.length) {
                        var about = pluck('Main_Area', response.data.area_List);
                        self.aboutDetails = getAllMapData(about[0].component);
                    }
                    self.stopLoader();
                }).catch(function (error) {
                    console.log('Error');
                    self.stopLoader();
                });

            });
        },
        Departurefrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (AirportCode == this.CityTo) {
                    this.returnValue = false;
                    alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M11')).set('closable', false);
                    this.CityFrom = '';

                } else {
                    this.returnValue = true;
                    this.CityFrom = AirportCode;
                }
            } else {
                var from = 'cityFrom' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].to) {
                        this.returnValue = false;
                        this.flightSearchCityName[from] = "";
                        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M11')).set('closable', false);
                    } else {
                        this.cityList[index].from = AirportCode
                        this.flightSearchCityName[from] = AirportName;
                        this.returnValue = true;
                    }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: AirportCode,
                        to: ''

                    });
                    this.flightSearchCityName[from] = AirportName;
                    this.returnValue = true;
                }
            }


        },
        Arrivalfrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (this.CityFrom == AirportCode) {
                    this.returnValue = false;
                    alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M11')).set('closable', false);
                    this.validationMessage = "";
                    this.CityTo = '';

                } else {
                    this.returnValue = true;
                    this.CityTo = AirportCode;
                }
            } else {
                var to = 'cityTo' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].from) {
                        this.returnValue = false;
                        this.flightSearchCityName[to] = "";
                        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M11')).set('closable', false);

                    } else {
                        this.cityList[index].to = AirportCode;
                        this.flightSearchCityName[to] = AirportName;
                        this.returnValue = true;
                    }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: '',
                        to: AirportCode

                    });
                    this.flightSearchCityName[to] = AirportName;
                    this.returnValue = true;
                }
            }
        },
        SearchFlight: function () {
            if (this.triptype == 'R') {
                var Departuredate = $('#from1').val() == "" ? "" : $('#from1').datepicker('getDate');
            } else {
                var Departuredate = $('#deptDate01').val() == "" ? "" : $('#deptDate01').datepicker('getDate');
            }

            if (!this.CityFrom) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M14')).set('closable', false);
                return false;
            }
            if (!this.CityTo) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M08')).set('closable', false);
                return false;
            }
            if (!Departuredate) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M09')).set('closable', false);
                return false;
            }
            var sec1TravelDate = moment(Departuredate).format('DD|MM|YYYY');

            var sectors = this.CityFrom + '-' + this.CityTo + '-' + sec1TravelDate;
            if (this.triptype == 'R') {
                var ArrivalDate = $('#retDate').val() == "" ? "" : $('#retDate').datepicker('getDate');
                if (!isNullorUndefined(ArrivalDate)) {
                    ArrivalDate = moment(ArrivalDate).format('DD|MM|YYYY');
                    if (!ArrivalDate) {
                        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M10')).set('closable', false);
                        return false;
                    } else {
                        sectors += '/' + this.CityTo + '-' + this.CityFrom + '-' + ArrivalDate;
                    }
                } else {
                    alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M10')).set('closable', false);
                    return false;
                }
            }
            var directFlight = this.direct_flight ? 'DF' : 'AF';
            var adult = this.selected_adult;
            var child = this.selected_child;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            getSuppliers([this.CityFrom + '|' + this.CityTo],
            function(supp) {
                var searchUrl = '/Flights/flight-listing.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                // searchUrl = searchUrl.toLocaleLowerCase();
                window.location.href = searchUrl;
            })
        },
        AddNewLeg() {
            if (this.legcount <= 5) {
                ++this.legcount;
                var legno = 1;
                if (this.cityList.length > 0) {
                    legno = Math.max.apply(Math, this.cityList.map(function (o) {
                        return o.id;
                    }));
                    legno = legno + 1;
                }

                this.cityList.push({
                    id: legno,
                    from: '',
                    to: ''
                });

            }
            console.log(this.cityList);
        },
        DeleteLeg(leg) {
            var legs = this.legcount;
            if (legs > 1) {
                --this.legcount;
                var legno = Math.max.apply(Math, this.cityList.map(function (o) {
                    return o.id;
                }));
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    this.cityList.splice(index, 1);

                }


            }
        },
        setCalender() {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = 1; //generalInformation.systemSettings.calendarDisplay;
            $("#deptDate01").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                onSelect: function (selectedDate) {
                    $("#retDate").datepicker("option", "minDate", selectedDate);

                }
            });
            $("#from1").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#retDate").datepicker("option", "minDate", selectedDate);

                }
            });

            $("#retDate").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#from1").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {}
            });
            $("#txtLeg1Date").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);

                }
            });
            $("#txtLeg2Date").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg1Date").val();
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg3Date").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg2Date").val();
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg4Date").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg3Date").val();
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg5Date").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg4Date").val();
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg6Date").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg5Date").val();
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {}
            });


        },
        onAdultChange: function () {
            alert(this.selected);
            //this.disp = this.selected;
        },
        onCheck: function (e) {
            var currid = $(e.target).attr('id');

            console.log(currid);
            var selectedDate = $("#deptDate01").val();
            $("#retDate").datepicker("option", "minDate", selectedDate);
            $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/EuroAfrica/holiday-detail.html?page=" + url;
                    console.log(url);
                    window.location.href = url;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;

        },
        stopLoader: function () {
            $('#preloader').delay(50).fadeOut(250);
        },
        swapLocations: function (id) {
            if ((this.CityFrom) && (this.CityTo)) {
                var from = this.CityFrom;
                var to = this.CityTo;
                this.CityFrom = to;
                this.CityTo = from;
                swpaloc(id)
            }
        },
        setTripType: function (triptype) {
            if (triptype == "R") {
                this.triptype = "R"
                this.CityTo = "";
                this.CityFrom = "";
                $("#deptDate01").val('');
                $("#retDate").val('');
                $('#radultdrp').val($('#radultdrp option:first-child').val()).trigger('change');
                $('#rchilddrp').val($('#rchilddrp option:first-child').val()).trigger('change');
                $('#rinfantdrp').val($('#rinfantdrp option:first-child').val()).trigger('change');
            }
            if (triptype == "O") {
                this.triptype = "O"
                this.CityTo = "";
                this.CityFrom = "";
                $("#deptDate01").val('');
                $("#retDate").val('');
                $('#adultdrp').val($('#adultdrp option:first-child').val()).trigger('change');
                $('#childdrp').val($('#childdrp option:first-child').val()).trigger('change');
                $('#infantdrp').val($('#infantdrp option:first-child').val()).trigger('change');
            }
            if (triptype == "M") {
                this.triptype = "M"
                this.CityTo = "";
                this.CityFrom = "";
                $("#deptDate01").val('');
                $("#retDate").val('');
                $('#madultdrp').val($('#madultdrp option:first-child').val()).trigger('change');
                $('#mchilddrp').val($('#mchilddrp option:first-child').val()).trigger('change');
                $('#minfantdrp').val($('#minfantdrp option:first-child').val()).trigger('change');
            }
        },
        MultiSearchFlight: function () {
            var sectors = '';
            var legDetails = [];
            for (var legValue = 1; legValue <= this.legcount; legValue++) {
                var temDeparturedate = $('#txtLeg' + (legValue) + 'Date').val() == "" ? "" : $('#txtLeg' + (legValue) + 'Date').datepicker('getDate');
                if (temDeparturedate != "" && this.cityList.length != 0 && this.cityList[legValue - 1].from != "" && this.cityList[legValue - 1].to != "") {
                    var departureFrom = this.cityList[legValue - 1].from;
                    var arrivalTo = this.cityList[legValue - 1].to;
                    var travelDate = moment(temDeparturedate).format('DD|MM|YYYY');
                    sectors += '/' + departureFrom + '-' + arrivalTo + '-' + travelDate;
                    legDetails.push(departureFrom + '|' + arrivalTo)
                } else {
                    alertify.alert('Alert', 'Please fill the Trip ' + (legValue) + '   fields !').set('closable', false);


                    return false;
                }
            }
            var directFlight = this.direct_flight ? 'DF' : 'AF';

            var adult = this.selected_adult;
            var child = this.selected_child;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            getSuppliers(legDetails,
            function(supp) {
                var searchUrl = '/Flights/flight-listing.html?flight=' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                // searchUrl = searchUrl.toLocaleLowerCase();
                window.location.href = searchUrl;
            })
        },
        limitText(count) {
            return `and ${count} other Airlines`
        },
        asyncFind(query) {
            if (query.length > 2) {
                this.isLoading = true;

                var newData = [];
                this.airlineList.filter(function (el) {
                    if (el.A.toLowerCase().indexOf(query.toLowerCase()) >= 0) {
                        newData.push({
                            "code": el.C,
                            "label": el.A
                        });
                    }
                });
                if (newData.length > 0) {
                    this.Airlineresults = newData;
                    this.isLoading = false;
                } else {
                    this.Airlineresults = [];
                    this.isLoading = false;
                }
            }


        },
        clearAll() {
            this.selectedAirline = []
        },
        getValidationMsgByCode: function (code) {
            if (sessionStorage.validationItems !== undefined) {
                var validationList = JSON.parse(sessionStorage.validationItems);
                for (let validationItem of validationList.Validation_List) {
                    if (code === validationItem.Code) {
                        return validationItem.Message;
                    }
                }
            }
        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                if (type == 'Adult') {
                    this.selected_adult = event.target.value;
                } else if (type == 'Infant') {
                    this.selected_infant = event.target.value;
                } else if (type == 'Child') {
                    this.selected_child = event.target.value;
                } else if (type == 'Cabin') {
                    this.selected_cabin = event.target.value;
                }
            }
        },
        getNewsEvent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var newsUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/News List/News List/News List.ftl';
                axios.get(newsUrl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    var newsData = response.data;
                    let newsDataListTemp = [];
                    if (newsData != undefined && newsData.Values != undefined) {
                        newsDataListTemp = newsData.Values;
                    }
                    var newsTemp = newsDataListTemp.slice(Math.max(newsDataListTemp.length - 3, 0));
                    self.newsEvent = newsTemp.sort().reverse();
                    setTimeout(() => {
                        this.newsCarousel();
                      }, 100);
                }).catch(function (error) {
                    console.log('Error');
                });
            });

        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/KhartoumTKT/news-detail.html?page=" + url;
                    console.log(url);
                    window.location.href = url;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;

        },
        stopLoader: function () {
            $('#preloader').delay(50).fadeOut(250);
        },
        datenumberFormatter: function (utc) {
            return (moment(utc).utcOffset("+05:30").format('DD'));
        },
        datemonthFormatter: function (utc) {
            return (moment(utc).utcOffset("+05:30").format('ll').slice(0, 3));
        },
    },
    mounted: function () {
        this.setCalender();
        this.pageContent();
        this.getNewsEvent();
        var vm = this;
        this.$nextTick(function () {
            $('#adultdrp').on("change", function (e) {
                vm.dropdownChange(e, 'Adult')
            });
            $('#infantdrp').on("change", function (e) {
                vm.dropdownChange(e, 'Infant')
            });
            $('#childdrp').on("change", function (e) {
                vm.dropdownChange(e, 'Child')
            });
            $('#cabindrp').on("change", function (e) {
                vm.dropdownChange(e, 'Cabin')
            });

            $('#radultdrp').on("change", function (e) {
                vm.dropdownChange(e, 'Adult')
            });
            $('#rinfantdrp').on("change", function (e) {
                vm.dropdownChange(e, 'Infant')
            });
            $('#rchilddrp').on("change", function (e) {
                vm.dropdownChange(e, 'Child')
            });
            $('#rcabindrp').on("change", function (e) {
                vm.dropdownChange(e, 'Cabin')
            });

            $('#madultdrp').on("change", function (e) {
                vm.dropdownChange(e, 'Adult')
            });
            $('#minfantdrp').on("change", function (e) {
                vm.dropdownChange(e, 'Infant')
            });
            $('#mchilddrp').on("change", function (e) {
                vm.dropdownChange(e, 'Child')
            });
            $('#mcabindrp').on("change", function (e) {
                vm.dropdownChange(e, 'Cabin')
            });
        })
    },
    updated: function () {
        this.setCalender();
    },
    watch: {

        selectedAirline: function () {
            if (this.selectedAirline.length > 4) {
                alert("exceed");
                return false;
            } else {
                var airlines = [];
                if (this.selectedAirline.length > 0) {
                    this.selectedAirline.forEach(function (airline) {
                        airlines.push(airline.code);
                    });
                }

                return this.preferAirline = airlines.length > 0 ? airlines.join('|') : '';
            }
        }
    }
});







function isNullorUndefined(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) {
        status = true;
    }
    return status;
}

function onPaxChange(e) {
    var currentadult = parseInt($('#adultdrp option:selected').val());
    var currentchild = parseInt($('#childdrp option:selected').val());
    var currentinfant = parseInt($('#infantdrp option:selected').val());
    var currenttotal = parseInt(currentadult) + parseInt(currentchild);
    var child = 9 - currentadult;
    maininstance.children = [{
        'value': 0,
        'text': 'Children (2-11 yrs)'
    }];
    for (i = 1; i <= child; i++) {
        maininstance.children.push({
            'value': i,
            'text': i + ' Child' + (i > 1 ? 'ren' : '')
        });
    }
    if (currentchild > child) {
        currentchild = child;
    }
    currentchild = (currentchild < 0) ? 0 : currentchild;
    maininstance.selected_child = currentchild;
    var infant = (((currenttotal > 2 && currenttotal < 7) && (currentadult > 3)) ? 3 : ((currenttotal > 6) ? (9 - currenttotal) : currentadult));
    maininstance.infants = [{
        'value': 0,
        'text': 'Infants (0-1 yr)'
    }];
    for (i = 1; i <= infant; i++) {
        maininstance.infants.push({
            'value': i,
            'text': i + ' Infant' + (i > 1 ? 's' : '')
        });
    }
    if (currentinfant > infant) {
        currentinfant = infant;
    }
    currentinfant = (currentinfant < 0) ? 0 : currentinfant;
    maininstance.selected_infant = currentinfant;
}

function onPaxChangeRound(e) {
    var currentadult = parseInt($('#radultdrp option:selected').val());
    var currentchild = parseInt($('#rchilddrp option:selected').val());
    var currentinfant = parseInt($('#rinfantdrp option:selected').val());
    var currenttotal = parseInt(currentadult) + parseInt(currentchild);
    var child = 9 - currentadult;
    maininstance.children = [{
        'value': 0,
        'text': 'Children (2-11 yrs)'
    }];
    for (i = 1; i <= child; i++) {
        maininstance.children.push({
            'value': i,
            'text': i + ' Child' + (i > 1 ? 'ren' : '')
        });
    }
    if (currentchild > child) {
        currentchild = child;
    }
    currentchild = (currentchild < 0) ? 0 : currentchild;
    maininstance.selected_child = currentchild;
    var infant = (((currenttotal > 2 && currenttotal < 7) && (currentadult > 3)) ? 3 : ((currenttotal > 6) ? (9 - currenttotal) : currentadult));
    maininstance.infants = [{
        'value': 0,
        'text': 'Infants (0-1 yr)'
    }];
    for (i = 1; i <= infant; i++) {
        maininstance.infants.push({
            'value': i,
            'text': i + ' Infant' + (i > 1 ? 's' : '')
        });
    }
    if (currentinfant > infant) {
        currentinfant = infant;
    }
    currentinfant = (currentinfant < 0) ? 0 : currentinfant;
    maininstance.selected_infant = currentinfant;
}

function onPaxChangeMulticity(e) {
    var currentadult = parseInt($('#madultdrp option:selected').val());
    var currentchild = parseInt($('#mchilddrp option:selected').val());
    var currentinfant = parseInt($('#minfantdrp option:selected').val());
    var currenttotal = parseInt(currentadult) + parseInt(currentchild);
    var child = 9 - currentadult;
    maininstance.children = [{
        'value': 0,
        'text': 'Children (2-11 yrs)'
    }];
    for (i = 1; i <= child; i++) {
        maininstance.children.push({
            'value': i,
            'text': i + ' Child' + (i > 1 ? 'ren' : '')
        });
    }
    if (currentchild > child) {
        currentchild = child;
    }
    currentchild = (currentchild < 0) ? 0 : currentchild;
    maininstance.selected_child = currentchild;
    var infant = (((currenttotal > 2 && currenttotal < 7) && (currentadult > 3)) ? 3 : ((currenttotal > 6) ? (9 - currenttotal) : currentadult));
    maininstance.infants = [{
        'value': 0,
        'text': 'Infants (0-1 yr)'
    }];
    for (i = 1; i <= infant; i++) {
        maininstance.infants.push({
            'value': i,
            'text': i + ' Infant' + (i > 1 ? 's' : '')
        });
    }
    if (currentinfant > infant) {
        currentinfant = infant;
    }
    currentinfant = (currentinfant < 0) ? 0 : currentinfant;
    maininstance.selected_infant = currentinfant;
}




function swpaloc(id) {
    var from = $("#Cityfrom" + id).val();
    var to = $("#Cityto" + id).val();
    $("#Cityfrom" + id).val(to);
    $("#Cityto" + id).val(from);
}

function newsCarousel(){
    $("#owl-demo-2").owlCarousel({
        autoplay:true,
        autoPlay : 8000,
        autoplayHoverPause:true, 
        stopOnHover : false,  
        items : 1,
        margin:10,  
        lazyLoad : true,
        navigation : true,
        itemsDesktop : [1199, 1],
        itemsDesktopSmall : [979, 1],
        itemsTablet : [768, 1],
          itemsMobile : [500, 1],
      });
         
         $( ".owl-prev").html('<i class="fa  fa-chevron-left"></i>');
         $( ".owl-next").html('<i class="fa fa-chevron-right"></i>');
}