Vue.component('loading', VueLoading)
var termsPage = new Vue({
    el:'#terms',
    name:'terms',
    data: {
        bannerSection:{},
       termsSection:{},
        isLoading: false,
        fullPage: true,
    },
    methods: {
       getData: function() {
            var self = this;
            self.isLoading = true;
            getAgencycode(function(response) {
                var Agencycode = response;
                console.log(Agencycode);
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var termsurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/05 Terms And Conditions/05 Terms And Conditions/05 Terms And Conditions.ftl';
                axios.get(termsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                            var bannerDetail = pluck('Banner_Section', response.data.area_List);
                            self.bannerSection = getAllMapData(bannerDetail[0].component);
                            var termscond = pluck('Main_Section', response.data.area_List);
                            self.termsSection = getAllMapData(termscond[0].component);
                            self.isLoading = false;
                            console.log(termscond);
                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });

        }
    },
    mounted: function() {  
        this.getData();
    }
});

