Vue.component('loading', VueLoading)
var contact = new Vue({
    el: '#contact',
    name: 'contact',
    data: {
        BannerSection: {},
        ContactSection: {},
        MapSection: {},
        isLoading: false,
        fullPage: true,
        cntusername: '',
        cntemail: '',
        cntcontact: '',
        cntmessage: '',
        cntsubject:''
    },
    methods: {
        getContactData: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/06 Contact Us/06 Contact Us/06 Contact Us.ftl';
                axios.get(aboutUsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    if (response.data.area_List.length > 0) {
                        var bannerDetail = pluck('Banner_Section', response.data.area_List);
                        self.BannerSection = getAllMapData(bannerDetail[0].component);
                        var mainsection = pluck('Main_Section', response.data.area_List);
                        self.ContactSection =getAllMapData(mainsection[0].component);
                        var maparea = pluck('Map_Section', response.data.area_List);
                        self.MapSection = getAllMapData(maparea[0].component);
                        self.isLoading = false;
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });

        },
        sendcontactus: async function () {
            if (!this.cntusername.trim()) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M01')).set('closable', false);

                return false;
            }
            if (!this.cntemail.trim()) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M02')).set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M03')).set('closable', false);
                return false;
            }
            if (!this.cntcontact.trim()) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M19')).set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M20')).set('closable', false);
                return false;
            }
            if (!this.cntsubject.trim()) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M21')).set('closable', false);
                return false;
            }

            if (!this.cntmessage.trim()) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M22')).set('closable', false);
                return false;
            } else {
                var self =this;
                self.isLoading = true;
                var toEmail = JSON.parse(localStorage.User).loginNode.email;
                var frommail = JSON.parse(localStorage.User).loginNode.email;
                // var frommail = "itsolutionsoneview@gmail.com";
                var postData = {
                    type: "AdminContactUsRequest",
                    fromEmail:frommail,
                    toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl +JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.cntusername,
                    emailId: this.cntemail,
                    contact: this.cntcontact,
                    message: this.cntmessage,

                };
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.cntusername,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };

                let agencyCode = localStorage.AgencyCode;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Contact Us",
                    keyword1: this.cntusername,
                    keyword2: this.cntemail,
                    keyword4: this.cntcontact,
                    keyword3: this.cntsubject,
                    text1: this.cntmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, postData);
                    sendMailService(emailApi, custmail);
                    this.cntemail = '';
                    this.cntusername = '';
                    this.cntcontact = '';
                    this.cntmessage = '';
                    this.cntsubject = '';
                    self.isLoading = false;
                    alertify.alert(this.getValidationMsgByCode('M15'), this.getValidationMsgByCode('M23'));
                   
                } catch (e) {
                    self.isLoading = false;
                }



            }
        },
        getValidationMsgByCode: function (code) {
            if (sessionStorage.validationItems !== undefined) {
              var validationList = JSON.parse(sessionStorage.validationItems);
              for (let validationItem of validationList.Validation_List) {
                if (code === validationItem.Code) {
                  return validationItem.Message;
                }
              }
            }
          }
    },
    mounted: function () {
        this.getContactData();
    }
});

