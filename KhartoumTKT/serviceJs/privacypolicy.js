Vue.component('loading', VueLoading)
var privacyPage = new Vue({
    el:'#policy',
    name:'policy',
    data: {
        bannerSection:{},
      policySection:{},
        isLoading: false,
        fullPage: true,
    },
    methods: {
       getData: function() {
            var self = this;
            self.isLoading = true;
            getAgencycode(function(response) {
                var Agencycode = response;
                console.log(Agencycode);
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Privacy Policy Page/Privacy Policy Page/Privacy Policy Page.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                            var bannerDetail = pluck('Banner_Area', response.data.area_List);
                            self.bannerSection = getAllMapData(bannerDetail[0].component);
                            var privacy = pluck('Main_Area', response.data.area_List);
                            self.policySection = getAllMapData(privacy[0].component);
                            self.isLoading = false;                    
                    }
                }).catch(function(error) {
                    console.log(error);
                    self.isLoading = false;
                });
            });

        }
    },
    mounted: function() {  
        this.getData();
    }
});

