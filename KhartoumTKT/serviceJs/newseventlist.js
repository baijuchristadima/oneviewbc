Vue.component('loading', VueLoading)
var newsPage = new Vue({
    el: '#newsevents',
    name:'newsevents',
    data: {
        bannerSection: {},
        newsDetails:[],
        newsSection: {},
        newsList:[],
        totalNews:'',
        currentPage: 1,
        currentPages: 1,
        pageLimit:6,
        fromPage: 1,
        totalpage: 1,
        totalNews:'' ,
        fromPage: 1,
        paginationLimit:1,
        constructedNumbers:[],
        isLoading: false,
        fullPage: true,
    },
    methods: {
        getData: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                console.log(Agencycode);
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var newssurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/08 New And Event Detail/08 New And Event Detail/08 New And Event Detail.ftl';
                axios.get(newssurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    if (response.data.area_List.length > 0) {
                        var bannerDetail = pluck('Banner_Section', response.data.area_List);
                        self.bannerSection = getAllMapData(bannerDetail[0].component);
                        var newsData = pluck('Main_Section', response.data.area_List);
                        self.newsSection = getAllMapData(newsData[0].component);
                       self.isLoading = false;
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });
        },    
        dateConvertMonth: function (utc) {
                return (moment(utc).format("MMM"));
        },
        dateConvertDate: function (utc) {
            return (moment(utc).format(" DD "));
        },
        getNewsDetails: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var newssurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/News List/News List/News List.ftl';
                axios.get(newssurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                        if (response.data.Values.length > 0) {
                          var urlData = window.location.href.split('?');
                           if(urlData[1] !== undefined){
                           var keyData = urlData[1].split('=');
                           var typeData = keyData[0];
                           var  Item = decodeURIComponent(keyData[1]);
                            console.log(Item);
                           for (const iterator of response.data.Values) {
                            if(typeData == 'category' && iterator.Category_Name !== undefined && iterator.Category_Name !== null) {
                              if(Item.toUpperCase() == iterator.Category_Name.toUpperCase()) {
                                self.newsList.push(iterator);
                                self.newsList.reverse();
                              }
                             } else if(typeData =='tag' && iterator.Tag_List !== undefined && iterator.Tag_List !== null) {
                               for (const tagD of iterator.Tag_List) {
                                 if(Item.toUpperCase() == tagD.Tag_Name.toUpperCase()) {
                                  self.newsList.push(iterator);
                                  //self.newsList.sort();
                                  self.newsList.reverse();
                                 }
                               }
                            }
                          }
                        } else {
                          self.newsList=response.data.Values;
                          self.newsList.reverse();
                        }

                        self.totalNews = Number(self.newsList.length);
                        self.totalpage = Math.ceil(self.totalNews/self.pageLimit);
                        self.currentPage = 1;
                        self.fromPage = 1;
                        if (Number(self.totalBlog) < 6) {
                         
                        }
                        self.constructAllPagianationLink();
                        self.isLoading = false;
                          }
                         }).catch(function(error) {
                         console.log(error);
                         self.isLoading = false;
                 });
             });
         },
        constructAllPagianationLink: function () {
            let limit = this.paginationLimit;
            this.constructedNumbers = [];
            for (let i = Number(this.fromPage); i <= (Number(this.totalpage) + limit); i++) {
              if (i <= Number(this.totalpage)) {
                this.constructedNumbers.push(i);
              }
            }
            this.currentPage = this.constructedNumbers;
            this.setNewsItem();
          },
          prevNextEvent: function (type) {
            let limit = this.paginationLimit;
            if (type == 'Previous') {
              if (this.currentPages > this.totalpage) {
                this.currentPages = this.currentPages - 1;
              }
              this.fromPage = this.currentPages;
              if (Number(this.fromPage) != 1) {
                // this.fromPage = Number(this.fromPage) - limit;
                this.currentPages = Number(this.currentPages) - 1;
                this.setNewsItem();
              }
                } else if (type == 'Next') {
              if (this.currentPages == 'undefined' || this.currentPages == '') {
              }
              if (this.currentPages <= this.totalpage) {
                let limit = this.paginationLimit;
                this.fromPage = this.currentPages;
                var totalP = (this.totalpage) + 1;
                if (Number(this.fromPage) != totalP) {
                  var count = this.currentPages + limit;
                  if (Number(count) <= Number(this.totalpage)) {
                    this.selectNews(this.currentPages);
                  }
      
                }
              }
            }
          },
          setNewsItem: function () {
                if (this.newsList != undefined ) {
              let start = 0;
              let end = Number(start) +  Number(this.pageLimit);
              if (Number(this.currentPages) == 1) {
              } else {
                var limit = this.totalpage;
                start = (Number(this.currentPages) + Number(this.pageLimit)) - 2;
                end = Number(start) + Number(this.pageLimit);
              }
              this.newsDetails= this.newsList.slice(start, end);
              //console.log(this.newsDetails);
            }
          },  
          selectNews: function (ev) {
            let limit = 1;
            console.log(ev);
            this.currentPages = this.currentPage[ev];
            this.setNewsItem();
          },
           
        getmoreinfo(url) {
                        if (url != null) {
                    if (url != "") {
                        url = url.split("/Template/")[1];
                        url = url.split(' ').join('-');
                        url = url.split('.').slice(0, -1).join('.');
                        url = "/KhartoumTKT/news-detail.html?page=" + url;
                        window.location.href = url;
                                   }
                    else {
                        url = "#";
                    }
                }
                else {
                    url = "#";
                }
                return url;
        } ,
           },
    mounted: function () {
        this.getData();
        this.getNewsDetails();
       
    }
});

