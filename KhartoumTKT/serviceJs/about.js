Vue.component('loading', VueLoading)
var aboutPage = new Vue({
    el:'#about',
    name:'about',
    data: {
        BannerSection:{},
        AboutSection:{},
        isLoading: false,
        fullPage: true,
    },
    methods: {
       getData: function() {
            var self = this;
            self.isLoading = true;
            getAgencycode(function(response) {
                var Agencycode = response;
                console.log(Agencycode);
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var aboutUsurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/03 About Us/03 About Us/03 About Us.ftl';
                axios.get(aboutUsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                            var bannerDetail = pluck('Banner_Section', response.data.area_List);
                            self.BannerSection = getAllMapData(bannerDetail[0].component);
                            var aboutDetail = pluck('Main_Area', response.data.area_List);
                            self.AboutSection = getAllMapData(aboutDetail[0].component);
                        self.isLoading = false;
                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });

        }
    },
    mounted: function() {  
        this.getData();
    }
});

