Vue.component('loading', VueLoading)
var promtionPage = new Vue({
    el: '#promotions',
    name:'promotions',
    data: {
        promotionSection: [],
        bannerSection: {},
        isLoading: false,
        fullPage: true,
    },
    methods: {
        getData: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                console.log(Agencycode);
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var promotionsurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/04 Promotion/04 Promotion/04 Promotion.ftl';
                axios.get(promotionsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    if (response.data.area_List.length > 0) {
                        var bannerDetail = pluck('Banner_Section', response.data.area_List);
                        self.bannerSection = getAllMapData(bannerDetail[0].component);
                        var promotionsData = pluck('Main_Section', response.data.area_List);
                        self.promotionSection = getAllMapData(promotionsData[0].component);
                        self.isLoading = false;

                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });

        },
        dateConvert: function (utc) {
            return (moment(utc).format("DD MMMM YYYY"));
        },
    },
    mounted: function () {
        this.getData();
    }
});

