Vue.component('loading', VueLoading)
var newsPage = new Vue({
    el: '#news',
    name:'news',
    data: {
        categories: [],
        bannerSection: {},
        newsSection: {},
        newsList: [],
        newsDeatilsData:[],
        tagDetails:[],
        latestNews: [],
        cmntLength:'',
        cmntShrink:'',
        name:'',
        pagePath:'',
        tagList:[],
        expand:true,
        categoryList:'',
        shrinkCmnt:true,
        allshow:'',
        email:'',
        comment:'',
        allComment:'',
        isLoading: false,
        fullPage: true,
    },
    methods: {
        getData: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                console.log(Agencycode);
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var newssurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/08 New And Event Detail/08 New And Event Detail/08 New And Event Detail.ftl';
                axios.get(newssurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    if (response.data.area_List.length > 0) {
                        var bannerDetail = pluck('Banner_Section', response.data.area_List);
                        self.bannerSection = getAllMapData(bannerDetail[0].component);
                        var newsData = pluck('Main_Section', response.data.area_List);
                        self.newsSection = getAllMapData(newsData[0].component);
                        self.isLoading = false;

                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });

        },
        postcomment: async function () {
                if (!this.name) {
                alertify.alert(this.getValidationMsgByCode('M07'),this.getValidationMsgByCode('M01')).set('closable', false);

                return false;
            }
            if (!this.email.trim()) {
               alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M02')).set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.email.match(emailPat);
            if (matchArray == null) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M03')).set('closable', false);
                return false;
            }
            if (!this.comment.trim()) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M18')).set('closable', false);
                return false;
            } else {
                var frommail = JSON.parse(localStorage.User).loginNode.email;
                var custmail = {
                    type:"UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.email) ? this.email : [this.email],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                   agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                   // agencyAddress: "Khartoumm",
                    personName: this.name,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                this.isLoading = true;
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
               let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertData = {
                    type: "News And Events Reply",
                    keyword1: this.name,
                    keyword2: this.email,
                    text1: this.comment,
                    date1: requestedDate,
                    keyword4:this.pagePath,
                    nodeCode: agencyCode
                };
                let responseObject = await cmsRequestData("POST", "cms/data", insertData , null);
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi,custmail);
                    this.name = '';
                    this.email = '';
                    this.comment = '';
                    this.viewComments();
                    this.isLoading = false;
                    alertify.alert(this.getValidationMsgByCode('M15'), this.getValidationMsgByCode('M30'));
                   
                } 
                catch (e) {
                    self.isLoading = false;
            }
            }
        },
            getValidationMsgByCode: function (code) {
            if (sessionStorage.validationItems !==undefined) {
              var validationList = JSON.parse(sessionStorage.validationItems);
              for (let validationItem of validationList.Validation_List) {
                if (code === validationItem.Code) {
                  return validationItem.Message;
                }
              }
            }
          },
    
        viewComments: async function () {
            var self = this;
            let allComment = [];
            let agencyCode = JSON.parse(localStorage.User).loginNode.code;
            let requestObject = { from: 0, size: 100, type: "News And Events Reply", nodeCode: agencyCode, orderBy: "desc" };
            let responseObject = await cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) 
            {
                if (responseObject != undefined && responseObject.data != undefined) {
                    allResult = JSON.parse(JSON.stringify(responseObject)).data;
                    for (let i = 0; i < allResult.length; i++) {
                      if (allResult[i].keyword4 == self.pagePath) {
                        let object = {
                            Name: allResult[i].keyword1,
                            Date:allResult[i].date1,
                            comment: allResult[i].text1
                        };
                        allComment.push(object);
                      }
                    }
                    self.allComment = allComment;
                    self.cmntLength=self.allComment.length;
                    //self.cmntShrink=self.allComment.slice(0,5);
                   
                    }
            });
        },
        showAll(){
            
            this.expand=false;
            this.cmntShrink=this.allComment;
                
        },
        collapseAll(){
            this.expand=true;
            this.cmntShrink=this.allComment.slice(0,5);
        
        },
        dateConvert: function (utc) {
            return (moment(utc).format("MMMM  DD,YYYY"));
        },
        dateConvert1: function (utc) {
           return (moment(utc).format("DD  MMM,YYYY"));
        },
        getNewsDetails: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
               // var pagepath = JSON.parse(sessionStorage.page);
               // console.log(pagepath);
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var newssurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/News List/News List/News List.ftl';
                axios.get(newssurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                   // var category;
                    if (response.data.Values.length > 0) {
                        self.newsList = response.data.Values;
                        self.newsList.sort();
                        self.newsList.reverse();
                        //self.latestNews = self.newsList[0];
                        //self.tagDetails=self.newsList.Tag_List;              
                    }
                    //console.log(response); 
                    self.newsList.forEach(element => {
                    self.categories.push(element.Category_Name);
                    //self.tagList.push(element.Tag_List);
                    element.Tag_List.forEach(tag =>{
                    self.tagList.push(tag.Tag_Name);

                    });
                
                       });                        
                       var categoryNames =  self.categories.map(name => name.trim().toUpperCase());
                       var setCategory = new Set(categoryNames);
                       self.categories=Array.from(setCategory);
                        var tagNames =  self.tagList.map(name => name.trim().toUpperCase());
                       var setTag = new Set(tagNames);
                       self.tagList=Array.from(setTag);
                    }).catch(function (error) {
                    console.log('Error');
                    self.isLoading = false;
                    
                });
                
                var newsDetailsUrl = getQueryStringValue('page');
                self.pagePath='B2B/AdminPanel/CMS/'+ Agencycode + '/Template/'+newsDetailsUrl+'.ftl';
                if (newsDetailsUrl != "") {
                    newsDetailsUrl = newsDetailsUrl.split('-').join(' ');
                    newsDetailsUrl = newsDetailsUrl.split('_')[0];
                    var topnewsurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + newsDetailsUrl + '.ftl';
                    axios.get(topnewsurl, {
                        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                    }).then(function (response) {
                        var pagecontent = pluck('Events', response.data.area_List);
                        self.newsDeatilsData = getAllMapData(pagecontent[0].component);
                        //self.pagePath=self.newsDeatilsData.More_details_page_link;
                       // console.log('newsdata', self.pagePath);
                    })
                }
            });

        },
       
        passQuery(type,query) {
            if(type == 'tag') {
                query = "/KhartoumTKT/news-event.html?tag=" + query;
            } else {
                query = "/KhartoumTKT/news-event.html?category=" + query;
            }

          //  console.log(query);
            window.location.href = query; 
        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/KhartoumTKT/news-detail.html?page=" + url;
                    //console.log(url);
                    //window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;
        //     var Agencycode = response;
        //     var huburl = ServiceUrls.hubConnection.cmsUrl;
        //     var portno = ServiceUrls.hubConnection.ipAddress;
        //     var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        //     var newssurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + newsDeatailUrl ;
        //    window.location.href = ;
        //     return newssurl;
        }        
                
        },
        
        mounted: function () {
        this.getData();
        this.getNewsDetails();
        this.viewComments();
    }
    
});
function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));

}
