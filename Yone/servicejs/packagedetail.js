const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
Vue.component('loading', VueLoading)
var packageView = new Vue({
  i18n,
  el: '#holiday-detail',
  name: 'holiday-detail',
  data: {
    mainContent: {},
    bannerSection: {},
    //booking form
    fname: '',
    lname: '',
    femail: '',
    fphone: '',
    fpack: '',
    fdate: '',
    fadult: 0,
    fchild: 0,
    finfants: 0,
    fmessage: '',
    starRating: '',
    //review form
    Name: '',
    Email: '',
    contact: '',
    Review: '',
    overitems: null,
    pageURLLink: '',
    reviewAvailable: false,
    allReview: [],
    avgrating: 0,
    ratingcount: '',
    isLoading: false,
    fullPage: true,
    packageArea: '',
    content: '',
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'AED',
    CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1)
  },
  mounted() {
    this.getPageDetails();
    this.getPageContent();
    this.viewReview();
    sessionStorage.active_e = 2;
    var vm = this;
    this.$nextTick(function () {
      $('#Adultmyselect').on("change", function (e) {
        vm.dropdownChange(e, 'Adult')
      });
      $('#Infantmyselect').on("change", function (e) {
        vm.dropdownChange(e, 'Infant')
      });
      $('#Childmyselect').on("change", function (e) {
        vm.dropdownChange(e, 'Child')
      });
      $('#to-1').change(function () {
        vm.fdate = $('#to-1').val();
      });
    })

  },
  methods: {
    getPageDetails: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Holiday Detail/Holiday Detail/Holiday Detail.ftl';
        axios.get(pageurl, {
          headers: {
            'content-type': 'text/html',
            'Accept': 'text/html',
            'Accept-Language': langauage
          }
        }).then(function (response) {
          self.content = response.data;
          var bannerDetails = pluck('Banner_Section', self.content.area_List);
          if (bannerDetails != undefined) {
            var bannerDetailsTemp = getAllMapData(bannerDetails[0].component);
            self.bannerSection = bannerDetailsTemp;
          }
          var packageSection = pluck('Main_Section', self.content.area_List);
          if (packageSection != undefined) {
            var packSectionTemp = getAllMapData(packageSection[0].component);
            self.packageArea = packSectionTemp;
          }

          self.selectionLabel();
        })
      });
    },
    getPageContent: function () {
      var self = this;
      self.isLoading = false;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var packageurl = getQueryStringValue('page');
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        if (packageurl != "") {
          packageurl = packageurl.split('-').join(' ');
          packageurl = packageurl.split('_')[0];
          var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
          self.pageURLLink = packageurl;
          axios.get(topackageurl, {
            headers: {
              'content-type': 'text/html',
              'Accept': 'text/html',
              'Accept-Language': langauage
            }
          }).then(function (response) {
            self.content = response.data;
            var main = pluck('Package_Section', self.content.area_List);
            if (main != undefined) {
              var mainTemp = getAllMapData(main[0].component);
              self.mainContent = mainTemp;
            }
          }).catch(function (error) {
            console.log(error);
            self.isLoading = false;
          })
        }
      });
    },
    bookingPackage: async function () {
      let self = this;
      let dateObj = document.getElementById("to-1");
      let dateValue = dateObj.value;
      //  dateValue = moment(String(dateValue)).format('YYYY-MM-DDThh:mm:ss');
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.femail.match(emailPat);
      if (this.fname == undefined || this.fname == '') {
        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M01')).set('closable', false);
        return;
      } else if (this.femail == undefined || this.femail == '') {
        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M02')).set('closable', false);
        return;
      } else if (matchArray == null) {
        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M03')).set('closable', false);
        return false;
      } else if (this.fphone == undefined || this.fphone == '') {
        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M19')).set('closable', false);
        return;
      } 
      else if (this.fphone.length<8 || this.fphone.length>16) {
        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M20')).set('closable', false);
        return;
      }
       else if (this.fdate == undefined || this.fdate == '') {
        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M27')).set('closable', false);
        return;
      } else if (this.fadult == undefined || this.fadult == '' || this.fadult == 0) {
        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M28')).set('closable', false);
        return;
      }
      else {
        this.isLoading = true;
        //  var toEmail = JSON.parse(localStorage.User).emailId;
        var toEmail = this.femail;
        var frommail = JSON.parse(localStorage.User).loginNode.email;
        // var frommail = "itsolutionsoneview@gmail.com";
        var postData = postData = {
          type: "Package Booking",
          fromEmail: frommail,
          toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
          ccEmails: [],
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          packegeName: this.mainContent.Package_Title,
          personName: this.fname,
          emailAddress: this.femail,
          contact: this.fphone,
          departureDate: dateValue,
          adults: this.fadult,
          child2to5: "NA",
          child6to11: this.fchild,
          infants: this.finfants,
          message: this.fmessage,
          // primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          primaryColor: '#ffffff',
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        var custmail = {
          type: "UserAddedRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(this.femail) ? this.femail : [this.femail],
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.fname,
          primaryColor: '#ffffff',
        //  primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        // dateValue = moment(dateValue, 'DD/MM/YYYY').format('YYYY-MM-DDThh:mm:ss');
        dateValue = moment(String(dateValue)).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
          type: "Package Booking",
          keyword1: this.fname,
          keyword2: this.lname,
          keyword3: this.femail,
          keyword4: this.fphone,
          keyword5: this.mainContent.Package_Title,
          number1: this.fadult,
          number2: this.fchild,
          number3: this.finfants,
          text1: this.fmessage,
          date2: requestedDate,
          date1: dateValue,
          nodeCode: agencyCode
        };
        let responseObject = await cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
          let insertID = Number(responseObject);
          var emailApi = ServiceUrls.emailServices.emailApi;

          sendMailService(emailApi, postData);
          sendMailService(emailApi, custmail);
          alertify.alert(this.getValidationMsgByCode('M15'), this.getValidationMsgByCode('M29')).set('closable', false);
          self.fname = "";
          self.lname = "";
          self.femail = "";
          self.fphone = "";
          self.fdate = "";
          $('#to-1').val('');
          $('#Adultmyselect').val($('#Adultmyselect option:first-child').val()).trigger('change');
          $('#Childmyselect').val($('#Childmyselect option:first-child').val()).trigger('change');
          $('#Infantmyselect').val($('#Infantmyselect option:first-child').val()).trigger('change');
          self.fmessage = "";
          this.isLoading = false;
        } catch (e) {
          this.isLoading = false;
        }
      }
    },
    addReviews: async function () {
      // this.isLoading=true;
      let ratings = this.getUserRating();
      ratings = Number(ratings);
      if (ratings == 0) {
        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M25')).set('closable', false);
        return;
      }
      var self = this;
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.Email.match(emailPat);
      if (this.Name == undefined || this.Name == '') {
        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M01')).set('closable', false);
        return;
      } else if (this.Email == undefined || this.Email == '') {
        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M02')).set('closable', false);
        return;

      } else if (matchArray == null) {
        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M03')).set('closable', false);
        return false;
      } else if (this.Review == undefined || this.Review == '') {
        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M24')).set('closable', false);
        return;
      } else {
        this.isLoading = true;
        var frommail = JSON.parse(localStorage.User).loginNode.email;
        // var frommail = "itsolutionsoneview@gmail.com";
        var custmail = {
          type: "UserAddedRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(this.Email) ? this.Email : [this.Email],
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.Name,
          primaryColor: '#ffffff',
         // primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
          type: "Package Review",
          keyword4: self.pageURLLink,
          keyword1: self.mainContent.Package_Title,
          keyword2: self.Name,
          keyword3: self.Email,
          text1: self.Review,
          number1: ratings,
          date1: requestedDate,
          nodeCode: agencyCode
        };
        let responseObject = await cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
          let insertID = Number(responseObject);
          var emailApi = ServiceUrls.emailServices.emailApi;
          sendMailService(emailApi, custmail);
          this.Email = '';
          this.Name = '';
          ratings = 0,
            this.Review = '';
          this.viewReview();
          this.userRating(0);
          alertify.alert(this.getValidationMsgByCode('M15'), this.getValidationMsgByCode('M26')).set('closable', false);
          this.isLoading = false;
        } catch (e) {
          this.isLoading = false;
        }
      }
    },
    userRating: function (num) {
      ulList = document.getElementById("ratingID");
      let m = 0;
      for (let i = 0; i < ulList.childNodes.length; i++) {
        let childNode = ulList.childNodes[i];
        if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
          m = m + 1;
          for (let k = 0; k < childNode.childNodes.length; k++) {
            let style = childNode.childNodes[k].style.color;
            if ((m) < Number(num)) {
              childNode.childNodes[k].style = "color: rgb(239, 158, 8)";
            } else if ((m) == Number(num)) {
              if (style.trim() == "rgb(239, 158, 8)") {
                childNode.childNodes[k].style = "color: #a9a9a9;";
              } else {
                childNode.childNodes[k].style = "color: rgb(239, 158, 8);";
              }
            } else {
              childNode.childNodes[k].style = "color: #a9a9a9";
            }
          }
        }
      }
    },
    getUserRating: function () {
      var ulList = document.getElementById("ratingID");
      let m = 0;
      for (let i = 0; i < ulList.childNodes.length; i++) {
        let childNode = ulList.childNodes[i];
        if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
          let style = "";
          for (let k = 0; k < childNode.childNodes.length; k++) {
            style = childNode.childNodes[k].style.color;
            if (style != undefined && style != '') {
              break;
            }
          }
          if (style.trim() == "a9a9a9") {
            break;
          } else if (style.trim() == "rgb(239, 158, 8)") {
            m = m + 1;
          }
        }
      }
      return m;
    },
    viewReview: async function () {
      var self = this;
      let allReview = [];
      Ratings = 0;
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
      let requestObject = {
        from: 0,
        size: 100,
        type: "Package Review",
        nodeCode: agencyCode,
        orderBy: "desc"
      };
      let responseObject = await cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
        if (responseObject != undefined && responseObject.data != undefined) {
          allResult = JSON.parse(JSON.stringify(responseObject)).data;
          for (let i = 0; i < allResult.length; i++) {
            if (allResult[i].keyword4 == self.pageURLLink) {
              let object = {
                Name: allResult[i].keyword2,
                Date: moment(String(allResult[i].date1), "YYYY-MM-DDThh:mm:ss").format('MMM DD,YYYY'),
                comment: allResult[i].text1,
                Ratings: allResult[i].number1,
              };
              allReview.push(object);
            }
          }
          self.allReview = allReview;
          self.ratingcount = allReview.length;
          allratingcount = self.ratingcount;
          var avgratingtemp = 0;
          for (let i = 0; i < self.ratingcount; i++) {
            avgratingtemp = avgratingtemp + allReview[i].Ratings;
          }
          self.avgrating = ((avgratingtemp) / allratingcount).toFixed(2);
          if (self.avgrating > 0) {
            self.reviewAvailable = true;
          }
          if (allReview.length == 0) {
            self.reviewAvailable = false;
          }

        }
      });

    },
    getAmount: function (amount) {
      amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
      return amount;
    },
    navigateSocialPage(url) {
    
      if (url.includes('?')) {
        var fullUrl = url + "=" + encodeURIComponent(window.location.href);
        window.open(fullUrl, "_blank");
      } else {
        window.open(url, "_blank");
      }
    },
    dropdownChange: function (event, type) {
      let dateObj = document.getElementById("to-1");
      if (event != undefined && event.target != undefined && event.target.value != undefined) {

        if (type == 'Adult') {
          this.fadult = event.target.value;
        } else if (type == 'Infant') {
          this.finfants = event.target.value;
        } else if (type == 'Child') {
          this.fchild = event.target.value;
        }
      }
      let dateValue = dateObj.value;
      setTimeout(function () {
        let dateObjNew = document.getElementById("to-1");
        dateObjNew.value = dateValue;
      }, 100);
    },
    getValidationMsgByCode: function (code) {
      if (sessionStorage.validationItems !== undefined) {
        var validationList = JSON.parse(sessionStorage.validationItems);
        for (let validationItem of validationList.Validation_List) {
          if (code === validationItem.Code) {
            return validationItem.Message;
          }
        }
      }
    },

    selectionLabel: function () {
      var self = this;
      $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
      });
      $('b[role="presentation"]').hide();
      $('.select2-selection__arrow').append('<i class="fa  fa-chevron-down"></i>');
      $("#Adultmyselect").append("<option value='0' selected disabled>" + self.packageArea.Adult_Placeholder + "</option>",
        "<option value='1'>1</option>",
        "<option value='2'>2</option>",
        "<option value='3'>3</option>",
        "<option value='4'>4</option>",
      );
      $("#Infantmyselect").append("<option value='0' selected disabled>" + self.packageArea.Infant_Placeholder + "</option>",
        "<option value='0'>0</option>",
        "<option value='1'>1</option>",
        "<option value='2'>2</option>",
        "<option value='3'>3</option>",
        "<option value='4'>4</option>"
      );
      $("#Childmyselect").append("<option value='0' selected disabled>" + self.packageArea.Child_Placeholder + "</option>",
        "<option value='0'>0</option>",
        "<option value='1'>1</option>",
        "<option value='2'>2</option>",
        "<option value='3'>3</option>",
        "<option value='4'>4</option>"
      );
    }
  }
})

function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}