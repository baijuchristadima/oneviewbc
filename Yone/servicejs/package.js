const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
Vue.component('loading', VueLoading)
var packagelist = new Vue({
  i18n,
  el: '#holidays',
  name: 'holidays',
  data: {
    isLoading: false,
    fullPage: true,
    packages: [],
    BannerSection: {},
    Labels: {},
    totalHolidaypkgs: 0,
    currentPage: 1,
    currentPages: 1,
    fromPage: 1,
    totalpage: 1,
    constructedNumbers: [],
    package: [],
    pageLimit: 9,
    paginationLimit: 1,
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
    CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    allReview:[]
  },
  methods: {
    getPagecontent: function () {
      var self = this;
      self.isLoading = false;
      getAgencycode(function (response) {
        var Agencycode = response;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Package List/Package List/Package List.ftl';
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Holidays/Holidays/Holidays.ftl';
        axios.get(topackageurl, {
          headers: {
            'content-type': 'text/html',
            'Accept': 'text/html',
            'Accept-Language': langauage
          }
        }).then(function (response) {
          var packageData = response.data;
            let holidayaPackageListTemp = [];        
            if (packageData != undefined && packageData.Values != undefined) {
            holidayaPackageListTemp = packageData.Values.filter(function (el) {
              return el.Is_Active == true
            });
          }
          self.packages = holidayaPackageListTemp;
          self.setTotalPackageCount();
          self.getpackage = true;
          self.isLoading=false;
        })
        .catch(function (error) {
          console.log('Error');
          self.content = [];
          self.isLoading = false;
        });
        axios.get(pageurl, {
          headers: {
            'content-type': 'text/html',
            'Accept': 'text/html',
            'Accept-Language': langauage
          }
        }).then(function (response) {
          self.content = response.data;
          var bannerDetails = pluck('Banner_Section', self.content.area_List);
          if (bannerDetails != undefined) {
            var bannerDetailsTemp = getAllMapData(bannerDetails[0].component);
            self.BannerSection = bannerDetailsTemp;
          }
          var packageHeaders = pluck('Main_Section', self.content.area_List);
          if (packageHeaders != undefined) {
            var packageTemp = getAllMapData(packageHeaders[0].component);
            self.Labels = packageTemp;
          }
          self.isLoading = false;
        }).catch(function (error) {
          console.log(error);
          self.content = [];
          self.isLoading = false;
        });
      });
    },
    getmoreinfo(url) {
      if (url != null) {
        if (url != "") {
          url = url.split("/Template/")[1];
          url = url.split(' ').join('-');
          url = url.split('.').slice(0, -1).join('.')
          url = "/Yone/package-detail.html?page=" + url;
          window.location.href = url;
        } else {
          url = "#";
        }
      } else {
        url = "#";
      }
      return url;
    },
    setTotalPackageCount: function () {
      if (this.packages != undefined && this.packages != undefined) {
        this.totalHolidaypkgs = Number(this.packages.length);
        this.totalpage = Math.ceil(this.totalHolidaypkgs / this.pageLimit);
        this.currentPage = 1;
        this.fromPage = 1;
        if (Number(this.totalHolidaypkgs) < 6) {}
        this.constructAllPagianationLink();
      }
    },
    constructAllPagianationLink: function () {
      let limit = this.paginationLimit;
      this.constructedNumbers = [];
      for (let i = Number(this.fromPage); i <= (Number(this.totalpage) + limit); i++) {
        if (i <= Number(this.totalpage)) {
          this.constructedNumbers.push(i);
        }
      }
      this.currentPage = this.constructedNumbers;
      this.setPackageItems();
    },
    prevNextEvent: function (type) {
      let limit = this.paginationLimit;
      if (type == 'Previous') {
        if (this.currentPages > this.totalpage) {
          this.currentPages = this.currentPages - 1;
        }
        this.fromPage = this.currentPages;
        if (Number(this.fromPage) != 1) {
          this.currentPages = Number(this.currentPages) - 1;
          this.setPackageItems();
        }

      } else if (type == 'Next') {
        if (this.currentPages == 'undefined' || this.currentPages == '') {}
        if (this.currentPages <= this.totalpage) {
          let limit = this.paginationLimit;
          this.fromPage = this.currentPages;
          var totalP = (this.totalpage) + 1;
          if (Number(this.fromPage) != totalP) {
            var count = this.currentPages + limit;
            if (Number(count) <= Number(this.totalpage)) {
              this.selectPacakges(this.currentPages);
            }

          }
        }
      }
    },
    selectPacakges: function (ev) {
      let limit = 1;
      this.currentPages = this.currentPage[ev];
      this.setPackageItems();
    },
    selected: function (ev) {
      let limit = 1;
      this.currentPages = this.currentPage[ev];
      this.setPackageItems();
    },
    setPackageItems: function () {
      if (this.packages != undefined && this.packages != undefined) {
        let start = 0;
        let end = Number(start) + Number(this.pageLimit);
        if (Number(this.currentPages) == 1) {} else {
          var limit = this.totalpage;
          start = (Number(this.currentPages) + Number(this.pageLimit)) - 2;
          end = Number(start) + Number(this.pageLimit);
        }
        this.package = this.packages.slice(start, end);
      }
    },
     viewReview: async function () {
            var self=this;
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestObject = { from: 0, size: 100, type: "Package Review", nodeCode: agencyCode, orderBy: "desc" };
               let responseObject = await cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
                  if (responseObject != undefined && responseObject.data != undefined) {
                    self.allReview = JSON.parse(JSON.stringify(responseObject)).data;                   
                }
                });
          },
          getrating:function(url){
              var self=this;
          //  url = url.split("/Template/")[1];
          //  url = url.split(' ').join('-');
          //  url = url.split('.').slice(0, -1).join('.');
          //  url=url.split('_')[0];
           let ratingTemp =[];
           ratingTemp =self.allReview.filter(rating=>rating.keyword1 == url);
           var sum = 0;
            if (ratingTemp.length > 0) {
                $.each(ratingTemp, function () {
                    sum += this.number1 ? this.number1 : 0;
                })
                sum = sum/ratingTemp.length;
            }
            return sum;
        },

  },
  mounted: function () {
    this.getPagecontent();
    this.viewReview();
  }
  
});
function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
