var currencycomp = Vue.component("yone-currency-select", {
  template: `<div v-show="showcurrncy && currencies && currencies.length">
  <select class='selectpicker' data-width='fit'>
  <option v-for='CurrencyOption in currencies'  :value="JSON.stringify(CurrencyOption)" :data-content='CurrencyOption.dataContent'></option>
  </select>
    </div>
      `,
  data() {
    return {
      currencies: [],
      startCurrency: localStorage.selectedCurrency
        ? localStorage.selectedCurrency
        : null,
      selectedCurrency: localStorage.selectedCurrency
        ? localStorage.selectedCurrency
        : "AED" || "AED",
      CurrencyOptions: [
        {
          text: "AED UAE Dirham",
          value: "AED",
          dataContent: '<span class="flag-icon flag-icon-ae"></span> AED',
        },
        {
          text: "AFN Afghanistan Afghani ",
          value: "AFN",
          dataContent: '<span class="flag-icon flag-icon-af"></span> AFN',
        },
        {
          text: "ALL Albania Lek ",
          value: "ALL",
          dataContent: '<span class="flag-icon flag-icon-al"></span> ALL',
        },
        {
          text: "AMD Armenia Dram ",
          value: "AMD",
          dataContent: '<span class="flag-icon flag-icon-am"></span> AMD ',
        },
        {
          text: "ANG Netherlands Antilles ",
          value: "ANG",
          dataContent: '<span class="flag-icon flag-icon-nl"></span> ANG',
        },
        {
          text: "AOA Angola Kwanza ",
          value: "AOA",
          dataContent: '<span class="flag-icon flag-icon-ao"></span> AOA',
        },
        {
          text: "ARS Argentina Peso ",
          value: "ARS",
          dataContent: '<span class="flag-icon flag-icon-ar"></span> ARS',
        },
        {
          text: "AUD Australia Dollar ",
          value: "AUD",
          dataContent: '<span class="flag-icon flag-icon-au"></span> AUD',
        },
        {
          text: "AWG Aruba Guilder ",
          value: "AWG",
          dataContent: '<span class="flag-icon flag-icon-aw"></span> AWG',
        },
        {
          text: "AZN Azerbaijan New Manat ",
          value: "AZN",
          dataContent: '<span class="flag-icon flag-icon-az"></span> AZN',
        },
        {
          text: "BAM Bosnia and Herzegovi ",
          value: "BAM",
          dataContent: '<span class="flag-icon flag-icon-ba"></span> BAM',
        },
        {
          text: "BBD Barbados Dollar ",
          value: "BBD",
          dataContent: '<span class="flag-icon flag-icon-bb"></span> BBD',
        },
        {
          text: "BDT Bangladesh Taka ",
          value: "BDT",
          dataContent: '<span class="flag-icon flag-icon-bd"></span> BDT',
        },
        {
          text: "BGN Bulgaria Lev ",
          value: "BGN",
          dataContent: '<span class="flag-icon flag-icon-bg"></span> BGN ',
        },
        {
          text: "BHD Bahrain Dinar ",
          value: "BHD",
          dataContent: '<span class="flag-icon flag-icon-bh"></span> BHD',
        },
        {
          text: "BIF Burundi Franc ",
          value: "BIF",
          dataContent: '<span class="flag-icon flag-icon-bi"></span> BIF',
        },
        {
          text: "BMD Bermuda Dollar ",
          value: "BMD",
          dataContent: '<span class="flag-icon flag-icon-bm"></span> BMD',
        },
        {
          text: "BND Brunei Darussalam Do ",
          value: "BND",
          dataContent: '<span class="flag-icon flag-icon-bn"></span> BND',
        },
        {
          text: "BOB Bolivian boliviano ",
          value: "BOB",
          dataContent: '<span class="flag-icon flag-icon-b0"></span> BOB',
        },
        {
          text: "BRL Brazil Real ",
          value: "BRL",
          dataContent: '<span class="flag-icon flag-icon-br"></span> BRL',
        },
        {
          text: "BSD Bahamas Dollar ",
          value: "BSD",
          dataContent: '<span class="flag-icon flag-icon-bs"></span> BSD ',
        },
        {
          text: "BTN Bhutan Ngultrum ",
          value: "BTN",
          dataContent: '<span class="flag-icon flag-icon-bt"></span> BTN',
        },
        {
          text: "BWP Botswana Pula ",
          value: "BWP",
          dataContent: '<span class="flag-icon flag-icon-bw"></span> BWP',
        },
        {
          text: "BYN Belarus Ruble ",
          value: "BYN",
          dataContent: '<span class="flag-icon flag-icon-by"></span> BYN',
        },
        {
          text: "BZD Belize Dollar ",
          value: "BZD",
          dataContent: '<span class="flag-icon flag-icon-bz"></span> BZD',
        },
        {
          text: "CAD Canada Dollar ",
          value: "CAD",
          dataContent: '<span class="flag-icon flag-icon-ca"></span> CAD',
        },
        {
          text: "CDF Congo/Kinshasa Franc ",
          value: "CDF",
          dataContent: '<span class="flag-icon flag-icon-ca"></span> CDF',
        },
        {
          text: "CHF Switzerland Franc ",
          value: "CHF",
          dataContent: '<span class="flag-icon flag-icon-ch"></span> CHF ',
        },
        {
          text: "CLP Chile Peso ",
          value: "CLP",
          dataContent: '<span class="flag-icon flag-icon-cl"></span> CLP',
        },
        {
          text: "CNY China Yuan Renminbi ",
          value: "CNY",
          dataContent: '<span class="flag-icon flag-icon-cn"></span> CNY',
        },
        {
          text: "COP Colombia Peso ",
          value: "COP",
          dataContent: '<span class="flag-icon flag-icon-co"></span> COP ',
        },
        {
          text: "CRC Costa Rica Colon ",
          value: "CRC",
          dataContent: '<span class="flag-icon flag-icon-cr"></span> CRC',
        },
        {
          text: "CUC Cuba Convertible Pes ",
          value: "CUC",
          dataContent: '<span class="flag-icon flag-icon-cu"></span> CUC',
        },
        {
          text: "CUP Cuba Peso ",
          value: "CUP",
          dataContent: '<span class="flag-icon flag-icon-cu"></span> CUP ',
        },
        {
          text: "CVE Cape Verde Escudo ",
          value: "CVE",
          dataContent: '<span class="flag-icon flag-icon-cv"></span> CVE',
        },
        {
          text: "CZK Czech Republic Korun ",
          value: "CZK",
          dataContent: '<span class="flag-icon flag-icon-cz"></span> CZK ',
        },
        {
          text: "DJF Djibouti Franc ",
          value: "DJF",
          dataContent: '<span class="flag-icon flag-icon-dj"></span> DJF ',
        },
        {
          text: "DKK Denmark Krone ",
          value: "DKK",
          dataContent: '<span class="flag-icon flag-icon-dk"></span> DKK ',
        },
        {
          text: "DOP Dominican Republic P ",
          value: "DOP",
          dataContent: '<span class="flag-icon flag-icon-do"></span> DOP',
        },
        {
          text: "DZD Algeria Dinar ",
          value: "DZD",
          dataContent: '<span class="flag-icon  flag-icon-dz"></span> DZD ',
        },
        {
          text: "EGP Egypt Pound ",
          value: "EGP",
          dataContent: '<span class="flag-icon flag-icon-eg"></span> EGP ',
        },
        {
          text: "ERN Eritrea Nakfa ",
          value: "ERN",
          dataContent: '<span class="flag-icon flag-icon-er"></span> ERN ',
        },
        {
          text: "ETB Ethiopia Birr ",
          value: "ETB",
          dataContent: '<span class="flag-icon flag-icon-et"></span> ETB ',
        },
        {
          text: "EUR Euro Member Countrie ",
          value: "EUR",
          dataContent: '<span class="flag-icon flag-icon-fr"></span> EUR',
        },
        {
          text: "FJD Fiji Dollar ",
          value: "FJD",
          dataContent: '<span class="flag-icon flag-icon-fj"></span> FJD ',
        },
        {
          text: "FKP Falkland Islands (Ma ",
          value: "FKP",
          dataContent: '<span class="flag-icon flag-icon-fk"></span> FKP',
        },
        {
          text: "GBP United Kingdom Pound ",
          value: "GBP",
          dataContent: '<span class="flag-icon flag-icon-gb"></span> GBP',
        },
        {
          text: "GEL Georgia Lari ",
          value: "GEL",
          dataContent: '<span class="flag-icon flag-icon-ge"></span> GEL ',
        },
        {
          text: "GGP Guernsey Pound ",
          value: "GGP",
          dataContent: '<span class="flag-icon flag-icon-gg"></span> GGP ',
        },
        {
          text: "GHS Ghana Cedi ",
          value: "GHS",
          dataContent: '<span class="flag-icon flag-icon-gh"></span> GHS ',
        },
        {
          text: "GIP Gibraltar Pound ",
          value: "GIP",
          dataContent: '<span class="flag-icon flag-icon-gi"></span> GIP',
        },
        {
          text: "GMD Gambia Dalasi ",
          value: "GMD",
          dataContent: '<span class="flag-icon flag-icon-gm"></span> GMD ',
        },
        {
          text: "GNF Guinea Franc ",
          value: "GNF",
          dataContent: '<span class="flag-icon flag-icon-gn"></span> GNF ',
        },
        {
          text: "GTQ Guatemala Quetzal ",
          value: "GTQ",
          dataContent: '<span class="flag-icon flag-icon-gt"></span> GTQ',
        },
        {
          text: "GYD Guyana Dollar ",
          value: "GYD",
          dataContent: '<span class="flag-icon flag-icon-gy"></span> GYD ',
        },
        {
          text: "HKD Hong Kong Dollar ",
          value: "HKD",
          dataContent: '<span class="flag-icon flag-icon-hk"></span> HKD',
        },
        {
          text: "HNL Honduras Lempira ",
          value: "HNL",
          dataContent: '<span class="flag-icon flag-icon-hn"></span> HNL',
        },
        {
          text: "HRK Croatia Kuna ",
          value: "HRK",
          dataContent: '<span class="flag-icon flag-icon-hr"></span> HRK',
        },
        {
          text: "HTG Haiti Gourde ",
          value: "HTG",
          dataContent: '<span class="flag-icon flag-icon-ht"></span> HTG ',
        },
        {
          text: "HUF Hungary Forint ",
          value: "HUF",
          dataContent: '<span class="flag-icon flag-icon-hu"></span> HUF ',
        },
        {
          text: "IDR Indonesia Rupiah ",
          value: "IDR",
          dataContent: '<span class="flag-icon flag-icon-id"></span> IDR',
        },
        {
          text: "ILS Israel Shekel ",
          value: "ILS",
          dataContent: '<span class="flag-icon flag-icon-il"></span> ILS',
        },
        {
          text: "IMP Isle of Man Pound ",
          value: "IMP",
          dataContent: '<span class="flag-icon flag-icon-im"></span> IMP',
        },
        {
          text: "INR India Rupee ",
          value: "INR",
          dataContent: '<span class="flag-icon flag-icon-in"></span> INR ',
        },
        {
          text: "IQD Iraq Dinar ",
          value: "IQD",
          dataContent: '<span class="flag-icon flag-icon-iq"></span> IQD ',
        },
        {
          text: "IRR Iran Rial ",
          value: "IRR",
          dataContent: '<span class="flag-icon flag-icon-ir"></span> IRR ',
        },
        {
          text: "ISK Iceland Krona ",
          value: "ISK",
          dataContent: '<span class="flag-icon flag-icon-is"></span> ISK ',
        },
        {
          text: "JEP Jersey Pound ",
          value: "JEP",
          dataContent: '<span class="flag-icon flag-icon-je"></span> JEP ',
        },
        {
          text: "JMD Jamaica Dollar ",
          value: "JMD",
          dataContent: '<span class="flag-icon flag-icon-jm"></span> JMD ',
        },
        {
          text: "JOD Jordan Dinar ",
          value: "JOD",
          dataContent: '<span class="flag-icon flag-icon-jo"></span> JOD ',
        },
        {
          text: "JPY Japan Yen ",
          value: "JPY",
          dataContent: '<span class="flag-icon flag-icon-jp"></span> JPY ',
        },
        {
          text: "KES Kenya Shilling ",
          value: "KES",
          dataContent: '<span class="flag-icon flag-icon-ke"></span> KES ',
        },
        {
          text: "KGS Kyrgyzstan Som ",
          value: "KGS",
          dataContent: '<span class="flag-icon flag-icon-kg"></span> KGS ',
        },
        {
          text: "KHR Cambodia Riel ",
          value: "KHR",
          dataContent: '<span class="flag-icon flag-icon-kh"></span> KHR ',
        },
        {
          text: "KMF Comoros Franc ",
          value: "KMF",
          dataContent: '<span class="flag-icon flag-icon-km"></span> KMF ',
        },
        {
          text: "KPW Korea (North) Won ",
          value: "KPW",
          dataContent: '<span class="flag-icon flag-icon-kp"></span> KPW',
        },
        {
          text: "KRW Korea (South) Won ",
          value: "KRW",
          dataContent: '<span class="flag-icon flag-icon-kr"></span> KRW',
        },
        {
          text: "KWD Kuwait Dinar ",
          value: "KWD",
          dataContent: '<span class="flag-icon flag-icon-kw"></span> KWD ',
        },
        {
          text: "KYD Cayman Islands Dolla ",
          value: "KYD",
          dataContent: '<span class="flag-icon flag-icon-ky"></span> KYD ',
        },
        {
          text: "KZT Kazakhstan Tenge ",
          value: "KZT",
          dataContent: '<span class="flag-icon flag-icon-kz"></span> KZT',
        },
        {
          text: "LAK Laos Kip ",
          value: "LAK",
          dataContent: '<span class="flag-icon flag-icon-la"></span> LAK ',
        },
        {
          text: "LBP Lebanon Pound ",
          value: "LBP",
          dataContent: '<span class="flag-icon flag-icon-lb"></span> LBP ',
        },
        {
          text: "LKR Sri Lanka Rupee ",
          value: "LKR",
          dataContent: '<span class="flag-icon flag-icon-lk"></span> LKR',
        },
        {
          text: "LRD Liberia Dollar ",
          value: "LRD",
          dataContent: '<span class="flag-icon flag-icon-lr"></span> LRD ',
        },
        {
          text: "LSL Lesotho Loti ",
          value: "LSL",
          dataContent: '<span class="flag-icon flag-icon-ls"></span> LSL ',
        },
        {
          text: "LYD Libya Dinar ",
          value: "LYD",
          dataContent: '<span class="flag-icon flag-icon-ly"></span> LYD ',
        },
        {
          text: "MAD Morocco Dirham ",
          value: "MAD",
          dataContent: '<span class="flag-icon flag-icon-ma"></span> MAD ',
        },
        {
          text: "MDL Moldova Leu ",
          value: "MDL",
          dataContent: '<span class="flag-icon flag-icon-md"></span> MDL ',
        },
        {
          text: "MGA Madagascar Ariary ",
          value: "MGA",
          dataContent: '<span class="flag-icon flag-icon-mg"></span> MGA',
        },
        {
          text: "MKD Macedonia Denar ",
          value: "MKD",
          dataContent: '<span class="flag-icon flag-icon-mk"></span> MKD',
        },
        {
          text: "MMK Myanmar (Burma) Kyat ",
          value: "MMK",
          dataContent: '<span class="flag-icon flag-icon-mm"></span> MMK',
        },
        {
          text: "MNT Mongolia Tughrik ",
          value: "MNT",
          dataContent: '<span class="flag-icon flag-icon-mn"></span> MNT',
        },
        {
          text: "MOP Macau Pataca ",
          value: "MOP",
          dataContent: '<span class="flag-icon flag-icon-mo"></span> MOP ',
        },
        {
          text: "MRO Mauritania Ouguiya ",
          value: "MRO",
          dataContent: '<span class="flag-icon flag-icon-mr"></span> MRO',
        },
        {
          text: "MUR Mauritius Rupee ",
          value: "MUR",
          dataContent: '<span class="flag-icon flag-icon-mu"></span> MUR',
        },
        {
          text: "MVR Maldives (Maldive Is ",
          value: "MVR",
          dataContent: '<span class="flag-icon flag-icon-mv"></span> MVR',
        },
        {
          text: "MWK Malawi Kwacha ",
          value: "MWK",
          dataContent: '<span class="flag-icon flag-icon-mw"></span> MWK ',
        },
        {
          text: "MXN Mexico Peso ",
          value: "MXN",
          dataContent: '<span class="flag-icon flag-icon-mx"></span> MXN ',
        },
        {
          text: "MYR Malaysia Ringgit ",
          value: "MYR",
          dataContent: '<span class="flag-icon flag-icon-my"></span> MYR',
        },
        {
          text: "MZN Mozambique Metical ",
          value: "MZN",
          dataContent: '<span class="flag-icon flag-icon-mz"></span> MZN',
        },
        {
          text: "NAD Namibia Dollar ",
          value: "NAD",
          dataContent: '<span class="flag-icon flag-icon-na"></span> NAD ',
        },
        {
          text: "NGN Nigeria Naira ",
          value: "NGN",
          dataContent: '<span class="flag-icon flag-icon-ng"></span> NGN ',
        },
        {
          text: "NIO Nicaragua Cordoba ",
          value: "NIO",
          dataContent: '<span class="flag-icon flag-icon-ni"></span> NIO',
        },
        {
          text: "NOK Norway Krone ",
          value: "NOK",
          dataContent: '<span class="flag-icon flag-icon-no"></span> NOK ',
        },
        {
          text: "NPR Nepal Rupee ",
          value: "NPR",
          dataContent: '<span class="flag-icon flag-icon-np"></span> NPR ',
        },
        {
          text: "NZD New Zealand Dollar ",
          value: "NZD",
          dataContent: '<span class="flag-icon flag-icon-nz"></span> NZD',
        },
        {
          text: "OMR Oman Rial ",
          value: "OMR",
          dataContent: '<span class="flag-icon flag-icon-om"></span> OMR ',
        },
        {
          text: "PAB Panama Balboa ",
          value: "PAB",
          dataContent: '<span class="flag-icon flag-icon-pa"></span> PAB ',
        },
        {
          text: "PEN Peru Sol ",
          value: "PEN",
          dataContent: '<span class="flag-icon flag-icon-pe"></span> PEN ',
        },
        {
          text: "PGK Papua New Guinea Kin ",
          value: "PGK",
          dataContent: '<span class="flag-icon flag-icon-pg"></span> PGK',
        },
        {
          text: "PHP Philippines Peso ",
          value: "PHP",
          dataContent: '<span class="flag-icon flag-icon-ph"></span> PHP',
        },
        {
          text: "PKR Pakistan Rupee ",
          value: "PKR",
          dataContent: '<span class="flag-icon flag-icon-pk"></span> PKR ',
        },
        {
          text: "PLN Poland Zloty ",
          value: "PLN",
          dataContent: '<span class="flag-icon flag-icon-pl"></span> PLN ',
        },
        {
          text: "PYG Paraguay Guarani ",
          value: "PYG",
          dataContent: '<span class="flag-icon flag-icon-py"></span> PYG',
        },
        {
          text: "QAR Qatar Riyal ",
          value: "QAR",
          dataContent: '<span class="flag-icon flag-icon-qa"></span> QAR ',
        },
        {
          text: "RON Romania New Leu ",
          value: "RON",
          dataContent: '<span class="flag-icon flag-icon-ro"></span> RON',
        },
        {
          text: "RSD Serbia Dinar ",
          value: "RSD",
          dataContent: '<span class="flag-icon flag-icon-rs"></span> RSD ',
        },
        {
          text: "RUB Russia Ruble ",
          value: "RUB",
          dataContent: '<span class="flag-icon flag-icon-ru"></span> RUB ',
        },
        {
          text: "RWF Rwanda Franc ",
          value: "RWF",
          dataContent: '<span class="flag-icon flag-icon-rw"></span> RWF ',
        },
        {
          text: "SAR Saudi Arabia Riyal ",
          value: "SAR",
          dataContent: '<span class="flag-icon flag-icon-sa"></span> SAR',
        },
        {
          text: "SBD Solomon Islands Doll ",
          value: "SBD",
          dataContent: '<span class="flag-icon flag-icon-sb"></span> SBD',
        },
        {
          text: "SCR Seychelles Rupee ",
          value: "SCR",
          dataContent: '<span class="flag-icon flag-icon-sc"></span> SCR',
        },
        {
          text: "SDG Sudan Pound ",
          value: "SDG",
          dataContent: '<span class="flag-icon flag-icon-sd"></span> SDG ',
        },
        {
          text: "SEK Sweden Krona ",
          value: "SEK",
          dataContent: '<span class="flag-icon flag-icon-se"></span> SEK ',
        },
        {
          text: "SGD Singapore Dollar ",
          value: "SGD",
          dataContent: '<span class="flag-icon flag-icon-sg"></span> SGD',
        },
        {
          text: "SHP Saint Helena Pound ",
          value: "SHP",
          dataContent: '<span class="flag-icon flag-icon-sh"></span> SHP ',
        },
        {
          text: "SLL Sierra Leone Leone ",
          value: "SLL",
          dataContent: '<span class="flag-icon flag-icon-sl"></span> SLL',
        },
        {
          text: "SOS Somalia Shilling ",
          value: "SOS",
          dataContent: '<span class="flag-icon flag-icon-so"></span> SOS',
        },
        {
          text: "SPL Seborga Luigino ",
          value: "SPL",
          dataContent: '<span class="flag-icon flag-icon-"></span> SPL',
        },
        {
          text: "SRD Suriname Dollar ",
          value: "SRD",
          dataContent: '<span class="flag-icon flag-icon-sr"></span> SRD',
        },
        {
          text: "SSP South Sudanese Pound ",
          value: "SSP",
          dataContent: '<span class="flag-icon flag-icon-ss"></span> SSP ',
        },
        {
          text: "STD Sao Tome and Princip ",
          value: "STD",
          dataContent: '<span class="flag-icon flag-icon-st"></span> STD',
        },
        {
          text: "SVC El Salvador Colon ",
          value: "SVC",
          dataContent: '<span class="flag-icon flag-icon-sv"></span> SVC',
        },
        {
          text: "SYP Syria Pound ",
          value: "SYP",
          dataContent: '<span class="flag-icon flag-icon-sy"></span> SYP ',
        },
        {
          text: "SZL Swaziland Lilangeni ",
          value: "SZL",
          dataContent: '<span class="flag-icon flag-icon-sz"></span> SZL',
        },
        {
          text: "THB Thailand Baht ",
          value: "THB",
          dataContent: '<span class="flag-icon flag-icon-th"></span> THB ',
        },
        {
          text: "TJS Tajikistan Somoni ",
          value: "TJS",
          dataContent: '<span class="flag-icon flag-icon-tj"></span> TJS',
        },
        {
          text: "TMT Turkmenistan Manat ",
          value: "TMT",
          dataContent: '<span class="flag-icon flag-icon-tm"></span> TMT',
        },
        {
          text: "TND Tunisia Dinar ",
          value: "TND",
          dataContent: '<span class="flag-icon flag-icon-tn"></span> TND ',
        },
        {
          text: "TOP Tonga Paanga ",
          value: "TOP ",
          dataContent:
            ' < span class = "flag-icon flag-icon-to" > < /span> TOP ',
        },
        {
          text: "TRY Turkey Lira ",
          value: "TRY",
          dataContent: '<span class="flag-icon flag-icon-tr"></span> TRY ',
        },
        {
          text: "TTD Trinidad and Tobago ",
          value: "TTD",
          dataContent: '<span class="flag-icon flag-icon-tt"></span> TTD',
        },
        {
          text: "TVD Tuvalu Dollar ",
          value: "TVD",
          dataContent: '<span class="flag-icon flag-icon-tv"></span> TVD ',
        },
        {
          text: "TWD Taiwan New Dollar ",
          value: "TWD",
          dataContent: '<span class="flag-icon flag-icon-tw"></span> TWD',
        },
        {
          text: "TZS Tanzania Shilling ",
          value: "TZS",
          dataContent: '<span class="flag-icon flag-icon-tz"></span> TZS',
        },
        {
          text: "UAH Ukraine Hryvnia ",
          value: "UAH",
          dataContent: '<span class="flag-icon flag-icon-ua"></span> UAH',
        },
        {
          text: "UGX Uganda Shilling ",
          value: "UGX",
          dataContent: '<span class="flag-icon flag-icon-ug"></span> UGX',
        },
        {
          text: "USD United States Dollar ",
          value: "USD",
          dataContent: '<span class="flag-icon flag-icon-us"></span> USD',
        },
        {
          text: "UYU Uruguay Peso ",
          value: "UYU",
          dataContent: '<span class="flag-icon flag-icon-uy"></span> UYU ',
        },
        {
          text: "UZS Uzbekistan Som ",
          value: "UZS",
          dataContent: '<span class="flag-icon flag-icon-uz"></span> UZS ',
        },
        {
          text: "VEF Venezuela Bolivar ",
          value: "VEF",
          dataContent: '<span class="flag-icon flag-icon-ve"></span> VEF',
        },
        {
          text: "VND Viet Nam Dong ",
          value: "VND",
          dataContent: '<span class="flag-icon flag-icon-vn"></span> VND ',
        },
        {
          text: "VUV Vanuatu Vatu ",
          value: "VUV",
          dataContent: '<span class="flag-icon flag-icon-vu"></span> VUV ',
        },
        {
          text: "WST Samoa Tala ",
          value: "WST",
          dataContent: '<span class="flag-icon flag-icon-ws"></span> WST',
        },
        {
          text: "XAF Central Africa CFA fra ",
          value: "XAF",
          dataContent: '<span class="flag-icon flag-icon-"></span> XAF',
        },
        {
          text: "XCD East Caribbean Dolla ",
          value: "XCD",
          dataContent: '<span class="flag-icon flag-icon-"></span> XCD ',
        },
        {
          text: "XOF West African CFA fra ",
          value: "XOF",
          dataContent: '<span class="flag-icon flag-icon-cf"></span> XOF',
        },
        {
          text: "XPF CFP Franc ",
          value: "XPF",
          dataContent: '<span class="flag-icon flag-icon-pf"></span> XPF ',
        },
        {
          text: "YER Yemen Rial ",
          value: "YER",
          dataContent: '<span class="flag-icon flag-icon-ye"></span> YER ',
        },
        {
          text: "ZAR South Africa Rand ",
          value: "ZAR",
          dataContent: '<span class="flag-icon flag-icon-za"></span> ZAR',
        },
        {
          text: "ZMW Zambia Kwacha ",
          value: "ZMW",
          dataContent: '<span class="flag-icon flag-icon-zm"></span> ZMW ',
        },
        {
          text: "ZWD Zimbabwe Dollar ",
          value: "ZWD",
          dataContent: '<span class="flag-icon flag-icon-zw"></span> ZWD',
        },
        {
          text: "ZWL Zimbabwe Dollar",
          value: "ZWL",
          dataContent: '<span class="flag-icon flag-icon-zw"></span> ZWL ',
        },
      ],
      currencyarr: [],
      defaultCurrency: [],
      showcurrncy: false,
      // v-on:change='onSelected(item.value, item.rate)'
      // v-on:change="onSelected2($event)"
    };
  },
  mounted: function () {
    var self = this;
    getAgencycode(function (response) {
      self.currencyarr = self.CurrencyOptions.find(
        (x) => x.value === self.selectedCurrency
      );
      let axiosConfig = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      var huburl = ServiceUrls.hubConnection.baseUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var AgencyCode = response;
      AgencyCode = AgencyCode.replace("AGY", "");
      var requrl = "/currencyrate/" + AgencyCode;

      axios
        .get(huburl + portno + requrl, axiosConfig)
        .then((response) => {
          var currency = response.data;
          if (currency) {
            self.defaultCurrency = self.CurrencyOptions.find(
              (x) => x.value === currency[0].toCurrencyCode.toUpperCase()
            );
            self.defaultCurrency.rate = 1;
            self.currencies.push({
              text: self.defaultCurrency.text,
              value: self.defaultCurrency.value,
              rate: self.defaultCurrency.rate,
              dataContent: self.defaultCurrency.dataContent,
            });
            if (localStorage.getItem("selectedCurrency") === null) {
              localStorage.selectedCurrency = currency[0].toCurrencyCode.toUpperCase();
              localStorage.CurrencyMultiplier = 1;
            }
            currency.map(function (value, key) {
              var tempcur = self.CurrencyOptions.find(
                (x) => x.value === value.fromCurrencyCode.toUpperCase()
              );
              self.currencies.push({
                text: tempcur.text,
                value: tempcur.value,
                rate: value.sellRate,
                dataContent: tempcur.dataContent,
              });
            });

            setTimeout(function () {
              currencySelect();
              var currencyTemp;
              if (self.startCurrency) {
                currencyTemp = self.currencies.find(
                  (x) => x.value === self.startCurrency
                );
              } else {
                currencyTemp = self.currencies.find(
                  (x) => x.value === self.defaultCurrency.value
                );
              }
              $(".selectpicker").selectpicker(
                "val",
                JSON.stringify(currencyTemp)
              );
              self.onSelected(currencyTemp.value, currencyTemp.rate);
            }, 10);
          }
        })
        .catch((err) => { });
    });

    var vm = this;
    this.$nextTick(function () {
      $(".selectpicker").on("change", function (e) {
        vm.dropdownChange(e);
      });
    });
  },
  methods: {
    onSelected: function (code, rate) {
      localStorage.selectedCurrency = code;
      localStorage.CurrencyMultiplier = rate;
      this.currencyarr = this.CurrencyOptions.find((x) => x.value === code);
      var hName = window.location.pathname.toString().split("/")[1];
      var pagePath = window.location.pathname;
      switch (pagePath) {
        case "/Hotels/hotel-listing.html":
          Vue_HotelListing.selectedCurrency = code;
          Vue_HotelListing.CurrencyMultiplier = rate;
          var slider = document.getElementById("ageSlider");
          slider.noUiSlider.set([
            parseFloat(Vue_HotelListing.lowestPrice),
            parseFloat(Vue_HotelListing.highestPrice),
          ]);
          slider.noUiSlider.updateOptions({
            range: {
              min: parseFloat(Vue_HotelListing.lowestPrice),
              max: parseFloat(Vue_HotelListing.highestPrice),
            },
          });
          slider.noUiSlider.on("update", function (values, handle) {
            $("#minPricevalue").text(
              Vue_HotelListing.$n(
                parseFloat(values[0] / Vue_HotelListing.CurrencyMultiplier),
                "currency",
                Vue_HotelListing.selectedCurrency
              )
            );
            $("#MaxPricevalue").text(
              Vue_HotelListing.$n(
                parseFloat(values[1] / Vue_HotelListing.CurrencyMultiplier),
                "currency",
                Vue_HotelListing.selectedCurrency
              )
            );

            Vue_HotelListing.priceFilter(values[0], values[1]);
          });
          break;
        case "/Flights/flight-listing.html":
          searchResults.selectedCurrency = code;
          searchResults.CurrencyMultiplier = rate;
          var slider = document.getElementById("price-slider");
          if (searchResults.resMinPrice != searchResults.resMaxPrice) {
            slider.noUiSlider.set([
              parseFloat(searchResults.resMinPrice),
              parseFloat(searchResults.resMaxPrice),
            ]);
            slider.noUiSlider.updateOptions({
              range: {
                min: parseFloat(searchResults.resMinPrice),
                max: parseFloat(searchResults.resMaxPrice),
              },
            });
            slider.noUiSlider.on("update", function (values, handle) {
              $("#minPricevalue").text(
                searchResults.$n(
                  parseFloat(values[0] / searchResults.CurrencyMultiplier),
                  "currency",
                  searchResults.selectedCurrency
                )
              );
              $("#MaxPricevalue").text(
                searchResults.$n(
                  parseFloat(values[1] / searchResults.CurrencyMultiplier),
                  "currency",
                  searchResults.selectedCurrency
                )
              );

              searchResults.changedMinPrice = parseFloat(values[0]);
              searchResults.changedMaxPrice = parseFloat(values[1]);
            });
          } else {
            searchResults.changedMinPrice = searchResults.resMinPrice;
            searchResults.changedMaxPrice = searchResults.resMaxPrice;
          }
          break;
        case "/Hotels/hotel-detail.html":
          store.state.selectedCurrency = code;
          store.state.currencyMultiplier = rate;
          break;
        case "/Flights/flight-confirmation.html":
          bookinginfo_vue.selectedCurrency = code;
          bookinginfo_vue.CurrencyMultiplier = rate;
          break;
        case "/Flights/flight-booking.html":
          Vue_FlightBook.selectedCurrency = code;
          Vue_FlightBook.CurrencyMultiplier = rate;
          Vue_FlightFare.selectedCurrency = code;
          Vue_FlightFare.CurrencyMultiplier = rate;
          Vue_FareUpdate.selectedCurrency = code;
          Vue_FareUpdate.CurrencyMultiplier = rate;
          break;

        case "/Yone/index.html":
          maininstance.selectedCurrency = code;
          maininstance.CurrencyMultiplier = rate;
          break;
        case "/Yone/":
          maininstance.selectedCurrency = code;
          maininstance.CurrencyMultiplier = rate;
          break;
        case "/Yone/package.html":
          packagelist.selectedCurrency = code;
          packagelist.CurrencyMultiplier = rate;
          break;
        case "/Yone/package-detail.html":
          packageView.selectedCurrency = code;
          packageView.CurrencyMultiplier = rate;
          break;

        default:
        // window.location.reload();
      }
    },
    dropdownChange: function (event) {
      var self = this;
      if (
        event != undefined &&
        event.target != undefined &&
        event.target.value != undefined
      ) {
        var value = JSON.parse(event.target.value);
        self.onSelected(value.value, value.rate);
      }
    },
  },
  watch: {
    currencies: function () {
      this.currencies.length == 0
        ? (this.showcurrncy = false)
        : this.currencies.length == 1
          ? (this.showcurrncy = false)
          : (this.showcurrncy = true);
    },
  },
});

var HeaderComponent = Vue.component("headeritem", {
  template: `
    <div>
    <a class="wp-btn"  :href="'https://api.whatsapp.com/send?phone='+TopSection.WhatsppNumber" target="_blank"><i class="fa fa-whatsapp"></i></a>
    <div class="vld-parent">
        <loading :active.sync="isLoading" :can-cancel="false" :is-full-page="fullPage"></loading>
    </div>
     <header>
        <div class="header-top">
            <div class="container relative">
          <div class="row">
             <div class="col-md-3 col-sm-3 col-xs-4">
                <div class="logo">
                   <a href="/Yone/index.html"><img :src="TopSection.Logo_246x85px" alt="logo"></a>
                </div>
             </div>
             <div class="col-md-9 col-sm-9 col-xs-8">
                <div class="call-to-action">
                   <div class="currency">
                      <form>
                      <yone-currency-select></yone-currency-select>
                      </form>
                   </div>
                   <a id="modal_retrieve" href="#modal" class="btn-clta" title="Retrieve the Bookings">{{TopSection.Booking_lookup_Label}}</a>
                   <div id="modal" class="popupContainer" style="display:none;">
                      <header class="popupHeader"> <span class="header_title">{{TopSection.Booking_lookup_Label}}</span> <span
                            class="modal_close"><i class="fa fa-times"></i></span> </header>
                      <section class="popupBody">
                         <!-- User Login -->
                         <div class="user_login">
                            <div>
                               <label>{{TopSection.Enter_Email_Address_Label}}</label>
                               <div class="validation_sec">
                               <input v-model="retrieveEmailId" type="text" id="text" name="text" :placeholder="TopSection.Email_Address_Placeholder" />
                               <span v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveEmailErormsg.empty||retrieveEmailErormsg.invalid }"  class="cd-signin-modal__error sp_validation">{{retrieveEmailErormsg.empty?getValidationMsgByCode('M02'):(retrieveEmailErormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                               </div>
                               <small>{{TopSection.Email_Address_Description}}</small>
                               <label>{{TopSection.ID_Number_Label}}</label>
                               <div class="validation_sec">
                               <input v-model="retrieveBookRefid" type="text" id="text" name="text" :placeholder="TopSection.ID_Number_Placeholder" />
                               <span v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveBkngRefErormsg }"  class="cd-signin-modal__error sp_validation">{{getValidationMsgByCode('M34')}}</span> </div>
                               <small>{{TopSection.ID_Number_Description}}</small>
                               <button type="submit" id="retrieve-booking" class="btn-blue"  v-on:click="retrieveBooking">{{TopSection.Continue_Button_Label}}</button>
                            </div>
                         </div>
                      </section>
                   </div>
                   <!--  Sign-in -->
                   <div class="popup">
                      <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#Signin" v-show="!userlogined">{{TopSection.Login_Label}}</button>
                      <button type="button" class="btn btn-info cr-btn btn-lg" data-toggle="modal"
                         data-target="#cr-account" v-show="!userlogined">{{TopSection.Sign_Up_Label}}</button>
                      <div class="modal modal-hd  fade" id="Signin" role="dialog">
                         <div class="modal-dialog">
                            <div class="modal-content">
                               <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                               </div>
                               <div class="modal-body">
                                  <div class="model-text">
                                     <h3>{{TopSection.Register_and_Sign_Title_Label}}<br><span>{{TopSection.Register_and_Sign_SubTitle_Label}}</span></h3>
                                     <ul>
                                        <li v-for="content in TopSection.Register_and_Sign_Content_List"><i class="fa  fa-check-square-o"></i>{{content.Name}}</li>
                                    
                                     </ul>
                                  </div>
                                  <div class="model-form">
                                     <h3>{{TopSection.Login_Label}}</h3>
                                     <hr>
                                     <form>
                                     <div class="validation_sec">
                                        <input type="email" id="email" name="email" class="form-control"
                                           :placeholder="TopSection.Email_Address_Placeholder" required v-model="username">
                                           <span class="cd-signin-modal__error sp_validation" v-bind:class="{ 'cd-signin-modal__error--is-visible': usererrormsg.empty||usererrormsg.invalid }">{{usererrormsg.empty?getValidationMsgByCode('M02'):(usererrormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                                       </div>
                                       <div class="validation_sec">
                                           <input type="password" id="password" :placeholder="TopSection.Password_Placeholder"
                                           class="form-control" required v-model="password">
                                          
                                           <span class="cd-signin-modal__error sp_validation" v-bind:class="{ 'cd-signin-modal__error--is-visible': psserrormsg }">
                                           {{getValidationMsgByCode('M34')}}</span>
                                           </div>
                                        <input type="submit" id="submit" name="submit" :value="TopSection.Login_Label" v-on:click.prevent="loginaction">
                                     </form>
                                     <a class="forgot-pass" href="#" data-toggle="modal"
                                        data-target="#forgot-pass" data-dismiss="modal">{{TopSection.Forgot_your_password_Label}}</a>
                                     <p class="cr-ac">{{TopSection.Do_not_Have_An_Account_Label}} <a href="#" data-toggle="modal" data-target="#cr-account" data-dismiss="modal">{{TopSection.Sign_Up_Label}}</a></p>
                                  </div>
                               </div>
                            </div>
                         </div>
                      </div>
                      <div class="modal fade" id="forgot-pass" role="dialog">
                         <div class="modal-dialog">
                            <div class="modal-content">
                               <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                               </div>
                               <div class="modal-body">
                                  <div class="model-text">
                                     <h3>{{TopSection.Register_and_Sign_Title_Label}}<br><span>{{TopSection.Register_and_Sign_SubTitle_Label}}</span></h3>
                                     <ul>
                                     <li v-for="content in TopSection.Register_and_Sign_Content_List"><i class="fa  fa-check-square-o"></i>{{content.Name}}</li>
                                    
                                     </ul>
                                  </div>
                                  <div class="model-form">
                                     <h3>{{TopSection.Forgot_your_password_Label}}</h3>
                                     <hr>
                                     <p v-html="TopSection.Enter_Email_to_Reset_Password_Label"></p>
                                     <form>
                                     <div class="validation_sec">
                                        <input type="email" id="email" name="email" class="form-control"
                                        :placeholder="TopSection.Email_Address_Placeholder" v-model="emailId">
                                        <span class="cd-signin-modal__error sp_validation" v-bind:class="{ 'cd-signin-modal__error--is-visible': userforgotErrormsg.empty||userforgotErrormsg.invalid }">
                                        {{userforgotErrormsg.empty?getValidationMsgByCode('M02'):(userforgotErrormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                                    </div>
                                        <input type="submit" id="submit" name="submit" :value="TopSection.Reset_Password_Label" v-on:click.prevent="forgotPassword">
                                     </form>
                                     <a class="back-login" href="#" data-toggle="modal" data-target="#Signin" data-dismiss="modal">{{TopSection.Back_To_Login_Label}}</a>
                                  </div>
                               </div>
                            </div>
                         </div>
                      </div>
                      <div class="modal fade" id="cr-account" role="dialog">
                         <div class="modal-dialog">
                            <div class="modal-content">
                               <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                               </div>
                               <div class="modal-body">
                                  <div class="model-text">
                                     <h3>{{TopSection.Register_and_Sign_Title_Label}}<br><span>{{TopSection.Register_and_Sign_SubTitle_Label}}</span></h3>
                                     <ul>
                                     <li v-for="content in TopSection.Register_and_Sign_Content_List"><i class="fa  fa-check-square-o"></i>{{content.Name}}</li>
                                    
                                     </ul>
                                  </div>
                                  <div class="model-form">
                                     <h3>{{TopSection.Sign_Up_Label}}</h3>
                                     <hr>
                                     <form>
                                     <div class="validation_sec" style="z-index:999;">
                                        <select v-model="registerUserData.title">
                                            <option v-for="titlelist in TopSection.Title_List">{{titlelist.Title}}</option>
                                        </select>
                                    </div>
                                    <div class="validation_sec">
                                        <input type="text" id="text" name="text" class="form-control name-se"
                                           :placeholder="TopSection.First_Name_Label" v-model="registerUserData.firstName">
                                           <span class="cd-signin-modal__error sp_validation" v-bind:class="{ 'cd-signin-modal__error--is-visible': userFirstNameErormsg }">
                                           {{getValidationMsgByCode('M35')}}</span>
                                    </div>
                                    <div class="validation_sec">
                                        <input type="text" id="text" name="text" class="form-control name-se"
                                           :placeholder="TopSection.Last_Name_Label" v-model="registerUserData.lastName">
                                           <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userLastNameErrormsg }">{{getValidationMsgByCode('M36')}}</span>
                                    </div>
                                    <div class="validation_sec">
                                        <input type="email" id="email" name="email" class="form-control"
                                        :placeholder="TopSection.Email_Address_Placeholder" v-model="registerUserData.emailId">
                                        <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userEmailErormsg.empty||userEmailErormsg.invalid }">{{userEmailErormsg.empty?getValidationMsgByCode('M02'):(userEmailErormsg.invalid?getValidationMsgByCode('M03'):'')}}</span>
                                    </div>
                                        <input type="checkbox" id="checkbox" v-model="isChecked"> {{TopSection.Send_me_Offers_and_deals_Label}}
                                        <input type="submit" id="submit" name="submit" :value="TopSection.Sign_Up_Label" v-on:click.prevent="registerUser">
                                     </form>
                                     <p class="cr-ac">{{TopSection.Already_have_an_account_Label}} <a href="#" data-toggle="modal" data-target="#Signin" data-dismiss="modal">{{TopSection.Login_Label}}</a></p>
                                  </div>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                   <!--dashbo-profile-->
                  <div class="profile-tab" v-show="userlogined">
                         <div class="dropdown">

                         <!--  Log in with updated profile -->
                         <a v-if="userinfo && userinfo.title && userinfo.title.id" class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">     
                         <img class="profile-user" v-if="[2,3,4].indexOf(userinfo.title.id)<0" src="/assets/images/user.png" alt="user">
                         <img class="profile-user" v-else src="/assets/images/user_lady.png" alt="user">
                         {{userinfo.firstName}} <i class="fa fa-angle-down"></i>
                         </a>
                         <!--  Log in with as guest -->
                         <a v-else class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">     
                         <img class="profile-user" src="/assets/images/user.png" alt="user">
                         {{userinfo.firstName}} <i class="fa fa-angle-down"></i>
                         </a>
                         
                         <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                         <a class="dropdown-item" href="/customer-profile.html"><i class="fa fa-dashboard"></i>{{TopSection.Dashboard_Label}}</a>
                         <a class="dropdown-item" href="/my-profile.html"><i class="fa fa-user"></i>{{TopSection.Profile_Label}}</a>
                         <a class="dropdown-item" href="/my-bookings.html"><i class="fa  fa-file-text-o"></i> {{TopSection.Booking_Label}}</a>
                         <a class="dropdown-item" href="#" v-on:click.prevent="logout"> <i class="fa  fa-power-off"></i>{{TopSection.Logout_Label}}</a>
                         </div>
                         </div>
                             </div>
                </div>
             </div>
          </div>
       </div>
    </div>
    <!--Navigation-->
    <div class="header-bottom">
       <div class="container">
          <div class="row">
             <div class="col-md-12 col-sm-12">
                <nav id="navbar-main" class="navbar navbar-default">
                   <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                         <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                      </button>
                   </div>
                   <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                      <ul class="nav navbar-nav">
                         <li :class="{ 'active': activeClass === '/index.html' || activeClass === '/'}"><a href="/Yone/index.html">{{topSectionLabel.Home_Label}}</a></li>
                         <li :class="{ 'active': activeClass === '/about.html'}"><a href="/Yone/about.html">{{topSectionLabel.About_Us_Label}}</a></li>
                         <li :class="{ 'active': activeClass === '/contact.html'}"><a href="/Yone/contact.html">{{topSectionLabel.Contact_Us_Label}}</a></li>
                      </ul>
                   </div>
                </nav>
             </div>
          </div>
       </div>
    </div>
    </header> 
    </div>
`,
  data() {
    return {
      language: "en",
      active_el: sessionStorage.active_el ? sessionStorage.active_el : 0,
      isChecked: false,
      TopSection: {},
      validationList: {},
      isLoading: false,
      fullPage: true,
      userlogined: this.checklogin(),
      userlogined: "",
      userinfo: [],
      username: "",
      password: "",
      emailId: "",
      retrieveEmailId: "",
      retrieveBookRefid: "",
      retrieveEmailErormsg: {
        empty: false,
        invalid: false,
      },
      psserrormsg: false,
      userFirstNameErormsg: false,
      userLastNameErrormsg: false,
      userEmailErormsg: {
        empty: false,
        invalid: false,
      },
      userPasswordErormsg: false,
      userVerPasswordErormsg: false,
      userPwdMisMatcherrormsg: false,
      userTerms: false,
      userforgotErrormsg: {
        empty: false,
        invalid: false,
      },

      usererrormsg: {
        empty: false,
        invalid: false,
      },
      retrieveBkngRefErormsg: false,
      registerUserData: {
        firstName: "",
        lastName: "",
        emailId: "",
        password: "",
        verifyPassword: "",
        terms: "",
        title: "Mr",
      },
      sessionids: "",
      Ipaddress: "",
      DeviceInfo: "",
      pageType: "",
      PageName: "",
      activeClass: "",
      topSectionLabel: {},
    };
  },
  created() {
    var self = this;
    var pathArray = window.location.pathname.split("/");
    self.activeClass = "/" + pathArray[pathArray.length - 1];
  },
  computed: {
    checkMobileOrNot: function () {
      if (screen.width <= 1020) {
        return true;
      } else {
        return false;
      }
    },
  },
  methods: {
    activate: function (el) {
      sessionStorage.active_el = el;
      this.active_el = el;
      if (el == 1 || el == 2) {
        maininstance.actvetab = el;
      } else {
        maininstance.actvetab = 0;
      }
    },
    getpagecontent: function () {
      var self = this;
      self.isLoading = true;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = localStorage.Languagecode
          ? localStorage.Languagecode
          : "en";
        var pageurl =
          huburl +
          portno +
          "/persons/source?path=/B2B/AdminPanel/CMS/" +
          Agencycode +
          "/Template/Master/Master/Master.ftl";
        axios
          .get(pageurl, {
            headers: {
              "content-type": "text/html",
              Accept: "text/html",
              "Accept-Language": langauage,
            },
          })
          .then(function (response) {
            if (response.data.area_List.length) {
              var headerSection = pluck(
                "Header_Section",
                response.data.area_List
              );
              self.TopSection = getAllMapData(headerSection[0].component);

              var Footer = pluck("FooterSection", response.data.area_List);
              self.topSectionLabel = getAllMapData(Footer[0].component);
            }
            self.isLoading = false;
          })
          .catch(function (error) {
            self.isLoading = false;
            console.log("Error");
          });
      });
    },
    getValidationMsgs: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = localStorage.Languagecode
          ? localStorage.Languagecode
          : "en";
        var pageurl =
          huburl +
          portno +
          "/persons/source?path=/B2B/AdminPanel/CMS/" +
          Agencycode +
          "/Template/Validation/Validation/Validation.ftl";
        axios
          .get(pageurl, {
            headers: {
              "content-type": "text/html",
              Accept: "text/html",
              "Accept-Language": langauage,
            },
          })
          .then(function (response) {
            if (response.data.area_List.length) {
              var validations = pluck("Validations", response.data.area_List);
              self.validationList = getAllMapData(validations[0].component);
              sessionStorage.setItem(
                "validationItems",
                JSON.stringify(self.validationList)
              );
            }
          })
          .catch(function (error) {
            console.log("Error");
          });
      });
    },
    loginaction: function () {
      if (!this.username.trim()) {
        this.usererrormsg = {
          empty: true,
          invalid: false,
        };
        return false;
      } else if (!this.validEmail(this.username.trim())) {
        this.usererrormsg = {
          empty: false,
          invalid: true,
        };
        return false;
      } else {
        this.usererrormsg = {
          empty: false,
          invalid: false,
        };
      }
      if (!this.password) {
        this.psserrormsg = true;
        return false;
      } else {
        this.psserrormsg = false;
        var self = this;
        self.isLoading = true;
        login(this.username, this.password, function (response) {
          if (response == false) {
            self.userlogined = false;
            self.isLoading = false;
            alertify.alert(
              this.getValidationMsgByCode("M07"),
              this.getValidationMsgByCode("M31")
            );
          } else {
            self.userlogined = true;
            self.userinfo = JSON.parse(localStorage.User);
            $("#Signin").modal("toggle");

            try {
              self.$eventHub.$emit("logged-in", {
                userName: self.username,
                password: self.password,
              });
              signArea.headerLogin({
                userName: self.username,
                password: self.password,
              });
              // self.username ='';
              // self.password = '';
              self.isLoading = false;
            } catch (error) {
              self.isLoading = false;
            }
          }
        });
      }
    },
    validEmail: function (email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },
    checklogin: function () {
      if (localStorage.IsLogin == "true") {
        this.userlogined = true;
        this.userinfo = JSON.parse(localStorage.User);
        return true;
      } else {
        this.userlogined = false;
        return false;
      }
    },
    logout: function () {
      this.userlogined = false;
      this.userinfo = [];
      localStorage.profileUpdated = false;
      try {
        this.$eventHub.$emit("logged-out");
        signArea.logout();
      } catch (error) { }
      commonlogin();
      // signOut();
      // signOutFb();
    },
    registerUser: function () {
      if (this.registerUserData.firstName.trim() == "") {
        this.userFirstNameErormsg = true;
        return false;
      } else {
        this.userFirstNameErormsg = false;
      }
      if (this.registerUserData.lastName.trim() == "") {
        this.userLastNameErrormsg = true;
        return false;
      } else {
        this.userLastNameErrormsg = false;
      }
      if (this.registerUserData.emailId == "") {
        this.userEmailErormsg = { empty: true, invalid: false };
        return false;
      } else if (!this.validEmail(this.registerUserData.emailId)) {
        this.userEmailErormsg = { empty: false, invalid: true };
        return false;
      } else {
        this.userEmailErormsg = { empty: false, invalid: false };
      }
      var vm = this;
      vm.isLoading = true;
      registerUser(
        this.registerUserData.emailId,
        this.registerUserData.firstName,
        this.registerUserData.lastName,
        this.registerUserData.title,
        function (response) {
          if (response.isSuccess == true) {
            try {
              if (this.isChecked) {
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format(
                  "YYYY-MM-DDThh:mm:ss"
                );
                let insertSubscibeData = {
                  type: "Newsletter",
                  date1: requestedDate,
                  keyword2: this.registerUserData.emailId,
                  nodeCode: agencyCode,
                };

                let responseObject = cmsRequestData(
                  "POST",
                  "cms/data",
                  insertSubscibeData,
                  null
                );
                this.registerUserData.firstName = "";
                this.registerUserData.emailId = "";
                this.registerUserData.title = "";
              }
            } catch (error) {
              vm.isLoading = false;
            }
            vm.username = response.data.data.user.loginId;
            vm.password = response.data.data.user.password;
            login(vm.username, vm.password, function (response) {
              if (response != false) {
                //  $('#Signin').modal('toggle');

                //  $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                window.location.href =
                  "/edit-my-profile.html?edit-profile=true";
                vm.isLoading = false;
              }
            });
          }
          vm.isLoading = false;
        }
      );
    },
    retrieveBooking: function () {
      if (this.retrieveEmailId.trim() == "") {
        //alert('Email required !');

        this.retrieveEmailErormsg = {
          empty: true,
          invalid: false,
        };
        return false;
      } else if (!this.validEmail(this.retrieveEmailId)) {
        //alert('Invalid Email !');
        this.retrieveEmailErormsg = {
          empty: false,
          invalid: true,
        };
        return false;
      } else {
        this.retrieveEmailErormsg = {
          empty: false,
          invalid: false,
        };
      }
      if (this.retrieveBookRefid == "") {
        this.retrieveBkngRefErormsg = true;
        return false;
      } else {
        this.retrieveBkngRefErormsg = false;
      }
      this.isLoading = true;
      var bookData = {
        BkngRefID: this.retrieveBookRefid,
        emailId: this.retrieveEmailId,
        redirectFrom: "retrievebooking",
        isMailsend: false,
      };
      localStorage.bookData = JSON.stringify(bookData);
      this.isLoading = false;
      window.location.href = "/Flights/flight-confirmation.html";
    },

    forgotPassword: function () {
      if (this.emailId.trim() == "") {
        this.userforgotErrormsg = {
          empty: true,
          invalid: false,
        };
        return false;
      } else if (!this.validEmail(this.emailId.trim())) {
        this.userforgotErrormsg = {
          empty: false,
          invalid: true,
        };
        return false;
      } else {
        this.userforgotErrormsg = {
          empty: false,
          invalid: false,
        };
      }
      var self = this;
      self.isLoading = true;
      var datas = {
        emailId: this.emailId,
        agencyCode: localStorage.AgencyCode,
        logoUrl:
          window.location.origin +
          "/" +
          localStorage.AgencyFolderName +
          "/website-informations/logo/logo.png",
        websiteUrl: window.location.origin,
        resetUri:
          window.location.origin +
          "/" +
          localStorage.AgencyFolderName +
          "/reset-password.html",
      };
      $(".cd-signin-modal__input[type=submit]").css(
        "cssText",
        "pointer-events:none;background:grey !important;"
      );

      var huburl = ServiceUrls.hubConnection.baseUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var requrl = ServiceUrls.hubConnection.hubServices.forgotPasswordUrl;

      axios
        .post(huburl + portno + requrl, datas)
        .then((response) => {
          if (response.data != "") {
            alertify.alert(this.getValidationMsgByCode("M07"), response.data);
            this.emailId = "";
          } else {
            alertify.alert(
              this.getValidationMsgByCode("M07"),
              this.getValidationMsgByCode("M32")
            );
          }
          $(".cd-signin-modal__input[type=submit]").css(
            "cssText",
            "pointer-events:auto;background:var(--primary-color) !important"
          );
          self.isLoading = false;
          $("#forgot-pass").modal("toggle");
        })
        .catch((err) => {
          console.log("FORGOT PASSWORD  ERROR: ", err);
          if (
            err.response.data.message ==
            "No User is registered with this emailId."
          ) {
            alertify.alert(
              this.getValidationMsgByCode("M07"),
              err.response.data.message
            );
          } else {
            alertify.alert(
              this.getValidationMsgByCode("M07"),
              this.getValidationMsgByCode("M33")
            );
          }
          $(".cd-signin-modal__input[type=submit]").css(
            "cssText",
            "pointer-events:auto;background:var(--primary-color) !important"
          );

          self.isLoading = false;
        });
    },
    Getvisit: function () {
      var self = this;
      var currentUrl = window.location.pathname;
      if (currentUrl == "/Yone/index.html" || currentUrl == "/Yone/") {
        self.pageType = "Master";
        self.PageName = "Home";
      } else if (currentUrl == "/Yone/package-detail.html") {
        self.pageType = "Package details";
        self.PageName = packageView.mainContent.Package_Name;
      } else {
        currentUrl = currentUrl.split("/Yone/")[1];
        currentUrl = currentUrl.split(".html")[0];
        self.PageName = currentUrl;
        self.pageType = "Master";
      }

      var TempDeviceInfo = detect.parse(navigator.userAgent);
      self.DeviceInfo = TempDeviceInfo;
      this.showYourIp();
    },
    showYourIp: function () {
      var self = this;
      var ipUrl = ServiceUrls.externalServiceUrl.ipAddressApi;
      fetch(ipUrl)
        .then((x) => x.json())
        .then(({ ip }) => {
          let Ipaddress = ip;
          let sessionids = ip.replace(/\./g, "");
          this.sendip(Ipaddress, sessionids);
        });
    },
    sendip: async function (Ipaddress, sessionids) {
      var self = this;
      if (
        self.PageName == null ||
        self.PageName == undefined ||
        self.PageName == ""
      ) {
        var pathArray = window.location.pathname.split("/");
        var activePage = pathArray[pathArray.length - 1];
      }
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
      let requestedDate = moment(String(new Date())).format(
        "YYYY-MM-DDThh:mm:ss"
      );
      let insertVisitData = {
        type: "Visit",
        keyword1: sessionids,
        ip1: Ipaddress,
        keyword2: self.pageType,
        keyword3: self.PageName,
        keyword4: localStorage.selectedCurrency
          ? localStorage.selectedCurrency
          : "AED",
        keyword5: localStorage.Languagecode ? localStorage.Languagecode : "en",
        text1: self.DeviceInfo.device.type,
        text2: self.DeviceInfo.os.name,
        text3: self.DeviceInfo.browser.name,
        date1: requestedDate,
        nodeCode: agencyCode,
      };
      cmsRequestData("POST", "cms/data", insertVisitData, null);
    },
    getValidationMsgByCode: function (code) {
      if (sessionStorage.validationItems !== undefined) {
        var validationList = JSON.parse(sessionStorage.validationItems);
        for (let validationItem of validationList.Validation_List) {
          if (code === validationItem.Code) {
            return validationItem.Message;
          }
        }
      }
    },
  },

  mounted: function () {
    $("#modal_retrieve").leanModal({
      top: 100,
      overlay: 0.6,
      closeButton: ".modal_close",
    });
    $(function () {
      $(".selectpicker").selectpicker();
    });
    if (localStorage.IsLogin == "true") {
      this.userlogined = true;
      this.userinfo = JSON.parse(localStorage.User);
    } else {
      this.userlogined = false;
    }
    this.getpagecontent();
    this.getValidationMsgs();
    setTimeout(() => {
      this.Getvisit();
    }, 1000);
  },
  watch: {
    updatelogin: function () {
      if (localStorage.IsLogin == "true") {
        this.userlogined = true;
        this.userinfo = JSON.parse(localStorage.User);
      } else {
        this.userlogined = false;
      }
    },
  },
});

var headerinstance = new Vue({
  el: "header",
  name: "headerArea",
  data() {
    return {
      key: "",
      content: null,
      getdata: true,
    };
  },
});
Vue.component("footeritem", {
  template: `
    <!-- Newsletter -->
    <div class="yone">
    <div class="vld-parent">
    <loading :active.sync="isLoading" :can-cancel="false" :is-full-page="fullPage"></loading>
</div>
    <section class="newsletter-in mobile-view" :style="{ background: 'url(' + NewsLetter.Newsletter_Image_1920x525_px+ ')'}">
       <div class="container">
          <div class="newsletter">
             <div class="news-text">
                <h3 v-bind:style="{ color: NewsLetter.Title_And_Description_Font_Color}">{{NewsLetter.Title}}</h3>
                <p v-bind:style="{ color: NewsLetter.Title_And_Description_Font_Color}" v-html="NewsLetter.Newsletter_Description">
                </p>
             </div>
             <div class="input-form">
                <input type="email" id="email" :placeholder="TopSection.Enter_Email_Address_Label" v-model="newsltremail">
                <button  @click.prevent="sendnewsletter">{{NewsLetter.Subscribe_Button_Label}}</button>
             </div>
             <div class="photographer-img">
                <img :src="NewsLetter.Image297x503px">
             </div>
          </div>
       </div>
    </section>
    <!-- End-Newsletter -->
       <div class="footer">
       <div class="container">
          <div class="row">
             <div class="footer-in">
                <div class="col-md-4 col-sm-4 footer-border">
                   <div class="about-footer">
                      <div class="ftr-logo"><a href="/Yone/index.html"><img :src="FooterSection.Logo_211x90px" alt="footer-logo"></a></div>
                      <p v-html="FooterSection.Description"></p>
                      <ul class="social">
                      <li v-for="social in FooterSection.Social_Media_List"><a :href="social.Url" target="_blank"><i :class="social.Icon"></i></a></li>
                        
                      </ul>
                   </div>
                </div>
                <div class="col-md-4 col-sm-4 footer-border">
                   <div class="contact-footer">
                      <h3>{{FooterSection.Contact_Info_Label}}</h3>
                      <ul>
                         <li>
                            <i :class="FooterSection.Address_Icon"></i>
                            <h4>{{FooterSection.Address_Label}}</h4>
                            <p v-html="FooterSection.Address_Description"></p>
                         </li>
                         <li>
                            <i :class="TopSection.Email_Icon"></i>
                            <h4>{{TopSection.Email_Address_Placeholder}}</h4>
                            <a :href="'mailto:'+FooterSection.Email_Id">{{FooterSection.Email_Id}}</a>
                         </li>
                         <li>
                            <i :class="TopSection.Phone_Icon"></i>
                            <h4>{{FooterSection.Phone_Number_Label}}</h4>
                            <a :href="'tel:'+FooterSection.Phone_Number"> {{FooterSection.Place_Name}} {{FooterSection.Phone_Number}} </a>
                         </li>
                      </ul>
                   </div>
                </div>
                <div class="col-md-4 col-sm-4 footer-border">
                   <div class="quick-link">
                      <h3>{{FooterSection.Quick_Links_Label}}</h3>
                      <ul>
                         <li><i class="fa   fa-dot-circle-o"></i><a href="/Yone/index.html">{{FooterSection.Home_Label}}</a></li>
                         <li><i class="fa   fa-dot-circle-o"></i><a href="/Yone/about.html">{{FooterSection.About_Us_Label}}</a></li>
                         <li><i class="fa   fa-dot-circle-o"></i><a href="/Yone/package.html">{{FooterSection.Holiday_Package_Label}}</a></li>
                         <li><i class="fa   fa-dot-circle-o"></i><a href="/Yone/contact.html">{{FooterSection.Contact_Us_Label}}</a></li>
                         <li><i class="fa   fa-dot-circle-o"></i><a href="/Yone/termsandconditons.html">{{FooterSection.Terms_of_Use_Label}}</a></li>
                         <li><i class="fa   fa-dot-circle-o"></i><a href="/Yone/privacypolicy.html">{{FooterSection.Privacy_Policy_Label}}</a></li>
                      </ul>
                   </div>
                </div>
             </div>
             <div class="ftr-btm">
                <div class="col-lg-6 col-md-6 col-sm-6">
                   <div class="copyright">{{FooterSection.Copyright_Description}}</div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-4">
                   <div class="powered-by">{{FooterSection.Powered_By_Label}} &nbsp; <a href="http://www.oneviewit.com/" target="_blank"><img
                            alt="" :src="FooterSection.Copyright_Logo95x34px"></a></div>
                </div>
             </div>
          </div>
       </div>
      </div>
    </div>
    `,
  data() {
    return {
      TopSection: {},
      FooterSection: {},
      NewsLetter: {},
      isLoading: false,
      fullPage: true,
      newsltremail: null,
    };
  },
  computed: {
    checkMobileOrNot: function () {
      if (screen.width <= 1020) {
        return true;
      } else {
        return false;
      }
    },
  },
  methods: {
    getpagecontent: function () {
      var self = this;
      self.isLoading = true;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = localStorage.Languagecode
          ? localStorage.Languagecode
          : "en";
        var pageurl =
          huburl +
          portno +
          "/persons/source?path=/B2B/AdminPanel/CMS/" +
          Agencycode +
          "/Template/Master/Master/Master.ftl";

        axios
          .get(pageurl, {
            headers: {
              "content-type": "text/html",
              Accept: "text/html",
              "Accept-Language": langauage,
            },
          })
          .then(function (response) {
            if (response.data.area_List.length) {
              var headerSection = pluck(
                "Header_Section",
                response.data.area_List
              );
              self.TopSection = getAllMapData(headerSection[0].component);

              var FooterContent = pluck(
                "FooterSection",
                response.data.area_List
              );
              self.FooterSection = getAllMapData(FooterContent[0].component);

              var news = pluck("NewsLetter_Section", response.data.area_List);
              self.NewsLetter = getAllMapData(news[0].component);

              self.isLoading = false;
            }
          })
          .catch(function (error) {
            console.log("Error");
            self.isLoading = false;
          });
      });
    },
    getValidationMsgByCode: function (code) {
      if (sessionStorage.validationItems !== undefined) {
        var validationList = JSON.parse(sessionStorage.validationItems);
        for (let validationItem of validationList.Validation_List) {
          if (code === validationItem.Code) {
            return validationItem.Message;
          }
        }
      }
    },
    sendnewsletter: async function () {
      if (!this.newsltremail) {
        alertify
          .alert(
            this.getValidationMsgByCode("M07"),
            this.getValidationMsgByCode("M02")
          )
          .set("closable", false);
        return false;
      }

      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.newsltremail.match(emailPat);
      if (matchArray == null) {
        alertify
          .alert(
            this.getValidationMsgByCode("M07"),
            this.getValidationMsgByCode("M03")
          )
          .set("closable", false);
        return false;
      } else {
        try {
          this.isLoading = true;
          this.agencyCode = JSON.parse(localStorage.User).loginNode.code || "";
          var agencyCode = this.agencyCode;
          var filterValue =
            "type='Newsletter' AND keyword2='" + this.newsltremail + "'";
          var allDBData = await this.getDbData4Table(
            this.agencyCode,
            filterValue,
            "date1"
          );
          if (
            allDBData != undefined &&
            allDBData.data != undefined &&
            allDBData.count > 0
          ) {
            alertify
              .alert(
                this.getValidationMsgByCode("M07"),
                this.getValidationMsgByCode("M04")
              )
              .set("closable", false);
            this.isLoading = false;
            console.log(allDBData);
            return false;
          } else {
            try {
              // var frommail = JSON.parse(localStorage.User).loginNode.email;
              // var frommail = "itsolutionsoneview@gmail.com";
              // var toEmail = JSON.parse(localStorage.User).loginNode.email;
              var toEmail = this.newsltremail;
              var frommail = JSON.parse(localStorage.User).loginNode.email;
              // var frommail = " itsolutionsoneview@gmail.com";
              var postData = {
                type: "AdminContactUsRequest",
                fromEmail: frommail,
                toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                logo:
                  ServiceUrls.hubConnection.logoBaseUrl +
                  JSON.parse(localStorage.User).loginNode.logo +
                  ".xhtml?ln=logo",
                agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                agencyAddress:
                  JSON.parse(localStorage.User).loginNode.address || "",
                personName: this.newsltremail.split("@")[0],
                emailId: this.newsltremail,
                //  contact: "9078978967",
                //  message: "hello",
              };
              var custmail = {
                type: "UserAddedRequest",
                fromEmail: frommail,
                toEmails: Array.isArray(this.newsltremail)
                  ? this.newsltremail
                  : [this.newsltremail],
                logo: JSON.parse(localStorage.User).loginNode.logo,
                agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                agencyAddress:
                  JSON.parse(localStorage.User).loginNode.address || "",
                personName: this.newsltremail.split("@")[0],
                primaryColor: "#ffffff",
                // primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                secondaryColor:
                  "#" +
                  JSON.parse(localStorage.User).loginNode.lookNfeel.secondary,
              };
              var agencyCode = this.agencyCode;
              let requestedDate = moment(String(new Date())).format(
                "YYYY-MM-DDThh:mm:ss"
              );
              let insertSubscibeData = {
                type: "Newsletter",
                date1: requestedDate,
                keyword2: this.newsltremail,
                nodeCode: agencyCode,
              };
              let responseObject = await cmsRequestData(
                "POST",
                "cms/data",
                insertSubscibeData,
                null
              );
              try {
                let insertID = Number(responseObject);
                this.newsltremail = "";
                this.newsltrname = "";
                var emailApi = ServiceUrls.emailServices.emailApi;
                sendMailService(emailApi, custmail);
                sendMailService(emailApi, postData);
                alertify
                  .alert(
                    this.getValidationMsgByCode("M15"),
                    this.getValidationMsgByCode("M23")
                  )
                  .set("closable", false);
                this.isLoading = false;
              } catch (e) {
                this.isLoading = false;
              }
            } catch (error) {
              this.isLoading = false;
            }
          }
        } catch (error) {
          this.isLoading = false;
        }
      }
    },
    async getDbData4Table(agencyCode, extraFilter, sortField) {
      var allDBData = [];
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var cmsURL = huburl + portno + "/cms/data/search/byQuery";
      var queryStr =
        "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
      if (extraFilter != undefined && extraFilter != "") {
        queryStr = queryStr + " AND " + extraFilter;
      }
      var requestObject = {
        query: queryStr,
        sortField: sortField,
        from: 0,
        orderBy: "desc",
      };
      let responseObject = await cmsRequestData(
        "POST",
        "cms/data/search/byQuery",
        requestObject,
        null
      );
      if (responseObject != undefined && responseObject.data != undefined) {
        allDBData = responseObject;
      }
      return allDBData;
    },
  },

  mounted: function () {
    this.getpagecontent();
  },
});

var footerinstance = new Vue({
  el: "footer",
  name: "footerArea",
  data() {
    return {
      key: 0,
      content: null,
      getdata: true,
    };
  },
});
function currencySelect() {
  $(".selectpicker").selectpicker("refresh");
}
