/* Fixed Nav JS
============================================================== */
var affixElement = '#navbar-main';

$(affixElement).affix({
	offset: {
		// Distance of between element and top page
		top: function () {
			return (this.top = $(affixElement).offset().top)
		},
	}
});


/* Booking form JS
============================================================== */
$("ul.nav-tabs a").click(function (e) {
  e.preventDefault();  
    $(this).tab('show');
});

/*----------------------------------------------------*/
// Select2
/*----------------------------------------------------*/

jQuery(document).ready(function($){
    $('.myselect').select2({minimumResultsForSearch: Infinity,'width':'100%'});
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>');
});


/***************************************************************
		BEGIN: VARIOUS DATEPICKER & SPINNER INITIALIZATION
***************************************************************/
$(document).ready(function () {
	$( function() {
    var dateFormat = "dd/mm/yy",
      from = $( "#from, #from1, #from2, #from3, #from4, #from5, #from6" )
        .datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to, #to1, #to2, #to3, #to4, #to5, #to6" ).datepicker({
        minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
});


/* Number Spinner JS
============================================================== */ 
 $(document).ready(function() {
    var options = {
      maxValue: 10,
      minValue: -5,
      step: 0.131,
      inputWidth: 100,
      start: -2,
      plusClick: function(val) {
        console.log(val);
      },
      minusClick: function(val) {
        console.log(val);
      },
      exceptionFun: function(val) {
        console.log("excep: " + val);
      },
      valueChanged: function(val) {
        console.log('change: ' + val);
      }
    }
    $(".wan-spinner-1").WanSpinner(options);
    $(".wan-spinner-2").WanSpinner().css("");
	$(".wan-spinner-3").WanSpinner({inputWidth: 100}).css("border-color", "#C0392B");

  });

/* Back-to-top JS
============================================================== */ 
jQuery(window).scroll(function () {
	scrollToTop('show');
});

jQuery(document).ready(function () {
	scrollToTop('click');
});

/* Animated Function */
function scrollToTop(i) {
	if (i == 'show') {
		if (jQuery(this).scrollTop() != 0) {
			jQuery('#totop').fadeIn();
		} else {
			jQuery('#totop').fadeOut();
		}
	}
	if (i == 'click') {
		jQuery('#totop').click(function () {
			jQuery('body,html').animate({
				scrollTop: 0
			}, 1000);
			return false;
		});
	}
}