const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
// Vue.component('vue-multiselect', window.VueMultiselect.default);

var flightserchfromComponent = Vue.component('flightsearch', {
    data() {
        return {
            access_token: '',
            KeywordSearch: '',
            resultItemsarr: [],
            autoCompleteProgress: false,
            highlightIndex: 0
        }
    },
    props: {
        itemText: String,
        itemId: String,
        placeHolderText: String,
        returnValue: Boolean,
        id: {
            type: String,
            default: '',
            required: false
        },
        leg: Number,
        //KeywordSearch:String
    },

    template: `<div class="autocomplete">
        <input type="text" :placeholder="placeHolderText" :id="id" :data-aircode="KeywordSearch" autocomplete="off" 
        v-model="KeywordSearch" class="form-control" 
        :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !autoCompleteProgress  }" 
        @input="onSelectedAutoCompleteEvent(KeywordSearch)"
            @keydown.down="down"
            @keydown.up="up"           
            @keydown.esc="autoCompleteProgress=false"
            @keydown.enter="onSelected(resultItemsarr[highlightIndex])"
            @keydown.tab="tabclick(resultItemsarr[highlightIndex])"/>
        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>        
        <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length>0">
            <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item)">
                {{ item.label }}
            </li>
        </ul>
    </div>`,


    methods: {
        up: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex < this.resultItemsarr.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
            if (this.$refs.options[this.highlightIndex]) {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            }

        },
        onSelectedAutoCompleteEvent: _.debounce(async function (keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
                this.autoCompleteProgress = true;
                // self.resultItemsarr = await getFlightResultUsingElasticSearch(keywordEntered);
                var cities = [
                  {
                    code: "ADD",
                    label: "Addis Ababa",
                  },
                  {
                    code: "ADE",
                    label: "Aden Intl",
                  },
                  {
                    code: "ADJ",
                    label: "Amman Civil",
                  },
                  {
                    code: "AMM",
                    label: "Amman Queen Alia",
                  },
                  {
                    code: "ASM",
                    label: "Asmara International Airport",
                  },
                  {
                    code: "BGF",
                    label: "Bangui",
                  },
                  {
                    code: "CAI",
                    label: "Cairo Intl",
                  },
                  {
                    code: "DOG",
                    label: "Dongola",
                  },
                  {
                    code: "DOH",
                    label: "Doha",
                  },
                  {
                    code: "DXB",
                    label: "Dubai Intl",
                  },
                  {
                    code: "EBB",
                    label: "Entebbe/Kampala",
                  },
                  {
                    code: "ELQ",
                    label: "Gassim",
                  },
                  {
                    code: "GXF",
                    label: "Seiyun",
                  },
                  {
                    code: "JED",
                    label: "Jeddah",
                  },
                  {
                    code: "JUB",
                    label: "Juba",
                  },
                  {
                    code: "KAN",
                    label: "Kano Aminu Kano",
                  },
                  {
                    code: "KSL",
                    label: "Kassala",
                  },
                  {
                    code: "KWI",
                    label: "Kuwait Intl",
                  },
                  {
                    code: "MCT",
                    label: "Muscat",
                  },
                  {
                    code: "MED",
                    label: "Madinah Abdulaziz",
                  },
                  {
                    code: "MQX",
                    label: "Makale",
                  },
                  {
                    code: "NBO",
                    label: "Jomo Kenyatta International Airport",
                  },
                  {
                    code: "NDJ",
                    label: "N'Djamena",
                  },
                  {
                    code: "RUH",
                    label: "Riyadh",
                  },
                  {
                    code: "RSS",
                    label: "Roseires Damazin",
                  },
                  {
                    code: "WUU",
                    label: "Waw",
                  },
                  {
                    code: "WHF",
                    label: "Wadi Halfa",
                  },
                  {
                    code: "KDX",
                    label: "Kadugli",
                  },
                  {
                    code: "ELF",
                    label: "El Fasher",
                  },
                  {
                    code: "EGN",
                    label: "El Geneina",
                  },
                  {
                    code: "KRT",
                    label: "Khartoum",
                  },
                  {
                    code: "UYL",
                    label: "Nyala",
                  },
                  {
                    code: "PZU",
                    label: "Port Sudan",
                  },
                ];

                self.resultItemsarr = cities.filter(function(e) {
                    return e.code.toLowerCase().search(keywordEntered.toLowerCase()) != -1 || e.label.toLowerCase().search(keywordEntered.toLowerCase()) != -1;
                });

            } else {
                this.autoCompleteProgress = false;
                this.resultItemsarr = [];

            }
        }, 100),
        onSelected: function (item) {
            this.autoCompleteProgress = false
            this.KeywordSearch = item.label;
            this.resultItemsarr = [];
            var targetWhenClicked = $(event.target).parent().parent().find("input").attr("id");
            if (maininstance.triptype != 'M') {
                if (event.target.id == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
                    $('#Cityto1').focus();
                } else if (event.target.id == "Cityto1" || targetWhenClicked == "Cityto1") {
                    $('#deptDate01').focus();
                }
            } else {
                var eventTarget = event.target.id;
                $(document).ready(function () {
                    if (eventTarget == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
                        $('#Cityto1').focus();
                    } else if (eventTarget == "Cityto1" || targetWhenClicked == "Cityto1") {
                        $('#deptDate01').focus();
                    } else if (eventTarget == "DeparturefromLeg1" || targetWhenClicked == "DeparturefromLeg1") {
                        $('#ArrivalfromLeg1').focus();
                    } else if (eventTarget == "ArrivalfromLeg1" || targetWhenClicked == "ArrivalfromLeg1") {
                        $('#txtLeg1Date').focus();
                    } else if (eventTarget == "DeparturefromLeg2" || targetWhenClicked == "DeparturefromLeg2") {
                        $('#ArrivalfromLeg2').focus();
                    } else if (eventTarget == "ArrivalfromLeg2" || targetWhenClicked == "ArrivalfromLeg2") {
                        $('#txtLeg2Date').focus();
                    } else if (eventTarget == "DeparturefromLeg3" || targetWhenClicked == "DeparturefromLeg3") {
                        $('#ArrivalfromLeg3').focus();
                    } else if (eventTarget == "ArrivalfromLeg3" || targetWhenClicked == "ArrivalfromLeg3") {
                        $('#txtLeg3Date').focus();
                    } else if (eventTarget == "DeparturefromLeg4" || targetWhenClicked == "DeparturefromLeg4") {
                        $('#ArrivalfromLeg4').focus();
                    } else if (eventTarget == "ArrivalfromLeg4" || targetWhenClicked == "ArrivalfromLeg4") {
                        $('#txtLeg4Date').focus();
                    } else if (eventTarget == "DeparturefromLeg5" || targetWhenClicked == "DeparturefromLeg5") {
                        $('#ArrivalfromLeg5').focus();
                    } else if (eventTarget == "ArrivalfromLeg5" || targetWhenClicked == "ArrivalfromLeg5") {
                        $('#txtLeg5Date').focus();
                    } else if (eventTarget == "DeparturefromLeg6" || targetWhenClicked == "DeparturefromLeg6") {
                        $('#ArrivalfromLeg6').focus();
                    } else if (eventTarget == "ArrivalfromLeg6" || targetWhenClicked == "ArrivalfromLeg6") {
                        $('#txtLeg6Date').focus();
                    }
                });

            }

            this.$emit('air-search-completed', item.code, item.label, this.leg);
        },
        tabclick: function (item) {
            if (!item) {

            } else {
                this.onSelected(item);
            }
        }

    },
    watch: {
        returnValue: function () {
            this.KeywordSearch = this.itemText;
        }
    }

});



var maininstance = new Vue({
    el:'#home',
    name:'home',
    i18n,
    data() {
        return {
            triptype: "R",
            Flight_form_caption: "",
            Oneway: "",
            Round_trip: "",
            Multicity: "",
            Advance_search_label: "",
            preferred_airline_placeholder: "",
            Direct_flight_option_label: "",
            Flight_tab_name: "",
            Hotel_tab_name: "",
            Package_tab_name: "",
            CityFrom: '',
            CityTo: '',
            validationMessage: '',
            adtcount: 1,
            chdcount: 0,
            infcount: 0,
            preferAirline: '',
            returnValue: true,
            legcount: 2,
            legs: [],
            cityList: [],
            adt: "adult",
            selected_cabin: 'Y',
            selected_adult: 1,
            selected_child: 0,
            selected_infant: 0,
            totalAllowdPax: 9,
            arabic_dropdown: '',
            childLabel: "",
            childrenLabel: "",
            adultLabel: "",
            adultsLabel: "",
            ageLabel: "",
            agesLabel: "",
            infantLabel: "",
            searchBtnLabel: "",
            addUptoLabel: "",
            tripsLabel: "",
            tripLabel: "",
            Totaltravaller: '',
            travellerdisply: false,
            child: 0,
            flightSearchCityName: {
                cityFrom1: '',
                cityTo1: '',
                cityFrom2: '',
                cityTo2: '',
                cityFrom3: '',
                cityTo3: '',
                cityFrom4: '',
                cityTo4: '',
                cityFrom5: '',
                cityTo5: '',
                cityFrom6: '',
                cityTo6: ''
            },
            advncedsearch: false,
            isLoading: false,
            direct_flight: false,
            airlineList: AirlinesDatas,
            Airlineresults: [],
            selectedAirline: [],
            adultrange: '',
            childrange: '',
            infantrange: '',
            donelabel: '',
            classlabel: '',
            bnrcaption: false,
            selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
            CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),

            key: '',
            content: null,
            getdata: true,
            dir: 'ltr',
            ipAddres: '',
            sessionids: '',
            Socialmedia: {},
            isLoading: false,
            fullPage: true,
            mainData:{},
            newsltremail:'',
            packages:[],
            domesticList:[],
            internationalList:[],
            internationalList2:[],
            isShow:true,
        }
    },
    mounted: function () {
        this.setCalender();
        // this.setip();
        this.getcmsdata();
        this.holidayPackageCard();
        var vm = this;
        this.$nextTick(function () {
            $('#rcabindrp').on("change", function (e) {
                vm.dropdownChange(e, 'Cabin')
            });
            $('#cabindrp').on("change", function (e) {
                vm.dropdownChange(e, 'Cabin')
            });
        })
    },
    updated: function () {
        this.setCalender();
    },
    watch: {

        selectedAirline: function () {
            if (this.selectedAirline.length > 4) {
                alert("exceed");
                return false;
            } else {
                var airlines = [];
                if (this.selectedAirline.length > 0) {
                    this.selectedAirline.forEach(function (airline) {
                        airlines.push(airline.code);
                    });
                }

                return this.preferAirline = airlines.length > 0 ? airlines.join('|') : '';
            }
        }
    },
    methods: {
        moment: function () {
            return moment();
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getcmsdata: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Home/Home/Home.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    
                    // var backg = self.pluck('main', self.content.area_List);
                    // var background = self.pluckcom('Banner', backg[0].component);
                    // setTimeout(function () {
                    //     owlcarosl(background);
                    // }, 100);
                    var main = self.pluck('Main_Section', self.content.area_List);
                    //self.Socialmedia.Title = self.pluckcom('Title', Socialmedia[0].component);
                    self.mainData = getAllMapData( main[0].component);
                    self.Totaltravaller= "1 " +self.mainData.Passenger_Label;
                    if(mainData.length==0){isShow==false;}
                    self.isLoading = false;
                   
                    setTimeout(function () {
                        carousels()
                    }, 100);
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                    self.isLoading = false;
                });
            });
        },
        getValidationMsgByCode: function (code) {
            if (sessionStorage.validationItems !== undefined) {
              var validationList = JSON.parse(sessionStorage.validationItems);
              for (let validationItem of validationList.Validation_List) {
                if (code === validationItem.Code) {
                  return validationItem.Message;
                }
              }
            }
          },
          sendnewsletter: async function () {
            if (!this.newsltremail) {
              alertify
                .alert(
                  this.getValidationMsgByCode("M07"),
                  this.getValidationMsgByCode("M02")
                )
                .set("closable", false);
              return false;
            }
      
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
              alertify
                .alert(
                   this.getValidationMsgByCode("M07"),
                  this.getValidationMsgByCode("M03")
                )
                .set("closable", false);
              return false;
            } else {
              try {
                this.isLoading = true;
                this.agencyCode = JSON.parse(localStorage.User).loginNode.code || "";
                var agencyCode = this.agencyCode;
                var filterValue =
                  "type='Newsletter' AND keyword2='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(
                  this.agencyCode,
                  filterValue,
                  "date1"
                );
                if (
                  allDBData != undefined &&
                  allDBData.data != undefined &&
                  allDBData.count > 0
                ) {
                  alertify
                    .alert(
                      this.getValidationMsgByCode("M07"),
                      this.getValidationMsgByCode("M04")
                    )
                    .set("closable", false);
                  this.isLoading = false;
                  console.log(allDBData);
                  return false;
                } else {
                  try {
                   //var Logo=TopSection.Logo152x42px;
                   // var frommail = JSON.parse(localStorage.User).loginNode.email;
                    var frommail = "itsolutionsoneview@gmail.com";
                   // var toEmail = JSON.parse(localStorage.User).loginNode.email;
                   var toEmail = this.newsltremail;
                  //  var frommail=JSON.parse(localStorage.User).loginNode.email;
                    // var frommail = " itsolutionsoneview@gmail.com";
                    // var postData = {
                    //   type: "AdminContactUsRequest",
                    //   fromEmail: frommail,
                    //   toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                    //   logo:Logo||"",
                    //   agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    //   agencyAddress:
                    //     JSON.parse(localStorage.User).loginNode.address || "",
                    //   personName: this.newsltremail.split("@")[0],
                    //   emailId: this.newsltremail,
                    //    contact: "",
                    //    message: " ",
                    // };
                    // var custmail = {
                    //   type: "UserAddedRequest",
                    //   fromEmail: frommail,
                    //   toEmails: Array.isArray(this.newsltremail)
                    //     ? this.newsltremail
                    //     : [this.newsltremail],
                    // //  logo: JSON.parse(localStorage.User).loginNode.logo,
                    // logo:TopSection.Logo152x42px,
                    //   agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    //   agencyAddress:
                    //     JSON.parse(localStorage.User).loginNode.address || "",
                    //   personName: this.newsltremail.split("@")[0],
                    //   primaryColor: "#ffffff",
                    //   // primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    //   secondaryColor:
                    //     "#" +
                    //     JSON.parse(localStorage.User).loginNode.lookNfeel.secondary,
                    // };
                    var agencyCode = this.agencyCode;
                    let requestedDate = moment(String(new Date())).format(
                      "YYYY-MM-DDThh:mm:ss"
                    );
                    let insertSubscibeData = {
                      type: "Newsletter",
                      date1: requestedDate,
                      keyword2: this.newsltremail,
                      nodeCode: agencyCode,
                    };
                    let responseObject = await cmsRequestData(
                      "POST",
                      "cms/data",
                      insertSubscibeData,
                      null
                    );
                    try {
                      let insertID = Number(responseObject);
                      this.newsltremail = "";
                      //this.newsltrname = "";
                      var emailApi = ServiceUrls.emailServices.emailApi;
                    //   sendMailService(emailApi, custmail);
                    //   sendMailService(emailApi, postData);
                      alertify
                        .alert(
                          this.getValidationMsgByCode("M15"),
                          this.getValidationMsgByCode("M23")
                        )
                        .set("closable", false);
                      this.isLoading = false;
                    } catch (e) {
                      this.isLoading = false;
                    }
                  } catch (error) {
                    this.isLoading = false;
                  }
                }
              } catch (error) {
                this.isLoading = false;
              }
            }
          },
          async getDbData4Table(agencyCode, extraFilter, sortField) {
            var allDBData = [];
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + "/cms/data/search/byQuery";
            var queryStr =
              "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != "") {
              queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
              query: queryStr,
              sortField: sortField,
              from: 0,
              orderBy: "desc",
            };
            let responseObject = await cmsRequestData(
              "POST",
              "cms/data/search/byQuery",
              requestObject,
              null
            );
            if (responseObject != undefined && responseObject.data != undefined) {
              allDBData = responseObject;
            }
            console.log( allDBData);
            return allDBData;
          },   getValidationMsgs: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Validation/Validation/Validation.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    if (response.data.area_List.length) {
                        var validations = pluck('Validations', response.data.area_List);
                        self.validationList = getAllMapData(validations[0].component);
                        sessionStorage.setItem('validationItems', JSON.stringify(self.validationList));
                    }
                }).catch(function (error) {
                    console.log('Error');
                });
            });
        },
        holidayPackageCard: function () 
        {
            var self = this;
            self.getcmsdata();
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Packages/Packages/Packages.ftl';

                axios.get(topackageurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    var packageData = response.data;
                    let holidayaPackageListTemp = [];
                    var internationalList=[]
                    if (packageData != undefined && packageData.Values != undefined) {
                        holidayaPackageListTemp = packageData.Values.filter(function (el) {
                            return el.Is_Active == true && el.Show_In_Home_Page
                        });
                    }
                    self.packages = holidayaPackageListTemp;
                    self.packages.forEach(element => {
                    if(element.IS_Domestic_==true){self.domesticList.push(element);}
                    if (element.Is_International==true){
                     internationalList.push(element);
                     //var length=internationalList.length;
                    self.packages=internationalList[0];
                    self.internationalList=internationalList;
                    // self.internationalList=internationalList.slice(1,4);
                    // self.internationalList2=internationalList.splice(5,length);
                      }
                      if(self.internationalList.length==0||self.domesticList.length==0||self.internationalList2.length==0 ){self.isShow==false;}
                    else{}
                    });
                   
                    
                }).catch(function (error) {
                    console.log('Error');
                });
            });

        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json'
                },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        // getPackages(url) {
        //     debugger
        //     if (url != "") {
        //         url = '/TarcoAviation/packages.html?search=' + url;
        //         window.location.href = url;
        //     }
        //     else {
        //         url = "#";
        //     }
        //     return url;
        // },

        Departurefrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (AirportCode == this.CityTo) {
                    this.returnValue = false;
                    alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M11')).set('closable', false);
                    this.CityFrom = '';

                } else {
                    this.returnValue = true;
                    this.CityFrom = AirportCode;
                }
            } else {
                var from = 'cityFrom' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].to) {
                        this.returnValue = false;
                        this.flightSearchCityName[from] = "";
                        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M11')).set('closable', false);
                    } else {
                        this.cityList[index].from = AirportCode
                        this.flightSearchCityName[from] = AirportName;
                        this.returnValue = true;
                    }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: AirportCode,
                        to: ''

                    });
                    this.flightSearchCityName[from] = AirportName;
                    this.returnValue = true;
                }
            }


        },
        Arrivalfrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (this.CityFrom == AirportCode) {
                    this.returnValue = false;
                    alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M11')).set('closable', false);
                    this.validationMessage = "";
                    this.CityTo = '';

                } else {
                    this.returnValue = true;
                    this.CityTo = AirportCode;
                }
            } else {
                var to = 'cityTo' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].from) {
                        this.returnValue = false;
                        this.flightSearchCityName[to] = "";
                        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M11')).set('closable', false);

                    } else {
                        this.cityList[index].to = AirportCode;
                        this.flightSearchCityName[to] = AirportName;
                        this.returnValue = true;
                    }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: '',
                        to: AirportCode

                    });
                    this.flightSearchCityName[to] = AirportName;
                    this.returnValue = true;
                }
            }
        },
        SearchFlight: function () {
            if (this.triptype == 'R') {
                var Departuredate = $('#from1').val() == "" ? "" : $('#from1').datepicker('getDate');
            } else {
                var Departuredate = $('#deptDate01').val() == "" ? "" : $('#deptDate01').datepicker('getDate');
            }

            if (!this.CityFrom) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M14')).set('closable', false);
                return false;
            }
            if (!this.CityTo) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M08')).set('closable', false);
                return false;
            }
            if (!Departuredate) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M09')).set('closable', false);
                return false;
            }
            var sec1TravelDate = moment(Departuredate).format('DD|MM|YYYY');

            var sectors = this.CityFrom + '-' + this.CityTo + '-' + sec1TravelDate;
            if (this.triptype == 'R') {
                var ArrivalDate = $('#retDate').val() == "" ? "" : $('#retDate').datepicker('getDate');
                if (!isNullorUndefined(ArrivalDate)) {
                    ArrivalDate = moment(ArrivalDate).format('DD|MM|YYYY');
                    if (!ArrivalDate) {
                        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M10')).set('closable', false);
                        return false;
                    } else {
                        sectors += '/' + this.CityTo + '-' + this.CityFrom + '-' + ArrivalDate;
                    }
                } else {
                    alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M10')).set('closable', false);
                    return false;
                }
            }
            var directFlight = this.direct_flight ? 'DF' : 'AF';
            var adult = this.selected_adult;
            var child = this.selected_child;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            getSuppliers([this.CityFrom + '|' + this.CityTo],
            function(supp) {
                var searchUrl = '/Flights/flight-listing.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                // searchUrl = searchUrl.toLocaleLowerCase();
                window.location.href = searchUrl;
            })
        },
        SearchFlightRedirect: function () {
            if (this.triptype == 'R') {
                var Departuredate = $('#from1').val() == "" ? "" : $('#from1').datepicker('getDate');
            } else {
                var Departuredate = $('#deptDate01').val() == "" ? "" : $('#deptDate01').datepicker('getDate');
            }

            if (!this.CityFrom) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M14')).set('closable', false);
                return false;
            }
            if (!this.CityTo) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M08')).set('closable', false);
                return false;
            }
            if (!Departuredate) {
                alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M09')).set('closable', false);
                return false;
            }
            var sec1TravelDate = moment(Departuredate).format('YYYY-MM-DD');

            var baseUrl = "https://preprod4.ttinteractive.com/Zenith/FrontOffice/tarcoair/en-GB/BookingEngine/SearchResult?";
            var sectors = "OriginAirportCode=" + this.CityFrom + '&DestinationAirportCode=' + this.CityTo;
            if (this.triptype == 'R') {
                var ArrivalDate = $('#retDate').val() == "" ? "" : $('#retDate').datepicker('getDate');
                if (!isNullorUndefined(ArrivalDate)) {
                    ArrivalDate = moment(ArrivalDate).format('YYYY-MM-DD');
                    if (!ArrivalDate) {
                        alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M10')).set('closable', false);
                        return false;
                    } else {
                        var passengers = 
                        "&TravelerTypes[0].Value=" + this.selected_adult + 
                        "&TravelerTypes[1].Value=" + this.selected_child + 
                        "&TravelerTypes[2].Value=" + this.selected_infant +
                        "&TravelerTypes[0].Key=AD&TravelerTypes[1].Key=CHD&TravelerTypes[2].Key=INF";
                        var url = baseUrl + sectors + "&TripType=round-trip&OutboundDate=" + sec1TravelDate + "&InboundDate=" + ArrivalDate + passengers + "&Currency=SDG";
                    }
                } else {
                    alertify.alert(this.getValidationMsgByCode('M07'), this.getValidationMsgByCode('M10')).set('closable', false);
                    return false;
                }
            } else {
                var passengers = 
                "&TravelerTypes[0].Value=" + this.selected_adult + 
                "&TravelerTypes[1].Value=" + this.selected_child + 
                "&TravelerTypes[2].Value=" + this.selected_infant +
                "&TravelerTypes[0].Key=AD&TravelerTypes[1].Key=CHD&TravelerTypes[2].Key=INF";
                var url = baseUrl + sectors + "&TripType=one-way&OutboundDate=" + sec1TravelDate + passengers + "&Currency=SDG";
            }


            // var directFlight = this.direct_flight ? 'DF' : 'AF';
            // var adult = this.selected_adult;
            // var child = this.selected_child;
            // var infant = this.selected_infant;
            // var cabin = this.selected_cabin;
            // var tripType = this.triptype;
            // var preferAirline = this.preferAirline;
            // var searchUrl = '/Flights/flight-listing.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-all-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
            // searchUrl = searchUrl.toLocaleLowerCase();
            // window.location.href = searchUrl;
            window.location.href = url;
        },
        AddNewLeg() {
            if (this.legcount <= 5) {
                ++this.legcount;
                var legno = 1;
                if (this.cityList.length > 0) {
                    legno = Math.max.apply(Math, this.cityList.map(function (o) {
                        return o.id;
                    }));
                    legno = legno + 1;
                }

                this.cityList.push({
                    id: legno,
                    from: '',
                    to: ''
                });

            }
            console.log(this.cityList);
        },
        DeleteLeg(leg) {
            var legs = this.legcount;
            if (legs > 1) {
                --this.legcount;
                var legno = Math.max.apply(Math, this.cityList.map(function (o) {
                    return o.id;
                }));
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    this.cityList.splice(index, 1);

                }


            }
        },
        setCalender() {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = 1; //generalInformation.systemSettings.calendarDisplay;
            $("#deptDate01").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                onSelect: function (selectedDate) {
                    $("#retDate").datepicker("option", "minDate", selectedDate);

                }
            });
            $("#from1").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#retDate").datepicker("option", "minDate", selectedDate);

                }
            });

            $("#retDate").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#from1").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {}
            });
            $("#txtLeg1Date").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);

                }
            });
            $("#txtLeg2Date").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg1Date").val();
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg3Date").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg2Date").val();
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg4Date").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg3Date").val();
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg5Date").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg4Date").val();
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg6Date").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg5Date").val();
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {}
            });


        },
        onAdultChange: function () {
            alert(this.selected);
            //this.disp = this.selected;
        },
        onCheck: function (e) {
            var currid = $(e.target).attr('id');

            console.log(currid);
            var selectedDate = $("#deptDate01").val();
            $("#retDate").datepicker("option", "minDate", selectedDate);
            $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
        },
        // getmoreinfo(url) {
        //     if (url != null) {
        //         if (url != "") {
        //             url = url.split("/Template/")[1];
        //             url = url.split(' ').join('-');
        //             url = url.split('.').slice(0, -1).join('.')
        //             url = "/T/holiday-detail.html?page=" + url;
        //             console.log(url);
        //             window.location.href = url;
        //         } else {
        //             url = "#";
        //         }
        //     } else {
        //         url = "#";
        //     }
        //     return url;

        // },
        stopLoader: function () {
            $('#preloader').delay(50).fadeOut(250);
        },
        swapLocations: function (id) {
            if ((this.CityFrom) && (this.CityTo)) {
                var from = this.CityFrom;
                var to = this.CityTo;
                this.CityFrom = to;
                this.CityTo = from;
                swpaloc(id)
            }
        },
        setTripType: function (triptype) {
            if (triptype == "R") {
                this.triptype = "R"
                this.CityTo = "";
                this.CityFrom = "";
                $("#Cityfrom1").val('');
                $("#Cityto1").val('');
                $("#deptDate01").val('');
                $("#retDate").val('');
            }
            if (triptype == "O") {
                this.triptype = "O"
                this.CityTo = "";
                this.CityFrom = "";
                $("#Cityfrom1").val('');
                $("#Cityto1").val('');
                $("#deptDate01").val('');
                $("#retDate").val('');
            }
            if (triptype == "M") {
                this.triptype = "M"
                this.CityTo = "";
                this.CityFrom = "";
                $("#deptDate01").val('');
                $("#retDate").val('');
            }
        },
        MultiSearchFlight: function () {
            var sectors = '';
            var legDetails = [];
            for (var legValue = 1; legValue <= this.legcount; legValue++) {
                var temDeparturedate = $('#txtLeg' + (legValue) + 'Date').val() == "" ? "" : $('#txtLeg' + (legValue) + 'Date').datepicker('getDate');
                if (temDeparturedate != "" && this.cityList.length != 0 && this.cityList[legValue - 1].from != "" && this.cityList[legValue - 1].to != "") {
                    var departureFrom = this.cityList[legValue - 1].from;
                    var arrivalTo = this.cityList[legValue - 1].to;
                    var travelDate = moment(temDeparturedate).format('DD|MM|YYYY');
                    sectors += '/' + departureFrom + '-' + arrivalTo + '-' + travelDate;
                    legDetails.push(departureFrom + '|' + arrivalTo)
                } else {
                    alertify.alert('Alert', 'Please fill the Trip ' + (legValue) + '   fields !').set('closable', false);


                    return false;
                }
            }
            var directFlight = this.direct_flight ? 'DF' : 'AF';

            var adult = this.selected_adult;
            var child = this.selected_child;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            getSuppliers(legDetails,
            function(supp) {
                var searchUrl = '/Flights/flight-listing.html?flight=' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                // searchUrl = searchUrl.toLocaleLowerCase();
                window.location.href = searchUrl;
            })
        },
        limitText(count) {
            return `and ${count} other Airlines`
        },
        asyncFind(query) {
            if (query.length > 2) {
                this.isLoading = true;

                var newData = [];
                this.airlineList.filter(function (el) {
                    if (el.A.toLowerCase().indexOf(query.toLowerCase()) >= 0) {
                        newData.push({
                            "code": el.C,
                            "label": el.A
                        });
                    }
                });
                if (newData.length > 0) {
                    this.Airlineresults = newData;
                    this.isLoading = false;
                } else {
                    this.Airlineresults = [];
                    this.isLoading = false;
                }
            }


        },
        clearAll() {
            this.selectedAirline = []
        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                if (type == 'Adult') {
                    this.selected_adult = event.target.value;
                } else if (type == 'Infant') {
                    this.selected_infant = event.target.value;
                } else if (type == 'Child') {
                    this.selected_child = event.target.value;
                } else if (type == 'Cabin') {
                    this.selected_cabin = event.target.value;
                }
            }
        },
        setpaxcount(item, action) {
            if (item == 'adt') {
                if (action == 'plus') {
                    if (this.selected_adult < this.totalAllowdPax) {
                        ++this.selected_adult;
                    }
                } else {
                    if (this.selected_adult > 1) {
                        --this.selected_adult;
                        if (this.selected_adult < this.selected_infant) {
                            this.selected_infant = this.selected_adult;
                        }

                    }
                }
            } else if (item == 'chd') {
                if (action == 'plus') {
                    if (this.selected_adult + this.selected_child < this.totalAllowdPax) {
                        ++this.selected_child;
                    }
                } else {
                    if (this.selected_child > 0) {
                        --this.selected_child;
                    }
                }
            } else if (item == 'inf') {
                if (action == 'plus') {
                    if (this.selected_adult + this.selected_child + this.selected_infant < this.totalAllowdPax) {
                        if (this.selected_infant < this.selected_adult) {
                            ++this.selected_infant
                        }

                    }
                } else {
                    if (this.selected_infant > 0) {
                        --this.selected_infant
                    }
                }
            } else { }
            if (this.selected_adult + this.selected_child > this.totalAllowdPax) {
                this.selected_child = 0;
            }

            if (this.selected_adult + this.selected_child + this.selected_infant > this.totalAllowdPax) {
                this.selected_infant = 0;

            }
            var cabin = getCabinName(this.selected_cabin);
            var totalpax = this.selected_adult + this.selected_child + this.selected_infant;
            if (parseInt(totalpax) > 1) {
                totalpax = totalpax ;
            } else {
                totalpax = totalpax;
            }
            this.Totaltravaller = totalpax +' '+this.mainData.Passenger_Label;
        },
    },
    filters: {
        moment: function (date) {
            return moment(date).format('DD MMM YYYY');
        }
    }

});

// var holidaysSec = new Vue({
//     el: '#holidays',
//     name: 'holidaysSec',
//     data() {
//         return {
//             pkgSearch: ''
//         }
//     },
//     methods: {
//         packageListing(name) {
//             holidypack.packageListing(name);
//         },
//     },
// });

function owlcarosl(imageUrl) {
    $('#main_banner').css('background-image', 'url(' + imageUrl + ')');
    $("#owl-demo-2").owlCarousel({
        items: 3,
        lazyLoad: true,
        loop: true,
        margin: 30,
        navigation: true,
        itemsDesktop: [991, 2],
        itemsDesktopSmall: [979, 2],
        itemsTablet: [768, 2],
        itemsMobile: [640, 1],
    });
    $(".owl-prev").html('<i class="fa fa-angle-left"></i>');
    $(".owl-next").html('<i class="fa fa-angle-right"></i>');
}

$(document).ready(function () {
    var active_el = (sessionStorage.active_el) ? sessionStorage.active_el : 3;
    if (active_el == 1) {
        $('.nav-tabs a[href="#flights"]').tab('show');
    } else if (active_el == 2) {
        $('.nav-tabs a[href="#hotels"]').tab('show');
    } else {
        $('.nav-tabs a[href="#holidays"]').tab('show');
    }

});



function isNullorUndefined(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) {
        status = true;
    }
    return status;
}
function getCabinName(cabinCode) {
    var cabinClass = 'Economy';
    if (cabinCode == "F") {
        cabinClass = "First Class";
    } else if (cabinCode == "C") {
        cabinClass = "Business";
    } else {
        try { cabinClass = getCabinClassObject(cabinCode).BasicClass; } catch (err) { }
    }
    return cabinClass;
}
function swpaloc(id) {
    var from = $("#Cityfrom" + id).val();
    var to = $("#Cityto" + id).val();
    $("#Cityfrom" + id).val(to);
    $("#Cityto" + id).val(from);
}  $(function  carousels(){
    $('#carousel-example-generic').carousel();
  });