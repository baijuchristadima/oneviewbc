﻿var ServiceUrls = {
    "hubConnection": {
        "baseUrl": "https://staginghubv3.oneviewitsolutions.com:",
        "webSocketUrl": "wss://staginghubv3.oneviewitsolutions.com:8012/events",
        // "baseUrl": "https://livehubv3.oneviewitsolutions.com:",
        // "webSocketUrl": "wss://livehubv3.oneviewitsolutions.com:8012/events",
        "cmsUrl": "https://cms.oneviewitsolutions.com:",
        "ipAddress": 9443,
        "webSocketIpAddress": 8012,
        "logoBaseUrl": "https://stag-prm.oneviewitsolutions.com/premier-v3/javax.faces.resource/",
        "hubServices": {
            "authenticateUrl": "/authenticate",
            "forgotPasswordUrl": "/profile/forgot",
            "creditLimit": "/transaction/availablecreditlimit",
            "reportUrl": "/commonReport/count",
            "dashboardReports": "/commonReport/upcomingcount",
            "reNewToken": "/refreshtoken",
            "myBookings": "/commonReport",
            "resetPassword": "/profile/password/reset",
            "changePassword": "/profile/password",
            "creditLimitWebSocket": "/events",
            "paymentGatewayCredentials": "/payment",
            "paymentGatewayUpdateStatus": "/payment/exception",
            "contact": "/contact",
            "emailSettings": "/emailSettings",
            "getLoginPageUrl": "/loginpage",
            "getBookingRef": "/payment/bookingRef",
            "getAgencyCodes": "/agencyCodes",
            "updateProfile": "/profile/update",
            "registerUser": "/registerUser",
            "getCoTravellers": "/b2c/cotraveler",
            "addCoTraveller": "/b2c/cotraveler",
            "deleteCoTraveller": "/b2c/cotraveler",
            "feedback": "/b2c/feedback",
            "cart": "/cart",
            "pgGetPostData": "/payment/getPostData",
            "pgGetSignature": "/payment/signature",
            "pgPurchase": "/payment/purchase",
            "pgPurchaseDone": "/payment",
            "pgPayStack": "/paystack/initialise",
            "pgSyberPayInitialise": "/syberPay/initialise",
            "pgSyberPayVerify": "/syberPay/verify",
            "pgFlutterwave": "/flutterwave/initialise",
            "pgFAB": "/epg/initialise",
            "pgWaafiPay": "/waafipay/initialise",
            "pgWaafiPayEvc": "/waafipay/evc/initialise",
            "pgWaafiPayZaad": "/waafipay/zaad/initialise",
            "pgNGeniusPay": "/ngenius/initialise",
            "pgEDahab": "/edahab/initialise",
            "stanbic": "/stanbic/initialise",
            "pgPayTabs": "/paytabs/initialise",
            "flights": {
                "airSearch": "/flightSearch/search",
                "airSelect": "/flightSelect",
                "airBooking": "/flightBook",
                "airTripDetails": "/tripDetail",
                "airIssueTicket": "/issueFlight",
                "AircancelPnr": "/cancelFlight",
                "addQuote": "/flight/addQuote",
                "pendingQuoteDetails": "/flight/pendingQuoteDetails",
                "removeQuote": "/flight/remove",
                "sendQuote": "/flight/saveSendQuote",
                "quoteDetails": "/flight/quoteDetails",
                "allQuotes": "/flight/allQuotes",
                "myBookingsCriteria": "/commonReport/flight",
                "getCoTravellers": "/traveler/1",
                "airMiniRule": "/miniRules"
            },
            "transfer": {

                "addQuote": "/transfer/addQuote",
                "pendingQuoteDetails": "/transfer/pendingQuoteDetails",
                "removeQuote": "/transfer/removeQuote",
                "sendQuote": "/transfer/saveSendQuote",
                "quoteDetails": "/transfer/quoteDetails",
                "allQuotes": "/transfer/allQuotes",
                "bookTransfer": "/bookTransfer",
                "myBookingsCriteria": "/commonReport/transfer",
                "transferbookdetails": "/transfer/bookingDetails",
                "transferSearch": "/searchTransfer",
                "transfercancel": "/cancelTransfer"

            },
            "hotels": {
                "serviceMarkup": "/markup/fetchapplicablemarkups",
                "currencyRates": "/currency/rates",
                "pendingQuoteDetails": "/quote/pendingQuoteDetails",
                "quoteDetails": "/quote/quoteDetails",
                "removeQuotation": "/quote/remove",
                "allQuotes": "quote/allQuotes",
                "addQuote": "/quote",
                "saveSendQuote": "/quote/saveSendQuote",
                "hotelBooking": "hotel/booking",
                "bookingReference": "hotel/booking/bookingbyref/",
                "bookingError": "hotel/booking/error",
                "bookingSuccess": "hotel/booking/success",
                "bookingChangeStatus": "hotel/booking/changeStatus",
                "searchBookings": "/commonReport/hotel",
                "allBookings": "/hotel/allBookings"

            },
            "insurance": {
                "tuneinsuranceDetails": "/insurDetails",
                "myBookingsCriteria": "/commonReport/insurance",
                "tpGetCurrencyCode": "/country/countries",
                "purchase": "/purchase",
                "tuneprotectDetail": "/tuneprotectDetail"
            },
        }
    },
    "cacheServiceApi": {
        "apiUrl": "http://oneviewcachedata.oneviewitsolutions.com",
        "services": {
            "getMinPrice": "/price-calendar/get-min-price",
            "addMinPrice": "/price-calendar/add-min-price",
            "transferCity": "/transfer/get-cities",
            "transferSubcity": "/transfer/get-subcities",
            "transferHotel": "transfer/get-transferhotels"
        }
    },
    "elasticSearch": {
        "elasticsearchHost": "ee3b963212284c488a7309cdab3b262c.ap-southeast-1.aws.found.io",
        "auth": "OVIT_BookingPortal:4Rfv7Ujm$",
        "protocol": "https",
        "port": "9243",
        "url": "https://airportinfo.oneviewitsolutions.com/",
        "airFilter": "https://api.oneviewitsolutions.com/matrixhub/airfilter"
},
    "emailServices": {
        "emailApi": "https://6ejwh0rlo3.execute-api.ap-south-1.amazonaws.com/default/GenericMail",
        "generatePdf": "https://template.oneviewitsolutions.com:9483/pdfGenerate",
        "getTemplate": "https://template.oneviewitsolutions.com:9483/template/all/",
        "htmlGenerate": "https://template.oneviewitsolutions.com:9483/htmlGenerate",
        "emailApiWithAttachment": "https://6ejwh0rlo3.execute-api.ap-south-1.amazonaws.com/default/GenericMail/attachment"

    },
    "externalServiceUrl":{
        "ipAddressApi":"https://api64.ipify.org?format=json"

    }
}