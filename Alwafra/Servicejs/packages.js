myPackageObject = new Vue({

  el: '#Package',
  data: {
    restObject: {},
    packages: [],
    commonData: {},
    content: null,
    banner: { img: '', caption: '', },
    label: { Breadcrumb_1: '', Breadcrumb_2: '', Breadcrumb_3: '', Breadcrumb_4: '' },
    labelPackage: { Main_Title: '', Days_Label: '', Nights_Label: '', From_Label: '' },
    totalNews: 0,
    currentPage: 1,
    currentPages: 1,
    fromPage: 1,
    totalpage: 1,
    constructedNumbers: [],
    showNews: [],
    pageLimit: 3,
    paginationLimit: 1,
    Banner:{Banner_Image:''}

  },
  key: 0,
  filters: {

    subStr: function (string) {
      if(string!=undefined || string!=""){
      string = string.replace(/<\/?[^>]+(>|$)/g, "");
      if (string.length > 100){
        return string.substr(0, 100) + '...';
      }
      else
        return string;
    }
  },
  titleStr: function (string) {
    if(string!=undefined || string!=""){
    string = string.replace(/<\/?[^>]+(>|$)/g, "");
    if (string.length > 10)
      return string.substr(0, 10) + '...';

    else
      return string;
  }
},
  },
  
  methods: {
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    

    getpagecontent: function () {
      if (localStorage.getItem("AgencyCode") === null) {
        localStorage.AgencyCode = JSON.parse(localStorage.User).loginNode.code;

      }
      var Agencycode = localStorage.AgencyCode;
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
      var self = this;
      // var packagecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Holidays/Holidays/Holidays.ftl';
      var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Packages/Packages/Packages.ftl';
      var banner = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Banners/Banners/Banners.ftl';
      var cmsurl = huburl + portno + homecms;
     
      axios.get(cmsurl, {
        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
      }).then(function (response) {
        console.log(response.data);
        self.content = response.data;
        let holidayPackageListTemp = [];
        if (self.content != undefined && self.content.Values != undefined) {
          holidayPackageListTemp = self.content.Values.filter(function (el) {
            return el.status == true && el.package_name !=null
          });
        }
        self.packages = holidayPackageListTemp;
        self.setTotalPackageCount();
        self.getpackage = true;



      }).catch(function (error) {
        console.log('Error');
        self.content = [];
      });
      axios.get(banner, {
        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
    }).then(function (response) {
        self.content = response.data;
        var Banner = self.pluck('Packages_Banner', self.content.area_List);
        if (Banner.length > 0) {
            self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
            self.Banner.Title = self.pluckcom('Title', Banner[0].component);
            self.Banner.Breadcrumb_1 = self.pluckcom('Breadcrumb_1', Banner[0].component);
            self.Banner.Breadcrumb_2 = self.pluckcom('Breadcrumb_2', Banner[0].component);
        }
    }).catch(function (error) {
        console.log('Error');
    });
    },
    setTotalPackageCount: function () {
      if (this.packages != undefined && this.packages != undefined) {
        this.totalNews = Number(this.packages.length);
        this.totalpage = Math.ceil(this.totalNews / this.pageLimit);
        this.currentPage = 1;
        this.fromPage = 1;
        if (Number(this.totalNews) < 6) {
          // this.pageLimit=3;
        }

        this.constructAllPagianationLink();

      }
    },
    constructAllPagianationLink: function () {
      let limit = this.paginationLimit;
      this.constructedNumbers = [];
      for (let i = Number(this.fromPage); i <= (Number(this.totalpage) + limit); i++) {
        if (i <= Number(this.totalpage)) {
          this.constructedNumbers.push(i);

        }

      }
      this.currentPage = this.constructedNumbers;
      this.setNewsItems();
    },
    prevNextEvent: function (type) {

      let limit = this.paginationLimit;
      if (type == 'Previous') {
        if (this.currentPages > this.totalpage) {
          this.currentPages = this.currentPages - 1;
        }

        // this.currentPage = event.target.innerText;
        // this.currentPage=parseInt(this.currentPage);
        this.fromPage = this.currentPages;
        if (Number(this.fromPage) != 1) {
          // this.fromPage = Number(this.fromPage) - limit;
          this.currentPages = Number(this.currentPages) - 1;
          this.setNewsItems();

          // this.constructAllPagianationLink();

        }

      } else if (type == 'Next') {
        if (this.currentPages == 'undefined' || this.currentPages == '') {
          // this.currentPages = sessionStorage.getItem("currentpage");
        }
        if (this.currentPages <= this.totalpage) {
          let limit = this.paginationLimit;
          this.fromPage = this.currentPages;
          var totalP = (this.totalpage) + 1;
          if (Number(this.fromPage) != totalP) {
            var count = this.currentPages + limit;
            if (Number(count) <= Number(this.totalpage)) {
              this.selectNewEvent(this.currentPages);
            }

          }
        }
      }
    },
    selectNewEvent: function (ev) {
      let limit = 1;
      console.log(ev);
      this.currentPages = this.currentPage[ev];

      this.setNewsItems();
    },
    selected: function (ev) {
      let limit = 1;
      console.log(ev);
      this.currentPages = this.currentPage[ev];

      this.setNewsItems();
    },
    setNewsItems: function () {
      if (this.packages != undefined && this.packages != undefined) {
        let start = 0;
        let end = Number(start) + Number(this.pageLimit);
        if (Number(this.currentPages) == 1) {
        } else {
          var limit = this.totalpage;
          start = (Number(this.currentPages) + Number(this.pageLimit)) - 2;
          end = Number(start) + Number(this.pageLimit);
        }
        this.showNews = this.packages.slice(start, end);
      }
    },
    startup: function () {

      this.getpagecontent();
    },
    getmoreinfo(url) {
      if (url != null) {
        if (url != "") {
          url = url.split("/Template/")[1];
          url = url.split(' ').join('-');
          url = url.split('.').slice(0, -1).join('.');
          url = "package-detail.html?page=" + url + "&from=pkg";
          window.location.href = url;
        }
        else {
          url = "#";
        }
      }
      else {
        url = "#";
      }
      return url;
    },
  },
  mounted: function () {

    this.getpagecontent();
    sessionStorage.active_e=1;
  }
})
