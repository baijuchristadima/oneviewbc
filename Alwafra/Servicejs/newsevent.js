mynewseventObject = new Vue({

    el: '#newsevent',
    data: {
      restObject: {},
      newsevents: [],
      commonData: {},
      content: null,
      banner: { img: '', caption: '', },
      totalNews: 0,
      getnewsevents:false,
      currentPage: 1,
      currentPages: 1,
      fromPage: 1,
      totalpage: 1,
      constructedNumbers: [],
      showNews: [],
      pageLimit: 4,
      paginationLimit: 1,
      count:0,
      commentCounts:0,
      allcomment:[],
      key: 0,
      sample:'',
      Banner:{Banner_Image:''}
    },
   
    filters: {
  
      subStr: function (string) {
        string = string.replace(/<\/?[^>]+(>|$)/g, "");
        if (string.length > 10)
          return string.substr(0, 75) + '...';
  
        else
          return string;
      },
      titleStr: function (string) {
        string = string.replace(/<\/?[^>]+(>|$)/g, "");
        if (string.length > 10)
          return string.substr(0, 10) + '...';
  
        else
          return string;
      },
      datefilter: function(date){
        date= date.replace(/,/g, "");
        return date;
    },
  
    },
    
    methods: {
      pluck(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
          if (item[key] != undefined) {
            Temparry.push(item[key]);
          }
        });
        return Temparry;
      },
      pluckcom(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
          if (item[key] != undefined) {
            Temparry = item[key];
          }
        });
        return Temparry;
      },
      getpagecontent: function () {
        if (localStorage.getItem("AgencyCode") === null) {
          localStorage.AgencyCode = JSON.parse(localStorage.User).loginNode.code;
  
        }
        var Agencycode = localStorage.AgencyCode;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        var self = this;
        var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/News and Events/News and Events/News and Events.ftl';
        var banner = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Banners/Banners/Banners.ftl';
        var newseventtagurl = getQueryStringValue('redirectpage');
        var redirectcategorypage = getQueryStringValue('redirectcategorypage');
        var cmsurl = huburl + portno + homecms;
        
        axios.get(cmsurl, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          let newseventListTemp = [];
           if (newseventtagurl != undefined && newseventtagurl != "") {
            newseventListTemp = self.content.Values.filter(function (el) {
              return el.status == true && el.Tags_Type.includes(newseventtagurl)
            });
          }
          else if (redirectcategorypage != undefined && redirectcategorypage != "") {
            newseventListTemp = self.content.Values.filter(function (el) {
              return el.status == true && el.Category_Type==redirectcategorypage
            });
          }

          else if (self.content != undefined && self.content.Values != undefined) {
            newseventListTemp = self.content.Values.filter(function (el) {
              return el.status == true
            });
          }
         
          self.newsevents = newseventListTemp;
          self.setTotalnewseventCount();
          self.getnewsevents = true;
  
  axios.get(banner, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
      }).then(function (response) {
          self.content = response.data;
          var Banner = self.pluck('News_and_Events_Banner', self.content.area_List);
          if (Banner.length > 0) {
              self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
              self.Banner.Title = self.pluckcom('Title', Banner[0].component);
              self.Banner.Breadcrumb_1 = self.pluckcom('Breadcrumb_1', Banner[0].component);
              self.Banner.Breadcrumb_2 = self.pluckcom('Breadcrumb_2', Banner[0].component);
          }
      }).catch(function (error) {
          console.log('Error');
      });
          console.log('Error');
          self.content = [];
        });

        axios.get(banner, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
      }).then(function (response) {
          self.content = response.data;
          var Banner = self.pluck('News_and_Events_Banner', self.content.area_List);
          if (Banner.length > 0) {
              self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
              self.Banner.Title = self.pluckcom('Title', Banner[0].component);
              self.Banner.Breadcrumb_1 = self.pluckcom('Breadcrumb_1', Banner[0].component);
              self.Banner.Breadcrumb_2 = self.pluckcom('Breadcrumb_2', Banner[0].component);
          }
      }).catch(function (error) {
          console.log('Error');
      });
      },
      findcount:function(url){
        alert(url);
      },
      setTotalnewseventCount: function () {
        if (this.newsevents != undefined && this.newsevents != undefined) {
          this.totalNews = Number(this.newsevents.length);
          this.totalpage = Math.ceil(this.totalNews / this.pageLimit);
          this.currentPage = 1;
          this.fromPage = 1;
          if (Number(this.totalNews) < 6) {
            // this.pageLimit=3;
          }
  
          this.constructAllPagianationLink();
  
        }
      },
      constructAllPagianationLink: function () {
        let limit = this.paginationLimit;
        this.constructedNumbers = [];
        for (let i = Number(this.fromPage); i <= (Number(this.totalpage) + limit); i++) {
          if (i <= Number(this.totalpage)) {
            this.constructedNumbers.push(i);
  
          }
  
        }
        this.currentPage = this.constructedNumbers;
        this.setNewsItems();
      },
      prevNextEvent: function (type) {
  
        let limit = this.paginationLimit;
        if (type == 'Previous') {
          if (this.currentPages > this.totalpage) {
            this.currentPages = this.currentPages - 1;
          }
  
          this.fromPage = this.currentPages;
          if (Number(this.fromPage) != 1) {
            this.currentPages = Number(this.currentPages) - 1;
            this.setNewsItems();
  
          }
  
        } else if (type == 'Next') {
          if (this.currentPages == 'undefined' || this.currentPages == '') {
          }
          if (this.currentPages <= this.totalpage) {
            let limit = this.paginationLimit;
            this.fromPage = this.currentPages;
            var totalP = (this.totalpage) + 1;
            if (Number(this.fromPage) != totalP) {

              var counts = this.currentPages + limit;
              if (Number(counts) <= Number(this.totalpage)) {
                this.selectNewEvent(this.currentPages);
              }
  
            }
          }
        }
      },
      selectNewEvent: function (ev) {
        let limit = 1;
        this.currentPages = this.currentPage[ev];
  
        this.setNewsItems();
      },
      setNewsItems: function () {
        if (this.newsevents != undefined && this.newsevents != undefined) {
          let start = 0;
          let end = Number(start) + Number(this.pageLimit);
          if (Number(this.currentPages) == 1) {
          } else {
            // var limit = this.totalpage;
            start = (Number(this.currentPages) + Number(this.pageLimit)) - 2;
            end = Number(start) + Number(this.pageLimit);
          }
          this.showNews = this.newsevents.slice(start, end);
        }
  
      },

      viewComment: async function () {
        var self = this;
  
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        let requestObject = { from: 0, size: 100, type: "News and Event Comments", nodeCode: agencyCode, orderBy: "desc" };
        let responseObject = await this.cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
          if (responseObject != undefined && responseObject.data != undefined) {
            self.allcomment = JSON.parse(JSON.stringify(responseObject)).data;
  
          }
  
        });
      },

      commentCount: function(url){
        var self = this;
          if (url != "") {
            url = url.split("/Template/")[1];
            url = url.split(' ').join('-');
            url = url.split('.').slice(0, -1).join('.');
            url = url;
            let commentnum = [];
            commentnum = self.allcomment;
            commentnum=commentnum.filter(r => r.keyword4 == url)
           
            return commentnum.length
           
  
          
        }
      
    },

      async cmsRequestData(callMethod, urlParam, data, headerVal) {
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        const url = huburl + portno + "/" + urlParam;
        if (data != null) {
          data = JSON.stringify(data);
        }
        const response = await fetch(url, {
          method: callMethod, // *GET, POST, PUT, DELETE, etc.
          credentials: "same-origin", // include, *same-origin, omit
          headers: { 'Content-Type': 'application/json' },
          body: data, // body data type must match "Content-Type" header
        });
        try {
          const myJson = await response.json();
          return myJson;
        } catch (error) {
          return object;
        }
      },
      datenumberFormatter: function (utc) {

        return (moment(utc).utcOffset("+05:30").format('DD'));
       
    },
    datemonthFormatter: function (utc) {

        return (moment(utc).utcOffset("+05:30").format('ll').slice(0, 3));

    },
    dateyearFormatter: function (utc) {

        return (moment(utc).utcOffset("+05:30").format('ll').slice(7, 12));

    },
      startup: function () {
  
        this.getpagecontent();
      },
      getmoreinfo(url) {
        if (url != null) {
          if (url != "") {
            url = url.split("/Template/")[1];
            url = url.split(' ').join('-');
            url = url.split('.').slice(0, -1).join('.');
            url = "news-detail.html?page=" + url + "&from=pkg";
            window.location.href = url;
          
           
  
          }
          else {
            url = "#";
          }
        }
        else {
          url = "#";
        }
        return url;
      },
    },
    mounted: function () {
      this.viewComment();
      this.getpagecontent();
      sessionStorage.active_e=3;
      
    }
  })
  function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
  }