var aboutus = new Vue({
    el: '#servicepage',
    name: 'servicepage',
    data: {
        serviceContent: {
            title: '',
            description: '',
        },
        Services: null,
        Banner:{Banner_Image:''}
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                //self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Services/Services/Services.ftl';
                var banner = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Banners/Banners/Banners.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(async function (response) {
                    self.content = response.data;
                    var serviceContent = self.pluck('Title_area', self.content.area_List);
                    if (serviceContent.length > 0) {
                        self.serviceContent.title = self.pluckcom('Title', serviceContent[0].component);
                        self.serviceContent.description = self.pluckcom('Introduction', serviceContent[0].component);
                    }

                    var service = self.pluck('services_area', self.content.area_List);
                    if (service.length > 0) {
                        self.Services = self.pluckcom('Services', service[0].component);

                    }
                }).catch(function (error) {
                    console.log('Error');
                });

                axios.get(banner, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var Banner = self.pluck('Our_Services_Banner', self.content.area_List);
                    if (Banner.length > 0) {
                        self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
                        self.Banner.Title = self.pluckcom('Title', Banner[0].component);
                        self.Banner.Breadcrumb_1 = self.pluckcom('Breadcrumb_1', Banner[0].component);
                        self.Banner.Breadcrumb_2 = self.pluckcom('Breadcrumb_2', Banner[0].component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                });
            });

        },
    },
    mounted: function () {
        this.getPagecontent();
        sessionStorage.active_e=2;
    },
})