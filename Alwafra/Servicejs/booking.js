myBookingObject = new Vue({

    el: '#booking',
    data: {
        Banner:{Banner_Image:''},

        restObject: {},
        addBookingForm: {
            fname: '',
            femail: '',
            fphone: '',
            fpack: '',
            fdate: '',
            fadult: 1,
            fchild: 0,
            finfants: 0,
            fmessage: ''
        },
        Package: { package_name: '' },
        adultcount: {},
        childcount: {},
        infantcount: {},
        label: { Warning_Alert_Type: '', success_Alert_Type: '' },
        Bookinglimit: {},
        labelBooking: { Name_Label: '', },
        commonData: {},
        commonDet: { Banner_image: '', Banner_title: '' },
        messageCon: '',
        pageURLLink: "",
        key: 0,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    },
    created: function () {
        this.getPagecontent();

    },
    methods: {
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var packageurl = getQueryStringValue('page');
                if (packageurl != "") {
                    packageurl = packageurl.split('-').join(' ');
                    //  langauage = packageurl.split('_')[1];
                    packageurl = packageurl.split('_')[0];
                    var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';

                    axios.get(topackageurl, {
                        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                    }).then(function (response) {
                        self.content = response.data;
                        var pagecontent = self.pluck('main', self.content.area_List);
                        if (pagecontent.length > 0) {
                            self.Package.package_name = self.pluckcom('package_name', pagecontent[0].component);
                        }


                        self.getdata = false;

                    }).catch(function (error) {
                        window.location.href = "#";
                        self.content = [];
                        self.getdata = true;
                    });

                }

                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Holiday Package Page/Holiday Package Page/Holiday Package Page.ftl';
                axios.get(topackageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.contentLabel = response.data;
                    if (self.contentLabel != undefined && self.contentLabel.area_List != undefined) {
                        var LabelCompnt = self.pluck('Common_Area', self.contentLabel.area_List);
                        self.label.Warning_Alert_Type = self.pluckcom('Warning_Alert_Type', LabelCompnt[0].component);
                        self.label.success_Alert_Type = self.pluckcom('Success_Alert_Type', LabelCompnt[0].component);
                    }

                    var LabelbookingCompnt = self.pluck('Booking_Page', self.contentLabel.area_List);
                    self.labelBooking.Main_Title = self.pluckcom('Main_Title', LabelbookingCompnt[0].component);
                    self.labelBooking.Name_Label = self.pluckcom('First_Name_Label', LabelbookingCompnt[0].component);
                    self.labelBooking.Name_Placeholder = self.pluckcom('First_Name_Placeholder', LabelbookingCompnt[0].component);
                    self.labelBooking.Email_Label = self.pluckcom('Email_Label', LabelbookingCompnt[0].component);
                    self.labelBooking.Email_Placeholder = self.pluckcom('Email_Placeholder', LabelbookingCompnt[0].component);
                    self.labelBooking.Phone_Label = self.pluckcom('Phone_Label', LabelbookingCompnt[0].component);
                    self.labelBooking.Phone_Placeholder = self.pluckcom('Phone_Placeholder', LabelbookingCompnt[0].component);
                    self.labelBooking.Package_Title_Label = self.pluckcom('Package_Title_Label', LabelbookingCompnt[0].component);
                    self.labelBooking.Book_Date_Label = self.pluckcom('Book_Date_Label', LabelbookingCompnt[0].component);
                    self.labelBooking.Book_Date_Placeholder = self.pluckcom('Book_Date_Palceholder', LabelbookingCompnt[0].component);
                    self.labelBooking.Adult_Label = self.pluckcom('Adult_Label', LabelbookingCompnt[0].component);
                    self.labelBooking.Child_Label = self.pluckcom('Child_Label', LabelbookingCompnt[0].component);
                    self.labelBooking.Infants_Label = self.pluckcom('Infants_Label', LabelbookingCompnt[0].component);
                    self.labelBooking.Message_Label = self.pluckcom('Message_Label', LabelbookingCompnt[0].component);
                    self.labelBooking.Message_Placeholder = self.pluckcom('Message__Placeholder', LabelbookingCompnt[0].component);
                    self.labelBooking.Book_Button_Label = self.pluckcom('Book_Button_Label', LabelbookingCompnt[0].component);
                    self.labelBooking.First_Name_Alert_Message = self.pluckcom('First_Name_Alert_Message', LabelbookingCompnt[0].component);
                    self.labelBooking.Email_Alert_Message = self.pluckcom('Email_Alert_Message', LabelbookingCompnt[0].component);
                    self.labelBooking.Phone_Alert_Message = self.pluckcom('Phone_Alert_Message', LabelbookingCompnt[0].component);
                    self.labelBooking.Book_Date_Alert_Message = self.pluckcom('Book_Date_Alert_Message', LabelbookingCompnt[0].component);
                    self.labelBooking.Adult_Alert_Message = self.pluckcom('Adult_Alert_Message', LabelbookingCompnt[0].component);
                    self.labelBooking.Incorrect_Email_Alert_Message = self.pluckcom('Incorrect_Email_Alert_Message', LabelbookingCompnt[0].component);
                    self.labelBooking.Email_content_custom_Message = self.pluckcom('Email_content_custom_Message', LabelbookingCompnt[0].component);
                    self.labelBooking.Success_Alert_Message = self.pluckcom('Success_Alert_Message', LabelbookingCompnt[0].component);

                    self.Bookinglimit.Adult_Limit = self.pluckcom('Adult_Limit', LabelbookingCompnt[0].component);
                    self.Bookinglimit.Children_Limit = self.pluckcom('Children_Limit', LabelbookingCompnt[0].component);
                    self.Bookinglimit.Infant_Limit = self.pluckcom('Infant_Limit', LabelbookingCompnt[0].component);

                    self.adultcounts();

                }).catch(function (error) {
                    window.location.href = "#";
                    self.content = [];
                    self.getdata = true;
                });
                var banner = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Banners/Banners/Banners.ftl';

                axios.get(banner, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var Banner = self.pluck('Booking_Banner', self.content.area_List);
                    if (Banner.length > 0) {
                        self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
                        self.Banner.Title = self.pluckcom('Title', Banner[0].component);
                        self.Banner.Breadcrumb_1 = self.pluckcom('Breadcrumb_1', Banner[0].component);
                        self.Banner.Breadcrumb_2 = self.pluckcom('Breadcrumb_2', Banner[0].component);
                        self.Banner.Breadcrumb_3 = self.pluckcom('Breadcrumb_3', Banner[0].component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                });



            });
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },

        adultcounts: function () {
            var adultLength = this.Bookinglimit.Adult_Limit;
            var adult = [];
            for (var i = 1; i <= adultLength; i++) {
                adult.push(i);
            }
            this.adultcount = adult;
            this.childcounts();
        },
        childcounts: function () {
            var childLength = this.Bookinglimit.Children_Limit;
            console.log(childLength);
            var child = [];
            for (var i = 0; i <= childLength; i++) {

                child.push(i);
            }
            this.childcount = child;
            this.infantcounts();
        },
        // infants count
        infantcounts: function () {
            var infantLength = this.Bookinglimit.Infant_Limit;
            console.log(infantLength);
            var infant = [];
            for (var i = 0; i <= infantLength; i++) {

                infant.push(i);
            }
            this.infantcount = infant;
            console.log(this.infantcount);

        },

        dropdownChange: function (event, type) {
            let dateObj = document.getElementById("from");
            if (event != undefined && event.target != undefined && event.target.value != undefined) {

                if (type == 'Adult') {
                    this.addBookingForm.fadult = event.target.value;

                } else if (type == 'Infant') {
                    this.addBookingForm.finfants = event.target.value;
                } else if (type == 'Child') {
                    this.addBookingForm.fchild = event.target.value;
                }
            }
            let dateValue = dateObj.value;
            setTimeout(function () {
                let dateObjNew = document.getElementById("from");
                dateObjNew.value = dateValue;
            }, 100);
        },

        addbooking: async function () {
            //this.getpagedet();
            let dateObj = document.getElementById("from");
            let dateValue = dateObj.value;
            // datevalue = moment(dateValue, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
            dateValue = moment(String(dateValue)).format('YYYY-MM-DDThh:mm:ss');


            let self = this;

            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.addBookingForm.femail.match(emailPat);
            if (this.addBookingForm.fname == undefined || this.addBookingForm.fname == '') {
                alertify.alert(this.label.Warning_Alert_Type, this.labelBooking.First_Name_Alert_Message);
                return;
            }
            else if (this.addBookingForm.femail == undefined || this.addBookingForm.femail == '') {
                alertify.alert(this.label.Warning_Alert_Type, this.labelBooking.Email_Alert_Message);
                return;

            }
            else if (matchArray == null) {
                alertify.alert(this.label.Warning_Alert_Type, this.labelBooking.Incorrect_Email_Alert_Message).set('closable', false);
                return false;
            } else if (this.addBookingForm.fphone == undefined || this.addBookingForm.fphone == '') {
                alertify.alert(this.label.Warning_Alert_Type, this.labelBooking.Phone_Alert_Message);
                return;
            }
            else if (dateValue == undefined || dateValue == '') {
                alertify.alert(this.label.Warning_Alert_Type, this.labelBooking.Book_Date_Alert_Message);
                return;

            }

            else if (this.addBookingForm.fadult == undefined || this.addBookingForm.fadult == '' || this.addBookingForm.fadult == 'AL') {
                alertify.alert(this.label.Warning_Alert_Type, this.labelBooking.Adult_Alert_Message);
                return;
            } else {
                var frommail = JSON.parse(localStorage.User).loginNode.email;
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.email) ? this.email : [this.email],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.username,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestObject = {
                    type: "Package Booking",
                    keyword1: self.addBookingForm.fname,
                    keyword3: self.addBookingForm.femail,
                    keyword4: self.addBookingForm.fphone,
                    keyword5: self.Package.package_name,
                    date1: dateValue,
                    number1: self.addBookingForm.fadult,
                    number2: self.addBookingForm.fchild,
                    number3: self.addBookingForm.finfants,
                    text1: self.addBookingForm.fmessage,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", requestObject, null);
                try {

                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, custmail);
                    alertify.alert(self.label.success_Alert_Type, self.labelBooking.Success_Alert_Message);
                    self.addBookingForm.fname = "";
                    self.addBookingForm.lname = "";
                    self.addBookingForm.femail = "";
                    self.addBookingForm.fphone = "";
                    self.addBookingForm.fpack = "";
                    self.dateValue = "";
                    self.addBookingForm.fadult = "";
                    self.addBookingForm.fchild = "";
                    self.addBookingForm.finfants = "";
                    self.addBookingForm.fmessage = "";
                } catch (e) {

                }
            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        dateFormatter: function (utc) {

            return (moment(utc).utcOffset("+05:30").format('ll').slice(0, 6));

        },

        startup: function () {
            this.getPagecontent();
        }


    },


    mounted: function () {
        this.getPagecontent();
        var vm = this;

        this.$nextTick(function () {
            $('#Adultmyselect').on("change", function (e) {
                vm.dropdownChange(e, 'Adult')

            });
            $('#Infantmyselect').on("change", function (e) {
                vm.dropdownChange(e, 'Infant')

            });
            $('#Childmyselect').on("change", function (e) {
                vm.dropdownChange(e, 'Child')

            });

            $('#from').change(function () {
                vm.addBookingForm.fdate = $('#from').val();

            });
        })

    }

});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
