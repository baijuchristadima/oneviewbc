const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var packagedetail = new Vue({
  i18n,
  el: '#package-detail',
  name: 'package-detail',
  data: {
    content: null,
    contentLabel: null,
    getdata: true,
    getpagecontent: false,
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
    CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    packageurl: '',
    addReviewForm: { Name: '', Email: '', Review: '' },
    label: { Warning_Alert_Type: '', success_Alert_Type: '' },
    labelPackageDet: {
      Destination_Label: '', Duration_Label: '', Dates_Label: '', Hotel_Label: '', Meals_Label: '', Price_Label: '',
      Tour_Videos_Label: '', Location_Map_Label: '', Button_Label: '', Itinerary_Tab_Label: '', Review_Tab_Label: '', Based_on_Label: '',
      Review_Label: '', Add_a_Review_Label: '', Add_a_Review_Condition_Label: '', Your_Rating_Label: '', Name_Label: '', Name_Placeholder: '',
      Email_Label: '', Email_Placeholder: '', Review_Text_Label: '', Review_Text_Placeholder: '', Submit_Review_Button_Label: '', Gallery_Tab_Label: '',
      Rating_Alert_Message: '', Name_Alert_Message: '', Email_alert_Message: '', Email_alert_Message: '', Incorrect_Email_Alert_Message: '',
      Review_Custom_Message: '', Review_Successful_Alert_Message: ''
    },
    reviewAvailable:false,
    allReview:[],
    avgrating: '',
    ratingcount: '',
    Package: {
      inner_Page_title: '',
      package_image: '',
      inner_Description: '',
      Region: '',
      Destination: '',
      No_of_Days: '',
      No_of_Nights: '',
      price: '',
      Tour_video_link: '',
      Map_location_Link: '',
      package_name:'',

    },
    Locations_table: [],
    itinerary: {},
    pageContent: {
      Inner_page_title: '',
      inner_BreadcrumbPath1: '',
      inner_BreadcrumbPath2: '',
    },
    pagelabel: {
      starting_from_label: '',
      booknow_label_1: '',
      booknow_label_2: '',
      Validity_label: '',
      Region_label: '',
      Destination_label: '',
      Call_us_label: '',
      phone_number: '',
      Share_on_text: '',
      facebook: '',


    },
    tabs: [],
    gallery_images: [],
    Banner:{}
  },
  methods: {
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;
      getAgencycode(function (response) {
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        // var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Package Page/Package Page/Package Page.ftl';
        var labelurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + "Holiday Package Page/Holiday Package Page/Holiday Package Page.ftl"
        var banner = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Banners/Banners/Banners.ftl';
        packageurl = getQueryStringValue('page');
        if (packageurl != "") {

          packageurl = packageurl.split('-').join(' ');
          //  langauage = packageurl.split('_')[1];
          packageurl = packageurl.split('_')[0];
          self.pageURLLink = packageurl;
          var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';

          axios.get(topackageurl, {
            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
          }).then(function (response) {
            self.content = response.data;
            var pagecontent = self.pluck('main', self.content.area_List);
            if (pagecontent.length > 0) {
              self.Package.inner_Page_title = self.pluckcom('inner_Page_title', pagecontent[0].component);
              self.Package.package_image = self.pluckcom('package_image', pagecontent[0].component);
              self.Package.inner_Description = self.pluckcom('inner_Description', pagecontent[0].component);
              self.Package.No_of_Days = self.pluckcom('No_of_Days', pagecontent[0].component);
              self.Package.No_of_Nights = self.pluckcom('No_of_Nights', pagecontent[0].component);
              self.Package.package_name = self.pluckcom('package_name', pagecontent[0].component);
              self.Package.Tour_video_link = self.pluckcom('Tour_video_link', pagecontent[0].component);
              self.Package.Map_location_Link = self.pluckcom('Map_location_Link', pagecontent[0].component);
              self.Locations_table = self.pluckcom('Locations_table', pagecontent[0].component);
              // setTimeout(function () { responsivetab() }, 10);

            }

            var pagecontent = self.pluck('itinerary', self.content.area_List);
            if (pagecontent.length > 0) {
              self.itinerary = self.pluckcom('content', pagecontent[0].component);
            }

            var pagecontent = self.pluck('Gallery', self.content.area_List);
            if (pagecontent.length > 0) {
              self.gallery_images = self.pluckcom('images', pagecontent[0].component);
            }

          }).catch(function (error) {
            window.location.href = "/Alwafra/package-detail.html";
            self.content = [];
            self.getdata = true;
          });

        }
        axios.get(labelurl, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.contentLabel = response.data;
          if (self.contentLabel != undefined && self.contentLabel.area_List != undefined) {
            var LabelCompnt = self.pluck('Common_Area', self.contentLabel.area_List);
            self.label.Warning_Alert_Type = self.pluckcom('Warning_Alert_Type', LabelCompnt[0].component);
            self.label.success_Alert_Type = self.pluckcom('Success_Alert_Type', LabelCompnt[0].component);
          }

          var LabelPackageDetCompnt = self.pluck('Package_Details_Page', self.contentLabel.area_List);
          self.labelPackageDet.Destination_Label = self.pluckcom('Destination_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Duration_Label = self.pluckcom('Duration_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Dates_Label = self.pluckcom('Dates_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Hotel_Label = self.pluckcom('Hotel_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Meals_Label = self.pluckcom('Meals_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Price_Label = self.pluckcom('Price_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Tour_Videos_Label = self.pluckcom('Tour_Videos_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Location_Map_Label = self.pluckcom('Location_Map_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Button_Label = self.pluckcom('Button_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Itinerary_Tab_Label = self.pluckcom('Itinerary_Tab_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Review_Tab_Label = self.pluckcom('Review_Tab_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Based_on_Label = self.pluckcom('Base_On_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Review_Label = self.pluckcom('Review_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Add_a_Review_Label = self.pluckcom('Add_a_Review_Title', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Add_a_Review_Condition_Label = self.pluckcom('Add_a_Review_Condition', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Your_Rating_Label = self.pluckcom('Your_Rating_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Name_Label = self.pluckcom('Name_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Name_Placeholder = self.pluckcom('Name_Placeholder', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Email_Label = self.pluckcom('Email_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Email_Placeholder = self.pluckcom('Email_Placeholder', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Review_Text_Label = self.pluckcom('Review_Text_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Review_Text_Placeholder = self.pluckcom('Review_Text_Placeholder', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Submit_Review_Button_Label = self.pluckcom('Submit_Review_Button_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Gallery_Tab_Label = self.pluckcom('Gallery_Tab_Label', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Rating_Alert_Message = self.pluckcom('Rating_Alert_Message', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Name_Alert_Message = self.pluckcom('Name_Alert_Message', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Email_alert_Message = self.pluckcom('Email_alert_Message', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Incorrect_Email_Alert_Message = self.pluckcom('Incorrect_Email_Alert_Message', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Review_Custom_Message = self.pluckcom('Review_Custom_Message', LabelPackageDetCompnt[0].component);
          self.labelPackageDet.Review_Successful_Alert_Message = self.pluckcom('Review_Successful_Alert_Message', LabelPackageDetCompnt[0].component);


          self.getdata = true;

        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
        axios.get(banner, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
      }).then(function (response) {
          self.content = response.data;
          var Banner = self.pluck('Package_Details_Banner', self.content.area_List);
          if (Banner.length > 0) {
              self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
              self.Banner.Title = self.pluckcom('Title', Banner[0].component);
              self.Banner.Breadcrumb_1 = self.pluckcom('Breadcrumb_1', Banner[0].component);
              self.Banner.Breadcrumb_2 = self.pluckcom('Breadcrumb_2', Banner[0].component);
              self.Banner.Breadcrumb_3 = self.pluckcom('Breadcrumb_3', Banner[0].component);
          }
      }).catch(function (error) {
          console.log('Error');
      });


      });
    },
    userRating: function (num) {
      var ulList = document.getElementById("ratingID");
      let m = 0;
      for (let i = 0; i < ulList.childNodes.length; i++) {
        let childNode = ulList.childNodes[i];
        if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
          m = m + 1;
          for (let k = 0; k < childNode.childNodes.length; k++) {
            let style = childNode.childNodes[k].style.color;
            if ((m) < Number(num)) {
              childNode.childNodes[k].style = "color: rgb(239, 158, 8)";
            } else if ((m) == Number(num)) {
              if (style.trim() == "rgb(239, 158, 8)") {
                childNode.childNodes[k].style = "color: a9a9a9;";
              } else {
                childNode.childNodes[k].style = "color: rgb(239, 158, 8);";
              }
            } else {
              childNode.childNodes[k].style = "color: a9a9a9";
            }
          }
        }
      }
    },
    getUserRating: function () {
      var ulList = document.getElementById("ratingID");
      let m = 0;
      for (let i = 0; i < ulList.childNodes.length; i++) {
        let childNode = ulList.childNodes[i];
        if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
          let style = "";
          for (let k = 0; k < childNode.childNodes.length; k++) {
            style = childNode.childNodes[k].style.color;
            if (style != undefined && style != '') {
              break;
            }
          }
          if (style.trim() == "a9a9a9") {
            break;
          } else if (style.trim() == "rgb(239, 158, 8)") {
            m = m + 1;
          }
        }
      }
      return m;
    },
    addReviews: function () {
      let ratings = this.getUserRating();
      console.log(ratings);
      ratings = Number(ratings);
      if (ratings == 0) {
        // ratings=1;

        // if(ratings==0 || Number(ratings) <=0){
        alertify.alert(this.label.Warning_Alert_Type, this.labelPackageDet.Rating_Alert_Message);
        return;
        //   }
      }
      let requestedDate = new Date();
      let self = this;
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.addReviewForm.Email.match(emailPat);
      if (this.addReviewForm.Name == undefined || this.addReviewForm.Name == '') {
        alertify.alert(this.label.Warning_Alert_Type, this.labelPackageDet.Name_Alert_Message);
        return;
      } else if (this.addReviewForm.Email == undefined || this.addReviewForm.Email == '') {
        alertify.alert(this.label.Warning_Alert_Type, this.labelPackageDet.Email_alert_Message);
        return;

      } else if (matchArray == null) {
        alertify.alert(this.label.Warning_Alert_Type, this.labelPackageDet.Incorrect_Email_Alert_Message).set('closable', false);
        return false;
      } else if (this.addReviewForm.Review == undefined || this.addReviewForm.Review == '') {
        alertify.alert(this.label.Warning_Alert_Type, this.labelPackageDet.Review_Custom_Message);
        return;
      }else {
        var frommail = 'uaecrt@gmail.com'
        var custmail = {
          fromEmail: frommail,
          toEmail: this.email,
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.username,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
          type: "Booking Review",
          keyword4: self.pageURLLink,
          keyword1: self.Package.package_name,
          keyword2: self.addReviewForm.Name,
          keyword3: self.addReviewForm.Email,
          text1: self.addReviewForm.Review,
          number1: ratings,
          date1: requestedDate,
          nodeCode: agencyCode
        };
        let responseObject =  this.cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
          let insertID = Number(responseObject);
          
          alertify.alert(self.label.success_Alert_Type, self.labelPackageDet.Review_Successful_Alert_Message);
          self.addReviewForm.Name = "";
          self.addReviewForm.Email = "";
          self.addReviewForm.Review = "";
          setTimeout(function () { self.viewReview();  }, 3000);
          // self.viewReview();
        } catch (e) {

        }



      }
    },
    async cmsRequestData(callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: { 'Content-Type': 'application/json' },
        body: data, // body data type must match "Content-Type" header
      });
      try {
        const myJson = await response.json();
        return myJson;
      } catch (error) {
        return object;
      }
    },
      bookNow:function(response){
        // var Agencycode = response;
        // var huburl = ServiceUrls.hubConnection.cmsUrl;
        // var portno = ServiceUrls.hubConnection.ipAddress;
        bookurl = getQueryStringValue('page');
              if (bookurl != "") {
                bookurl = bookurl.split('-').join(' ');
                  //  langauage = packageurl.split('_')[1];
                  bookurl = bookurl.split('_')[0];
                  bookurl = "booking.html?page=" + bookurl + "&from=pkg" ;
                  window.location.href =bookurl;

              }

      },     
    dateFormatter: function (utc) {
      return (moment(utc).utcOffset("+05:30").format("DD MMM YYYY"));
    },
    getAmount: function (amount) {
      amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
      return amount;
    },
    viewReview: async function () {	
      var self = this;	
      let allReview = [];	
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;	
      let requestObject = { from: 0, size: 100, type: "Booking Review", nodeCode: agencyCode, orderBy: "desc" };	
      let responseObject = await this.cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {	
        if (responseObject != undefined && responseObject.data != undefined) {	
          allResult = JSON.parse(JSON.stringify(responseObject)).data;	
          for (let i = 0; i < allResult.length; i++) {	
            if (allResult[i].keyword4 == self.pageURLLink) {	
              let object = {	
                Name: allResult[i].keyword2,	
                Date: moment(String(allResult[i].date1), "YYYY-MM-DDThh:mm:ss").format('MMM DD,YYYY'),	
                comment: allResult[i].text1,	
                Ratings: allResult[i].number1,	
              };	
              allReview.push(object);	
            }	
          }	
          self.allReview = allReview;	
         
          
          
        }	
        self.ratingcount = allReview.length;
        allratingcount = self.ratingcount;
        var avgratingtemp = 0;
        for (let i = 0; i < self.ratingcount; i++) {
          
          avgratingtemp = avgratingtemp + allReview[i].Ratings;
        }
        self.avgrating = ((avgratingtemp) / allratingcount).toFixed(2);
        if(self.avgrating>0){
          self.reviewAvailable=true;
      }
      });	
    },


  },

  mounted: function () {

    this.getPagecontent();
    this.viewReview();
    sessionStorage.active_e=1;


  },

});

function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
function responsivetab() {
  //Horizontal Tab
  // $('#parentHorizontalTab').easyResponsiveTabs({
  //     type: 'default', //Types: default, vertical, accordion
  //     width: 'auto', //auto or any width like 600px
  //     fit: true, // 100% fit in a container
  //     tabidentify: 'hor_1', // The tab groups identifier
  //     activate: function(event) { // Callback function if tab is switched
  //         var $tab = $(this);
  //         var $info = $('#nested-tabInfo');
  //         var $name = $('span', $info);
  //         $name.text($tab.text());
  //         $info.show();
  //     }
  // });

  // Child Tab
  $('#ChildVerticalTab_1').easyResponsiveTabs({
    type: 'vertical',
    width: 'auto',
    fit: true,
    tabidentify: 'ver_1', // The tab groups identifier
    activetab_bg: '#fff', // background color for active tabs in this group
    inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
    active_border_color: '#c1c1c1', // border color for active tabs heads in this group
    active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
  });

  //Vertical Tab
  $('#parentVerticalTab').easyResponsiveTabs({
    type: 'vertical', //Types: default, vertical, accordion
    width: 'auto', //auto or any width like 600px
    fit: true, // 100% fit in a container
    closed: 'accordion', // Start closed if in accordion view
    tabidentify: 'hor_1', // The tab groups identifier
    activate: function (event) { // Callback function if tab is switched
      var $tab = $(this);
      var $info = $('#nested-tabInfo2');
      var $name = $('span', $info);
      $name.text($tab.text());
      $info.show();
    }
  });
}


