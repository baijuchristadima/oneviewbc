const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var packagedetail = new Vue({
  i18n,
  el: '#news-event-detail',
  name: 'news-event-detail',
  data: {
    content: null,
    contentLabel: null,
    getdata: true,
    getpagecontent: false,
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
    CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    newseventurl: '',

    label: { Warning_Alert_Type: '', success_Alert_Type: '' },
    commentCount: 0,
    allComment: [],
    newsandevent: {
      date: '',
      Page_title: '',
      event_image: '',
      content: '',
      category: '',
      Tags_Type: '',
      User_Type: '',


    },
    filteredtagarray: [],
    moretagsaval: [],
    newsevents: [],
    Locations_table: [],
    itinerary: {},
    pageContent: {
      Inner_page_title: '',
      inner_BreadcrumbPath1: '',
      inner_BreadcrumbPath2: '',
    },
    referenceURL: '',
    commentAvailable: false,
    categoryposts: [],
    pagelabel: {
      starting_from_label: '',
      booknow_label_1: '',
      booknow_label_2: '',
      Validity_label: '',
      Region_label: '',
      Destination_label: '',
      Call_us_label: '',
      phone_number: '',
      Share_on_text: '',
      facebook: '',


    },
    commatags: false,
    moretags: [],
    searhRes: true,
    links: {},
    addCommentForm: { Name: '', Email: '', WebURL: '', Comment: '' },
    categorycount: 0,
    sortBy: 'price_high',
    isResult: false,
    search: '',
    tabs: [],
    gallery_images: [],
    Banner: { Title: '', Banner_Image: '' }
  },
  computed: {
    filteredList() {
      var filters = this.newsevents.sort((a, b) => {
        if (this.sortBy == 'price_high') {
          return b.News_or_Event_Date - a.News_or_Event_Date;
        }

      });
      filters.reverse();
      return filters.slice(0, 4);
    },
    searchList() {
      var start = 0
      var end = 0
      var searching = this.newsevents.filter(pack => pack.News_or_Event_Title.toLowerCase().includes(this.search.toLowerCase()))

      if (this.search.length > 0) {
        end = searching.length;
        if (end == 0) { this.searhRes = false; }
        else
          this.searhRes = true;

        return searching.slice(start, end);

      }
    }

  },
  methods: {
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;
      getAgencycode(function (response) {
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        // var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Package Page/Package Page/Package Page.ftl';
        var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/News and Events/News and Events/News and Events.ftl';
        var cmsurl = huburl + portno + homecms;
        newseventurl = getQueryStringValue('page');
        self.referenceURL = newseventurl;
        if (newseventurl != "") {

          newseventurl = newseventurl.split('-').join(' ');
          //  langauage = newseventurl.split('_')[1];
          newseventurl = newseventurl.split('_')[0];
          self.pageURLLink = newseventurl;
          var tonewsurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + newseventurl + '.ftl';
          var banner = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Banners/Banners/Banners.ftl';
          axios.get(tonewsurl, {
            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
          }).then(function (response) {
            self.content = response.data;
            var pagecontent = self.pluck('News_and_Events', self.content.area_List);
            if (pagecontent.length > 0) {
              self.newsandevent.content = self.pluckcom('News_or_Event_Content', pagecontent[0].component);
              self.newsandevent.date = self.pluckcom('News_or_Event_Date', pagecontent[0].component);
              self.newsandevent.event_image = self.pluckcom('News_or_Event_Image', pagecontent[0].component);
              self.newsandevent.Page_title = self.pluckcom('News_or_Event_Title', pagecontent[0].component);
              self.newsandevent.category = self.pluckcom('Category_Type', pagecontent[0].component);
              self.newsandevent.Tags_Type = self.pluckcom('Tags_Type', pagecontent[0].component);
              self.newsandevent.User_Type = self.pluckcom('User_Type', pagecontent[0].component);



            }
            var mediaLinks = self.pluck('Social_Media_Links', self.content.area_List);
            if (mediaLinks.length > 0) {
              self.links.facebook = self.pluckcom('Event_Facebook_Link', mediaLinks[0].component);
              self.links.twitter = self.pluckcom('Event_Twitter_Link_', mediaLinks[0].component);
              self.links.insta = self.pluckcom('Event_Instagram_Link', mediaLinks[0].component);
              self.links.linkedin = self.pluckcom('Event_LinkedIn_Link', mediaLinks[0].component);
              self.links.youtube = self.pluckcom('Event_Youtube_Link', mediaLinks[0].component);
            }


          }).catch(function (error) {
            window.location.href = "/Alwafra/news-detail.html";
            self.content = [];
            self.getdata = true;
          });

          axios.get(cmsurl, {
            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
          }).then(function (response) {
            console.log(response.data);
            self.content = response.data;
            let newseventListTemp = [];
            if (self.content != undefined && self.content.Values != undefined) {
              newseventListTemp = self.content.Values.filter(function (el) {
                return el.status == true && el.News_or_Event_Title != null
              });
            }
            self.newsevents = newseventListTemp;
            // self.seperatetag();
            setTimeout(function () { self.seperatetag(); }, 500);
            self.morefiltertags();

            // self.morefiltertags();
            self.categorypost();


          }).catch(function (error) {
            console.log('Error');
            self.content = [];
          });
          axios.get(banner, {
            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
          }).then(function (response) {
            self.content = response.data;
            var Banner = self.pluck('News_and_Events_Banner', self.content.area_List);
            if (Banner.length > 0) {
              self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
              self.Banner.Title = self.pluckcom('Title', Banner[0].component);
              self.Banner.Breadcrumb_1 = self.pluckcom('Breadcrumb_1', Banner[0].component);
              self.Banner.Breadcrumb_2 = self.pluckcom('Breadcrumb_2', Banner[0].component);
            }
          }).catch(function (error) {
            console.log('Error');
          });

        }




      });
    },
    getmoreinfo(url) {
      if (url != null) {
        if (url != "") {
          url = url.split("/Template/")[1];
          url = url.split(' ').join('-');
          url = url.split('.').slice(0, -1).join('.');
          url = "news-detail.html?page=" + url + "&from=pkg";
          window.location.href = url;



        }
        else {
          url = "#";
        }
      }
      else {
        url = "#";
      }
      return url;
    },
    gotopage: function (tag) {
      console.log(tag);
      url = "news-event.html?redirectpage=" + tag + "&from=pkg";
      window.location.href = url;

    },
    gotopagecategory: function (categ) {
      console.log(categ);
      var eventnews = this.newsevents;
      var categoriesin = [];
      for (i = 0; i < eventnews.length; i++) {
        if (eventnews[i].Tags_Type == categ) {
          var sele = eventnews[i].News_or_Event_Title;
          categoriesin.push(sele);

        }
        //  localStorage.setItem("taggeditems", JSON.stringify(tagsin));
        url = "news-event.html?redirectcategorypage=" + categ + "&from=pkg";
        window.location.href = url;
      }
    },

    addComment: function () {
      let requestedDate = new Date();
      let self = this;
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.addCommentForm.Email.match(emailPat);
      if (this.addCommentForm.Name == undefined || this.addCommentForm.Name == '') {
        alertify.alert('Warning', "Please enter name");
        return;
      } else if (this.addCommentForm.Email == undefined || this.addCommentForm.Email == '') {
        alertify.alert('Warning', "Please enter email");
        return;

      } else if (matchArray == null) {
        alertify.alert('Warning', "Your email seems incorrect").set('closable', false);
        return false;
      } else if (this.addCommentForm.Comment == undefined || this.addCommentForm.Comment == '') {
        alertify.alert('Warning', 'Please enter comment to continue');
        return;
      } else {
        var frommail = 'uaecrt@gmail.com'
        var custmail = {
          fromEmail: frommail,
          toEmail: this.email,
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.username,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
          type: "News and Event Comments",
          keyword10: self.newsandevent.Page_title,
          keyword1: self.addCommentForm.Name,
          date1: requestedDate,
          keyword2: self.addCommentForm.Email,
          text1: self.addCommentForm.Comment,
          keyword3: self.addCommentForm.WebURL,
          keyword4: self.referenceURL,
          nodeCode: agencyCode
        };
        let responseObject = this.cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
          let insertID = Number(responseObject);
          alertify.alert("Success", "Your comment successfully added");
          self.addCommentForm.Name = "";
          self.addCommentForm.Email = "";
          self.addCommentForm.Comment = "";
          self.addCommentForm.WebURL = "";
          setTimeout(function () { self.viewComment(); }, 3000);
          // self.viewReview();
        } catch (e) {

        }



      }
    },
    datemonthFormatter: function (utc) {

      return (moment(utc).utcOffset("+05:30").format('dddd').slice(0, 3));

    },
    dateonlyFormatter: function (utc) {

      return (moment(utc).utcOffset("+05:30").format('DD-MM-YYYY'));

    },
    categorypost: function () {
      var category = [];
      var eventcategory = this.newsevents;
      for (i = 0; i < eventcategory.length; i++) {
        var categories = eventcategory[i].Category_Type;
        category.push(categories);

      }
      this.categorycount = category.length;
      // var arr = ["jam", "beef", "cream", "jam"]
      this.categoryposts = category.reduce((acc, val) => {
        acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
        return acc;
      }, {});
    },
    seperatetag: function () {
      var tags = this.newsandevent.Tags_Type;
      var tagsin = [];
      var sele = [];
      if (tags.includes(',')) {
        sele = tags.split(",");
        for (i = 0; i < sele.length; i++) {
          tagsin.push(sele[i]);
        }
        this.moretagsaval = tagsin;
        this.commatags = true;
        console.log(tagsin);

      }
    },
    morefiltertags: function () {
      var eventnews = this.newsevents;
      var tags = [];
      var selec = [];
      for (i = 0; i < eventnews.length; i++) {
        var taging = eventnews[i].Tags_Type;
        if (taging.includes(',')) {
          var lastChar = taging.slice(-1);
          if (lastChar == ',') {
            taging = taging.slice(0, -1);
          }
          // taging=taging.split(",")[0];
          selec = taging.toString();
          selec = taging.split(",");
          
          for (p = 0; p < selec.length; p++) {
            tags.push(selec[p]);
            // this.moretags = tags;
          }
        }
        else
          tags.push(taging);
      }
      var outputArray = [];
      var count = 0;
      var start = false;

      for (j = 0; j < tags.length; j++) {
        for (k = 0; k < outputArray.length; k++) {
          if (tags[j] == outputArray[k]) {
            start = true;
          }
        }
        count++;
        if (count == 1 && start == false) {
          outputArray.push(tags[j]);
        }
        this.filteredtagarray = outputArray;
        start = false;
        count = 0;
      }

      console.log(this.filteredtagarray);
    },
    async cmsRequestData(callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: { 'Content-Type': 'application/json' },
        body: data, // body data type must match "Content-Type" header
      });
      try {
        const myJson = await response.json();
        return myJson;
      } catch (error) {
        return object;
      }
    },

    // dateFormatter: function (utc) {
    //   return (moment(utc).utcOffset("+05:30").format("DD-MMM-YYYY"));
    // },
    getAmount: function (amount) {
      amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
      return amount;
    },
    viewComment: function () {
      // this.$nextTick(function () {
      var self = this;
      let allComment = [];
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
      let requestObject = { from: 0, size: 100, type: "News and Event Comments", nodeCode: agencyCode, orderBy: "desc" };
      let responseObject = this.cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
        if (responseObject != undefined && responseObject.data != undefined) {
          allResult = JSON.parse(JSON.stringify(responseObject)).data;
          for (let i = 0; i < allResult.length; i++) {
            if ((allResult[i].keyword4) == (self.referenceURL)) {
              let object = {
                Name: allResult[i].keyword1,
                Date: moment(String(allResult[i].date1), "YYYY-MM-DDThh:mm:ss").format('MMM DD,YYYY'),
                comment: allResult[i].text1,
              };
              allComment.push(object);
            }
          }
          self.allComment = allComment;
          self.commentCount = allComment.length;
          if (self.commentCount > 0) {
            self.commentAvailable = true;
          }



        }

      });
      // });

    },


  },

  mounted: function () {

    this.getPagecontent();
    sessionStorage.active_e = 3;
    this.viewComment();


  },

});

function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}



