var aboutus = new Vue({
    el: '#aboutuspage',
    name: 'aboutuspage',
    data: {
        // content: null,
        getdata: false,
        aboutUsContent: {
            title: '',
            descriptionImage: '',
            description: '',
        },
        Feature:{},
        Banner:{Banner_Image:''}
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/About us/About us/About us.ftl';
                var banner = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Banners/Banners/Banners.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var Aboutcontent = self.pluck('main', self.content.area_List);
                    if (Aboutcontent.length > 0) {
                        self.aboutUsContent.title = self.pluckcom('Title', Aboutcontent[0].component);
                    }
                    var AboutcontentDetail = self.pluck('content', self.content.area_List);
                    if (AboutcontentDetail.length > 0) {
                        self.aboutUsContent.descriptionImage = self.pluckcom('Description_Image', AboutcontentDetail[0].component);
                        self.aboutUsContent.description = self.pluckcom('About_Us_Content', AboutcontentDetail[0].component);
                    }


                    var Feature = self.pluck('About_Us_List', self.content.area_List);
                    if (Feature.length > 0) {

                        self.Feature.Features_List = self.pluckcom('List', Feature[0].component);

                    }

                    self.getdata = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.aboutUsContent = [];
                });
                axios.get(banner, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var Banner = self.pluck('About_Us_Banner', self.content.area_List);
                    if (Banner.length > 0) {
                        self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
                        self.Banner.Title = self.pluckcom('Title', Banner[0].component);
                        self.Banner.Breadcrumb_1 = self.pluckcom('Breadcrumb_1', Banner[0].component);
                        self.Banner.Breadcrumb_2 = self.pluckcom('Breadcrumb_2', Banner[0].component);
                    }
                }).catch(function (error) {
                    console.log('Error');
                });


            });

        },
    },
    mounted: function () {
        this.getPagecontent();
        sessionStorage.active_e=5;
    },
})