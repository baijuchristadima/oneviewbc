
const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var holidypack = new Vue({
  i18n,
  el: '#holiday',
  name: 'holiday',
  data() {
    return {
      key: '',
      content: null,
      getdata: true,
      selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
      CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
      packages: '',
      pagebanner:{
        Book_Now_Label: "Book now",
        Day_Label: "Days",
        Night_Label: "Nights"
      },
      getpackage: false,
      allReview: [],
      isLoading: false,
      fullPage: true,
    }

  },
  mounted: function () {
    this.viewReview();
    this.getpagecontent();
  },
  methods: {
    moment: function () {
      return moment();
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getpagecontent: function () {
      this.isLoading = true;
      if (localStorage.getItem("AgencyCode") === null) {
        localStorage.AgencyCode = JSON.parse(localStorage.User).loginNode.code;

      }
      var Agencycode = localStorage.AgencyCode;
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
      var self = this;
      // var packagecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Holidays/Holidays/Holidays.ftl';
      var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Package List/Package List/Package List.ftl';
      var cmsurl = huburl + portno + homecms;
      // banner and labels
      var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Packages/Packages/Packages.ftl';
          axios.get(pageurl, {
            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
          }).then(function (response) {
            self.content = response.data;
            var pagecontent = self.pluck('Banner_Area', self.content.area_List);
            if (pagecontent.length > 0) {
              self.pagebanner.Breadcrumb1 = self.pluckcom('Breadcrumb1', pagecontent[0].component);
              self.pagebanner.Breadcrumb2 = self.pluckcom('Breadcrumb2', pagecontent[0].component);
              self.pagebanner.Banner_Title = self.pluckcom('Banner_Title', pagecontent[0].component);
              self.pagebanner.Banner_Image = self.pluckcom('Banner_Image', pagecontent[0].component);
              // self.pagebanner.Day_Label = self.pluckcom('Day_Label', pagecontent[0].component);
              // self.pagebanner.Night_Label = self.pluckcom('Night_Label', pagecontent[0].component);
              
            }
            var pagecontentsLabel = self.pluck('Labels', self.content.area_List);
            if (pagecontentsLabel.length > 0) {
              self.pagebanner.Book_Now_Label = self.pluckcom('Book_Now_Label', pagecontentsLabel[0].component);
              self.pagebanner.Day_Label = self.pluckcom('Day_Label', pagecontentsLabel[0].component);
              self.pagebanner.Night_Label = self.pluckcom('Night_Label', pagecontentsLabel[0].component);
            }
            

          }).catch(function (error) {
            console.log('Error');
            self.content = [];
          });


      axios.get(cmsurl, {
        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
      }).then(function (response) {
        self.isLoading = true;
        self.content = response.data;
        let holidayPackageListTemp = [];
        if (self.content != undefined && self.content.Values != undefined) {
          holidayPackageListTemp = self.content.Values.filter(function (el) {
            return el.Status == true
          });
        }
        self.packages = holidayPackageListTemp;
        self.getpackage = true;
        self.isLoading = false;

      }).catch(function (error) {
        console.log('Error');
        self.content = [];
      });
      
      // axios.get(pageurl, {
      //   headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
      // }).then(function (response) {
      //   self.content = response.data;
      //   var pagecontents = self.pluck('Labels', self.content.area_List);
      //       if (pagecontents.length > 0) {
      //         self.pagebanner.Book_Now_Label = self.pluckcom('Book_Now_Label', pagecontents[0].component);
      //         self.pagebanner.Day_Label = self.pluckcom('Day_Label', pagecontents[0].component);
      //         self.pagebanner.Night_Label = self.pluckcom('Night_Label', pagecontents[0].component);
      //       }

        

      // }).catch(function (error) {
      //   console.log('Error');
      //   self.content = [];
      // });
    },
    getmoreinfo(url) {
      if (url != null) {
        if (url != "") {
          url = url.split("/Template/")[1];
          url = url.split(' ').join('-');
          url = url.split('.').slice(0, -1).join('.')
          url = "/A2Z/demo-page/holidaydetails.html?page=" + url;
         
          console.log(this.url);
          window.location.href = url;
        }
        else {
          url = "#";
        }
      }
      else {
        url = "#";
      }
      console.log(this.url);
      return url;

    },
    viewReview: async function () {
      var self = this;
      self.isLoading = true;
      self.isLoading = true;
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
      let requestObject = { from: 0, size: 100, type: "Reviews", nodeCode: agencyCode, orderBy: "desc" };
      let responseObject = await this.cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
        if (responseObject != undefined && responseObject.data != undefined) {
          self.allReview = JSON.parse(JSON.stringify(responseObject)).data;

        }
        self.isLoading = false;
      });
    },
    async cmsRequestData(callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: { 'Content-Type': 'application/json' },
        body: data, // body data type must match "Content-Type" header
      });
      try {
        const myJson = await response.json();
        return myJson;
      } catch (error) {
        return object;
      }
    },
    getrating: function (url) {
      var self = this;
      url = url.split("/Template/")[1];
      url = url.split(' ').join('-');
      url = url.split('.').slice(0, -1).join('.');
      url = url.split('-').join(' ');
      url = url.split('_')[0];
      let reviewrate = [];
      reviewrate = self.allReview;
      reviewrate=reviewrate.filter(r => r.keyword4 == url)
      var sum = 0;
      if (reviewrate.length > 0) {
        $.each(reviewrate, function () {
          sum += this.number1 ? this.number1 : 0;
        })
        sum = sum / reviewrate.length;
      }
      console.log(sum);
      return sum;
    },
  },
  filters: {
    moment: function (date) {
      return moment(date).format('DD MMM YYYY');
    }
  }

});
function owlcarosl() {

  $("#homepackage").owlCarousel({
    navigation: true, // Show next and prev buttons
    slideSpeed: 2000,
    pagination: false,
    paginationSpeed: 2000,
    singleItem: false,
    dots: true,
    mouseDrag: true,
    items: 4,
    transitionStyle: "goDown",
    itemsCustom: [
      [0, 1],
      [450, 1],
      [600, 2],
      [700, 2],
      [1000, 3],
      [1200, 3]
    ],
  });
}

var count=0;
    
    function myFun(f) {
    
    count += 1;
    
    f.myText.value = count;
    
    }
var header = new Vue({
  el: 'head',
  name: 'header',

});