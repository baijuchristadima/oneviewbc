const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var packageView = new Vue({
  i18n,

  el: '#Holidaydetails',
  name: 'Holidaydetails',
  data: {
    content: null,
    packagecontent: {
      Image: "",
      Days: "",
      Night: "",
      Price: "",
      Package_Description: "",
      Itinerary: "",
      Post_Button_Label: ""
    },
    getpackage: false,
    getdata: false,
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
    CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    pagecontent: {
      Breadcrum1: '',
      Breadcrumb2: '',
    },
    review: {},
    Traveller_Limit_Count: '',
    //booking form
    cntusername: '',
    cntemail: '',
    cntcontact: '',
    cntdate: '',
    cntperson: 1,
    CoupenCode: '',
    grandtotal: '',
    CoupenInfo: {
      Amount: "",
      ApplyCoupen: false,
      discountpercent: false,
      CoupenCode: "",
      newPrice: "",
      DisPercent: "",
      Disamount: ""
    },
    Travellercount: {},
    Coupon_Success_Message: '',

    //review form
    username: '',
    email: '',
    contact: '',
    comment: '',
    overitems: null,
    banner: {
      Banner_Image: '',
      Banner_Title: ''
    },
    allReview: [],
    pageURLLink: '',
    pagebanner: {
      Breadcrumb1: "",
      Breadcrumb3: "",
      Inner_Banner_Title: ""

    },
    pagelabels: {},
    alerts: {},
    alerttypes: {
      Success: "",
      warning: "",
      Booking: "",
      Review: ""
    },
    destination: '',
    allFlightInventory: [],
    allHotelInventory: [],
    allMappedFlights: [],
    allMappedHotels: [],
    allDepartureList: [],
    allAirlineList: [],
    allStarList: [],
    allHotelList: [],
    allNightList: [],
    selectedDeparture: {},
    selectedNight: {},
    selectedStar: {},
    selectedHotel: {},
    selectedAirLine: {},
    totalFareAmount: '0',
    Flight_Label: 'Flight Details',
    Hotel_Label: 'Hotel Details',
    Check_In_Label: 'Check In',
    Check_Out_Label: 'Check Out',
    Room_Type_Label: 'Room Type',
    isrtl: false,
    isLoading: false,
    fullPage: true,
    socialMediaList: []
  },
  mounted: function () {
    this.getPageTitle();
    this.recreateDatePicker();
    var self = this;
    // this.getBanner();
    this.viewReview();
    var dateFormat = "dd/mm/yy";
    from = $("#from").datepicker({
      minDate: "0d",
      maxDate: "360d",
      numberOfMonths: 1,
      dateFormat: dateFormat,
      showOn: "both",
      buttonText: "<i class='fa fa-calendar'></i>",
      duration: "fast",
      showAnim: "slide",
      showOptions: {
        direction: "up"
      },
      onSelect: function (selectedDate, date) {
        let dateObj = document.getElementById("from");
        dateObj.value = selectedDate;
        $("#from").datepicker("setDate", selectedDate);
        self.constructDefaultValues(self.destination, selectedDate);
      }
    });

  },
  methods: {
    recreateDatePicker: function () {
      var systemDateFormat = generalInformation.systemSettings.systemDateFormat;

      var self = this;
      Vue.nextTick(function () {
        $("#traveldate").datepicker({
          minDate: "0d",
          maxDate: "360d",
          numberOfMonths: 1,
          changeMonth: true,
          autoclose: true,
          showButtonPanel: false,
          // onSelect: function (selectedDate) {
          //     // $(this).parents('.txt_animation').addClass('focused');
          // }
        });



      }.bind(self));
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    onChange(event) {
      console.log(event.target.value)
    },
    defaultValues: function () {
      this.allFlightInventory = [];
      this.allHotelInventory = [];
      this.allMappedFlights = [];
      this.allMappedHotels = [];
      this.allDepartureList = [];
      this.allAirlineList = [];
      this.allStarList = [];
      this.allHotelList = [];
      this.allNightList = [];
      this.selectedDeparture = {};
      this.selectedNight = {};
      this.selectedStar = {};
      this.selectedHotel = {};
      this.selectedAirLine = {};
    },
    getPageTitle: function () {
      var self = this;
      self.isLoading = true;
      self.defaultValues();
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        if (langauage == 'ar') {
          self.isrtl = true;
        } else {
          self.isrtl = false;
        }
        // banner and labels
        var flightInventory = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Inventory - Flight/Inventory - Flight/Inventory - Flight.ftl';
        var hotelInventory = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Inventory - Hotel/Inventory - Hotel/Inventory - Hotel.ftl';
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Packages/Packages/Packages.ftl';
        axios.get(pageurl, {
          headers: {
            'content-type': 'text/html',
            'Accept': 'text/html',
            'Accept-Language': langauage
          }
        }).then(function (response) {
          self.content = response.data;

          var maincontent = self.pluck('Common_Area', response.data.area_List);
          if (maincontent.length > 0) {
            // self.packagecontent = maincontent[0].component;
            self.Traveller_Limit_Count = self.pluckcom('Traveller_Limit_Count', maincontent[0].component);
            self.Coupon_Success_Message = self.pluckcom('Coupon_Success_Message', maincontent[0].component);
            self.alerttypes.Success = self.pluckcom('Success_alert_Type', maincontent[0].component);
            self.alerttypes.warning = self.pluckcom('Warning_Alert_Type', maincontent[0].component);
            self.alerttypes.Booking = self.pluckcom('Booking_Alert_Type', maincontent[0].component);
            self.alerttypes.Review = self.pluckcom('Review_Alert_Type', maincontent[0].component);
            self.socialMediaList = self.pluckcom('Social_Media_List', maincontent[0].component);
            self.Travellercounts();
          }

          var pagecontent = self.pluck('Banner_Area', self.content.area_List);
          if (pagecontent.length > 0) {
            self.pagebanner.Breadcrumb1 = self.pluckcom('Breadcrumb1', pagecontent[0].component);
            self.pagebanner.Breadcrumb3 = self.pluckcom('Breadcrumb3', pagecontent[0].component);
            self.pagebanner.Inner_Banner_Title = self.pluckcom('Inner_Banner_Title', pagecontent[0].component);
          }
          var pagecontent = self.pluck('Labels', self.content.area_List);
          if (pagecontent.length > 0) {
            self.pagelabels.Book_Now_Label = self.pluckcom('Book_Now_Label', pagecontent[0].component);
            self.pagelabels.Day_Label = self.pluckcom('Day_Label', pagecontent[0].component);
            self.pagelabels.Night_Label = self.pluckcom('Night_Label', pagecontent[0].component);
            self.pagelabels.Review_Label = self.pluckcom('Review_Label', pagecontent[0].component);
            self.pagelabels.Per_Person_Label = self.pluckcom('Per_Person_Label', pagecontent[0].component);
            self.pagelabels.Name_Placeholder = self.pluckcom('Name_Placeholder', pagecontent[0].component);
            self.pagelabels.Email_Label = self.pluckcom('Email_Label', pagecontent[0].component);
            self.pagelabels.Email_Placeholder = self.pluckcom('Email_Placeholder', pagecontent[0].component);
            self.pagelabels.Phone_number_Label = self.pluckcom('Phone_number_Label', pagecontent[0].component);
            self.pagelabels.Phone_Number_Placeholder = self.pluckcom('Phone_Number_Placeholder', pagecontent[0].component);
            self.pagelabels.Tour_Date_Label = self.pluckcom('Tour_Date_Label', pagecontent[0].component);
            self.pagelabels.Tour_Date_Placeholder = self.pluckcom('Tour_Date_Placeholder', pagecontent[0].component);
            self.pagelabels.Number_of_Person_Label = self.pluckcom('Number_of_Person_Label', pagecontent[0].component);
            self.pagelabels.Number_of_Person_Placeholder = self.pluckcom('Number_of_Person_Placeholder', pagecontent[0].component);
            self.pagelabels.Book_Now_Label = self.pluckcom('Book_Now_Label', pagecontent[0].component);
            self.pagelabels.Write_Review_Label = self.pluckcom('Write_Review_Label', pagecontent[0].component);
            self.pagelabels.Email_Label = self.pluckcom('Email_Label', pagecontent[0].component);
            self.pagelabels.Phone_number_Label = self.pluckcom('Phone_number_Label', pagecontent[0].component);
            self.pagelabels.Comment_Label = self.pluckcom('Comment_Label', pagecontent[0].component);
            self.pagelabels.Post_Button_Label = self.pluckcom('Post_Button_Label', pagecontent[0].component);
            self.pagelabels.Name_Label = self.pluckcom('Name_Label', pagecontent[0].component);
            self.pagelabels.Table_Type_Label = self.pluckcom('Table_Type_Label', pagecontent[0].component);
            self.pagelabels.Table_Quantity_Label = self.pluckcom('Table_Quantity_Label', pagecontent[0].component);
            self.pagelabels.Table_Subtotal_Label = self.pluckcom('Table_Subtotal_Label', pagecontent[0].component);
            self.pagelabels.Table_Adult_Label = self.pluckcom('Table_Adult_Label', pagecontent[0].component);
            self.pagelabels.Total_Label = self.pluckcom('Total_Label', pagecontent[0].component);
            self.pagelabels.Promo_Code_Label = self.pluckcom('Promo_Code_Label', pagecontent[0].component);
            self.pagelabels.Grand_Total_Label = self.pluckcom('Grand_Total_Label', pagecontent[0].component);
            self.pagelabels.Apply_Now_Label = self.pluckcom('Apply_Now_Label', pagecontent[0].component);
            self.pagelabels.Rating_Label = self.pluckcom('Rating_Label', pagecontent[0].component);
            self.pagelabels.Coupon_Code_Placeholder = self.pluckcom('Coupon_Code_Placeholder', pagecontent[0].component);
            self.pagelabels.Discount_Percent_Label = self.pluckcom('Discount_Percent_Label', pagecontent[0].component);
            self.pagelabels.Departure_From_Label = self.pluckcom('Departure_From_Label', pagecontent[0].component);
            self.pagelabels.Flight_Option_Label = self.pluckcom('Flight_Option_Label', pagecontent[0].component);
            self.pagelabels.Hotel_Option_Label = self.pluckcom('Hotel_Option_Label', pagecontent[0].component);
            self.pagelabels.Hotel_Label = self.pluckcom('Hotel_Label', pagecontent[0].component);
            self.pagelabels.Night_Option_Label = self.pluckcom('Night_Option_Label', pagecontent[0].component);
            self.Flight_Label = self.pluckcom('Flight_Details_Label', pagecontent[0].component);
            self.Hotel_Label = self.pluckcom('Hotel_Details_Label', pagecontent[0].component);
            self.Check_In_Label = self.pluckcom('Check_In_Label', pagecontent[0].component);
            self.Check_Out_Label = self.pluckcom('Check_Out_Label', pagecontent[0].component);
            self.Room_Type_Label = self.pluckcom('Room_Type_Label', pagecontent[0].component);


          }

          var pagecontent = self.pluck('Alert_Messages', self.content.area_List);
          if (pagecontent.length > 0) {
            self.alerts.Name_Alert_Message = self.pluckcom('Name_Alert_Message', pagecontent[0].component);
            self.alerts.Email_Alert_Message = self.pluckcom('Email_Alert_Message', pagecontent[0].component);
            self.alerts.Phone_Number_Alert_Message = self.pluckcom('Phone_Number_Alert_Message', pagecontent[0].component);
            self.alerts.Tour_Date_Alert_Message = self.pluckcom('Tour_Date_Alert_Message', pagecontent[0].component);
            self.alerts.Coupon_Error_Alert_Message = self.pluckcom('Coupon_Error_Alert_Message', pagecontent[0].component);
            self.alerts.Rating_Alert_Message = self.pluckcom('Rating_Alert_Message', pagecontent[0].component);
            self.alerts.Comment_Alert_Message = self.pluckcom('Comment_Alert_Message', pagecontent[0].component);
            self.alerts.Enter_Coupon_Message = self.pluckcom('Enter_Coupon_Message', pagecontent[0].component);
            self.alerts.Incorrect_Mail_Alert_Message = self.pluckcom('Incorrect_Mail_Alert_Message', pagecontent[0].component);
            self.alerts.Valid_Phone_Number_Message = self.pluckcom('Valid_Phone_Number_Message', pagecontent[0].component);
            self.alerts.Success_Booking_Message = self.pluckcom('Success_Booking_Message', pagecontent[0].component);
            self.alerts.Review_Thanks_Message = self.pluckcom('Review_Thanks_Message', pagecontent[0].component);
          }
          self.isLoading = false;
          var packageurl = getQueryStringValue('page');
          var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
          if (packageurl != "") {
            packageurl = packageurl.split('-').join(' ');
            packageurl = packageurl.split('_')[0];
            var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
            self.pageURLLink = packageurl;
            axios.get(topackageurl, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': langauage
              }
            }).then(function (response) {

              var maincontent = self.pluck('Package', response.data.area_List);
              if (maincontent.length > 0) {
                self.packagecontent.Title = self.pluckcom('Title', maincontent[0].component);
                self.packagecontent.Image = self.pluckcom('Image', maincontent[0].component);
                self.packagecontent.Days = self.pluckcom('Days', maincontent[0].component);
                self.packagecontent.Night = self.pluckcom('Night', maincontent[0].component);
                self.packagecontent.Price = self.pluckcom('Price', maincontent[0].component);
                self.packagecontent.Basic_Price = self.pluckcom('Basic_Price', maincontent[0].component);
                self.packagecontent.Package_Description = self.pluckcom('Package_Description', maincontent[0].component);
                self.packagecontent.Itinerary = self.pluckcom('Itinerary', maincontent[0].component);
                self.packagecontent.Post_Button_Label = self.pluckcom('Post_Button_Label', maincontent[0].component);
                var destination = self.pluckcom('Destination', maincontent[0].component);
                axios.get(flightInventory, {
                  headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                  }
                }).then(function (response) {
                  if (response.data != undefined && response.data.Values != undefined) {
                    self.allFlightInventory = response.data.Values;
                    axios.get(hotelInventory, {
                      headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                      }
                    }).then(function (response) {
                      if (response.data != undefined && response.data.Values != undefined) {
                        self.allHotelInventory = response.data.Values;
                        var datevalue = moment(new Date().setDate(new Date().getDate() + 1)).format('DD/MM/YYYY');
                        self.constructDefaultValues(destination, datevalue);
                        self.calc();
                      }
                    }).catch(function (error) {
                      var datevalue = moment(new Date().setDate(new Date().getDate() + 1)).format('DD/MM/YYYY');
                      self.constructDefaultValues(destination, datevalue);
                      self.calc();
                    });;

                  }
                }).catch(function (error) {
                  self.calc();
                });;
              }
            })
          }

        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });


      });
    },
    setDefaultTime: function (date) {
      date.setHours(0);
      date.setMinutes(0);
      date.setSeconds(0);
    },
    getUIDate: function (date) {
      return moment(date, 'YYYY-MM-DD HH:mm:ss').format('DD MMM,HH:mm');
    },
    constructDefaultValues: function (destination, datevalue) {
      this.destination = destination;
      var self = this;
      let dateObj = document.getElementById("from");
      dateObj.value = datevalue;
      var tourDate = new Date(moment(dateObj.value, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
      self.setDefaultTime(tourDate);
      var allDeparturePlace = [];
      var allStarPlace = [];
      if (this.allFlightInventory != undefined && destination != undefined) {
        this.allMappedFlights = this.allFlightInventory.filter(function (flight) {
          var fromdate = new Date(moment(flight.From_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
          var toDate = new Date(moment(flight.To_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
          self.setDefaultTime(fromdate);
          self.setDefaultTime(toDate);
          return flight.Destination == destination && tourDate.getTime() >= fromdate.getTime() && toDate.getTime() >= tourDate.getTime();
        });
      }
      if (this.allHotelInventory != undefined && destination != undefined) {
        this.allMappedHotels = this.allHotelInventory.filter(function (flight) {
          var fromdate = new Date(moment(flight.From_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
          var toDate = new Date(moment(flight.To_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
          self.setDefaultTime(fromdate);
          self.setDefaultTime(toDate);
          return flight.Destination == destination && flight.Active == true && tourDate.getTime() >= fromdate.getTime() && toDate.getTime() >= tourDate.getTime();
        });
      }

      this.allMappedFlights.forEach(element => {
        var isthere = false;
        var sourceTime = "";

        for (var k = 0; k < allDeparturePlace.length; k++) {
          if (allDeparturePlace[k].Name == element.Source) {
            isthere = true;
            break;
          }
        }
        if (isthere == false) {
          allDeparturePlace.push({
            Name: element.Source
          });
        }
      });
      this.allMappedHotels.forEach(element => {
        var isthere = false;
        for (var k = 0; k < allStarPlace.length; k++) {
          if (allStarPlace[k].Id == element.Star_Rating) {
            isthere = true;
            break;
          }
        }
        if (isthere == false) {
          allStarPlace.push({
            Name: element.Star_Rating + " Star",
            Id: element.Star_Rating
          });
        }
      });
      if (allDeparturePlace.length > 0) {
        this.selectedDeparture = allDeparturePlace[0];
      }
      allStarPlace.sort(function (a, b) {
        return Number(a.Id) - Number(b.Id);
      });

      //allStarPlace= allStarPlace.sort(function(a, b){return a.Id<b.Id});
      if (allStarPlace.length > 0) {
        this.selectedStar = allStarPlace[0];
      }
      this.departureChangeEvent();
      this.startRatingChangeEvent();
      this.allDepartureList = allDeparturePlace;
      this.allStarList = allStarPlace;


    },
    startRatingChangeEvent: function () {
      var allHotelListTemp = [];
      var selectedHotelTemp;
      var self = this;
      this.allMappedHotels.forEach(element => {

        if (element.Star_Rating != undefined && element.Star_Rating == this.selectedStar.Id) {
          var isthere = false;
          for (var k = 0; k < allHotelListTemp.length; k++) {
            if (allHotelListTemp[k].Hotel_Name == element.Hotel_Name) {
              isthere = true;
              break;
            }
          }
          if (isthere == false) {
            allHotelListTemp.push(element);
          }
        }
      });
      if (allHotelListTemp.length > 0) {
        selectedHotelTemp = allHotelListTemp[0];

      }
      this.selectedHotel = selectedHotelTemp;
      this.hotelChangeEvent();
      this.allHotelList = allHotelListTemp;

    },
    hotelChangeEvent: function () {
      var allNightListTemp = [];
      var self = this;
      var selectedNightTemp = {};
      let dateObj = document.getElementById("from").value;
      var tourDate = new Date(moment(dateObj, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
      self.setDefaultTime(tourDate);
      if (this.selectedHotel != undefined && this.selectedHotel.Price != undefined) {
        for (var i = 0; i < this.selectedHotel.Price.length; i++) {
          var element = this.selectedHotel.Price[i];
          var fromdate = new Date(moment(element.From_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
          var toDate = new Date(moment(element.To_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
          self.setDefaultTime(fromdate);
          self.setDefaultTime(toDate);
          if (tourDate.getTime() >= fromdate.getTime() && toDate.getTime() >= tourDate.getTime()) {
            allNightListTemp.push(element);
          }
        }

      }
      if (allNightListTemp.length > 0) {
        selectedNightTemp = allNightListTemp[0];

      }


      this.selectedNight = selectedNightTemp;
      this.allNightList = allNightListTemp;
      this.nightChangeEvent();


    },
    hotelCheckinDateChangeEvent: function () {
      let dateObj = document.getElementById("from").value;
      var checkinDate = new Date(moment(dateObj, 'DD/MM/YYYY').format('YYYY-MM-DD'));
      if (this.selectedAirLine != undefined && this.selectedAirLine.data != undefined && this.selectedAirLine.data.Flight_Details != undefined && this.selectedAirLine.data.Flight_Details.length > 0) {
        var flight = this.selectedAirLine.data.Flight_Details[this.selectedAirLine.data.Flight_Details.length - 1];
        checkinDate = new Date(flight.Destination_Date);
      }
      var checkin = new Date(moment(checkinDate, 'YYYY-MM-DD').format('DD-MMMM-YYYY'));
      var checkOut = new Date(moment(checkinDate, 'YYYY-MM-DD').format('DD-MMMM-YYYY'));
      checkOut.setDate(checkOut.getDate() + Number(this.selectedNight.Nights));
      if (this.selectedHotel != undefined) {
        this.selectedHotel.checkin = moment(checkin).format('DD MMMM YYYY');;
        this.selectedHotel.checkOut = moment(checkOut).format('DD MMMM YYYY');
      }

    },
    nightChangeEvent: function () {
      var self = this;
      this.calc();
      this.hotelCheckinDateChangeEvent();
      this.$nextTick(function () {
        self.callOwl();
      }.bind(self));
    },
    callOwl: function () {

      $("#htl-in-item").owlCarousel({
        autoplay: true,
        autoPlay: 8000,
        autoplayHoverPause: true,
        stopOnHover: false,
        items: 1,
        margin: 10,
        lazyLoad: true,
        navigation: true,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [991, 1],
        itemsTablet: [600, 1]
      });

      $(".owl-prev").html('<i class="fa  fa-angle-left"></i>');
      $(".owl-next").html('<i class="fa  fa-angle-right"></i>');

    },
    departureChangeEvent: function () {
      var allAirlinesPlace = [];
      var selectedAirLineTemp = {};




      this.allMappedFlights.forEach(element => {
        if (element.Flight_Details != undefined && element.Flight_Details.length > 0 && element.Source == this.selectedDeparture.Name) {
          let dateObj = document.getElementById("from").value;
          var tourDate = new Date(moment(dateObj, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss'));
          var isthere = false;
          var sourceTime = "";
          if (element.Flight_Details.length > 0) {
            var fromdate = new Date(moment(element.Flight_Details[0].Source_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
            sourceTime = moment(fromdate).format('HH:mm');
          }
          if (sourceTime != undefined && sourceTime != '') {
            sourceTime = "(" + sourceTime + ")";
          }

          for (let i = 0; i < element.Flight_Details.length; i++) {
            var flightObj = element.Flight_Details[i];
            if (i == 0) {
              var fromdate = new Date(moment(flightObj.Source_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
              var toDate = new Date(moment(flightObj.Destination_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
              flightObj.Source_Date = moment(tourDate, 'YYYY-MM-DD[T]HH:mm:ss').format('YYYY-MM-DD') + " " + flightObj.Source_Date.split(' ')[1];
              var diffDays = toDate.getDate() - fromdate.getDate();
              tourDate.setDate(tourDate.getDate() + diffDays);
              flightObj.Destination_DateORG = flightObj.Destination_Date;
              flightObj.Destination_Date = moment(tourDate, 'YYYY-MM-DD[T]HH:mm:ss').format('YYYY-MM-DD') + " " + flightObj.Destination_Date.split(' ')[1];
            } else {
              var prevFlight = element.Flight_Details[i - 1];
              var fromdateSRC = new Date(moment(prevFlight.Destination_DateORG).format('YYYY-MM-DD[T]HH:mm:ss'));
              var toDateSRC = new Date(moment(flightObj.Source_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
              var diffDays = toDateSRC.getDate() - fromdateSRC.getDate();
              tourDate.setDate(tourDate.getDate() + diffDays);

              var fromdate = new Date(moment(flightObj.Source_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
              var toDate = new Date(moment(flightObj.Destination_Date).format('YYYY-MM-DD[T]HH:mm:ss'));
              diffDays = toDate.getDate() - fromdate.getDate();
              flightObj.Source_Date = moment(tourDate, 'YYYY-MM-DD[T]HH:mm:ss').format('YYYY-MM-DD') + " " + flightObj.Source_Date.split(' ')[1];
              tourDate.setDate(tourDate.getDate() + diffDays);
              flightObj.Destination_DateORG = flightObj.Destination_Date;
              flightObj.Destination_Date = moment(tourDate, 'YYYY-MM-DD[T]HH:mm:ss').format('YYYY-MM-DD') + " " + flightObj.Destination_Date.split(' ')[1];
            }
          }
          var flightOption = element.Flight_Details[0];
          for (var k = 0; k < allAirlinesPlace.length; k++) {
            if (allAirlinesPlace[k].Name == flightOption.Air_Line_Name + sourceTime) {
              isthere = true;
              break;
            }
          }


          if (isthere == false) {
            allAirlinesPlace.push({
              Name: flightOption.Air_Line_Name + sourceTime,
              data: element
            });
          }
        }



      });
      if (allAirlinesPlace.length > 0) {
        selectedAirLineTemp = allAirlinesPlace[0];

      }
      this.selectedAirLine = selectedAirLineTemp;
      this.allAirlineList = allAirlinesPlace;
      this.flightChangeEvent();
    },
    flightChangeEvent: function () {
      this.calc();
      this.hotelCheckinDateChangeEvent();
    },
    getAmount: function (amount) {
      amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
      return amount;
    },
    sendbooking: async function () {
      let dateObj = document.getElementById("from");
      let dateValue = dateObj.value;
      datevalue = moment(dateValue, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
      // dateValue = moment(String(dateValue)).format('YYYY-MM-DDThh:mm:ss');
      if (!this.cntusername) {
        alertify.alert(this.alerttypes.warning, this.alerts.Name_Alert_Message).set('closable', false);

        return false;
      }
      if (!this.cntemail) {
        alertify.alert(this.alerttypes.warning, this.alerts.Email_Alert_Message).set('closable', false);
        return false;
      }
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.cntemail.match(emailPat);
      if (matchArray == null) {
        alertify.alert(this.alerttypes.warning, this.alerts.Incorrect_Mail_Alert_Message).set('closable', false);
        return false;
      }
      if (!this.cntcontact) {
        alertify.alert(this.alerttypes.warning, this.alerts.Phone_Number_Alert_Message).set('closable', false);
        return false;
      }
      if (this.cntcontact.length < 8) {
        alertify.alert(this.alerttypes.warning, this.alerts.Valid_Phone_Number_Message).set('closable', false);
        return false;
      }
      if (dateValue == undefined || dateValue == '') {
        alertify.alert(this.alerttypes.warning, this.alerts.Tour_Date_Alert_Message).set('closable', false);
        return false;
      } else {
        this.isLoading = true;
        var frommail = JSON.parse(localStorage.User).loginNode.email;
        var custmail = {
          type: "UserAddedRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.cntusername,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;

        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');

        var originalPrice = this.grandtotal;
        if (this.CoupenInfo.ApplyCoupen == true) {
          originalPrice = this.CoupenInfo.newPrice;
        }
        var couponprice = 0.00;
        if (this.CoupenInfo.ApplyCoupen == true) {
          couponprice = this.CoupenInfo.Amount;
        }
        var cocode = "N/A";
        if (this.CoupenInfo.ApplyCoupen == true) {
          cocode = this.CoupenInfo.CoupenCode;
        }
        var percent = 0.00;
        var percentamt = 0.00;
        if (this.CoupenInfo.discountpercent == true) {
          percent = this.CoupenInfo.DisPercent;
          percentamt = this.CoupenInfo.Disamount;
        }


        let insertContactData = {
          type: "Booking",
          keyword1: this.cntusername,
          keyword2: this.cntemail,
          keyword4: this.cntcontact,
          keyword5: this.packagecontent.Title,
          keyword6: this.selectedCurrency,
          keyword7: this.packagecontent.Days,
          keyword8: this.packagecontent.Night,
          number2: this.cntperson,
          amount4: this.totalFareAmount,
          date1: datevalue,
          nodeCode: agencyCode,
          date2: requestedDate,
          amount3: this.grandtotal,
          amount2: originalPrice,
          amount1: couponprice,
          keyword3: cocode,
          amount5: percent,
          amount6: percentamt
        };
        let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
          let insertID = Number(responseObject);
          var emailApi = ServiceUrls.emailServices.emailApi;
          sendMailService(emailApi, custmail);
          this.cntemail = '';
          this.cntusername = '';
          this.cntcontact = '';
          this.cntdate = '';
          this.CoupenInfo.ApplyCoupen = false;
          this.CoupenInfo.Amount = "";
          this.CoupenInfo.discountpercent = false;
          this.CoupenInfo.CoupenCode = "";
          this.CoupenInfo.newPrice = "";
          this.CoupenCode = "";
          dateObj.value = null;
          $('#travel').val($('#travel option:first-child').val()).trigger('change');
          this.isLoading = false;
          alertify.alert('Booking', this.alerts.Success_Booking_Message);
        } catch (e) {

        }



      }
    },
    calculateFlightAndHotelAmount: function () {
      var flightRecommendAmount = 0;
      var flightActualAmount = 0;
      var hotelRecommendAmount = 0;
      var hotelActualAmount = 0;
      if (this.selectedAirLine.data != undefined && this.selectedAirLine.data.Recommend_Amount != undefined && this.selectedAirLine.data.Actual_Amount != undefined) {
        flightRecommendAmount = this.selectedAirLine.data.Recommend_Amount;
        flightActualAmount = this.selectedAirLine.data.Actual_Amount;
      }
      if (this.selectedNight != undefined && this.selectedNight.Recommend_Amount != undefined && this.selectedNight.Actual_Amount != undefined) {
        hotelRecommendAmount = this.selectedNight.Recommend_Amount;
        hotelActualAmount = this.selectedNight.Actual_Amount;
      }
      var totalRecommendAmount = Number(flightRecommendAmount) + Number(hotelRecommendAmount);
      var totalActualAmount = Number(flightActualAmount) + Number(hotelActualAmount);
      if (this.packagecontent.Basic_Price != undefined && this.packagecontent.Basic_Price != '') {
        this.totalFareAmount = Number(this.packagecontent.Basic_Price) + Number(totalActualAmount);
      } else {
        this.totalFareAmount = this.packagecontent.Price;
      }
      if (this.allNightList.length == 0) {
        this.totalFareAmount = this.packagecontent.Price;
      }
    },
    calc: function () {
      this.calculateFlightAndHotelAmount();
      this.grandtotal = this.cntperson * this.totalFareAmount;
      this.CoupenInfo.ApplyCoupen = false;
      this.CoupenInfo.Amount = "";
      this.CoupenInfo.discountpercent = false;
      this.CoupenInfo.CoupenCode = "";
      this.CoupenInfo.newPrice = "";
      this.CoupenCode = "";
      // alert("hai");
    },

    sendReview: async function () {
      let ratings = this.getUserRating();
      console.log(ratings);
      ratings = Number(ratings);
      if (ratings == 0) {
        alertify.alert(this.alerttypes.warning, this.alerts.Rating_Alert_Message);
        return;
      }
      if (!this.username) {
        alertify.alert(this.alerttypes.warning, this.alerts.Name_Alert_Message).set('closable', false);

        return false;
      }
      if (!this.email) {
        alertify.alert(this.alerttypes.warning, this.alerts.Email_Alert_Message).set('closable', false);
        return false;
      }
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.email.match(emailPat);
      if (matchArray == null) {
        alertify.alert(this.alerttypes.warning, this.alerts.Incorrect_Mail_Alert_Message).set('closable', false);
        return false;
      }
      if (this.contact.length < 8) {
        alertify.alert(this.alerttypes.warning, this.alerts.Valid_Phone_Number_Message).set('closable', false);
        return false;
      }
      if (!this.contact) {
        alertify.alert(this.alerttypes.warning, this.alerts.Phone_Number_Alert_Message).set('closable', false);
        return false;
      }

      if (!this.comment) {
        alertify.alert(this.alerttypes.warning, this.alerts.Comment_Alert_Message).set('closable', false);
        return false;
      } else {
        this.isLoading = true;
        var frommail = JSON.parse(localStorage.User).loginNode.email;
        var custmail = {
          type: "UserAddedRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(this.email) ? this.email : [this.email],
          logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          personName: this.username,
          primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
          secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
        };
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
          type: "Reviews",
          keyword1: this.username,
          keyword2: this.email,
          keyword3: this.contact,
          keyword4: this.pageURLLink,
          text1: this.comment,
          keyword5: this.packagecontent.Title,
          number1: ratings,
          nodeCode: agencyCode,
          date1: requestedDate,
        };
        let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
          let insertID = Number(responseObject);
          var emailApi = ServiceUrls.emailServices.emailApi;
          sendMailService(emailApi, custmail);
          this.email = '';
          this.username = '';
          this.contact = '';
          this.comment = '';
          this.isLoading = false;
          alertify.alert('Review', this.alerts.Review_Thanks_Message);
        } catch (e) {

        }



      }
    },
    viewReview: async function () {
      var self = this;
      let allReview = [];
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
      let requestObject = {
        from: 0,
        size: 100,
        type: "Reviews",
        nodeCode: agencyCode,
        orderBy: "desc"
      };
      let responseObject = await this.cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
        if (responseObject != undefined && responseObject.data != undefined) {
          allResult = JSON.parse(JSON.stringify(responseObject)).data;
          for (let i = 0; i < allResult.length; i++) {
            if (allResult[i].keyword4 == self.pageURLLink) {
              let object = {
                Name: allResult[i].keyword1,
                Date: moment(String(allResult[i].date1), "YYYY-MM-DDThh:mm:ss").format('MMM DD,YYYY'),
                comment: allResult[i].text1,
                Ratings: allResult[i].number1,
              };
              allReview.push(object);
            }
          }
          self.allReview = allReview;
        }

      });
    },
    userRating: function (num) {
      var ulList = document.getElementById("ratingID");
      let m = 0;
      for (let i = 0; i < ulList.childNodes.length; i++) {
        let childNode = ulList.childNodes[i];
        if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
          m = m + 1;
          for (let k = 0; k < childNode.childNodes.length; k++) {
            let style = childNode.childNodes[k].style.color;
            if ((m) < Number(num)) {
              childNode.childNodes[k].style = "color: rgb(239, 158, 8)";
            } else if ((m) == Number(num)) {
              if (style.trim() == "rgb(239, 158, 8)") {
                childNode.childNodes[k].style = "color: a9a9a9;";
              } else {
                childNode.childNodes[k].style = "color: rgb(239, 158, 8);";
              }
            } else {
              childNode.childNodes[k].style = "color: a9a9a9";
            }
          }
        }
      }
    },
    getUserRating: function () {
      var ulList = document.getElementById("ratingID");
      let m = 0;
      for (let i = 0; i < ulList.childNodes.length; i++) {
        let childNode = ulList.childNodes[i];
        if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
          let style = "";
          for (let k = 0; k < childNode.childNodes.length; k++) {
            style = childNode.childNodes[k].style.color;
            if (style != undefined && style != '') {
              break;
            }
          }
          if (style.trim() == "a9a9a9") {
            break;
          } else if (style.trim() == "rgb(239, 158, 8)") {
            m = m + 1;
          }
        }
      }
      return m;
    },

    async cmsRequestData(callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
        },
        body: data, // body data type must match "Content-Type" header
      });
      try {
        const myJson = await response.json();
        return myJson;
      } catch (error) {
        return object;
      }
    },
    getGlobalcoupencode: async function () {
      var self = this;
      var CoupenCode = self.CoupenCode;
      var packamt = self.grandtotal;
      if (CoupenCode == "") {
        alertify.alert(self.alerttypes.warning, self.alerts.Enter_Coupon_Message);
      } else {
        let agencyCode = JSON.parse(localStorage.User).loginNode.code;

        let dateObj = document.getElementById("from");
        let datevalue = "CURDATE()"; //moment(dateObj.value, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
        if (datevalue == undefined || datevalue == '') {
          datevalue = "CURDATE()";
        }
        var query = "select * from cms_forms_data  where  type='Coupon code-All Packages' AND nodeCode='#NodeCode#' AND amount2<=#priceAmount# AND  amount3>=#priceAmount# AND keyword3='True' AND keyword1='#Code#' AND date2 >= #TravelDate# AND date1<=#TravelDate# ";
        query = query.replace("#NodeCode#", agencyCode).replace("#priceAmount#", packamt).replace("#priceAmount#", packamt).replace("#Code#", CoupenCode).replace("#TravelDate#", datevalue).replace("#TravelDate#", datevalue);
        let postData = {
          from: 0,
          sortField: 'nodeCode',
          orderBy: 'asc',
          query: query
        };

        let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", postData, null);

        if (responseObject != undefined && responseObject.data != undefined && responseObject.data.length > 0 && (responseObject.data[0].amount1 != undefined || responseObject.data[0].amount4 != undefined)) {
          var Result = responseObject.data[0];
          self.CoupenInfo.Amount = Result.amount1;
          self.CoupenInfo.DisPercent = Result.amount4;
          self.CoupenInfo.ApplyCoupen = true;
          self.CoupenInfo.CoupenCode = CoupenCode;
          var cuurntPrice = self.grandtotal;
          if (self.CoupenInfo.DisPercent != undefined && self.CoupenInfo.DisPercent != '') {
            self.CoupenInfo.discountpercent = true;
            self.CoupenInfo.Disamount = cuurntPrice * (self.CoupenInfo.DisPercent / 100);
            var percentageamount = self.CoupenInfo.Disamount;
            self.CoupenInfo.newPrice = parseFloat(cuurntPrice) - (parseFloat(self.CoupenInfo.Amount) + percentageamount);
          } else {
            self.CoupenInfo.discountpercent = false;
            self.CoupenInfo.newPrice = parseFloat(cuurntPrice) - (parseFloat(self.CoupenInfo.Amount));
          }
        } else {
          alertify.alert(self.alerttypes.warning, self.alerts.Coupon_Error_Alert_Message);
          self.CoupenInfo.ApplyCoupen = false;
          self.CoupenInfo.Amount = "";
          self.CoupenInfo.discountpercent = false;
          self.CoupenInfo.CoupenCode = "";
          self.CoupenInfo.newPrice = "";
          self.CoupenInfo.Disamount = "";
        }




        /*var query = {
          _source: [
            "keyword1",
            "date1",
            "date2",
            "amount1",
            "keyword3",
            "amount2",
            "amount3",
            "amount4"
          ],
          query: {
            bool: {
              filter: [
                {
                  match_phrase: {
                    keyword1: {
                      query: CoupenCode
                    }
                  }
                },
                {
                  range: {
                    date1: {
                      lte: "now"
                    }
                  }
                },
                {
                  range: {
                    date2: {
                      gte: "now"
                    }
                  }
                },
                {
                  range: {
                    amount2: {
                      lte: packamt
                    }
                  }
                },
                {
                  range: {
                    amount3: {
                      gte: packamt
                    }
                  }
                },
                {
                  match_phrase: {
                    type: {
                      query: "Coupon code-All Packages"
                    }
                  }
                },
                {
                  match_phrase: {
                    nodeCode: {
                      query: agencyCode
                    }
                  }
                },
                {
                  match_phrase: {
                    keyword3: {
                      query: "True"
                    }
                  }
                }
              ]
            }
          }

        };
        var client = new elasticsearch.Client({
          host: [{
            host: ServiceUrls.elasticSearch.elasticsearchHost,
            auth: "a2z:agy435",
            protocol: ServiceUrls.elasticSearch.protocol,
            port: ServiceUrls.elasticSearch.port,
            requestTimeout: 60000
          }],
          log: 'trace'
        });
        client.search({ index: 'cms_forms_data', size: 5, pretty: true, filter_path: 'hits.hits._source', body: query }).then(function (resp) {
          if (isEmpty(resp)) {
            alertify.alert(self.alerttypes.warning, self.alerts.Coupon_Error_Alert_Message);
            self.CoupenInfo.ApplyCoupen = false;
            self.CoupenInfo.Amount = "";
            self.CoupenInfo.discountpercent = false;
            self.CoupenInfo.CoupenCode = "";
            self.CoupenInfo.newPrice = "";
            self.CoupenInfo.Disamount = "";
          }
          else {
            console.log("coupencodeGlobal", resp);
            var Result = resp.hits.hits;
            if (Result.length > 0) {
              Result = Result.find((element) => {
                return (element._source.amount1 != null || element._source.amount1 != null);
              });
              console.log(Result);
              self.CoupenInfo.Amount = Result._source.amount1;
              self.CoupenInfo.DisPercent = Result._source.amount4;
              self.CoupenInfo.ApplyCoupen = true;
              self.CoupenInfo.CoupenCode = CoupenCode;
              var cuurntPrice = self.grandtotal;
              // self.CoupenInfo.Disamount=cuurntPrice*(self.CoupenInfo.DisPercent/100);
              // var percentageamount=self.CoupenInfo.Disamount;
              self.CoupenInfo.newPrice = parseFloat(cuurntPrice) - (parseFloat(self.CoupenInfo.Amount));

              if (self.CoupenInfo.DisPercent != null || self.CoupenInfo.DisPercent != null) {
                self.CoupenInfo.discountpercent = true;
                var disResult = resp.hits.hits;
                if (disResult.length > 0) {
                  disResult = disResult.find((element) => {
                    return (element._source.amount1 != null || element._source.amount1 != null || element._source.amount4 != null || element._source.amount4 != null);
                  });

                  self.CoupenInfo.Amount = disResult._source.amount1;
                  self.CoupenInfo.DisPercent = disResult._source.amount4;
                  self.CoupenInfo.ApplyCoupen = true;
                  self.CoupenInfo.CoupenCode = CoupenCode;
                  var cuurntPrice = self.grandtotal;
                  self.CoupenInfo.Disamount = cuurntPrice * (self.CoupenInfo.DisPercent / 100);
                  var percentageamount = self.CoupenInfo.Disamount;
                  self.CoupenInfo.newPrice = parseFloat(cuurntPrice) - (parseFloat(self.CoupenInfo.Amount) + percentageamount);
                }
              }

            }
          }

        });*/

      }
    },
    clearCoupon: function () {
      var self = this;
      self.CoupenInfo.ApplyCoupen = false;
      self.CoupenInfo.discountpercent = false;
      self.CoupenInfo.Amount = "";
      self.CoupenInfo.DisPercent = "";
      self.CoupenInfo.CoupenCode = "";
      self.CoupenInfo.newPrice = "";
      self.CoupenCode = "";
    },

    Travellercounts: function () {
      var arrayLength = this.Traveller_Limit_Count;
      var count = [];
      for (var i = 1; i <= arrayLength; i++) {
        count.push(i);
      }
      this.Travellercount = count;
    },
    navigateSocialPage(url) {
      if (url.includes('?')) {
        var fullUrl = url + "=" + encodeURIComponent(window.location.href);
        window.open(fullUrl, "_blank");
      } else {
        window.open(url, "_blank");
      }
    },
    socialMediaShare: async function (name) {
      var TempDeviceInfo = detect.parse(navigator.userAgent);
      var deviceType = TempDeviceInfo.device.type;
      let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
       var user = localStorage.getItem("User");
       if (user != null && user != undefined) {
        var userData = JSON.parse(user);
        var userId = userData.loginId
          ? userData.loginId
          : "Guest";
      }
        let insertSharingData = {
          type: "Sharing on social media",
          date1: requestedDate,
          keyword1: name,
          keyword2: userId,
          text1: this.packagecontent.Title,
          keyword5: deviceType,
          nodeCode: agencyCode
        };
        let responseObject = await this.cmsRequestData("POST", "cms/data", insertSharingData, null);
        try {
          let insertID = Number(responseObject);
        } catch (e) {
      }
    },


  }
})

function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

function responsivetab() {
  //Horizontal Tab
  $('#parentHorizontalTab').easyResponsiveTabs({
    type: 'default', //Types: default, vertical, accordion
    width: 'auto', //auto or any width like 600px
    fit: true, // 100% fit in a container
    tabidentify: 'hor_1', // The tab groups identifier
    activate: function (event) { // Callback function if tab is switched
      var $tab = $(this);
      var $info = $('#nested-tabInfo');
      var $name = $('span', $info);
      $name.text($tab.text());
      $info.show();
    }
  });

  // Child Tab
  $('#ChildVerticalTab_1').easyResponsiveTabs({
    type: 'vertical',
    width: 'auto',
    fit: true,
    tabidentify: 'ver_1', // The tab groups identifier
    activetab_bg: '#fff', // background color for active tabs in this group
    inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
    active_border_color: '#c1c1c1', // border color for active tabs heads in this group
    active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
  });

  //Vertical Tab
  $('#parentVerticalTab').easyResponsiveTabs({
    type: 'vertical', //Types: default, vertical, accordion
    width: 'auto', //auto or any width like 600px
    fit: true, // 100% fit in a container
    closed: 'accordion', // Start closed if in accordion view
    tabidentify: 'hor_1', // The tab groups identifier
    activate: function (event) { // Callback function if tab is switched
      var $tab = $(this);
      var $info = $('#nested-tabInfo2');
      var $name = $('span', $info);
      $name.text($tab.text());
      $info.show();
    }
  });
}

function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
}