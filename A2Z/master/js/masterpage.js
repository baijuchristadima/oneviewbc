var HeaderComponent = Vue.component('headeritem', {
    template: `<div id="header">
    <nav id="navbar-main" class="navbar navbar-default navbar-fixed-top">
      <div class="container">
      <div class="row"   v-if="getdata"></div>
        <div class="row" v-else v-for="item in pluck('main',content.area_List)">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 flxBox ar_direction">
            <div class="navbar-header" >
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span
                  class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a class="navbar-brand" href="/"><img :src="pluckcom('Logo',item.component)" alt=""></a> </div>
            
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
              <ul class="nav navbar-nav"  >
                <li  @click="activate(1)" :class="{ active : active_el == 1 }"><a href="/">{{pluckcom('Flight_menu_label',item.component)}}</a></li>
                <li  @click="activate(2)" :class="{ active : active_el == 2 }"><a href="/">{{pluckcom('Hotel_menu_label',item.component)}}</a></li>
                <li  @click="activate(3)" :class="{ active : active_el == 3 }" style="display:none;"><a href="/">{{pluckcom('Flight_and_hotel_menu_label',item.component)}}</a></li>
                <li  @click="activate(4)" :class="{ active : active_el == 4 }"><a href="/A2Z/demo-page/packages.html">{{pluckcom('Holiday_menu_label',item.component)}}</a></li>
                <li><a id="modal_retrieve1" href="#modal" v-if="userlogined==false" class="btn-clta" :title="pluckcom('Retrieve_booking_label',item.component)">{{pluckcom('Retrieve_booking_label',item.component)}}</a></li>
              </ul>
            </div>
            <div class="call-to-action">
            <!--iflogin-->
            <label id="bind-login-info" for="profile2" class="profile-dropdown" v-show="userlogined">
                  <input type="checkbox" id="profile2">
                  <img v-if="[2,3,4].indexOf(userinfo.title?userinfo.title.id:1)<0" src="/assets/images/user.png">
                  <img v-else src="/assets/images/user_lady.png">
                  <span>{{userinfo.firstName+' '+userinfo.lastName }}</span>
                  <label for="profile2"><i class="fa fa-angle-down" aria-hidden="true"></i></label>
                  <ul v-for="prfitem in pluck('Dropdown_menu',content.area_List)" >
                    <li><a href="/customer-profile.html"><i class="fa fa-th-large" aria-hidden="true"></i> {{pluckcom('Dashboard_label',prfitem.component)}}</a></li>
                    <li><a href="/my-profile.html"><i class="fa fa-user" aria-hidden="true"></i>{{pluckcom('Profile_label',prfitem.component)}}</a></li>
                    <li><a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i> {{pluckcom('Bookings_label',prfitem.component)}}</a></li>
                    <li><a href="#" v-on:click.prevent="logout"><i class="fa fa-power-off" aria-hidden="true"></i> {{pluckcom('Logout_label',prfitem.component)}}</a></li>
                  </ul>
                </label>
                <!--ifnotlogin-->
              <div id="sign-bt-area" class="cd-main-nav js-main-nav" v-show="userlogined==false" >
                <ul class="cd-main-nav__list js-signin-modal-trigger">
                  <!-- inser more links here -->
                  <li>
                    <a data-toggle="modal" data-target="#onboarding" class="cd-main-nav__item cd-main-nav__item--signin" href="#">{{pluckcom('Login_button_label',item.component)}}</a>
                  </li>
                </ul>
              </div>
              <a id="modal_retrieve" href="#modal" v-if="userlogined==false" class="btn-clta" :title="pluckcom('Retrieve_booking_label',item.component)">{{pluckcom('Retrieve_booking_label',item.component)}}</a>
              <div id="modal" class="popupContainer" style="display:none;" v-for="retrvitem in pluck('Retrieve_the_Bookings',content.area_List)" >
                <header class="popupHeader"> <span class="header_title">{{pluckcom('Retrieve_booking_label',item.component)}}</span> <span
                    class="modal_close"><i class="fa fa-times"></i></span> </header>
                <section class="popupBody">
                  <!-- User Login -->
                  <div class="user_login">
                    <div>
                    <p>
                   
                      <label>{{pluckcom('Email_label',retrvitem.component)}}</label>
                      <div class="validation_sec">
                      <input v-model="retrieveEmailId" type="text" id="txtretrivebooking" v-bind:class="{ 'cd-signin-modal__input--has-error': retrieveEmailErormsg }" name="text" :placeholder="pluckcom('Email_placeholder',retrvitem.component)" />
                      <span v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveEmailErormsg }"  class="cd-signin-modal__error sp_validation">Email Required!</span></div>
                      <small>{{pluckcom('Email_text',retrvitem.component)}}</small>
                       </p>
                      <p>
                      
                      <label>{{pluckcom('ID_number_Label',retrvitem.component)}}</label>
                      <div class="validation_sec">
                      <input v-model="retrieveBookRefid" v-bind:class="{ 'cd-signin-modal__input--has-error': retrieveBkngRefErormsg }" type="text" id="text" name="text" :placeholder="pluckcom('ID_number_placeholder',retrvitem.component)" />
                      <span v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveBkngRefErormsg }"  class="cd-signin-modal__error sp_validation">Booking Reference Id Required!</span> </div>
                      <small>{{pluckcom('Id_number_text',retrvitem.component)}}</small>
                        </p>
                      <button type="submit" v-on:click="retrieveBooking" id="retrieve-booking" class="btn-blue">{{pluckcom('Submit_button_name',retrvitem.component)}}</button>
                    </div>
                  </div>
                </section>
              </div>
              
              <div class="currency lag">
              <currency-select></currency-select>              
                
              </div>
              <div class="lang lag">
                    <lang-select-a2z @languagechange="getpagecontent"></lang-select-a2z>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
                    <div class="modal fade" id="onboarding" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content" v-for="signitem in pluck('Sign_in_and_Register',content.area_List)">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div class="model-text" v-html="pluckcom('Onboarding_message',signitem.component).replace(/<li>/g, '&lt;li&gt;&lt;i class=&quot;fa fa-check-square-o&quot;&gt;&lt;/i&gt;')">
                                    
                                </div>
                
                                <div class="model-form">
                                    <h3 v-if="formType=='register'">{{pluckcom('Register_button_name',signitem.component)}}</h3>
                                    <h3 v-if="formType=='signin'">{{pluckcom('Sign_in_button_name',signitem.component)}}</h3>
                                    <h3 v-if="formType=='forgot'">{{pluckcom('Reset_password_button_name',signitem.component)}}</h3>
                                    <hr>
                                    <ul>
                                        <li class="fb-model"><i class="fa fa-facebook"></i><a href="#">Facebook</a></li>
                                        <li class="google-model"><i class="fa fa-google-plus"></i><a href="#">Google</a></li>
                                    </ul>
                
                                    <form v-if="formType=='register'">
                                        <select v-model="registerUserData.title">
                                            <option>Title</option>
                                            <option>Mr</option>
                                            <option>Ms</option>
                                            <option>Mrs</option>
                                        </select>
                                        <input type="text" id="text" name="text" class="form-control name-se" :placeholder="pluckcom('First_name_placeholder',signitem.component)" v-model="registerUserData.firstName">
                                        <input type="text" id="text" name="text" class="form-control" :placeholder="pluckcom('Last_name_placeholder',signitem.component)" v-model="registerUserData.lastName">
                                        <input type="email" id="email" name="email" class="form-control" :placeholder="pluckcom('Email_placeholder',signitem.component)" v-model="registerUserData.emailId">
                                        <input type="checkbox" id="checkbox"><span>{{pluckcom('Register_message',signitem.component)}}</span>
                                        <p v-if="userFirstNameErormsg" style="color: red;margin-bottom:15px;">{{pluckcom('Firstname_error_message',signitem.component)}}</p>
                                        <p v-if="userLastNameErrormsg" style="color: red;margin-bottom:15px;">{{pluckcom('Lastname_error_message',signitem.component)}}</p>
                                        <p v-if="userEmailErormsg" style="color: red;margin-bottom:15px;">{{pluckcom('Email_error_message',signitem.component)}}</p>
                                        <input type="submit" id="submit" name="submit" :value="pluckcom('Register_button_name',signitem.component)" @click.prevent="registerUser">
                                    </form>
                                    <form v-if="formType=='signin'">
                                        <input type="email" id="email" name="email" class="form-control" :placeholder="pluckcom('Email_placeholder',signitem.component)" v-model="username">
                                        <input type="password" id="password" :placeholder="pluckcom('password_placeholder',signitem.component)" class="form-control" v-model="password">
                                        <p v-if="usererrormsg" style="color: red;margin-bottom:15px;">{{pluckcom('Username_error_message',signitem.component)}}</p>
                                        <p v-if="psserrormsg" style="color: red;margin-bottom:15px;">{{pluckcom('Password_error_message',signitem.component)}}</p>
                                        <input type="submit" id="submit" name="submit" :value="pluckcom('Sign_in_button_name',signitem.component)" @click.prevent="loginaction">
                                    </form>
                                    <form v-if="formType=='forgot'">
                                        <input type="email" id="email" name="email" class="form-control" :placeholder="pluckcom('Email_placeholder',signitem.component)" v-model="emailId">
                                        <p v-if="userforgotErrormsg" style="color: red;margin-bottom:15px;">{{pluckcom('Email_error_message',signitem.component)}}</p>
                                        <input type="submit" id="submit" name="submit" :value="pluckcom('Reset_password_button_name',signitem.component)" @click.prevent="forgotPassword">
                                    </form>
                
                                    <a v-if="formType=='signin'||formType=='register'" class="forgot-pass" href="#" @click.prevent="formType='forgot'">{{pluckcom('Forgot_password_link_name',signitem.component)}}</a>
                                    <a v-if="formType=='forgot'" class="forgot-pass" href="#" @click.prevent="formType='signin'">{{pluckcom('Back_to_login_name',signitem.component)}}</a>
                                    <p v-if="formType=='register'" class="cr-ac">{{pluckcom('Signin_link_question',signitem.component)}} <a href="#" @click.prevent="formType='signin'">{{pluckcom('Sign_in_button_name',signitem.component)}}</a></p>
                                    <p v-if="formType=='signin'" class="cr-ac">{{pluckcom('Register_link_question',signitem.component)}} <a href="#" @click.prevent="formType='register'">{{pluckcom('Register_button_name',signitem.component)}}</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
  </div>`,
    data() {
        return {
            username: '',
            password: '',
            emailId: '',
            retrieveEmailId: '',
            retrieveBookRefid: '',
            usererrormsg: false,
            psserrormsg: false,
            userFirstNameErormsg: false,
            userLastNameErrormsg: false,
            userEmailErormsg: false,
            userPasswordErormsg: false,
            userVerPasswordErormsg: false,
            userPwdMisMatcherrormsg: false,
            userPwdMisMatcherrormsg: false,
            userTerms: false,
            userforgotErrormsg: false,
            retrieveBkngRefErormsg: false,
            retrieveEmailErormsg: false,
            userlogined: this.checklogin(),
            userinfo: [],
            registerUserData: {
                firstName: '',
                lastName: '',
                emailId: '',
                password: '',
                verifyPassword: '',
                terms: '',
                title: 'Mr'
            },
            Languages: [],
            language: 'en',
            content: {area_List: []},
            getdata: true,
            active_el: (sessionStorage.active_el) ? sessionStorage.active_el : 1,
            isLoading: false,
            fullPage: true,
            PageName: '',
            DeviceInfo: '',
            sessionids: '',
            Ipaddress: '',
            pageType: '',
            formType: 'signin'
        }
    },
    methods: {
        loginaction: function () {

            if (!this.username) {
                this.usererrormsg = true;
                return false;
            } else if (!this.validEmail(this.username)) {
                this.usererrormsg = true;
                return false;
            } else {
                this.usererrormsg = false;
            }
            if (!this.password) {
                this.psserrormsg = true;
                return false;

            } else {
                this.psserrormsg = false;
                var self = this;
                login(this.username, this.password, function (response) {
                    if (response == false) {
                        self.userlogined = false;
                        alert("Invalid username or password.");
                    } else {
                        self.userlogined = true;
                        self.userinfo = JSON.parse(localStorage.User);
                        $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                        $('#onboarding').modal('hide');
                        try {
                            self.$eventHub.$emit('logged-in', {
                                userName: self.username,
                                password: self.password
                            });
                            signArea.headerLogin({
                                userName: self.username,
                                password: self.password
                            })
                        } catch (error) {

                        }
                    }
                });

            }



        },
        validEmail: function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        checklogin: function () {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);
                return true;
            } else {
                this.userlogined = false;
                return false;
            }
        },
        logout: function () {
            this.userlogined = false;
            this.userinfo = [];
            localStorage.profileUpdated = false;
            try {
                this.$eventHub.$emit('logged-out');
                signArea.logout()
            } catch (error) {

            }
            commonlogin();
        },
        registerUser: function () {
            var vm = this;
            if (this.registerUserData.firstName == "") {
                this.userFirstNameErormsg = true;
                return false;
            } else {
                this.userFirstNameErormsg = false;
            }
            if (this.registerUserData.lastName == "") {
                this.userLastNameErrormsg = true;
                return false;
            } else {
                this.userLastNameErrormsg = false;
            }
            if (this.registerUserData.emailId == "") {
                this.userEmailErormsg = true;
                return false;
            } else if (!this.validEmail(this.registerUserData.emailId)) {
                this.userEmailErormsg = true;
                return false;
            } else {
                this.userEmailErormsg = false;
            }
            registerUser(this.registerUserData.emailId, this.registerUserData.firstName, this.registerUserData.lastName, this.registerUserData.title, function (response) {
                if (response.isSuccess == true) {
                    vm.username = response.data.data.user.loginId;
                    vm.password = response.data.data.user.password;
                    login(vm.username, vm.password, function (response) {
                        if (response != false) {
                            $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                            $('#onboarding').modal('hide');
                            window.location.href = "/edit-my-profile.html?edit-profile=true";
                        }
                    });
                }

            });
        },

        forgotPassword: function () {
            var self = this;
            getAgencycode(function (response) {
                var agencyCodeResponse = response;

                if (self.emailId == "") {
                    self.userforgotErrormsg = true;
                    return false;
                } else {
                    self.userforgotErrormsg = false;
                }

                var datas = {
                    emailId: self.emailId,
                    agencyCode: agencyCodeResponse,
                    logoUrl: window.location.origin + "/" + localStorage.AgencyFolderName + "/website-informations/logo/logo.png",
                    websiteUrl: window.location.origin,
                    resetUri: window.location.origin + "/" + localStorage.AgencyFolderName + "/reset-password.html"

                };

                $(".model-form input[type=submit]").css("cssText", "pointer-events:none;background:grey !important;");

                var huburl = ServiceUrls.hubConnection.baseUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var requrl = ServiceUrls.hubConnection.hubServices.forgotPasswordUrl;
                axios.post(huburl + portno + requrl, datas)
                    .then((response) => {
                        if (response.data != "") {
                            alert(response.data);
                        } else {
                            alert("Error in forgot password. Please contact admin.");
                        }
                        $(".model-form input[type=submit]").css("cssText", "pointer-events:auto;background:#3a7fc1 !important");

                    })
                    .catch((err) => {
                        console.log("FORGOT PASSWORD  ERROR: ", err);
                        if (err.response.data.message == 'No User is registered with this emailId.') {
                            alert(err.response.data.message);
                        } else {
                            alert('We have found some technical difficulties. Please contact admin!');
                        }
                        $(".model-form input[type=submit]").css("cssText", "pointer-events:auto;background:#3a7fc1 !important");

                    });
            });
        },

        retrieveBooking: function () {
            if (this.retrieveEmailId == "") {
                //alert('Email required !');
                this.retrieveEmailErormsg = true;
                return false;
            } else if (!this.validEmail(this.retrieveEmailId)) {
                //alert('Invalid Email !');
                this.retrieveEmailErormsg = true;
                return false;
            } else {
                this.retrieveEmailErormsg = false;
            }
            if (this.retrieveBookRefid == "") {
                this.retrieveBkngRefErormsg = true;
                return false;
            } else {
                this.retrieveBkngRefErormsg = false;
            }
            var bookData = {
                BkngRefID: this.retrieveBookRefid,
                emailId: this.retrieveEmailId,
                redirectFrom: 'retrievebooking',
                isMailsend: false
            };
            localStorage.bookData = JSON.stringify(bookData);
            window.location.href = '/Flights/flight-confirmation.html';
        },

        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        activate: function (el) {
            sessionStorage.active_el = el;
            this.active_el = el;
        },
        getpagecontent: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Master page/Master page/Master page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = false;
                    window.localStorage.setItem("cmsContent", JSON.stringify(response.data));

                    Vue.nextTick(function () {
                        (

                            function () {
                                self.active_el = (sessionStorage.active_el) ? sessionStorage.active_el : 1;
                                //Login/Signup modal window - by CodyHouse.co
                                function ModalSignin(element) {
                                    this.element = element;
                                    this.blocks = this.element.getElementsByClassName('js-signin-modal-block');
                                    this.switchers = this.element.getElementsByClassName('js-signin-modal-switcher')[0].getElementsByTagName('a');
                                    this.triggers = document.getElementsByClassName('js-signin-modal-trigger');
                                    this.hidePassword = this.element.getElementsByClassName('js-hide-password');
                                    this.init();
                                };

                                ModalSignin.prototype.init = function () {
                                    var self1 = this;
                                    //open modal/switch form
                                    for (var i = 0; i < this.triggers.length; i++) {
                                        (function (i) {
                                            self1.triggers[i].addEventListener('click', function (event) {
                                                if (event.target.hasAttribute('data-signin')) {
                                                    event.preventDefault();
                                                    self1.showSigninForm(event.target.getAttribute('data-signin'));
                                                }
                                            });
                                        })(i);
                                    }

                                    //close modal
                                    this.element.addEventListener('click', function (event) {
                                        if (hasClass(event.target, 'js-signin-modal') || hasClass(event.target, 'js-close')) {
                                            event.preventDefault();
                                            removeClass(self1.element, 'cd-signin-modal--is-visible');
                                        }
                                    });
                                    //close modal when clicking the esc keyboard button
                                    document.addEventListener('keydown', function (event) {
                                        (event.which == '27') && removeClass(self1.element, 'cd-signin-modal--is-visible');
                                    });

                                    //hide/show password
                                    for (var i = 0; i < this.hidePassword.length; i++) {
                                        (function (i) {
                                            self1.hidePassword[i].addEventListener('click', function (event) {
                                                self1.togglePassword(self1.hidePassword[i]);
                                            });
                                        })(i);
                                    }

                                    //IMPORTANT - REMOVE THIS - it's just to show/hide error messages in the demo

                                };

                                ModalSignin.prototype.togglePassword = function (target) {
                                    var password = target.previousElementSibling;
                                    ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text'): password.setAttribute('type', 'password');
                                    target.textContent = ('Hide' == target.textContent) ? 'Show' : 'Hide';
                                    putCursorAtEnd(password);
                                }

                                ModalSignin.prototype.showSigninForm = function (type) {
                                    // show modal if not visible
                                    !hasClass(this.element, 'cd-signin-modal--is-visible') && addClass(this.element, 'cd-signin-modal--is-visible');
                                    // show selected form
                                    for (var i = 0; i < this.blocks.length; i++) {
                                        this.blocks[i].getAttribute('data-type') == type ? addClass(this.blocks[i], 'cd-signin-modal__block--is-selected') : removeClass(this.blocks[i], 'cd-signin-modal__block--is-selected');
                                    }
                                    //update switcher appearance
                                    var switcherType = (type == 'signup') ? 'signup' : 'login';
                                    for (var i = 0; i < this.switchers.length; i++) {
                                        this.switchers[i].getAttribute('data-type') == switcherType ? addClass(this.switchers[i], 'cd-selected') : removeClass(this.switchers[i], 'cd-selected');
                                    }
                                };

                                ModalSignin.prototype.toggleError = function (input, bool) {
                                    // used to show error messages in the form
                                    toggleClass(input, 'cd-signin-modal__input--has-error', bool);
                                    toggleClass(input.nextElementSibling, 'cd-signin-modal__error--is-visible', bool);
                                }

                                var signinModal = document.getElementsByClassName("js-signin-modal")[0];
                                if (signinModal) {
                                    new ModalSignin(signinModal);
                                }

                                // toggle main navigation on mobile
                                var mainNav = document.getElementsByClassName('js-main-nav')[0];
                                if (mainNav) {
                                    mainNav.addEventListener('click', function (event) {
                                        if (hasClass(event.target, 'js-main-nav')) {
                                            var navList = mainNav.getElementsByTagName('ul')[0];
                                            toggleClass(navList, 'cd-main-nav__list--is-visible', !hasClass(navList, 'cd-main-nav__list--is-visible'));
                                        }
                                    });
                                }

                                //class manipulations - needed if classList is not supported
                                function hasClass(el, className) {
                                    if (el.classList) return el.classList.contains(className);
                                    else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
                                }

                                function addClass(el, className) {
                                    var classList = className.split(' ');
                                    if (el.classList) el.classList.add(classList[0]);
                                    else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
                                    if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
                                }

                                function removeClass(el, className) {
                                    var classList = className.split(' ');
                                    if (el.classList) el.classList.remove(classList[0]);
                                    else if (hasClass(el, classList[0])) {
                                        var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
                                        el.className = el.className.replace(reg, ' ');
                                    }
                                    if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
                                }

                                function toggleClass(el, className, bool) {
                                    if (bool) addClass(el, className);
                                    else removeClass(el, className);
                                }
                                $("#modal_retrieve").leanModal({
                                    top: 100,
                                    overlay: 0.6,
                                    closeButton: ".modal_close"
                                });
                                $("#modal_retrieve1").leanModal({
                                    top: 100,
                                    overlay: 0.6,
                                    closeButton: ".modal_close"
                                });
                                //credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
                                function putCursorAtEnd(el) {
                                    if (el.setSelectionRange) {
                                        var len = el.value.length * 2;
                                        el.focus();
                                        el.setSelectionRange(len, len);
                                    } else {
                                        el.value = el.value;
                                    }
                                };
                            })();
                    }.bind(self));
                    self.isLoading = false;

                }).catch(function (error) {
                    console.log('Error');
                    this.content = [];
                });
            });
        },
        Getvisit: function () {
            var self = this;
            var urlString = window.location.href;
            var url = new URL(urlString);
            var socialMediaUrl = url.searchParams.get("source");
            if (socialMediaUrl) {
                sessionStorage.setItem("a2zSocialMedia", atob(socialMediaUrl));
            }
            var currentUrl = window.location.pathname;
            if (currentUrl == "/A2Z/") {
                self.pageType = "Master";
                self.PageName = "Home"
            } else if (currentUrl == "/A2Z/demo-page/holidaydetails.html") {
                self.pageType = "Package details";
                self.PageName = packageView.packagecontent.Title;
            } else {
                page = currentUrl.substring(0, currentUrl.length - 5);
                var page = page.split('/');
                self.PageName = page[3];
                self.pageType = "Master";
            }
                var TempDeviceInfo = detect.parse(navigator.userAgent);
                self.DeviceInfo = TempDeviceInfo;
                self.showYourIp();
        },
        showYourIp: function () {
            var self = this;
            var ipUrl = ServiceUrls.externalServiceUrl.ipAddressApi;
            fetch(ipUrl)
              .then(x => x.json())
              .then(({ ip }) => {
                let Ipaddress = ip;
                let sessionids = ip.replace(/\./g, '');
                self.sendip(Ipaddress,sessionids);
              });
          },
        // sessionid: function () {
        //     var GUID = function () {
        //         var S4 = function () {
        //             return (
        //                 Math.floor(
        //                     Math.random() * 0x10000 /* 65536 */
        //                 ).toString(16)
        //             );
        //         };
        //         return (
        //             S4() + S4() + "-" +
        //             S4() + "-" +
        //             S4() + "-" +
        //             S4() + "-" +
        //             S4() + S4() + S4()
        //         );
        //     };
        //     if (!window.name.match(/^G/)) {
        //         window.name = "G" + GUID();
        //     }
        //     //window.name = window.name.replace(/^GUID-/, "");
        //     this.sessionids = window.name;
        // },
        sendip: async function (Ipaddress,sessionids) {
                var self = this;
                if (self.PageName == null || self.PageName == undefined || self.PageName == "") {
                    var pathArray = window.location.pathname.split('/');
                    var activePage = pathArray[pathArray.length - 1];

                    if (activePage == 'packages.html') {
                        self.PageName = packageView.packagecontent.Title
                    }
                }
                var socialMedia = JSON.parse(sessionStorage.getItem("a2zSocialMedia"));
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertVisitData = {
                    type: "Visit",
                    keyword1: sessionids,
                    ip1: Ipaddress,
                    keyword2: self.pageType,
                    keyword3: self.PageName,
                    keyword4: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'AED',
                    keyword5: (localStorage.Languagecode) ? localStorage.Languagecode : "en",
                    keyword7: socialMedia ? socialMedia.mktChannel : null || null,
                    text4: socialMedia ? socialMedia.mktCampaign : null || null,
                    date2: socialMedia ? socialMedia.mktDate : null || null,
                    text1: self.DeviceInfo.device.type,
                    text2: self.DeviceInfo.os.name,
                    text3: self.DeviceInfo.browser.name,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertVisitData, null);
                let insertID = Number(responseObject);
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json'
                },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
    },
    mounted: function () {


        if (localStorage.IsLogin == "true") {
            this.userlogined = true;
            this.userinfo = JSON.parse(localStorage.User);

        } else {
            this.userlogined = false;

        }
        this.getpagecontent();
        setTimeout(() =>{
            this.Getvisit();
         },1000)

    },
    watch: {
        updatelogin: function () {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);

            } else {
                this.userlogined = false;

            }

        }

    }

});

var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});

var FooterComponent = Vue.component('footeritem', {
    template: `<div class="container">
  <div class="row"   v-if="getdata"></div>
  <div class="row" v-else v-for="item in pluck('Footer',content.area_List)">

  <div class="fixed-bottom whatsappIconPosition" v-if="pluckcom('Is_Whatsapp_Enabled',item.component)">
    <a :href="pluckcom('Whatsapp_Link',item.component) + pluckcom('Whatsapp_Number_With_Country_Code',item.component)" target="_blank" class="whatsapp">
        <img :src="pluckcom('Whatsapp_Icon_70x70px',item.component)" class="whatsappIconSize"></a>
    </div>

    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
      <div class="link">
        <ul>
          <li v-for="Links in pluckcom('Links',item.component)"><a href="#" v-on:click="getmoreinfo(Links.URL)">{{Links.Page_Name}}</a></li>
          
        </ul>
      </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 card-padding"> <img v-for="img in pluckcom('Payment_types',item.component)" :src="img.Image" alt=""> </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 link">
      <ul>
        <li v-for="media in pluckcom('Social_Media_List',item.component)" style="cursor:pointer;">
        <a :href="media.Url" target="_blank"><i class="fa" v-bind:class="media.Icon" style="font-size: 1.7em;"></i></a>
        </li>
      </ul>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
      <div class="c-text"> {{pluckcom('Copy_right_Info',item.component)}} </div>
    </div>
  </div>
</div>`,
    data() {
        return {
            content: null,
            getdata: true,
            isLoading: false,
            fullPage: true,
        }

    },
    mounted: function () {
        var self = this;
        self.isLoading = true;
        getAgencycode(function (response) {
            var Agencycode = response;
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";

            var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Master page/Master page/Master page.ftl';
            var cmsurl = huburl + portno + homecms;
            axios.get(cmsurl, {
                headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                }
            }).then(function (response) {
                self.content = response.data;
                self.getdata = false;
                self.isLoading = false;
            }).catch(function (error) {
                console.log('Error');
                self.content = [];
            });
        });


    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = "/A2Z/demo-page/" + url;

                    window.location.href = url;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            console.log(this.url);
            return url;

        },

    }

});

var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: '',
        }
    },

});

function searchArray(nameKey, myArray, tagName) {
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i][tagName] === nameKey) {
            return myArray[i];
        }
    }
}

var languagecomp = Vue.component('lang-select-a2z', {
    template: `
        <div v-if="Languages.length>2">
            <ul>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{language}}
                        <svg focusable="false" color="inherit" fill="currentcolor" aria-hidden="true" role="presentation" viewBox="0 0 150 150" preserveAspectRatio="xMidYMid meet" size="12" width="12" height="12" class="sc-chPdSV cKssQA"><path d="M86.8 118.6l60.1-61.5c5.5-5.7 5.5-14.8 0-20.5l-5-5.1c-5.4-5.5-14.3-5.6-19.8-.2l-.2.2L76 78.4 30.1 31.5c-5.4-5.5-14.3-5.6-19.8-.2l-.2.2-5 5.1c-5.5 5.7-5.5 14.8 0 20.5l60.1 61.5c2.8 2.9 6.8 4.4 10.8 4.1 4 .3 8-1.3 10.8-4.1z"></path></svg>
                    </a>
                    <div class="nav-link dropdown-toggle dropdown-menu" aria-labelledby="dropdown09">
                        <a href="#" class="dropdown-item" v-for="item in Languages" @click.prevent="onSelected(item.code, item.lang)">{{item.lang}}</a>     
                    </div>
                </li>
            </ul>
        </div>
        <div v-else  class="nav-item dropdown">
            <a @click.prevent="onSelected(oppositeLang.code, oppositeLang.lang)" class="nav-link dropdown-toggle" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{oppositeLang.lang}}
            </a>
        </div>
    `,
    data() {
        return {
            Languages: [],
            language: (localStorage.Language) ? localStorage.Language : 'English',
            languagecode: (localStorage.Languagecode) ? localStorage.Languagecode : 'en',
            oppositeLang: {
                code: "ar",
                lang: "العربية"
            }
        }
    },
    methods: {
        onSelected: function (code, lang) {
            if (code == 'en') {
                oppositeLang = this.Languages.filter(function (e) {
                    return e.code == 'ar'
                })[0];
            } else {
                oppositeLang = this.Languages.filter(function (e) {
                    return e.code == 'en'
                })[0];
            }
            localStorage.Languagecode = code;
            localStorage.Language = lang;
            this.languagecode = code;
            this.language = lang;
            localStorage.Languages = this.Languages;
            this.$emit('languagechange');

            //language change direction  

            var hName = window.location.pathname.toString().split("/")[1].toLowerCase();
            var pagePath = (window.location.href.indexOf(hName + "/demo-page") > -1) ? hName + "/demo-page" : window.location.pathname.toLowerCase();

            //  if (pagePath != '/Flights/flight-listing.html' && pagePath != '/Hotels/hotel-listing.html') {


            const html = document.documentElement
            if (this.languagecode == 'ar') {

                html.setAttribute('dir', 'rtl');
                html.setAttribute('lang', 'ar');
                localStorage.direction = "rtl";

                if (pagePath == '/Nirvana/') {
                    maininstance.arabic_dropdown = "dwn_icon";
                    // hotelSearch.arabic_dropdown = "dwn_icon";

                }


                //hotelRooms.arabic_dropdown= "dwn_icon";

                $(".en_dwn").addClass("dwn_icon");
                // $("#rangeSlider").addClass("direction_rtl");
                $(".arborder").addClass("arbrder_left");
                $(".ar_direction").addClass("ar_direction1");

                $(".footer_address_sec").addClass("ar_direction12");




            } else {
                html.setAttribute('dir', 'ltr');
                html.setAttribute('lang', 'en');
                localStorage.direction = "ltr";
                if (pagePath == '/Nirvana/') {
                    maininstance.arabic_dropdown = "";
                    //hotelSearch.arabic_dropdown = "";

                }
                // hotelRooms.arabic_dropdown= "";
                $(".en_dwn").removeClass("dwn_icon");
                // $("#rangeSlider").removeClass("direction_rtl");
                $(".arborder").removeClass("arbrder_left");
                $(".ar_direction").removeClass("ar_direction1");

                $(".footer_address_sec").removeClass("ar_direction12");
            }
            footerinstance.key = Math.random();
            switch (pagePath) {
                case "/A2Z_India/":
                    cmsapp.getcmsdata();
                    break;
                case "/SGS_Demo/":
                    cmsapp.getcmsdata();
                    break;    
                case "/Nirvana/":
                    maininstance.getPagecontent();
                    break;
                case "/Nirvana/index.html":
                    maininstance.getPagecontent();
                    break;
                case "/Flights/flight-listing.html":
                    window.location.reload();
                    break;
                case "/Flights/flight-booking.html":
                case "/Flights/flight-confirmation.html":
                case "/Hotels/hotel-listing.html":
                    window.location.reload();
                    break;
                case "/Hotels/hotel-detail.html":
                    el_hotelDetail.$children[0].getPagecontent();
                    break;
                case "/" + hName + "/demo-page/index.html":
                    holidy.getpagecontent();
                    break;
                case "/Nirvana/book-now.html":
                    Packageinfo.getPagecontent();
                    break;
                case "/" + hName + "/demo-page/packages.html":
                    holidypack.getpagecontent();
                    break;
                case "/" + hName + "/demo-page/holidaydetails.html":
                    packageView.getPageTitle();
                    break;
                case "/Nirvana/packageInfo.html":
                    packageDetailInfo.getPagecontent();
                    break;
                case "/Nirvana/package.html":
                    packagelist.getPagecontent();
                    break;
                case "/Nirvana/events.html":
                    packagelist.getPagecontent();
                    break;
                case "/Nirvana/offers.html":
                    packagelist.getPagecontent();
                    break;
                case "/Nirvana/uae-attractions.html":
                    packagelist.getPagecontent();
                    break;
                case "/Nirvana/aboutus.html":
                    packagelist.getPagecontent();
                    break;
                case "/my-profile.html":
                    myprofile_vue.getPagecontent();
                    myprofile_vue.key = Math.random();
                    break;
                case "/easy2go/package.html":
                    packagelist.getPagecontent();
                    break;
                case "/easy2go/offers.html":
                    packagelist.getPagecontent();
                    break;
                case "/easy2go/":
                    maininstance.getPagecontent();
                    break;
                case "/Biscordint/":
                    maininstance.pageContent();
                    break;
                case "/Biscordint/flight-deals.html":
                    packagelist.getPageTitle();
                    break;
                case "/Biscordint/flight-deals-details.html":
                    packageView.getPageTitle();
                    break;
                case "/Biscordint/contact-us.html":
                    contact.getPagecontent();
                    break;
                case "/Biscordint/holiday-packagesview.html":
                    Packageinfo.getPagecontent();
                    break;
                case "/Biscordint/apply-visa.html":
                    applyvisa.getPagecontent();
                    break;
                case "/Biscordint/apply-visa-form.html":
                    visa.getPagecontent();
                    break;
                case "/CompareTicket/":
                    maininstance.pageContent();
                    break;
                case "/CompareTicket/index.html":
                    maininstance.pageContent();
                    break;

                default:
                    window.location.reload();
                    break;
            }

        }
    },
    mounted: function () {
        var hName = window.location.pathname.toString().split("/")[1].toLowerCase();
        var pagePath = (window.location.href.indexOf(hName + "/demo-page") > -1) ? hName + "/demo-page" : window.location.pathname.toLowerCase();
        //  if (pagePath != '/Flights/flight-listing.html' && pagePath != '/Hotels/hotel-listing.html') {
        if (localStorage.direction) {
            const html = document.documentElement;
            html.setAttribute('dir', localStorage.direction);
            html.setAttribute('lang', localStorage.Languagecode);
        }

        if (localStorage.direction == 'rtl') {
            $(".arborder").addClass("arbrder_left");
            $(".ar_direction").addClass("ar_direction1");
        } else {
            $(".arborder").removeClass("arbrder_left");
            $(".ar_direction").removeClass("ar_direction1");
        }

        // }

        var self = this;
        getAgencycode(function (response) {
            let axiosConfig = {
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8'
                }
            };
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var AgencyCode = response;
            var requrl = '/templates?path=/B2B/AdminPanel/CMS/' + AgencyCode + '/Template/Home/Home'
            //forgot password

            axios.get(huburl + portno + requrl, axiosConfig)
                .then((response) => {
                    var languages = response.data;
                    if (languages) {
                        languages = languages.filter(function (item) {
                            return item.indexOf("B2B/AdminPanel") !== 0;
                        });

                        languages.map(function (value, key) {
                            var langauge = value.split('.')[0].split('_')[1];
                            $.getJSON('/Resources/HubUrls/languages.json', function (json) {
                                let lang = json.Languages.find(o => o.code.toLowerCase() === langauge.toLowerCase());
                                if (typeof lang !== "undefined") {
                                    self.Languages.push({
                                        'code': lang.code,
                                        'lang': lang.nativeName
                                    });
                                    if (localStorage.Languagecode && localStorage.Languagecode == 'en') {
                                        self.oppositeLang = {
                                            code: "ar",
                                            lang: "العربية"
                                        }
                                    } else {
                                        self.oppositeLang = {
                                            code: "en",
                                            lang: "English"
                                        }
                                    }
                                }
                            })
                        })

                    }
                    console.log(self.Languages);
                })
                .catch((err) => {});
        });
    }
});