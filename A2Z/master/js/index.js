// var header = new Vue({
//     //i18n,
//     el: "#header",    
//     data: {
//         selectedLanguage: 'en',
//         content: {
//             "response": {
//               "general": {
//                 "bannerImage": "../assets/images/banner.jpg",
//                 "logo": "../assets/images/logo.png",
//                 "address": "",
//                 "agencyName": "a2zBooking",
//                 "emailID": "",
//                 "contactNo": "",
//                 "copyRight": ""
//               }
//             }
//         }
//         // selectedCurrency: Vue_localization.defaultCurrency,
//         // languageOptions: Vue_localization.languageOptions,
//         // CurrencyOptions: Vue_localization.CurrencyOptions
//     },
//     methods: {
//         onChange(value) {
//             // this.selectedCurrency=value;
//             // test.selectedCurrency=this.selectedCurrency;
//         }
//     }
// });


// var test = new Vue({
//     //i18n,
//     el: "#testDiv",
//     data: {
//         selectedLanguage: 'en',
//         selectedCurrency: header.selectedCurrency
//     }
// });

var maininstance = new Vue({
  el: '#main_banner',
  name: 'main_banner',
  data: {
    labels: {}
  },
  mounted: function (){
    this.pageContent();
  },
  methods: {
    pageContent: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        self.dir = langauage == "ar" ? "rtl" : "ltr";
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Home/Home/Home.ftl';
        axios.get(pageurl, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          if (response.data.area_List.length) {
            var FormSearch = self.pluck('Search_Area_Lebels', self.content.area_List);
            self.labels = {
              Flight_Tab_Label: self.pluckcom('Flight_Tab_Label', FormSearch[0].component),
              Hotel_Tab_Label: self.pluckcom('Hotel_Tab_Label', FormSearch[0].component),
              Holiday_Tab_Label: self.pluckcom('Holiday_Tab_Label', FormSearch[0].component),
              Oneway_Label: self.pluckcom('Oneway__Label', FormSearch[0].component),
              Round_Trip_Label: self.pluckcom('Round_Trip_Label', FormSearch[0].component),
              Multi_City_Label: self.pluckcom('Multi_City_Label', FormSearch[0].component),
              Nearby_Airport_Label: self.pluckcom('Nearby_Airport_Label', FormSearch[0].component),
              Direct_Flight_Label: self.pluckcom('Direct_Flight_Label', FormSearch[0].component),
              Advanced_Search_Label: self.pluckcom('Advanced_Search_Label', FormSearch[0].component),
              Search_Button_Label: self.pluckcom('Search_Button_Label', FormSearch[0].component),
              Adult_Label: self.pluckcom('Adult_Label', FormSearch[0].component),
              Child_Label: self.pluckcom('Child_Label', FormSearch[0].component),
              Infant_Label: self.pluckcom('Infant_Label', FormSearch[0].component),
              Class_Label: self.pluckcom('Class_Label', FormSearch[0].component),
              Economy_Label: self.pluckcom('Economy_Label', FormSearch[0].component),
              Business_Label: self.pluckcom('Business_Label', FormSearch[0].component),
              First_Class_Label: self.pluckcom('First_Class_Label', FormSearch[0].component),
              Done_Button_Label: self.pluckcom('Done_Button_Label', FormSearch[0].component),
              Child_Age_Label: self.pluckcom('Child_Age_Label', FormSearch[0].component),
              Infant_Age_Label: self.pluckcom('Infant_Age_Label', FormSearch[0].component),
              Adult_Age_Label: self.pluckcom('Adult_Age_Label', FormSearch[0].component),
              Traveler_Placeholder: self.pluckcom('Traveler_Placeholder', FormSearch[0].component),
              placeholderfrom: self.pluckcom('From_City_Placeholder', FormSearch[0].component),
              placeholderTo: self.pluckcom('To_City_Placeholder', FormSearch[0].component),
              departureDatePlaceholder: self.pluckcom('Departure_Date_Placeholder', FormSearch[0].component),
              returnDatePlaceholder: self.pluckcom('Return_Date_Placeholder', FormSearch[0].component),
              Preferred_Airline_Placeholder: self.pluckcom('Preferred_Airline_Placeholder', FormSearch[0].component),
              Add_Trip_Label: self.pluckcom('Add_Trip_Label', FormSearch[0].component),
              Trip_Label: self.pluckcom('Trip_Label', FormSearch[0].component),
              City_Placeholder: self.pluckcom('City_Placeholder', FormSearch[0].component),
              Include_City_Label: self.pluckcom('Include_City_Label', FormSearch[0].component),
              Distance_Label: self.pluckcom('Distance_Label', FormSearch[0].component),
              Traveler_Label: self.pluckcom('Traveler_Label', FormSearch[0].component),
              Room_Label: self.pluckcom('Room_Label', FormSearch[0].component),
              Add_Room_Label: self.pluckcom('Add_Room_Label', FormSearch[0].component),
              Delete_Button_Label: self.pluckcom('Delete_Button_Label', FormSearch[0].component),
            }
            setTimeout(function() {
              $(".showHideButton, .showHideButton2, .showHideButton3").off();
              $(".showHideButton, .showHideButton2, .showHideButton3").click(function () {
                $("#showHide, #showHide2, #showHide3").slideToggle(500);
                if ($(this).text() == 'Show') {
                  $(this).text(self.labels.Advanced_Search_Label);
                }
                else {
                  $(this).text(self.labels.Advanced_Search_Label);
                }
              });
            }, 500);
          }
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
      });
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
  }
})

var cmsapp = new Vue({
  el: '#cmsdiv',
  name: 'cms',
  data() {
    return {
      key: '',
      content: null,
      getdata: true,
      dir: 'ltr',
      Socialmedia:{},
      isLoading: false,
      fullPage: true,
    }
  },
  mounted: function () {
    this.getcmsdata();


  },
  methods: {


    moment: function () {
      return moment();
    },
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getcmsdata: function () {
      var self = this;
      // self.isLoading = true;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        self.dir = langauage == "ar" ? "rtl" : "ltr";
        var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Home/Home/Home.ftl';
        var cmsurl = huburl + portno + homecms;
        axios.get(cmsurl, {
          headers: {
            'content-type': 'text/html',
            'Accept': 'text/html',
            'Accept-Language': langauage
          }
        }).then(function (response) {
          self.content = response.data;
          self.getdata = false;
          var backg = self.pluck('main', self.content.area_List);
          var background = self.pluckcom('Banner', backg[0].component);
          setTimeout(function () {
            owlcarosl(background);
          }, 100);
              var Socialmedia = self.pluck('Social_Media_Aggregation', self.content.area_List);
              self.Socialmedia.Title = self.pluckcom('Title', Socialmedia[0].component);
              self.Socialmedia.Icon = self.pluckcom('Icon', Socialmedia[0].component);
              // self.isLoading = false;
              self.stopLoader();
        }).catch(function (error) {
          console.log('Error');
          self.content = [];
          // self.isLoading = false;
          self.stopLoader();
        });
      });
    },
    async cmsRequestData(callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
        },
        body: data, // body data type must match "Content-Type" header
      });
      try {
        const myJson = await response.json();
        return myJson;
      } catch (error) {
        return object;
      }
    },
    stopLoader: function () {
      var div = document.getElementById("preloader");
      // if (div.style.display !== "none") {  
      div.style.display = "none";
      // }  else {  
      //     div.style.display = "block";  
      // }
  }
  },
  filters: {
    moment: function (date) {
      return moment(date).format('DD MMM YYYY');
    }
  }

});

function owlcarosl(imageUrl) {
  $('#main_banner').css('background-image', 'url(' + imageUrl + ')');
  $("#owl-demo-2").owlCarousel({
    items: 3,
    lazyLoad: true,
    loop: true,
    margin: 30,
    navigation: true,
    itemsDesktop: [991, 2],
    itemsDesktopSmall: [979, 2],
    itemsTablet: [768, 2],
    itemsMobile: [640, 1],
  });
  $(".owl-prev").html('<i class="fa fa-angle-left"></i>');
  $(".owl-next").html('<i class="fa fa-angle-right"></i>');
}

$(document).ready(function () {
  var active_el = (sessionStorage.active_el) ? sessionStorage.active_el : 1;
  if (active_el == 1) {
    $('.nav-tabs a[href="#flights"]').tab('show');
  } else if (active_el == 2) {
    $('.nav-tabs a[href="#hotels"]').tab('show');
  } else if (active_el == 3) {
    $('.nav-tabs a[href="#flight-hotle"]').tab('show');
  } else {
    $('.nav-tabs a[href="#flights"]').tab('show');
  }

});
