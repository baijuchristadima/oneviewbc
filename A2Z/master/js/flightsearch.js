var finalResult = [];
var searcharry = [];
var travellerComponent = Vue.component('travellers', {
  props: {
    value: {
      default: ''
    },
    itemId: String,
    adtNo: {
      default: 1
    },
    chdNo: Number,
    infNo: Number,
    labels: Object
  },
  template: ` <div class="travellersDropdown flt">
  <div class="custom-select-v3 lg">
    <input class="form-control" v-model="Totaltravaller" :placeholder="Totaltravaller" readonly autocomplete="off" :id="'totStr'+itemId" type="button">
  </div><div class="travelerSelectWindow active" style="display: none;">
  <div :id="'travel'+itemId">
    <div class="travelDropdown">
      <div class="section-pass">
        <fieldset class="ar_direction">
          <label>{{labels.Adult_Label}}</label>
          <div class="trvFld"><span class="custom-select-v3 sm">
            <select class="elmAdult _apxSelection" :id="'adt'+itemId" v-on:change="setCHDINFTravellers" v-model="selected_adult">
            <option v-for="adult in adults" :value="adult.value">{{adult.text}}</option>
            </select>
            </span></div>
          <span class="ageRange">{{labels.Adult_Age_Label}}</span>
        </fieldset>
      </div>
      <div class="section-pass">
        <fieldset class="ar_direction">
          <label>{{labels.Child_Label}}</label>
          <div class="trvFld"><span class="custom-select-v3 sm">
            <select class="elmChild _apxSelection" :id="'chd'+itemId" v-model="selected_child" v-on:change="SetInfantTravellers">
            <option v-for="child in children"  :value="child.value">{{child.text}}</option>
            </select>
            </span></div>
          <span class="ageRange">{{labels.Child_Age_Label}}</span>
        </fieldset>
      </div>
      <div class="section-pass">
        <fieldset class="ar_direction">
          <label>{{labels.Infant_Label}}</label>
          <div class="trvFld"><span class="custom-select-v3 sm">
            <select class="elmInfant _apxSelection" :id="'inf'+itemId" v-model="selected_infant" v-on:change="setTravelInfo" >
            <option v-for="infant in infants" :value="infant.value">{{infant.text}}</option>
            </select>
            </span></div>
          <span class="ageRange">{{labels.Infant_Age_Label}}</span>
        </fieldset>
      </div>
    </div>
  </div>
  <div class="classSec ar_direction">
    <label class="sp_lab">{{labels.Class_Label}}</label>
    <div class="col-md-12 text-center m-t-10 sp_radio_btns">
      <div class="radio radio-success radio-inline">
        <input type="radio" v-model="flightClass" :id="'inlineRadio1'+itemId" v-bind:value="'Economy'" v-on:change="setTravelInfo" :name="'radioInline'+itemId" checked >
        <label :for="'inlineRadio1'+itemId">{{labels.Economy_Label}}</label>
      </div>      
      <div class="radio radio-success radio-inline">
        <input type="radio" v-model="flightClass" :id="'inlineRadio3'+itemId" v-bind:value="'Business'" v-on:change="setTravelInfo" :name="'radioInline'+itemId">
        <label :for="'inlineRadio3'+itemId">{{labels.Business_Label}}</label>
      </div>
      <div class="radio radio-success radio-inline">
        <input type="radio" v-model="flightClass" :id="'inlineRadio4'+itemId" v-bind:value="'First Class'" v-on:change="setTravelInfo" :name="'radioInline'+itemId">
        <label :for="'inlineRadio4'+itemId">{{labels.First_Class_Label}}</label>
      </div>
    </div>
  </div>
  <input id="dataObject" value="{&quot;class&quot;:&quot;Economy&quot;,&quot;noOfadults&quot;:&quot;1&quot;,&quot;noOfChildren&quot;:&quot;0&quot;,&quot;noOfInfant&quot;:&quot;0&quot;}" type="hidden">
  <div class="closeDropdown"> <span class="closeBtn closeLink">{{labels.Done_Button_Label}}</span> </div>
</div></div>`,
  data: function () {
    return {
      adults: [{
        'value': 1,
        'text': '1'
      }, {
        'value': 2,
        'text': '2'
      }, {
        'value': 3,
        'text': '3'
      }, {
        'value': 4,
        'text': '4'
      }, {
        'value': 5,
        'text': '5'
      }, {
        'value': 6,
        'text': '6'
      }, {
        'value': 7,
        'text': '7'
      }, {
        'value': 8,
        'text': '8'
      }, {
        'value': 9,
        'text': '9'
      }],
      children: [{
        'value': 0,
        'text': '0'
      }, {
        'value': 1,
        'text': '1'
      }, {
        'value': 2,
        'text': '2'
      }, {
        'value': 3,
        'text': '3'
      }, {
        'value': 4,
        'text': '4'
      }, {
        'value': 5,
        'text': '5'
      }, {
        'value': 6,
        'text': '6'
      }, {
        'value': 7,
        'text': '7'
      }, {
        'value': 8,
        'text': '8'
      }],
      infants: [{
        'value': 0,
        'text': '0'
      }, {
        'value': 1,
        'text': '1'
      }],
      selected_adult: 1,
      selected_child: 0,
      selected_infant: 0,
      totalAllowdPax: 9,
      Totaltravaller: this.Traveler_Placeholder,
      child: 0,
      flightClass: getCabinName((searcharry.cabin) ? searcharry.cabin.toUpperCase() : 'Y')
    }
  },
  methods: {
    setCHDINFTravellers: function () {
      // this.selected_adult = parseInt(event.target.value);
      this.children = [];
      this.infants = [];
      this.child = this.totalAllowdPax - this.selected_adult;
      for (var chd = 0; chd <= this.child; chd++) {
        this.children.push({
          'value': chd,
          'text': chd
        })
      }
      if (this.selected_child > 0 && this.selected_child <= this.child) {
        this.selected_child = this.selected_child;
      } else {
        this.selected_child = 0;
      }
      infant = parseInt(this.selected_adult);
      if (infant + parseInt(this.selected_adult) + parseInt(this.selected_child) > 9) {
        infant = parseInt(this.totalAllowdPax) - (parseInt(this.selected_adult) + parseInt(this.selected_child));
      }
      infant = ((parseInt(infant) < 0) ? 0 : infant);
      if (infant == 0) this.selected_infant = 0;
      for (var inf = 0; inf <= infant; inf++) {
        this.infants.push({
          'value': inf,
          'text': inf
        })
      }
      if (this.selected_infant > 0) {
        this.selected_infant = (this.selected_infant <= infant) ? this.selected_infant : 0;
      }
      totalpax = parseInt(this.selected_adult) + parseInt(this.selected_child) + parseInt(this.selected_infant);
      this.Totaltravaller = totalpax + " Travellers, " + this.flightClass;
      this.$emit('set-chdinf-travellers', this.selected_adult, this.selected_child, this.selected_infant, this.flightClass);
    },
    SetInfantTravellers: function (event) {
      this.infants = [];
      remiaingpax = parseInt(this.selected_adult);
      if (remiaingpax + parseInt(this.selected_adult) + parseInt(this.selected_child) > 9) {
        remiaingpax = parseInt(this.totalAllowdPax) - (parseInt(this.selected_adult) + parseInt(this.selected_child));
      }
      remiaingpax = ((parseInt(remiaingpax) < 0) ? 0 : remiaingpax);
      for (var inf = 0; inf <= remiaingpax; inf++) {
        this.infants.push({
          'value': inf,
          'text': inf
        })
      }
      if (this.selected_infant > 0) {
        this.selected_infant = (this.selected_infant <= remiaingpax) ? this.selected_infant : 0;
      }
      totalpax = parseInt(this.selected_adult) + parseInt(this.selected_child) + parseInt(this.selected_infant);
      this.Totaltravaller = totalpax + " Travellers, " + this.flightClass;
      this.$emit('set-chdinf-travellers', this.selected_adult, this.selected_child, this.selected_infant, this.flightClass);
    },
    setTravelInfo: function () {
      totalpax = parseInt(this.selected_adult) + parseInt(this.selected_child) + parseInt(this.selected_infant);
      this.Totaltravaller = totalpax + " Travellers, " + this.flightClass;
      this.$emit('set-chdinf-travellers', this.selected_adult, this.selected_child, this.selected_infant, this.flightClass);
    },
    setmodify: function () {
      this.selected_adult = this.adtNo;
      this.selected_child = this.chdNo;
      this.selected_infant = this.infNo;
      this.setCHDINFTravellers()
    }
  },
  watch: {
    adtNo: function () {
      this.setmodify();
    },
    labels: function () {
      this.Totaltravaller = this.labels.Traveler_Placeholder;
    }
  }
});
var flightserchfromComponent = Vue.component('flightserch', {
  data() {
    return {
      access_token: '',
      KeywordSearch: '',
      resultItemsarr: [],
      autoCompleteProgress: false,
      highlightIndex: 0,
      agencyNode: ""
    }
  },
  props: {
    itemText: String,
    itemId: String,
    placeHolderText: String,
    returnValue: Boolean,
    id: {
      type: String,
      default: '',
      required: false
    },
  },
  template: `<div class="autocomplete">
      <input type="text" :disabled="id=='fromcity1'&&agencyNode=='AGY1190'" :placeholder="placeHolderText" :id="id" :data-aircode="KeywordSearch" autocomplete="off"  
      v-model="KeywordSearch" class="form-control" 
      :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !autoCompleteProgress }"  
      @input="onSelectedAutoCompleteEvent(KeywordSearch);highlightIndex=0"
      @keydown.down="down"
      @keydown.up="up"
      @keydown.tab="autoCompleteProgress=false"
      @keydown.esc="autoCompleteProgress=false"
      @keydown.enter="onSelected(resultItemsarr[highlightIndex].label,resultItemsarr[highlightIndex].code)" />
      <ul ref="searchautocomplete" class="autocomplete-results" v-if="resultItemsarr.length>0">
          <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item.label,item.code)"">
          {{ item.label }}
      </li>
      </ul>
  </div>`,
  mounted() {
    this.agencyNode = localStorage.AgencyCode;
  },
  methods: {
    up: function () {
      if (this.autoCompleteProgress) {
        if (this.highlightIndex > 0) {
          this.highlightIndex--
        }
      } else {
        this.autoCompleteProgress = true;
      }
      this.fixScrolling();
    },
    down: function () {
      if (this.autoCompleteProgress) {
        if (this.highlightIndex < this.resultItemsarr.length - 1) {
          this.highlightIndex++
        } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
          this.highlightIndex = 0;
        }
      } else {
        this.autoCompleteProgress = true;
      }
      this.fixScrolling();
    },
    fixScrolling: function () {
      if (this.$refs.options[this.highlightIndex]) {
        var liH = this.$refs.options[this.highlightIndex].clientHeight;
        if (liH == 50) {
          liH = 32;
        }
        if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
          this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
        }
      }
    },
    onSelectedAutoCompleteEvent: _.debounce(async function (keyEnter) {
      var self = this;
      var keywordEntered = keyEnter;
      if (keywordEntered.length > 2) {
        this.autoCompleteProgress = true;
        self.resultItemsarr = await getFlightResultUsingElasticSearch(keywordEntered);
        
      } else {
        this.autoCompleteProgress = false;
        this.resultItemsarr = [];
      }
    }, 100),
    onSelected: function (label, code) {
      this.autoCompleteProgress = false
      this.KeywordSearch = label;
      this.resultItemsarr = [];
      this.$emit('air-search-completed', code, label);
    }
  }
});
var milticityflight = Vue.component('muticitydiv', {
  props: {
    value: {
      default: ''
    },
    ind: Number,
    lgth: Number,
    labels: Object
  },
  data() {
    return {
      // placeholderfrom: "From City",
      // placeholderTo: "To City",
    }
  },
  template: ` <div style="width: 100%;float:left; display: inline-block;" :data-id="lgth">
  <div class="flx_package ar_direction wsec">
  <div class="col-md-3  col-sm-6 col-xs-12 search-col-padding">
  <flightserch :place-holder-text="labels.placeholderfrom"  :id="'fromcity'+(value+3)" @air-search-completed="DepartureFrom"></flightserch>                            
    <span class="change-destination"  @click="SwapLocation(value+3,value)" ></span> </div>
  <div class="col-md-3 col-sm-6 col-xs-12 search-col-padding">
  <flightserch  :place-holder-text="labels.placeholderTo" :id="'tocity'+(value+3)" @air-search-completed="ArrivalFrom"></flightserch> </div>
  <div class="col-md-2 col-sm-6 col-xs-12 search-col-padding">
    <input :id="'multidepadte'+ind" class="calendar" :data-id="ind" type="text" :placeholder="labels.departureDatePlaceholder" readonly="readonly"  />
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 search-col-padding">
    <div class="close-btn"><a @click="DelteeLeg(value)" v-if="lgth==ind&& lgth!=1" ><i class="fa fa-close" aria-hidden="true"></i></a>
    <a class="add-btn" v-if="lgth==ind&& lgth!=5" @click="addNewLeg(value)"><i class="fa fa-plus" aria-hidden="true"></i>{{labels.Add_Trip_Label}} {{5-lgth}} {{labels.Trip_Label}}</a>
    </div>
  </div>
  </div>

</div>`,
  methods: {
    addNewLeg: function (leg) {
      this.$emit('add-new-leg', leg);

    },
    DelteeLeg: function (leg) {
      this.$emit('delete-leg', leg);
    },
    DepartureFrom: function (code, label) {
      this.$emit('departure-from', code, label, this.ind);
    },
    ArrivalFrom: function (code, label) {
      this.$emit('arrival-from', code, label, this.ind);
    },
    SwapLocation: function (id, leg) {
      this.$emit('swap-location', id, leg);
    }
  },
  mounted: function () {
    if (localStorage.direction == 'rtl') {
      $(".ar_direction").addClass("ar_direction1");
    } else {
      $(".ar_direction").removeClass("ar_direction1");

    }
  }
});
var preferdairlinecomp = Vue.component('preferdairline', {
  props: {
    placeholderPreferred: String,
  },
  template: `<div class="autocomplete">
      <input type="text" :placeholder="placeholderPreferred"  v-model="KeywordSearch" class="form-control" :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItems.length > 0 || resultItems.length == 0 && !autoCompleteProgress  }" @keyup="onSelectedAutoCompleteEvent($event)"/>
      <ul class="autocomplete-results" v-if=" resultItems.length > 0">
          <li class="autocomplete-result" v-for="(item,i) in resultItems" :key="i" @click="onSelected(item.code, item.label)">
              {{ item.label }}
          </li>
      </ul>
  </div>`,
  data() {
    return {
      KeywordSearch: '',
      resultItems: [],
      autoCompleteProgress: false,
      airlineList: []
    }
  },
  methods: {
    onSelectedAutoCompleteEvent: _.debounce(function (event) {
      var keywordEntered = event.target.value;

      if (keywordEntered.length > 2) {
        this.autoCompleteProgress = true;
        var newData = [];
        this.airlineList.filter(function (el) {
          if (el.A.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
            newData.push({
              "code": el.C,
              "label": el.A
            });
          }
        });
        this.resultItems = newData;
        this.autoCompleteProgress = false;
        console.log(newData);
      }
    }, 100),
    onSelected: function (code, label) {
      this.KeywordSearch = label;
      this.resultItems = [];
      this.$emit('prefred-search-completed', code, label);
    }
  },
  created: function () {
    this.airlineList = AirlinesDatas;
  }
});
var oneway = Vue.component('oneway', {
  props: {
    placeholderfrom: String,
    placeholderTo: String,
    departureDatePlaceholder: String,
    labels: Object
  },
  data() {
    return {
      itemid: "oneway",
      // placeholderfrom: "From City",
      // placeholderTo: "To City",
      CityFrom: '',
      CityTo: '',
      validationMessage: '',
      cabinclass: 'Y',
      adtcount: 1,
      chdcount: 0,
      infcount: 0,
      returnValue: true,
      id1: 'fromcity1',
      id2: 'tocity1',
      preferAirline: '',
      Search: '',
      Departure_Date: '',
      Return_Date: '',
      adtno: 1,
      chdno: 0,
      infno: 0,
      agencyNode: localStorage.AgencyCode,
      calendarSearch: false
    }
  },
  created: function () {
    setModifySerach();

  },
  mounted: function () {
    var dateFormat = generalInformation.systemSettings.systemDateFormat;
    var noOfMonths = generalInformation.systemSettings.calendarDisplay;
    var startDate = new Date();
    var sDate = new Date(moment(startDate, "DD/MM/YYYY")).getTime();
    $("#deptDate").datepicker({
      minDate: "0d",
      maxDate: "360d",
      numberOfMonths: parseInt(noOfMonths),
      changeMonth: true,
      changeYear: true,
      showButtonPanel: false,
      dateFormat: dateFormat,
      onSelect: function (date) {
        var startDate = $(this).datepicker('getDate');
        sDate = startDate.getTime();

        $("#arrDate01").datepicker("option", "minDate", startDate);
        $("#multidepadte").datepicker("option", "minDate", startDate);
        $("#txtLeg2Date").datepicker("option", "minDate", startDate);
        $("#txtLeg3Date").datepicker("option", "minDate", startDate);
        $("#txtLeg4Date").datepicker("option", "minDate", startDate);
        $("#txtLeg5Date").datepicker("option", "minDate", startDate);
        $("#txtLeg6Date").datepicker("option", "minDate", startDate);
      }
    });
    $('#deptDate').val(moment(startDate).format('DD MMM YY, ddd'));
    this.setmodify();
  },
  methods: {
    setmodify: function () {
      this.CityFrom = (searcharry.originDestination) ? searcharry.originDestination[0].originLocation : "";
      this.CityTo = (searcharry.originDestination) ? searcharry.originDestination[0].destinationLocation : "";
      if (searcharry.originDestination) {
        $("#deptDate").val(moment(searcharry.originDestination[0].departureDate, 'DD-MM-YYYY').format('DD MMM YY, ddd'));
        $("#deptDate01").val(moment(searcharry.originDestination[0].departureDate, 'DD-MM-YYYY').format('DD MMM YY, ddd'));
        $("#multidepadte").val(moment(searcharry.originDestination[0].departureDate, 'DD-MM-YYYY').format('DD MMM YY, ddd'));
        this.adtcount = (searcharry.passengerTypeQuantity) ? searcharry.passengerTypeQuantity.adt : 1;
        this.chdcount = (searcharry.passengerTypeQuantity) ? searcharry.passengerTypeQuantity.chd : 0;
        this.infcount = (searcharry.passengerTypeQuantity) ? searcharry.passengerTypeQuantity.inf : 0;
        this.adtno = this.adtcount
        this.chdno = this.chdcount;
        this.infno = this.infcount;
        $("#adtoneway").val(this.adtcount);
        $("#adttwoway").val(this.adtcount);
        $("#adtmulticity").val(this.adtcount);
        $("#chdoneway").val(this.chdcount);
        $("#chdtwoway").val(this.chdcount);
        $("#chdmulticity").val(this.chdcount);
        $("#infoneway").val(this.infcount);
        $("#inftwoway").val(this.infcount);
        $("#infmulticity").val(this.infcount);
        var tptalpx = this.adtcount + this.chdcount + this.infcount;
        this.cabinclass = (searcharry.cabin) ? searcharry.cabin.toUpperCase() : 'Y';
        var cabin = getCabinName(this.cabinclass.toUpperCase());
        $("input[name=radioInlineoneway][value='" + cabin + "']").prop('checked', true);
        $("#totStroneway").val(tptalpx + ' Travellers, ' + cabin);
        $("#totStrtwoway").val(tptalpx + ' Travellers, ' + cabin);
        $("#totStrmulticity").val(tptalpx + ' Travellers, ' + cabin);
      }
    },
    Departurefrom(AirportCode, AirportName) {
      if (AirportCode == this.CityTo) {
        this.returnValue = false;
        this.validationMessage = "Departure and arrival airports should not be same !";
        var self = this;
        this.CityFrom = '';
        setTimeout(function () {
          self.validationMessage = '';
        }, 1500);
      } else {
        this.returnValue = true;
        this.CityFrom = AirportCode;
      }
    },
    Arrivalfrom(AirportCode, AirportName) {
      if (this.CityFrom == AirportCode) {
        this.returnValue = false;
        this.validationMessage = "Departure and arrival airports should not be same !";
        this.CityTo = '';
        var self = this;
        setTimeout(function () {
          self.validationMessage = '';
        }, 1500);
      } else {
        this.returnValue = true;
        this.CityTo = AirportCode;
      }
    },
    SetPreferdAirline(code, label) {
      this.preferAirline = code;
    },
    OnewaySerch: function () {
      var Departuredate = $('#deptDate').val() == "" ? "" : $('#deptDate').datepicker('getDate');
      if (!this.CityFrom) {
        this.validationMessage = "Please fill origin !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = '';
        }, 1500);
        return false;
      }
      if (!this.CityTo) {
        this.validationMessage = "Please fill destination !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = '';
        }, 1500);
        return false;
      }
      if (!Departuredate) {
        this.validationMessage = "Please choose departure date !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = '';
        }, 1500);
        return false;
      }
      if ($('#checkbox-list-3').is(':checked')) {
        var directFlight = "DF";
      } else {
        var directFlight = "AF";
      }
      var adult = this.adtcount;
      var child = this.chdcount;
      var infant = this.infcount;
      var cabin = this.cabinclass ? this.cabinclass : 'Y';
      var sec1TravelDate = moment(Departuredate).format('DD|MM|YYYY');
      var sectors = this.CityFrom + '-' + this.CityTo + '-' + sec1TravelDate;
      var preferAirline = this.preferAirline;
      var calendarSearch = this.calendarSearch ? '1' : '0';
      getSuppliers([this.CityFrom + '|' + this.CityTo],
      function(supp) {
        var searchUrl = '/Flights/flight-listing.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+ supp +'-50-F-O-F-' + preferAirline + '-' + directFlight + '-' + calendarSearch;
        // searchUrl = searchUrl.toLocaleLowerCase();
        window.location.href = searchUrl;
      })
    },
    setCHDINFTravellers(adt, chd, inf, cabin) {
      this.cabinclass = getAircabinclass(cabin);
      this.adtcount = parseInt(adt);
      this.chdcount = parseInt(chd);
      this.infcount = parseInt(inf);
    },
    getAircabinclass: function (cabin) {
      getAircabinclass(cabin)
    },
    getCabinName: function (cabin) {
      getCabinName(cabin)
    },
    swapLocations: function (id) {
      if ((this.CityFrom) && (this.CityTo)) {
        var from = this.CityFrom;
        var to = this.CityTo;
        this.CityFrom = to;
        this.CityTo = from;
        swpaloc(id)
      }
    }
  },

});
var roundtrip = Vue.component('roundtrip', {
  props: {
    placeholderfrom: String,
    placeholderTo: String,
    departureDatePlaceholder: String,
    returnDatePlaceholder: String,
    labels: Object
  },
  data() {
  return {
      itemid: "twoway",
      // placeholderfrom: "From City",
      // placeholderTo: "To City",
      CityFrom: '',
      CityTo: '',
      validationMessage: '',
      cabinclass: '',
      adtcount: 1,
      chdcount: 0,
      infcount: 0,
      id1: 'fromcity2',
      id2: 'tocity2',
      preferAirline: '',
      Search: '',
      Departure_Date: '',
      Return_Date: '',
      adtno: 1,
      chdno: 0,
      infno: 0,
      agencyNode: localStorage.AgencyCode,
      calendarSearch: false
    }
  },
  created: function () { },
  mounted: function () {
    var dateFormat = generalInformation.systemSettings.systemDateFormat;
    var noOfMonths = generalInformation.systemSettings.calendarDisplay;
    var startDate = new Date();
    var endDate = new Date();
    var sDate = new Date(moment(startDate, "DD/MM/YYYY")).getTime();
    var eDate = new Date(moment(endDate, "DD/MM/YYYY")).getTime();
    $("#deptDate01").datepicker({
      minDate: "0d",
      maxDate: "360d",
      numberOfMonths: parseInt(noOfMonths),
      changeMonth: true,
      showButtonPanel: false,
      dateFormat: dateFormat,
      onSelect: function (date) {
        $("#arrDate01").datepicker("option", "minDate", date);
        var startDate = $(this).datepicker('getDate');
        sDate = startDate.getTime();
      }
    });
    $("#arrDate01").datepicker({
      minDate: "0d",
      maxDate: "360d",
      numberOfMonths: parseInt(noOfMonths),
      changeMonth: true,
      showButtonPanel: false,
      dateFormat: dateFormat,
      beforeShow: function (event, ui) {
        var selectedDate = $("#deptDate01").val();
        $("#arrDate01").datepicker("option", "minDate", selectedDate);
      },
      onSelect: function (selectedDate) {
        var endDate = $(this).datepicker('getDate');
        eDate = endDate.getTime();
      }
    });
    this.setmodify();
  },
  methods: {
    setmodify: function () {
      this.CityFrom = (searcharry.originDestination) ? searcharry.originDestination[0].originLocation : "";
      this.CityTo = (searcharry.originDestination) ? searcharry.originDestination[0].destinationLocation : "";
      if (searcharry.originDestination) {
        $("#deptDate").val(moment(searcharry.originDestination[0].departureDate, 'DD-MM-YYYY').format('DD MMM YY, ddd'));
        $("#deptDate01").val(moment(searcharry.originDestination[0].departureDate, 'DD-MM-YYYY').format('DD MMM YY, ddd'));
        $("#multidepadte").val(moment(searcharry.originDestination[0].departureDate, 'DD-MM-YYYY').format('DD MMM YY, ddd'));
        if (searcharry.triptype.toLowerCase() != "o") {
          $("#arrDate01").val(moment(searcharry.originDestination[1].departureDate, 'DD-MM-YYYY').format('DD MMM YY, ddd'));
        }
        this.adtcount = (searcharry.passengerTypeQuantity) ? searcharry.passengerTypeQuantity.adt : 1;
        this.chdcount = (searcharry.passengerTypeQuantity) ? searcharry.passengerTypeQuantity.chd : 0;
        this.infcount = (searcharry.passengerTypeQuantity) ? searcharry.passengerTypeQuantity.inf : 0;
        this.adtno = this.adtcount
        this.chdno = this.chdcount;
        this.infno = this.infcount;
        $("#adtoneway").val(this.adtcount);
        $("#adttwoway").val(this.adtcount);
        $("#adtmulticity").val(this.adtcount);
        $("#chdoneway").val(this.chdcount);
        $("#chdtwoway").val(this.chdcount);
        $("#chdmulticity").val(this.chdcount);
        $("#infoneway").val(this.infcount);
        $("#inftwoway").val(this.infcount);
        $("#infmulticity").val(this.infcount);
        var tptalpx = this.adtcount + this.chdcount + this.infcount;
        this.cabinclass = (searcharry.cabin) ? searcharry.cabin.toUpperCase() : 'Y';
        var cabin = getCabinName(this.cabinclass.toUpperCase());
        $("input[name=radioInlinetwoway][value='" + cabin + "']").prop('checked', true);

        $("#totStroneway").val(tptalpx + ' Travellers, ' + cabin);
        $("#totStrtwoway").val(tptalpx + ' Travellers, ' + cabin);
        $("#totStrmulticity").val(tptalpx + ' Travellers, ' + cabin);
      }
    },
    TwowaySerch: function () {
      var Departuredate = $('#deptDate01').val() == "" ? "" : $('#deptDate01').datepicker('getDate');
      var ArrivalDate = $('#arrDate01').val() == "" ? "" : $('#arrDate01').datepicker('getDate');
      if (!this.CityFrom) {
        this.validationMessage = "Please fill origin !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = '';
        }, 1500);
        return false;
      }
      if (!this.CityTo) {
        this.validationMessage = "Please fill destination !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = '';
        }, 1500);
        return false;
      }
      if (!Departuredate) {
        this.validationMessage = "Please choose departure date !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = '';
        }, 1500);
        return false;
      }
      if (!ArrivalDate) {
        this.validationMessage = "Please choose arrival date !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = '';
        }, 1500);
        return false;
      }
      if ($('#checkbox-list-6').is(':checked')) {
        var directFlight = "DF";
      } else {
        var directFlight = "AF";
      }
      var adult = this.adtcount;
      var child = this.chdcount;
      var infant = this.infcount;
      var cabin = this.cabinclass ? this.cabinclass : 'Y';
      var sec1TravelDate = moment(Departuredate).format('DD|MM|YYYY');
      var returnDate = moment(ArrivalDate).format('DD|MM|YYYY');;
      var sectors = this.CityFrom + '-' + this.CityTo + '-' + sec1TravelDate;
      sectors += '/' + this.CityTo + '-' + this.CityFrom + '-' + returnDate;
      var preferAirline = this.preferAirline;
      var calendarSearch = this.calendarSearch ? '1' : '0';
      getSuppliers([this.CityFrom + '|' + this.CityTo],
        function(supp) {
          var searchUrl = '/Flights/flight-listing.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-R-F-' + preferAirline + '-' + directFlight + '-' + calendarSearch;
          // searchUrl = searchUrl.toLocaleLowerCase();
          window.location.href = searchUrl;
        }
      );
      
    },
    setCHDINFTravellers(adt, chd, inf, cabin) {
      this.cabinclass = getAircabinclass(cabin);
      this.adtcount = parseInt(adt);
      this.chdcount = parseInt(chd);
      this.infcount = parseInt(inf);
    },
    Departurefrom(AirportCode, AirportName) {
      this.CityFrom = AirportCode;
    },
    Arrivalfrom(AirportCode, AirportName) {
      this.CityTo = AirportCode;
    },
    SetPreferdAirline(code, label) {
      this.preferAirline = code;
    },
    getAircabinclass: function (cabin) {
      getAircabinclass(cabin)
    },
    swapLocations: function (id) {
      if ((this.CityFrom) && (this.CityTo)) {
        var from = this.CityFrom;
        var to = this.CityTo;
        this.CityFrom = to;
        this.CityTo = from;
        swpaloc(id)
      }
    }
  }
});
var multicity = Vue.component('multicity', {
  props: {
    placeholderfrom: String,
    placeholderTo: String,
    departureDatePlaceholder: String,
    labels: Object
  },
  data() { 
    return {
      itemid: "multicity",
      // placeholderfrom: "From City",
      // placeholderTo: "To City",
      CityFrom: '',
      CityTo: '',
      cityList: [],
      validationMessage: '',
      cabinclass: '',
      adtcount: 1,
      chdcount: 0,
      infcount: 0,
      returnValue: true,
      legcount: 1,
      legs: [],
      id1: 'fromcity3',
      id2: 'tocity3',
      preferAirline: '',
      Search: '',
      Departure_Date: '',
      Return_Date: '',
      adtno: 1,
      chdno: 0,
      infno: 0,
      agencyNode: localStorage.AgencyCode,
      calendarSearch: false
    }
  },
  mounted: function () {
    var dateFormat = generalInformation.systemSettings.systemDateFormat;
    var noOfMonths = generalInformation.systemSettings.calendarDisplay;
    var startDate = new Date();
    var endDate = new Date();
    $("#multidepadte").datepicker({
      minDate: "0d",
      maxDate: "360d",
      numberOfMonths: parseInt(noOfMonths),
      changeMonth: true,
      showButtonPanel: false,
      dateFormat: dateFormat,
      onSelect: function (date) {
        $("#multidepadte1").datepicker("option", "minDate", date);
        $("#multidepadte2").datepicker("option", "minDate", date);
        $("#multidepadte3").datepicker("option", "minDate", date);
        $("#multidepadte4").datepicker("option", "minDate", date);
        $("#multidepadte5").datepicker("option", "minDate", date);
        var startDate = $(this).datepicker('getDate');
      }
    });
    $("#multidepadte1").datepicker({
      minDate: $("#multidepadte").datepicker("getDate") == null ? "0d" : $("#multidepadte").datepicker("getDate"),
      maxDate: "360d",
      numberOfMonths: parseInt(noOfMonths),
      changeMonth: true,
      showButtonPanel: false,
      dateFormat: dateFormat,
      beforeShow: function (event, ui) {
        var selectedDate = $("#multidepadte").val();
        $("#multidepadte1").datepicker("option", "minDate", selectedDate);
      },
      onSelect: function (date) {
        $("#multidepadte2").datepicker("option", "minDate", date);
        $("#multidepadte3").datepicker("option", "minDate", date);
        $("#multidepadte4").datepicker("option", "minDate", date);
        $("#multidepadte5").datepicker("option", "minDate", date);
        var startDate = $(this).datepicker('getDate');
      }
    });
    this.setmodify();
  },
  updated: function () {
    var dateFormat = generalInformation.systemSettings.systemDateFormat;
    var noOfMonths = generalInformation.systemSettings.calendarDisplay;
    var startDate = new Date();
    $("#multidepadte2").datepicker({
      minDate: $("#multidepadte1").datepicker("getDate") == null ? "0d" : $("#multidepadte1").datepicker("getDate"),
      maxDate: "360d",
      numberOfMonths: parseInt(noOfMonths),
      changeMonth: true,
      showButtonPanel: false,
      dateFormat: dateFormat,
      onSelect: function (date) {
        $("#multidepadte3").datepicker("option", "minDate", date);
        $("#multidepadte4").datepicker("option", "minDate", date);
        $("#multidepadte5").datepicker("option", "minDate", date);
        var startDate = $(this).datepicker('getDate');
      }
    });
    $("#multidepadte3").datepicker({
      minDate: $("#multidepadte2").datepicker("getDate") == null ? "0d" : $("#multidepadte2").datepicker("getDate"),
      maxDate: "360d",
      numberOfMonths: parseInt(noOfMonths),
      changeMonth: true,
      showButtonPanel: false,
      dateFormat: dateFormat,
      onSelect: function (date) {
        $("#multidepadte4").datepicker("option", "minDate", date);
        $("#multidepadte5").datepicker("option", "minDate", date);
        var startDate = $(this).datepicker('getDate');
      }
    });
    $("#multidepadte4").datepicker({
      minDate: $("#multidepadte3").datepicker("getDate") == null ? "0d" : $("#multidepadte3").datepicker("getDate"),
      maxDate: "360d",
      numberOfMonths: parseInt(noOfMonths),
      changeMonth: true,
      showButtonPanel: false,
      dateFormat: dateFormat,
      onSelect: function (date) {
        $("#multidepadte5").datepicker("option", "minDate", date);
        var startDate = $(this).datepicker('getDate');
      }
    });
    $("#multidepadte5").datepicker({
      minDate: $("#multidepadte4").datepicker("getDate") == null ? "0d" : $("#multidepadte4").datepicker("getDate"),
      maxDate: "360d",
      numberOfMonths: parseInt(noOfMonths),
      changeMonth: true,
      showButtonPanel: false,
      dateFormat: dateFormat,
      onSelect: function (date) {
        var startDate = $(this).datepicker('getDate');
      }
    });
    $('#multidepadte5').val(moment(startDate).format('DD MMM YY, ddd'));
  },
  created: function () {
    var legno = this.legcount;
    this.cityList.push({
      id: legno,
      from: '',
      to: ''
    });
    this.setmodify();
  },
  methods: {
    setmodify: function () {
      this.CityFrom = (searcharry.originDestination) ? searcharry.originDestination[0].originLocation : "";
      this.CityTo = (searcharry.originDestination) ? searcharry.originDestination[0].destinationLocation : "";
      if (searcharry.originDestination) {
        $("#deptDate").val(moment(searcharry.originDestination[0].departureDate, 'DD-MM-YYYY').format('DD MMM YY, ddd'));
        $("#deptDate01").val(moment(searcharry.originDestination[0].departureDate, 'DD-MM-YYYY').format('DD MMM YY, ddd'));
        $("#multidepadte").val(moment(searcharry.originDestination[0].departureDate, 'DD-MM-YYYY').format('DD MMM YY, ddd'));
        if (searcharry.triptype.toLowerCase() != "o") {
          $("#arrDate01").val(moment(searcharry.originDestination[1].departureDate, 'DD-MM-YYYY').format('DD MMM YY, ddd'));
        }
        this.adtcount = (searcharry.passengerTypeQuantity) ? searcharry.passengerTypeQuantity.adt : 1;
        this.chdcount = (searcharry.passengerTypeQuantity) ? searcharry.passengerTypeQuantity.chd : 0;
        this.infcount = (searcharry.passengerTypeQuantity) ? searcharry.passengerTypeQuantity.inf : 0;
        this.adtno = this.adtcount
        this.chdno = this.chdcount;
        this.infno = this.infcount;
        $("#adtoneway").val(this.adtcount);
        $("#adttwoway").val(this.adtcount);
        $("#adtmulticity").val(this.adtcount);
        $("#chdoneway").val(this.chdcount);
        $("#chdtwoway").val(this.chdcount);
        $("#chdmulticity").val(this.chdcount);
        $("#infoneway").val(this.infcount);
        $("#inftwoway").val(this.infcount);
        $("#infmulticity").val(this.infcount);
        var tptalpx = this.adtcount + this.chdcount + this.infcount;
        this.cabinclass = (searcharry.cabin) ? searcharry.cabin : 'Y';
        var cabin = getCabinName(this.cabinclass.toUpperCase());
        $("input[name=radioInlinemulticity][value='" + cabin + "']").prop('checked', true);
        $("#totStroneway").val(tptalpx + ' Travellers, ' + cabin);
        $("#totStrtwoway").val(tptalpx + ' Travellers, ' + cabin);
        $("#totStrmulticity").val(tptalpx + ' Travellers, ' + cabin);
      }
      if (searcharry.originDestination) {
        if (searcharry.triptype.toLowerCase() != "o") {
          this.legcount = searcharry.originDestination.length - 1;
          this.cityList = [];
          for (i = 1; i < searcharry.originDestination.length; i++) {
            this.cityList.push({
              id: i,
              from: searcharry.originDestination[i].originLocation,
              to: searcharry.originDestination[i].destinationLocation
            });
            $("#multidepadte" + i).val(moment(searcharry.originDestination[i].departureDate, 'DD-MM-YYYY').format('DD MMM YY, ddd'));
            getAirportNameUsingElasticserch(searcharry.originDestination[i].originLocation, i, function (response, id) {
              if (response.name) {
                var from = 'fromcity' + (3 + id);
                $("#" + from).val(response.name + ", " + response.iso_country + '(' + response.iata_code + ')');
              }
            });
            getAirportNameUsingElasticserch(searcharry.originDestination[i].destinationLocation, i, function (response, id) {
              if (response.name) {
                var to = 'tocity' + (3 + id);
                $("#" + to).val(response.name + ", " + response.iso_country + '(' + response.iata_code + ')');
              }
            });
          }
          console.log(this.cityList);
        }
      }
    },
    MulticitySerch: function () {
      var Departuredate = $('#multidepadte').val() == "" ? "" : $('#multidepadte').datepicker('getDate');
      if (!this.CityFrom) {
        this.validationMessage = "Please fill origin !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = '';
        }, 1500);
        return false;
      }
      if (!this.CityTo) {
        this.validationMessage = "Please fill destination !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = '';
        }, 1500);
        return false;
      }
      if (!Departuredate) {
        this.validationMessage = "Please choose departure date !";
        var self = this;
        setTimeout(function () {
          self.validationMessage = '';
        }, 1500);
        return false;
      }
      var legDetails = [];
      var sec1TravelDate = moment(Departuredate).format('DD|MM|YYYY');
      var sectors = this.CityFrom + '-' + this.CityTo + '-' + sec1TravelDate;
      for (var legValue = 1; legValue <= this.legcount; legValue++) {
        var temDeparturedate = $('#multidepadte' + legValue).val() == "" ? "" : $('#multidepadte' + legValue).datepicker('getDate');
        if (temDeparturedate != "" && this.cityList[legValue - 1].from != "" && this.cityList[legValue - 1].to != "") {
          var departureFrom = this.cityList[legValue - 1].from;
          var arrivalTo = this.cityList[legValue - 1].to;
          var travelDate = moment(temDeparturedate).format('DD|MM|YYYY');
          sectors += '/' + departureFrom + '-' + arrivalTo + '-' + travelDate;
          legDetails.push(departureFrom + '|' + arrivalTo)
        } else {
          this.validationMessage = "Please fill the Trip " + legValue + "   fields !";
          var self = this;
          setTimeout(function () {
            self.validationMessage = '';
          }, 1500);
          return false;
        }
      }
      if ($('#checkbox-list-6').is(':checked')) {
        var directFlight = "DF";
      } else {
        var directFlight = "AF";
      }
      var adult = this.adtcount;
      var child = this.chdcount;
      var infant = this.infcount;
      var cabin = this.cabinclass ? this.cabinclass : 'Y';
      var preferAirline = this.preferAirline;
      var calendarSearch = this.calendarSearch ? '1' : '0';
      getSuppliers(legDetails,
      function(supp) {
        var searchUrl = '/Flights/flight-listing.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-M-F-' + preferAirline + '-' + directFlight + '-' + calendarSearch;
        // searchUrl = searchUrl.toLocaleLowerCase();
        window.location.href = searchUrl;
      });
    },
    setCHDINFTravellers(adt, chd, inf, cabin) {
      this.cabinclass = getAircabinclass(cabin);
      this.adtcount = parseInt(adt);
      this.chdcount = parseInt(chd);
      this.infcount = parseInt(inf);
    },
    Departurefrom: function (AirportCode, AirportName, leg) {
      if (!leg) {
        this.CityFrom = AirportCode;
      } else {
        var index = this.cityList.findIndex(function (element) {
          return element.id === leg;
        })
        if (index !== -1) {
          this.cityList[index].from = AirportCode
        } else {
          this.cityList.push({
            id: leg,
            from: AirportCode,
            to: ''
          });
        }
      }
    },
    Arrivalfrom: function (AirportCode, AirportName, leg) {
      if (!leg) {
        this.CityTo = AirportCode
      } else {
        var index = this.cityList.findIndex(function (element) {
          return element.id === leg;
        })
        if (index !== -1) {
          this.cityList[index].to = AirportCode
        } else {
          this.cityList.push({
            id: leg,
            from: '',
            to: AirportCode
          });
        }
      }
    },
    SetPreferdAirline(code, label) {
      this.preferAirline = code;
    },
    getAircabinclass: function (cabin) {
      getAircabinclass(cabin)
    },
    getAirportNameUsingElasticserch: function (Iatacode, callback) {
      getAirportNameUsingElasticserch(Iatacode, callback)
    },
    AddNewLeg(leg) {
      console.log(leg);
      if (this.legcount < 5) {
        ++this.legcount;
        var legno = Math.max.apply(Math, this.cityList.map(function (o) {
          return o.id;
        }));
        legno = legno + 1;
        this.cityList.push({
          id: legno,
          from: '',
          to: ''
        });
      }
      console.log(this.cityList);
    },
    DeleteLeg(leg) {
      var legs = this.legcount;
      if (legs > 1) {
        --this.legcount;
        var index = this.cityList.findIndex(function (element) {
          return element.id === leg;
        })
        if (index !== -1) {
          this.cityList.splice(index, 1);
        }
      }
    },
    SwapLocation(id, leg) {
      var index = this.cityList.findIndex(function (element) {
        return element.id === leg;
      })
      if (index !== -1) {
        var from = this.cityList[index].from;
        var to = this.cityList[index].to;
        this.cityList[index].from = to;
        this.cityList[index].to = from;
        swpaloc(id);
      }
    },
    swapLocations: function (id) {
      if ((this.CityFrom) && (this.CityTo)) {
        var from = this.CityFrom;
        var to = this.CityTo;
        this.CityFrom = to;
        this.CityTo = from;
        swpaloc(id)
      }
    }
  }
});

/**Sorting Function for AutoCompelte Start**/
function SortInputFirstFlight(input, data) {

  var output = [];
  for (var i = 0; i < data.length; i++) {
      data[i].municipality = isNullorEmptyToBlank(data[i].municipality);
      output.push(data[i]);
  }
  return output;
}
/**Sorting Function for AutoCompelte End**/

function swpaloc(id) {
  var from = $("#fromcity" + id).val();
  var to = $("#tocity" + id).val();
  $("#fromcity" + id).val(to);
  $("#tocity" + id).val(from);
}

function setModifySerach() {
  var urlarray = getUrlVars();
  var urldata = urlarray.flight;
  if (urldata == undefined) { } else {
    urldata = urldata.substring(1, urldata.length);
    var tripLeg = urldata.split('/');
    searcharry = createflightSearchArry(tripLeg);
  }
}

function getUrlVars() {
  var vars = [],
    hash;
  var newUrl = decodeURIComponent(window.location.href);
  var hashes = newUrl.slice(newUrl.indexOf('?') + 1).split('&');
  for (var i = 0; i < hashes.length; i++) {
    hash = hashes[i].split('=');
    vars.push(hash[0]);
    vars[hash[0]] = hash[1];
  }
  return vars;
}

function createflightSearchArry(tripLeg) {
  var originDestinationInformationArr = [];
  //var totalLeg = tripLeg.length;
  var triptypes = tripLeg[tripLeg.length - 1];
  tripTyppee = triptypes.split('-')[7];
  tripTyppee = tripTyppee.toLowerCase();
  
  for (var i = 0; i < tripLeg.length - 1; i++) {
    var Trip = tripLeg[i];
    var TripFrom = Trip.split('-')[0].toUpperCase();
    var TripTo = Trip.split('-')[1].toUpperCase();
    if (i == 0) {
      // setTimeout(function () {
      // }, 3000);
    }
    if (tripTyppee == 'o') {
      getairportfromelasticserch(TripFrom, 1, 1);
      getairportfromelasticserch(TripTo, 2, 1);
    } else if (tripTyppee == 'r') {
      if (i == 0) {
        getairportfromelasticserch(TripFrom, 1, 2);
        getairportfromelasticserch(TripTo, 2, 2);
      }
    } else if (tripTyppee == 'm') {
      getairportfromelasticserch(TripFrom, 1, Number(i + 3));
      getairportfromelasticserch(TripTo, 2, Number(i + 3));
    }

    var TripDate = Trip.split('-')[2];
    var TripArray = {
      departureDate: moment(TripDate, 'DD|MM|YYYY').format("DD-MM-YYYY"),
      originLocation: TripFrom,
      destinationLocation: TripTo,
    }
    originDestinationInformationArr.push(TripArray);
  }

  var totalADT = triptypes.split('-')[0];
  var totalCHD = triptypes.split('-')[1];
  var totalINF = triptypes.split('-')[2];
  var tripClass = triptypes.split('-')[3];
  if (tripClass.toLowerCase() == 'all') {
    tripClass = 'All';
  } else {
    tripClass = tripClass.toLowerCase();
  }

  if (tripTyppee == 'o') {
    // typeType = 'o';
    $("#flights ul.nav-tabs2 li").removeClass('active');
    $("#flights ul.nav-tabs2 li:first").addClass('active');
    $("#deptDate").val(moment(originDestinationInformationArr[0].departureDate, 'DD-MM-YYYY').format('DD/MM/YYYY'));
    $("#deptDate01").val(moment(originDestinationInformationArr[0].departureDate, 'DD-MM-YYYY').format('DD/MM/YYYY'));
    $("#multidepadte").val(moment(originDestinationInformationArr[0].departureDate, 'DD-MM-YYYY').format('DD/MM/YYYY'));
  } else if (tripTyppee == 'r') {
    // typeType = 'r';
    $("#flights ul.nav-tabs2 li").removeClass('active');
    $("#flights ul.nav-tabs2 li:nth-child(2)").addClass('active');
    $("#flights div.tab-content .tab-pane").removeClass('active in');
    $("#round-trip").addClass('active in');
    $("#arrDate01").val(moment(originDestinationInformationArr[1].departureDate, 'DD-MM-YYYY').format('DD/MM/YYYY'));
  } else if (tripTyppee == 'm') {
    // typeType = 'm';
    $("#flights ul.nav-tabs2 li").removeClass('active');
    $("#flights ul.nav-tabs2 li:nth-child(3)").addClass('active');
    $("#flights div.tab-content .tab-pane").removeClass('active in');
    $("#multi-city").addClass('active in');
  }
  var flightSearchRQ = {
    originDestination: originDestinationInformationArr,
    cabin: tripClass,
    triptype: tripTyppee,
    passengerTypeQuantity: {
      adt: parseInt(totalADT),
      chd: parseInt(totalCHD),
      inf: parseInt(totalINF)
    }
  };
  return flightSearchRQ;
}

function getairportfromelasticserch(airportcode, dir, index) {
  var uppercaseLetter = airportcode.toUpperCase();
  var query = {
    query: {
      bool: {
        should: [{
          bool: {
            should: [{
              wildcard: { iata_code: { value: uppercaseLetter, boost: 3.0 } }
            }]
          }
        },],
        must: [{
          "exists": {
            "field": "iata_code"
          }
        }]
      }
    }
  };
  axios.post(ServiceUrls.elasticSearch.url, query, {
    headers: {
      "Content-Type": "application/json"
    }
    }).then(function (resp) {
      finalResult = [];
      var hits = resp.hits.hits;
      var Citymap = new Map();
      for (var i = 0; i < hits.length; i++) {
        Citymap.set(hits[i]._source.iata_code, hits[i]._source);
      }
      var get_values = Citymap.values();
      var Cityvalues = [];
      for (var ele of get_values) {
        Cityvalues.push(ele);
      }
      if (Cityvalues.length > 0) {
        var city = Cityvalues[0].name + ", " + Cityvalues[0].iso_country + '(' + Cityvalues[0].iata_code + ')';
        if (dir == 1) {
          if ($("#fromcity" + index).val() == "") {
            $("#fromcity" + index).val(city);
          }
  
          // $("#fromcity1").val(city);
          // $("#fromcity2").val(city);
          // $("#fromcity3").val(city);
        } else {
          if ($("#tocity" + index).val() == "") {
            $("#tocity" + index).val(city);
          }
          //$("#tocity1").val(city);
          // $("#tocity2").val(city);
          //$("#tocity3").val(city);
        }
      }
    }).catch(function (error) {
        console.log(error)
    });
  // var client = new elasticsearch.Client({
  //   host: [{
  //     host: ServiceUrls.elasticSearch.elasticsearchHost,
  //     auth: ServiceUrls.elasticSearch.auth,
  //     protocol: ServiceUrls.elasticSearch.protocol,
  //     port: ServiceUrls.elasticSearch.port,
  //     requestTimeout: 60000
  //   }],
  //   log: 'trace'
  // });
  // client.search({
  //   index: 'airport_info',
  //   size: 1,
  //   timeout: "3000ms",
  //   body: query                    
  // }).then(function (resp) {
  //   finalResult = [];
  //   var hits = resp.hits.hits;
  //   var Citymap = new Map();
  //   for (var i = 0; i < hits.length; i++) {
  //     Citymap.set(hits[i]._source.iata_code, hits[i]._source);
  //   }
  //   var get_values = Citymap.values();
  //   var Cityvalues = [];
  //   for (var ele of get_values) {
  //     Cityvalues.push(ele);
  //   }
  //   if (Cityvalues.length > 0) {
  //     var city = Cityvalues[0].name + ", " + Cityvalues[0].iso_country + '(' + Cityvalues[0].iata_code + ')';
  //     if (dir == 1) {
  //       if ($("#fromcity" + index).val() == "") {
  //         $("#fromcity" + index).val(city);
  //       }

  //       // $("#fromcity1").val(city);
  //       // $("#fromcity2").val(city);
  //       // $("#fromcity3").val(city);
  //     } else {
  //       if ($("#tocity" + index).val() == "") {
  //         $("#tocity" + index).val(city);
  //       }
  //       //$("#tocity1").val(city);
  //       // $("#tocity2").val(city);
  //       //$("#tocity3").val(city);
  //     }
  //   }
  // })
}

function isNullorEmpty(value) {
  var status = false;
  if (value == null || value == undefined || value == "undefined") { status = true; }
  if (!status && $.trim(value) == '') { status = true; }
  return status;
}

function isNullorEmptyToBlank(value, optval) {
  return isNullorEmpty(value) ? (isNullorEmpty(optval) ? '' : optval) : value;
}