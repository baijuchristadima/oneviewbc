var HeaderComponent = Vue.component('headeritem', {
    template: `     
    <div><header> 
    <div v-if="getdata"></div>  
    <div v-else v-for="item in pluck('Head',content.area_List)"> 
    <div class="topSec">
        <div class="container">
            <div class="col-sm-3">
                <a href="/"><img :src="header.logo_Image" :alt="header.Image_Name"></a>
            </div>
            <div class="col-sm-9 headerContact">
                <div class="h_top">
                <div class="hd_right">
                <div class="sl-nav">
                    <currency-select></currency-select>          
                    <lang-select @languagechange="headerData"></lang-select>
                </div>
                <div class="retrive_booking_sec" data-toggle="modal" data-target="#myModal"><span>{{pluckcom('Retrieve_booking_label',item.component)}}</span></div>
                <ul id="sign-bt-area" class="login-reg  js-signin-modal-trigger"  v-show="!userlogined">
                    <li><a href="javascript:void(0);"  data-signin="login" class="cd-main-nav__item cd-main-nav__item--signin"><i class="fa fa-lock"></i> {{pluckcom('Sign_in_lable',item.component)}}</a></li>
                </ul>
                </div>
                <!--
                <ul>
                <li><i class="fa fa-phone"></i> {{header.Phone_Number}}</li>
                <li><i class="fa fa-envelope"></i> <a :href="'mailto:' + header.Email_Id">{{header.Email_Id}}</a></li>
            </ul>
                -->
            <!--iflogin-->
            <label id="bind-login-info" for="profile2" class="profile-dropdown" v-show="userlogined">
                <input type="checkbox" id="profile2">
                <img src="/assets/images/user.png">
                <span>{{userinfo.firstName+' '+userinfo.lastName }}</span>
                <label for="profile2"><i class="fa fa-angle-down" aria-hidden="true"></i></label>
                <ul>
                    <li><a href="/customer-profile.html"><i class="fa fa-th-large" aria-hidden="true"></i>{{pluckcom('My_dashboard_label',item.component)}}</a></li>
                    <li><a href="/my-profile.html"><i class="fa fa-user" aria-hidden="true"></i>{{pluckcom('My_profile_label',item.component)}}</a></li>
                    <li><a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i>{{pluckcom('My_booking_label',item.component)}}</a></li>
                    <li><a href="#" v-on:click.prevent="logout"><i class="fa fa-power-off" aria-hidden="true"></i>{{pluckcom('Logout_label',item.component)}}</a></li>
                </ul>
            </label>
            <!--ifnotlogin-->
                </div>
            </div>

            <!--view booking popup-->
        <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog  modal-smsp">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">View Bookings</h4>
            </div>
            <div class="modal-body">
            <div class="user_login">
            <div>
            <p><label>Enter your email address</label></p>
            <div class="validation_sec">
            <input v-model="bEmail" type="text" id="txtretrivebooking" name="text" placeholder="name@example.com" class=""> 
            <span v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveEmailErormsg }"  class="cd-signin-modal__error sp_validation">Email Required!</span>
            </div> 
            <p></p> 
            <p><label>Enter your ID number</label></p>
            <div class="validation_sec">
            <input v-model="bRef" type="text" id="text" name="text" placeholder="Example: AGY509-8509" class=""> 
            <span  v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveBkngRefErormsg }"   class="cd-signin-modal__error sp_validation">Booking Reference Id Required!</span>
            </div> 
            <p></p>
            <div class="viewradio_sec">
            <label class="radio_view">Flight
                <input v-model="bService" value="F" type="radio" checked="checked" name="radio">
                <span class="checkmark"></span>
            </label>
            <label class="radio_view">Hotel
                <input v-model="bService" value="H" type="radio" name="radio">
                <span class="checkmark"></span>
            </label>
            </div> 
            <button v-on:click="retrieveBooking" type="submit" id="retrieve-booking" class="btn-blue">Continue</button></div></div>
            </div>            
          </div>
          
        </div>
      </div>
            </div>
            <!--view booking popup close-->

        
        <!--popup area-->
        <div class="cd-signin-modal js-signin-modal"> <!-- this is the entire modal form, including the background -->
		<div class="cd-signin-modal__container"> <!-- this is the container wrapper -->
			<ul class="cd-signin-modal__switcher js-signin-modal-switcher js-signin-modal-trigger">
				<li><a href="#0" data-signin="login" data-type="login">{{pluckcom('Sign_in_lable',item.component)}}</a></li>
				<li><a href="#0" data-signin="signup" data-type="signup">{{pluckcom('register_label',item.component)}}</a></li>
			</ul>

			<div class="cd-signin-modal__block js-signin-modal-block" data-type="login"> <!-- log in form -->
                <div class="cd-signin-modal__form">
                <div class="connect">
                <a href="#"><img src="https://ovit-fileupload.s3.amazonaws.com/B2B/AdminPanel/CMS/AGY435/Images/24042019052933.jpg" alt=""></a>
                <a href="#"><img src="https://ovit-fileupload.s3.amazonaws.com/B2B/AdminPanel/CMS/AGY435/Images/24042019052949.jpg" alt=""></a>
                </div>
					<p class="cd-signin-modal__fieldset">
                        <label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" 
                        for="signin-email">{{pluckcom('signinemail_label',item.component)}}</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signin-email" type="email" :placeholder="pluckcom('signinemail_label',item.component)" v-model="username" >
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': usererrormsg.empty||usererrormsg.invalid }">{{usererrormsg.empty?'Please enter email!':(usererrormsg.invalid?'Email seems incorrect!':'')}}</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <label class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace" 
                        for="signin-password">{{pluckcom('signinpassword_label',item.component)}}</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signin-password" type="password"  :placeholder="pluckcom('signinpassword_label',item.component)"  v-model="password" >
						<a v-on:click="showhidepassword" class="cd-signin-modal__hide-password js-hide-password changeShowTxtCls">{{pluckcom('showpassword_label',item.component)}}</a>
                        <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': psserrormsg }">
                        Please enter password!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width" type="submit"
                        v-on:click="loginaction" :value="pluckcom('loginbutton_label',item.component)">
                    </p>
                    <div id="myGoogleButton"></div>
                    <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false"
        data-use-continue-as="false" onlogin="checkLoginState();"></div>
                    <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="reset">{{pluckcom('forgotpassword_label',item.component)}}</a></p>
				</div>
				
				
			</div> <!-- cd-signin-modal__block -->

			<div class="cd-signin-modal__block js-signin-modal-block" data-type="signup"> <!-- sign up form -->
                <div class="cd-signin-modal__form">
                <div class="connect">
                <a href="#"><img src="https://ovit-fileupload.s3.amazonaws.com/B2B/AdminPanel/CMS/AGY435/Images/24042019052933.jpg" alt=""></a>
                <a href="#"><img src="https://ovit-fileupload.s3.amazonaws.com/B2B/AdminPanel/CMS/AGY435/Images/24042019052949.jpg" alt=""></a>
                </div>
                    <p class="cd-signin-modal__fieldset">
                        <label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">Title</label>                        
                        <select v-model="registerUserData.title" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border down_select" id="signup-title">
                            <option selected>Mr</option>
                            <option selected>Ms</option>
                            <option>Mrs</option>
                        </select>
                        <span class="cd-signin-modal__error">Title seems incorrect!</span>
                    </p>
					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">First Name</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signup-firstname" type="text" :placeholder="pluckcom('signupfirstname_label',item.component)" v-model="registerUserData.firstName">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userFirstNameErormsg }" >Plese enter first name!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">Last Name</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signup-lastname" type="text" :placeholder="pluckcom('signuplastname_label',item.component)" v-model="registerUserData.lastName">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userLastNameErrormsg }">Plese enter last name!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" for="signup-email">Email</label>
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                        id="signup-email" type="email" :placeholder="pluckcom('signupemail_label',item.component)" v-model="registerUserData.emailId">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userEmailErormsg.empty||userEmailErormsg.invalid }">{{userEmailErormsg.empty?'Please enter email!':(userEmailErormsg.invalid?'Email seems incorrect!':'')}}</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding" type="submit" 
                        :value="pluckcom('createaccountbutton_label',item.component)" v-on:click="registerUser">
                    </p>
                    <div id="myGoogleButtonReg"></div>
                    <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false"
        data-use-continue-as="false" onlogin="checkLoginState();"></div>                    
				</div>
			</div> <!-- cd-signin-modal__block -->

			<div class="cd-signin-modal__block js-signin-modal-block" data-type="reset"> <!-- reset password form -->
				<p class="cd-signin-modal__message">{{pluckcom('paswordlostmessage_label',item.component)}}</p>

				<div class="cd-signin-modal__form">
					<p class="cd-signin-modal__fieldset">
						<label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" for="reset-email">E-mail</label>
						<input v-model="emailId" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" id="reset-email" type="email" :placeholder="pluckcom('forgotpassemail_label',item.component)">
						<span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userforgotErrormsg.empty||userforgotErrormsg.invalid }">{{userforgotErrormsg.empty?'Please enter email!':(userforgotErrormsg.invalid?'Email seems incorrect!':'')}}</span>
					</p>

					<p class="cd-signin-modal__fieldset">
                        <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding" type="submit" 
                        :value="pluckcom('resetpassbutton_label',item.component)" v-on:click="forgotPassword">
                    </p>
                    <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="login">{{pluckcom('backtologinlink_label',item.component)}}</a></p>
				</div>

				
			</div> <!-- cd-signin-modal__block -->
			<a href="#0" class="cd-signin-modal__close js-close">Close</a>
		</div> <!-- cd-signin-modal__container -->
	</div> 
        <!--popup area close-->
    </div>
    <div class="navSec">
        <div class="container">
            <nav class="navbar navbar-inverse">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img :src="header.logo_Image" :alt="header.Image_Name"></a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-center">
                        <li id="homeli"><a href="/Biscordint">{{menus.Home}}</a></li>
                        <li><a href="/Biscordint/about-us.html">{{menus.About_Us}}</a></li>
                        <li id="holidyli"><a href="/Biscordint/holiday-packages.html" >{{menus.Packages}}</a></li>
                        <li  id="dealli"><a href="/Biscordint/flight-deals.html">{{menus.Flight_Deals}}</a></li>
                        <li><a href="/Biscordint/services.html">{{menus.Services}}</a></li>
                        <li><a href="/Biscordint/testimonials.html">{{menus.Testimonials}}</a></li>
                        <li><a href="/Biscordint/news-and-events.html">{{menus.News_Events}}</a></li>
                        <li><a href="/Biscordint/coming-soon.html">{{menus.Travel_Grants}}</a></li>
                        <li><a href="/Biscordint/contact-us.html">{{menus.Contact_Us}}</a></li>
                    </ul>

                </div>

            </nav>
        </div>
    </div>
    </div>
</header></div>`,
    data() {
        return {
            // content: null,
            header: {
                logo_Image: '',
                Image_Name: '',
                Phone_Number: '',
                Email_Id: '',
            },
            menus: {
                Home: '',
                About_Us: '',
                Packages: '',
                Flight_Deals: '',
                Services: '',
                Testimonials: '',
                News_Events: '',
                Contact_Us: '',
                Travel_Grants:''

            },
            username: '',
            password: '',
            emailId: '',
            retrieveEmailId: '',
            retrieveBookRefid: '',
            usererrormsg: { empty: false, invalid: false },
            psserrormsg: false,
            userFirstNameErormsg: false,
            userLastNameErrormsg: false,
            userEmailErormsg: { empty: false, invalid: false },
            userPasswordErormsg: false,
            userVerPasswordErormsg: false,
            userPwdMisMatcherrormsg: false,
            userTerms: false,
            userforgotErrormsg: { empty: false, invalid: false },
            retrieveBkngRefErormsg: false,
            retrieveEmailErormsg: false,
            userlogined: this.checklogin(),
            userinfo: [],
            registerUserData: {
                firstName: '',
                lastName: '',
                emailId: '',
                password: '',
                verifyPassword: '',
                terms: '',
                title: 'Mr'
            },
            Languages: [],
            language: 'en',
            content: null,
            getdata: true,
            active_el: (sessionStorage.active_el) ? sessionStorage.active_el : 1,
            bEmail: '',
            bRef: '',
            bService: 'F',
            show: 'show',
            hide: 'hide',
        }
    },
    methods: {
        headerData: function() {
            var self = this;

            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Header/Header/Header.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    self.content = response.data;
                    if (response.data.area_List.length) {
                        var headerComponent = self.pluck('Head', self.content.area_List);
                        self.header.logo_Image = self.pluckcom('logo_Image', headerComponent[0].component);
                        self.header.Image_Name = self.pluckcom('Image_Name', headerComponent[0].component);
                        self.header.Phone_Number = self.pluckcom('Phone_Number', headerComponent[0].component);
                        self.header.Email_Id = self.pluckcom('Email_Id', headerComponent[0].component);

                        var headerComponentM = self.pluck('Menu', self.content.area_List);
                        self.menus.Home = self.pluckcom('Home', headerComponentM[0].component);
                        self.menus.About_Us = self.pluckcom('About_Us', headerComponentM[0].component);
                        self.menus.Packages = self.pluckcom('Packages', headerComponentM[0].component);
                        self.menus.Flight_Deals = self.pluckcom('Flight_Deals', headerComponentM[0].component);
                        self.menus.Services = self.pluckcom('Services', headerComponentM[0].component);
                        self.menus.Testimonials = self.pluckcom('Testimonials', headerComponentM[0].component);
                        self.menus.News_Events = self.pluckcom('News__Events', headerComponentM[0].component);
                        self.menus.Contact_Us = self.pluckcom('Contact_Us', headerComponentM[0].component);
                        self.menus.Travel_Grants = self.pluckcom('Travel_Grants', headerComponentM[0].component);
                        
                        setTimeout(function() { bindactivemenu() }, 10);
                        self.getdata = false;
                        Vue.nextTick(function() {
                            (

                                function() {
                                    self.active_el = (sessionStorage.active_el) ? sessionStorage.active_el : 1;
                                    //Login/Signup modal window - by CodyHouse.co
                                    function ModalSignin(element) {
                                        this.element = element;
                                        this.blocks = this.element.getElementsByClassName('js-signin-modal-block');
                                        this.switchers = this.element.getElementsByClassName('js-signin-modal-switcher')[0].getElementsByTagName('a');
                                        this.triggers = document.getElementsByClassName('js-signin-modal-trigger');
                                        this.hidePassword = this.element.getElementsByClassName('js-hide-password');
                                        this.init();
                                    };

                                    ModalSignin.prototype.init = function() {
                                        var self1 = this;
                                        //open modal/switch form
                                        for (var i = 0; i < this.triggers.length; i++) {
                                            (function(i) {
                                                self1.triggers[i].addEventListener('click', function(event) {
                                                    if (event.target.hasAttribute('data-signin')) {
                                                        event.preventDefault();
                                                        self1.showSigninForm(event.target.getAttribute('data-signin'));
                                                    }
                                                });
                                            })(i);
                                        }

                                        //close modal
                                        this.element.addEventListener('click', function(event) {
                                            if (hasClass(event.target, 'js-signin-modal') || hasClass(event.target, 'js-close')) {
                                                event.preventDefault();
                                                removeClass(self1.element, 'cd-signin-modal--is-visible');
                                            }
                                        });
                                        //close modal when clicking the esc keyboard button
                                        document.addEventListener('keydown', function(event) {
                                            (event.which == '27') && removeClass(self1.element, 'cd-signin-modal--is-visible');
                                        });

                                        // //hide/show password
                                        // for (var i = 0; i < this.hidePassword.length; i++) {
                                        //     (function(i) {
                                        //         self1.hidePassword[i].addEventListener('click', function(event) {
                                        //             self1.togglePassword(self1.hidePassword[i]);
                                        //         });
                                        //     })(i);
                                        // }

                                        //IMPORTANT - REMOVE THIS - it's just to show/hide error messages in the demo

                                    };

                                    // ModalSignin.prototype.togglePassword = function(target) {
                                    //     var password = target.previousElementSibling;
                                    //     ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text'): password.setAttribute('type', 'password');
                                    //     target.textContent = ('Hide' == target.textContent) ? 'Show' : 'Hide';
                                    //     putCursorAtEnd(password);
                                    // }

                                    ModalSignin.prototype.showSigninForm = function(type) {
                                        // show modal if not visible
                                        !hasClass(this.element, 'cd-signin-modal--is-visible') && addClass(this.element, 'cd-signin-modal--is-visible');
                                        // show selected form
                                        for (var i = 0; i < this.blocks.length; i++) {
                                            this.blocks[i].getAttribute('data-type') == type ? addClass(this.blocks[i], 'cd-signin-modal__block--is-selected') : removeClass(this.blocks[i], 'cd-signin-modal__block--is-selected');
                                        }
                                        //update switcher appearance
                                        var switcherType = (type == 'signup') ? 'signup' : 'login';
                                        for (var i = 0; i < this.switchers.length; i++) {
                                            this.switchers[i].getAttribute('data-type') == switcherType ? addClass(this.switchers[i], 'cd-selected') : removeClass(this.switchers[i], 'cd-selected');
                                        }
                                    };

                                    ModalSignin.prototype.toggleError = function(input, bool) {
                                        // used to show error messages in the form
                                        toggleClass(input, 'cd-signin-modal__input--has-error', bool);
                                        toggleClass(input.nextElementSibling, 'cd-signin-modal__error--is-visible', bool);
                                    }

                                    var signinModal = document.getElementsByClassName("js-signin-modal")[0];
                                    if (signinModal) {
                                        new ModalSignin(signinModal);
                                    }

                                    // toggle main navigation on mobile
                                    var mainNav = document.getElementsByClassName('js-main-nav')[0];
                                    if (mainNav) {
                                        mainNav.addEventListener('click', function(event) {
                                            if (hasClass(event.target, 'js-main-nav')) {
                                                var navList = mainNav.getElementsByTagName('ul')[0];
                                                toggleClass(navList, 'cd-main-nav__list--is-visible', !hasClass(navList, 'cd-main-nav__list--is-visible'));
                                            }
                                        });
                                    }

                                    //class manipulations - needed if classList is not supported
                                    function hasClass(el, className) {
                                        if (el.classList) return el.classList.contains(className);
                                        else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
                                    }

                                    function addClass(el, className) {
                                        var classList = className.split(' ');
                                        if (el.classList) el.classList.add(classList[0]);
                                        else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
                                        if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
                                    }

                                    function removeClass(el, className) {
                                        var classList = className.split(' ');
                                        if (el.classList) el.classList.remove(classList[0]);
                                        else if (hasClass(el, classList[0])) {
                                            var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
                                            el.className = el.className.replace(reg, ' ');
                                        }
                                        if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
                                    }

                                    function toggleClass(el, className, bool) {
                                        if (bool) addClass(el, className);
                                        else removeClass(el, className);
                                    }
                                    // $("#modal_retrieve").leanModal({
                                    //     top: 100,
                                    //     overlay: 0.6,
                                    //     closeButton: ".modal_close"
                                    // });
                                    //credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
                                    function putCursorAtEnd(el) {
                                        if (el.setSelectionRange) {
                                            var len = el.value.length * 2;
                                            el.focus();
                                            el.setSelectionRange(len, len);
                                        } else {
                                            el.value = el.value;
                                        }
                                    };
                                })();
                        }.bind(self));
                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.menus = [];
                    self.header = [];

                });
            });
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        loginaction: function() {

            if (!this.username.trim()) {
                this.usererrormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.username.trim())) {
                this.usererrormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.usererrormsg = { empty: false, invalid: false };
            }
            if (!this.password) {
                this.psserrormsg = true;
                return false;

            } else {
                this.psserrormsg = false;
                var self = this;
                login(this.username, this.password, function(response) {
                    if (response == false) {
                        self.userlogined = false;
                        alert("Invalid username or password.");
                    } else {
                        self.userlogined = true;
                        self.userinfo = JSON.parse(localStorage.User);
                        $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                        try {
                            self.$eventHub.$emit('logged-in', { userName: self.username, password: self.password });
                            signArea.headerLogin({ userName: self.username, password: self.password })
                        } catch (error) {

                        }
                    }
                });

            }



        },
        validEmail: function(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        checklogin: function() {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);
                return true;
            } else {
                this.userlogined = false;
                return false;
            }
        },
        logout: function() {
            this.userlogined = false;
            this.userinfo = [];
            localStorage.profileUpdated = false;
            try {
                this.$eventHub.$emit('logged-out');
                signArea.logout()
            } catch (error) {

            }
            commonlogin();
            // signOut();
            // signOutFb();
        },
        registerUser: function() {
            if (this.registerUserData.firstName.trim() == "") {
                this.userFirstNameErormsg = true;
                return false;
            } else {
                this.userFirstNameErormsg = false;
            }
            if (this.registerUserData.lastName.trim() == "") {
                this.userLastNameErrormsg = true;
                return false;
            } else {
                this.userLastNameErrormsg = false;
            }
            if (this.registerUserData.emailId.trim() == "") {
                this.userEmailErormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.registerUserData.emailId.trim())) {
                this.userEmailErormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.userEmailErormsg = { empty: false, invalid: false };
            }
            var vm = this;
            registerUser(this.registerUserData.emailId, this.registerUserData.firstName, this.registerUserData.lastName, this.registerUserData.title, function(response) {
                if (response.isSuccess == true) {
                    vm.username = response.data.data.user.loginId;
                    vm.password = response.data.data.user.password;
                    login(vm.username, vm.password, function (response) {
                        if (response != false) {
                            $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                            window.location.href = "/edit-my-profile.html?edit-profile=true";
                        }
                    });
                }

            });
        },

        forgotPassword: function() {
            if (this.emailId.trim() == "") {
                this.userforgotErrormsg = { empty: true, invalid: false };
                return false;
            } else if (!this.validEmail(this.emailId.trim())) {
                this.userforgotErrormsg = { empty: false, invalid: true };
                return false;
            } else {
                this.userforgotErrormsg = { empty: false, invalid: false };
            }

            var datas = {
                emailId: this.emailId,
                agencyCode: localStorage.AgencyCode,
                logoUrl: window.location.origin + "/" + localStorage.AgencyFolderName + "/website-informations/logo/logo.png",
                websiteUrl: window.location.origin,
                resetUri: window.location.origin + "/" + localStorage.AgencyFolderName + "/reset-password.html"
            };
            $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:none;background:grey !important;");

            var huburl = ServiceUrls.hubConnection.baseUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var requrl = ServiceUrls.hubConnection.hubServices.forgotPasswordUrl;
            axios.post(huburl + portno + requrl, datas)
                .then((response) => {
                    if (response.data != "") {
                        alert(response.data);
                    } else {
                        alert("Error in forgot password. Please contact admin.");
                    }
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:var(--primary-color) !important");

                })
                .catch((err) => {
                    console.log("FORGOT PASSWORD  ERROR: ", err);
                    if (err.response.data.message == 'No User is registered with this emailId.') {
                        alert(err.response.data.message);
                    } else {
                        alert('We have found some technical difficulties. Please contact admin!');
                    }
                    $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:var(--primary-color) !important");

                })

        },

        retrieveBooking: function() {
            if (this.bEmail == "") {
                //alert('Email required !');
                this.retrieveEmailErormsg = true;
                return false;

            } else if (!this.validEmail(this.bEmail)) {
                //alert('Invalid Email !');
                this.retrieveEmailErormsg = true;
                return false;

            } else {
                this.retrieveEmailErormsg = false;
            }
            if (this.bRef == "") {
                this.retrieveBkngRefErormsg = true;
                return false;

            } else {
                this.retrieveBkngRefErormsg = false;
            }
            if (!this.retrieveBkngRefErormsg && !this.retrieveEmailErormsg && !this.retrieveEmailErormsg) {
                switch (this.bService) {
                    case 'F':
                        this.retBookFlight();
                        break;
                    case 'H':
                        var vm = this;
                        var hubUrl = ServiceUrls.hubConnection.baseUrl + ServiceUrls.hubConnection.ipAddress;

                        axios({
                            method: "get",
                            url: hubUrl + "/hotelBook/bookingbyref/" + vm.bRef + ":" + vm.bEmail,
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer ' + localStorage.access_token
                            }
                        }).then(response => {
                            window.sessionStorage.setItem('userAction', vm.bRef);
                            window.location.href = "/Hotels/hotel-detail.html#/hotelConfirmation";

                        }).catch(error => {
                            alertify.alert('Error!', 'Booking details not found!');
                        });
                        break;
                    default:
                        break;
                }
            }

        },
        retBookFlight: function() {
            if (this.bEmail == "") {
                alert('Email required !');
                return false;
            } else if (!this.validEmail(this.bEmail)) {
                alert('Invalid Email !');
                return false;
            } else {
                this.retrieveEmailErormsg = false;
            }
            if (this.bRef == "") {
                this.retrieveBkngRefErormsg = true;
                return false;
            } else {
                this.retrieveBkngRefErormsg = false;
            }
            var bookData = {
                BkngRefID: this.bRef,
                emailId: this.bEmail,
                redirectFrom: 'retrievebooking',
                isMailsend: false
            };
            localStorage.bookData = JSON.stringify(bookData);
            window.location.href = '/Flights/flight-confirmation.html';
        },
        showhidepassword: function(event) {
            var password = event.target.previousElementSibling;
            ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text'): password.setAttribute('type', 'password');
            event.target.textContent = (this.hide == event.target.textContent) ? this.show : this.hide;
        },
    },
    mounted: function() {
        if (localStorage.IsLogin == "true") {
            this.userlogined = true;
            this.userinfo = JSON.parse(localStorage.User);

        } else {
            this.userlogined = false;

        }
        this.headerData();
    },
    watch: {
        updatelogin: function() {
            if (localStorage.IsLogin == "true") {
                this.userlogined = true;
                this.userinfo = JSON.parse(localStorage.User);

            } else {
                this.userlogined = false;

            }

        },
        bRef: function() {
            this.retrieveBkngRefErormsg = false;
        }

    }
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('partners', {
    template: `    
    <section class="partners">
    <ul id="partners">
        <li class="item" v-for="items in logo">
            <div class="partnersLogos">
                <img :src="items.Partner_logo">
            </div>
        </li>
    </ul>
</section>
    `,
    data() {
        return {
            // content: null,
            logo: null,
        }
    },
    methods: {
        partnersLogo: function() {
            var self = this;

            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Footer/Footer/Footer.ftl';

                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    self.content = response.data;
                    if (response.data.area_List.length) {
                        var partnerComponent = self.pluck('Partners', self.content.area_List);
                        var logoArray = self.pluckcom('logo', partnerComponent[0].component);
                        self.logo = logoArray;
                        setTimeout(function() { partners() }, 1000);
                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.logo = [];
                });
            });
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },

    },
    mounted: function() {
        this.partnersLogo();

    },

})

Vue.component('footeritem', {
    template: `  
    <footer class="bgimg-1">
        <div class="container">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="addressSec"><img :src="footer.Logo" alt="footerlogo">
                    <ul>
                        <li class="flx_sec">
                            <div class="icon"><i class="fa fa-phone"></i></div>
                            <div class="iconDetails">+2348033562295 </div>
                        </li>
                        <li class="flx_sec">
                            <div class="icon"><i class="fa fa-envelope"></i></div>
                            <div class="iconDetails"><a :href="'mailto:'+footer.reservation_Email">{{footer.reservation_Email}}</a></div>
                        </li>
                        <li class="flx_sec">
                            <div class="icon"><i class="fa fa-map-marker"></i></div>
                            <div class="iconDetails">{{footer.Address}}</div>
                        </li>
                    </ul>
                    <div class="socialIcons"><a href="https://www.facebook.com/Biscordint-Travels-Tours-782512125132579/"><i class="fa fa-facebook"></i></a> <a href="https://twitter.com/biscordint"><i class="fa fa-twitter"></i></a> <a href="https://www.linkedin.com/in/biscordint-travel-agency-09196668/"><i class="fa fa-linkedin"></i></a> <a href="https://www.instagram.com/biscordinttravels/"><i class="fa fa-instagram"></i></a> <img src="/Biscordint/images/iata.png" alt="iata"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="sitemap">
                    <div class="siteMap_sec">
                        <h4>{{footer.Site_map}}</h4>
                        <div class="flx_sec">
                            <ul>
                                <li class="active"><a href="index.html">{{menus.Home}}</a></li>
                                <li><a href="about-us.html">{{menus.About_Us}}</a></li>
                                <li><a href="services.html">{{menus.Services}}</a></li>
                            </ul>
                            <ul>
                                <li><a href="holiday-packages.html">{{menus.Packages}}</a></li>
                                <li><a href="flight-deals.html">{{menus.Flight_Deals}}</a></li>
                                <li><a href="contact-us.html">{{menus.Contact_Us}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="newsletter">
                    <h4>{{Newsletter.Title}}</h4>
                    <p>{{Newsletter.short_Description}}</p> <input type="text" :placeholder="Newsletter.Email_placeholder" class="newstxt" v-model="newsltremail"> <button v-on:click="sendnewsletter">{{Newsletter.Button_label}}</button>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container flx_sec">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <p> © {{footer.Copyright_Notice}}</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
                    <p>
                        Powered by:
                        <a href="http://www.oneviewit.com/" target="_blank"><img src="/Biscordint/images/oneview.png" alt="oneview"></a></p>
                </div>
            </div>
        </div>
    </footer>`,
    data() {
        return {
            // content: null,
            // contentH:null,
            footer: {
                Logo: '',
                Phone_number: '',
                name: '',
                Address: '',
                Site_map: '',
                reservation_Email: '',
                Copyright_Notice: '',
            },
            menus: {
                Home: '',
                About_Us: '',
                Packages: '',
                Flight_Deals: '',
                Services: '',
                Testimonials: '',
                News_Events: '',
                Contact_Us: '',

            },
            Newsletter: {
                Title: '',
                short_Description: '',
                Email_placeholder: '',
                Button_label: '',
            },
            newsltremail: null,

        }
    },
    methods: {
        footerData: function() {
            var self = this;

            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Footer/Footer/Footer.ftl';


                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    self.content = response.data;
                    if (response.data.area_List.length) {
                        var footerComponent = self.pluck('Footer', self.content.area_List);
                        self.footer.Logo = self.pluckcom('Logo', footerComponent[0].component);
                        self.footer.Phone_number = self.pluckcom('Phone_number', footerComponent[0].component);
                        self.footer.name = self.pluckcom('name', footerComponent[0].component);
                        self.footer.Address = self.pluckcom('Address', footerComponent[0].component);
                        self.footer.reservation_Email = self.pluckcom('reservation_Email', footerComponent[0].component);
                        self.footer.Address = self.pluckcom('Address', footerComponent[0].component);
                        self.footer.Copyright_Notice = self.pluckcom('Copyright_Notice', footerComponent[0].component);
                        var footerComponentS = self.pluck('Site_map', self.content.area_List);
                        self.footer.Site_map = self.pluckcom('Title', footerComponentS[0].component);

                        var footerComponentN = self.pluck('Newsletter', self.content.area_List);
                        self.Newsletter.Title = self.pluckcom('Title', footerComponentN[0].component);
                        self.Newsletter.short_Description = self.pluckcom('short_Description', footerComponentN[0].component);
                        self.Newsletter.Email_placeholder = self.pluckcom('Email_placeholder', footerComponentN[0].component);
                        self.Newsletter.Button_label = self.pluckcom('Button_label', footerComponentN[0].component);


                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.footer = [];
                });
            });

            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Header/Header/Header.ftl';

                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    self.contentH = response.data;
                    if (response.data.area_List.length) {
                        var headerComponentM = self.pluck('Menu', self.contentH.area_List);
                        self.menus.Home = self.pluckcom('Home', headerComponentM[0].component);
                        self.menus.About_Us = self.pluckcom('About_Us', headerComponentM[0].component);
                        self.menus.Packages = self.pluckcom('Packages', headerComponentM[0].component);
                        self.menus.Flight_Deals = self.pluckcom('Flight_Deals', headerComponentM[0].component);
                        self.menus.Services = self.pluckcom('Services', headerComponentM[0].component);
                        self.menus.Testimonials = self.pluckcom('Testimonials', headerComponentM[0].component);
                        self.menus.News_Events = self.pluckcom('News__Events', headerComponentM[0].component);
                        self.menus.Contact_Us = self.pluckcom('Contact_Us', headerComponentM[0].component);

                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.menus = [];
                });
            });


        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },


        // Form Post


        sendnewsletter: async function () {

            if (!this.newsltremail) {
                alertify.alert('Alert', 'Email Id required.');
                return false;;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.');
                return false;
            } else {
                
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                var filterValue = "type='Newsletter' AND keyword1='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                    return false;
                } else {
                    var fromEmail = JSON.parse(localStorage.User).loginNode.email;
                    var custmail = {
                        type: "UserAddedRequest",
                        fromEmail: fromEmail,
                        toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                        logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                        agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                        agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                        personName: this.newsltremail.split("@")[0],
                        primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                        secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                    };



                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = { type: "Newsletter",date1: requestedDate, keyword1: this.newsltremail, keyword2:"Subscribe Newsletter",nodeCode: agencyCode };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        var emailApi = ServiceUrls.emailServices.emailApi;
                        sendMailService(emailApi, custmail);
                        alertify.alert('Newsletter', 'Thank you for subscribing !');
                    } catch (e) {

                    }
                }

            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
          
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }           
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
               
                const myJson = await response.json();
                return myJson;
            } catch (error) {
               
                return object;
            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {
           
            var allDBData = [];
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        }




    },
    mounted: function() {
        this.footerData();
    },

})

function partners() {
    $("#partners").owlCarousel({
        items: 6,
        itemsCustom: false,
        itemsDesktop: [1199, 6],
        itemsDesktopSmall: [991, 4],
        itemsTablet: [768, 4],
        itemsTabletSmall: [600, 2],
        itemsMobile: [479, 2],
        singleItem: false,
        itemsScaleUp: false,

        //Autoplay
        autoPlay: true,
        stopOnHover: true,

        // Navigation
        navigation: false,
        navigationText: ['<img src="images/left-arrow.png">', '<img src="images/right-arrow.png">'],
        rewindNav: true,
        scrollPerPage: false,

        //Pagination
        pagination: false,
        paginationNumbers: false,

        // Responsive 
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
    });
}
var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});
function bindactivemenu() {
    var pgurl = window.location.pathname;
    pgurl = pgurl.split("/")[2];
    $("#myNavbar ul li a").each(function() {
        var atag = $(this).attr("href");
        atag=  atag.split("/Biscordint/")[1];
        if (pgurl == atag) {
            $(this).parent().addClass("active");
        } else if (pgurl == "flight-deals-details.html") {
            $("#dealli").addClass("active");
        } else if (pgurl == "holiday-packagesview.html") {
            $("#holidyli").addClass("active");
        } else if (pgurl == "") {
            $("#homeli").addClass("active");

        }
    });
}

function searchArray(nameKey, myArray, tagName) {
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i][tagName] === nameKey) {
            return myArray[i];
        }
    }
}