 generalInformation = {
    systemSettings: {       
        calendarDisplay: 1,
       
    },
};
var images = [];
var currimg = 0;
const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var flightserchfromComponent = Vue.component('flightserch', {
    data() {
        return {
            access_token: '',
            KeywordSearch: '',
            resultItemsarr: [],
            autoCompleteProgress: false,
            highlightIndex: 0
        }
    },
    props: {
        itemText: String,
        itemId: String,
        placeHolderText: String,
        returnValue: Boolean,
        id: { type: String, default: '', required: false },
        leg: Number,
        //KeywordSearch:String
    },

    template: `<div class="autocomplete">
        <input type="text" :placeholder="placeHolderText" :id="id" :data-aircode="KeywordSearch" autocomplete="off" 
        v-model="KeywordSearch" class="formtxt" 
        :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !autoCompleteProgress  }" 
        @input="onSelectedAutoCompleteEvent(KeywordSearch)"
            @keydown.down="down"
            @keydown.up="up"           
            @keydown.esc="autoCompleteProgress=false"
            @keydown.enter="onSelected(resultItemsarr[highlightIndex])"
            @keydown.tab="tabclick(resultItemsarr[highlightIndex])"/>
        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>        
        <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length>0">
            <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item)">
                {{ item.label }}
            </li>
        </ul>
    </div>`,


    methods: {
        up: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex < this.resultItemsarr.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
            if (this.$refs.options[this.highlightIndex]) {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            }

        },
        onSelectedAutoCompleteEvent: _.debounce(async function (keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
               
              this.autoCompleteProgress = true;
              self.resultItemsarr = await getFlightResultUsingElasticSearch(keywordEntered);

            } else {
                this.autoCompleteProgress = false;
                this.resultItemsarr = [];

            }
        }, 100),
        onSelected: function (item) {
            this.KeywordSearch = item.label;
            this.resultItemsarr = [];
            var targetWhenClicked = $(event.target).parent().parent().find("input").attr("id");
            if (maininstance.triptype != 'M') {
                if (event.target.id == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
                    $('#Cityto1').focus();
                } else if (event.target.id == "Cityto1" || targetWhenClicked == "Cityto1") {
                    $('#deptDate01').focus();
                }
            } else {
                var eventTarget = event.target.id;
                $(document).ready(function () {
                    if (eventTarget == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
                        $('#Cityto1').focus();
                    } else if (eventTarget == "Cityto1" || targetWhenClicked == "Cityto1") {
                        $('#deptDate01').focus();
                    } else if (eventTarget == "DeparturefromLeg1" || targetWhenClicked == "DeparturefromLeg1") {
                        $('#ArrivalfromLeg1').focus();
                    } else if (eventTarget == "ArrivalfromLeg1" || targetWhenClicked == "ArrivalfromLeg1") {
                        $('#txtLeg1Date').focus();
                    } else if (eventTarget == "DeparturefromLeg2" || targetWhenClicked == "DeparturefromLeg2") {
                        $('#ArrivalfromLeg2').focus();
                    } else if (eventTarget == "ArrivalfromLeg2" || targetWhenClicked == "ArrivalfromLeg2") {
                        $('#txtLeg2Date').focus();
                    } else if (eventTarget == "DeparturefromLeg3" || targetWhenClicked == "DeparturefromLeg3") {
                        $('#ArrivalfromLeg3').focus();
                    } else if (eventTarget == "ArrivalfromLeg3" || targetWhenClicked == "ArrivalfromLeg3") {
                        $('#txtLeg3Date').focus();
                    } else if (eventTarget == "DeparturefromLeg4" || targetWhenClicked == "DeparturefromLeg4") {
                        $('#ArrivalfromLeg4').focus();
                    } else if (eventTarget == "ArrivalfromLeg4" || targetWhenClicked == "ArrivalfromLeg4") {
                        $('#txtLeg4Date').focus();
                    } else if (eventTarget == "DeparturefromLeg5" || targetWhenClicked == "DeparturefromLeg5") {
                        $('#ArrivalfromLeg5').focus();
                    } else if (eventTarget == "ArrivalfromLeg5" || targetWhenClicked == "ArrivalfromLeg5") {
                        $('#txtLeg5Date').focus();
                    } else if (eventTarget == "DeparturefromLeg6" || targetWhenClicked == "DeparturefromLeg6") {
                        $('#ArrivalfromLeg6').focus();
                    } else if (eventTarget == "ArrivalfromLeg6" || targetWhenClicked == "ArrivalfromLeg6") {
                        $('#txtLeg6Date').focus();
                    }
                });

            }

            this.$emit('air-search-completed', item.code, item.label, this.leg);


        },
        tabclick: function (item) {
            if (!item) {

            } else {
                this.onSelected(item);
            }
        }

    },
    watch: {
        returnValue: function () {
            this.KeywordSearch = this.itemText;
        }

    }

});
var maininstance = new Vue({
    i18n,
    el: '#indexmain',
    name: 'indexmain',
    data: {
        titleDetails: { Title: '' },
        banner: null,
        // content: null,

        formSearch: {
            flightTab: '',
            hotelTab: '',
            carTab: '',
            insuranceTab: '',
            oneWay: '',
            roundTrip: '',
            multiCity: '',
            buttonLabel: '',
            flightDeparturePlaceholder: '',
            flightArrivalPlaceholder: '',
            flightDatePlaceholderD: '',
            flightDatePlaceholderD: '',
            economy: '',
            business: '',
            first: '',
            adults: '',
            children: '',
            infants: '',
            hotelCityPlaceholder: '',
            hotelArrivalPlaceholder: '',
            hotelDatePlaceA: '',
            hotelDatePlaceD: '',
            occupancy: '',
            roomLabelSi: '',
            roomLabelDo: '',
            carStartingLocation: '',
            carDropoffLocation: '',
            carPickupDateP: '',
            carDropDateP: '',
            carPickupTimeP: '',
            carPickupTimeL: '',
            carDropTimeL: ''
        },
        dealsArea: { title1: '', title2: '' },
        packageArea: { title1: '', title2: '' },
        // contentFlight:null,
        flightDeals: null,
        holidayPackages: null,
        insuranceForm: {
            Name_of_Insured_label: '',
            Date_of_birth_label: '',
            Passport_number_label: '',
            gender: '',
            gender_male_label: '',
            gender_female_label: '',
            Telephone_number_label: '',
            Email_address_label: '',
            Destination_Label: '',
            Form_Title: '',
            Period_of_insurance__Label: '',
            Departure_date_Label: '',
            Return_date_Label: '',
            Trip_Purpose__label: '',
            Address_Home_label: '',
            Address_Office_Label: '',
            DECLARATION_Label: '',
            Declaration_content: '',
            Accept_Declaration_statement: '',
            Date_Label: '',
            Occupation_label: '',
            Button_Submit: '',
        },
        Visacountry: [],
        triptype: "O",
        CityFrom: '',
        CityTo: '',
        validationMessage: '',
        adtcount: 1,
        chdcount: 0,
        infcount: 0,
        preferAirline: '',
        returnValue: true,
        legcount: 2,
        legs: [],
        cityList: [],
        adt: "adult",

        cabinclass: [{ 'value': 'Y', 'text': 'Economy' },
        { 'value': 'C', 'text': 'Business' },
        { 'value': 'F', 'text': 'First' }
        ],
        selected_cabin: 'Y',
        selected_adults: 1,
        selected_children: 0,
        selected_infant: 0,
        totalAllowdPax: 9,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        arabic_dropdown: '',
        childLabel: "",
        childrenLabel: "",
        adultLabel: "",
        adultsLabel: "Adults",
        ageLabel: "year",
        agesLabel: "years",
        infantLabel: "Infant",
        searchBtnLabel: "Search",
        addUptoLabel: "add upto",
        tripsLabel: "trips",
        tripLabel: "trip",
        Totaltravaller: '1 Travellers, Economy',
        travellerdisply: false,
        travellerdisplymul: false,
        child: 0,
        flightSearchCityName: { cityFrom1: '', cityTo1: '', cityFrom2: '', cityTo2: '', cityFrom3: '', cityTo3: '', cityFrom4: '', cityTo4: '', cityFrom5: '', cityTo5: '', cityFrom6: '', cityTo6: '' },
        advncedsearch: false,
        isLoading: false,
        direct_flight: false,
        airlineList: AirlinesDatas,
        Airlineresults: [],
        selectedAirline: [],
        adultrange: '',
        childrange: '',
        infantrange: '',
        donelabel: 'Done',
        classlabel: 'class',
        hotelInit: Math.random(), //hotel init 
        iName: null,
        iDob: null,
        iPassportNo: null,
        iGender: null,
        iPhoneno: null,
        iEmail: null,
        iOccupation: null,
        iDestination: null,
        iPeriod: null,
        iDepartureDate: null,
        iReturnDate: null,
        iReturnDate: null,
        iAddress: null,
        iAddressOffice: null,
        iPurpose: null,
        insterms: true,
        cartimedisplay: false,
        //visa section
        visatype: 'R',
        visaResponse: [],
        isSameCountry: false,
        showLoader: false,
        eVisaCountryList: [],
        popupcontent: {},
        eVisaUrl: "",
        popupurl: "",
        popupStatus: "",
        countrySelected: "",
        key: 0,
        Header_Text_Label: '',
        Go_Button_Label: ''
    },
    methods: {
        pageContent: function () {
            var self = this;

            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Home/Home/Home.ftl';
                var holidaypackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Package/Package/Package.ftl';
                var flightdealurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Flight deals/Flight deals/Flight deals.ftl';
                self.eVisaUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/EVISA Page/EVISA Page/EVISA Page.ftl';
                self.popupurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/News and Notice/News and Notice/News and Notice.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    if (response.data.area_List.length) {
                        var titleComponent = self.pluck('Title', self.content.area_List);
                        self.titleDetails.Title = self.pluckcom('Title_Name', titleComponent[0].component);
                        var bannerCompnt = self.pluckcom('Banner_Image', titleComponent[0].component);
                        self.banner = bannerCompnt;
                        var formSearch = self.pluck('Form_Search', self.content.area_List);
                        self.formSearch.flightTab = self.pluckcom('Flight_tab_Name', formSearch[0].component);
                        self.formSearch.hotelTab = self.pluckcom('Hotel_tab_Name', formSearch[0].component);
                        self.formSearch.carTab = self.pluckcom('Car_tab_Name', formSearch[0].component);
                        self.formSearch.insuranceTab = self.pluckcom('Insurance_tab_Name', formSearch[0].component);
                        self.formSearch.oneWay = self.pluckcom('Oneway', formSearch[0].component);
                        self.formSearch.roundTrip = self.pluckcom('Round_Trip', formSearch[0].component);
                        self.formSearch.multiCity = self.pluckcom('Multi_City', formSearch[0].component);
                        self.formSearch.buttonLabel = self.pluckcom('Search_button_label', formSearch[0].component);
                        self.formSearch.flightDeparturePlaceholder = self.pluckcom('Flight_departure_placeholder', formSearch[0].component);
                        self.formSearch.flightArrivalPlaceholder = self.pluckcom('Flight_arrival_placeholder', formSearch[0].component);
                        self.formSearch.flightDatePlaceholderD = self.pluckcom('Flight_departure_date_placeholder', formSearch[0].component);
                        self.formSearch.flightDatePlaceholderR = self.pluckcom('Flight_arrival_date_placeholder', formSearch[0].component);
                        self.formSearch.economy = self.pluckcom('Economy_class', formSearch[0].component);
                        self.formSearch.business = self.pluckcom('Business_class', formSearch[0].component);
                        self.formSearch.first = self.pluckcom('First_class', formSearch[0].component);
                        self.formSearch.adults = self.pluckcom('adults_label', formSearch[0].component);
                        self.formSearch.children = self.pluckcom('children_label', formSearch[0].component);
                        self.formSearch.infants = self.pluckcom('Infants_label', formSearch[0].component);
                        self.formSearch.hotelCityPlaceholder = self.pluckcom('Hotel_city_placeholder', formSearch[0].component);
                        self.formSearch.hotelDatePlaceA = self.pluckcom('Hotel_arrival_date_placeholder', formSearch[0].component);
                        self.formSearch.hotelDatePlaceD = self.pluckcom('Hotel_departure_date_placeholder', formSearch[0].component);
                        self.formSearch.occupancy = self.pluckcom('Hotel_Occupancy_placeholder', formSearch[0].component);
                        self.formSearch.roomLabelSi = self.pluckcom('Room_single_label', formSearch[0].component);
                        self.formSearch.roomLabelDo = self.pluckcom('Room_double_label', formSearch[0].component);
                        self.formSearch.carStartingLocation = self.pluckcom('Car_Starting_location__placeholder', formSearch[0].component);
                        self.formSearch.carDropoffLocation = self.pluckcom('Car_drop_off_location__placeholder', formSearch[0].component);
                        self.formSearch.carPickupDateP = self.pluckcom('Car_pickup_date_placeholder', formSearch[0].component);
                        self.formSearch.carDropDateP = self.pluckcom('Car_drop_off_date_placeholder', formSearch[0].component);
                        self.formSearch.carPickupTimeP = self.pluckcom('Car_pickup_time_placeholder', formSearch[0].component);
                        self.formSearch.carPickupTimeL = self.pluckcom('Pickup_Time_label', formSearch[0].component);
                        self.formSearch.carDropTimeL = self.pluckcom('Drop_off_Time_label', formSearch[0].component);

                        var dealsArea = self.pluck('Deals_Area', self.content.area_List);
                        self.dealsArea.title1 = self.pluckcom('Deals_Title', dealsArea[0].component);
                        self.dealsArea.title2 = self.pluckcom('Title_End', dealsArea[0].component);



                        var packageArea = self.pluck('Package_Area', self.content.area_List);
                        self.packageArea.title1 = self.pluckcom('Package_Title', packageArea[0].component);
                        self.packageArea.title2 = self.pluckcom('Title_end', packageArea[0].component);


                        // Insurance form
                        var insuranceForm = self.pluck('Insurance_form', self.content.area_List);
                        self.insuranceForm.Name_of_Insured_label = self.pluckcom('Name_of_Insured_label', insuranceForm[0].component);
                        self.insuranceForm.Date_of_birth_label = self.pluckcom('Date_of_birth_label', insuranceForm[0].component);
                        self.insuranceForm.Passport_number_label = self.pluckcom('Passport_number_label', insuranceForm[0].component);
                        self.insuranceForm.gender = self.pluckcom('gender', insuranceForm[0].component);
                        self.insuranceForm.gender_male_label = self.pluckcom('gender_male_label', insuranceForm[0].component);
                        self.insuranceForm.gender_female_label = self.pluckcom('gender_female_label', insuranceForm[0].component);
                        self.insuranceForm.Telephone_number_label = self.pluckcom('Telephone_number_label', insuranceForm[0].component);
                        self.insuranceForm.Email_address_label = self.pluckcom('Email_address_label', insuranceForm[0].component);
                        self.insuranceForm.Destination_Label = self.pluckcom('Destination_Label', insuranceForm[0].component);
                        self.insuranceForm.Form_Title = self.pluckcom('Form_Title', insuranceForm[0].component);
                        self.insuranceForm.Period_of_insurance__Label = self.pluckcom('Period_of_insurance_Label', insuranceForm[0].component);
                        self.insuranceForm.Departure_date_Label = self.pluckcom('Departure_date_Label', insuranceForm[0].component);
                        self.insuranceForm.Return_date_Label = self.pluckcom('Return_date_Label', insuranceForm[0].component);
                        self.insuranceForm.Trip_Purpose__label = self.pluckcom('Trip_Purpose_label', insuranceForm[0].component);
                        self.insuranceForm.Address_Home_label = self.pluckcom('Address_Home_label', insuranceForm[0].component);
                        self.insuranceForm.Address_Office_Label = self.pluckcom('Address_Office_Label', insuranceForm[0].component);
                        self.insuranceForm.DECLARATION_Label = self.pluckcom('DECLARATION_Label', insuranceForm[0].component);
                        self.insuranceForm.Declaration_content = self.pluckcom('Declaration_content', insuranceForm[0].component);
                        self.insuranceForm.Accept_Declaration_statement = self.pluckcom('Accept_Declaration_statement', insuranceForm[0].component);
                        self.insuranceForm.Date_Label = self.pluckcom('Date_Label', insuranceForm[0].component);
                        self.insuranceForm.Occupation_label = self.pluckcom('Occupation_label', insuranceForm[0].component);
                        self.insuranceForm.Button_Submit = self.pluckcom('Button_Submit', insuranceForm[0].component);

                        self.bindBanner();
                        self.holidayPackageCard(holidaypackageurl, langauage);
                        self.flightDealsCard(flightdealurl, langauage);

                    }




                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });




        },
        getEVisaCountry: function (eVisaUrl, langauage) {
            var self = this;
            axios.get(eVisaUrl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
                // self.contentPackage = response.data.Values;
                if (response != undefined && response.data != undefined) {
                    var visa = self.pluck('Banner_Area', response.data.area_List);

                    self.Header_Text_Label = self.pluckcom('Header_Text_Label', visa[0].component);
                    self.Go_Button_Label = self.pluckcom('Go_Button_Label', visa[0].component);
                    self.eVisaCountryList = response.data.area_List[1].Page_Details.component[0].EVISA_Countries;
                }

            }).catch(function (error) {

                console.log('Error');
            });
        },
        getpopupcontent: function (popupurl, langauage) {
            var self = this;
            axios.get(popupurl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
                self.content = response.data;
                var noticearea = self.pluck('Notice', self.content.area_List);
                self.popupcontent.Notice_Label = self.pluckcom('Notice_Label', noticearea[0].component);
                self.popupcontent.Notice_content = self.pluckcom('Notice_Content', noticearea[0].component);
                self.popupcontent.Notice_Image = self.pluckcom('Notice_Image', noticearea[0].component);
                self.popupStatus = self.pluckcom('Status', noticearea[0].component);
                self.load();

                // self.packageArea.title2 = self.pluckcom('Title_end', noticearea[0].component);

                // self.contentPackage = response.data.Values;
                if (response != undefined && response.data != undefined) {
                    self.popupcontent.news = response.data.area_List
                }

            }).catch(function (error) {

                console.log('Error');
            });
        },
        holidayPackageCard(holidaypackageurl, langauage) {
            var self = this;
            axios.get(holidaypackageurl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {


                let holidayaPackageListTemp = [];
                if (response != undefined && response.data != undefined && response.data.Values != undefined) {
                    holidayaPackageListTemp = response.data.Values.filter(function (el) {
                        return el.Status == true &&
                            el.Show_In_Home_Page == true
                    });
                }
                self.holidayPackages = holidayaPackageListTemp;
                if (self.holidayPackages != null) {
                    self.getpackage = true;
                    setTimeout(function () { packageCarousel() }, 10);
                }
            }).catch(function (error) {
                console.log('Error');
                self.flightDeals = [];
            });

        },
        flightDealsCard(flightdealurl, langauage) {
            var self = this;
            axios.get(flightdealurl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
                // self.contentPackage = response.data.Values;
                var DealListTemp = [];
                if (response != undefined && response.data != undefined && response.data.Values != undefined) {
                    DealListTemp = response.data.Values.filter(function (el) {
                        return el.Status == true &&
                            el.Show_In_Home_Page == true
                    });
                    if (DealListTemp.length > 0) {
                        self.flightDeals = DealListTemp;
                        setTimeout(function () { dealsCarousel() }, 10);
                    }

                }


            }).catch(function (error) {
                console.log('Error');
                self.flightDeals = [];
            });




        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = null;
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        // EvisaContent: function () {
        //     var self = this;
        //     getAgencycode(function () {
        //         var Agencycode = "AGY20342";
        //         var huburl = ServiceUrls.hubConnection.cmsUrl;
        //         var portno = ServiceUrls.hubConnection.ipAddress;
        //         var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Evisa Requirement/Evisa Requirement/Evisa Requirement.ftl';

        //         axios.get(topackageurl, {
        //             headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': 'en' }
        //         }).then(function (visaresponse) {
        //             var VisaData = visaresponse.data;
        //             console.log(VisaData);
        //             let visaresponseTemp = [];
        //             if (VisaData != undefined && VisaData.Values != undefined) {
        //                 visaresponseTemp = VisaData.Values;
        //             }
        //           self.Visacountry=visaresponseTemp;

        //         }).catch(function (error) {
        //             console.log('Error');
        //             self.content = [];
        //         });
        //     });

        // },


        Departurefrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (AirportCode == this.CityTo) {
                    this.returnValue = false;
                    alertify.alert('Alert', 'Departure and arrival airports should not be same !');

                    this.CityFrom = '';

                } else {
                    this.returnValue = true;
                    this.CityFrom = AirportCode;
                }
            } else {
                var from = 'cityFrom' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].to) {
                        this.returnValue = false;
                        this.flightSearchCityName[from] = "";
                        alertify.alert('Alert', 'Departure and arrival airports should not be same !');
                    } else {
                        this.cityList[index].from = AirportCode
                        this.flightSearchCityName[from] = AirportName;
                        this.returnValue = true;
                    }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: AirportCode,
                        to: ''

                    });
                    this.flightSearchCityName[from] = AirportName;
                    this.returnValue = true;
                }
            }


        },
        Arrivalfrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (this.CityFrom == AirportCode) {
                    this.returnValue = false;
                    alertify.alert('Alert', 'Departure and arrival airports should not be same !');
                    this.validationMessage = "";
                    this.CityTo = '';

                } else {
                    this.returnValue = true;
                    this.CityTo = AirportCode;
                }
            } else {
                var to = 'cityTo' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].from) {
                        this.returnValue = false;
                        this.flightSearchCityName[to] = "";
                        alertify.alert('Alert', 'Departure and arrival airports should not be same !');

                    } else {
                        this.cityList[index].to = AirportCode;
                        this.flightSearchCityName[to] = AirportName;
                        this.returnValue = true;
                    }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: '',
                        to: AirportCode

                    });
                    this.flightSearchCityName[to] = AirportName;
                    this.returnValue = true;
                }
            }
        },
        SearchFlight: function () {

            var Departuredate = $('#deptDate01').val() == "" ? "" : $('#deptDate01').datepicker('getDate');
            if (!this.CityFrom) {
                alertify.alert('Alert', 'Please fill origin !');

                return false;
            }
            if (!this.CityTo) {
                alertify.alert('Alert', 'Please fill destination ! ');
                return false;
            }
            if (!Departuredate) {
                alertify.alert('Alert', 'Please choose departure date !').set('closable', false);
                return false;
            }
            var sec1TravelDate = moment(Departuredate).format('DD|MM|YYYY');

            var sectors = this.CityFrom + '-' + this.CityTo + '-' + sec1TravelDate;
            if (this.triptype == 'R') {
                var ArrivalDate = $('#retDate').val() == "" ? "" : $('#retDate').datepicker('getDate');
                if (!isNullorUndefined(ArrivalDate)) {
                    ArrivalDate = moment(ArrivalDate).format('DD|MM|YYYY');
                    if (!ArrivalDate) {
                        alertify.alert('Alert', 'Please choose return  date !').set('closable', false);

                        return false;
                    } else {
                        sectors += '/' + this.CityTo + '-' + this.CityFrom + '-' + ArrivalDate;
                    }
                } else {
                    alertify.alert('Alert', 'Please choose return date !').set('closable', false);

                    return false;
                }
            }
            var directFlight = this.direct_flight ? 'DF' : 'AF';

            var adult = this.selected_adults;
            var child = this.selected_children;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            getSuppliers([this.CityFrom + '|' + this.CityTo],
            function(supp) {
                var searchUrl = '/Flights/flight-listing.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-150-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                // searchUrl = searchUrl.toLocaleLowerCase();
                window.location.href = searchUrl;
            })
        },
        AddNewLeg() {
            if (this.legcount <= 5) {
                ++this.legcount;
                var legno = 1;
                if (this.cityList.length > 0) {
                    legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                    legno = legno + 1;
                }

                this.cityList.push({
                    id: legno,
                    from: '',
                    to: ''
                });

            }
            console.log(this.cityList);
        },
        DeleteLeg(leg) {
            var legs = this.legcount;
            if (legs > 1) {
                --this.legcount;
                var legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    this.cityList.splice(index, 1);

                }


            }
        },
        setCalender() {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = generalInformation.systemSettings.calendarDisplay;
            $("#deptDate01").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#retDate").datepicker("option", "minDate", selectedDate);

                }
            });

            $("#retDate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) { }
            });
            $("#txtLeg1Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);

                }
            });
            $("#txtLeg2Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg1Date").val();
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg3Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg2Date").val();
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg4Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg3Date").val();
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg5Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg4Date").val();
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg6Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg5Date").val();
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) { }
            });


        },
        setpaxcount(item, action) {
            if (item == 'adt') {
                if (action == 'plus') {
                    if (this.selected_adults < this.totalAllowdPax) {
                        ++this.selected_adults;
                    }
                } else {
                    if (this.selected_adults > 1) {
                        --this.selected_adults;
                        if (this.selected_adults < this.selected_infant) {
                            this.selected_infant = this.selected_adults;
                        }

                    }
                }
            } else if (item == 'chd') {
                if (action == 'plus') {
                    if (this.selected_adults + this.selected_children < this.totalAllowdPax) {
                        ++this.selected_children;
                    }
                } else {
                    if (this.selected_children > 0) {
                        --this.selected_children;
                    }
                }
            } else if (item == 'inf') {
                if (action == 'plus') {
                    if (this.selected_adults + this.selected_children + this.selected_infant < this.totalAllowdPax) {
                        if (this.selected_infant < this.selected_adults) {
                            ++this.selected_infant
                        }

                    }
                } else {
                    if (this.selected_infant > 0) {
                        --this.selected_infant
                    }
                }
            } else { }
            if (this.selected_adults + this.selected_children > this.totalAllowdPax) {
                this.selected_children = 0;
            }

            if (this.selected_adults + this.selected_children + this.selected_infant > this.totalAllowdPax) {
                this.selected_infant = 0;

            }
            var cabin = getCabinName(this.selected_cabin);
            var totalpax = this.selected_adults + this.selected_children + this.selected_infant;
            if (parseInt(totalpax) > 1) {
                totalpax = totalpax + ' Passengers';
            } else {
                totalpax = totalpax + ' Passenger'
            }
            this.Totaltravaller = totalpax + ',' + cabin;
        },
        clickoutside: function () {
            this.triptype = 'O';
        },

        MultiSearchFlight: function () {
            var sectors = '';
            var legDetails = [];
            for (var legValue = 1; legValue <= this.legcount; legValue++) {
                var temDeparturedate = $('#txtLeg' + (legValue) + 'Date').val() == "" ? "" : $('#txtLeg' + (legValue) + 'Date').datepicker('getDate');
                if (temDeparturedate != "" && this.cityList.length != 0 && this.cityList[legValue - 1].from != "" && this.cityList[legValue - 1].to != "") {
                    var departureFrom = this.cityList[legValue - 1].from;
                    var arrivalTo = this.cityList[legValue - 1].to;
                    var travelDate = moment(temDeparturedate).format('DD|MM|YYYY');
                    sectors += '/' + departureFrom + '-' + arrivalTo + '-' + travelDate;
                    legDetails.push(departureFrom + '|' + arrivalTo)
                } else {
                    alertify.alert('Alert', 'Please fill the Trip ' + (legValue) + '   fields !').set('closable', false);


                    return false;
                }
            }
            var directFlight = this.direct_flight ? 'DF' : 'AF';

            var adult = this.selected_adults;
            var child = this.selected_children;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            getSuppliers(legDetails,
            function(supp) {
                var searchUrl = '/Flights/flight-listing.html?flight=' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-150-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                // searchUrl = searchUrl.toLocaleLowerCase();
                window.location.href = searchUrl;
            })

        },
        getmoreinfo(url, item) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/Biscordint/" + item + ".html?page=" + url;
                    console.log(this.url);
                    window.location.href = url; // "/Biscordint/holiday-packagesview.html";
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            console.log(this.url);
            return url;

        },
        getvisapage(country) {

            if (country != "") {
               
                window.localStorage.setItem("countrySelected", country);
                window.location.href = window.location.origin + "/Biscordint/apply-visa.html"; // "/Biscordint/holiday-packagesview.html";
            } else {
                alertify.alert('Alert', 'Please select the country').set('closable', false);
                return false;
            }
        },
        //  getvisapage(link) {
        //     console.log(link);
        //     if (link != null) {
        //       if (link != "") {
        //         link = link.split("/Template/")[1];
        //         link = link.split(' ').join('-');
        //         link = link.split('.').slice(0, -1).join('.')
        //         // link = "/apply-visa.html?page=" + link + "&from=pkg";
        //         // window.location=link;
        //         link=link.split('_')[0];
        //         link = "/apply-visa.html?page=" + link + "&from=pkg";
        //         link = link.split("/Biscordint/")[0];
        //         window.location.href = link;
        //       }
        //       else {
        //         link = "#";
        //       }
        //     }

        //   },
        dateConvert: function (utc) {
            // this.value=this.date
            return (moment(utc).format("DD-MMM-YYYY"));
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },closeInsurance:function(){
            if($("#flightTab")!=undefined){
                $("#flightTab").click();
            }
            
        },
        sendTravelInsurance: async function () {
            if (!this.iName) {
                alertify.alert('Alert', 'Name required.').set('closable', false);
                return false;
            }
            if (!this.iDob) {
                alertify.alert('Alert', 'Date Of Birth required.').set('closable', false);
                return false;
            }
            if (!this.iPassportNo) {
                alertify.alert('Alert', 'Passport Number required.').set('closable', false);
                return false;
            }
            if (!this.iGender) {
                alertify.alert('Alert', 'Gender required.').set('closable', false);
                return false;
            }
            if (!this.iPhoneno) {
                alertify.alert('Alert', 'Phone number required.').set('closable', false);
                return false;
            }
            if (this.iPhoneno.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            if (!this.iEmail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.iEmail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.iOccupation) {
                alertify.alert('Alert', 'Occupation required.').set('closable', false);
                return false;
            }
            if (!this.iDestination) {
                alertify.alert('Alert', 'Destination required.').set('closable', false);
                return false;
            }
            if (!this.iPeriod) {
                alertify.alert('Alert', 'Period required.').set('closable', false);
                return false;
            }
            if (!this.iDepartureDate) {
                alertify.alert('Alert', 'Departure Date required.').set('closable', false);
                return false;
            }
            if (!this.iReturnDate) {
                alertify.alert('Alert', 'Return Date required.').set('closable', false);
                return false;
            }
            if (!this.iPurpose) {
                alertify.alert('Alert', 'Purpose required.').set('closable', false);
                return false;
            }
            if (!this.iAddress) {
                alertify.alert('Alert', 'Address required.').set('closable', false);
                return false;
            }
            if (!this.iAddressOffice) {
                alertify.alert('Alert', 'Office Address required.').set('closable', false);
                return false;
            } else {
                var fromEmail = JSON.parse(localStorage.User).loginNode.email;

                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(this.iEmail) ? this.iEmail : [this.iEmail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name,
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address,
                    personName: this.iName,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };


                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                this.iDob = moment(this.iDob).format('YYYY-MM-DDThh:mm:ss');
                this.iDepartureDate = moment(this.iDepartureDate).format('YYYY-MM-DDThh:mm:ss');
                this.iReturnDate = moment(this.iReturnDate).format('YYYY-MM-DDThh:mm:ss');
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Travel Insurance",
                    keyword1: this.iName,
                    date1: this.iDob,
                    keyword7: this.iPassportNo,
                    text1: this.iGender,
                    keyword8: this.iPhoneno,
                    keyword2: this.iEmail,
                    keyword3: this.iOccupation,
                    keyword4: this.iDestination,
                    number3: this.iPeriod,
                    date2: this.iDepartureDate,
                    date3: this.iReturnDate,
                    text2: this.iPurpose,
                    keyword5: this.iAddress,
                    keyword6: this.iAddressOffice,
                    date4: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, custmail);
                    this.iEmail = '';
                    this.iName = '';
                    // this.cntcontact = '';
                    // this.cntmessage = '';
                    // this.cntsubject = '';
                    alertify.alert('Travel Insurance', 'Thank you for contacting us.We shall get back to you.');
                } catch (e) {

                }



            }
        },
        insureClaender: function () {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = generalInformation.systemSettings.calendarDisplay;
            $("#insdob").datepicker({
                maxDate: "0d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                changeYear: true,
                yearRange: '-100:+0'

            });
            $("#insdeparture").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                // onSelect: function (selectedDate) {
                //     $("#insreturn").datepicker("option", "minDate", selectedDate);

                // }
            });

            $("#insreturn").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#insdeparture").val();
                    $("#insreturn").datepicker("option", "minDate", selectedDate);
                },

            });
        },
        bindBanner: function () {
            var self = this;
            if (self.banner.length > 0) {
                for (var i = 0; i < self.banner.length; i++) {
                    images.push(self.banner[i].Image);
                }
                $.fn.preload = function () {
                    this.each(function () {
                        $('<img/>')[0].src = this;
                    });
                }
                setTimeout(loadimg, 5000);

            }
        },

        

        load: function () {
            if (window.sessionStorage) {
                if (this.popupStatus == true) {
                    if (sessionStorage.getItem("popupsat") === null) {

                        $("#onloadmodel").modal("show");
                        sessionStorage.setItem("popupsat", "true")
                        if (sessionStorage.getItem("visited") === null) {

                            // $("#onloadmodel").modal("show");
                            sessionStorage.setItem("visited", "true")

                        }
                        else {
                            $("#onloadmodel").modal("hide");
                        }

                    }
                    else {
                        $("#onloadmodel").modal("hide");
                    }
                }
                else { }
            }

        },
        getAmount: function (amount) {
            amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
            return amount;
        },

        // fileUpload4AWS(file) {
        //     let returnURL = "";
        //     if (file != undefined && file.name != undefined) {
        //         let url = "";
        //         let arr = file.name.split('.');
        //         let consFileName = arr[0] + "_" + (new Date).getTime() + "." + arr[1];
        //         var configObject = await this.callingAPI("GET", "JsonPages/Url.json", null, null);
        //         AWS.config.credentials = new AWS.Credentials(configObject.Access_Key, configObject.Secret_Key);
        //         AWS.config.region = configObject.regionID;
        //         var bucket = new AWS.S3({ params: { Bucket: configObject.Bucket_Name } });
        //         var objKey = configObject.Form_Folder + consFileName;
        //         var params = { Key: objKey, ContentType: file.type, Body: file, ACL: 'public-read' };
        //         returnURL = configObject.Bucket_Url + configObject.Form_Folder + consFileName;
        //         await bucket.putObject(params, await function (err, data) {
        //             if (err) {
        //                 //console.log("Error : "+err);
        //             } else {


        //                 //console.log(returnURL);

        //             }
        //         });
        //     }
        //     return returnURL;


        // },

    },
    updated: function () {
        this.setCalender();
    },
    mounted: function () {
        var vm = this;

        this.pageContent();
        this.flightDealsCard();
        this.holidayPackageCard();
        this.setCalender();
        this.insureClaender();


        // this.EvisaContent();

        this.$nextTick(function () {
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            this.getEVisaCountry(this.eVisaUrl, langauage);
            this.getpopupcontent(this.popupurl, langauage);
                      
            loadCountryPicker();
           

            $('#visaCountryOrigin, #visaCountryDestination').on('changed.bs.select', function (e) {

                var citizenship = '';
                var destination = '';
                if (e.target.id == "visaCountryOrigin") {
                    citizenship = $(this).find(':selected').data('country-code');
                    destination = $('#visaCountryDestination').find(':selected').data('country-code');
                } else {
                    citizenship = $('#visaCountryOrigin').find(':selected').data('country-code');
                    destination = $(this).find(':selected').data('country-code');
                }

                if (e.target.id == "visaCountryDestination" && $('#visaCountryOrigin').val() == "") {
                    // alertify.alert("Error", "Please input country of origin.");
                } else if (e.target.id == "visaCountryOrigin" && $('#visaCountryDestination').val() == "") {
                    // alertify.alert("Error", "Please input destination country.");
                } else if (citizenship == destination) {
                    vm.isSameCountry = true;
                }
                else {
                    vm.showLoader = true;
                    vm.visaResponse = [];
                    vm.isSameCountry = false;


                    axios.get("https://requirements-api.sandbox.joinsherpa.com/v2/entry-requirements", {
                        params: {
                            citizenship: citizenship,
                            destination: destination,
                            language: 'en',
                            key: 'AIzaSyCt3T6Ck6-V3-uLt9CVIdiB5SM2xQ8MmIc',
                            affiliateId: 'biscordint'
                        },
                        headers: { accept: '*/*' }
                    })
                        .then(function (response) {
                            vm.visaResponse = response.data.visa;
                            vm.isSameCountry = false;
                            vm.showLoader = false;

                        })
                        .catch(function (error) {
                            var responseObject={requirement:"UNKNOWN",textual:{class:"normal",text:["Unfortunately, we do not have any information about this at the moment."]}};
                            vm.visaResponse = [responseObject];
                            vm.showLoader = false;
                            vm.isSameCountry = false;
                           

                        })
                }

            });
            //add for getting date for insurance form
            $('#insdob').change(function () {
                vm.iDob = $('#insdob').val();
              });
            $('#insdeparture').change(function () {
                vm.iDepartureDate = $('#insdeparture').val();
              });
            $('#insreturn').change(function () {
                vm.iReturnDate = $('#insreturn').val();
              });

        })


    },
    watch: {
        triptype: function () {
            if (this.triptype == "M") {
                this.travellerdisply = false;
            }
        }


    }
});


function dealsCarousel() {

    $("#flightdeals").owlCarousel({
        items: 2,
        itemsCustom: false,
        itemsDesktop: [2000, 2],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: [600, 1],
        itemsMobile: [479, 1],
        singleItem: false,
        itemsScaleUp: false,
        slideSpeed: 3000,
        //Autoplay
        autoPlay: true,
        stopOnHover: true,

        // Navigation
        navigation: true,
        navigationText: ['<i class="fa  fa-angle-left"></i>', '<i class="fa  fa-angle-right"></i>'],
        rewindNav: true,
        scrollPerPage: false,

        //Pagination
        pagination: false,
        paginationNumbers: false,

        // Responsive 
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
    });
}

function packageCarousel() {

    $("#holidaypackages").owlCarousel({
        items: 2,
        itemsCustom: false,
        itemsDesktop: [2000, 2],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: [600, 1],
        itemsMobile: [479, 1],
        singleItem: false,
        itemsScaleUp: false,
        slideSpeed: 3000,
        //Autoplay
        autoPlay: true,
        stopOnHover: true,

        // Navigation
        navigation: true,
        navigationText: ['<i class="fa  fa-angle-left"></i>', '<i class="fa  fa-angle-right"></i>'],
        rewindNav: true,
        scrollPerPage: false,

        //Pagination
        pagination: false,
        paginationNumbers: false,

        // Responsive 
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
    });
}


function getCabinName(cabinCode) {
    var cabinClass = 'Economy';
    if (cabinCode == "F") {
        cabinClass = "First Class";
    } else if (cabinCode == "C") {
        cabinClass = "Business";
    } else {
        try { cabinClass = getCabinClassObject(cabinCode).BasicClass; } catch (err) { }
    }
    return cabinClass;
}

function isNullorUndefined(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) { status = true; }
    return status;
}


function loadimg() {
    $('.banner').animate({ opacity: 1 }, 500, function () {
        //finished animating, minifade out and fade new back in           
        $('.banner').animate({ opacity: 0.7 }, 100, function () {
            currimg++;
            if (currimg > images.length - 1) {
                currimg = 0;
            }
            var newimage = images[currimg];
            //swap out bg src                
            $('.banner').css("background-image", "url(" + newimage + ")");
            //animate fully back in
            $('.banner').animate({ opacity: 1 }, 400, function () {
                //set timer for next
                setTimeout(loadimg, 5000);

            });

        });

    });

}
$(function() {
    
    loadCountryPicker();
})

