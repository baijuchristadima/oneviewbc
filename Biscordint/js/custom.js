
(function($) { 

/*--select-form-js--*/    
$(document).ready(function(){
  
	$(function() {
		"use strict";
		
		$( "#departure_date" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#return_date" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#check_out" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#check_in" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#package_start" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#car_start" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#car_end" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#cruise_start" ).datepicker({ minDate: -0, maxDate: "+3M" });
		$( "#adult_count" ).spinner({
			min: 1
		});
		$( "#child_count" ).spinner( {
			min: 1
		});
		$( "#hotel_adult_count" ).spinner( {
			min: 1
		});
		$( "#hotel_child_count" ).spinner( {
			min: 1
		});
		// $('.selectpicker').selectpicker({
		// 	style: 'custom-select-button'
		// });
});
});
    
    /*---select-box-js---*/


    
    
    
    /*-- Seven Id --*/
    $( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#from-1,#from-2,#from-3,#from-4,#from-5,#from-6,#from-7,#from-8,#from-9" )
        .datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
         buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to-1,#to-2,#to-3,#to-4,#to-5,#to-6,#to-6,#to-7,#to-8" ).datepicker({
        minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
         buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
    
    
    
    



        
    
    

})(jQuery);


$(document).ready(function () {
    $("#holidaypackage").owlCarousel({
        items: 2,
        itemsCustom: false,
        itemsDesktop: [2000, 2],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: [600, 1],
        itemsMobile: [479, 1],
        singleItem: false,
        itemsScaleUp: false,
        slideSpeed: 1000,
        //Autoplay
        autoPlay: true,
        stopOnHover: true,

        // Navigation
        navigation: true,
        navigationText: ['<i class="fa  fa-angle-left"></i>', '<i class="fa  fa-angle-right"></i>'],
        rewindNav: true,
        scrollPerPage: false,

        //Pagination
        pagination: false,
        paginationNumbers: false,

        // Responsive 
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
    });
 
    

});