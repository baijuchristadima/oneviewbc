var aboutus = new Vue({
    el: '#aboutuspage',
    name: 'aboutuspage',
    data: {
        // content: null,
        getdata: false,
        aboutUsContent: {
            title: '',
            home: 'Home',
            breadcrumb: '',
            titleImage: '',
            descriptionImage: '',
            description: '',
        },
        visionContent: null,
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/About us/About us/About us.ftl';

                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(async function (response) {
                    self.content = response.data;
                    var Aboutcontent = self.pluck('main', self.content.area_List);
                    if (Aboutcontent.length > 0) {
                        self.aboutUsContent.title = self.pluckcom('Title', Aboutcontent[0].component);
                        self.aboutUsContent.breadcrumb = self.pluckcom('Breadcrumb_label', Aboutcontent[0].component);
                        self.aboutUsContent.titleImage = self.pluckcom('Title_image', Aboutcontent[0].component);
                    }
                    var AboutcontentDetail = self.pluck('content', self.content.area_List);
                    if (AboutcontentDetail.length > 0) {
                        self.aboutUsContent.descriptionImage = self.pluckcom('Description_Image', AboutcontentDetail[0].component);
                        self.aboutUsContent.description = self.pluckcom('Description', AboutcontentDetail[0].component);
                    }


                    var vision = self.pluck('Flex', self.content.area_List);
                    if (vision.length > 0) {

                        self.visionContent = self.pluckcom('flex_content', vision[0].component);

                    }

                    self.getdata = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.aboutUsContent = [];
                });


            });

        },
    },
    mounted: function () {
        this.getPagecontent();
    },
})