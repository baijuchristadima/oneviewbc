
var TextFieldComponent = {
  props: ["data", "parentKey"],
  inject: ['$validator'],
  template: `
    <div :class="classObject">
      <div class="inp">
        <label>{{data.label}}<span class="red">{{(data.required?' *':'')}}</span><span class="text-danger required_txt">{{ errors.first('visa'+(data.key+parentKey)) }}</span></label>
        
        <input v-if="data.required"  :name="'visa'+(data.key+parentKey)" data-vv-as=" " v-validate="validationFields" type="text" v-model="internalValue" @input="onInput" placeholder="">
        <input v-else :name="'visa'+(data.key+parentKey)" type="text"  v-model="internalValue" @input="onInput" placeholder="">
      </div>
    </div>
    `,
  data() {
    return {
      internalValue: "",
      validationFields: this.data.field_type.toLowerCase() == 'email'?'required|email':'required'
    }
  },
  methods: {
    onInput:function() { this.$emit("input", this.internalValue) },
  },
  computed: {
    classObject: function () {
      var label = this.data.label.length;
      return {
        'col-lg-4 col-md-4 col-sm-4 col-xs-12': label < 50,
        'col-lg-6 col-md-6 col-sm-6 col-xs-12': label >= 50 && label < 100,
        'col-lg-9 col-md-9 col-sm-9 col-xs-12': label >= 80 && label < 110,
        'col-lg-12 col-md-12 col-sm-12 col-xs-12': label >= 110
      }
    }
  },
}

var DropDownComponent = {
  props: ["data", "parentKey"],
  inject: ['$validator'],
  template: `
    <div :class="classObject">
      <div class="inp">
        <label>{{data.label}}<span class="red">{{(data.required?' *':'')}}</span><span class="text-danger required_txt">{{ errors.first('visa'+(data.key+parentKey)) }}</span></label>
        
        <i class="fa fa-chevron-down"></i>
        <select  :ref="'visa'+(data.key+parentKey)"  v-model="internalValue" @change="onChange($event,data.key+parentKey,data,data.key)" v-if="data.required" :id="'visa'+(data.key+parentKey)" :name="'visa'+(data.key+parentKey)" data-vv-as=" " v-validate="'required'">
          <option v-for="(option, index) in data.options"  :key="index" :value="option.Name">{{option.Name}}</option>
        </select>
        <select  :ref="'visa'+(data.key+parentKey)"  v-model="internalValue" @change="onChange($event,data.key+parentKey,data,data.key)" v-else :id="'visa'+(data.key+parentKey)" :name="'visa'+(data.key+parentKey)">
          <option v-for="(option, index) in data.options" :key="index" :value="option.Name">{{option.Name}}</option>
        </select>
      </div>
    </div>
    `,
  data() {
    return {
      internalValue: ""
    }
  },
  watch: {
    internalValue() {
      
      this.$emit("input", this.internalValue);
    }
  },
  methods:{
    onChange:function(event,parentKey,data,datakey){
          var maxDateID = datakey;
          var vm = this;
      if(data.field_type=='drop down with key'){
        var option=data.options;
        var value=event.target.value;
        option.forEach(function(element) {
          var nextKey=Number(parentKey);
          nextKey=nextKey+Number(element.ID)*10;
          var docNext=document.getElementsByName('visa'+nextKey);
          if(docNext.length>0){
            docNext=docNext[0];
          }
          if(docNext!=undefined){
            var required=false;
            if(element.Name==value){
              required=true;
            }else{
                required = false;
                vm.$parent.$refs['visa' + nextKey][0].$data.internalValue = "";
                vm.$parent.$refs['visa' + nextKey][0].$refs['visa' + nextKey].value = " "
            }
            vm.$parent.$data.dataPerTraveller
            if(data.dataPerTraveller.fields!=undefined&&data.dataPerTraveller.fields.length>maxDateID){
              var fields= data.dataPerTraveller.fields[maxDateID];
              fields.required=required;
              fields.answer=required?fields.answer:"";
            }
             docNext.disabled=required?false:true;
             maxDateID=Number(maxDateID)+1;
          }
        });
      }
    }
  },
  computed: {
    classObject: function () {
      var label = this.data.label.length;
      return {
        'col-lg-4 col-md-4 col-sm-4 col-xs-12': label < 50,
        'col-lg-6 col-md-6 col-sm-6 col-xs-12': label >= 50 && label < 100,
        'col-lg-9 col-md-9 col-sm-9 col-xs-12': label >= 80 && label < 110,
        'col-lg-12 col-md-12 col-sm-12 col-xs-12': label >= 110
      }
    }
  },
}

var DateComponent = {
  components: {
    vuejsDatepicker
  },
  inject: ['$validator'],
  props: ["data", "parentKey"],
  template: `
    <div :class="classObject">
      <div class="inp">
        <label>{{data.label}}<span class="red" >{{(data.required?' *':'')}}</span><span class="text-danger required_txt">{{ errors.first('visa'+(data.key+parentKey)) }}</span></label>
          
          <vuejs-datepicker v-if="data.required" :name="'visa'+(data.key+parentKey)"  data-vv-as=" " :disabled-dates="data.disabledDates" v-validate="'required'" placeholder="Select Date" v-model="internalValue" @selected="dateSelected($event,data,data.key)"></vuejs-datepicker>
          <vuejs-datepicker v-else :name="'visa'+(data.key+parentKey)"  :disabled-dates="data.disabledDates" placeholder="Select Date" v-model="internalValue" @selected="dateSelected($event,data,data.key)"></vuejs-datepicker>
      </div>
    </div>
    `,
  data() {
    return {
      internalValue: ""
    }
  },
  mounted() {
    
  },
  methods: {
    dateSelected:function(date,data,parentKey){
      for(var i=0;i<data.maxDateID.length;i++){
        var maxDateID=Number(data.maxDateID[i])-1;
        if(data.dataPerTraveller.fields!=undefined&&data.dataPerTraveller.fields.length>maxDateID){
          var fields= data.dataPerTraveller.fields[maxDateID];
          var disabledDates={to:new Date(date)};
          fields.disabledDates=disabledDates;
         }
      }
     
      
      
    }
  },
  watch: {
    internalValue() {
      this.$emit("input", moment(new Date(this.internalValue)).format('YYYY-MM-DDThh:mm:ss'))
    }
  },
  computed: {
    classObject: function () {
      var label = this.data.label.length;
      return {
        'col-lg-4 col-md-4 col-sm-4 col-xs-12': label < 50,
        'col-lg-6 col-md-6 col-sm-6 col-xs-12': label >= 50 && label < 100,
        'col-lg-9 col-md-9 col-sm-9 col-xs-12': label >= 80 && label < 110,
        'col-lg-12 col-md-12 col-sm-12 col-xs-12': label >= 110
      }
    }
  },
}

var OptionComponent = {
  props: ["data", "parentKey"],
  inject: ['$validator'],
  template: `
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="inp">
        <label>{{data.label}}</label>
        <label class="visa_status">Yes
          <input type="radio" value="Yes" :name="'visa'+(data.key+parentKey)" v-model="internalValueOption">
            <span class="visa_status_checkmark"></span>
        </label>
        <label class="visa_status">No
          <input type="radio" value="No" :name="'visa'+(data.key+parentKey)" v-model="internalValueOption">
          <span class="visa_status_checkmark"></span>
        </label>
      </div>
    </div>
    `,
  data() {
    return {
      internalValueOption: "No",
    }
  },
  watch: {
    internalValueOption() {
      this.$emit("input", this.internalValueOption)
    }
  },
}

var OptionWithReasonComponent = {
  props: ["data", "parentKey"],
  inject: ['$validator'],
  template: `
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="inp">
            <label>{{data.label}}</label>
            <label class="visa_status">Yes
              <input type="radio" value="Yes" :name="'visa'+(data.key+parentKey)" v-model="internalValueOption">
                <span class="visa_status_checkmark"></span>
            </label>
            <label class="visa_status">No
              <input type="radio" value="No" :name="'visa'+(data.key+parentKey)" v-model="internalValueOption">
              <span class="visa_status_checkmark"></span>
            </label>
          </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" v-if="internalValueOption=='Yes'">
          <div class="inp">
            <label>{{'Specify reasons'}}<span class="red">{{(data.required? ' *':'')}}</span> <span class="text-danger required_txt">{{ errors.first('visa'+(data.key+parentKey)) }}</span></label>
            
            <input v-if="data.required&&internalValueOption=='Yes'" :name="'visa'+(data.key+parentKey)" data-vv-as=" " v-validate="'required'" type="text" v-model="internalValueText" placeholder="">
            <input v-else :name="'visa'+(data.key+parentKey)" type="text" v-model="internalValueText" placeholder="">
          </div>
        </div>
      </div>
    </div>
    `,
  data() {
    return {
      internalValueOption: "No",
      internalValueText: ""
    }
  },
  mounted() {
    this.$watch(
      function (vm) { return (vm.internalValueOption, vm.internalValueText, Date.now())},
      function () {
        this.$emit("input", { internalValueOption: this.internalValueOption, internalValueText: this.internalValueText })

      }
    )
  }
}

var FileComponent = {
  props: ["data", "parentKey"],
  inject: ['$validator'],
  template: `
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <div class="inp">
        <label>{{data.label}}<span class="red">{{(data.required?' *':'')}}</span><span class="text-danger required_txt">{{ errors.first('visa'+(data.key+parentKey)) }}</span></label>
        
        <label class="file-upload btn btn-primary">
          {{data.label}} 
          <input disabled="true" v-if="data.required" :id="'visa'+(data.key+parentKey)" :name="'visa'+(data.key+parentKey)" data-vv-as=" " v-validate="'required'" type="file" :ref="'file'+(data.key+parentKey)" @change="handleFileUpload" />
          <input disabled="true" v-else :id="'visa'+(data.key+parentKey)" :name="'visa'+(data.key+parentKey)" type="file" :ref="'file'+(data.key+parentKey)" @change="handleFileUpload" accept="application/jpeg,application/png,application/jpg" />
        </label>
        <button class="fileupload" @click.prevent="fileUpload('visa'+(data.key+parentKey))">Upload</button>
      </div>
    </div>
    `,
  data() {
    return {
      file: {},
      fileUrl: "",
    }
  },
  methods: {
    handleFileUpload: function () {
      var self = this;
      file_temp = self.$refs['file' + (self.data.key + self.parentKey)].files[0];
      var ext = file_temp.name.split('.')[1];
      if (ext == "jpeg" || ext == "png" || ext == "jpg") {
        self.file = self.$refs['file' + (self.data.key + self.parentKey)].files[0];
        self.submitFile();;
      } else {
        alertify.alert("Warning", "Incorrect file format");
        self.file = "";
        self.$refs['file' + (self.data.key + self.parentKey)].value='';
      }
      // console.log('>>>> 1st element in files array >>>> ', this.file);
    },fileUpload:function(name){
       $("#"+name).prop('disabled', false);
       $("#"+name).click();
       $("#"+name).prop('disabled', true);
      //$("#"+name).disabled="true";
    },
    submitFile: function () {
     
      var self = this;
      if (self.file != null && self.file != undefined && !jQuery.isEmptyObject(self.file)) {
        $.getJSON('../../credentials/AgencyCredentials.json', function (json) {
          let Password=json.file_upload_password[0].Password;
          var LoggedUser = window.localStorage.getItem("AgencyCode");
          // var UserName=LoggedUser.loginNode.code;
          var encodedString = btoa(LoggedUser + ":" + Password);
          var formData = new FormData();
          formData.append('file', self.file);
          // console.log('>> formData >> ', formData);
          var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "file" + (self.data.key + self.parentKey)
          // You should have a server side REST API 
          axios.post(url,
            formData, {
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'multipart/form-data',
              'Authorization': 'Basic ' + encodedString
            }
          }
          ).then(function (repo) {
            console.log('SUCCESS!!');
            self.fileUrl = repo.data.message;
              alertify.alert("Success", "File Successfully uploaded.");
          })
            .catch(function () {
              console.log('FAILURE!!');
            });
        })
      }
      else {
        alertify.alert("warning", "please select the file")
      }
    }
  },
  watch: {
    fileUrl() {
      this.$emit("input", this.fileUrl)
    }
  },
}




const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var visa= new Vue({
  components: {
    vuejsDatepicker
  },
  i18n,
  el: '#visa',
  name: 'visa',
  data: function () {
    return {
      content: null,
      getdata: false,
      pageTitle: {
        Title: '',
        Banner_image: '',
        Breadcrumb_path1: '',
        Breadcrumb_path2: ''
      },
      fileLink: '',
      file: null,
      fileLink_add_doc: '',
      file_add_doc: null,
      fileLink_photo: '',
      file_photo: null,
      numberOfTraveller: 2,
      visaFields: [],
      visaApplicationSuccess: false,
      visaInformation: {},
      pgRef: "",
      htmlValue: "",
      dontShowPopUp: false,
      visaReq:[],
      formFields:[],
      customerEmailField:{},
      travlerDetails:[],
      requestFormTraveller:[],
      currentTraveller:0
    }
  },
  methods: {
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {

      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";

        var country = window.localStorage.getItem("countrySelected")

        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/EVISA Page/EVISA Page/EVISA Page.ftl';
        var visaUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/EVISA ' + country + '/EVISA ' + country + '/EVISA ' + country +'.ftl';
        var dropDownUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Drop Down Values/Drop Down Values/Drop Down Values.ftl';
        var formURL = huburl + portno + '/cms/table/' + Agencycode ;
        self.constructAllFields(formURL,country);
        axios.get(pageurl, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          var pagecontent = self.pluck('Banner_Area', self.content.area_List);
          if (pagecontent.length > 0) {
            self.pageTitle.Banner_image = self.pluckcom('Banner_image', pagecontent[0].component);
            self.pageTitle.Title = self.pluckcom('Title', pagecontent[0].component);
            self.pageTitle.Breadcrumb_path1 = self.pluckcom('Breadcrumb_path1', pagecontent[0].component);
            self.pageTitle.Breadcrumb_path2 = self.pluckcom('Breadcrumb_path2', pagecontent[0].component);
          }

          self.getdata = true;

        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
        if (window.location.href.toLowerCase().indexOf("/apply-visa-form") >= 0) {
          window.localStorage.removeItem("isDone");

          self.numberOfTraveller = JSON.parse(atob(window.localStorage.getItem("visaInformation"))).numberOfTraveller;

          var dropdownParsed = [];

          axios.get(dropDownUrl, {
            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
          }).then(function (response) {
           

            var dropDownResponse = response.data.Values;

            for (var j = 0; j < dropDownResponse.length; j++) {
              dropdownParsed.push({
                ID: dropDownResponse[j].ID,
                Values: dropDownResponse[j].Values
              });

            }
            axios.get(visaUrl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
              var response = response.data.area_List;
              var fieldPertraveller = [];
              if(response.length>0&&response[0].Page_Settings!=undefined){
                self.customerEmailField = self.getAllMapData(response[0].Page_Settings.component);
              }
             
              for (var i = 1; i <= self.numberOfTraveller; i++) {
                var dataPerTraveller = {};
                var dataFields = [];
                var questions= [];
                dataPerTraveller.traveller = i;
               
                for (var index = 1; index < response.length; index++) {
                  var values = [];
                  var pickID=response[index]["Field_" + index]["component"][4].Pick_List_ID;
                  try {
                    values = _.find(dropdownParsed, { 'ID':  pickID}).Values
                  } catch (error) {
                    values = [];
                  }

                  var type = response[index]["Field_" + index]["component"][3].Data_Type;
                  var optionForField = "";
                  if (type.toLowerCase() == "option") {
                    optionForField = index;
                  }
                  // dataFields.push({
                  //   label: response[index]["Field_" + index]["component"][0].Label,
                  //   required: response[index]["Field_" + index]["component"][1].Required,
                  //   show: response[index]["Field_" + index]["component"][2].Need_To_Show,
                  //   type: type || null,
                  //   dropdown: values,
                  //   inputValue: type.toLowerCase() == "option" ? "Yes" : "",
                  //   optionForField: optionForField
                  // });

                  var componentType = "";
                  var disabledDates={to:''};
                  if(type.toLowerCase()=='date with today'){
                    disabledDates.to=new Date(Date.now() - 8640000);
                  }
                  var disableFieldID=[pickID];
                  switch (type.toLowerCase()) {
                    case "text":
                    case "text area":
                    case "textarea":
                    case "email":
                      componentType = TextFieldComponent;
                      break;
                    case "dropdown":
                    case "drop down":
                    case "drop down with key":
                      componentType = DropDownComponent;
                        break;
                    case "date":
                    case "date with today":  
                      componentType = DateComponent;
                      break;
                    case "option":
                      componentType = OptionComponent;
                      break;
                    case "option with reason":
                    case "optionwithreason":
                      componentType = OptionWithReasonComponent;
                      break;
                    case "file upload":
                    case "fileupload":
                      componentType = FileComponent;
                      break;
                    default:
                      componentType = TextFieldComponent;
                      break;
                  }

                  var needToshowField = response[index]["Field_" + index]["component"][2].Need_To_Show;
                  if (needToshowField) {
                    dataFields.push(
                      { label: response[index]["Field_" + index]["component"][0].Label, 
                        options: values, 
                        type: componentType, 
                        field_type:type.toLowerCase(),
                        maxDateID:disableFieldID,
                        disabledDates:disabledDates,
                        dataPerTraveller:dataPerTraveller,
                        answer: (type.toLowerCase() == "optionwithreason" || type.toLowerCase() == "option with reason") ? {
                          internalValueOption: "No",
                          internalValueText: ""
                        } : "", 
                        key: index, 
                        show: needToshowField,
                        required: response[index]["Field_" + index]["component"][1].Required,
                        field_Name:"Field " + index,
                        // show: needToshowField,
                        required: response[index]["Field_" + index]["component"][1].Required
                      },
                    );
                  }

                }
                dataPerTraveller.fields = dataFields;
                fieldPertraveller.push(dataPerTraveller);
                dataFields = [];
              }

              self.visaFields = fieldPertraveller;
              self.$nextTick(function () {
                $(document).ready(function () {
                  try {
                    $('.file-upload').file_upload();
                    $('#verticalTab').easyResponsiveTabs({
                      type: 'vertical',
                      width: 'auto',
                      fit: true
                    });
                  } catch (error) {

                  }

                });
              })
            }).catch(function (error) {
              console.log('Error');
            });

          }).catch(function (error) {
            console.log('Error');
          });



        }
        
        if (window.location.href.toLowerCase().indexOf("/apply-visa-confirm") >= 0) {
          if (self.getParameterByName("status") == 101) {
            self.requestFormTraveller = JSON.parse(atob(window.localStorage.getItem("RequestFormTraveller")));
            if (window.localStorage.getItem("RequestForm")==undefined) {
                
          
                
            } else {
              self.dontShowPopUp = true;
              if(window.localStorage.getItem("RequestForm")!=undefined){
                  var visaReq= JSON.parse(atob(window.localStorage.getItem("RequestForm")));
                  self.dontShowPopUp = false;
                  setTimeout(() => {
                      self.updateTravellerForm(visaReq,0,"Success");
                  }, 100);

                  window.localStorage.removeItem("RequestForm");
              }
            }
            self.visaApplicationSuccess = true;
            //self.visaInformationFields = JSON.parse(atob(window.localStorage.getItem("visaInformationFields")));
            self.pgRef = JSON.parse(atob(window.localStorage.getItem("pgRef")));
            self.htmlValue = JSON.parse(atob(window.localStorage.getItem("htmlValue")));
            window.localStorage.setItem("isDone", "true");
          }
          else if (self.getParameterByName("status") == null) {
            window.location.href = "/";
          }
        }
      });

    },getAllMapData: function (contentArry) {
        var tempDataObject = {};
        if (contentArry != undefined) {
            contentArry.map(function (item) {
                let allKeys = Object.keys(item)
                for (let j = 0; j < allKeys.length; j++) {
                    let key = allKeys[j];
                    let value = item[key];
                    if (key != 'name' && key != 'type') {
                        if (value == undefined || value == null) {
                            value = "";
                        }
                        tempDataObject[key] = value;
                    }
                }
            });
        }
        return tempDataObject;
    },constructAllFields:function (formURL,country){
        var self=this;
       
        try {
          setTimeout(() => {
            axios({
              method: 'GET',
              url: formURL,
              data: undefined,
              headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
              let allKeywords = [];
              var allForms = response.data;
              for (let i = 0; i < allForms.length; i++) {
                if (allForms[i].type == "E-Visa " + country) {

                  let tableDetails = allForms[i].tableDetails;
                  for (let k = 0; k < tableDetails.length; k++) {
                    let key = tableDetails[k].columnName;
                    let value = tableDetails[k].fieldName;
                    allKeywords[key] = value;
                  }
                  break;
                }
              }
              self.formFields = allKeywords;


            }).catch(function (error) {
              console.log(error);
            });
          }, 200);
        } catch (error) {
          
        }
        
        
    },updateTravellerForm: function(visaReq,index,status){
        if(visaReq!=undefined&&visaReq.length>Number(index)){
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            
            var serviceReq=visaReq[index];
            serviceReq.text25="Payment-"+status+"";
            var self=this;
            var url = huburl + portno + "/cms/data/"+serviceReq.id;
            if (serviceReq != null) {
                serviceReq = JSON.stringify(serviceReq);
              }
            axios({
              method: 'put',
              url: url,
              data: serviceReq,
              headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                console.log(response);
              if (visaReq.length > (Number(index) + 1)) {
                self.updateTravellerForm(visaReq,(Number(index) + 1),status);
              }
              if ((visaReq.length - 1) == Number(index)) {
                self.sendEmail2Customer(visaReq,status);
              }
              
            }).catch(function (error) {
              console.log(error);
            });
        }
        

    },
    hideUnhide: function (item) {
      if (item.inputValue.toLowerCase() == "yes" || item.inputValue.toLowerCase() == "") {
        $("#visa" + item.optionForField).hide();
      } else {
        $("#visa" + item.optionForField).show();
      }
    },
    handleFileUpload: function (item, index) {
     
      var self = this;
      file_temp = self.$refs['file' + index][0].files[0];
      var ext = file_temp.name.split('.')[1];
      if (ext == "jpeg" || ext == "png" || ext == "jpg") {
        item.file = self.$refs['file' + index][0].files[0];
      } else {
        alertify.alert("warning", "Incorrect file format");
        item.file = "";
      }
      // console.log('>>>> 1st element in files array >>>> ', this.file);
    },
    // submitFile: function (item, index) {
    //  
    //   var self = this;
    //   if (item.file != null && item.file != undefined) {
    //     $.getJSON('js/AgencyCredentials.json', function (json) {
    //       var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
    //       json.User_Name=LoggedUser;
    //       var encodedString = btoa(json.User_Name + ":" + json.Password);
    //       var formData = new FormData();
    //       formData.append('file', item.file);
    //       // console.log('>> formData >> ', formData);
    //       var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "file" + index
    //       // You should have a server side REST API 
    //       axios.post(url,
    //         formData, {
    //         headers: {
    //           'Accept': 'application/json',
    //           'Content-Type': 'multipart/form-data',
    //           'Authorization': 'Basic ' + encodedString
    //         }
    //       }
    //       ).then(function (repo) {
    //         console.log('SUCCESS!!');
    //         item.inputValue = repo.data.message;
    //         alertify.alert("Success", "file sucessfully uploaded");
    //       })
    //         .catch(function () {
    //           console.log('FAILURE!!');
    //         });

    //     })
    //   }
    //   else {
    //     alertify.alert("warning", "please select the file")
    //   }
    // },
    sendEmail2Customer:function (visaReq,status){
        var LoggedUser = JSON.parse(window.localStorage.getItem("User"));

        try {
          var main = this.pluckcom('main', JSON.parse(window.localStorage.getItem("cmsContent")).area_List);
          this.logoUrl = this.pluckcom('Logo', main.component);
        }
        catch (error) {
          this.logoUrl = ServiceUrls.hubConnection.logoBaseUrl + LoggedUser.loginNode.logo + '.xhtml?ln=logo';
        }
        //this.guestDetails.guestEmail
        
      var emailContent = JSON.parse(atob(window.localStorage.getItem("emailContent")));
        var requestFormTraveller = JSON.parse(atob(window.localStorage.getItem("RequestFormTraveller")));

        var data = {
          fromEmail: LoggedUser.loginNode.email || "itsolutionsoneview@gmail.com",
          toEmail:[] ,
          ccEmails: null,
          bccEmails: null,
          primaryColor: "#FFF",
          secondaryColor: "#FFF",
          subject: emailContent.Email_Subject,
          logoUrl: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
          agencyName: JSON.parse(localStorage.User).loginNode.name || "",
          agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
          year: new Date().getFullYear(),
          agencyNumber: LoggedUser.loginNode.code,
          agencyEmail: LoggedUser.loginNode.email,
          fullName: requestFormTraveller[0].nameValue,
          cntEmail: LoggedUser.emailId,
          cntTel: LoggedUser.contactNumber,
          statusContent: emailContent.Email_Content,
          referenceId: this.pgRef,
          sectionColor: "#7f3e98",
          travellers: []
        }
        

        
        for(let i=0;i<requestFormTraveller.length;i++){
            var emailField=requestFormTraveller[i].emailField;
            var nameValue=requestFormTraveller[i].nameValue;
            var passportID=requestFormTraveller[i].passportID;
            var travelDate=requestFormTraveller[i].travelDate;
            let reqTravaller={index:Number(i)+1,name:nameValue,passportId:passportID||"",travelDate:travelDate}; 
            data.travellers.push(reqTravaller);
            //data.toEmail.push(emailField);
           /*if(i==0||(data.toEmail==undefined ||data.toEmail=='')){
            data.toEmail=[emailField];
           }*/
        }
        for(let i=0;i<requestFormTraveller.length;i++){
          var emailField=requestFormTraveller[i].emailField;
          var nameValue=requestFormTraveller[i].nameValue;
          data.toEmail=[emailField];
          data.fullName=nameValue;
          var mailrequrl = "https://obo34f0rg5.execute-api.us-east-1.amazonaws.com/default/VisaResponse";
          
          var request=JSON.parse(JSON.stringify(data));
          sendMailService(mailrequrl, request);
        }
       
        
    },backTraveller:function(location,type){
      var hash = 'tab'+(Number(location)-1),lis = $("ul.resp-tabs-list > li");
      lis.removeClass("resp-tab-active");
      var li=document.getElementById(hash);
      li.click();
      this.currentTraveller=(Number(location)-1);
    },nextTraveller:function(location,type){
      var id="travller"+location;
     
      var need2Continue=true;
      for (let index = 0; index < this.visaFields.length; index++) {
        const element = this.visaFields[index];
        if(location==index){
          for(var k=0;k<element.fields.length;k++ ){
            const fileds=element.fields[k];
            if(fileds.required==true&&(fileds.answer==undefined||fileds.answer=='')){
              need2Continue=false;
              break;
            }
          }
          break;
        }
      }
      
      
      if(need2Continue==false){
        this.$validator.validate();
      }else{
        var hash = 'tab'+(Number(location)+1),lis = $("ul.resp-tabs-list > li");
        lis.removeClass("resp-tab-active");
        var li=document.getElementById(hash);
        li.click();
        this.currentTraveller=(Number(location)+1);
      }
      

     
    },
    sendForm2CMS: function (index) {

      var fieldMap = this.formFields;
      var country = window.localStorage.getItem("countrySelected");
      var agencyCode = JSON.parse(localStorage.User).loginNode.code;
      var self = this;
        if(Number(index)==0){
            self.visaReq=[];
            self.travlerDetails=[];
        }
      
      if (this.visaFields.length > Number(index)) {
        travller = this.visaFields[index];
        var allFields = travller.fields;
        var serviceReq = {text25:"NA"};
        var count = 1;

        allFields.forEach(function (fields) {
          var key = "Field " + count;
          var reqKey = fieldMap[key];
          var value = fields.answer;
          // if (fields.type.toLowerCase() == "date" && fields.inputValue != "") {
          //   value = moment(new Date(fields.inputValue)).format('YYYY-MM-DDThh:mm:ss')
          // }
          console.log(typeof fields.answer)
          if(value==undefined||value==''){
            value='NA';
          }
          if (typeof fields.answer == "object") {
           
            serviceReq[reqKey] = value.internalValueOption;
            serviceReq[fieldMap["Field " + (count + 1)]] = value.internalValueText||"";
            count = Number(count) + 1;
          }else {
            serviceReq[reqKey] = value;
          }
          count = Number(count) + 1;
        });
        serviceReq["type"] = "E-Visa "+country;
        serviceReq["nodeCode"] = agencyCode;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var url = huburl + portno + "/cms/data";
        if (serviceReq != null) {
          serviceReq = JSON.stringify(serviceReq);
        }
        console.log(serviceReq);
        axios({
          method: 'post',
          url: url,
          data: serviceReq,
          headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            serviceReq=JSON.parse(serviceReq);
            serviceReq['id'] = response.data;
            console.log(serviceReq);
            self.visaReq.push(serviceReq);


            var emailField=serviceReq[self.customerEmailField.Email_Field_ID];
           var nameValue=serviceReq[self.customerEmailField.Name_Field_ID];
           var passportID=serviceReq[self.customerEmailField.Passport_Field_ID];
           var travelDate=serviceReq[self.customerEmailField.Travel_Date_Field_ID];
           travelDate=moment(travelDate).format("DD-MM-YYYY");
          
           /*for(var j in serviceReq){
                var sub_key = j;
                var sub_val = serviceReq[j];
                if(sub_key==self.customerEmailField.Email_Field_ID){
                    emailField=sub_val;
                }
                if(sub_key==self.customerEmailField.Name_Field_ID){
                    nameValue=sub_val;
                }
                if(sub_key==self.customerEmailField.Passport_Field_ID){
                    passportID=sub_val;
                }
                if(sub_key==self.customerEmailField.Travel_Date_Field_ID){
                    travelDate=sub_val;
                }
               
            }*/
            let travleRow={emailField:emailField,nameValue:nameValue,passportID:passportID,travelDate:travelDate};
           
            self.travlerDetails.push(travleRow);
          if (self.visaFields.length > (Number(index) + 1)) {
            self.sendForm2CMS((Number(index) + 1));
          }
          if ((self.visaFields.length - 1) == Number(index)) {
            window.localStorage.setItem("RequestForm", btoa(JSON.stringify(self.visaReq)));
            window.localStorage.setItem("RequestFormTraveller", btoa(JSON.stringify(self.travlerDetails)));
            window.localStorage.setItem("emailContent", btoa(JSON.stringify(self.customerEmailField)));
            self.submitApplication();
          }
          
        }).catch(function (error) {
          console.log(error);
        })



      }

    }, submitForm: function (index) {
      var vm = this;
      this.$validator.validate().then(function (result) {
       
        if (result) {
          if (vm.visaFields.length > 0) {
            vm.sendForm2CMS(0);
          }
        }
        else {
          alertify.alert("Warning", "Please enter required fields.");
        }
      });
    },
    submitApplication: function () {
     
      var email = "";
      var requestFormTraveller = JSON.parse(atob(window.localStorage.getItem("RequestFormTraveller")));
      if (requestFormTraveller != undefined && requestFormTraveller.length>0){
        let travelerObject = requestFormTraveller[0];
        email = travelerObject.emailField;
      }

      var currentPayGateways = undefined;
      var user = JSON.parse(localStorage.User);
      if (user.loginNode.paymentGateways != null) {
        currentPayGateways = user.loginNode.paymentGateways;
      }
      var paymentDetails = {};
      var totalCost = JSON.parse(atob(window.localStorage.getItem("visaInformation"))).totalCost
      var htmlValue = JSON.parse(atob(window.localStorage.getItem("visaInformation"))).visaInformation;
      window.localStorage.setItem("htmlValue", btoa(JSON.stringify(htmlValue)))

      
      paymentDetails["bookingReference"] = "";
      paymentDetails["totalAmount"] = totalCost;
      paymentDetails["customerEmail"] = email;

      if (totalCost && totalCost == 0 || email == "") {
        return;
      }


      var rand = Math.floor(Math.random() * 10);

      paymentDetails["cartID"] = moment(new Date()).format("MMYYDD-mmkkss-SSSS") + rand;

      //window.localStorage.setItem("visaInformationFields", btoa(JSON.stringify(this.visaFields)))
      window.localStorage.setItem("pgRef", btoa(JSON.stringify(paymentDetails["cartID"])))
      // paymentDetails["cartID"] = "";
      paymentDetails["supplier"] = "";
      paymentDetails["currentPayGateways"] = currentPayGateways;
      //before pg
      console.log("before pg", localStorage.access_token);
      if (currentPayGateways != undefined) {
        if (currentPayGateways.length > 0) {
          if (currentPayGateways[0].id == 9) {
            //Merch Integration
            window.location.href = '/pay.html';
          } else {
            //Redirection
            var paymentForm = paymentManager(paymentDetails, window.location.origin + "/Biscordint/apply-visa-confirm.html", "");
          }
        } else {
          alertify.alert('Warning', 'Payment Gateway Not Available.');
        }
      }
      else {
        alertify.alert('Warning', 'Payment Gateway Not Available.');
      }
      console.log("before pg form", localStorage.access_token);
    },
    bindCMSInputData: function (numberOfTraveller) {

    },
    getParameterByName: function (name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, '\\$&');
      var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }
  },

  mounted: function () {
    this.getPagecontent();
    this.bindCMSInputData();
    
  }
});