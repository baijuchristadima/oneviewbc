var aboutus = new Vue({
    el: '#servicepage',
    name: 'servicepage',
    data: {
        // content: null,
        getdata: false,
        serviceContent: {
            title: '',
            home: 'Home',
            breadcrumb: '',
            titleImage: '',
            description: '',
            serviceTitle: '',
        },
        services: null,
        specifications: null,


    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;

            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Services/Services/Services.ftl';

                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(async function (response) {
                    self.content = response.data;
                    var serviceContent = self.pluck('Title_area', self.content.area_List);
                    if (serviceContent.length > 0) {
                        self.serviceContent.title = self.pluckcom('Title', serviceContent[0].component);
                        self.serviceContent.breadcrumb = self.pluckcom('Breadcrumb_Label', serviceContent[0].component);
                        self.serviceContent.titleImage = self.pluckcom('Title_Image', serviceContent[0].component);
                        self.serviceContent.description = self.pluckcom('Introduction', serviceContent[0].component);
                    }

                    var service = self.pluck('services_area', self.content.area_List);
                    if (service.length > 0) {
                        self.serviceContent.serviceTitle = self.pluckcom('Service_Title', service[0].component);
                        self.services = self.pluckcom('service', service[0].component);

                    }
                    var specification = self.pluck('specification', self.content.area_List);
                    if (service.length > 0) {

                        self.specifications = self.pluckcom('specifications', specification[0].component);

                    }


                    self.getdata = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.serviceContent = [];
                });


            });

        },
    },
    mounted: function () {
        this.getPagecontent();
    },
})