const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
  })
var Packageinfo=new Vue({
    i18n,
  el: '#Packageinfo',
  name: 'Packageinfo',
  data: {
      content: null,
      getdata: true,
      getpagecontent: false,
      selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
      CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
      packageurl:'',
     
      Package: {
        Subtitle: '',
          Image:'',
          Validity_From:'',
          Validity_To:'',
          Region:'',
          Destination:'',
          Days:'',
          Nights:'',
          Price:'',
      },
      pageContent: {
        Inner_page_title:'',
        inner_BreadcrumbPath1:'',
        inner_BreadcrumbPath2:'',
      },
      pagelabel: {
        starting_from_label:'',
        booknow_label_1:'',
        booknow_label_2:'',
        Validity_label:'',
        Region_label:'',
        Destination_label:'',
        Call_us_label:'',
        phone_number:'',
        Share_on_text:'',
        facebook:'',
        

      },
      tabs:[],
      gallery:[],
  },
  methods: {
      pluck(key, contentArry) {
          var Temparry = [];
          contentArry.map(function (item) {
              if (item[key] != undefined) {
                  Temparry.push(item[key]);
              }
          });
          return Temparry;
      },
      pluckcom(key, contentArry) {
          var Temparry = [];
          contentArry.map(function (item) {
              if (item[key] != undefined) {
                  Temparry = item[key];
              }
          });
          return Temparry;
      },
      getPagecontent: function () {
          var self = this;
          getAgencycode(function (response) {
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var Agencycode = response;
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Package Page/Package Page/Package Page.ftl';
            
               packageurl = getQueryStringValue('page');
              if (packageurl != "") {
                  packageurl = packageurl.split('-').join(' ');
                  //  langauage = packageurl.split('_')[1];
                  packageurl = packageurl.split('_')[0];
                  var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
                  
                  axios.get(topackageurl, {
                      headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                  }).then(function (response) {
                      self.content = response.data;
                  var pagecontent = self.pluck('main', self.content.area_List);
                  if (pagecontent.length > 0) {
                      self.Package.Subtitle = self.pluckcom('Subtitle', pagecontent[0].component);
                      self.Package.Image = self.pluckcom('Image', pagecontent[0].component);
                      self.Package.Validity_From = self.pluckcom('Validity_From', pagecontent[0].component);
                      self.Package.Validity_To = self.pluckcom('Validity_To', pagecontent[0].component);
                      self.Package.Region = self.pluckcom('Region', pagecontent[0].component);
                      self.Package.Destination = self.pluckcom('Destination', pagecontent[0].component);
                      self.Package.Days = self.pluckcom('Days', pagecontent[0].component);
                      self.Package.Nights = self.pluckcom('Nights', pagecontent[0].component);
                      self.Package.Price = self.pluckcom('Price', pagecontent[0].component);
                    
                      setTimeout(function () { responsivetab() }, 10);

                  }
                  var subcontent = self.pluck('Overview', self.content.area_List);
                  if (subcontent.length > 0) {
                      self.tabs = self.pluckcom('Overview_items', subcontent[0].component);
                      
                  }
                  var subcontent = self.pluck('Overview', self.content.area_List);
                  if (subcontent.length > 0) {
                      self.gallery = self.pluckcom('gallery_images', subcontent[0].component);
                      
                  }
                  

                  self.getdata = false;

                }).catch(function (error) {
                  window.location.href="/Biscordint/holiday-packages.html";
                  self.content = [];
                  self.getdata = true;
              });
 
              }
              axios.get(pageurl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language':langauage }
            }).then(function (response) {
                self.content = response.data;
                var pagecontent = self.pluck('Title_area', self.content.area_List);
                if (pagecontent.length > 0) {
                  self.pageContent.Common_image = self.pluckcom('Common_image', pagecontent[0].component);
                    self.pageContent.Inner_page_title = self.pluckcom('Inner_page_title', pagecontent[0].component);
                    self.pageContent.inner_BreadcrumbPath1= self.pluckcom('inner_BreadcrumbPath1', pagecontent[0].component);
                    self.pageContent.inner_BreadcrumbPath2 = self.pluckcom('inner_BreadcrumbPath2', pagecontent[0].component);
                    
                }
                var pagecontent = self.pluck('Inner_page_labels', self.content.area_List);
                if (pagecontent.length > 0) {
                    self.pagelabel.starting_from_label = self.pluckcom('starting_from_label', pagecontent[0].component);
                    self.pagelabel.booknow_label_1 = self.pluckcom('booknow_label_1', pagecontent[0].component);
                    self.pagelabel.booknow_label_2= self.pluckcom('booknow_label_2', pagecontent[0].component);
                    self.pagelabel.Duration_label = self.pluckcom('Duration_label', pagecontent[0].component);
                    self.pagelabel.Validity_label = self.pluckcom('Validity_label', pagecontent[0].component);
                    self.pagelabel.Region_label = self.pluckcom('Region_label', pagecontent[0].component);
                    self.pagelabel.Destination_label = self.pluckcom('Destination_label', pagecontent[0].component);
                    self.pagelabel.Call_us_label = self.pluckcom('Call_us_label', pagecontent[0].component);
                    self.pagelabel.phone_number = self.pluckcom('phone_number', pagecontent[0].component);
                    self.pagelabel.Share_on_text = self.pluckcom('Share_on_text', pagecontent[0].component);
                    self.pagelabel.facebook = self.pluckcom('facebook', pagecontent[0].component);
                    
                }             


                self.getdata = true;

            }).catch(function (error) {
                console.log('Error');
                self.content = [];
            });

            

            });        
      },
      bookNow:function(response){
        // var Agencycode = response;
        // var huburl = ServiceUrls.hubConnection.cmsUrl;
        // var portno = ServiceUrls.hubConnection.ipAddress;
        bookurl = getQueryStringValue('page');
              if (bookurl != "") {
                bookurl = bookurl.split('-').join(' ');
                  //  langauage = packageurl.split('_')[1];
                  bookurl = bookurl.split('_')[0];
                  bookurl = "/Biscordint/booknow.html?page=" + bookurl ;
                  window.location.href =bookurl;
                  
              }
       
      },     
      dateFormatter:function(utc){         
        return(moment(utc).utcOffset("+05:30").format("DD MMM YYYY"));    
      },
      getAmount: function (amount) {
        amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
        return amount;
      }

  },
  mounted: function () {

      this.getPagecontent();
      


  },
  
});

function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
function responsivetab() {
  //Horizontal Tab
  $('#parentHorizontalTab').easyResponsiveTabs({
      type: 'default', //Types: default, vertical, accordion
      width: 'auto', //auto or any width like 600px
      fit: true, // 100% fit in a container
      tabidentify: 'hor_1', // The tab groups identifier
      activate: function(event) { // Callback function if tab is switched
          var $tab = $(this);
          var $info = $('#nested-tabInfo');
          var $name = $('span', $info);
          $name.text($tab.text());
          $info.show();
      }
  });

  // Child Tab
  $('#ChildVerticalTab_1').easyResponsiveTabs({
      type: 'vertical',
      width: 'auto',
      fit: true,
      tabidentify: 'ver_1', // The tab groups identifier
      activetab_bg: '#fff', // background color for active tabs in this group
      inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
      active_border_color: '#c1c1c1', // border color for active tabs heads in this group
      active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
  });

  //Vertical Tab
  $('#parentVerticalTab').easyResponsiveTabs({
      type: 'vertical', //Types: default, vertical, accordion
      width: 'auto', //auto or any width like 600px
      fit: true, // 100% fit in a container
      closed: 'accordion', // Start closed if in accordion view
      tabidentify: 'hor_1', // The tab groups identifier
      activate: function(event) { // Callback function if tab is switched
          var $tab = $(this);
          var $info = $('#nested-tabInfo2');
          var $name = $('span', $info);
          $name.text($tab.text());
          $info.show();
      }
  });
}


