const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var packageView = new Vue({
  i18n,
  el: '#flightdealsdetails',
  name: 'flightdealsdetails',
  data: {
    getpackage: false,
    getdata: false,
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
    CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    pagecontent: {
      Title: '',
      Breadcrum1: '',
      Breadcrumb2: '',
      Image: '',
      Duration: ''
    },
    maincontent: {
      Image: '',
      Duration: '',
      Title: '',
      From: '',
      to: '',
      Price: '',
      Currency: '',
      NowLabel: '',
      BookLabel: '',
      Starting_From_Label: '',
      DurationLabel: '',
      FromLabel: '',
      ToLabel: '',
      CallusLabel: '',
      CallNo: '',
      ShareonLabel: '',
      FacebookLabel: '',
      BookNowUrl: ''



    },
    overitems: null
  },
  mounted() {

    this.getPageTitle();

  },
  methods: {

    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },

    getPageTitle: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Flight Deals Details Page/Flight Deals Details Page/Flight Deals Details Page.ftl';
        // var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Flight deals/Flight deals/Flight deals.ftl';
        axios.get(pageurl, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          self.content = response.data;
          var pagecontent = self.pluck('banner_area', self.content.area_List);
          if (pagecontent.length > 0) {
            self.pagecontent.Title = self.pluckcom('Title', pagecontent[0].component);
            self.pagecontent.Breadcrum1 = self.pluckcom('Breadcrum1', pagecontent[0].component);
            self.pagecontent.Breadcrumb2 = self.pluckcom('Breadcrumb2', pagecontent[0].component);
            self.pagecontent.Image = self.pluckcom('Image', pagecontent[0].component);

          }
          self.getdata = true;

        }).catch(function (error) {
          console.log('Error');
          self.content = [];
        });
        var packageurl = getQueryStringValue('page');
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        if (packageurl != "") {
          packageurl = packageurl.split('-').join(' ');
          packageurl = packageurl.split('_')[0];
          var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
          axios.get(topackageurl, {
            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
          }).then(function (response) {
            self.content = response.data;
            var maincontent = self.pluck('main', self.content.area_List);
            if (maincontent.length > 0) {
              self.maincontent.Image = self.pluckcom('Image', maincontent[0].component);
              self.maincontent.Duration = self.pluckcom('Duration', maincontent[0].component);
              self.maincontent.From = self.pluckcom('From', maincontent[0].component);
              self.maincontent.To = self.pluckcom('To', maincontent[0].component);
              self.maincontent.Title = self.pluckcom('Title', maincontent[0].component);
              self.maincontent.Price = self.pluckcom('Price', maincontent[0].component);
              self.maincontent.Currency = self.pluckcom('Currency', maincontent[0].component);
              self.maincontent.BookLabel = self.pluckcom('BookLabel', maincontent[0].component);
              self.maincontent.NowLabel = self.pluckcom('NowLabel', maincontent[0].component);
              self.maincontent.Starting_From_Label = self.pluckcom('Starting_From_Label', maincontent[0].component);
              self.maincontent.DurationLabel = self.pluckcom('DurationLabel', maincontent[0].component);
              self.maincontent.FromLabel = self.pluckcom('FromLabel', maincontent[0].component);
              self.maincontent.ToLabel = self.pluckcom('ToLabel', maincontent[0].component);
              self.maincontent.CallusLabel = self.pluckcom('CallusLabel', maincontent[0].component);
              self.maincontent.CallNo = self.pluckcom('CallNo', maincontent[0].component);
              self.maincontent.ShareonLabel = self.pluckcom('ShareonLabel', maincontent[0].component);
              self.maincontent.FacebookLabel = self.pluckcom('FacebookLabel', maincontent[0].component);
              self.maincontent.BookNowUrl = self.pluckcom('BookNowUrl', maincontent[0].component);
           
              setTimeout(function () { responsivetab() }, 10);

            }
            var item = self.pluck('Overview_Items', self.content.area_List);
            if (item.length > 0) {

              self.overitems = self.pluckcom('Items', item[0].component);

            }
            self.getdata = true;



          }).catch(function (error) {
            console.log('Error');
            self.content = [];
          });
        }
      });
    },
    getAmount: function (amount) {
      amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
      return amount;
    }


  }
})
function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
function responsivetab() {
  //Horizontal Tab
  $('#parentHorizontalTab').easyResponsiveTabs({
    type: 'default', //Types: default, vertical, accordion
    width: 'auto', //auto or any width like 600px
    fit: true, // 100% fit in a container
    tabidentify: 'hor_1', // The tab groups identifier
    activate: function (event) { // Callback function if tab is switched
      var $tab = $(this);
      var $info = $('#nested-tabInfo');
      var $name = $('span', $info);
      $name.text($tab.text());
      $info.show();
    }
  });

  // Child Tab
  $('#ChildVerticalTab_1').easyResponsiveTabs({
    type: 'vertical',
    width: 'auto',
    fit: true,
    tabidentify: 'ver_1', // The tab groups identifier
    activetab_bg: '#fff', // background color for active tabs in this group
    inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
    active_border_color: '#c1c1c1', // border color for active tabs heads in this group
    active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
  });

  //Vertical Tab
  $('#parentVerticalTab').easyResponsiveTabs({
    type: 'vertical', //Types: default, vertical, accordion
    width: 'auto', //auto or any width like 600px
    fit: true, // 100% fit in a container
    closed: 'accordion', // Start closed if in accordion view
    tabidentify: 'hor_1', // The tab groups identifier
    activate: function (event) { // Callback function if tab is switched
      var $tab = $(this);
      var $info = $('#nested-tabInfo2');
      var $name = $('span', $info);
      $name.text($tab.text());
      $info.show();
    }
  });
}