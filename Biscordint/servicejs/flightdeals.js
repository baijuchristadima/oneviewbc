const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var packagelist = new Vue({
    i18n,
    el: '#flightdeals',
    name:'flightdeals',
    data:{
        packages:'',
        getpackage: false,
        getdata:false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        pagecontent:{
          Title:'',
          Breadcrumb1:'',
          Breadcrumb2:''

        }
    },
    mounted(){

      this.getPageTitle();
      
    },
    methods:{
      pluck(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
            if (item[key] != undefined) {
                Temparry.push(item[key]);
            }
        });
        return Temparry;
    },
    pluckcom(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
            if (item[key] != undefined) {
                Temparry = item[key];
            }
        });
        return Temparry;
    },

      getPageTitle: function(){
        var self = this;
        getAgencycode(function (response) {
          var Agencycode = response;
          var huburl = ServiceUrls.hubConnection.cmsUrl;
          var portno = ServiceUrls.hubConnection.ipAddress;
          var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
          var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Flight deals Page/Flight deals Page/Flight deals Page.ftl';
          var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Flight deals/Flight deals/Flight deals.ftl';
          axios.get(pageurl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language':langauage }
       }).then(function (response) {
        self.content = response.data;
        var pagecontent = self.pluck('Banner', self.content.area_List);
        if (pagecontent.length > 0) {
            self.pagecontent.Title = self.pluckcom('Title', pagecontent[0].component);
            self.pagecontent.Breadcrumb1 = self.pluckcom('Breadcrumb1', pagecontent[0].component);
            self.pagecontent.Breadcrumb2 = self.pluckcom('Breadcrumb2', pagecontent[0].component);
            self.pagecontent.background_Image=self.pluckcom('background_Image',pagecontent[0].component);
    
        }
        self.getdata = true;

    }).catch(function (error) {
        console.log('Error');
        self.content = [];
    });



    axios.get(topackageurl, {
      headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language':langauage }
  }).then(function (response) {
      var packageData = response.data;
      

      let holidayaPackageListTemp=[];
      if(packageData!=undefined&&packageData.Values!=undefined){
          holidayaPackageListTemp = packageData.Values.filter(function (el) {
              return el.Status == true;
          });
      }
      self.packages =holidayaPackageListTemp;
      self.getpackage = true;
      


  }).catch(function (error) {
      console.log('Error');
      self.content = [];
    });
  });
},
getmoreinfo(url) {
  if (url != null) {
      if (url != "") {
          url = url.split("/Template/")[1];
          url = url.split(' ').join('-');
          url = url.split('.').slice(0, -1).join('.')
          url = "/Biscordint/flight-deals-details.html?page=" + url;
          console.log(url);
          window.location.href =url;
      }
      else {
          url = "#";
      }
  }
  else {
      url = "#";
  }
  return url;
  
},
getAmount:function(amount){
    amount= parseFloat(amount.replace( /[^\d\.]*/g, ''));
    return amount;
}
}
})