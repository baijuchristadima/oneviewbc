
const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
new Vue({
    i18n,
    el: '#testmonials',
    name: 'testmonials',
    data: function () {
        return {
            content: null,
            getdata: false,
            pageTitle: {
                Title: '',
                Banner_image: '',
                Breadcrumb_path1: '',
                Breadcrumb_path2: ''
            },
            pageContent: {
                Name_Label: '',
                Email_Label: '',
                Image_Label: '',
                Feedback_Label: '',
                SubmitLabel: '',
                UploadLabel: '',
                cusMessage: ''

            },
            feedback: '',
            fname: '',
            file: '',
            femail: '',
            fmessage: '',
            requestedDate: '',
            feedBacks: [],
            allReview : []
        }
    },
    filters: {

        subStr: function (string) {
            if (string.length > 100)
                return string.substring(0, 200) + '...';

            else
                return string;
        }

    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {


            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Testimonials/Testimonials/Testimonials.ftl';
                //   var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Package/Package/Package.ftl';

                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var pagecontent = self.pluck('Banner_Area', self.content.area_List);
                    if (pagecontent.length > 0) {
                        self.pageTitle.Banner_image = self.pluckcom('Banner_image', pagecontent[0].component);
                        self.pageTitle.Title = self.pluckcom('Title', pagecontent[0].component);
                        self.pageTitle.Breadcrumb_path1 = self.pluckcom('Breadcrumb_path1', pagecontent[0].component);
                        self.pageTitle.Breadcrumb_path2 = self.pluckcom('Breadcrumb_path2', pagecontent[0].component);
                    }

                    self.getdata = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
                // PAGE CONTENT

                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var pagecontent = self.pluck('Feedback', self.content.area_List);
                    if (pagecontent.length > 0) {
                        self.pageContent.Name_Label = self.pluckcom('Name_Label', pagecontent[0].component);
                        self.pageContent.Email_Label = self.pluckcom('Email_Label', pagecontent[0].component);
                        self.pageContent.Image_Label = self.pluckcom('Image_Label', pagecontent[0].component);
                        self.pageContent.Feedback_Label = self.pluckcom('_Feedback_Label', pagecontent[0].component);
                        self.pageContent.SubmitLabel = self.pluckcom('Button_Label', pagecontent[0].component);
                        self.pageContent.UploadLabel = self.pluckcom('_upload_button_label', pagecontent[0].component);
                        self.pageContent.cusMessage = self.pluckcom('Success_Message', pagecontent[0].component);
                        console.log(self.pageContent.nameLabel);
                    }

                    self.getdata = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        submitFeedback: async function () {
            var self = this;
            if (!this.fname) {
                alertify.alert('Alert', 'Name required.').set('closable', false);

                return false;
            }
            // if (!this.file) {
            //  alertify.alert('Alert', 'Picture required.').set('closable', false);

            // return false;
            // }
            if (!this.femail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.femail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.fmessage) {
                alertify.alert('Alert', 'Feedback required.').set('closable', false);
                return false;
            } else {
                var toEmail = JSON.parse(localStorage.User).loginNode.email;
                var frommail = JSON.parse(localStorage.User).loginNode.email;
                var postData = {
                    type: "AdminContactUsRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.fname,
                    emailId: this.femail,
                    message: this.fmessage,


                };


                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.femail) ? this.femail : [this.femail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.fname,
                    message: self.pageContent.cusMessage,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                //let today = new Date().toLocaleDateString()
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                console.log(requestedDate);
                // let toDateVal = await moment(today).format();
                let insertFeedbackData = {
                    type: "Feedback",
                    keyword1: this.fname,
                    keyword2: this.femail,
                    // text1:this.file,
                    text2: this.fmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertFeedbackData, null);
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, postData);
                    sendMailService(emailApi, custmail);
                    this.femail = '';
                    this.fname = '';
                    // this.file= '';
                    this.fmessage = '';
                    this.requestedDate = '';
                    alertify.alert('Contact us', self.pageContent.cusMessage);
                } catch (e) {

                }



            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        getFeedbacks: async function () {
            var self=this;
        
                let agencyCode = localStorage.AgencyCode;
                let requestObject = { from: 0, size: 100, type: "Feedback", nodeCode: agencyCode, orderBy: "desc" };
               let responseObject = await this.cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
                  if (responseObject != undefined && responseObject.data != undefined) {
                    allResult = JSON.parse(JSON.stringify(responseObject)).data;
                    let allReview=[];
                    for (let i = 0; i < allResult.length; i++) {
                      if (allResult[i].text2 != null || allResult[i].text2 != undefined ) {
                        let object = {
                          Name: allResult[i].keyword1,
                          Email:allResult[i].keyword2,
                          Date: moment(String(allResult[i].date1), "YYYY-MM-DDThh:mm:ss").format('MMM DD,YYYY'),
                          comment: allResult[i].text2,
                        };
                        allReview.push(object);
                      }
                    }
                    self.allReview =allReview;
                }
                
                });
          },

          async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        
        // fetchData: function () {
        //     getAgencycode(function (response) {
        //         var Agencycode = response;
        //     let data = { from: 0, size: 100, type: "Feedback", nodeCode: Agencycode, orderBy: "desc" };
        //     var callMethod = "POST";
        //     var languageID = "en";
        //     var huburl = ServiceUrls.hubConnection.cmsUrl;
        //     var portno = ServiceUrls.hubConnection.ipAddress;
        //     var url = huburl + portno + '/cms/data/search'
        //     if (languageID == undefined || languageID == '') {
        //         languageID = "en";
        //     }
        //     let headers = {
        //         'Content-Type': 'application/json',

        //         'Accept-Language': languageID,
        //         "Access-Control-Request-Headers": "*",
        //         "Access-Control-Request-Method": "*",
        //         "accept":"application/json"
        //     }
        //     url = encodeURI(url);
        //     data = JSON.stringify(data);
        //     return fetch(url,
        //         {
        //             method: callMethod, // *GET, POST, PUT, DELETE, etc.
        //             //credentials: "same-origin", // include, *same-origin, omit
        //             headers: headers,
        //             body: data, // body data type must match "Content-Type" header
        //         }).then(function (response) {
        //             return response.json();
        //         });
        //     });
        // },
        // getFeedbacks: function () {
        //     var self = this;
        //     var content=self.fetchData()
        //     content.then(function (response) {
        //         if (response.code == undefined) {
        //             self.feedBacks = JSON.parse(JSON.stringify(response)).data;
        //         }

        //     });



        // }


    },

    mounted: function () {
        this.getPagecontent();
        this.getFeedbacks();
    },

});