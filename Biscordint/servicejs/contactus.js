const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var contact = new Vue({
    i18n,
    el: '#contactuspage',
    name: 'contactuspage',
    data: {
        content: null,
        getdata: false,
        pageContent: {
            Title: '',
            Title_Image:'',
            Breadcrumb_Label:'',
            Breadcrumblabel2:'',
            Title_Image:''
        },
        form:{
            Form_Title:'',
            Name_label:'',
            Email_label:'',
            Phone_Label:'',
            Nationality_label:'',
            Button_label:'',
            Message_Placeholder:'',
            Address_Title:'',
            Address_Field:'',
            Contact_Title:'',
            Phone_subtitle:'',
            Phone_content:'',
            Mail_Title:'',
            Mail_Content:''
            

        },
        title:{
            Branches_Title:'',
            LocationTitle:''
        },
        branches:null,
        cntusername: '',
        cntemail: '',
        cntcontact: '',
        cntmessage: '',
        cntnationality:''
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact Us Page/Contact Us Page/Contact Us Page.ftl';
                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language':langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var pagecontent = self.pluck('Title_area', self.content.area_List);
                    if (pagecontent.length > 0) {
                        self.pageContent.Title = self.pluckcom('Title', pagecontent[0].component);
                        self.pageContent.Title_Image = self.pluckcom('Title_Image', pagecontent[0].component);
                        self.pageContent.Breadcrumb_Label = self.pluckcom('Breadcrumb_Label', pagecontent[0].component);
                        self.pageContent.Breadcrumblabel2 = self.pluckcom('Breadcrumblabel2', pagecontent[0].component);

                
                    }
                    var form = self.pluck('Form_Area', self.content.area_List);
                    if (form.length > 0) {
                        self.form.Form_Title = self.pluckcom('Form_Title', form[0].component);
                        self.form.Name_label = self.pluckcom('Name_label', form[0].component);
                        self.form.Email_label = self.pluckcom('Email_label', form[0].component);
                        self.form.Phone_Label = self.pluckcom('Phone_Label', form[0].component);
                        self.form.Nationality_label = self.pluckcom('Nationality_label', form[0].component);
                        self.form.Message_Placeholder = self.pluckcom('Message_Placeholder', form[0].component);
                        self.form.Button_label = self.pluckcom('Button_label', form[0].component);
                        
                    }
                    var form = self.pluck('Address_Area', self.content.area_List);
                    if (form.length > 0) {
                        self.form.Address_Title = self.pluckcom('Address_Title', form[0].component);
                        self.form.Address_Field = self.pluckcom('Address_Field', form[0].component);
                        self.form.Contact_Title = self.pluckcom('Contact_Title', form[0].component);
                        self.form.Phone_subtitle = self.pluckcom('Phone_subtitle', form[0].component);
                        self.form.Phone_content = self.pluckcom('Phone_content', form[0].component);
                        self.form.Mail_Title = self.pluckcom('Mail_Title', form[0].component);
                        self.form.Mail_Content = self.pluckcom('Mail_Content', form[0].component);
                        
                    }
                    var title = self.pluck('Titles', self.content.area_List);
                    if (form.length > 0) {
                        self.title.Branches_Title = self.pluckcom('Branches_Title', title[0].component);
                        self.title.LocationTitle = self.pluckcom('LocationTitle', title[0].component);
                    }
                   
                    var branch = self.pluck('Branch', self.content.area_List);
                    if (branch.length > 0) {

                        self.branches =  self.pluckcom('Branch_Details', branch[0].component);
                        self.setMapforBranch();

                    }
                    




                    self.getdata = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });
        },
        sendcontactus: async function () {
            if (!this.cntusername) {
                alertify.alert('Alert', 'Name required.').set('closable', false);

                return false;
            }
            if (!this.cntemail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.cntcontact) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            if (!this.cntnationality) {
                alertify.alert('Alert', 'Nationality required.').set('closable', false);
                return false;
            }

            if (!this.cntmessage) {
                alertify.alert('Alert', 'Message required.').set('closable', false);
                return false;
            } else {
                var frommail = JSON.parse(localStorage.User).loginNode.email;
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl +JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.cntusername,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Contact us",
                    keyword1: this.cntusername,
                    keyword2: this.cntemail,
                    keyword4: this.cntcontact,
                    keyword3: this.cntnationality,
                    text1: this.cntmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, custmail);
                    this.cntemail = '';
                    this.cntusername = '';
                    this.cntcontact = '';
                    this.cntmessage = '';
                    this.cntnationality = '';
                    alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
                } catch (e) {

                }



            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        setMapforBranch:function(){
           var self=this;
           var branches=self.branches;
          var locations=[];
           for(var i =0;i<branches.length;i++){
               var descrption=branches[i].BranchName+"</br><a target=\"_blank\" href=\"https://maps.google.com/maps?t=m&f=d&saddr=Current+Location&daddr=25.234022,55.308862\"><b>Get Directions</b></a><br/>"+branches[i].Address+"<br/>Contact :"+branches[i].PhoneNo+"<br/>Email ID :"+branches[i].Email+"";         
                 locations.push({latitude:branches[i].Latitude,longitude:branches[i].Longitude,Title:branches[i].Title,descrption:descrption});
           }
           console.log(locations);
           //var locations= [["25.0750853","55.308862","Branch Name </br><a target=\"_blank\" href=\"https://maps.google.com/maps?t=m&f=d&saddr=Current+Location&daddr=25.234022,55.308862\"><b> Get Directions </b></a><br/>Address<br/>Contact : 9895177923<br/>Fax : 6995555<br/>Email ID :fkldjsfk@gmail.com"],["18.2416726","42.4783768","<a target=\"_blank\" href=\"https://maps.google.com/maps?t=m&f=d&saddr=Current+Location&daddr=25.234022,55.308862\"><b> Get Directions </b></a><br/>Address<br/>Contact : 9895177923<br/>Fax : 6995555<br/>Email ID :fkldjsfk@gmail.com"],["24.3865729","54.2784171","<a target=\"_blank\" href=\"https://maps.google.com/maps?t=m&f=d&saddr=Current+Location&daddr=25.234022,55.308862\"><b> Get Directions </b></a><br/>Address<br/>Contact : 9895177923<br/>Fax : 6995555<br/>Email ID :fkldjsfk@gmail.com"]]
           var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: new google.maps.LatLng(locations[0].latitude,  locations[0].longitude),
            mapTypeId: google.maps.MapTypeId.ROADMAP
    
        });
    
        var infowindow = new google.maps.InfoWindow();
    
        var marker, i;
    
        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i].latitude, locations[i].longitude),
                map: map,
                title: locations[i].Title
            });
    
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(locations[i].descrption);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
        }
    },
    mounted: function () {
        this.getPagecontent();
    },
});