const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
new Vue({
    i18n,
    el: '#booknow',
    name: 'booknow',
    data: {
        content: null,
        getdata: true,
        getpagecontent: false,
        bookname: '',
        bookemail: '',
        bookphone: '',
        bookadult: '',
        bookdate: '',
        bookchild: '',
        bookinfant: '',
        bookmessage: '',
        bookpack: '',

        Package: {
            Subtitle: '',
        },
        banner: {
            title: '',
            breadcrumb_path1: '',
            breadcrumb_path2: '',
            NameLabel: '',
            EmailLabel: '',
            PhoneLabel: '',
            Package_Name_label: '',
            Tentative_Travel_Date_Label: '',
            Select_of_Adults_Label: '',
            Number_of_Children_Label: '',
            Number_of_Infants: '',
            Additional_Information_Label: '',
            Button_Label: '',
            cusMessage: 'Thank you for the request, your request is being reviewed our sales team will soon revert with the status.'

        },
        tabs: null,

    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var packageurl = getQueryStringValue('page');
                if (packageurl != "") {
                    packageurl = packageurl.split('-').join(' ');
                    //  langauage = packageurl.split('_')[1];
                    packageurl = packageurl.split('_')[0];
                    var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';

                    axios.get(topackageurl, {
                        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                    }).then(function (response) {
                        self.content = response.data;
                        var pagecontent = self.pluck('main', self.content.area_List);
                        if (pagecontent.length > 0) {
                            self.Package.Subtitle = self.pluckcom('Subtitle', pagecontent[0].component);
                        }


                        self.getdata = false;

                    }).catch(function (error) {
                        window.location.href = "/Biscordint/holiday-packages.html";
                        self.content = [];
                        self.getdata = true;
                    });

                }

            });
        },
        gettitle: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/BookNow/BookNow/BookNow.ftl';
                axios.get(topackageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var pagecontent = self.pluck('Title_area', self.content.area_List);
                    if (pagecontent.length > 0) {
                        self.banner.title = self.pluckcom('title', pagecontent[0].component);
                        self.banner.breadcrumb_path1 = self.pluckcom('breadcrumb_path1', pagecontent[0].component);
                        self.banner.breadcrumb_path2 = self.pluckcom('breadcrumb_path2', pagecontent[0].component);
                        self.banner.NameLabel = self.pluckcom('NameLabel', pagecontent[0].component);
                        self.banner.EmailLabel = self.pluckcom('EmailLabel', pagecontent[0].component);
                        self.banner.PhoneLabel = self.pluckcom('PhoneLabel', pagecontent[0].component);
                        self.banner.Package_Name_label = self.pluckcom('Package_Name_label', pagecontent[0].component);
                        self.banner.Tentative_Travel_Date_Label = self.pluckcom('Tentative_Travel_Date_Label', pagecontent[0].component);
                        self.banner.Select_of_Adults_Label = self.pluckcom('Select_of_Adults_Label', pagecontent[0].component);
                        self.banner.Number_of_Children_Label = self.pluckcom('Number_of_Children_Label', pagecontent[0].component);
                        self.banner.Number_of_Infants = self.pluckcom('Number_of_Infants', pagecontent[0].component);
                        self.banner.Additional_Information_Label = self.pluckcom('Additional_Information_Label', pagecontent[0].component);
                        self.banner.Button_Label = self.pluckcom('Button_Label', pagecontent[0].component);
                        self.banner.cusMessage = self.pluckcom('Booking_Success_Message', pagecontent[0].component);
                    }


                    self.getdata = false;

                }).catch(function (error) {
                    window.location.href = "/Biscordint/holiday-packages.html";
                    self.content = [];
                    self.getdata = true;
                });
            });

        },
        setCalender: function () {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = generalInformation.systemSettings.calendarDisplay;
            $("#traveldate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,

            });
        },
        booknow: async function () {
            var self = this;
            if (!this.bookname) {
                alertify.alert('Alert', 'Name required.').set('closable', false);

                return false;
            }
            // if (!this.file) {
            //  alertify.alert('Alert', 'Picture required.').set('closable', false);

            // return false;
            // }
            if (!this.bookphone) {
                alertify.alert('Alert', 'Phone Number required.').set('closable', false);
                return false;
            }
            this.bookpack = this.Package.Subtitle;

            if (!this.bookemail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.bookemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            this.bookdate = $('#traveldate').val() == "" ? "" : $('#traveldate').datepicker('getDate');

            if (!this.bookdate) {
                alertify.alert('Alert', 'pick a date.').set('closable', false);
                return false;
            }
            if (!this.bookadult) {
                alertify.alert('Alert', 'Select number of Adults').set('closable', false);
                return false;
            } else {
                var frommail = JSON.parse(localStorage.User).loginNode.email;
                var toEmail = JSON.parse(localStorage.User).emailId;
                var postData = {
                    type: "PackageBookingRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                    ccEmails: [],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    packegeName: this.bookpack,
                    personName: this.bookname,
                    emailAddress: this.bookemail,
                    contact: this.bookphone,
                    departureDate: this.bookdate,
                    adults: this.bookadult,
                    child2to5: this.bookchild,
                    child6to11: "0",
                    infants: this.bookinfant,
                    message: this.bookmessage,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary

                };


                var custmail = {
                    type: "ThankYouRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.bookemail) ? this.bookemail : [this.bookemail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.bookname,
                    message: self.banner.cusMessage,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };


                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                var traveldate = $('#traveldate').val() == "" ? "" : $('#traveldate').datepicker('getDate');
                let toDateVal = await moment(traveldate).format('YYYY-MM-DDThh:mm:ss');
                let insertFeedbackData = {
                    type: "Package Booking",
                    keyword1: this.bookname,
                    keyword2: this.bookemail,
                    keyword4: this.bookphone,
                    keyword3: this.bookpack,
                    date1: toDateVal,
                    number2: this.bookadult,
                    number3: this.bookchild,
                    number4: this.bookinfant,
                    text1: this.bookmessage,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertFeedbackData, null);
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, postData);
                    sendMailService(emailApi, custmail);
                    this.bookemail = '';
                    this.bookname = '';
                    this.bookphone = '',
                    this.bookpack = '';
                    this.bookdate = '';
                    this.bookadult = '';
                    this.bookchild = '';
                    this.bookinfant = '';
                    this.bookmessage = '';


                    alertify.alert('Book package', self.banner.cusMessage);
                } catch (e) {

                }



            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },

    },
    mounted: function () {

        this.getPagecontent();
        this.gettitle();
        this.setCalender();



    }


});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}



