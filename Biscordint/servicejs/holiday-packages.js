const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var packageList = new Vue({
    i18n,
    el: '#holidayPackages',
    name: 'holidayPackages',
    data: {
        content: null,
        getdata: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        pageContent: {
            Title: '',
            Breadcrumbpath1: '',
            Breadcrumb2: '',
            Common_image: '',
        },
        packages: '',
        getpackage: false,
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Package Page/Package Page/Package Page.ftl';
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Package/Package/Package.ftl';

                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var pagecontent = self.pluck('Title_area', self.content.area_List);
                    if (pagecontent.length > 0) {
                        self.pageContent.Common_image = self.pluckcom('Common_image', pagecontent[0].component);
                        self.pageContent.Title = self.pluckcom('Title', pagecontent[0].component);
                        self.pageContent.Breadcrumb1 = self.pluckcom('Breadcrumb_path1', pagecontent[0].component);
                        self.pageContent.Breadcrumb2 = self.pluckcom('Breadcrumb_path2', pagecontent[0].component);

                    }

                    self.getdata = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });


                axios.get(topackageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    var packageData = response.data;
                    let holidayaPackageListTemp = [];
                    if (packageData != undefined && packageData.Values != undefined) {
                        holidayaPackageListTemp = packageData.Values.filter(function (el) {
                            return el.Status == true
                        });
                    }
                    self.packages = holidayaPackageListTemp;
                    self.getpackage = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });

        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/Biscordint/holiday-packagesview.html?page=" + url;
                    console.log(this.url);
                    window.location.href = url;// "/Biscordint/holiday-packagesview.html";
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            console.log(this.url);
            return url;

        },
        getAmount: function (amount) {
            amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
            return amount;
        }

    },
    mounted: function () {
        this.getPagecontent();

    },

});