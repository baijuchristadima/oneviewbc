var aboutus = new Vue({
    el: '#newsAndEvents',
    name: 'newsAndEvents',
    data: {
        // content: null,
        
        getdata: false,
        newsEventContent: {
            title: '',
            home: 'Home',
            breadcrumb: '',
            titleImage: '',
            button_label:'',
        },
        newsCards:null,
        Cards:{
            Title: '',
            Image: '',
            Date: '',
            Image: '',
            Details:'',
            More_Info:'',
         }
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        dateConvert: function (utc) {
            // this.value=this.date
            return (moment(utc).format("DD-MMM-YYYY"));
          },
        getNewsContent: function () {
            var self = this;

            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/NewsAndEventsPage/NewsAndEventsPage/NewsAndEventsPage.ftl';

                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(async function (response) {
                    self.content = response.data;
                    var newsEventContent = self.pluck('Background_Image', self.content.area_List);
                    if (newsEventContent.length > 0) {
                        self.newsEventContent.title = self.pluckcom('Title', newsEventContent[0].component);
                        self.newsEventContent.breadcrumb = self.pluckcom('Breadcrumb_Label', newsEventContent[0].component);
                        self.newsEventContent.titleImage = self.pluckcom('Image', newsEventContent[0].component); 
                        self.newsEventContent.button_label = self.pluckcom('News_card_Button_Label', newsEventContent[0].component);
                    }

                    self.getdata = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.newsEventContent = [];
                });


                var pageurlM = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/News and Events/News and Events/News and Events.ftl';

                axios.get(pageurlM, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(async function (response) {
                   self.contents = response.data;
                    var newsCards = self.contents;
                    if (newsCards.Values.length > 0) {
                    
                        self.newsCards=newsCards.Values;
                   
                    }

                    // self.getdata = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.newsCards = [];
                });


            });

        },
        newsMoreInfo: function (link) 
        {
        var self = this;                
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var lang="_"+langauage;
                link=link.replace(lang,'');
                if(link!=null)
                {   
                    var pageurlN = huburl + portno + '/persons/source?path=/'+ link;

                axios.get(pageurlN, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(async function (response) {
                    self.content = response.data;
                     var newsContent = self.pluck('NewsCards', self.content.area_List);
                    if (newsContent.length > 0) {
                        self.Cards.Date = self.pluckcom('Date', newsContent[0].component);
                        self.Cards.Title = self.pluckcom('Title', newsContent[0].component);
                        self.Cards.Image = self.pluckcom('Image', newsContent[0].component);
                        self.Cards.Details = self.pluckcom('Details', newsContent[0].component);
                        self.Cards.More_Info = self.pluckcom('More_Info', newsContent[0].component);
                    }

                }).catch(function (error) {
                    console.log('Error');
                    self.content3 = [];
                });
                }
           
                

        },


    },
    mounted: function () {
        this.getNewsContent();
    //    this.newsMoreInfo("B2B/AdminPanel/CMS/AGY40450/Template/News and Events/News and Events/News and Events_en.ftl");
    },
})