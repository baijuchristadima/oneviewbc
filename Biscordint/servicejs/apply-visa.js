const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
  })
var applyvisa=new Vue({
    i18n,
  el: '#applyvisa',
  name: 'applyvisa',
  data: {
      content: null,
      getdata: true,
      getpagecontent: false,
      selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
      CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
      visaurl:'',
     
      visadet: {},
      visatyp:[],  
      selectedvisa:{},
      visacost:'',
      Header_Information:'',
      number:1,
      totalVisaCost:0,
      VisaCost:0,
      commonarea:{Title:'',Banner_image:'',Breadcrumb_path1:'',Breadcrumb_path2:''}
  },
  methods: {
      pluck(key, contentArry) {
          var Temparry = [];
          contentArry.map(function (item) {
              if (item[key] != undefined) {
                  Temparry.push(item[key]);
              }
          });
          return Temparry;
      },
      pluckcom(key, contentArry) {
          var Temparry = [];
          contentArry.map(function (item) {
              if (item[key] != undefined) {
                  Temparry = item[key];
              }
          });
          return Temparry;
      },
      dropdownChange: function (event, type) {
        
        if (event != undefined && event.target != undefined && event.target.value != undefined) {
  
          
          
        }
        
      },
      getPagecontent: function () {
          var self = this;
          getAgencycode(function (response) {
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var Agencycode = response;
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            
              //  packageurl = getQueryStringValue('page');
            var country = window.localStorage.getItem("countrySelected");
            if (country != "") {
                  // packageurl = packageurl.split('-').join(' ');
                  //  langauage = packageurl.split('_')[1];
                  // packageurl = packageurl.split('_')[0];
                var country = window.localStorage.getItem("countrySelected")
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/EVISA ' + country + '/EVISA ' + country + '/EVISA ' + country +'.ftl';
                var mainurl=  huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/EVISA Page/EVISA Page/EVISA Page.ftl'
               
                  axios.get(topackageurl, {
                      headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                  }).then(function (response) {
                      self.content = response.data;
                    var pagecontent = self.pluck('Page_Settings', self.content.area_List);
                  if (pagecontent.length > 0) {
                    self.visadet.info = self.pluckcom('Visa_Additional_Information', pagecontent[0].component);
                    self.visatyp = self.pluckcom('Visa_Types', pagecontent[0].component);
                    self.selectedvisa=self.visatyp[0];
                    self.VisaCost = self.selectedvisa.Price
                    self.Header_Information = self.pluckcom('Header_Information', pagecontent[0].component);
                  }            
                  self.getdata = false;

                   window.localStorage.removeItem("visaInformation");
                   window.localStorage.removeItem("pgRef");
                   window.localStorage.removeItem("htmlValue");
                }).catch(function (error) {
                  window.location.href="/Biscordint/index.html";
                  self.content = [];
                  self.getdata = true;
              });

              axios.get(mainurl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
                self.content = response.data;
              var pagecontent = self.pluck('Banner_Area', self.content.area_List);
            if (pagecontent.length > 0) {
              self.commonarea.Title = self.pluckcom('Title', pagecontent[0].component);
              self.commonarea.Banner_image = self.pluckcom('Banner_image', pagecontent[0].component);
              self.commonarea.Breadcrumb_path1 = self.pluckcom('Breadcrumb_path1', pagecontent[0].component);
              self.commonarea.Breadcrumb_path2 = self.pluckcom('Breadcrumb_path2', pagecontent[0].component);
              
            }            
            self.getdata = false;

          }).catch(function (error) {
            window.location.href="/Biscordint/index.html";
            self.content = [];
            self.getdata = true;
        });
 
              }
            

            });        
      },
      bookNow:function(response){
        bookurl = getQueryStringValue('page');
              if (bookurl != "") {
                bookurl = bookurl.split('-').join(' ');
                  //  langauage = packageurl.split('_')[1];
                  bookurl = bookurl.split('_')[0];
                  bookurl = "/Biscordint/booknow.html?page=" + bookurl ;
                  window.location.href =bookurl;
                  
              }
       
      },     
      dateFormatter:function(utc){         
        return(moment(utc).utcOffset("+05:30").format("DD MMM YYYY"));    
      },
      getAmount: function (amount) {
        console.log(amount);
        try {
          amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
        } catch (error) {
          
        }
         
        return amount;
      }, 
      decrement: function(){
        //add validation for negative
        if (this.number>1) {
          this.number--;
          this.totalVisaCost = this.number*this.VisaCost;          
        }

      },
      increment: function(){
        //add validation for limit
        this.number++;
        this.totalVisaCost = this.number*this.VisaCost;
      },
      applyNow: function(){
        var visaInformation = {
          numberOfTraveller: this.number,
          visaType: this.selectedvisa.Name,
          totalCost: this.totalVisaCost,
          visaInformation: this.visadet.info
        };
       
        window.localStorage.setItem("visaInformation", btoa(JSON.stringify(visaInformation)));
        window.location = 'apply-visa-form.html'
      }
  },
  mounted: function () {

      this.getPagecontent();
      var vm = this;
    

      this.$nextTick(function () {
        $('#visatyp').on("change", function (e) {
          vm.dropdownChange(e, 'type')
          vm.VisaCost = vm.selectedvisa.Price
          vm.number=1;
        }
        );
      })


  },
  watch: {
    selectedvisa:function(){
      this.totalVisaCost = this.number*this.totalVisaCost;
      this.totalVisaCost = this.selectedvisa.Price
      

    }
  }
  
});

function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));

}
// number of people addtion and decrease function
// $( document ).ready(function() {
 
// });
// var span = document.getElementById("number")
// var span1 = document.getElementById("visacost")
// var number = 1
// function change(dir){{
//   span.innerHTML = dir ? ++number > 9 ? 1 & ( number = 1 ) : number : --number < 1 ? 10 && ( number = 9 ) : number
//   console.log(document.getElementById('visacost').innerHTML); 
//   console.log(document.getElementById('number').innerHTML); 
// }
//   document.getElementById('visacost').innerHTML = document.getElementById('number').innerHTML * document.getElementById('visaorgcost').innerHTML
// }



