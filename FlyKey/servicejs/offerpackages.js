const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
Vue.component('loading', VueLoading)
var offerelist = new Vue({
    i18n,
    el: '#packagepage',
    name: 'packagepage',
    data: {
        content: null,
        getdata: false,
        packages: null,
        getpackage: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        spPackages: null,
        spTitle: '',
        pkgPgBnr: '',
        homebreadcrumb: '',
        offertag:'',
        moredetails:'',
        isLoading: false,
        fullPage: true,
    },
    methods: {
        getPagecontent: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                 
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                //var packagepage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Offer Page/Offer Page/Offer Page.ftl';
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';
                var generalPageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/General Details/General Details/General Details.ftl';
                axios.get(generalPageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = true;
                    if (response.data.area_List.length > 0 && response.data.area_List[0].Offer_Page_Details!=undefined) {
                        self.pkgPgBnr = pluckcom('Banner_Image', response.data.area_List[0].Offer_Page_Details.component);
                        self.spTitle = pluckcom('Title', response.data.area_List[0].Offer_Page_Details.component);
                    }
                    if (response.data.area_List.length > 4 && response.data.area_List[4].Common_Details!=undefined) {
                        self.homebreadcrumb = pluckcom('Home_Breadcrump_Label', response.data.area_List[4].Common_Details.component);
                        self.moredetails = pluckcom('View_Button_Name', response.data.area_List[4].Common_Details.component);
                        self.offertag = pluckcom('Special_Offer_Tag', response.data.area_List[4].Common_Details.component);
                    }
                    self.isLoading = false;
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                    self.isLoading = false;
                });
               

                axios.get(topackageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    var packageData = response.data;
                    

                    let holidayaPackageListTemp=[];
                    if(packageData!=undefined&&packageData.Values!=undefined){
                        holidayaPackageListTemp = packageData.Values.filter(function (el) {
                            return el.Status == true &&
                                    el.Offer_Package == true
                                //     return el.Status == true &&
                                // (new Date(el.To_Date.replace(/-/g, "/"))).getTime() >= Date.now() &&
                                //     el.Offer_Package == true
                        });
                    }
                    self.packages =holidayaPackageListTemp;
                   // self.packages =packageData.Values;
                    self.getpackage = true;
                    
                    self.isLoading = false;

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                    self.isLoading = false;
                });

            });

        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "packageview.html?page=" + url + "&from=offer";
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;
        }
    },
    mounted: function () {
        this.getPagecontent();
    },
});