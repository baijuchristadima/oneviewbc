const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
Vue.component('star-rating', VueStarRating.default);
Vue.component('loading', VueLoading)
var Packageinfo = new Vue({
    i18n,
    el: '#Packageinfo',
    name: 'Packageinfo',
    data: {
        content: null,
        getdata: true,
        pagecontent: null,
        getpagecontent: false,
        isLoading: false,
        fullPage: true,
        gallery: null,
        galleryon: false,
        packagetabs: null,
        locationTabName:"",
        galleryTabName:"",
        reviewTabName:"",
        latitude: '',
        longitude: '',
        packagename: '',
        refNum: '',
        emailConfiguration:{},
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        username: '',
        userfname: '',
        userlname: '',
        Emailid: '',
        contactno: '',
        bookdate: '',
        adtcount: 1,
        chdcount: 0,
        infcount: 0,
        message: '',
        hasCity: false,
        Title: '',
        Backgroundimage: '',
        Home_breadcrumb: '',
        Price: '',
        SpecialOffer: '',
        Booktabletitle: '',
        Bookbutton: '',
        FirstName: '',
        LastName: '',
        Email: '',
        Phone: '',
        Bookdate: '',
        Adult: '',
        Child: '',
        Infant: '',
        Messagelabel: '',
        getmapurl: false,
        Night:'',
        Day:'',
        reviewFormLabels:{},
        Package: {
            Title: '',
            Applicable: '',
            // City: '',
            Destination: '',
            From: '',
            To: '',
            Price: '',
            Days: '',
            Nights: '',
            // Short_description: '',
            Offer_Packages: false,
            Image: '',
            mapurl: null,
        },
        customizeLabel:'',
        Agencynode: '',
        packageUrl:'',
        review:{
            Name:'',
            Email:'',
            Review:'',
            rating:1
        }
    },
    methods: {
        getPagecontent: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                self.Agencynode = Agencycode;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var packageurl = getQueryStringValue('page');
                if (packageurl != "") {
                    packageurl = packageurl.split('-').join(' ');
                    //  langauage = packageurl.split('_')[1];
                    packageurl = packageurl.split('_')[0];
                    var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
                    var pageinfourl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/General Details/General Details/General Details.ftl';
                    axios.get(topackageurl, {
                        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                    }).then(async function (response) {
                        //self.content = response.data;
                        let Package_InfoTemp={};
                        let Booking_InfoTemp={};
                        let Detail_PageTemp={};
                        let Package_SettingTemp={};
                        if(response!=undefined&&response.data!=undefined&&response.data.area_List!=undefined){
                            let areaList=response.data.area_List;
                            if(areaList.length>0 && areaList[0].Package_Info!=undefined){
                                Package_InfoTemp = await self.getAllMapData(areaList[0].Package_Info.component);
                            }
                            if(areaList.length>1 && areaList[1].Booking_Info!=undefined){
                                Booking_InfoTemp = await self.getAllMapData(areaList[1].Booking_Info.component);
                            }
                            if(areaList.length>2 && areaList[2].Detail_Page!=undefined){
                                Detail_PageTemp = await self.getAllMapData(areaList[2].Detail_Page.component);
                            }
                            if(areaList.length>3 && areaList[3].Package_Setting!=undefined){
                                Package_SettingTemp = await self.getAllMapData(areaList[3].Package_Setting.component);
                            }

                        }
                        self.content ={
                            Package_Info:Package_InfoTemp,
                            Booking_Info:Booking_InfoTemp,
                            Detail_Page:Detail_PageTemp,
                            Package_Setting:Package_SettingTemp
                        };
                        self.packagetabs = Detail_PageTemp.Package_Overview;
                        //self.Title = Package_InfoTemp.Name;
                        self.packagename= Package_InfoTemp.Name;
                        self.Package.Applicable =Package_InfoTemp.Applicable_Title;
                        // self.Package.City =Booking_InfoTemp.City;
                        self.Package.Destination=Booking_InfoTemp.Destination;
                        self.Package.From =Booking_InfoTemp.From;
                        self.Package.To =Booking_InfoTemp.To_Date;
                        self.Package.Price =Booking_InfoTemp.Price;
                        self.Package.Days =Booking_InfoTemp.Days;
                        self.Package.Nights =Booking_InfoTemp.Nights;
                        // self.Package.Short_description =Detail_PageTemp.Short_Description;
                        // self.Package.Short_description =Detail_PageTemp.Short_Description;
                        self.Package.mapurl= Detail_PageTemp.Google_Map_Embed_URL;
                        self.gallery = Detail_PageTemp.Gallery;
                        self.Package.Image =Detail_PageTemp.Image;
                        self.Package.Offer_Packages =Package_SettingTemp.Offer_Package;

                        if (self.Package.mapurl != "") {
                            self.getmapurl = true;
                        }
                        if (self.gallery != "") {
                            self.galleryon = true;
                        }

                       
                        self.packageUrl = packageurl;
                        self.getdata = false;
                        self.setclaneder();
                        self.isLoading = false;
                    }).catch(function (error) {
                        window.location.href="package.html";
                        self.content = [];
                        self.getdata = true;
                        self.isLoading = false;
                    });
                    axios.get(pageinfourl, {
                        headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                    }).then(async function (response) {

                        let bookFormData={};
                        let packageInfoData={};
                        let commonInfoData={};
                        var emailConfigurationTempData={};
                        if(response!=undefined&&response.data!=undefined&&response.data.area_List!=undefined){
                            let areaList=response.data.area_List;
                            var review = pluck('Review_Form_Details', areaList);
                            self.reviewFormLabels = await self.getAllMapData(review[0].component);

                            if(areaList.length>0 && areaList[0].Offer_Page_Details!=undefined){
                                let titleDataTemp = await self.getAllMapData(areaList[0].Offer_Page_Details.component);
                                emailConfigurationTempData.offer=titleDataTemp;
                            }
                            if(areaList.length>1 && areaList[1].Packages_Page_Details!=undefined){
                                let titleDataTemp = await self.getAllMapData(areaList[1].Packages_Page_Details.component);
                                emailConfigurationTempData.package=titleDataTemp;
                                self.customizeLabel = pluckcom('Customize_Label', response.data.area_List[1].Packages_Page_Details.component);
                            }

                            if(areaList.length>2 && areaList[2].Package_Info_Page!=undefined){
                                packageInfoData = await self.getAllMapData(areaList[2].Package_Info_Page.component);
                            }
                            if(areaList.length>3 && areaList[3].Book_Form_Details!=undefined){
                                bookFormData = await self.getAllMapData(areaList[3].Book_Form_Details.component);
                            }
                            if(areaList.length>4 && areaList[4].Common_Details!=undefined){
                                commonInfoData = await self.getAllMapData(areaList[4].Common_Details.component);
                            }
                        }
                        self.Day = commonInfoData.Days_Label;
                        self.Night = commonInfoData.Nights_Label
                        self.emailConfiguration=emailConfigurationTempData;
                        self.Title =packageInfoData.Title;
                        self.Backgroundimage =packageInfoData.Banner_Image;
                        self.Home_breadcrumb =commonInfoData.Home_Breadcrump_Label;
                        self.Price =commonInfoData.Price_Label;
                        self.SpecialOffer =commonInfoData.Special_Offer_Tag;
                        self.locationTabName=packageInfoData.Location_Tab_Name;
                        self.galleryTabName=packageInfoData.Gallery_Tab_Name;
                        self.reviewTabName=packageInfoData.Review_Tab_Name;
                        self.Booktabletitle =bookFormData.Title;
                        self.Bookbutton =bookFormData.Book_Button_Name;
                        self.FirstName =bookFormData.First_Name_Place_Holder;
                        self.LastName =bookFormData.Last_Name_Place_Holder;
                        self.Email =bookFormData.Email_Place_Holder;
                        self.Phone =bookFormData.Phone_Place_Holder;
                        self.Bookdate =bookFormData.Date_Place_Holder;
                        self.Adult =bookFormData.Adult_Place_Holder;
                        self.Child =bookFormData.Child_Place_Holder;
                        self.Infant =bookFormData.Infant_Place_Holder;
                        self.Messagelabel =bookFormData.Message_Place_Holder;
                        self.getpagecontent = true
                        self.pagecontent = response.data;
                     
                        self.isLoading = false;
                    }).catch(function (error) {
                        console.log('Error');
                        self.pagecontent = [];
                        self.isLoading = false;
                    });

                }


            });
        },async getAllMapData(contentArry){
            var tempDataObject = {};
            if(contentArry!=undefined){
                contentArry.map(function (item) {
                    let allKeys=Object.keys(item)
                    for(let j=0;j<allKeys.length;j++){
                        let key=allKeys[j];
                        let value= item[key];
                        
                        if(key!='name' && key!='type'){
                            if(value==undefined||value==null){
                                value="";
                            }
                            tempDataObject[key]=value;
                        }
                    }
                });
            }
            return tempDataObject;
        },async getEmailObject(configureObject){
            let returnObject=undefined;
            if(configureObject!=undefined){
                returnObject=configureObject;
            }
            return returnObject;
        },
        async bookpackage () {
            if (!this.userfname.trim()) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M034')).set('closable', false);

                return false;
            }
            if (!this.userlname.trim()) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M035')).set('closable', false);

                return false;
            }
            if (!this.Emailid.trim()) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M026')).set('closable', false);

                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.Emailid.match(emailPat);
            if (matchArray == null) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M027')).set('closable', false);

                return false;
            }
            if (!this.contactno.trim()) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M030')).set('closable', false);

                return false;
            }
            if (this.contactno.length < 8) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M031')).set('closable', false);

                return false;
            }
            this.bookdate = $('#traveldate').val();
            if (!this.bookdate) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M036')).set('closable', false);

                return false;
            }
            if (!this.message) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M008')).set('closable', false);

                return false;
            }
            else {
                self.isLoading = true;
                this.username = this.userfname + " " + this.userlname;
                var fromPage = getQueryStringValue('from');
                var fromEmail = JSON.parse(localStorage.User).loginNode.email;
                var toEmail = { To_Email: '' };

                let configureObject=undefined;
                switch (fromPage.toLocaleLowerCase()) {
                    case 'pkg':
                        configureObject=await this.getEmailObject(this.emailConfiguration.package);
                        break;
                    case 'offer':
                        configureObject=await this.getEmailObject(this.emailConfiguration.offer);
                    break;
                    default:
                        configureObject={To_Email:toEmail};
                        break;
                }
                let agencyAddress="";
                var cc=[];
                let cusMessage="Thank you for the request, your request is being reviewed our sales team will soon revert with the status.";
                if(configureObject!=undefined && configureObject.To_Email!=undefined){
                    toEmail=configureObject.To_Email;
                }
                if(configureObject!=undefined && configureObject.CC!=undefined){
                    cc=configureObject.CC;
                }
                if(configureObject!=undefined && configureObject.Customer_Message!=undefined){
                    cusMessage=configureObject.Customer_Message;
                }
                
                if(JSON.parse(localStorage.User)!=undefined&&JSON.parse(localStorage.User).loginNode!=undefined&&
                    JSON.parse(localStorage.User).loginNode.address!=undefined){
                    agencyAddress=JSON.parse(localStorage.User).loginNode.address;
                }
                var postData = {
                    type: "PackageBookingRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(toEmail.To_Email) ? toEmail.To_Email : [toEmail.To_Email],
                    ccEmails:cc,
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + '.xhtml?ln=logo',
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: agencyAddress || "",
                    packegeName: this.packagename,
                    personName: this.username,
                    emailAddress: this.Emailid,
                    contact: this.contactno,
                    departureDate: this.bookdate,
                    adults: this.adtcount,
                    child2to5: this.chdcount,
                    child6to11: "0",
                    infants: this.infcount,
                    message: this.message,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                var custmail = {
                    type: "ThankYouRequest",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(this.Emailid) ? this.Emailid : [this.Emailid],
                    logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + '.xhtml?ln=logo',
                    agencyName: JSON.parse(localStorage.User).loginNode.name,
                    agencyAddress: agencyAddress || "",
                    personName: this.username,
                    message:cusMessage,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };
                var traveldate = $('#traveldate').val() == "" ? "" : $('#traveldate').datepicker('getDate');
                let toDateVal = await moment(traveldate).format('YYYY-MM-DDThh:mm:ss');
                let requestedDate = await moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let requestObject = {
                    type: "Package Booking",
                    nodeCode: this.Agencynode,
                    keyword1: this.packagename,
                    keyword2: this.userfname + " " + this.userlname,
                    keyword3: this.Emailid,
                    keyword4: this.contactno,
                    date1: toDateVal,
                    date2: requestedDate,
                    number1: this.adtcount,
                    number2: this.chdcount,
                    number3: this.infcount,
                    text1:this.message
                };
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var cmsURL = huburl + portno + '/cms/data';      
                
                let responseObject = await this.cmsRequestData("POST", "cms/data", requestObject, null);
                var emailApi = ServiceUrls.emailServices.emailApi;
                sendMailService(emailApi, postData);
                sendMailService(emailApi, custmail);
                self.isLoading = false;
                alertify.alert(this.getValidationMsgByCode('M023'), this.getValidationMsgByCode('M033')).set('closable', false);
                this.clearValues();
            }



        },
        setclaneder: function () {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            $("#traveldate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    // $(this).parents('.txt_animation').addClass('focused');
                }
            });
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        getValidationMsgByCode: function (code) {
            if (sessionStorage.validationItems !== undefined) {
              var validationList = JSON.parse(sessionStorage.validationItems);
              for (let validationItem of validationList.Validation_List) {
                if (code === validationItem.Code) {
                  return validationItem.Message;
                }
              }
            }
          },
        clearValues() {
        this.username = '';
        this.userfname =  '';
        this.userlname =  '';
        this.Emailid =  '';
        this.contactno =  '';
        this.bookdate =  '';
        $('#traveldate').val('');
        this.adtcount =  1;
        this.chdcount =  0;
        this.infcount =  0;
        this.message='';
        },
        customizePackage(url) {
            if (url != null) {
                if (url != "") {
                    url = "detail-request.html?page=" + url + "&from=pkg";
                    window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;
        },
        addReviews: async function () {
            var self = this;
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.review.Email.match(emailPat);
            if (this.review.Name == undefined || this.review.Name == '') {
              alertify.alert(getValidationMsgByCode('M022'), getValidationMsgByCode('M025')).set('closable', false);
              return;
            } else if (this.review.Email == undefined || this.review.Email == '') {
              alertify.alert(getValidationMsgByCode('M022'), getValidationMsgByCode('M026')).set('closable', false);
              return;
      
            } else if (matchArray == null) {
                alertify.alert(getValidationMsgByCode('M022'), getValidationMsgByCode('M027')).set('closable', false);
              return false;
            } else if (this.review.Review == undefined || this.review.Review == '') {
              alertify.alert(getValidationMsgByCode('M022'), 'Review required').set('closable', false);
              return;
            } else {
              this.isLoading = true;
              var frommail = JSON.parse(localStorage.User).loginNode.email;
              var custmail = {
                type: "UserAddedRequest",
                fromEmail: frommail,
                toEmails: Array.isArray(this.review.Email) ? this.review.Email : [this.review.Email],
                logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                personName: this.review.Name,
                primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
              };
              let agencyCode = JSON.parse(localStorage.User).loginNode.code;
              let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
              let insertContactData = {
                type: "Package Review",
                keyword4: self.packageUrl,
                keyword1: self.packagename,
                keyword2: self.review.Name,
                keyword3: self.review.Email,
                text1: self.review.Review,
                number1: self.review.rating,
                date1: requestedDate,
                nodeCode: agencyCode
              };
              let responseObject = await cmsRequestData("POST", "cms/data", insertContactData, null);
              try {
                let insertID = Number(responseObject);
                var emailApi = ServiceUrls.emailServices.emailApi;
                sendMailService(emailApi, custmail);
                this.review.email = '';
                this.review.username = '';
                this.review.comment = '';
                // this.viewReview();
                alertify.alert(getValidationMsgByCode('M023'), getValidationMsgByCode('M009')).set('closable', false);
                this.isLoading = false;
              } catch (e) {
                this.isLoading = false;
              }
            }
          },
    },
    mounted: function () {

        this.getPagecontent();
        var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
        $("#traveldate").datepicker({
            minDate: "0d",
            maxDate: "360d",
            numberOfMonths: 1,
            changeMonth: true,
            showButtonPanel: false,
            dateFormat: systemDateFormat,
            onSelect: function (selectedDate) {
                // $(this).parents('.txt_animation').addClass('focused');
            }
        });


    },
    updated: function () {
        var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
        var self = this;
        if (!this.bookdate) {
            $("#traveldate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    self.bookdate = selectedDate;
                }
            });
        }
    }
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}


