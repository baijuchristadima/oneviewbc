const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
Vue.component('loading', VueLoading)
var contactus = new Vue({
    i18n,
    el: '#contactuspage',
    name: 'contactuspage',
    data: {
        content: null,
        getdata: false,
        offerdata: false,
        isLoading: false,
        fullPage: true,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        pageContent: {
            Title: '',
            Home: '',
            Background: '',
            Subtitle: '',
            Description: '',
            mapurl: '',
            NamePlaceholder: '',
            EmailPlaceholder: '',
            PhonePlaceholder: '',
            SubjectPlaceholder: '',
            MessagePlaceholder: '',
            Button: ''
        },
        branch: {
            Address: '',
            Phone: '',
            Email: '',
            Facebook: '',
            Twitter: '',
            Linkedin: '',
            Instagtram: '',
            Addresslabel: '',
            Phonelabel: '',
            Emaillabel: ''
        },
        offerContent: {
            title: '',
            Subtitle: '',
            description: '',
            link: '',
            button: '',
            offerImage:''
        },
        cntusername: '',
        cntemail: '',
        cntcontact: '',
        cntmessage: '',
        cntsubject:''
    },
    methods: {
        getPagecontent: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact us/Contact us/Contact us.ftl';

                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var pagecontent = pluck('Contact_us', self.content.area_List);
                    if (pagecontent.length > 0) {
                        self.pageContent.Title = pluckcom('Title', pagecontent[0].component);
                        self.pageContent.Home = pluckcom('Home_breadcrumb_label', pagecontent[0].component);
                        self.pageContent.Background = pluckcom('Background_image', pagecontent[0].component);
                        self.pageContent.Subtitle = pluckcom('Subtitle', pagecontent[0].component);
                        self.pageContent.Description = pluckcom('Description', pagecontent[0].component);
                        self.pageContent.mapurl = pluckcom('Google_map_embed_url', pagecontent[0].component);
                        self.pageContent.NamePlaceholder = pluckcom('Name_placeholder', pagecontent[0].component);
                        self.pageContent.EmailPlaceholder = pluckcom('Email_placeholder', pagecontent[0].component);
                        self.pageContent.PhonePlaceholder = pluckcom('Phone_placeholder', pagecontent[0].component);
                        self.pageContent.SubjectPlaceholder = pluckcom('Subject_placeholder', pagecontent[0].component);
                        self.pageContent.MessagePlaceholder = pluckcom('Message_placeholder', pagecontent[0].component);
                        self.pageContent.Button = pluckcom('Button_label', pagecontent[0].component);
                    }
                    var branch = pluck('Branch', self.content.area_List);
                    if (branch.length > 0) {
                        self.branch.Address = pluckcom('Address', branch[0].component);
                        self.branch.Phone = pluckcom('Phone_number', branch[0].component);
                        self.branch.Email = pluckcom('Email_address', branch[0].component);
                        self.branch.Facebook = pluckcom('Facebook_link', branch[0].component);
                        self.branch.Twitter = pluckcom('Twitter_link', branch[0].component);
                        self.branch.Linkedin = pluckcom('Linkedin_link', branch[0].component);
                        self.branch.Instagtram = pluckcom('Instagtram_link', branch[0].component);
                        self.branch.WhatsApp = pluckcom('Whatsapp_Link', branch[0].component);
                        self.branch.Addresslabel = pluckcom('Address_label', branch[0].component);
                        self.branch.Phonelabel = pluckcom('Phone_label', branch[0].component);
                        self.branch.Emaillabel = pluckcom('Email_label', branch[0].component);
                    }
                    var Offerdata = pluck('Offer_section', self.content.area_List);
                    if (Offerdata.length > 0) {
                        self.offerdata = pluckcom('Status', Offerdata[0].component);
                        self.offerContent.title = pluckcom('Title', Offerdata[0].component);
                        self.offerContent.Subtitle = pluckcom('Subtitle', Offerdata[0].component);
                        self.offerContent.description = pluckcom('Description', Offerdata[0].component);
                        self.offerContent.link = pluckcom('Button_link', Offerdata[0].component);
                        self.offerContent.button = pluckcom('Button_label', Offerdata[0].component);
                        self.offerContent.offerImage = pluckcom('offer_Image', Offerdata[0].component);


                    }

                    self.getdata = true;
                    self.isLoading = false;
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                    self.isLoading = false;
                });


            });

        },
        sendcontactus: async function () {
            if (!this.cntusername.trim()) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M025')).set('closable', false);

                return false;
            }
            if (!this.cntemail.trim()) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M026')).set('closable', false);

                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M027')).set('closable', false);

                return false;
            }
            if (!this.cntcontact.trim()) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M030')).set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M031')).set('closable', false);
                return false;
            }
            if (!this.cntsubject.trim()) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M032')).set('closable', false);
                return false;
            }

            if (!this.cntmessage.trim()) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M008')).set('closable', false);

                return false;
            } else {
                this.isLoading = true;
                var toEmail = JSON.parse(localStorage.User).loginNode.email;
                var frommail=JSON.parse(localStorage.User).loginNode.email;   
                var postData = {
                    type: "AdminContactUsRequest",
                    fromEmail:frommail,
                    toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl +JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.cntusername,
                    emailId: this.cntemail,
                    contact: this.cntcontact,
                    message: this.cntmessage,

                };
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                    logo: ServiceUrls.hubConnection.logoBaseUrl +JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: this.cntusername,
                    primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                };


                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Contact Us",
                    keyword1: this.cntusername,
                    keyword2: this.cntemail,
                    keyword3: this.cntcontact,
                    keyword4: this.cntsubject,
                    text1: this.cntmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    sendMailService(emailApi, postData);
                    sendMailService(emailApi, custmail);
                    this.cntemail = '';
                    this.cntusername = '';
                    this.cntcontact = '';
                    this.cntmessage = '';
                    this.cntsubject = '';
                    this.isLoading = false;
                    alertify.alert(this.getValidationMsgByCode('M023'), this.getValidationMsgByCode('M029')).set('closable', false);

                } catch (e) {
                    this.isLoading = false;
                }



            }
        },
        getValidationMsgByCode: function (code) {
            if (sessionStorage.validationItems !== undefined) {
              var validationList = JSON.parse(sessionStorage.validationItems);
              for (let validationItem of validationList.Validation_List) {
                if (code === validationItem.Code) {
                  return validationItem.Message;
                }
              }
            }
          }
    },
    mounted: function () {
        this.getPagecontent();
    },
});