Vue.component('loading', VueLoading)
var packagelist = new Vue({
    el: '#privacyPolicy',
    name: 'privacyPolicy',
    data: {
        privacyPageDetails:{},
        isLoading: false,
        fullPage: true,
    },
    methods: {
        getPagecontent: function() {
            var self = this;
            self.isLoading = true;
            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Privacy Policy/Privacy Policy/Privacy Policy.ftl';
                axios.get(aboutUsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                        if(response.data.area_List[0].Privacy_Policy!=undefined){
                            var titleDataTemp = getAllMapData(response.data.area_List[0].Privacy_Policy.component);
                            self.privacyPageDetails=titleDataTemp;
                        }
                        self.isLoading = false;
                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });

        }
    },
    mounted: function() {
        this.getPagecontent(); 
    }
});

