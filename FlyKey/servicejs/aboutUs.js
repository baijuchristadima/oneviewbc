const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
Vue.component('loading', VueLoading)
var aboutus = new Vue({
    i18n,
    el: '#aboutuspage',
    name: 'aboutuspage',
    data: {
        content: null,
        getdata: false,
        offerdata: false,   
        isLoading: false,
        fullPage: true,    
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        aboutUsContent: {
            Title: '',
            Home: '',
            Subtitle1: '',
            Description1: '',
            Subtitle2: '',
            Description2: '',
            Image: '',
            Subtitle3: '',
            Description3: '',
            Background: ''
        },
        offerContent: {
            title: '',
            Subtitle: '',
            description: '',
            link: '',
            button: '',
            offerImage: ''
        }
    },
    methods: {
        getPagecontent: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/About Us page/About Us page/About Us page.ftl';

                axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(async function (response) {
                    self.content = response.data;
                    var Aboutcontent = pluck('About_us', self.content.area_List);
                    if (Aboutcontent.length > 0) {
                        self.aboutUsContent.Title = pluckcom('Title', Aboutcontent[0].component);
                        self.aboutUsContent.Home = pluckcom('Home_breadcrumb_label', Aboutcontent[0].component);
                        self.aboutUsContent.Subtitle1 = pluckcom('Subtitle_1', Aboutcontent[0].component);
                        self.aboutUsContent.Description1 = pluckcom('Description1', Aboutcontent[0].component);
                        self.aboutUsContent.Subtitle2 = pluckcom('Subtitle2', Aboutcontent[0].component);
                        self.aboutUsContent.Description2 = pluckcom('Description2', Aboutcontent[0].component);
                        self.aboutUsContent.Image = pluckcom('Image', Aboutcontent[0].component);
                        self.aboutUsContent.Subtitle3 = pluckcom('Subtitle3', Aboutcontent[0].component);
                        self.aboutUsContent.Description3 = pluckcom('Description3', Aboutcontent[0].component);
                        self.aboutUsContent.Background = pluckcom('Background_image', Aboutcontent[0].component);
                    }
                    var Offerdata = pluck('Offer_section', self.content.area_List);
                    if (Offerdata.length > 0) {
                        self.offerdata=pluckcom('Status', Offerdata[0].component);
                        self.offerContent.title = pluckcom('Title', Offerdata[0].component);
                        self.offerContent.Subtitle = pluckcom('Subtitle', Offerdata[0].component);
                        self.offerContent.description = pluckcom('Description', Offerdata[0].component);
                        self.offerContent.link = pluckcom('Button_link', Offerdata[0].component);
                        self.offerContent.button = pluckcom('Button_label', Offerdata[0].component);
                        self.offerContent.offerImage = pluckcom('Offer_Image', Offerdata[0].component);
                        
                        


                    }
                    self.isLoading = false;
                    self.getdata = true;

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                    self.isLoading = false;
                });


            });

        }
    },
    mounted: function () {
        this.getPagecontent();
    },
});