const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
Vue.component('loading', VueLoading)
var packagelist = new Vue({
    i18n,
    el: '#packagepage',
    name: 'packagepage',
    data: {
        content: null,
        getdata: false,
        packages: null,
        getpackage: false,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        spPackages: null,
        spTitle: '',
        pkgPgBnr: '',
        tourLabel: '',
        tripTitle: '', 
        tripButtonLabel: '',
        customizeLabel: '', 
        homebreadcrumb: '',
        offertag:'',
        moredetails:'',
        isLoading: false,
        fullPage: true,

        countryBanner:'',
        countryCode:'',
        filteredList:[],
        countryName:''
    },
    methods: {
        getPagecontent: function () {
            var self = this;
            self.isLoading = true;
            getAgencycode(function (response) {
                 
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
               // var packagepage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Packages/Packages/Packages.ftl';
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';
                var generalPageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/General Details/General Details/General Details.ftl';

                axios.get(generalPageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    self.getdata = true;
                    if (response.data.area_List.length > 1 && response.data.area_List[1].Packages_Page_Details!=undefined) {
                        self.pkgPgBnr = pluckcom('Banner_Image', response.data.area_List[1].Packages_Page_Details.component);
                        self.spTitle = pluckcom('Title', response.data.area_List[1].Packages_Page_Details.component);
                        self.tourLabel = pluckcom('Tours_Label', response.data.area_List[1].Packages_Page_Details.component);
                        self.tripTitle = pluckcom('Design_Trip_Title', response.data.area_List[1].Packages_Page_Details.component);
                        self.tripButtonLabel = pluckcom('Design_Trip_Button_Label', response.data.area_List[1].Packages_Page_Details.component);
                        self.customizeLabel = pluckcom('Customize_Label', response.data.area_List[1].Packages_Page_Details.component);
                    }
                    if (response.data.area_List.length > 4 && response.data.area_List[4].Common_Details!=undefined) {
                        self.homebreadcrumb = pluckcom('Home_Breadcrump_Label', response.data.area_List[4].Common_Details.component);
                        self.moredetails =pluckcom('View_Button_Name', response.data.area_List[4].Common_Details.component);
                        self.offertag = pluckcom('Special_Offer_Tag', response.data.area_List[4].Common_Details.component);
                    }
                    self.isLoading = false;
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                    self.isLoading = false;
                });
                axios.get(topackageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    var packageData = response.data;
                    let holidayaPackageListTemp=[];
                    if(packageData!=undefined&&packageData.Values!=undefined){
                        holidayaPackageListTemp = packageData.Values.filter(function (el) {
                          return el.Status == true && el.Holiday_Package == true
                            // return el.Status == true &&
                            //     (new Date(el.To_Date.replace(/-/g, "/"))).getTime() >= Date.now() &&
                            //         el.Holiday_Package == true
                        });
                    }
                   self.packages =holidayaPackageListTemp;
                   // self.packages =packageData.Values;
                    self.getpackage = true;
                   
                    self.isLoading = false;
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                    self.isLoading = false;
                });

            });

        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "packageview.html?page=" + url + "&from=pkg";
                    window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;
        },
        getPackage: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var Language = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';

                var searchData = self.getSearchData();
                if (searchData != "" || searchData != null) {
                    self.searchCriteria = searchData;
                    var countrycode = searchData.countrycode;
                    self.countryCode = countrycode;
                    self.countryBanner = searchData.img;
                    self.countryName = searchData.CName;
                    axios.get(topackageurl, {
                        headers: {
                            'content-type': 'text/html',
                            'Accept': 'text/html',
                            'Accept-Language': Language
                        }
                    }).then(function (response) {
                        var packageData = [];
                        var packageData = response.data;
                        if (packageData != undefined && packageData.Values != undefined) {
                            self.Packages = packageData.Values;
                            tempfilteredList = self.Packages.filter(function (x) {
                                if(x.Country_Code){
                                    return x.Country_Code.toLowerCase() ==  self.countryCode.toLowerCase();
                                }
                                
                            });
                            self.filteredList = tempfilteredList;
                        }
                    }).catch(function (error) {
                        console.log('Error', error);
                        self.filteredList = self.packages;
                        self.pageLoad = false;
                    });
                }
            });
        },
        getSearchData: function () {
            var urldata = this.getUrlVars();
            return urldata;
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            let decodeUrl= decodeURIComponent(window.location.href);
            var hashes = decodeUrl.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        getMoreDetails(name,id) {
            if (name != null && id != null) {
                if (name != "" && id !="") {
                    url = "detail-request.html?id=" + id + "&name=" +name;
                    window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;
        },
        customizePackage(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "detail-request.html?page=" + url + "&from=pkg";
                    window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;
        }
    },
    mounted: function () {
        this.getPagecontent();
        this.getPackage();
    },
});