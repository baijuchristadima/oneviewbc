Vue.component('loading', VueLoading)
var packagelist = new Vue({
    el: '#termsAndCondition',
    name: 'termsAndCondition',
    data: {
        termsPageDetails:{},
        commonPageDetails:{},
        isLoading: false,
        fullPage: true,
    },
    methods: {
       getPagecontent: function() {
            var self = this;
            self.isLoading = true;
            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Terms and Conditions/Terms and Conditions/Terms and Conditions.ftl';
                axios.get(aboutUsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                        if(response.data.area_List[0].Terms_and_Conditions!=undefined){
                            var titleDataTemp = getAllMapData(response.data.area_List[0].Terms_and_Conditions.component);
                            self.termsPageDetails=titleDataTemp;
                            console.log(titleDataTemp);
                        }
                        self.isLoading = false;
                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.isLoading = false;
                });
            });

        }
    },
    mounted: function() {  
        this.getPagecontent();
    }
});

