Vue.component('loading', VueLoading);
Vue.component('star-rating', VueStarRating.default);
var flightserchfromComponent = Vue.component('flightserch', {
  data() {
      return {
          access_token: '',
          KeywordSearch: '',
          resultItemsarr: [],
          autoCompleteProgress: false,
          highlightIndex: 0
      }
  },
  props: {
      itemText: String,
      itemId: String,
      placeHolderText: String,
      returnValue: Boolean,
      id: { type: String, default: '', required: false },
      leg: Number,
      //KeywordSearch:String
  },

  template: `<div class="autocomplete">
  <span class="clearable">
      <input type="text" :placeholder="placeHolderText" :id="id" :data-aircode="KeywordSearch" autocomplete="off" 
      v-model="KeywordSearch" class="form-control" 
      :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !autoCompleteProgress  }" 
      @input="onSelectedAutoCompleteEvent($event)"
          @keydown.down="down"
          @keydown.up="up"           
          @keydown.esc="autoCompleteProgress=false"
          @keydown.enter="onSelected(resultItemsarr[highlightIndex])"
          @keydown.tab="tabclick(resultItemsarr[highlightIndex])"/>
          <i :class="'clearable__clear'+id" class="clearable__clear">&times;</i></span>
      <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>        
      <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length>0">
          <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item)">
              {{ item.label }}
          </li>
      </ul>
  </div>`,

  mounted: function() {
      setTimeout(() => {
          $(".clearable").each(function() {
          
          const $inp = $(this).find("input:text"),
              $cle = $(this).find(".clearable__clear");

          $inp.on("input", function(){
              $cle.toggle(!!this.value);
          });
          
          $cle.on("touchstart click", function(e) {
              e.preventDefault();
              $inp.val("").trigger("input");
          });
          
          });
      }, 100);
  },
  methods: {
      up: function () {
          if (this.autoCompleteProgress) {
              if (this.highlightIndex > 0) {
                  this.highlightIndex--
              }
          } else {
              this.autoCompleteProgress = true;
          }
          this.fixScrolling();
      },

      down: function () {
          if (this.autoCompleteProgress) {
              if (this.highlightIndex < this.resultItemsarr.length - 1) {
                  this.highlightIndex++
              } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
                  this.highlightIndex = 0;
              }
          } else {
              this.autoCompleteProgress = true;
          }
          this.fixScrolling();
      },
      fixScrolling: function () {
          if (this.$refs.options[this.highlightIndex]) {
              var liH = this.$refs.options[this.highlightIndex].clientHeight;
              if (liH == 50) {
                  liH = 32;
              }
              if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                  this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
              }
          }

      },
      onSelectedAutoCompleteEvent: _.debounce(async function (event) {
          var self = this;
          var keywordEntered = event.target.value;
          if (keywordEntered.length > 2) {
              this.autoCompleteProgress = true;
              self.resultItemsarr = await getFlightResultUsingElasticSearch(keywordEntered);

          } else {
              this.autoCompleteProgress = false;
              this.resultItemsarr = [];

          }
      }, 100),
      onSelected: function (item) {
          this.KeywordSearch = item.label;
          this.resultItemsarr = [];
          var targetWhenClicked = $(event.target).parent().parent().find("input").attr("id");
          if (event.target.id == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
              $('#Cityto1').focus();
          } 
          this.$emit('air-search-completed', item.code, item.label, this.leg);


      },
      tabclick: function (item) {
          if (!item) {

          } else {
              this.onSelected(item);
          }
      }

  },
  watch: {
      returnValue: function () {
          this.KeywordSearch = this.itemText;
      }

  }

});
var detailrequest = new Vue({
  el: '#detailrequest',
  name: 'detailrequest',
  data: {
    configDetails: '',
    mainContent: {},
    detailInfo: {},
    regionList: '',
    selectedRegion: [],
    selectedExperiance: [],
    selectedTravel: [],
    selectedType: [],
    selectedAdult: 1,
    selectedChildren: 0,
    selectedInfants: 0,
    tripLength: 1,
    countryid: '',
    countryname: '',
    commonData: {},
    bannerData: {},
    packageurl:'',
    allComments:[],
    isLoading: false,
    fullPage: true,
    // form
    fname: '',
    lname: '',
    email: '',
    countryCode: '+91',
    phonenumber: '',
    age: '',
    info: '',
    budget: '',
    addinfo: '',
    flightInfo: '',
    signup: false,
    offer: false,
    fromDate:'',
    flightInfoCheck:false,
    video:"",
    additional_adult: 1,
    additional_child: 0,
    additional_infant: 0,
    additional_child_age: [
      { age: 2 },
      { age: 2 },
      { age: 2 },
      { age: 2 },
      { age: 2 },
      { age: 2 },
      { age: 2 },
      { age: 2 },
      { age: 2 },
      { age: 2 },
    ],
    depAirport: "",
    arrAirport: "",
    internationalFlightDetails: ""
  },
  mounted() {
    this.getPageContent();
    this.setclaneder();
    var vm = this;
    this.$nextTick(function () {
      $('#traveldate').on("change", function (e) {
          vm.dropdownChange(e, 'traveldate')
      });
  });
  setTimeout(function () {
    vm.readReview();
  }, 1000);

  },
  watch:{
        flightInfoCheck(){
            this.flightInfo = "";
        }
    },
  methods: {
    getPageContent: function () {
      var self = this;
      self.isLoading = true;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var cmsURL = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Detail Request/Detail Request/Detail Request.ftl';
        var packageurl = getQueryStringValue('page');
        self.countryid = getQueryStringValue('id');
        self.countryname = getQueryStringValue('name');
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        if (packageurl != "") {
          packageurl = packageurl.split('-').join(' ');
          packageurl = packageurl.split('_')[0];
          var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
          self.pageURLLink = packageurl;
          axios.get(topackageurl, {
            headers: {
              'content-type': 'text/html',
              'Accept': 'text/html',
              'Accept-Language': langauage
            }
          }).then(function (response) {
            self.content = response.data;
            var main = pluck('Package_Info', self.content.area_List);
            if (main != undefined) {
              self.mainContent = getAllMapData(main[0].component);
            }
            var detail = pluck('Detail_Page', self.content.area_List);
            if (detail != undefined) {
              self.detailInfo = getAllMapData(detail[0].component);
            }
            var selected = pluck('Selected_Section', self.content.area_List);
            if (selected != undefined) {
              var list = getAllMapData(selected[0].component);
            }
            self.packageurl = packageurl;
            const selectedRegionList = list.Selected_Region_List;
            const selectedExperiencesList = list.Selected_Experience_List;
            const travelStyleList = list.Selected_Travel_Style_List;
            const typeList = list.Selected_Accommodation_Type_List;

            self.getPackageconfig(selectedRegionList, selectedExperiencesList, travelStyleList, typeList);
          }).catch(function (error) {
            console.log('Error');
          })
        } else {
          self.getPackageconfig();
        }
        axios.get(cmsURL, {
          headers: {
            'content-type': 'text/html',
            'Accept': 'text/html',
            'Accept-Language': langauage
          }
        }).then(function (response) {
          if (response.data.area_List.length) {
            var main = pluck('Main_Area', response.data.area_List);
            self.commonData = getAllMapData(main[0].component);
            var banner = pluck('Banner_Area', response.data.area_List);
            self.bannerData = getAllMapData(banner[0].component);
          }
          self.isLoading = false;
        }).catch(function (error) {
          console.log('Error');
          self.isLoading = false;
        })
      });
    },
    getPackageconfig: function (selectedRegionList, selectedExperiencesList, travelStyleList, typeList) {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        var homecms = commonPath + Agencycode + '/Template/Package Configuration/Package Configuration/Package Configuration.ftl';
        var cmsurl = huburl + portno + homecms;
        axios.get(cmsurl, {
          headers: {
            'content-type': 'text/html',
            'Accept': 'text/html',
            'Accept-Language': langauage
          }
        }).then(function (res) {
          console.log(res);
          if (res.data.area_List.length) {
            self.content = res.data.area_List;
            var main = pluck('Configurations', self.content);
            self.configDetails = getAllMapData(main[0].component);
            if (!self.countryid) {
              self.regionList = self.configDetails.Region_List.filter(function (el) {
                return el.Country_Code.toLowerCase() == self.mainContent.Country_Code.toLowerCase();
              })
              self.regionList.forEach(function (arrayItem) {
                if (arrayItem != undefined && arrayItem != null) {
                  for (var i in selectedRegionList) {
                    if (selectedRegionList[i].Id == arrayItem.Id) {
                      arrayItem.isSelected = true;
                      self.selectedRegion.push(arrayItem);
                    }
                  }
                }
              });
              if (selectedExperiencesList) {
                self.configDetails.Experiences_List.forEach(function (arrayItem) {
                  if (arrayItem != undefined && arrayItem != null) {
                    for (var i in selectedExperiencesList) {
                      if (selectedExperiencesList[i].Id == arrayItem.Id) {
                        arrayItem.isSelected = true;
                        self.selectedExperiance.push(arrayItem);
                      }
                    }
                  }
                });
              }
              if (travelStyleList) {
                self.configDetails.Travel_Style.forEach(function (arrayItem) {
                  if (arrayItem != undefined && arrayItem != null) {
                    for (var i in travelStyleList) {
                      if (travelStyleList[i].Id == arrayItem.Id) {
                        if(self.selectedTravel.length < 1){
                          arrayItem.isSelected = true;
                          self.selectedTravel.push(arrayItem);
                        }
                      }
                    }
                  }
                });
              }
              if (typeList) {
                self.configDetails.Accommodation_Type_List.forEach(function (arrayItem) {
                  if (arrayItem != undefined && arrayItem != null) {
                    for (var i in typeList) {
                      if (typeList[i].Id == arrayItem.Id) {
                        arrayItem.isSelected = true;
                        self.selectedType.push(arrayItem);
                      }
                    }
                  }
                });
              }
              self.video = self.configDetails.Video.filter(function (el) {
                return el.Country_Code.toLowerCase() == self.mainContent.Country_Code.toLowerCase();
              })
            } else {
              self.regionList = self.configDetails.Region_List.filter(function (el) {
                return el.Country_Code.toLowerCase() == self.countryid.toLowerCase();
              })
              self.configDetails.Country_List.filter(function (el) {
                if (el.Country_Code == self.countryid) {
                  self.mainContent.Country_Name = el.Country_Name;
                  self.detailInfo.Image = el.Country_Banner_Image_1934_x_560px;
                }
              })
              self.video = self.configDetails.Video.filter(function (el) {
                return el.Country_Code.toLowerCase() == self.countryid.toLowerCase()
              })
            }
            // self.constructSelectedRegion(self.regionList)
            // var tempRegionList =  self.configDetails.Region_List
            // regionList = tempRegionList.filter(function (el) {
            //     return el.Status == true
            // });
            // console.log('RegionList',regionList)
            // self.experiencesList =  self.configDetails.Experiences_List;
            // self.travelStyle = self.Travel_Style;
          }
          self.isLoading = false;
        }).catch(function (error) {
          console.log('Error', error);
          self.isLoading = false;
          // self.content = [];
        });
      });
    },
    selectRegion: function (item) {
      var self = this;
      let index = self.selectedRegion.findIndex(a => a === item)
      if (index >= 0) {
        self.selectedRegion.splice(index, 1)
      } else {
        self.selectedRegion.push(item)
      }
      // self.selectedRegion = [...self.selectedRegion];

    },
    selectExperiences: function (item) {
      var self = this;
      let index = self.selectedExperiance.findIndex(a => a === item)
      if (index >= 0) {
        self.selectedExperiance.splice(index, 1)
      } else {
        self.selectedExperiance.push(item)
      }

    },
    selectTravel: function (item) {
      var self = this;
      if(self.selectedTravel){
      for (var i in  self.selectedTravel) {
        if (self.selectedTravel[i].isSelected == true) {
           self.selectedTravel[i].isSelected = false;
        }
      }
    }
    if(item.isSelected ==false){
      item.isSelected =true;
    }
      // let index = self.selectedTravel.findIndex(a => a === item)
      // if (index >= 0) {
      //   self.selectedTravel.splice(index, 1)
      // } else {
        self.selectedTravel.splice(0);
        self.selectedTravel.push(item)
      // }

    },
    selectType: function (item) {
      var self = this;
      let index = self.selectedType.findIndex(a => a === item)
      if (index >= 0) {
        self.selectedType.splice(index, 1)
      } else {
        self.selectedType.push(item)
      }
    },
    setpaxcount(item, action) {
      if (item == 'adt') {
        if (action == 'plus') {
          ++this.selectedAdult;
        } else {
          if (this.selectedAdult > 1) {
            --this.selectedAdult;
          }
        }
      } else if (item == 'chd') {
        if (action == 'plus') {
          ++this.selectedChildren;
        } else {
          if (this.selectedChildren > 0) {
            --this.selectedChildren;
          }
        }
      } else if (item == 'inf') {
        if (action == 'plus') {
          ++this.selectedInfants

        } else {
          if (this.selectedInfants > 0) {
            --this.selectedInfants
          }
        }
      }
    },
    setclaneder: function () {
      var vm = this;
      $(function () {
      //   $('#traveldate').daterangepicker({
      //     locale: {
      //           format: 'MM/DD/YYYY'
      //     },
      //     autoApply: true,
      //     minDate: moment(),
      //     alwaysShowCalendars: true,
      //     autoUpdateInput: false,
      // }, function(start, end, label) { 
      //     $('#traveldate').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
      //     vm.fromDate = 'Start Date: ' + start.format('MM/DD/YYYY') + ' - End Date: ' + end.format('MM/DD/YYYY');
      // });
      if($('#traveldatefrom, #traveldateto').length){
          // check if element is available to bind ITS ONLY ON HOMEPAGE
          var currentDate = moment().format("MM/DD/YYYY");
      
          $('#traveldatefrom, #traveldateto').daterangepicker({
              locale: {
                  format: 'MM/DD/YYYY'
              },
              alwaysShowCalendars: true,
              minDate: currentDate,
              autoApply: true,
              autoUpdateInput: false,
          }, function(start, end, label) {
          // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
          // Lets update the fields manually this event fires on selection of range
          var selectedStartDate = start.format('MM/DD/YYYY'); // selected start
          var selectedEndDate = end.format('MM/DD/YYYY'); // selected end
      
          $checkinInput = $('#traveldatefrom');
          $checkoutInput = $('#traveldateto');
      
          // Updating Fields with selected dates
          $checkinInput.val(selectedStartDate);
          $checkoutInput.val(selectedEndDate);
      
          // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
          var checkOutPicker = $checkoutInput.data('daterangepicker');
          checkOutPicker.setStartDate(selectedStartDate);
          checkOutPicker.setEndDate(selectedEndDate);
      
          // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
          var checkInPicker = $checkinInput.data('daterangepicker');
          checkInPicker.setStartDate(selectedStartDate);
          checkInPicker.setEndDate(selectedEndDate);
          vm.fromDate = 'From Date: ' + moment($('#traveldatefrom').val(), 'MM/DD/YYYY').format('YYYY-MM-DDThh:mm:ss') 
          + ' - To Date: ' + moment($('#traveldateto').val(), 'MM/DD/YYYY').format('YYYY-MM-DDThh:mm:ss')
          });
      
      } // End Daterange Picker
        // $("#traveldate").datepicker({
        //   dateFormat: "dd/mm/yy",
        //   minDate: "0d",
        //   numberOfMonths: 1,
        //   showOn: "both",
        //   buttonText: "<i class='fa fa-calendar'></i>",
        //   duration: "fast",
        //   showAnim: "slide",
        //   showOptions: {
        //     direction: "up"
        //   }
        // })
        // .on( "change", function() {
        //   to.datepicker( "option", "minDate", getDate( this ) );
        // }),
        // function getDate( element ) {
        //   var date;
        //   try {
        //     date = $.datepicker.parseDate( dateFormat, element.value );
        //   } catch( error ) {
        //     date = null;
        //   }

        //   return date;
        // }
        if($('#flightfrom, #flightto').length){
          // check if element is available to bind ITS ONLY ON HOMEPAGE
          var currentDate = moment().format("MM/DD/YYYY");
      
          $('#flightfrom, #flightto').daterangepicker({
              locale: {
                  format: 'MM/DD/YYYY'
              },
              alwaysShowCalendars: true,
              minDate: currentDate,
              autoApply: true,
              autoUpdateInput: false,
          }, function(start, end, label) {
          // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
          // Lets update the fields manually this event fires on selection of range
          var selectedStartDate = start.format('MM/DD/YYYY'); // selected start
          var selectedEndDate = end.format('MM/DD/YYYY'); // selected end
      
          $checkinInput = $('#flightfrom');
          $checkoutInput = $('#flightto');
      
          // Updating Fields with selected dates
          $checkinInput.val(selectedStartDate);
          $checkoutInput.val(selectedEndDate);
      
          // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
          var checkOutPicker = $checkoutInput.data('daterangepicker');
          checkOutPicker.setStartDate(selectedStartDate);
          checkOutPicker.setEndDate(selectedEndDate);
      
          // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
          var checkInPicker = $checkinInput.data('daterangepicker');
          checkInPicker.setStartDate(selectedStartDate);
          checkInPicker.setEndDate(selectedEndDate);
          });
      
      } // End Daterange Picker
      });
    },
    dropdownChange: function (event, type) {
      if (event != undefined && event.target != undefined && event.target.value != undefined) {
          if (type == 'traveldate') {
              this.fromDate = event.target.value;
          }
      }
    },
    getRegionList:function(){
      let region = this.selectedRegion.map(({ Name }) => Name)
      return region.join(',');
    },
    getExperianceList:function(){
      let experience = this.selectedExperiance.map(({ Name }) => Name)
      return experience.join(',');
    },
    getTravelList:function(){
      let travel = this.selectedTravel.map(({ Name }) => Name)
      return travel.join(',');
    },
    getTypeList:function(){
      let result = this.selectedType.map(({ Name }) => Name)
      return result.join(',');
    },
    sendDesignedTrip: async function () {
      var vm = this;
      if (!this.fname.trim()) {
        alertify.alert(getValidationMsgByCode('M022'), getValidationMsgByCode('M034')).set('closable', false);
        return false;
      }
      if (!this.email.trim()) {
        alertify.alert(getValidationMsgByCode('M022'), getValidationMsgByCode('M026')).set('closable', false);

        return false;
      }
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.email.match(emailPat);
      if (matchArray == null) {
          alertify.alert(getValidationMsgByCode('M022'), getValidationMsgByCode('M027')).set('closable', false);
          return false;
      }
      if (this.offer==false) {
          alertify.alert(getValidationMsgByCode('M022'), getValidationMsgByCode('M048')).set('closable', false);
          return false;
      } 
      else{
      this.isLoading = true;
      var fromEmail = JSON.parse(localStorage.User).loginNode.email;
      var toEmail = JSON.parse(localStorage.User).loginNode.parentEmailId;
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
      let requestedDate = moment(new Date()).format('YYYY-MM-DDThh:mm:ss');
      let traveldate = 'From Date: ' + moment($('#traveldatefrom').val(), 'MM/DD/YYYY').format('YYYY-MM-DDThh:mm:ss') 
          + ' - To Date: ' + moment($('#traveldateto').val(), 'MM/DD/YYYY').format('YYYY-MM-DDThh:mm:ss');
      var region = this.getRegionList();
      var exp = this.getExperianceList();
      var traveltype = this.getTravelList();
      var type = this.getTypeList();
      var postData = {
        type: "PackageBookingRequest",
        fromEmail: fromEmail,
        toEmails: Array.isArray(toEmail.To_Email) ? toEmail.To_Email : [toEmail.To_Email],
        ccEmails: [],
        logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + '.xhtml?ln=logo',
        agencyName: JSON.parse(localStorage.User).loginNode.name || "",
        agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
        packegeName: this.countryname ? this.countryname : this.mainContent.Country_Name,
        personName: this.fname,
        emailAddress: this.email,
        contact: this.phonenumber,
        departureDate: this.bookdate,
        adults: this.selectedAdult,
        child2to5: this.selectedChildren,
        child6to11: "0",
        infants: this.selectedInfants,
        message: "",
        primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
        secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
      };
      var custmail = {
        type: "ThankYouRequest",
        fromEmail: fromEmail,
        toEmails: Array.isArray(this.email) ? this.email : [this.email],
        logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + '.xhtml?ln=logo',
        agencyName: JSON.parse(localStorage.User).loginNode.name,
        agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
        personName: this.fname,
        message: 'Thank you for the request. Our Travel Consultants will be in contact with you shortly.',
        primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
        secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
      };
      var childAge = [];
      for (let index = 0; index < this.additional_child; index++) {
        childAge.push(this.additional_child_age[index].age);
      }
      this.internationalFlightDetails = `Adults: ${this.additional_adult}, Children: ${this.additional_child}, Children Ages: ${childAge.join(", ")}, Infant: ${this.additional_infant}`;
      let insertData = {
        type: "Designed Trip Booking",
        keyword1: this.fname,
        keyword2: this.lname,
        keyword3: this.email,
        keyword4: this.phonenumber,
        number1: this.selectedAdult,
        number2: this.selectedChildren,
        number3: this.selectedInfants,
        text8: region,
        text9: exp,
        text10: traveltype,
        text11: type,
        date2: moment($('#traveldatefrom').val(), 'MM/DD/YYYY').format('YYYY-MM-DDThh:mm:ss'),
        date3: moment($('#traveldateto').val(), 'MM/DD/YYYY').format('YYYY-MM-DDThh:mm:ss'),
        // keyword5: this.tripLength,
        keyword6: this.budget,
        text12: this.flightInfo,
        text1: this.flightInfoCheck ? this.internationalFlightDetails : "",
        text13: this.depAirport,
        text14: this.arrAirport,
        text15: $('#flightfrom').val(),
        text16: $('#flightto').val(),
        keyword7: this.age,
        text2: this.addinfo,
        text3: this.offer,
        text4: this.signup,
        text6:this.countryname ? this.countryname : this.mainContent.Name,
        date1: requestedDate,
        nodeCode: agencyCode
      };
      let responseObject = await cmsRequestData("POST", "cms/data", insertData, null);
      try {
        let insertID = Number(responseObject);
        var emailApi = ServiceUrls.emailServices.emailApi;
        sendMailServiceWithCallBack(emailApi, postData, function() {
          sendMailServiceWithCallBack(emailApi, custmail, function() {
            vm.isLoading = false;
            alertify.alert(getValidationMsgByCode('M023'), getValidationMsgByCode('M009')).set('onok', function(closeEvent){ window.location.href="index.html"} );
          });
        });
      } catch (e) {
        this.isLoading = false;
      }
      }
    },
    readReview: async function () {
      var self = this;
      self.allComments = [];
      var filterValue = "type='Package Review' AND keyword4='" + self.packageurl + "'";
      self.getDbDataTableValue(filterValue, "ingest_timestamp").then(function (response) {
      if (response != undefined && response.data != undefined && response.status == 200) {
        self.allComments = response.data.data;
        self.isLoading = false;
      } 
      else {
        console.log("error:", response);
      }
    })
    },
    getDbDataTableValue: async function (extraFilter, sortField) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/cms/data/search/byQuery";
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
      var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
      if (extraFilter != undefined && extraFilter != '') {
          queryStr = queryStr + " AND " + extraFilter;
      }
      var requestObject = {
          query: queryStr,
          sortField: sortField,
          from: 0,
          orderBy: "desc"
      };
      if (requestObject != null) {
          requestObject = JSON.stringify(requestObject);
      }
      try {
          let allDBData = await axios({
              url: url,
              method: 'POST',
              headers: {
                  'Content-Type': 'application/json',
              },
              data: requestObject
          })
          return allDBData;
      } catch (error) {
          console.log(error);
          return error.response.data.code;
      }
    },
    dateFormatter: function (utc) {
      return (moment(utc).utcOffset("+05:30").format("DD MMM YYYY"));
    },
    Departurefrom(AirportCode, AirportName, leg) {
      this.depAirport = AirportName;
    },
    Arrivalfrom(AirportCode, AirportName, leg) {
      this.arrAirport =  AirportName;
    }
  }
});

function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function () {
  if (detailrequest.selectedRegion.length == 0) {
    alertify.alert(getValidationMsgByCode('M022'), getValidationMsgByCode('M044')).set('closable', false);
    return false;
  } else if (detailrequest.selectedExperiance.length == 0) {
    alertify.alert(getValidationMsgByCode('M022'), getValidationMsgByCode('M045')).set('closable', false);
    return false;
  } else if (detailrequest.selectedTravel.length == 0 || detailrequest.selectedTravel[0].isSelected == false) {
    alertify.alert(getValidationMsgByCode('M022'), getValidationMsgByCode('M046')).set('closable', false);
    return false;
  } else if (detailrequest.selectedType.length == 0) {
    alertify.alert(getValidationMsgByCode('M022'), getValidationMsgByCode('M047')).set('closable', false);
    return false;
  } else {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({
      opacity: 0
    }, {
      step: function (now, mx) {
        //as the opacity of current_fs reduces to 0 - stored in "now"
        //1. scale current_fs down to 80%
        scale = 1 - (1 - now) * 0.2;
        //2. bring next_fs from the right(50%)
        left = (now * 50) + "%";
        //3. increase opacity of next_fs to 1 as it moves in
        opacity = 1 - now;
        current_fs.css({
          'transform': 'scale(' + scale + ')',
        });
        next_fs.css({
          'left': left,
          'opacity': opacity
        });
      },
      duration: 100,
      complete: function () {
        current_fs.hide();
        animating = false;
      },
      //this comes from the custom easing plugin
      easing: 'easeInOutBack'
    });
  }
});
$(".next2").click(function () {
  if (!detailrequest.fromDate) {
    alertify.alert(getValidationMsgByCode('M022'), getValidationMsgByCode('M006')).set('closable', false);
    return false;
  } else if (!detailrequest.budget) {
    alertify.alert(getValidationMsgByCode('M022'), getValidationMsgByCode('M043')).set('closable', false);
    return false;
  } 
  else if (detailrequest.flightInfoCheck && (detailrequest.depAirport == '' || detailrequest.arrAirport == '' || $('#flightfrom').val() == '' || $('#flightto').val() == '')) {
    alertify.alert(getValidationMsgByCode('M022'), getValidationMsgByCode('M054')).set('closable', false);
    return false;
  }
  else {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({
      opacity: 0
    }, {
      step: function (now, mx) {
        //as the opacity of current_fs reduces to 0 - stored in "now"
        //1. scale current_fs down to 80%
        scale = 1 - (1 - now) * 0.2;
        //2. bring next_fs from the right(50%)
        left = (now * 50) + "%";
        //3. increase opacity of next_fs to 1 as it moves in
        opacity = 1 - now;
        current_fs.css({
          'transform': 'scale(' + scale + ')',
        });
        next_fs.css({
          'left': left,
          'opacity': opacity
        });
      },
      duration: 100,
      complete: function () {
        current_fs.hide();
        animating = false;
      },
      //this comes from the custom easing plugin
      easing: 'easeInOutBack'
    });
  }
});