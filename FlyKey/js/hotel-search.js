$(document).ready(function () {
    if (localStorage.direction == 'rtl') {
        $("#rangeSlider").removeClass("range_sec");
    }

});

Vue.directive('click-outside', {
    bind: function (el, binding, vnode) {
        el.clickOutsideEvent = function (event) {
            // here I check that click was outside the el and his childrens
            if (!(el == event.target || el.contains(event.target))) {
                // and if it did, call method provided in attribute value
                if (vnode.context[binding.expression]) {
                    vnode.context[binding.expression](event);
                }
            }
        };
        document.body.addEventListener('click', el.clickOutsideEvent)
    },
    unbind: function (el) {
        document.body.removeEventListener('click', el.clickOutsideEvent)
    },
});


var hotelRooms = Vue.component("hotel-rooms", {
    data: function () {
        return {
            adults: [
                { 'value': 1 },
                { 'value': 2 },
                { 'value': 3 },
                { 'value': 4 },
                { 'value': 5 },
                { 'value': 6 },
                { 'value': 7 }
            ],
            children: [
                { 'value': 0 },
                { 'value': 1 },
                { 'value': 2 },

            ],
            childrenAges: [
                { 'value': 1, },
                { 'value': 2, },
                { 'value': 3, },
                { 'value': 4, },
                { 'value': 5, },
                { 'value': 6, },
                { 'value': 7, },
                { 'value': 8, },
                { 'value': 9, },
                { 'value': 10 },
                { 'value': 11 },
                { 'value': 12 },
                // { 'value': 13 },
                // { 'value': 14 },
                // { 'value': 15 },
                // { 'value': 16 },
                // { 'value': 17 }
            ],
            selectedAdults: 1,
            selectedChildren: 0,
            selectedChildrenAges: [
                { child1: 1 },
                { child2: 1 }
            ]
        };
    },
    props: {
        roomId: Number,
        getRoomDetails: Function,
        childLabel: String,
        childrenLabel: String,
        adultLabel: String,
        adultsLabel: String,
        ageLabel: String,
        agesLabel: String,
        roomLabel: String,
        roomsLabel: String
    },
    mounted: function () {
        this.$nextTick(function () {
            if (localStorage.direction == 'rtl') {
                //  this.arabic_dropdown = "dwn_icon";
                $(".en_dwn").addClass("dwn_icon");
            } else {
                //  this.arabic_dropdown = "";
                $(".en_dwn").removeClass("dwn_icon");
            }
        })
        this.$watch(function (vm) { return vm.selectedAdults, vm.selectedChildren, vm.selectedChildrenAges, Date.now(); },
            function () {
                // Executes if data above have changed.
                this.$emit("get-room-details", {
                    roomId: this.roomId,
                    selectedAdults: this.selectedAdults,
                    selectedChildren: this.selectedChildren,
                    selectedChildrenAges: this.selectedChildrenAges
                })
                this.$nextTick(function () {
                    if (localStorage.direction == 'rtl') {
                        //  this.arabic_dropdown = "dwn_icon";
                        $(".en_dwn").addClass("dwn_icon");
                    } else {
                        //  this.arabic_dropdown = "";
                        $(".en_dwn").removeClass("dwn_icon");
                    }
                })
            }
        )
    }
});

var hotelSeatch = Vue.component("hotel-search", {
    data: function () {
        return {
            //city search
            isCompleted: false,
            keywordSearch: "",
            autoCompleteResult: [],
            cityCode: "",
            cityName: "",
            //supplier select
            showSupplierList: false,
            supplierList: [],
            selectedSuppliers: [],
            supplierSearchLabel: "All Suppliers",
            //coutries
            countries: [
                { code: "NG", name: "Nigeria" },
                { code: "AF", name: "Afghanistan" },
                { code: "AL", name: "Albania" },
                { code: "DZ", name: "Algeria" },
                { code: "AS", name: "American Samoa" },
                { code: "AD", name: "Andorra" },
                { code: "AO", name: "Angola" },
                { code: "AI", name: "Anguilla" },
                { code: "AQ", name: "Antartica" },
                { code: "AG", name: "Antigua And Barbuda" },
                { code: "AR", name: "Argentina" },
                { code: "AM", name: "Armenia" },
                { code: "AW", name: "Aruba" },
                { code: "AU", name: "Australia" },
                { code: "AT", name: "Austria" },
                { code: "AZ", name: "Azerbaijan" },
                { code: "BS", name: "Bahamas" },
                { code: "BH", name: "Bahrain" },
                { code: "BD", name: "Bangladesh" },
                { code: "BB", name: "Barbados" },
                { code: "BY", name: "Belarus" },
                { code: "BE", name: "Belgium" },
                { code: "BZ", name: "Belize" },
                { code: "BJ", name: "Benin" },
                { code: "BM", name: "Bermuda" },
                { code: "BT", name: "Bhutan" },
                { code: "BO", name: "Bolivia" },
                { code: "BQ", name: "Bonaire St Eustatius And Saba " },
                { code: "BA", name: "Bosnia-Herzegovina" },
                { code: "BW", name: "Botswana" },
                { code: "BR", name: "Brazil" },
                { code: "IO", name: "British Indian Ocean Territory" },
                { code: "BN", name: "Brunei Darussalam" },
                { code: "BG", name: "Bulgaria" },
                { code: "BF", name: "Burkina Faso" },
                { code: "BI", name: "Burundi" },
                { code: "KH", name: "Cambodia" },
                { code: "CM", name: "Cameroon-Republic Of" },
                { code: "CB", name: "Canada Buffer" },
                { code: "CA", name: "Canada" },
                { code: "CV", name: "Cape Verde-Republic Of" },
                { code: "KY", name: "Cayman Islands" },
                { code: "CF", name: "Central African Republic" },
                { code: "TD", name: "Chad" },
                { code: "CL", name: "Chile" },
                { code: "CN", name: "China" },
                { code: "CX", name: "Christmas Island" },
                { code: "CC", name: "Cocos Islands" },
                { code: "CO", name: "Colombia" },
                { code: "KM", name: "Comoros" },
                { code: "CG", name: "Congo Brazzaville" },
                { code: "CD", name: "Congo The Democratic Rep Of" },
                { code: "CK", name: "Cook Islands" },
                { code: "CR", name: "Costa Rica" },
                { code: "CI", name: "Cote D Ivoire" },
                { code: "HR", name: "Croatia" },
                { code: "CU", name: "Cuba" },
                { code: "CW", name: "Curacao" },
                { code: "CY", name: "Cyprus" },
                { code: "CZ", name: "Czech Republic" },
                { code: "DK", name: "Denmark" },
                { code: "DJ", name: "Djibouti" },
                { code: "DM", name: "Dominica" },
                { code: "DO", name: "Dominican Republic" },
                { code: "TP", name: "East Timor Former Code)" },
                { code: "EC", name: "Ecuador" },
                { code: "EG", name: "Egypt" },
                { code: "SV", name: "El Salvador" },
                { code: "EU", name: "Emu European Monetary Union" },
                { code: "GQ", name: "Equatorial Guinea" },
                { code: "ER", name: "Eritrea" },
                { code: "EE", name: "Estonia" },
                { code: "ET", name: "Ethiopia" },
                { code: "FK", name: "Falkland Islands" },
                { code: "FO", name: "Faroe Islands" },
                { code: "ZZ", name: "Fictitious Points" },
                { code: "FJ", name: "Fiji" },
                { code: "FI", name: "Finland" },
                { code: "FR", name: "France" },
                { code: "GF", name: "French Guiana" },
                { code: "PF", name: "French Polynesia" },
                { code: "GA", name: "Gabon" },
                { code: "GM", name: "Gambia" },
                { code: "GE", name: "Georgia" },
                { code: "DE", name: "Germany" },
                { code: "GH", name: "Ghana" },
                { code: "GI", name: "Gibraltar" },
                { code: "GR", name: "Greece" },
                { code: "GL", name: "Greenland" },
                { code: "GD", name: "Grenada" },
                { code: "GP", name: "Guadeloupe" },
                { code: "GU", name: "Guam" },
                { code: "GT", name: "Guatemala" },
                { code: "GW", name: "Guinea Bissau" },
                { code: "GN", name: "Guinea" },
                { code: "GY", name: "Guyana" },
                { code: "HT", name: "Haiti" },
                { code: "HN", name: "Honduras" },
                { code: "HK", name: "Hong Kong" },
                { code: "HU", name: "Hungary" },
                { code: "IS", name: "Iceland" },
                { code: "IN", name: "India" },
                { code: "ID", name: "Indonesia" },
                { code: "IR", name: "Iran" },
                { code: "IQ", name: "Iraq" },
                { code: "IE", name: "Ireland-Republic Of" },
                { code: "IL", name: "Israel" },
                { code: "IT", name: "Italy" },
                { code: "JM", name: "Jamaica" },
                { code: "JP", name: "Japan" },
                { code: "JO", name: "Jordan" },
                { code: "KZ", name: "Kazakhstan" },
                { code: "KE", name: "Kenya" },
                { code: "KI", name: "Kiribati" },
                { code: "KP", name: "Korea Dem Peoples Rep Of" },
                { code: "KR", name: "Korea Republic Of" },
                { code: "KW", name: "Kuwait" },
                { code: "KG", name: "Kyrgyzstan" },
                { code: "LA", name: "Lao Peoples Dem Republic" },
                { code: "LV", name: "Latvia" },
                { code: "LB", name: "Lebanon" },
                { code: "LS", name: "Lesotho" },
                { code: "LR", name: "Liberia" },
                { code: "LY", name: "Libya" },
                { code: "LI", name: "Liechtenstein" },
                { code: "LT", name: "Lithuania" },
                { code: "LU", name: "Luxembourg" },
                { code: "MO", name: "Macao -Sar Of China-" },
                { code: "MK", name: "Macedonia -Fyrom-" },
                { code: "MG", name: "Madagascar" },
                { code: "MW", name: "Malawi" },
                { code: "MY", name: "Malaysia" },
                { code: "MV", name: "Maldives Island" },
                { code: "ML", name: "Mali" },
                { code: "MT", name: "Malta" },
                { code: "MH", name: "Marshall Islands" },
                { code: "MQ", name: "Martinique" },
                { code: "MR", name: "Mauritania" },
                { code: "MU", name: "Mauritius Island" },
                { code: "YT", name: "Mayotte" },
                { code: "MB", name: "Mexico Buffer" },
                { code: "MX", name: "Mexico" },
                { code: "FM", name: "Micronesia" },
                { code: "MD", name: "Moldova" },
                { code: "MC", name: "Monaco" },
                { code: "MN", name: "Mongolia" },
                { code: "ME", name: "Montenegro" },
                { code: "MS", name: "Montserrat" },
                { code: "MA", name: "Morocco" },
                { code: "MZ", name: "Mozambique" },
                { code: "MM", name: "Myanmar" },
                { code: "NA", name: "Namibia" },
                { code: "NR", name: "Nauru" },
                { code: "NP", name: "Nepal" },
                { code: "AN", name: "Netherlands Antilles" },
                { code: "NL", name: "Netherlands" },
                { code: "NC", name: "New Caledonia" },
                { code: "NZ", name: "New Zealand" },
                { code: "NI", name: "Nicaragua" },
                { code: "NE", name: "Niger" },
                { code: "NU", name: "Niue" },
                { code: "NF", name: "Norfolk Island" },
                { code: "MP", name: "Northern Mariana Islands" },
                { code: "NO", name: "Norway" },
                { code: "OM", name: "Oman" },
                { code: "PK", name: "Pakistan" },
                { code: "PW", name: "Palau Islands" },
                { code: "PS", name: "Palestine - State Of" },
                { code: "PA", name: "Panama" },
                { code: "PG", name: "Papua New Guinea" },
                { code: "PY", name: "Paraguay" },
                { code: "PE", name: "Peru" },
                { code: "PH", name: "Philippines" },
                { code: "PL", name: "Poland" },
                { code: "PT", name: "Portugal" },
                { code: "PR", name: "Puerto Rico" },
                { code: "QA", name: "Qatar" },
                { code: "RE", name: "Reunion Island" },
                { code: "RO", name: "Romania" },
                { code: "RU", name: "Russia" },
                { code: "XU", name: "Russia" },
                { code: "RW", name: "Rwanda" },
                { code: "WS", name: "Samoa-Independent State Of" },
                { code: "SM", name: "San Marino" },
                { code: "ST", name: "Sao Tome And Principe Islands " },
                { code: "SA", name: "Saudi Arabia" },
                { code: "SN", name: "Senegal" },
                { code: "RS", name: "Serbia" },
                { code: "SC", name: "Seychelles Islands" },
                { code: "SL", name: "Sierra Leone" },
                { code: "SG", name: "Singapore" },
                { code: "SX", name: "Sint Maarten" },
                { code: "SK", name: "Slovakia" },
                { code: "SI", name: "Slovenia" },
                { code: "SB", name: "Solomon Islands" },
                { code: "SO", name: "Somalia" },
                { code: "ZA", name: "South Africa" },
                { code: "SS", name: "South Sudan" },
                { code: "ES", name: "Spain" },
                { code: "LK", name: "Sri Lanka" },
                { code: "SH", name: "St. Helena Island" },
                { code: "KN", name: "St. Kitts" },
                { code: "LC", name: "St. Lucia" },
                { code: "PM", name: "St. Pierre And Miquelon" },
                { code: "VC", name: "St. Vincent" },
                { code: "SD", name: "Sudan" },
                { code: "SR", name: "Suriname" },
                { code: "SZ", name: "Swaziland" },
                { code: "SE", name: "Sweden" },
                { code: "CH", name: "Switzerland" },
                { code: "SY", name: "Syrian Arab Republic" },
                { code: "TW", name: "Taiwan" },
                { code: "TJ", name: "Tajikistan" },
                { code: "TZ", name: "Tanzania-United Republic" },
                { code: "TH", name: "Thailand" },
                { code: "TL", name: "Timor Leste" },
                { code: "TG", name: "Togo" },
                { code: "TK", name: "Tokelau" },
                { code: "TO", name: "Tonga" },
                { code: "TT", name: "Trinidad And Tobago" },
                { code: "TN", name: "Tunisia" },
                { code: "TR", name: "Turkey" },
                { code: "TM", name: "Turkmenistan" },
                { code: "TC", name: "Turks And Caicos Islands" },
                { code: "TV", name: "Tuvalu" },
                { code: "UM", name: "U.S. Minor Outlying Islands" },
                { code: "UG", name: "Uganda" },
                { code: "UA", name: "Ukraine" },
                { code: "AE", name: "United Arab Emirates" },
                { code: "GB", name: "United Kingdom" },
                { code: "US", name: "United States Of America" },
                { code: "UY", name: "Uruguay" },
                { code: "UZ", name: "Uzbekistan" },
                { code: "VU", name: "Vanuatu" },
                { code: "VA", name: "Vatican" },
                { code: "VE", name: "Venezuela" },
                { code: "VN", name: "Vietnam" },
                { code: "VG", name: "Virgin Islands-British" },
                { code: "VI", name: "Virgin Islands-United States" },
                { code: "WF", name: "Wallis And Futuna Islands" },
                { code: "EH", name: "Western Sahara" },
                { code: "YE", name: "Yemen Republic" },
                { code: "ZM", name: "Zambia" },
                { code: "ZW", name: "Zimbabwe" },
            ],
            countryOfResidence: "AE",
            nationality: "AE",
            // datepicker
            checkIn: "",
            checkOut: "",
            numberOfNights: 1,
            numberOfRooms: 1,
            roomSelectionDetails: {
                room1: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room2: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room3: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room4: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room5: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                }
            },
            KMrange: 5,
            arabic_dropdown: '',
            highlightIndex: 0,
            content: {},
            searchForm: [],
            Hotel_form_caption: "",
            Hotel_city_placeholder: "",
            childLabel: "",
            childrenLabel: "",
            adultLabel: "",
            adultsLabel: "",
            ageLabel: "",
            agesLabel: "",
            roomLabel: "",
            roomsLabel: "",
            rangeLabel: "",
            searchBtnLabel: "",
            Search_range_label: "Include Cities Within",
            showroom: false,
            hotelLabel: {},
           
           checkIn: "",
           checkOut:"",
           hotelDestination: ""
        }
    },
    created: function () {
        var agencyNode = window.localStorage.getItem("User");
        if (agencyNode) {
            agencyNode = JSON.parse(agencyNode);
            var servicesList = agencyNode.loginNode.servicesList;
            for (var i = 0; i < servicesList.length; i++) {
                if (servicesList[i].name == "Hotel" && servicesList[i].provider.length > 0) {
                    for (var j = 0; j < servicesList[i].provider.length; j++) {
                        this.supplierList.push({ id: servicesList[i].provider[j].id, name: servicesList[i].provider[j].name.toUpperCase(), supplierType: servicesList[i].provider[j].supplierType });
                        this.selectedSuppliers = this.supplierList.map(function (supplier) { return supplier.id });
                    }
                    break;
                } else if (servicesList[i].name == "Hotel" && servicesList[i].provider.length == 0) {
                    //setLoginPage("login");
                }
            }
            this.getPagecontent();

        }
    },
    computed: {
        selectAll: {
            get: function () {
                return this.supplierList ? this.selectedSuppliers.length == this.supplierList.length : false;
            },
            set: function (value) {
                var selectedSuppliers = [];

                if (value) {
                    this.supplierList.forEach(function (supplier) {
                        selectedSuppliers.push(supplier.id);
                    });
                }

                this.selectedSuppliers = selectedSuppliers;
            }
        }
    },
    methods: {
        getValidationMsgByCode: function (code) {
            if (sessionStorage.validationItems !== undefined) {
              var validationList = JSON.parse(sessionStorage.validationItems);
              for (let validationItem of validationList.Validation_List) {
                if (code === validationItem.Code) {
                  return validationItem.Message;
                }
              }
            }
          },
        onClickAutoCompleteEvent: function () {
            // if (this.autoCompleteResult.length > 0) {
            this.isCompleted = true;
            // } else {
            // this.isCompleted = false;
            // }
        },
        onKeyUpAutoCompleteEvent: _.debounce(function (keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
                var cityName = keywordEntered;
                var cityarray = cityName.split(' ');
                var uppercaseFirstLetter = "";
                for (var k = 0; k < cityarray.length; k++) {
                    uppercaseFirstLetter += cityarray[k].charAt(0).toUpperCase() + cityarray[k].slice(1).toLowerCase()
                    if (k < cityarray.length - 1) { uppercaseFirstLetter += " "; }
                }
                uppercaseFirstLetter = "*" + uppercaseFirstLetter + "*";
                var lowercaseLetter = "*" + cityName.toLowerCase() + "*";
                var uppercaseLetter = "*" + cityName.toUpperCase() + "*";


                var should = [];
                var i = 0;
                var supplier_type = "";
                for (var key in self.supplierList) {
                    // check if the property/key is defined in the object itself, not in parent
                    if (self.supplierList.hasOwnProperty(key)) {
                        supplier_type = [self.supplierList[key].supplierType, "Both"];
                        var supplierIndex = self.selectedSuppliers.indexOf(self.supplierList[key].id);
                        if (supplierIndex > -1) {

                            var supplier_id_query = {
                                bool: {
                                    must: [{
                                        match_phrase: { supplier_id: self.selectedSuppliers[supplierIndex] }
                                    },
                                    {
                                        terms: { supplier_type: supplier_type }
                                    }
                                    ]
                                }
                            };

                            should.push(supplier_id_query);
                            i++;
                        }
                    }
                }

                var query = {
                    query: {
                        bool: {
                            must: [{
                                bool: {
                                    should: [
                                        { wildcard: { supplier_city_name: uppercaseFirstLetter } },
                                        { wildcard: { supplier_city_name: uppercaseLetter } },
                                        { wildcard: { supplier_city_name: lowercaseLetter } }
                                    ]
                                }
                            },
                            { bool: { should } }
                            ]
                        }
                    }
                };

                var client = new elasticsearch.Client({
                    host: [{
                        host: ServiceUrls.elasticSearch.elasticsearchHost,
                        auth: ServiceUrls.elasticSearch.auth,
                        protocol: ServiceUrls.elasticSearch.protocol,
                        port: ServiceUrls.elasticSearch.port
                    }],
                    log: 'trace'
                });

                client.search({ index: 'city_map', size: 150, body: query }).then(function (resp) {
                    finalResult = [];
                    var hits = resp.hits.hits;
                    var Citymap = new Map();
                    for (var i = 0; i < hits.length; i++) {
                        Citymap.set(hits[i]._source.oneview_city_id, hits[i]._source);
                    }
                    var get_values = Citymap.values();
                    var Cityvalues = [];
                    for (var ele of get_values) {
                        Cityvalues.push(ele);
                    }
                    var results = SortInputFirst(cityName, Cityvalues);
                    for (var i = 0; i < results.length; i++) {
                        finalResult.push({
                            "code": results[i].oneview_city_id,
                            "label": results[i].supplier_city_name + ", " + results[i].oneview_country_name,
                            "location": results[i].location

                        });
                    }
                    var newData = [];
                    finalResult.forEach(function (item, index) {
                        if (item.label.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {

                            newData.push(item);
                        }
                    });
                    console.log(newData);
                    self.autoCompleteResult = newData;
                    self.isCompleted = true;
                    self.highlightIndex = 0;
                });

            } else {
                this.autoCompleteResult = [];
                this.isCompleted = false;
            }
        }, 100),
        updateResults: function (item) {
            this.keywordSearch = item.label;
            this.autoCompleteResult = [];
            this.cityCode = item.code;
            this.cityName = item.label;
            window.sessionStorage.setItem("cityLocation", JSON.stringify(item.location));
            $('#txtCheckInDate').focus();

        },
        showSupplierListDropdown: function () {
            if (this.supplierList.length != 1) {
                this.showSupplierList = !this.showSupplierList;
            }
            this.isCompleted = false;
        },
        getRoomDetails: function (data) {
            var selectedRoom = this.roomSelectionDetails['room' + (data.roomId + 1)];
            selectedRoom.adult = data.selectedAdults;
            selectedRoom.children = data.selectedChildren;
            if (data.selectedChildren == 0) {
                selectedRoom.childrenAges = [];
            } else if (data.selectedChildren == 1) {
                selectedRoom.childrenAges = [data.selectedChildrenAges[0]];
            } else if (data.selectedChildren == 2) {
                selectedRoom.childrenAges = data.selectedChildrenAges;
            }

            this.roomSelectionDetails['room' + (data.roomId + 1)] = selectedRoom;
        },
        setNumberOfNights: function () {
            var dt2 = $('#txtCheckOutDate');
            var startDate = $('#txtCheckInDate').datepicker('getDate');
            var a = $("#txtCheckInDate").datepicker('getDate').getTime();
            var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
            var c = 24 * 60 * 60 * 1000;
            var diffDates = Math.round(Math.abs((a - b) / c));

            if (event.target.value.match(/[^0-9\.]/gi)) {
                alertify.alert('Validate', 'Only numbers allowed.');
                this.numberOfNights = diffDates;
            } else if (event.target.value.match(/[d*\.]/gi)) {
                alertify.alert('Validate', 'Only whole numbers allowed.');
                this.numberOfNights = diffDates;
            } else if (event.target.value > 90) {
                alertify.alert('Maximum Nights', 'Maximum 90 nights allowed.');
                this.numberOfNights = diffDates;
            } else if (event.target.value == 0) {
                alertify.alert('Validate', 'Minimum one night allowed.');
                this.numberOfNights = diffDates;
            } else if (event.target.value == "") {
                this.numberOfNights = diffDates;
            } else {
                startDate.setDate(startDate.getDate() + this.numberOfNights);
                dt2.datepicker('setDate', startDate);
                eDate = dt2.datepicker('getDate').getTime();
                this.checkOut = $("#txtCheckOutDate").val();
            }
        },
        searchHotels: function () {
            var rooms = "";
            var adultCount = 0;
            var childCount = 0;

            for (var roomIndex = 1; roomIndex <= parseInt(this.numberOfRooms); roomIndex++) {
                var roomSelectionDetails = this.roomSelectionDetails['room' + roomIndex];
                adultCount += roomSelectionDetails.adult;
                childCount += roomSelectionDetails.children;
                rooms += "&room" + roomIndex + "=ADT_" + roomSelectionDetails.adult + ",CHD_" + roomSelectionDetails.children;
                for (var childIndex = 0; childIndex < roomSelectionDetails.childrenAges.length; childIndex++) {
                    rooms += "_" + roomSelectionDetails.childrenAges[childIndex]['child' + (childIndex + 1)];
                }
            }

            if (this.cityName.trim() == "" || this.cityCode == "") {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M005')).set('closable', false);
            }
            //  else if (this.selectedSuppliers.length == 0) {
            //     alertify.alert('Warning',this.getValidationMsgByCode('M013')).set('closable', false);
            // } 
            else if (this.checkIn == "") {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M014')).set('closable', false);
            } else if (this.checkOut == "") {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M021')).set('closable', false);
            } else if (this.countryOfResidence == "") {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M015')).set('closable', false);
            } else if (this.nationality == "") {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M016')).set('closable', false);
            } else if (this.numberOfNights == "") {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M017')).set('closable', false);
            } else if (this.numberOfRooms == "") {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M018')).set('closable', false);
            } else if (this.numberOfNights > 90) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M019')).set('closable', false);
            } else if (parseInt(adultCount + childCount) > 9) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M020')).set('closable', false);
            } else {

                var checkIn = $('#txtCheckInDate').val() == "" ? "" : $('#txtCheckInDate').datepicker('getDate');
                var checkOut = $('#txtCheckOutDate').val() == "" ? "" : $('#txtCheckOutDate').datepicker('getDate');

                var searchCriteria = "nationality=" + this.nationality +
                    "&country_of_residence=" + this.countryOfResidence +
                    "&city_code=" + this.cityCode +
                    "&city=" + this.cityName +
                    "&cin=" + moment(checkIn).format('DD/MM/YYYY') +
                    "&cout=" + moment(checkOut).format('DD/MM/YYYY') +
                    "&night=" + this.numberOfNights +
                    "&adtCount=" + adultCount +
                    "&chdCount=" + childCount +
                    "&guest=" + (adultCount + childCount) +
                    "&num_room=" + this.numberOfRooms + rooms +
                    "&KMrange=" + this.KMrange +
                    "&hasCORandNat=true" +
                    // "&supplier=" + this.selectedSuppliers.join(",") +
                    // "&sort=" + "price-a" +
                    "&uuid=" + uuidv4();
                var uri = "/Hotels/hotel-listing.html?" + searchCriteria;
               
                window.location.href = uri;
            }

        },
        hideSupplier: function () {
            this.showSupplierList = false;
        },
        hideCity: function () {
            this.isCompleted = false;
        },
        up: function () {
            if (this.isCompleted) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.isCompleted = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.isCompleted) {
                if (this.highlightIndex < this.autoCompleteResult.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.autoCompleteResult.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.isCompleted = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
           
            var liH = this.$refs.options[this.highlightIndex].clientHeight;
            if (liH == 50) {
                liH = 32;
            }
            if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
            }
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            contentArry.map(function (item) {
                let allKeys = Object.keys(item)
                for (let j = 0; j < allKeys.length; j++) {
                    let key = allKeys[j];
                    let value = item[key];

                    if (key != 'name' && key != 'type') {
                        if (value == undefined || value == null) {
                            value = "";
                        }
                        tempDataObject[key] = value;
                    }
                }
            });
            return tempDataObject;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Home/Home/Home.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    var Form = self.pluck('Booking_Search_Section', response.data.area_List);
                    self.hotelLabel = self.getAllMapData(Form[0].component);
                    self.hotelDestination = self.pluckcom('Hotel_Destination_Placeholder',Form[0].component);
                    self.checkIn = self.pluckcom('Check_In_Placeholder',Form[0].component);
                    self.checkOut = self.pluckcom('Check_Out_Placeholder',Form[0].component);
                    self.roomLabel = self.pluckcom('Rooms_Label',Form[0].component);
                    self.adultsLabel = self.pluckcom('Adults_Label',Form[0].component);
                    self.childrenLabel = self.pluckcom('Children_Label',Form[0].component);
                    self.ageLabel = self.pluckcom('Year_Label',Form[0].component);
                    self.searchBtnLabel = self.pluckcom('Button_Label',Form[0].component);
                    self.roomsLabel = self.pluckcom('Room_Label',Form[0].component);
                    self.adultLabel = self.pluckcom('Adult_Label',Form[0].component);
                    self.childLabel = self.pluckcom('Child_Label',Form[0].component);
                     self.searchForm = self.pluck('Form_Search', self.content.area_List);
                    // self.Hotel_form_caption = self.pluckcom('Hotel_form_caption', self.searchForm[0].component);
                    // self.Hotel_city_placeholder = self.pluckcom('Hotel_city_placeholder', self.searchForm[0].component);
                    // self.Search_range_label = self.pluckcom('Search_range_label', self.searchForm[0].component);

                    // self.childLabel = self.pluckcom('child_label', self.searchForm[0].component);
                    // self.childrenLabel = self.pluckcom('children_label', self.searchForm[0].component);
                    // self.adultLabel = self.pluckcom('Adult_label', self.searchForm[0].component);
                    // self.adultsLabel = self.pluckcom('adults_label', self.searchForm[0].component);
                    // self.ageLabel = self.pluckcom('Age_Label', self.searchForm[0].component);
                    // self.agesLabel = self.pluckcom('Ages_Label', self.searchForm[0].component);
                    // self.roomLabel = self.pluckcom('Room_label', self.searchForm[0].component);
                    // self.roomsLabel = self.pluckcom('Rooms_label', self.searchForm[0].component);
                    // self.rangeLabel = self.pluckcom('Range_Label', self.searchForm[0].component);
                    // self.searchBtnLabel = self.pluckcom('Search_button_label', self.searchForm[0].component);

                    self.ValidationPopUpLabel = self.pluckcom('ValidationPopUp_Label', self.searchForm[0].component);
                    self.ValidationRequiredFieldsLabel = self.pluckcom('ValidationRequiredFields_Label', self.searchForm[0].component);
                   

                    // if (localStorage.direction == 'rtl') {
                    //     $.datepicker.setDefaults($.datepicker.regional["ar"]);
                    // } else {
                    //     $.datepicker.setDefaults($.datepicker.regional["en-GB"]);
                    // }

                    if (localStorage.direction == 'rtl') {
                        $(".ajs-modal").addClass("ar_direction1");
                    } else {
                        $(".ajs-modal").removeClass("ar_direction1");
                    }

                    if (localStorage.direction == 'rtl') {
                        alertify.defaults.glossary.title = 'أليرتفاي جي اس';
                        alertify.defaults.glossary.ok = 'موافق';
                        alertify.defaults.glossary.cancel = 'إلغاء';
                        alertify.confirm().set('labels', { ok: 'موافق', cancel: 'إلغاء' });
                        alertify.alert().set('label', 'موافق');
                    } else {
                        alertify.defaults.glossary.title = 'AlertifyJS';
                        alertify.defaults.glossary.ok = 'OK';
                        alertify.defaults.glossary.cancel = 'Cancel';
                        alertify.confirm().set('labels', { ok: 'OK', cancel: 'Cancel' });
                        alertify.alert().set('label', 'OK');
                    }

                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });

            });
        },
    },
    watch: {
        selectedSuppliers: function () {
            if (this.selectedSuppliers.length == this.supplierList.length && this.supplierList.length != 1) {
                this.supplierSearchLabel = 'All Suppliers';
            } else if (this.selectedSuppliers.length == 0) {
                this.supplierSearchLabel = 'No supplier selected';
            } else if (this.selectedSuppliers.length > 4) {
                this.supplierSearchLabel = this.selectedSuppliers.length + ' supplier selected'
            } else {
                var vm = this;
                this.supplierSearchLabel = this.selectedSuppliers.map(function (id) {
                    return vm.supplierList.filter(function (supplier) {
                        return id == supplier.id;
                    })[0].name;
                }).join(",");
            }
            this.autoCompleteResult = [];
            this.keywordSearch = "";
        }
    },
    mounted: function () {



        if (localStorage.direction == 'rtl') {
            // $("#rangeSlider").removeClass("range_sec ");
            $("#rangeSlider").addClass("range_sec");
            this.arabic_dropdown = "dwn_icon";

            $(".ar_direction").addClass("ar_direction1");
        } else {
            $("#rangeSlider").removeClass("range_sec");

            this.arabic_dropdown = "";
            $(".ar_direction").removeClass("ar_direction1");
        }



        var vm = this;
        this.$nextTick(function () {
            var generalInformation = {
                systemSettings: {
                    systemDateFormat: 'dd M y,D',
                    calendarDisplay: 1,
                    calendarDisplayInMobile: 1
                },
            }

            var setDate = function (days) { return moment(new Date).add(days, 'days').format("DD/MM/YYYY"); };

            var startDate = setDate(1);
            var endDate = setDate(2);

            var dateFormat = generalInformation.systemSettings.systemDateFormat;

            var tempnumberofmonths = 1;
            if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
                tempnumberofmonths = 1;
            } else if (parseInt($(window).width()) > 999) {
                tempnumberofmonths = 1;
            }

            var sDate = new Date(moment(startDate, "DD/MM/YYYY")).getTime();
            var eDate = new Date(moment(endDate, "DD/MM/YYYY")).getTime();

            //Checkin Date
            $("#txtCheckInDate").datepicker({
                dateFormat: dateFormat,
                minDate: 0, // 0 for search
                maxDate: "360d",
                numberOfMonths: parseInt(tempnumberofmonths),
                onSelect: function (date) {

                    var dt2 = $('#txtCheckOutDate');
                    var startDate = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('getDate');
                    sDate = startDate.getTime();
                    startDate.setDate(startDate.getDate() + 1);
                    dt2.datepicker('option', 'minDate', startDate);
                    dt2.datepicker('setDate', minDate);
                    var a = $("#txtCheckInDate").datepicker('getDate').getTime();
                    var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
                    var c = 24 * 60 * 60 * 1000;
                    var diffDays = Math.round(Math.abs((a - b) / c));
                    vm.numberOfNights = diffDays;
                    vm.checkIn = $("#txtCheckInDate").val();
                    $('.ui-datepicker-current-day').click();
                },
                beforeShowDay: function (date) {

                    var container = $(".multi_dropBox");
                    container.hide();

                    if (date.getTime() == eDate) {
                        return [true, 'event event-selected event-selected-right', ''];
                    } else if (date.getTime() <= eDate && date.getTime() >= $(this).datepicker('getDate').getTime()) {
                        if (date.getTime() == $(this).datepicker('getDate').getTime()) {
                            return [true, 'event-selected event-selected-left', ''];
                        } else {
                            return [true, 'event-selected', ''];
                        }
                    } else {
                        return [true, '', ''];
                    }
                }
            });
            $("#txtCheckInDate").datepicker({ dateFormat: dateFormat }).datepicker("setDate", new Date().getDay + 1);
            vm.checkIn = $("#txtCheckInDate").val();

            //Checkout Date
            $('#txtCheckOutDate').datepicker({
                dateFormat: dateFormat,
                minDate: 2, // 2 for search
                maxDate: "360d",
                numberOfMonths: parseInt(tempnumberofmonths),
                onSelect: function (date) {
                    var a = $("#txtCheckInDate").datepicker('getDate').getTime();
                    var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
                    var c = 24 * 60 * 60 * 1000;
                    var diffDays = Math.round(Math.abs((a - b) / c));
                    if (diffDays > 90) {
                        alertify.alert('Maximum Nights', 'Maximum 90 nights allowed.');
                        vm.setNumberOfNights();
                    } else {
                        vm.numberOfNights = diffDays;
                        eDate = $(this).datepicker('getDate').getTime();
                    }
                    vm.checkOut = $("#txtCheckOutDate").val();
                },
                beforeShowDay: function (date) {
                    if (date.getTime() == sDate) {
                        return [true, 'event event-selected event-selected-left', ''];
                    } else if (date.getTime() >= sDate && date.getTime() <= $(this).datepicker('getDate').getTime()) {
                        if (date.getTime() == $(this).datepicker('getDate').getTime()) {
                            return [true, 'event-selected event-selected-right', ''];
                        } else {
                            return [true, 'event-selected', ''];
                        }
                    } else {
                        return [true, '', ''];
                    }
                }
            });

            $("#txtCheckOutDate").datepicker({ dateFormat: dateFormat }).datepicker("setDate", new Date().getDay + 2);
            vm.checkOut = $("#txtCheckOutDate").val();

            var stepSlider = document.getElementById('rangeSlider');

            if (stepSlider.noUiSlider) {
                stepSlider.noUiSlider.updateOptions({
                    direction: localStorage.direction == 'rtl' ? 'rtl' : 'ltr'
                });
            } else {
                noUiSlider.create(stepSlider, {
                    start: [5],
                    step: 1,
                    direction: localStorage.direction == 'rtl' ? 'rtl' : 'ltr',
                    range: {
                        'min': [1],
                        'max': [10]
                    }
                });
            }

            stepSlider.noUiSlider.on('update', function (values, handle) {
                vm.KMrange = parseInt(values[handle]);
            });

        });

    }
});


/**Sorting Function for AutoCompelte Start**/
function SortInputFirst(input, data) {
    var first = [];
    var others = [];
    for (var i = 0; i < data.length; i++) {
        if (data[i].supplier_city_name.toLowerCase().startsWith(input.toLowerCase())) {
            first.push(data[i]);
        } else {

            others.push(data[i]);
        }
    }
    first.sort(function (a, b) {
        var nameA = a.supplier_city_name.toLowerCase(),
            nameB = b.supplier_city_name.toLowerCase()
        if (nameA < nameB) //sort string ascending
            return -1
        if (nameA > nameB)
            return 1
        return 0 //default return value (no sorting)
    })
    others.sort(function (a, b) {
        var nameA = a.supplier_city_name.toLowerCase(),
            nameB = b.supplier_city_name.toLowerCase()
        if (nameA < nameB) //sort string ascending
            return -1
        if (nameA > nameB)
            return 1
        return 0 //default return value (no sorting)
    })
    return (first.concat(others));
}
/**Sorting Function for AutoCompelte End**/

/**UUID Generation Start**/
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
/**UUID Generation End**/