/* Booking form JS
============================================================== */
$("ul.nav-tabs a").click(function (e) {
  e.preventDefault();  
    $(this).tab('show');
});
/*----------------------------------------------------*/
// Select2
/*----------------------------------------------------*/

// jQuery(document).ready(function($){
//     $('.myselect').select2({minimumResultsForSearch: Infinity,'width':'100%'});
//     $('b[role="presentation"]').hide();
//     $('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>');
// });


/***************************************************************
		BEGIN: VARIOUS DATEPICKER & SPINNER INITIALIZATION
***************************************************************/
$(document).ready(function () {
	$( function() {
    var dateFormat = "dd/mm/yy",
      from = $( "#from, #from1, #from2, #from3, #from4, #from5, #from6" )
        .datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to, #to1, #to2, #to3, #to4, #to5, #to6" ).datepicker({
        minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
});

/* Scroll To Top JS
============================================================== */
// jQuery(window).scroll(function () {
// 	scrollToTop('show');
// });

// jQuery(document).ready(function () {
// 	scrollToTop('click');
// });
$(document).ready(function()
{
    $(window).scroll(function()
    {
        if ($(this).scrollTop() > 100)
        {
            $('.scrollToTop').fadeIn();
        }
        else
        {
            $('.scrollToTop').fadeOut();
        }
    });
    $('.scrollToTop').click(function()
    {
        $('html, body').animate(
        {
            scrollTop: 0
        }, 800);
        return false;
    });
});

/* Animated Function */
// function scrollToTop(i) {
// 	if (i == 'show') {
// 		if (jQuery(this).scrollTop() != 0) {
// 			jQuery('#toTop').fadeIn();
// 		} else {
// 			jQuery('#toTop').fadeOut();
// 		}
// 	}
// 	if (i == 'click') {
// 		jQuery('#toTop').click(function () {
// 			jQuery('body,html').animate({
// 				scrollTop: 0
// 			}, 500);
// 			return false;
// 		});
// 	}
// }



/* Deals Slider JS
============================================================== */
    // $(document).ready(function() {

    //   $("#owl-demo-2").owlCarousel({
    //     items: 3,
    //     lazyLoad: true,
    //     loop: true,
    //     margin: 30,
		// autoPlay: true,
    //     navigation : true,
    //      itemsDesktop : [991, 2],
    //     itemsDesktopSmall : [979, 2],
    //     itemsTablet : [768, 2],
		// itemsMobile : [640, 1],  
    //   });
    //       $( ".owl-prev").html('<i class="fa fa-chevron-left"></i>');
    //      $( ".owl-next").html('<i class="fa fa-chevron-right"></i>');

    // });

/* Package Slider JS
============================================================== */
    // $(document).ready(function() {

    //   $("#owl-demo-1").owlCarousel({
    //     items: 2,
    //     lazyLoad: true,
    //     loop: true,
    //     margin: 30,
		// autoPlay: true,
    //     navigation : true,
    //      itemsDesktop : [1024, 1],
    //     itemsDesktopSmall : [979, 2],
    //     itemsTablet : [768, 2],
		// itemsMobile : [640, 1],  
    //   });
    //       $( ".owl-prev").html('<i class="fa fa-chevron-left"></i>');
    //      $( ".owl-next").html('<i class="fa fa-chevron-right"></i>');
    // });
		

/*---select-box-js---*/
    
//     jQuery(document).ready(function($){
//     $('.myselect').select2({minimumResultsForSearch: Infinity,'width':'100%'});
//     $('b[role="presentation"]').hide();
//     $('.select2-selection__arrow').append('<i class="fa  fa-chevron-down"></i>');
// });
// 	    jQuery(document).ready(function($){
//     $('.myselect-2').select2({minimumResultsForSearch: Infinity,'width':'100%'});
//     $('b[role="presentation"]').hide();
//     $('.select2-selection__arrow').append('');
// });


/* Steps Form JS
============================================================== */

// //jQuery time
// var current_fs, next_fs, previous_fs; //fieldsets
// var left, opacity, scale; //fieldset properties which we will animate
// var animating; //flag to prevent quick multi-click glitches

// $(".next").click(function(){
//   debugger
// 	if(animating) return false;
// 	animating = true;
	
// 	current_fs = $(this).parent();
// 	next_fs = $(this).parent().next();
	
// 	//activate next step on progressbar using the index of next_fs
// 	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
// 	//show the next fieldset
// 	next_fs.show(); 
// 	//hide the current fieldset with style
// 	current_fs.animate({opacity: 0}, {
// 		step: function(now, mx) {
// 			//as the opacity of current_fs reduces to 0 - stored in "now"
// 			//1. scale current_fs down to 80%
// 			scale = 1 - (1 - now) * 0.2;
// 			//2. bring next_fs from the right(50%)
// 			left = (now * 50)+"%";
// 			//3. increase opacity of next_fs to 1 as it moves in
// 			opacity = 1 - now;
// 			current_fs.css({
//         'transform': 'scale('+scale+')',
//       });
// 			next_fs.css({'left': left, 'opacity': opacity});
// 		}, 
// 		duration: 100, 
// 		complete: function(){
// 			current_fs.hide();
// 			animating = false;
// 		}, 
// 		//this comes from the custom easing plugin
// 		easing: 'easeInOutBack'
// 	});
// });

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 100, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function(){
	return false;
})

/* Number Spinner JS
============================================================== */
// var numberSpinner = (function() {
//   $('.number-spinner>.ns-btn>a').click(function() {
//     var btn = $(this),
//       oldValue = btn.closest('.number-spinner').find('input').val().trim(),
//       newVal = 0;

//     if (btn.attr('data-dir') === 'up') {
//       newVal = parseInt(oldValue) + 1;
//     } else {
//       if (oldValue > 1) {
//         newVal = parseInt(oldValue) - 1;
//       } else {
//         newVal = 0;
//       }
//     }
//     btn.closest('.number-spinner').find('input').val(newVal);
//   });
//   $('.number-spinner>input').keypress(function(evt) {
//     evt = (evt) ? evt : window.event;
//     var charCode = (evt.which) ? evt.which : evt.keyCode;
//     if (charCode > 31 && (charCode < 48 || charCode > 57)) {
//       return false;
//     }
//     return true;
//   });
// })();

/* Range Slider JS
============================================================== */
var rangeSlider = function(){
  var slider = $('.range-slider'),
      range = $('.range-slider__range'),
      value = $('.range-slider__value');
    
  slider.each(function(){

    value.each(function(){
      var value = $(this).prev().attr('value');
      $(this).html(value);
    });

    range.on('input', function(){
      $(this).next(value).html(this.value);
    });
  });
};

rangeSlider();


/* Calendar JS
============================================================== */
$(document).ready(function () {
	$( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#from" )
        .datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar-o'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to" ).datepicker({
        minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar-o'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
});


