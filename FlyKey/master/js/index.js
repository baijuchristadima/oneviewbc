generalInformation = {
    systemSettings: {
        calendarDisplay: 1,

    },
};
const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})

var flightserchfromComponent = Vue.component('flightserch', {
    data() {
        return {
            access_token: '',
            KeywordSearch: '',
            resultItemsarr: [],
            autoCompleteProgress: false,
            highlightIndex: 0
        }
    },
    props: {
        itemText: String,
        itemId: String,
        placeHolderText: String,
        returnValue: Boolean,
        id: { type: String, default: '', required: false },
        leg: Number,
        //KeywordSearch:String
    },

    template: `<div class="autocomplete">
    <span class="clearable">
        <input type="text" :placeholder="placeHolderText" :id="id" :data-aircode="KeywordSearch" autocomplete="off" 
        v-model="KeywordSearch" class="form-control" 
        :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !autoCompleteProgress  }" 
        @input="onSelectedAutoCompleteEvent($event)"
            @keydown.down="down"
            @keydown.up="up"           
            @keydown.esc="autoCompleteProgress=false"
            @keydown.enter="onSelected(resultItemsarr[highlightIndex])"
            @keydown.tab="tabclick(resultItemsarr[highlightIndex])"/>
            <i :class="'clearable__clear'+id" class="clearable__clear">&times;</i></span>
        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>        
        <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length>0">
            <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item)">
                {{ item.label }}
            </li>
        </ul>
    </div>`,

    mounted: function() {
        setTimeout(() => {
            $(".clearable").each(function() {
            
            const $inp = $(this).find("input:text"),
                $cle = $(this).find(".clearable__clear");

            $inp.on("input", function(){
                $cle.toggle(!!this.value);
            });
            
            $cle.on("touchstart click", function(e) {
                e.preventDefault();
                $inp.val("").trigger("input");
            });
            
            });
        }, 100);
    },
    methods: {
        up: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex < this.resultItemsarr.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
            if (this.$refs.options[this.highlightIndex]) {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            }

        },
        onSelectedAutoCompleteEvent: _.debounce(async function (event) {
            var self = this;
            var keywordEntered = event.target.value;
            if (keywordEntered.length > 2) {
                this.autoCompleteProgress = true;
                self.resultItemsarr = await getFlightResultUsingElasticSearch(keywordEntered);

            } else {
                this.autoCompleteProgress = false;
                this.resultItemsarr = [];

            }
        }, 100),
        onSelected: function (item) {
            this.KeywordSearch = item.label;
            this.resultItemsarr = [];
            var targetWhenClicked = $(event.target).parent().parent().find("input").attr("id");
                if (maininstance.triptype != 'M') {
                if (event.target.id == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
                    $('#Cityto1').focus();
                } else if (event.target.id == "Cityto1" || targetWhenClicked == "Cityto1") {
                    $('#from-1').focus();
                }
            } else {
                var eventTarget = event.target.id;
                $(document).ready(function () {
                    if (eventTarget == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
                        $('#Cityto1').focus();
                    } else if (eventTarget == "Cityto1" || targetWhenClicked == "Cityto1") {
                        $('#from-1').focus();
                    } else if (eventTarget == "DeparturefromLeg1" || targetWhenClicked == "DeparturefromLeg1") {
                        $('#ArrivalfromLeg1').focus();
                    } else if (eventTarget == "ArrivalfromLeg1" || targetWhenClicked == "ArrivalfromLeg1") {
                        $('#txtLeg1Date').focus();
                    } else if (eventTarget == "DeparturefromLeg2" || targetWhenClicked == "DeparturefromLeg2") {
                        $('#ArrivalfromLeg2').focus();
                    } else if (eventTarget == "ArrivalfromLeg2" || targetWhenClicked == "ArrivalfromLeg2") {
                        $('#txtLeg2Date').focus();
                    } else if (eventTarget == "DeparturefromLeg3" || targetWhenClicked == "DeparturefromLeg3") {
                        $('#ArrivalfromLeg3').focus();
                    } else if (eventTarget == "ArrivalfromLeg3" || targetWhenClicked == "ArrivalfromLeg3") {
                        $('#txtLeg3Date').focus();
                    } else if (eventTarget == "DeparturefromLeg4" || targetWhenClicked == "DeparturefromLeg4") {
                        $('#ArrivalfromLeg4').focus();
                    } else if (eventTarget == "ArrivalfromLeg4" || targetWhenClicked == "ArrivalfromLeg4") {
                        $('#txtLeg4Date').focus();
                    } else if (eventTarget == "DeparturefromLeg5" || targetWhenClicked == "DeparturefromLeg5") {
                        $('#ArrivalfromLeg5').focus();
                    } else if (eventTarget == "ArrivalfromLeg5" || targetWhenClicked == "ArrivalfromLeg5") {
                        $('#txtLeg5Date').focus();
                    } else if (eventTarget == "DeparturefromLeg6" || targetWhenClicked == "DeparturefromLeg6") {
                        $('#ArrivalfromLeg6').focus();
                    } else if (eventTarget == "ArrivalfromLeg6" || targetWhenClicked == "ArrivalfromLeg6") {
                        $('#txtLeg6Date').focus();
                    }
                });

            }

            this.$emit('air-search-completed', item.code, item.label, this.leg);


        },
        tabclick: function (item) {
            if (!item) {

            } else {
                this.onSelected(item);
            }
        }

    },
    watch: {
        returnValue: function () {
            this.KeywordSearch = this.itemText;
        }

    }

});
Vue.component('loading', VueLoading)
var maininstance = new Vue({
    i18n,
    el: '#home',
    name: 'home',
    data: {
        banner: {},
        bookingForm: {},
        dealsSection: {},
        packageSection: {},
        partnersSection: {},
        packages: {},
        deals: {},
        validationList: {},
        flightSearchCityName: { cityFrom1: '', cityTo1: '', cityFrom2: '', cityTo2: '', cityFrom3: '', cityTo3: '', cityFrom4: '', cityTo4: '', cityFrom5: '', cityTo5: '', cityFrom6: '', cityTo6: '' },
        CityFrom: '',
        CityTo: '',
        selected_adults: 1,
        selected_children: 0,
        selected_infant: 0,
        totalAllowdPax: 9,
        travelInfo: false,
        Totaltravaller: '1 Traveller(s) & Economy',
        returnValue: true,
        triptype: "R",
        direct_flight: false,
        selected_cabin: 'Y',
        preferAirline: '',
        legcount: 2,
        cityList: [],
        travellerdisplymul: false,
        travellerdisply:false,
        hotelInit: Math.random(), //hotel init
        startDate: '',
        endDate: '',
        message: '',
        destination: '',
        agencyCode: '',
        Totalpassenger: '1 Passenger',
        passenger: '',
        isLoading: false,
        fullPage: true,
        actvetab: sessionStorage.active_el ? (sessionStorage.active_el == 0 || sessionStorage.active_el == 3) ? 1 : sessionStorage.active_el : 1,
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'AED',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        cruisedisply:false,
        transferdisply:false,
        activitydisply:false,
        child: 0,
        childLabel: "",
        childrenLabel: "",
        adultLabel: "",
        adultsLabel: "",
        ageLabel: "",
        agesLabel: "",
        infantLabel: "",
        adults: [
            {'value': 0, 'text': '0 Adult' },
            { 'value': 1, 'text': '1 Adult' },
            { 'value': 2, 'text': '2 Adults' },
            { 'value': 3, 'text': '3 Adults' },
            { 'value': 4, 'text': '4 Adults' },
            { 'value': 5, 'text': '5 Adults' },
            { 'value': 6, 'text': '6 Adults' },
            { 'value': 7, 'text': '7 Adults' },
            { 'value': 8, 'text': '8 Adults' },
            { 'value': 9, 'text': '9 Adults' }
        ],
        children: [
            { 'value': 0, 'text': 'Children' },
            { 'value': 1, 'text': '1 Child' },
            { 'value': 2, 'text': '2 Children' },
            { 'value': 3, 'text': '3 Children' },
            { 'value': 4, 'text': '4 Children' },
            { 'value': 5, 'text': '5 Children' },
            { 'value': 6, 'text': '6 Children' },
            { 'value': 7, 'text': '7 Children' },
            { 'value': 8, 'text': '8 Children' }

        ],
        infants: [
            { 'value': 0, 'text': 'Infants' },
            { 'value': 1, 'text': '1 Infant' }

        ],
        countryList:[],
        selectedcountry:'',
        appSection:{},
        visa:{
            fullName:'',
            nationality:'',
            country:'',
            phoneNumber:'',
            email:'',
            whereDidYouHear:''
        },
        countryLists: countryList,
        services: 'flight'
    },
    methods: {
        getValidationMsgByCode: function (code) {
            if (sessionStorage.validationItems !== undefined) {
                var validationList = JSON.parse(sessionStorage.validationItems);
                for (let validationItem of validationList.Validation_List) {
                    if (code === validationItem.Code) {
                        return validationItem.Message;
                    }
                }
            }
        },
        pageContent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                if (langauage == "ar") {
                    self.Totaltravaller = "1 مسافر واقتصاد";
                    self.Totalpassenger = "1 راكب";
                    //selected_cabin="تحديد"
                }
                var homePageURL = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Home/Home/Home.ftl';
                axios.get(homePageURL, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    if (response.data.area_List.length) {
                        var bannerDetails = self.pluck('Banner_Section', response.data.area_List);
                        self.banner = self.getAllMapData(bannerDetails[0].component);
                        //selected_cabin="Select"
                        var Form = self.pluck('Booking_Search_Section', response.data.area_List);
                        self.bookingForm = self.getAllMapData(Form[0].component);
                        setTimeout(() => {
                            initSelect2();
                            $('.fotorama').fotorama();
                        }, 100);
                               
                        self.passenger = self.pluckcom('Passenger_Label', Form[0].component);

                        var dealsArea = self.pluck('Deals_Section', response.data.area_List);
                        self.dealsSection = self.getAllMapData(dealsArea[0].component);

                        var packageArea = self.pluck('Package_Section', response.data.area_List);
                        self.packageSection = self.getAllMapData(packageArea[0].component);

                        var partnersArea = self.pluck('Partners_Section', response.data.area_List);
                        self.partnersSection = self.getAllMapData(partnersArea[0].component);
                        
                        setTimeout(function () { partnersCarousel() }, 100);

                        var appsection = self.pluck('App_Section', response.data.area_List);
                        self.appSection = self.getAllMapData(appsection[0].component);
                    }
                    self.stopLoader();
                }).catch(function (error) {
                    console.log('Error');
                    self.stopLoader();
                });

            });
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            contentArry.map(function (item) {
                let allKeys = Object.keys(item)
                for (let j = 0; j < allKeys.length; j++) {
                    let key = allKeys[j];
                    let value = item[key];

                    if (key != 'name' && key != 'type') {
                        if (value == undefined || value == null) {
                            value = "";
                        }
                        tempDataObject[key] = value;
                    }
                }
            });
            return tempDataObject;
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPackageconfig: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = commonPath + Agencycode + '/Template/Package Configuration/Package Configuration/Package Configuration.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (res) {
                    console.log(res);
                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var val = pluck('Configurations', self.content);
                        self.countryList = self.pluckcom('Country_List', val[0].component); 
                    }
                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });
            });
        },
        // searchPackage:function(countrydetails){
        //     if(!countrydetails){
        //         alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M049')).set('closable', false);
        //         return false;
        //     }
        //     else{
        //         var searchCriteria =
        //         "countrycode=" + countrydetails.Country_Code +
        //         "&img=" + countrydetails.Country_Banner_Image_1934_x_560px + 
        //         "&CName=" + countrydetails.Country_Name;
        //         searchCriteria = searchCriteria.split(' ').join('-');
        //         var uri = "/FlyKey/package.html?" + searchCriteria;
        //         window.location.href = uri;
        //     }
        
        // },
        searchPackage(name,id) {
            if (name != null && id != null) {
                if (name != "" && id !="") {
                    url = "detail-request.html?id=" + id + "&name=" +name;
                    window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;
        },
        holidayPackageCard: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/List Of Packages/List Of Packages/List Of Packages.ftl';

                axios.get(topackageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    var packageData = response.data;
                    let holidayaPackageListTemp = [];
                    if (packageData != undefined && packageData.Values != undefined) {
                        holidayaPackageListTemp = packageData.Values.filter(function (el) {
                            return el.Status == true;
                        });
                    }
                    self.packages = holidayaPackageListTemp;
                    // self.CountryList = self.packages;
                   // self.packages = packageData.Values;
                    setTimeout(function () { carouselPack2() }, 1000);
                }).catch(function (error) {
                    console.log('Error');
                });
            });

        },
        getDeals: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Deals/Deals/Deals.ftl';
                axios.get(topackageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    var packageData = response.data;
                    let dealsListTemp = [];
                    if (packageData != undefined && packageData.Values != undefined) {
                        dealsListTemp = packageData.Values.filter(function (el) {
                            return el.Is_Active == true;
                        });
                    }
                    self.deals = dealsListTemp;

                    setTimeout(function () { carouselDeals() }, 10);
                }).catch(function (error) {
                    console.log('Error');
                });
            });

        },
        Departurefrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (AirportCode == this.CityTo) {
                    this.returnValue = false;
                    alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M010')).set('closable', false);

                    this.CityFrom = '';

                } else {
                    this.returnValue = true;
                    this.CityFrom = AirportCode;
                }
            } else {
                var from = 'cityFrom' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].to) {
                        this.returnValue = false;
                        this.flightSearchCityName[from] = "";
                        alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M010')).set('closable', false);
                    } else {
                        this.cityList[index].from = AirportCode
                        this.flightSearchCityName[from] = AirportName;
                        this.returnValue = true;
                    }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: AirportCode,
                        to: ''

                    });
                    this.flightSearchCityName[from] = AirportName;
                    this.returnValue = true;
                }
            }


        },
        Arrivalfrom(AirportCode, AirportName, leg) {
            if (leg == 0) {
                if (this.CityFrom == AirportCode) {
                    this.returnValue = false;
                    alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M010')).set('closable', false);
                    this.validationMessage = "";
                    this.CityTo = '';

                } else {
                    this.returnValue = true;
                    this.CityTo = AirportCode;
                }
            } else {
                var to = 'cityTo' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].from) {
                        this.returnValue = false;
                        this.flightSearchCityName[to] = "";
                        alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M010')).set('closable', false);

                    } else {
                        this.cityList[index].to = AirportCode;
                        this.flightSearchCityName[to] = AirportName;
                        this.returnValue = true;
                    }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: '',
                        to: AirportCode

                    });
                    this.flightSearchCityName[to] = AirportName;
                    this.returnValue = true;
                }
            }
        },
        setCalender() {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = generalInformation.systemSettings.calendarDisplay;
            $("#deptDate01").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: { direction: "up" },
                onSelect: function (selectedDate) {
                    $("#retDate").datepicker("option", "minDate", selectedDate);

                }
            });
            // $("#deptDate02").datepicker({
            //     minDate: "dateToday",
            //     numberOfMonths: 1,
            //     showOn: "both",
            //     buttonText: " ",
            //     duration: "fast",
            //     showAnim: "slide",
            //     showOptions: { direction: "up" },
            //     onSelect: function (selectedDate) {
            //         $("#deptDate03").datepicker("option", "minDate", selectedDate);

            //     }
            // });
            // $("#deptDate03").datepicker({
            //     minDate: "dateToday",
            //     numberOfMonths: 1,
            //     showOn: "both",
            //     buttonText: " ",
            //     duration: "fast",
            //     showAnim: "slide",
            //     showOptions: { direction: "up" },
            //     onSelect: function (selectedDate) {
            //         $("#retDate").datepicker("option", "minDate", selectedDate);

            //     }
            // });
            if($('#deptDate02, #deptDate03').length){
                // check if element is available to bind ITS ONLY ON HOMEPAGE
                var currentDate = moment().format("MM/DD/YYYY");
            
                $('#deptDate02, #deptDate03').daterangepicker({
                    locale: {
                        format: 'MM/DD/YYYY'
                    },
                    alwaysShowCalendars: true,
                    minDate: currentDate,
                    autoApply: true,
                    autoUpdateInput: false,
                }, function(start, end, label) {
                // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
                // Lets update the fields manually this event fires on selection of range
                var selectedStartDate = start.format('MM/DD/YYYY'); // selected start
                var selectedEndDate = end.format('MM/DD/YYYY'); // selected end
            
                $checkinInput = $('#deptDate02');
                $checkoutInput = $('#deptDate03');
            
                // Updating Fields with selected dates
                $checkinInput.val(selectedStartDate);
                $checkoutInput.val(selectedEndDate);
            
                // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
                var checkOutPicker = $checkoutInput.data('daterangepicker');
                checkOutPicker.setStartDate(selectedStartDate);
                checkOutPicker.setEndDate(selectedEndDate);
            
                // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
                var checkInPicker = $checkinInput.data('daterangepicker');
                checkInPicker.setStartDate(selectedStartDate);
                checkInPicker.setEndDate(selectedEndDate);
            
                });
            
            } // End Daterange Picker
            // $("#deptDate04").datepicker({
            //     minDate: "dateToday",
            //     numberOfMonths: 1,
            //     showOn: "both",
            //     buttonText: " ",
            //     duration: "fast",
            //     showAnim: "slide",
            //     showOptions: { direction: "up" },
            //     onSelect: function (selectedDate) {
            //         $("#deptDate05").datepicker("option", "minDate", selectedDate);

            //     }
            // });
            // $("#deptDate05").datepicker({
            //     minDate: "dateToday",
            //     numberOfMonths: 1,
            //     showOn: "both",
            //     buttonText: " ",
            //     duration: "fast",
            //     showAnim: "slide",
            //     showOptions: { direction: "up" },
            //     onSelect: function (selectedDate) {
            //         $("#retDate").datepicker("option", "minDate", selectedDate);

            //     }
            // });
            if($('#deptDate04, #deptDate05').length){
                // check if element is available to bind ITS ONLY ON HOMEPAGE
                var currentDate = moment().format("MM/DD/YYYY");
            
                $('#deptDate04, #deptDate05').daterangepicker({
                    locale: {
                        format: 'MM/DD/YYYY'
                    },
                    alwaysShowCalendars: true,
                    minDate: currentDate,
                    autoApply: true,
                    autoUpdateInput: false,
                }, function(start, end, label) {
                // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
                // Lets update the fields manually this event fires on selection of range
                var selectedStartDate = start.format('MM/DD/YYYY'); // selected start
                var selectedEndDate = end.format('MM/DD/YYYY'); // selected end
            
                $checkinInput = $('#deptDate04');
                $checkoutInput = $('#deptDate05');
            
                // Updating Fields with selected dates
                $checkinInput.val(selectedStartDate);
                $checkoutInput.val(selectedEndDate);
            
                // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
                var checkOutPicker = $checkoutInput.data('daterangepicker');
                checkOutPicker.setStartDate(selectedStartDate);
                checkOutPicker.setEndDate(selectedEndDate);
            
                // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
                var checkInPicker = $checkinInput.data('daterangepicker');
                checkInPicker.setStartDate(selectedStartDate);
                checkInPicker.setEndDate(selectedEndDate);
            
                });
            
            } // End Daterange Picker
            $("#deptDate06").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: { direction: "up" },
                onSelect: function (selectedDate) {
                    $("#deptDate07").datepicker("option", "minDate", selectedDate);

                }
            });
            $("#deptDate07").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: { direction: "up" },
                onSelect: function (selectedDate) {
                    $("#retDate").datepicker("option", "minDate", selectedDate);

                }
            });
            $("#retDate").datepicker({
                minDate: "dateToday",
                numberOfMonths: 1,
                showOn: "both",
                buttonText: " ",
                duration: "fast",
                showAnim: "slide",
                showOptions: { direction: "up" },
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) { }
            });


            // $("#txtLeg1Date").datepicker({
            //     minDate: "dateToday",
            //     numberOfMonths: 1,
            //     showOn: "both",
            //     buttonText: " ",
            //     duration: "fast",
            //     showAnim: "slide",
            //     showOptions: { direction: "up" },
            //     dateFormat: systemDateFormat,
            //     onSelect: function (selectedDate) {
            //         $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);

            //     }
            // });
            $('#txtLeg1Date').daterangepicker({
                locale: {
                      format: 'MM/DD/YYYY'
                },
                autoApply: true,
                singleDatePicker: true,
                minDate: moment()
            }, function(start, end, label) {
                var selectedStartDate = start.format('MM/DD/YYYY'); // selected start
                $checkinInput = $('#txtLeg2Date');
                var checkInPicker = $checkinInput.data('daterangepicker');
            
                $checkinInput.val(selectedStartDate);
                checkInPicker.setStartDate(selectedStartDate);
                checkInPicker.setEndDate(selectedStartDate);
                checkInPicker.minDate = moment(start.format('MM/DD/YYYY'));

            });
            $('#txtLeg2Date').daterangepicker({
                locale: {
                      format: 'MM/DD/YYYY'
                },
                autoApply: true,
                singleDatePicker: true,
                minDate: $('#txtLeg1Date').val()
            }, function(start, end, label) {
                var selectedStartDate = start.format('MM/DD/YYYY'); // selected start
                $checkinInput = $('#txtLeg3Date');
                var checkInPicker = $checkinInput.data('daterangepicker');
                if (checkInPicker) {
                    $checkinInput.val(selectedStartDate);
                    checkInPicker.setStartDate(selectedStartDate);
                    checkInPicker.setEndDate(selectedStartDate);
                    checkInPicker.minDate = moment(start.format('MM/DD/YYYY'));
                }
            });
            // $("#txtLeg2Date").datepicker({
            //     minDate: "dateToday",
            //     numberOfMonths: 1,
            //     showOn: "both",
            //     buttonText: " ",
            //     duration: "fast",
            //     showAnim: "slide",
            //     showOptions: { direction: "up" },
            //     dateFormat: systemDateFormat,
            //     beforeShow: function (event, ui) {
            //         var selectedDate = $("#txtLeg1Date").val();
            //         $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
            //     },
            //     onSelect: function (selectedDate) {
            //         $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
            //         $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
            //         $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
            //         $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            //     }
            // });

            // $("#txtLeg3Date").datepicker({
            //     minDate: "dateToday",
            //     numberOfMonths: 1,
            //     showOn: "both",
            //     buttonText: " ",
            //     duration: "fast",
            //     showAnim: "slide",
            //     showOptions: { direction: "up" },
            //     dateFormat: systemDateFormat,
            //     beforeShow: function (event, ui) {
            //         var selectedDate = $("#txtLeg2Date").val();
            //         $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
            //     },
            //     onSelect: function (selectedDate) {
            //         $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
            //         $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
            //         $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            //     }
            // });
            $('#txtLeg3Date').daterangepicker({
                locale: {
                      format: 'MM/DD/YYYY'
                },
                autoApply: true,
                singleDatePicker: true,
                minDate: $('#txtLeg2Date').val()
            }, function(start, end, label) {
                var selectedStartDate = start.format('MM/DD/YYYY'); // selected start
                $checkinInput = $('#txtLeg4Date');
                var checkInPicker = $checkinInput.data('daterangepicker');
            
                if (checkInPicker) {
                    $checkinInput.val(selectedStartDate);
                    checkInPicker.setStartDate(selectedStartDate);
                    checkInPicker.setEndDate(selectedStartDate);
                    checkInPicker.minDate = moment(start.format('MM/DD/YYYY'));
                }
            });
            // $("#txtLeg4Date").datepicker({
            //     minDate: "dateToday",
            //     numberOfMonths: 1,
            //     showOn: "both",
            //     buttonText: " ",
            //     duration: "fast",
            //     showAnim: "slide",
            //     showOptions: { direction: "up" },
            //     dateFormat: systemDateFormat,
            //     beforeShow: function (event, ui) {
            //         var selectedDate = $("#txtLeg3Date").val();
            //         $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
            //     },
            //     onSelect: function (selectedDate) {
            //         $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
            //         $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            //     }
            // });

            $('#txtLeg4Date').daterangepicker({
                locale: {
                      format: 'MM/DD/YYYY'
                },
                autoApply: true,
                singleDatePicker: true,
                minDate: $('#txtLeg3Date').val()
            }, function(start, end, label) {
                var selectedStartDate = start.format('MM/DD/YYYY'); // selected start
                $checkinInput = $('#txtLeg5Date');
                var checkInPicker = $checkinInput.data('daterangepicker');
            
                if (checkInPicker) {
                    $checkinInput.val(selectedStartDate);
                    checkInPicker.setStartDate(selectedStartDate);
                    checkInPicker.setEndDate(selectedStartDate);
                    checkInPicker.minDate = moment(start.format('MM/DD/YYYY'));
                }
            });
            // $("#txtLeg5Date").datepicker({
            //     minDate: "dateToday",
            //     numberOfMonths: 1,
            //     showOn: "both",
            //     buttonText: " ",
            //     duration: "fast",
            //     showAnim: "slide",
            //     showOptions: { direction: "up" },
            //     dateFormat: systemDateFormat,
            //     beforeShow: function (event, ui) {
            //         var selectedDate = $("#txtLeg4Date").val();
            //         $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
            //     },
            //     onSelect: function (selectedDate) {
            //         $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            //     }
            // });
            $('#txtLeg5Date').daterangepicker({
                locale: {
                      format: 'MM/DD/YYYY'
                },
                autoApply: true,
                singleDatePicker: true,
                minDate: $('#txtLeg4Date').val()
            }, function(start, end, label) {
                var selectedStartDate = start.format('MM/DD/YYYY'); // selected start
                $checkinInput = $('#txtLeg6Date');
                var checkInPicker = $checkinInput.data('daterangepicker');
            
                if (checkInPicker) {
                    $checkinInput.val(selectedStartDate);
                    checkInPicker.setStartDate(selectedStartDate);
                    checkInPicker.setEndDate(selectedStartDate);
                    checkInPicker.minDate = moment(start.format('MM/DD/YYYY'));
                }
            });
            // $("#txtLeg6Date").datepicker({
            //     minDate: "dateToday",
            //     numberOfMonths: 1,
            //     showOn: "both",
            //     buttonText: " ",
            //     duration: "fast",
            //     showAnim: "slide",
            //     showOptions: { direction: "up" },
            //     dateFormat: systemDateFormat,
            //     beforeShow: function (event, ui) {
            //         var selectedDate = $("#txtLeg5Date").val();
            //         $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
            //     },
            //     onSelect: function (selectedDate) { }
            // });
            $('#txtLeg6Date').daterangepicker({
                locale: {
                      format: 'MM/DD/YYYY'
                },
                autoApply: true,
                singleDatePicker: true,
                minDate: $('#txtLeg5Date').val()
            }, function(start, end, label) {
                // var selectedStartDate = start.format('MM/DD/YYYY'); // selected start
                // $checkinInput = $('#txtLeg3Date');
                // var checkInPicker = $checkinInput.data('daterangepicker');
            
                // $checkinInput.val(selectedStartDate);
                // checkInPicker.setStartDate(selectedStartDate);
                // checkInPicker.setEndDate(selectedStartDate);
            });
            
            if (this.triptype == 'O') {
                if ($('#search_checkin').length) {
    
                    $('#search_checkin').daterangepicker({
                        locale: {
                              format: 'MM/DD/YYYY'
                        },
                        autoApply: true,
                        minDate: moment(),
                        singleDatePicker: true,
                        autoUpdateInput: false,
                    }, function(start, end, label) {
                        var selectedStartDate = start.format('MM/DD/YYYY'); // selected start
                        $checkinInput = $('#search_checkin');
                        $checkinInput.val(selectedStartDate);
                
                        // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
                        var checkInPicker = $checkinInput.data('daterangepicker');
                        checkInPicker.setStartDate(selectedStartDate);
                        checkInPicker.setEndDate(selectedStartDate);
                    });

                }
            } else {
                // $("#search_checkin").datepicker({
                //     minDate: "0d",
                //     maxDate: "360d",
                //     numberOfMonths: numberofmonths,
                //     changeMonth: false,
                //     showButtonPanel: false,
                //     dateFormat: systemDateFormat,
                //     onSelect: function (selectedDate) {
                //         $("#search_checkout").datepicker("option", "minDate", selectedDate);
    
                //     }
                // });
    
                // $("#search_checkout").datepicker({
                //     minDate: "0d",
                //     maxDate: "360d",
                //     numberOfMonths: numberofmonths,
                //     changeMonth: false,
                //     showButtonPanel: false,
                //     dateFormat: systemDateFormat,
                //     beforeShow: function (event, ui) {
                //         var selectedDate = $("#search_checkin").val();
                //         $("#search_checkout").datepicker("option", "minDate", selectedDate);
                //     },
                //     onSelect: function (selectedDate) { }
                // });
    
                if($('#search_checkin, #search_checkout').length){
                    // check if element is available to bind ITS ONLY ON HOMEPAGE
                    var currentDate = moment().format("MM/DD/YYYY");
                
                    $('#search_checkin, #search_checkout').daterangepicker({
                        locale: {
                            format: 'MM/DD/YYYY'
                        },
                        alwaysShowCalendars: true,
                        minDate: currentDate,
                        autoApply: true,
                        autoUpdateInput: false,
                    }, function(start, end, label) {
                    // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
                    // Lets update the fields manually this event fires on selection of range
                    var selectedStartDate = start.format('MM/DD/YYYY'); // selected start
                    var selectedEndDate = end.format('MM/DD/YYYY'); // selected end
                
                    $checkinInput = $('#search_checkin');
                    $checkoutInput = $('#search_checkout');
                
                    // Updating Fields with selected dates
                    $checkinInput.val(selectedStartDate);
                    $checkoutInput.val(selectedEndDate);
                
                    // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
                    var checkOutPicker = $checkoutInput.data('daterangepicker');
                    checkOutPicker.setStartDate(selectedStartDate);
                    checkOutPicker.setEndDate(selectedEndDate);
                
                    // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
                    var checkInPicker = $checkinInput.data('daterangepicker');
                    checkInPicker.setStartDate(selectedStartDate);
                    checkInPicker.setEndDate(selectedEndDate);
                
                    });
                
                } // End Daterange Picker
            }

        },
        swapLocations: function (id) {

            if ((this.CityFrom) && (this.CityTo)) {
                var from = this.CityFrom;
                var to = this.CityTo;
                this.CityFrom = to;
                this.CityTo = from;
                swpaloc(id)
            }
        },
        showhide: function () {

            this.travelInfo = !this.travelInfo;
        },
        SearchFlight: function () {
            var Departuredate = $('#search_checkin').val() == "" ? "" : $('#search_checkin').val();
            if (!this.CityFrom) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M001')).set('closable', false);

                return false;
            }
            if (this.selected_cabin==null||this.selected_cabin=='') {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M041')).set('closable', false);

                return false;
            }  if (this.selected_adults==0) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M042')).set('closable', false);

                return false;
            }
            if (!this.CityTo) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M002')).set('closable', false);
                return false;
            }
            if (!Departuredate) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M003')).set('closable', false);
                return false;
            }
            var sec1TravelDate = moment(Departuredate).format('DD|MM|YYYY');

            var sectors = this.CityFrom + '-' + this.CityTo + '-' + sec1TravelDate;

            if (this.triptype == 'R') {
                var ArrivalDate = $('#search_checkout').val() == "" ? "" : $('#search_checkout').val();
                if (!isNullorUndefined(ArrivalDate)) {
                    ArrivalDate = moment(ArrivalDate).format('DD|MM|YYYY');
                    if (!ArrivalDate) {
                        alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M004')).set('closable', false);

                        return false;
                    } else {
                        sectors += '/' + this.CityTo + '-' + this.CityFrom + '-' + ArrivalDate;
                    }
                } else {
                    alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M004')).set('closable', false);

                    return false;
                }
            }
            var directFlight = this.direct_flight ? 'DF' : 'AF';
            var adult = this.selected_adults;
            var child = this.selected_children;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            getSuppliers([this.CityFrom + '|' + this.CityTo],
            function(supp) {
                var searchUrl = '/Flights/flight-listing.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                // searchUrl = searchUrl.toLocaleLowerCase();
                sessionStorage.active_el = 1;
                window.location.href = searchUrl;
            })
        },
        setTripType: function (type) {
            this.triptype = type;
            if (this.triptype == 'M') {
                $('#txtLeg1Date, #txtLeg2Date').val("");
            }
            $('#search_checkin').val("");
            $('#search_checkout').val("");
        },
        DeleteLeg(leg) {
            var legs = this.legcount;
            if (legs > 1) {
                --this.legcount;
                var legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    this.cityList.splice(index, 1);

                }


            }
        },
        AddNewLeg() {
            if (this.legcount <= 5) {
                ++this.legcount;
                var legno = 1;
                if (this.cityList.length > 0) {
                    legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                    legno = legno + 1;
                }

                this.cityList.push({
                    id: legno,
                    from: '',
                    to: ''
                });
            }
            },
        clickoutside: function () {
            this.triptype = 'R';
           // window.location.reload();
        },
        MultiSearchFlight: function () {
            var sectors = '';
            var legDetails = [];
            for (var legValue = 1; legValue <= this.legcount; legValue++) {
                var temDeparturedate = $('#txtLeg' + (legValue) + 'Date').val() == "" ? "" : $('#txtLeg' + (legValue) + 'Date').val();
                if (temDeparturedate != "" && this.cityList.length != 0 && this.cityList[legValue - 1].from != "" && this.cityList[legValue - 1].to != "") {
                    var departureFrom = this.cityList[legValue - 1].from;
                    var arrivalTo = this.cityList[legValue - 1].to;
                    var travelDate = moment(temDeparturedate).format('DD|MM|YYYY');
                    sectors += '/' + departureFrom + '-' + arrivalTo + '-' + travelDate;
                    legDetails.push(departureFrom + '|' + arrivalTo)
                } else {
                    alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M011') + (legValue) + this.getValidationMsgByCode('M012')).set('closable', false);


                    return false;
                }
            }
            var directFlight = this.direct_flight ? 'DF' : 'AF';

            var adult = this.selected_adults;
            var child = this.selected_children;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            getSuppliers(legDetails,
            function(supp) {
                var searchUrl = '/Flights/flight-listing.html?flight=' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                // searchUrl = searchUrl.toLocaleLowerCase();
                window.location.href = searchUrl;
            })
        },
        SaveCurise: async function () {
            var self = this;
            if (!self.destination) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M005')).set('closable', false);
                return false;
            }
            var startDate = $('#deptDate02').val() == "" ? "" : $('#deptDate02').val();
            if (!startDate) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M006')).set('closable', false);
                return false;
            }
            var endDate = $('#deptDate03').val() == "" ? "" : $('#deptDate03').val();
            if (!endDate) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M007')).set('closable', false);
                return false;
            }
            if (!self.message) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M008')).set('closable', false);
                return false;
            }

            try {
                this.isLoading = true;
                self.agencyCode = JSON.parse(localStorage.User).loginNode.code || ""
                var agencyCode = self.agencyCode;

                try {
                    var agencyCode = self.agencyCode
                    var startDate = $('#deptDate02').val() == "" ? "" : $('#deptDate02').val();

                    var endDate = $('#deptDate03').val() == "" ? "" : $('#deptDate03').val();
                    let insertSubscibeData = {
                        type: "Cruise",
                        date1: moment(String(startDate)).format('YYYY-MM-DDThh:mm:ss'),
                        date2: moment(String(endDate)).format('YYYY-MM-DDThh:mm:ss'),
                        keyword1: self.destination,
                        keyword3: self.message,
                        number1: self.selected_adults,
                        number2: self.selected_children,
                        number3: self.selected_infant,
                        nodeCode: agencyCode
                    };

                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        self.destination = '';
                        self.message = '';
                        $('#deptDate02').val('');
                        $('#deptDate03').val('');
                        this.isLoading = false;
                        alertify.alert(this.getValidationMsgByCode('M023'),this.getValidationMsgByCode('M009')).set('closable', false);
                    } catch (e) {
                        this.isLoading = false;
                    }
                } catch (error) {
                    self.isLoading = false;
                }

            } catch (error) {
                self.isLoading = false;
            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {

            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {

                const myJson = await response.json();
                return myJson;
            } catch (error) {

                return object;
            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {

            var allDBData = [];
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        },
        SaveActivity: async function () {
            var self = this;

            if (!self.destination) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M005')).set('closable', false);
                return false;
            }
            var startDate = $('#deptDate06').val() == "" ? "" : $('#deptDate06').datepicker('getDate');
            if (!startDate) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M006')).set('closable', false);
                return false;
            }
            var endDate = $('#deptDate07').val() == "" ? "" : $('#deptDate07').datepicker('getDate');
            if (!endDate) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M007')).set('closable', false);
                return false;
            }
            if (!self.message) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M008')).set('closable', false);
                return false;
            }

            try {
                this.isLoading = true;
                self.agencyCode = JSON.parse(localStorage.User).loginNode.code || ""
                var agencyCode = self.agencyCode;

                try {
                    var agencyCode = self.agencyCode
                    var startDate = $('#deptDate06').val() == "" ? "" : $('#deptDate06').datepicker('getDate');

                    var endDate = $('#deptDate07').val() == "" ? "" : $('#deptDate07').datepicker('getDate');
                    let insertSubscibeData = {
                        type: "Activities",
                        date1: moment(String(startDate)).format('YYYY-MM-DDThh:mm:ss'),
                        date2: moment(String(endDate)).format('YYYY-MM-DDThh:mm:ss'),
                        keyword1: self.destination,
                        keyword2: self.message,
                        number1: self.selected_adults,
                        number2: self.selected_children,
                        number3: self.selected_infant,
                        nodeCode: agencyCode
                    };

                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        self.destination = '';
                        self.message = '';
                        $('#deptDate06').val('');
                        $('#deptDate07').val('');
                        this.isLoading = false;
                        alertify.alert(this.getValidationMsgByCode('M023'),this.getValidationMsgByCode('M009')).set('closable', false);
                        
                    } catch (e) {
                        this.isLoading = false;
                    }
                } catch (error) {
                    self.isLoading = false;
                }


            } catch (error) {
                self.isLoading = false;
            }
        },
        SaveTransfer: async function () {
            var self = this;
            if (!self.destination) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M005')).set('closable', false);
                return false;
            }
            var startDate = $('#deptDate04').val() == "" ? "" : $('#deptDate04').val();
            if (!startDate) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M006')).set('closable', false);
                return false;
            }
            var endDate = $('#deptDate05').val() == "" ? "" : $('#deptDate05').val();
            if (!endDate) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M007')).set('closable', false);
                return false;
            }
            if (!self.message) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M008')).set('closable', false);
                return false;
            }

            try {
                this.isLoading = true;
                self.agencyCode = JSON.parse(localStorage.User).loginNode.code || ""
                var agencyCode = self.agencyCode;

                try {
                    var agencyCode = self.agencyCode
                    var startDate = $('#deptDate04').val() == "" ? "" : $('#deptDate04').val();

                    var endDate = $('#deptDate05').val() == "" ? "" : $('#deptDate05').val();
                    let insertSubscibeData = {
                        type: "Transfer",
                        date1: moment(String(startDate)).format('YYYY-MM-DDThh:mm:ss'),
                        date2: moment(String(endDate)).format('YYYY-MM-DDThh:mm:ss'),
                        keyword1: self.destination,
                        keyword2: self.message,
                        number1: self.selected_adults,
                        number2: self.selected_children,
                        number3: self.selected_infant,
                        nodeCode: agencyCode
                    };

                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        self.destination = '';
                        self.message = '';
                        $('#deptDate04').val('');
                        $('#deptDate05').val('');
                        this.isLoading = false;
                        alertify.alert(this.getValidationMsgByCode('M023'),this.getValidationMsgByCode('M009')).set('closable', false);
                    } catch (e) {
                        this.isLoading = false;
                    }
                } catch (error) {
                    self.isLoading = false;
                }
                
            } catch (error){
                self.isLoading = false;
            }
        },
        saveVisa: async function () {
            var self = this;
            if (!self.visa.fullName) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M050')).set('closable', false);
                return false;
            }
            if (!self.visa.nationality) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M051')).set('closable', false);
                return false;
            }
            if (!self.visa.country) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M052')).set('closable', false);
                return false;
            }
            if (!self.visa.phoneNumber) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M030')).set('closable', false);
                return false;
            }
            if (!self.visa.email) {
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M026')).set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = self.visa.email.match(emailPat);
            if (matchArray == null) {
                alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M027')).set('closable', false);

                return false;
            }
            // if (!self.visa.whereDidYouHear) {
            //     alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M053')).set('closable', false);
            //     return false;
            // }
            else{
                this.isLoading = true;
                self.agencyCode = JSON.parse(localStorage.User).loginNode.code || ""
                var agencyCode = self.agencyCode;

                try {
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertVisaData = {
                        type: "Visa",
                        date1: requestedDate,
                        keyword1: self.visa.fullName,
                        keyword2: self.visa.nationality,
                        keyword3: self.visa.country,
                        keyword4: self.visa.phoneNumber,
                        text1: self.visa.email,
                        text2: self.visa.whereDidYouHear,
                        nodeCode: agencyCode
                    };
                    var frommail = JSON.parse(localStorage.User).loginNode.email;
                     // var frommail = "itsolutionsoneview@gmail.com";
                    let cusMessage="Thank you for the request. Our Visa Consultants will be in contact with you shortly.";
                    var custmail = {
                        type: "ThankYouRequest",
                        fromEmail: frommail,
                        toEmails: Array.isArray(self.visa.email) ? self.visa.email : [self.visa.email],
                        logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + '.xhtml?ln=logo',
                        agencyName: JSON.parse(localStorage.User).loginNode.name,
                        agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                        personName: self.visa.fullName,
                        message:cusMessage,
                        primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                        secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
                    };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertVisaData, null);
                    try {
                        let insertID = Number(responseObject);
                        self.visa.fullName = '';
                        self.visa.nationality = '';
                        self.visa.country = '';
                        self.visa.phoneNumber = '';
                        self.visa.email = '';
                        self.visa.whereDidYouHear = '';
                        var emailApi = ServiceUrls.emailServices.emailApi;
                        sendMailService(emailApi, custmail);
                        this.isLoading = false;
                        alertify.alert(this.getValidationMsgByCode('M023'),this.getValidationMsgByCode('M009')).set('closable', false);
                    } catch (e) {
                        this.isLoading = false;
                    }
                } catch (error) {
                    self.isLoading = false;
                }
            }
        },
        getValidationMsgs: function () {


            var self = this;
            getAgencycode(function (response) {

                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var homePageURL = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Validation/Validation/Validation.ftl';
                axios.get(homePageURL, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {

                    if (response.data.area_List.length) {

                        var validationData = self.pluck("Validations", response.data.area_List);
                        self.validationList = self.getAllMapData(validationData[0].component);
                        sessionStorage.setItem('validationItems', JSON.stringify(self.validationList));


                    }
                }).catch(function (error) {
                    console.log('Error');

                });

            });

        },
        dateConvert: function (utc) {
            return (moment(utc).format("DD-MMM-YYYY"));
        },
        stopLoader: function () {
            $('#preloader').delay(50).fadeOut(250);
        },
        showhidetraveller: function () {
            this.travellerdisply == true ? this.travellerdisply = false : this.travellerdisply = true;
        },
        
        setCHDINFTravellers: function (event) {

            this.children = [];
            this.infants = [];
            this.child = this.totalAllowdPax - this.selected_adults;
            var chdtext = "Child";
            for (var chd = 0; chd <= this.child; chd++) {
                chdtext = chd == 0 ? chdtext = "0 " + this.childLabel : chd == 1 ? chdtext = chd + " " + this.childLabel : chdtext = chd + " " + this.childrenLabel;
                this.children.push({
                    'value': chd,
                    'text': chdtext
                })
            }
            if (this.selected_children > 0 && this.selected_children <= this.child) {
                this.selected_children = this.selected_children;
            } else {
                this.selected_children = 0;
            }
            infant = parseInt(this.selected_adults);
            if (infant + parseInt(this.selected_adults) + parseInt(this.selected_children) > 9) {
                infant = parseInt(this.totalAllowdPax) - (parseInt(this.selected_adults) + parseInt(this.selected_children));
            }
            infant = ((parseInt(infant) < 0) ? 0 : infant);
            if (infant == 0) this.selected_infant = 0;
            var inftext = "";
            for (var inf = 0; inf <= infant; inf++) {
                inftext = inf == 0 ? inftext = "0 " + this.infantLabel : inf == 1 ? inftext = " 1 " + this.infantLabel : inftext = inf + " " + this.infantLabel;
                this.infants.push({
                    'value': inf,
                    'text': inftext
                })
            }
            if (this.selected_infant > 0) {
                this.selected_infant = (this.selected_infant <= infant) ? this.selected_infant : 0;
            }
            totalpax = parseInt(this.selected_adults) + parseInt(this.selected_children) + parseInt(this.selected_infant);
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            if(totalpax>0 && this.selected_cabin==""){
             this.Totalpassenger = totalpax + " " + this.bookingForm.Passenger_Label;
             this.Totaltravaller = totalpax + " " + this.bookingForm.Traveler_Placeholder;
             }
             if(totalpax==0 && this.selected_cabin==""){
                 if (langauage == "ar") {
                     self.Totaltravaller = "1 مسافر واقتصاد";
                     self.Totalpassenger = "1 راكب";
                     //selected_cabin="تحديد"
                 }
                 else{
                 this.Totaltravaller = "1 Traveller(s) & Economy";
                 }
             }
             if(totalpax==0 && this.selected_cabin!=""){
                     this.Totaltravaller = this.getCabinName(this.selected_cabin.toUpperCase());
             }
             if(totalpax!=0 && this.selected_cabin!=""){
                this.Totalpassenger = totalpax + " " + this.bookingForm.Passenger_Label;
                //this.Totaltravaller = totalpax + " " + this.bookingForm.Traveler_Placeholder;
                this.Totaltravaller =totalpax + " " + this.bookingForm.Traveler_Placeholder+"," +this.getCabinName(this.selected_cabin.toUpperCase());
            }
        },
        SetInfantTravellers: function () {
            this.infants = [];
            remiaingpax = parseInt(this.selected_adults);
            if (remiaingpax + parseInt(this.selected_adults) + parseInt(this.selected_children) > 9) {
                remiaingpax = parseInt(this.totalAllowdPax) - (parseInt(this.selected_adults) + parseInt(this.selected_children));
            }
            remiaingpax = ((parseInt(remiaingpax) < 0) ? 0 : remiaingpax);
            for (var inf = 0; inf <= remiaingpax; inf++) {
                inftext = inf == 0 ? inftext = "0 " + this.infantLabel : inf == 1 ? inftext = " 1 " + this.infantLabel : inftext = inf + " " + this.infantLabel;
                this.infants.push({
                    'value': inf,
                    'text': inftext
                })
            }
            if (this.selected_infant > 0) {
                this.selected_infant = (this.selected_infant <= remiaingpax) ? this.selected_infant : 0;
            }
            totalpax = parseInt(this.selected_adults) + parseInt(this.selected_children) + parseInt(this.selected_infant);
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            if(totalpax>0 && this.selected_cabin==""){
             this.Totalpassenger = totalpax + " " + this.bookingForm.Passenger_Label;
             this.Totaltravaller = totalpax + " " + this.bookingForm.Traveler_Placeholder;
             }
             if(totalpax==0 && this.selected_cabin==""){
                 if (langauage == "ar") {
                     self.Totaltravaller = "1 مسافر واقتصاد";
                     self.Totalpassenger = "1 راكب";
                     //selected_cabin="تحديد"
                 }
                 else{
                 this.Totaltravaller = "1 Traveller(s) & Economy";
                 }
             }
             if(totalpax==0 && this.selected_cabin!=""){
               
                 this.Totaltravaller = this.getCabinName(this.selected_cabin.toUpperCase());
             }
             if(totalpax!=0 && this.selected_cabin!=""){
                this.Totalpassenger = totalpax + " " + this.bookingForm.Passenger_Label;
                //this.Totaltravaller = totalpax + " " + this.bookingForm.Traveler_Placeholder;
                this.Totaltravaller =totalpax + " " + this.bookingForm.Traveler_Placeholder+"," +this.getCabinName(this.selected_cabin.toUpperCase());
            }
        },
        setTravelInfo: function () {
            totalpax = parseInt(this.selected_adults) + parseInt(this.selected_children) + parseInt(this.selected_infant);
          var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
           if(totalpax>0 && this.selected_cabin==""){
            this.Totalpassenger = totalpax + " " + this.bookingForm.Passenger_Label;
            this.Totaltravaller = totalpax + " " + this.bookingForm.Traveler_Placeholder;
            }
            if(totalpax==0 && this.selected_cabin==""){
                if (langauage == "ar") {
                    self.Totaltravaller = "1 مسافر واقتصاد";
                    self.Totalpassenger = "1 راكب";
                    //selected_cabin="تحديد"
                }
                else{
                this.Totaltravaller = "1 Traveller(s) & Economy";
                }
            }
            if(totalpax==0 && this.selected_cabin!=""){
                this.Totaltravaller = this.getCabinName(this.selected_cabin.toUpperCase());
            }
            if(totalpax!=0 && this.selected_cabin!=""){
                this.Totalpassenger = totalpax + " " + this.bookingForm.Passenger_Label;
                //this.Totaltravaller = totalpax + " " + this.bookingForm.Traveler_Placeholder;
                this.Totaltravaller =totalpax + " " + this.bookingForm.Traveler_Placeholder+"," +this.getCabinName(this.selected_cabin.toUpperCase());
            }
        },

        getCabinName:function(cabinCode) {
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            if (langauage == 'en') {
                    if (cabinCode == "F") {
                    cabinClass = "First";
                } else if (cabinCode == "Y") {
                    cabinClass = "Economy";
                }else if (cabinCode == "C") {
                    cabinClass = "Business";
                }
                 else {
                    try { cabinClass = getCabinClassObject(cabinCode).BasicClass; } catch (err) { }
                }
                return cabinClass;
            } else {
                var cabinClass = '';
                if (cabinCode == "F") {
                    cabinClass = "اعمال";
                } else if (cabinCode == "C") {
                    cabinClass = "أول";
                }else if (cabinCode == "Y") {
                    cabinClass = "الاقتصاد";
                } else {
                    try { cabinClass = getCabinClassObject(cabinCode).BasicClass; } catch (err) { }
                }
                return cabinClass;
            }
        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "packageview.html?page=" + url + "&from=pkg";
                    window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;
        },
        getMoreDetails(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "detail-request.html?page=" + url + "&from=pkg";
                    window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;
        },
        changeTab(service) {
            this.services = service;
            $("#deptDate02").val("");
            $("#deptDate03").val("");
            $("#deptDate04").val("");
            $("#deptDate05").val("");
            this.transferdisply = false;
            this.cruisedisply = false;
            this.travellerdisply = false;
            this.destination = "";
            if (langauage == "ar") {
                this.Totaltravaller = "1 مسافر واقتصاد";
                this.Totalpassenger = "1 راكب";
                //selected_cabin="تحديد"
            } else {
                this.Totaltravaller = '1 Traveller(s) & Economy';
                this.Totalpassenger = '1 Passenger';
            }
            this.selected_adults = 1;
            this.selected_children = 0;
            this.selected_infant = 0;
            this.message = "";
            this.visa = {
                fullName:'',
                nationality:'',
                country:'',
                phoneNumber:'',
                email:'',
                whereDidYouHear:''
            };
        }
    },
    updated: function () {
        var self = this;
        self.setCalender();


    },

    mounted: function () {
        var self = this;
        setTimeout(() => {
            $('#search_checkin').val("");
            $('#search_checkout').val("");
        }, 100);
        //if (sessionStorage.validationItems == undefined) {
            self.getValidationMsgs();
       // }
        self.pageContent();
        self.holidayPackageCard();
        self.getDeals();
        self.getPackageconfig();

        self.actvetab = sessionStorage.active_el ? (sessionStorage.active_el == 0 || sessionStorage.active_el == 3) ? 1 : sessionStorage.active_el : 1
    }
});
function carouselDeals() {
    $("#owl-demo-2").owlCarousel({
        items: 3,
        lazyLoad: true,
        loop: true,
        margin: 30,
        autoPlay: true,
        navigation: true,
        itemsDesktop: [991, 2],
        itemsDesktopSmall: [979, 2],
        itemsTablet: [768, 2],
        itemsMobile: [640, 1],
    });
    $(".owl-prev").html('<i class="fa fa-chevron-left"></i>');
    $(".owl-next").html('<i class="fa fa-chevron-right"></i>');
}
function partnersCarousel() {
    var owl = $("#owl-demo");
    owl.owlCarousel({
        autoPlay: true,
        items: 4, //10 items above 1000px browser width
        itemsDesktop: [1000, 4], //5 items between 1000px and 901px
        itemsDesktopSmall: [900, 3], // 3 items betweem 900px and 601px
        itemsTablet: [600, 2], //2 items between 600 and 0;
        itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
        pagination: false
    });
    $(".next").click(function() {
        owl.trigger('owl.next');
    })
    $(".prev").click(function() {
        owl.trigger('owl.prev');
    });
}
function carouselPack2() {

    $("#owl-demo-1").owlCarousel({
        items: 2,
        lazyLoad: true,
        loop: true,
        margin: 30,
        autoPlay: true,
        navigation: true,
        itemsDesktop: [1024, 1],
        itemsDesktopSmall: [979, 2],
        itemsTablet: [768, 2],
        itemsMobile: [640, 1],
    });
    $(".owl-prev").html('<i class="fa fa-chevron-left"></i>');
    $(".owl-next").html('<i class="fa fa-chevron-right"></i>');


}
function swpaloc(id) {
    var from = $("#Cityfrom" + id).val();
    var to = $("#Cityto" + id).val();
    $("#Cityfrom" + id).val(to);
    $("#Cityto" + id).val(from);
}
function isNullorUndefined(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) { status = true; }
    return status;
}
jQuery(document).ready(function ($) {
    initSelect2();
});
function initSelect2() {
    $('.custom-select-v3 lg').select2({
       width:'100%'
    });
   }

 $(document).on('click', '.close-icon-n', function () {
    maininstance.visa.nationality =''
  });
 $(document).on('click', '.close-icon-c', function () {
    maininstance.visa.country =''
  });