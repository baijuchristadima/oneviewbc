var HeaderComponent = Vue.component('headeritem', {
  template: `  <div id="header">
  <div class="top">
    <div class="container ar_direction">
      <div class="row">
        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
          <div class="logo">
            <a href="#" @click="clickOnLogo"><img :src="MainSection.Logo_267_x_97_px" alt=""></a>
          </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 pull-right">
          <div class="toll-free">
             <span><i class="fa fa-phone" aria-hidden="true"></i></span><a :href="'tel:'+TopSection.Phone_Number">{{TopSection.Phone_Number}}</a> 
          </div>
          <ul class="list-unstyled list-inline lang">
          <lang-select @languagechange="getpagecontent"></lang-select>
          </ul>
          <ul class="list-unstyled list-inline crnc">
          <currency-select></currency-select>
          </ul>
          <div class="login">
           
          <a id="sign-bt-area" class="login-btn  js-signin-modal-trigger" data-signin="login" v-show="!userlogined"><i :class="'fa '+ TopSection.Login_User_Icon" aria-hidden="true"></i>{{TopSection.Login_Label}}</a>
          <a id="modal_retrieve" data-toggle="modal" data-target="#myModal">{{TopSection.View_Bookings}}</a>
          
          <!--iflogin-->
          <label id="bind-login-info" for="profile2" class="profile-dropdown btn" v-show="userlogined">
              <input type="checkbox" id="profile2">
              <img src="/assets/images/user.png">
              <span>{{userinfo.firstName+' '+userinfo.lastName }}</span>
              <label for="profile2">
                  <i class="fa fa-angle-down" aria-hidden="true"></i>
              </label>
              <ul>
                  <li>
                      <a href="/customer-profile.html">
                          <i class="fa fa-th-large" aria-hidden="true"></i>{{TopSection.Dashboard_Label}}
                      </a>
                  </li>
                  <li>
                      <a href="/my-profile.html">
                          <i class="fa fa-user" aria-hidden="true"></i>{{TopSection.My_Profile_Label}}
                      </a>
                  </li>
                  <li>   
                      <a href="/my-bookings.html"><i class="fa fa-file-text-o" aria-hidden="true"></i>{{TopSection.My_Booking_Label}}</a>
                  </li>
                  <li>
                      <a href="#" v-on:click.prevent="logout"><i class="fa fa-power-off" aria-hidden="true"></i>{{TopSection.Logout_Label}}</a>
                  </li>
              </ul>
          </label>
          </a>
          <!--ifnotlogin-->
        </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog  modal-smsp">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">{{TopSection.View_Bookings}}</h4>
            </div>
            <div class="modal-body">
            <div class="user_login">
            <div>
            <p><label>{{TopSection.Enter_Email_Address}}</label></p>
            <div class="validation_sec">
            <input v-model="retrieveEmailId" type="text" id="txtretrivebooking" name="text" :placeholder="TopSection.Name_Placeholder" class=""> 
            <span v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveEmailErormsg }"  class="cd-signin-modal__error sp_validation">Email Required!</span>
            </div> 
            <p></p> 
            <p><label>{{TopSection.Enter_Id}}</label></p>
            <div class="validation_sec">
            <input v-model="retrieveBookRefid" type="text" id="text" name="text" :placeholder="TopSection.Booking_Placeholder" class=""> 
            <span  v-bind:class="{ 'cd-signin-modal__error--is-visible': retrieveBkngRefErormsg }"   class="cd-signin-modal__error sp_validation">Booking Reference Id Required!</span>
            </div> 
            <button type="submit"  v-on:click="retrieveBooking" id="retrieve-booking" class="cd-signin-modal__input cd-signin-modal__input--full-width">{{TopSection.Continue}}</button></div></div>
            </div>            
          </div>
          
        </div>
      </div>
  <!--popup area-->
  <div class="cd-signin-modal js-signin-modal ar_direction"> 
      <div class="cd-signin-modal__container ar_direction"> <!-- this is the container wrapper -->
          <ul class="cd-signin-modal__switcher js-signin-modal-switcher js-signin-modal-trigger">
              <li><a href="#0" data-signin="login" data-type="login">{{TopSection.Sign_In_Label}}</a></li>
              <li><a href="#0" data-signin="signup" data-type="signup">{{TopSection.Register_Label}}</a></li>
          </ul>
          <div class="cd-signin-modal__block js-signin-modal-block" data-type="login"> <!-- log in form -->
              <div class="cd-signin-modal__form">
                  <p class="cd-signin-modal__fieldset">
                      <label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" 
                          for="signin-email">Email</label>
                      <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                          id="signin-email" type="email" :placeholder="TopSection.Email_Placeholder" v-model="username" >
                          <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': usererrormsg.empty||usererrormsg.invalid }">{{usererrormsg.empty? getValidationMsgByCode('M026'):(usererrormsg.invalid?getValidationMsgByCode('M027'):'')}}</span>
                  </p>
                  <p class="cd-signin-modal__fieldset">
                      <label class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace" 
                          for="signin-password">Password</label>
                      <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                          id="signin-password" type="password"  :placeholder="TopSection.Password_Placeholder"  v-model="password" >
                          <a v-on:click="showhidepassword" class="cd-signin-modal__hide-password js-hide-password changeShowTxtCls">{{TopSection.Show_Label}}</a>
                          <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': psserrormsg }">
                          {{getValidationMsgByCode('M039')}}</span>
                  </p>

                  <p class="cd-signin-modal__fieldset">
                      <input class="cd-signin-modal__input cd-signin-modal__input--full-width" type="submit" v-on:click="loginaction" :value="TopSection.Login_Button_Label">
                  </p>
                  <div id="myGoogleButton"></div>
                  <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false"
                      data-use-continue-as="false" onlogin="checkLoginState();">
                  </div>
                  <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="reset">{{TopSection.Forgot_Password_Label}}</a></p>
              </div>
          
          
      </div> <!-- cd-signin-modal__block -->

      <div class="cd-signin-modal__block js-signin-modal-block" data-type="signup"> <!-- sign up form -->
          <div class="cd-signin-modal__form">
              <p class="cd-signin-modal__fieldset">
                  <label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">Title</label>                        
                  <select v-model="registerUserData.title" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border down_select" id="signup-title">
                      <option selected>Mr</option>
                      <option selected>Ms</option>
                      <option>Mrs</option>
                  </select>
                  <span class="cd-signin-modal__error">Title seems incorrect!</span>
              </p>
              <p class="cd-signin-modal__fieldset">
                  <label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">First Name</label>
                  <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                  id="signup-firstname" type="text" :placeholder="TopSection.First_Name_Placeholder" v-model="registerUserData.firstName">
                  <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userFirstNameErormsg }" >{{getValidationMsgByCode('M034')}}</span>
              </p>

              <p class="cd-signin-modal__fieldset">
                  <label class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace" for="signup-username">Last Name</label>
                  <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                  id="signup-lastname" type="text" :placeholder="TopSection.Last_Name_Placeholder" v-model="registerUserData.lastName">
                  <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userLastNameErrormsg }">{{getValidationMsgByCode('M035')}}</span>
              </p>

              <p class="cd-signin-modal__fieldset">
                  <label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" for="signup-email">Email</label>
                  <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" 
                  id="signup-email" type="email" :placeholder="TopSection.Email_Placeholder" v-model="registerUserData.emailId">
                  <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userEmailErormsg.empty||userEmailErormsg.invalid }">{{userEmailErormsg.empty?getValidationMsgByCode('M026'):(userEmailErormsg.invalid?getValidationMsgByCode('M027'):'')}}</span>
              </p>

              <p class="cd-signin-modal__fieldset">
                  <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding" type="submit" 
                  :value="TopSection.Create_Account_Button_Label" v-on:click="registerUser">
              </p>
              <div id="myGoogleButtonReg"></div>
              <div class="fb-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false"
  data-use-continue-as="false" onlogin="checkLoginState();"></div>                    
          </div>
      </div> <!-- cd-signin-modal__block -->

      <div class="cd-signin-modal__block js-signin-modal-block" data-type="reset"> <!-- reset password form -->
          <p class="cd-signin-modal__message" v-html="TopSection.Forgot_Password_Description"></p>

          <div class="cd-signin-modal__form">
              <p class="cd-signin-modal__fieldset">
                  <label class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace" for="reset-email">E-mail</label>
                  <input v-model="emailId" class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border" id="reset-email" type="email" :placeholder="TopSection.Email_Placeholder">
                  <span class="cd-signin-modal__error" v-bind:class="{ 'cd-signin-modal__error--is-visible': userforgotErrormsg.empty||userforgotErrormsg.invalid }">{{userforgotErrormsg.empty?getValidationMsgByCode('M026'):(userforgotErrormsg.invalid?getValidationMsgByCode('M027'):'')}}</span>
              </p>

              <p class="cd-signin-modal__fieldset">
                  <input class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding" type="submit" 
                  :value="TopSection.Reset_Password_Button_Label" v-on:click="forgotPassword">
              </p>
              <p class="cd-signin-modal__bottom-message js-signin-modal-trigger"><a href="#0" data-signin="login">{{TopSection.Back_To_Login_Label}}</a></p>
          </div>

          
      </div> <!-- cd-signin-modal__block -->
      <a href="#0" class="cd-signin-modal__close js-close">Close</a>
  </div> <!-- cd-signin-modal__container -->
</div> 
  <!--popup area close-->
</div>
`,
  data() {
    return {
      language: 'en',
      TopSection: {},
      MainSection: {},
      isLoading: false,
      fullPage: true,
      sessionids: "",
      Ipaddress: "",
      userlogined: this.checklogin(),
      userlogined: '',
      userinfo: [],

      activePageName: '',
      username: '',
      password: '',
      emailId: '',
      retrieveEmailId: '',
      usererrormsg: { empty: false, invalid: false },
      psserrormsg: false,
      userFirstNameErormsg: false,
      userLastNameErrormsg: false,
      userEmailErormsg: { empty: false, invalid: false },
      userPasswordErormsg: false,
      userVerPasswordErormsg: false,
      userPwdMisMatcherrormsg: false,
      userTerms: false,
      userforgotErrormsg: { empty: false, invalid: false },
      registerUserData: {
        firstName: '',
        lastName: '',
        emailId: '',
        password: '',
        verifyPassword: '',
        terms: '',
        title: 'Mr'
      },
      show: '',
      hide: '',
      activeClass: '',
      FooterSection:{},
      retrieveBkngRefErormsg: false,
      retrieveEmailErormsg: false,
      retrieveEmailId: '',
      retrieveBookRefid: '',
    }
  },
  created() {
    var self=this;
    var pathArray = window.location.pathname.split('/');
    self.activeClass ='/'+ pathArray[pathArray.length-1];
   
  },
  computed: {
    checkMobileOrNot: function () {
      if( screen.width <= 1020 ) {
        return true;
    }
    else {
        return false;
    }
    },
},
  methods: {
    getpagecontent: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/01 Master/01 Master/01 Master.ftl';
        axios.get(pageurl, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          if (response.data.area_List.length) {
            var headerSection = pluck('Top_Section', response.data.area_List);
            self.TopSection = getAllMapData(headerSection[0].component);
            var mainContent = pluck('Main_Section', response.data.area_List);
            self.MainSection = getAllMapData(mainContent[0].component);

            var FooterContent = pluck('Footer_Section', response.data.area_List);
            self.FooterSection = getAllMapData(FooterContent[0].component);


          }
          self.getLables();
          Vue.nextTick(function () {
            (

                function () {
                    self.active_el = (sessionStorage.active_el) ? sessionStorage.active_el : 0;
                    //Login/Signup modal window - by CodyHouse.co
                    function ModalSignin(element) {
                        this.element = element;
                        this.blocks = this.element.getElementsByClassName('js-signin-modal-block');
                        this.switchers = this.element.getElementsByClassName('js-signin-modal-switcher')[0].getElementsByTagName('a');
                        this.triggers = document.getElementsByClassName('js-signin-modal-trigger');
                        this.hidePassword = this.element.getElementsByClassName('js-hide-password');
                        this.init();
                    };

                    ModalSignin.prototype.init = function () {
                        var self1 = this;
                        //open modal/switch form
                        for (var i = 0; i < this.triggers.length; i++) {
                            (function (i) {
                                self1.triggers[i].addEventListener('click', function (event) {
                                    if (event.target.hasAttribute('data-signin')) {
                                        event.preventDefault();
                                        self1.showSigninForm(event.target.getAttribute('data-signin'));
                                    }
                                });
                            })(i);
                        }

                        //close modal
                        this.element.addEventListener('click', function (event) {
                            if (hasClass(event.target, 'js-signin-modal') || hasClass(event.target, 'js-close')) {
                                event.preventDefault();
                                removeClass(self1.element, 'cd-signin-modal--is-visible');
                            }
                        });
                        //close modal when clicking the esc keyboard button
                        document.addEventListener('keydown', function (event) {
                            (event.which == '27') && removeClass(self1.element, 'cd-signin-modal--is-visible');
                        });

                        // //hide/show password
                        // for (var i = 0; i < this.hidePassword.length; i++) {
                        //     (function(i) {
                        //         self1.hidePassword[i].addEventListener('click', function(event) {
                        //             self1.togglePassword(self1.hidePassword[i]);
                        //         });
                        //     })(i);
                        // }

                        //IMPORTANT - REMOVE THIS - it's just to show/hide error messages in the demo

                    };

                    // ModalSignin.prototype.togglePassword = function(target) {
                    //     var password = target.previousElementSibling;
                    //     ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text'): password.setAttribute('type', 'password');
                    //     target.textContent = ('Hide' == target.textContent) ? 'Show' : 'Hide';
                    //     putCursorAtEnd(password);
                    // }

                    ModalSignin.prototype.showSigninForm = function (type) {
                        // show modal if not visible
                        !hasClass(this.element, 'cd-signin-modal--is-visible') && addClass(this.element, 'cd-signin-modal--is-visible');
                        // show selected form
                        for (var i = 0; i < this.blocks.length; i++) {
                            this.blocks[i].getAttribute('data-type') == type ? addClass(this.blocks[i], 'cd-signin-modal__block--is-selected') : removeClass(this.blocks[i], 'cd-signin-modal__block--is-selected');
                        }
                        //update switcher appearance
                        var switcherType = (type == 'signup') ? 'signup' : 'login';
                        for (var i = 0; i < this.switchers.length; i++) {
                            this.switchers[i].getAttribute('data-type') == switcherType ? addClass(this.switchers[i], 'cd-selected') : removeClass(this.switchers[i], 'cd-selected');
                        }
                    };

                    ModalSignin.prototype.toggleError = function (input, bool) {
                        // used to show error messages in the form
                        toggleClass(input, 'cd-signin-modal__input--has-error', bool);
                        toggleClass(input.nextElementSibling, 'cd-signin-modal__error--is-visible', bool);
                    }

                    var signinModal = document.getElementsByClassName("js-signin-modal")[0];
                   
                    if (signinModal) {
                        new ModalSignin(signinModal);
                    }

                    // toggle main navigation on mobile
                    var mainNav = document.getElementsByClassName('js-main-nav')[0];
                    if (mainNav) {
                        mainNav.addEventListener('click', function (event) {
                            if (hasClass(event.target, 'js-main-nav')) {
                                var navList = mainNav.getElementsByTagName('ul')[0];
                                toggleClass(navList, 'cd-main-nav__list--is-visible', !hasClass(navList, 'cd-main-nav__list--is-visible'));
                            }
                        });
                    }

                    //class manipulations - needed if classList is not supported
                    function hasClass(el, className) {
                        if (el.classList) return el.classList.contains(className);
                        else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
                    }

                    function addClass(el, className) {
                        var classList = className.split(' ');
                        if (el.classList) el.classList.add(classList[0]);
                        else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
                        if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
                    }

                    function removeClass(el, className) {
                        var classList = className.split(' ');
                        if (el.classList) el.classList.remove(classList[0]);
                        else if (hasClass(el, classList[0])) {
                            var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
                            el.className = el.className.replace(reg, ' ');
                        }
                        if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
                    }

                    function toggleClass(el, className, bool) {
                        if (bool) addClass(el, className);
                        else removeClass(el, className);
                    }
                    // $("#modal_retrieve").leanModal({
                    //     top: 100,
                    //     overlay: 0.6,
                    //     closeButton: ".modal_close"
                    // });
                    //credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
                    function putCursorAtEnd(el) {
                        if (el.setSelectionRange) {
                            var len = el.value.length * 2;
                            el.focus();
                            el.setSelectionRange(len, len);
                        } else {
                            el.value = el.value;
                        }
                    };
                })();
        }.bind(self));
        }).catch(function (error) {
          console.log('Error');
        });
      });
    },
    getValidationMsgs: function () {
      var self = this;
      getAgencycode(function (response) {
          var Agencycode = response;
          var huburl = ServiceUrls.hubConnection.cmsUrl;
          var portno = ServiceUrls.hubConnection.ipAddress;
          var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
          var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Validation/Validation/Validation.ftl';
          axios.get(pageurl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
          }).then(function (response) {
              if (response.data.area_List.length) {
                  var validations = pluck('Validations', response.data.area_List);
                  self.validationList = getAllMapData(validations[0].component);
                  sessionStorage.setItem('validationItems', JSON.stringify(self.validationList));
              }
          }).catch(function (error) {
              console.log('Error');
          });
      });
    },
    getValidationMsgByCode: function (code) {
      if (sessionStorage.validationItems !== undefined) {
          var validationList = JSON.parse(sessionStorage.validationItems);
          for (let validationItem of validationList.Validation_List) {
              if (code === validationItem.Code) {
                  return validationItem.Message;
              }
          }
      }
   },
    getLables: function() {
      this.hide = this.TopSection.Hide_Label;
      this.show = this.TopSection.Show_Label;
    },
    Getvisit: function () {
      var self = this;
      var currentUrl = window.location.pathname;
      if (currentUrl == "/FlyKey/") {
        self.pageType = "Master";
        self.PageName = "Home"
      }
      // else if (currentUrl == "holidaydetails.html") {
      //   self.pageType = "Package details";
      //   self.PageName = packageView.packagecontent.Title;
      // } else {
      //   page = currentUrl.substring(0, currentUrl.length - 5);
      //   var page = page.split('/');
      //   self.PageName = page[3];
      //   self.pageType = "Master";
      // }
      var TempDeviceInfo = detect.parse(navigator.userAgent);
      self.DeviceInfo = TempDeviceInfo;
      this.showYourIp();
    },
    showYourIp: function () {
      var self = this;
      var ipUrl = ServiceUrls.externalServiceUrl.ipAddressApi;
      fetch(ipUrl)
        .then(x => x.json())
        .then(({ ip }) => {
          let Ipaddress = ip;
          let sessionids = ip.replace(/\./g, '');
          this.sendip(Ipaddress,sessionids);
        });
    },
    sendip: async function (Ipaddress,sessionids) {
      var self = this;
      if (self.PageName == null || self.PageName == undefined || self.PageName == "") {
        var pathArray = window.location.pathname.split('/');
        var activePage = pathArray[pathArray.length - 1];

        if (activePage == 'packageview.html') {
          self.pageType="Package"
          self.PageName = Packageinfo.packagename;
        }
        else{
          self.PageName = activePage;
        }
      }
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;
      let requestedDate = moment(new Date()).format('YYYY-MM-DDThh:mm:ss');
      let insertVisitData = {
        type: "Visit",
        keyword1: sessionids,
        ip1: Ipaddress,
        keyword2: self.pageType || "Page",
        keyword3: self.PageName,
        keyword4: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'AED',
        keyword5: (localStorage.Languagecode) ? localStorage.Languagecode : "en",
        text1: self.DeviceInfo.device.type,
        text2: self.DeviceInfo.os.name,
        text3: self.DeviceInfo.browser.name,
        date1: requestedDate,
        nodeCode: agencyCode
      };
     cmsRequestData("POST", "cms/data", insertVisitData, null);
    },
    clickOnLogo: function () {
      var searchUrl = '/FlyKey/index.html';
      window.location.href = searchUrl;
    },
    checklogin: function () {
      if (localStorage.IsLogin == "true") {
          this.userlogined = true;
          this.userinfo = JSON.parse(localStorage.User);
          return true;
      } else {
          this.userlogined = false;
          return false;
      }
    },
    loginaction: function () {

      if (!this.username.trim()) {
          this.usererrormsg = { empty: true, invalid: false };
          return false;
      } else if (!this.validEmail(this.username.trim())) {
          this.usererrormsg = { empty: false, invalid: true };
          return false;
      } else {
          this.usererrormsg = { empty: false, invalid: false };
      }
      if (!this.password) {
          this.psserrormsg = true;
          return false;

      } else {
          this.psserrormsg = false;
          var self = this;
          this.isLoading = true;
          login(this.username, this.password, function (response) {
              if (response == false) {
                  self.userlogined = false;
                  self.isLoading = false;
                  alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M040'));
              } else {
                  self.userlogined = true;
                  self.userinfo = JSON.parse(localStorage.User);
                  $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                  try {
                      self.$eventHub.$emit('logged-in', { userName: self.username, password: self.password });
                      signArea.headerLogin({ userName: self.username, password: self.password })
                      self.isLoading = false;
                  } catch (error) {
                    self.isLoading = false;
                  }
              }
          });

      }



  },

  logout: function () {
     this.isLoading = true;
      this.userlogined = false;
      this.userinfo = [];
      localStorage.profileUpdated = false;
      try {
          this.$eventHub.$emit('logged-out');
          signArea.logout()
      } catch (error) {
      }
      commonlogin();
      // signOut();
      // signOutFb();
  },
  registerUser: function () {
      if (this.registerUserData.firstName.trim() == "") {
          this.userFirstNameErormsg = true;
          return false;
      } else {
          this.userFirstNameErormsg = false;
      }
      if (this.registerUserData.lastName.trim() == "") {
          this.userLastNameErrormsg = true;
          return false;
      } else {
          this.userLastNameErrormsg = false;
      }
      if (this.registerUserData.emailId.trim() == "") {
          this.userEmailErormsg = { empty: true, invalid: false };
          return false;
      } else if (!this.validEmail(this.registerUserData.emailId.trim())) {
          this.userEmailErormsg = { empty: false, invalid: true };
          return false;
      } else {
          this.userEmailErormsg = { empty: false, invalid: false };
      }
      var vm = this;
      vm.isLoading = true;
      registerUser(this.registerUserData.emailId, this.registerUserData.firstName, this.registerUserData.lastName, this.registerUserData.title, function (response) {
          if (response.isSuccess == true) {
              vm.username = response.data.data.user.loginId;
              vm.password = response.data.data.user.password;
              login(vm.username, vm.password, function (response) {
                  if (response != false) {
                     // $('.cd-signin-modal').removeClass('cd-signin-modal--is-visible');
                      window.location.href = "/edit-my-profile.html?edit-profile=true";
                      vm.isLoading = false;
                  }
              });
          }
          vm.isLoading = false;
      });
  },

  forgotPassword: function () {
      if (this.emailId.trim() == "") {
          this.userforgotErrormsg = { empty: true, invalid: false };
          return false;
      } else if (!this.validEmail(this.emailId.trim())) {
          this.userforgotErrormsg = { empty: false, invalid: true };
          return false;
      } else {
          this.userforgotErrormsg = { empty: false, invalid: false };
      }
      this.isLoading = true;
      var datas = {
          emailId: this.emailId,
          agencyCode: localStorage.AgencyCode,
          logoUrl: window.location.origin + "/" + localStorage.AgencyFolderName + "/website-informations/logo/logo.png",
          websiteUrl: window.location.origin,
          resetUri: window.location.origin + "/" + localStorage.AgencyFolderName + "/reset-password.html"
      };
      $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:none;background:grey !important;");

      var huburl = ServiceUrls.hubConnection.baseUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var requrl = ServiceUrls.hubConnection.hubServices.forgotPasswordUrl;
      axios.post(huburl + portno + requrl, datas)
          .then((response) => {
              if (response.data != "") {
                this.isLoading = false;
                alertify.alert(this.getValidationMsgByCode('M022'),response.data);
              } else {
                this.isLoading = false;
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M037'));
              }
              $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:var(--primary-color) !important");

          })
          .catch((err) => {
              console.log("FORGOT PASSWORD  ERROR: ", err);
              if (err.response.data.message == 'No User is registered with this emailId.') {
                this.isLoading = false;
                alertify.alert(this.getValidationMsgByCode('M022'),err.response.data.message);
              } else {
                this.isLoading = false;
                alertify.alert(this.getValidationMsgByCode('M022'),this.getValidationMsgByCode('M038'));
              }
              $(".cd-signin-modal__input[type=submit]").css("cssText", "pointer-events:auto;background:var(--primary-color) !important");

          })

  },
  showhidepassword: function (event) {
    var password = event.target.previousElementSibling;
    ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text') : password.setAttribute('type', 'password');
    event.target.textContent = (this.hide == event.target.textContent) ? this.show : this.hide;
},
validEmail: function (email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
},
getValidationMsgByCode: function (code) {
  if (sessionStorage.validationItems !== undefined) {
      var validationList = JSON.parse(sessionStorage.validationItems);
      for (let validationItem of validationList.Validation_List) {
          if (code === validationItem.Code) {
              return validationItem.Message;
          }
      }
  }
},
        retrieveBooking: function () {
            if (this.retrieveEmailId == "") {
                //alert('Email required !');
                this.retrieveEmailErormsg = true;
                return false;
            } else if (!this.validEmail(this.retrieveEmailId)) {
                //alert('Invalid Email !');
                this.retrieveEmailErormsg = true;
                return false;
            } else {
                this.retrieveEmailErormsg = false;
            }
            if (this.retrieveBookRefid == "") {
                this.retrieveBkngRefErormsg = true;
                return false;
            } else {
                this.retrieveBkngRefErormsg = false;
            }
            var bookData = {
                BkngRefID: this.retrieveBookRefid,
                emailId: this.retrieveEmailId,
                redirectFrom: 'retrievebooking',
                isMailsend: false
            };
            localStorage.bookData = JSON.stringify(bookData);
            window.location.href = '/Flights/flight-confirmation.html';
        }
  },
  mounted: function () {
    document.onreadystatechange = () => {
      if (document.readyState == "complete") {
        if (localStorage.direction == 'rtl') {
          $(".ar_direction").addClass("ar_direction1");
          document.getElementById("styleid").setAttribute("href", 'css/rtl-style.css');
          document.getElementById("mediaid").setAttribute("href", 'css/rtl-media.css');
          $(".ar_direction").addClass("ar_direction1");
        } else {
          $(".ar_direction").removeClass("ar_direction1");
          document.getElementById("styleid").setAttribute("href", 'css/ltr-style.css');
          document.getElementById("mediaid").setAttribute("href", 'css/ltr-media.css');
          $(".ar_direction").removeClass("ar_direction1");
        }
      }
    }
    if (localStorage.IsLogin == "true") {
      this.userlogined = true;
      this.userinfo = JSON.parse(localStorage.User);

    } else {
      this.userlogined = false;

    }
    this.getpagecontent();
    this.getValidationMsgs();
    setTimeout(() => {
      this.Getvisit();
    }, 1000);
  },
  watch: {
    updatelogin: function () {
        if (localStorage.IsLogin == "true") {
            this.userlogined = true;
            this.userinfo = JSON.parse(localStorage.User);

        } else {
            this.userlogined = false;

        }
    }
  }
})

var headerinstance = new Vue({
  el: 'header',
  name: 'headerArea',
  data() {
    return {
      key: '',
      content: null,
      getdata: true
    }

  },

});

Vue.component('footeritem', {
  template: `
  <div class="ar_direction">
  <!--<div id="newsletter" :style="{ background: 'url(' +NewsletterSection.Background_Image_1920x466px+ ')'}" v-if="!checkMobileOrNot">-->
  <div id="newsletter" v-if="!checkMobileOrNot">
      <div class="container">
        <div class="row title">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <svg x="0px" y="0px" width="68px" height="68px" viewBox="0 0 68 68" enable-background="new 0 0 68 68" xml:space="preserve">
    <g>
        <g>
            <path d="M61.797,8.311H6.204c-3.265,0-5.928,2.661-5.928,5.928v39.523c0,3.256,2.652,5.928,5.928,5.928h55.593    c3.256,0,5.928-2.652,5.928-5.928V14.239C67.725,10.982,65.072,8.311,61.797,8.311z M60.979,12.263L34.126,39.117L7.042,12.263    H60.979z M4.228,52.941V15.038l19.033,18.87L4.228,52.941z M7.022,55.736l19.045-19.045l6.674,6.615    c0.772,0.768,2.02,0.766,2.788-0.006l6.506-6.506l18.943,18.941H7.022z M63.773,52.941L44.832,34l18.941-18.942V52.941z"/>
        </g>
    </g>
    </svg>
             <h3>{{NewsletterSection.Title}}</h3>
             <!--<p v-html="NewsletterSection.Description"></p>-->
          </div>
        </div>
        <div class="row">
            <form style="display: block;display: flex;justify-content: center;">
                <!--<div class="col-lg-offset-2 col-md-offset-1 col-lg-3 col-md-4 col-sm-4 col-xs-12"><input class="input" :placeholder="NewsletterSection.Name_Placeholder" type="text"  v-model="newsltrname"></div>-->
                <!--<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"><input class="input" :placeholder="NewsletterSection.Email_Placeholder" type="text"  v-model="newsltremail"></div>-->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><input class="input" :placeholder="NewsletterSection.Email_Placeholder" type="text"  v-model="newsltremail"></div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12"><button type="submit" class="sub-btn"@click.prevent="sendnewsletter">{{NewsletterSection.Submit_Label}}</button></div>
            </form>
        </div>
      </div>
    </div>
  <div class="cont-sec">
    <div class="container">
      <div class="row">
      	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <ul>
          	<li><h3>{{FooterSection.Contact_Section_Title_1}} <br><span>{{FooterSection.Contact_Section_Title_2}}</span></h3></li>
            <li>
                  <div class="icon">
                    <i class="fa fa-mobile-phone" aria-hidden="true"></i>
                  </div>
                <div class="head">{{FooterSection.Phone_Label}}</div><br>
                <p><a :href="'tel:'+TopSection.Phone_Number">{{TopSection.Phone_Number}}</a></p>
            </li>
            <li>
                  <div class="icon size-34">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                  </div>
                <div class="head">{{FooterSection.Email_Label}}</div><br>
                <p><a :href="'mailto:'+FooterSection.Email_Address">{{FooterSection.Email_Address}}</a></p>
            </li>
            <li>
                  <div class="icon size-42">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                  </div>
                <div class="head">{{FooterSection.Address_Label}}</div><br>
                <p>{{FooterSection.Address}}</p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="inner">
    	<div class="row">
          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
            <div class="about">
              <div class="ftr-logo"><img :src="FooterSection.Footer_Logo_188_x_70px" alt=""></div>
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
            <div class="link">
              <h3>{{FooterSection.Corporate_Label}}</h3>
              <ul>
                <li> <a href="/FlyKey/index.html">{{MainSection.Home_Label}}</a></li>
                <li> <a href="/FlyKey/aboutus.html">{{MainSection.About_Us_Label}}</a></li>
                <!--<li> <a href="/FlyKey/package.html">{{MainSection.Holidays_Label}}</a></li>-->

              </ul>
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
            <div class="link">
              <h3>{{FooterSection.Legal}}</h3>
              <ul>
                <li> <a href="/FlyKey/termsandconditions.html">{{FooterSection.Terms_Label}}</a></li>
                <li> <a href="/FlyKey/privacypolicy.html">{{FooterSection.Privacy_Label}}</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
            <div class="link">
              <h3>{{FooterSection.Support_Label}}</h3>
              <ul>
                <li> <a href="/FlyKey/contactus.html">{{MainSection.Contact_Us_Label}}</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
            <div class="link">
              <h3>{{FooterSection.Social_Media_Label}}</h3>
              <ul>
                <li v-for="item in FooterSection.Social_Media_List"><a :href="item.URL" target="_blank"><i class="fa" v-bind:class="item.Icon"></i>{{item.Name}}</a></li>
              </ul>
            </div>
          </div>
          <!--<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
            <div class="link">
              <h3>{{FooterSection.Countries_Label}}</h3>
              <ul>
                <li v-for="country in FooterSection.Country_List"> <a href="#">{{country.Country_Name}}</a></li>
              </ul>
            </div>
          </div>-->
        </div>
    </div>
  </div>
  <div class="ftr-btm">
    <div class="container">
		<div class="inner">
        	<div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="copyright">{{FooterSection.Copyright_Content}}</div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="powered-by">{{FooterSection.Powered_By_Label}} &nbsp; <a href="http://www.oneviewit.com/" target="_blank"><img alt="" :src="FooterSection.Oneview_Logo_95_x_34px"></a></div>
              </div>
            </div>
            </div>
          <div class="back-top text-center" title="Back to Top"> <a href="#" id="toTop" style="display: inline;"> <i class="fa fa-arrow-circle-up" aria-hidden="true"></i> </a> </div>
    </div>
  </div>
</div>
    `,
  data() {
    return {
      newsltremail: null,
      newsltrname: null,
      TopSection: {},
      MainSection: {},
      FooterSection: {},
      NewsletterSection: {},
      isLoading: false,
      fullPage: true,
    }
  },
  computed: {
    checkMobileOrNot: function () {
      if( screen.width <= 1020 ) {
        return true;
    }
    else {
        return false;
    }
    },
},
  methods: {
    getValidationMsgByCode: function (code) {
      if (sessionStorage.validationItems !== undefined) {
          var validationList = JSON.parse(sessionStorage.validationItems);
          for (let validationItem of validationList.Validation_List) {
              if (code === validationItem.Code) {
                  return validationItem.Message;
              }
          }
      }
   },
    getpagecontent: function () {
      var self = this;
      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/01 Master/01 Master/01 Master.ftl';
        axios.get(pageurl, {
          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        }).then(function (response) {
          if (response.data.area_List.length) {
            var headerSection = pluck('Top_Section', response.data.area_List);
            self.TopSection = getAllMapData(headerSection[0].component);
            var mainContent = pluck('Main_Section', response.data.area_List);
            self.MainSection = getAllMapData(mainContent[0].component);
            var FooterContent = pluck('Footer_Section', response.data.area_List);
            self.FooterSection = getAllMapData(FooterContent[0].component);
            var NewsletterContent = pluck('Newsletter_Section', response.data.area_List);
            self.NewsletterSection = getAllMapData(NewsletterContent[0].component);


          }
        }).catch(function (error) {
          console.log('Error');
        });
      });

    },
    sendnewsletter: async function () {
      // if (!this.newsltrname) {
      //   alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M025')).set('closable', false);;
      //   return false;
      // }
      if (!this.newsltremail) {
        alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M026')).set('closable', false);;
        return false;
      }
     
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.newsltremail.match(emailPat);
      if (matchArray == null) {
        alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M027')).set('closable', false);;
        return false;
      } else {
        try {
          this.isLoading = true;
          this.agencyCode = JSON.parse(localStorage.User).loginNode.code || ""
          var agencyCode = this.agencyCode;
          var filterValue = "type='Newsletter' AND keyword1='" + this.newsltrname + "' AND keyword2='" + this.newsltremail + "'";
          var allDBData = await this.getDbData4Table(this.agencyCode, filterValue, "date1");

          if (allDBData != undefined && allDBData.data != undefined && allDBData.status == 200) {
            alertify.alert(this.getValidationMsgByCode('M022'), this.getValidationMsgByCode('M028')).set('closable', false);
            self.isLoading = false;
            console.log(allDBData);
            return false;

          }
          else {
            try {
              // var frommail = JSON.parse(localStorage.User).loginNode.email;
             // var frommail = "itsolutionsoneview@gmail.com";
              var toEmail = JSON.parse(localStorage.User).loginNode.email;
              var frommail=JSON.parse(localStorage.User).loginNode.email;   
              var postData = {
                  type: "AdminContactUsRequest",
                  fromEmail:frommail,
                  toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                  logo: ServiceUrls.hubConnection.logoBaseUrl +JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                  agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                  agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                  personName: this.cntusername,
                  emailId: this.cntemail,
                  contact: this.cntcontact,
                  message: this.cntmessage,

              };
              var custmail = {
                type: "UserAddedRequest",
                fromEmail: frommail,
                toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                logo: JSON.parse(localStorage.User).loginNode.logo,
                agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                personName: this.newsltrname,
                primaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                secondaryColor: '#' + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary
              };
              var agencyCode = this.agencyCode
              let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
              let insertSubscibeData = {
                type: "Newsletter",
                date1: requestedDate,
                keyword1: this.newsltrname,
                keyword2: this.newsltremail,
                nodeCode: agencyCode
              };
              let responseObject = await cmsRequestData("POST", "cms/data", insertSubscibeData, null);
              try {
                let insertID = Number(responseObject);
                this.newsltremail = '';
                this.newsltrname = '';
                var emailApi = ServiceUrls.emailServices.emailApi;
                sendMailService(emailApi, custmail);
                sendMailService(emailApi, postData);
                alertify.alert(this.getValidationMsgByCode('M023'), this.getValidationMsgByCode('M029')).set('closable', false);;
                this.isLoading = false;
              } catch (e) {
                this.isLoading = false;
              }
            } catch (error) {
              self.isLoading = false;
            }

          }
        } catch (error) {
          self.isLoading = false;
        }


      }
    },
    async getDbData4Table(agencyCode, extraFilter, sortField) {

      var allDBData = [];
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      var cmsURL = huburl + portno + '/cms/data/search/byQuery';
      var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
      if (extraFilter != undefined && extraFilter != '') {
        queryStr = queryStr + " AND " + extraFilter;
      }
      var requestObject = {
        query: queryStr,
        sortField: sortField,
        from: 0,
        orderBy: "desc"
      };
      let responseObject = await cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
      if (responseObject != undefined && responseObject.data != undefined) {
        allDBData = responseObject.data;
      }
      return allDBData;

    }

  },
  mounted: function () {
    document.onreadystatechange = () => {
      if (document.readyState == "complete") {
        if (localStorage.direction == 'rtl') {
          $(".ar_direction").addClass("ar_direction1");
          document.getElementById("styleid").setAttribute("href", 'css/rtl-style.css');
          document.getElementById("mediaid").setAttribute("href", 'css/rtl-media.css');
          alertify.defaults.glossary.ok = 'حسنا';
        } else {
          $(".ar_direction").removeClass("ar_direction1");
          document.getElementById("styleid").setAttribute("href", 'css/ltr-style.css');
          document.getElementById("mediaid").setAttribute("href", 'css/ltr-media.css');
          alertify.defaults.glossary.ok = 'OK';
        }
      }
    }
    this.getpagecontent();
    
/* Scroll To Top JS
============================================================== */
jQuery(window).scroll(function () {
	scrollToTop('show');
});

jQuery(document).ready(function () {
	scrollToTop('click');
});

/* Animated Function */
function scrollToTop(i) {
	if (i == 'show') {
		if (jQuery(this).scrollTop() != 0) {
			jQuery('#toTop').fadeIn();
		} else {
			jQuery('#toTop').fadeOut();
		}
	}
	if (i == 'click') {
		jQuery('#toTop').click(function () {
			jQuery('body,html').animate({
				scrollTop: 0
			}, 500);
			return false;
		});
	}
}
  },

})

var footerinstance = new Vue({
  el: 'footer',
  name: 'footerArea',
  data() {
    return {
      key: 0,
      content: null,
      getdata: true
    }

  },
});
