const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
Vue.component('loading', VueLoading)
var packagelist = new Vue({
    i18n,
    el: '#privacyPolicy',
    name: 'privacyPolicy',
    data: {
        termsPageDetails:{},
        commonPageDetails:{},
        isLoading: false,
        fullPage: true,
    },
    methods: {
        slider:function(){
            if(this.commonPageDetails.Partner_Details!=undefined&&this.commonPageDetails.Partner_Details.length>0){
                Vue.nextTick(function() {
                    $('.autoplay').slick({
                        slidesToShow: 6,
                        slidesToScroll: 1,
                        autoplay: true,
                        autoplaySpeed: 2000,
                        responsive: [
                          {
                            breakpoint: 1024,
                            settings: {
                              slidesToShow: 6,
                              slidesToScroll: 3,
                              infinite: true,
                              dots: false
                            }
                          },
                          {
                            breakpoint: 990,
                            settings: {
                              slidesToShow: 5,
                              slidesToScroll: 2
                            }
                          },
                          {
                            breakpoint: 600,
                            settings: {
                              slidesToShow: 2,
                              slidesToScroll: 1
                          }
                        }
                      ]
                    });
                },this);
            }
            
            
        },getPagecontent: function() {
            var self = this;
            self.isLoading = true;
            console.log("TEST");
            getAgencycode(function(response) {
                var Agencycode = response;
                
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var aboutUsurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Privacy Policy/Privacy Policy/Privacy Policy.ftl';
                var homePageURL = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/General Details/General Details/General Details.ftl';

                axios.get(aboutUsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                        console.log("TEST");
                        if(response.data.area_List[0].Privacy_Policy!=undefined){
                            var titleDataTemp =self.getAllMapData(response.data.area_List[0].Privacy_Policy.component);
                            self.termsPageDetails=titleDataTemp;
                            console.log(titleDataTemp);
                        }
                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.isLoading = false;
                });

                axios.get(homePageURL, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    if (response.data.area_List.length) {
                        if(response.data.area_List[1].Common_Details!=undefined){
                            var titleDataTemp =self.getAllMapData(response.data.area_List[1].Common_Details.component);
                            self.commonPageDetails=titleDataTemp;
                            self.slider();
                        }
                    }


                    self.isLoading = false;


                }).catch(function (error) {
                    console.log('Error');
                   
                });
                

            });

        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        }

        
    },
    updated: function () {
        
    },
    mounted: function() {
        
        this.getPagecontent();
       
    },watch:{
        
    }
});

