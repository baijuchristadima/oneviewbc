const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
Vue.component('loading', VueLoading)
var packagelist = new Vue({
    i18n,
    el: '#contactUS',
    name: 'contactUS',
    data: {
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        contactUsPage:{},
        formDetails:{},
        commonPageDetails:{},
        agencyCode:"",
        formData:{FirstName:"",LastName:"",Email:"",Phone:"",Message:""},
        reg: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/,
        isLoading: false,
        fullPage: true,

    },
    methods: {
        slider:function(){
            if(this.commonPageDetails.Partner_Details!=undefined&&this.commonPageDetails.Partner_Details.length>0){
                Vue.nextTick(function() {
                    $('.autoplay').slick({
                        slidesToShow: 6,
                        slidesToScroll: 1,
                        autoplay: true,
                        autoplaySpeed: 2000,
                        responsive: [
                          {
                            breakpoint: 1024,
                            settings: {
                              slidesToShow: 6,
                              slidesToScroll: 3,
                              infinite: true,
                              dots: false
                            }
                          },
                          {
                            breakpoint: 990,
                            settings: {
                              slidesToShow: 5,
                              slidesToScroll: 2
                            }
                          },
                          {
                            breakpoint: 600,
                            settings: {
                              slidesToShow: 2,
                              slidesToScroll: 1
                          }
                        }
                      ]
                    });
                },this);
            }
            
        },getPagecontent: function() {
            var self = this;
            self.isLoading = true;
            getAgencycode(function(response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.dir = langauage == "ar" ? "rtl" : "ltr";
                var contactUsurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Contact US/Contact US/Contact US.ftl';
                var homePageURL = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/General Details/General Details/General Details.ftl';
                self.agencyCode=Agencycode;
                
                axios.get(contactUsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                        if(response.data.area_List[0].Page_Details!=undefined){
                            var titleDataTemp =self.getAllMapData(response.data.area_List[0].Page_Details.component);
                            self.contactUsPage=titleDataTemp;
                        }
                        if(response.data.area_List[1].Contact_Form!=undefined){
                            var titleDataTemp =self.getAllMapData(response.data.area_List[1].Contact_Form.component);
                            self.formDetails=titleDataTemp;
                            console.log(self.formDetails);
                        }
                    }
                }).catch(function(error) {
                    console.log('Error');
                    self.isLoading = false;
                });

                axios.get(homePageURL, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function(response) {
                    if (response.data.area_List.length > 0) {
                        if(response.data.area_List[1].Common_Details!=undefined){
                            var titleDataTemp =self.getAllMapData(response.data.area_List[1].Common_Details.component);
                            self.commonPageDetails=titleDataTemp;
                            self.slider();
                            
                        }
                    }
                    self.isLoading = false;
                }).catch(function(error) {
                    console.log('Error');
                    self.isLoading = false;
                });

            });

        },send:function(){
            var msg="";
            var id="";
            if(this.formData.FirstName==undefined||this.formData.FirstName==''){
                msg="Please Enter first name to proceed.";
                id="fname";
            }else if(this.formData.LastName==undefined||this.formData.LastName==''){
                msg="Please Enter last name to proceed.";
                id="lname";
            }else if(this.formData.Email==undefined||this.formData.Email==''){
                msg="Please Enter email to proceed.";
                id="email";
            }else if(!this.reg.test(this.formData.Email)){
                msg="Please Enter valid email to proceed.";
                id="email";
            }else if(this.formData.Phone==undefined||this.formData.Phone==''){
                msg="Please Enter phone to proceed.";
                id="phone";
            }else if(this.formData.Message==undefined||this.formData.Message==''){
                msg="Please Enter message to proceed.";
                id="message";
            }

            if(msg!=''){
                
                $("#"+id).focus();
                alertify.alert('Alert',msg);
                return;
            }else{
                self.isLoading = true;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let requestObject = {
                    type: "Contact US",
                    nodeCode: this.agencyCode,
                    keyword1: this.formData.FirstName,
                    keyword2: this.formData.LastName,
                    keyword3: this.formData.Email,
                    keyword4: this.formData.Phone,
                    text1:this.formData.Message,
                    date1: requestedDate,
                    
                }
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var cmsURL = huburl + portno + '/cms/data';

                let axiosConfig = {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                    }
                };

                axios.post(cmsURL, JSON.stringify(requestObject),axiosConfig).then(function(response) {
                    console.log(response);
                    alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
                }).catch(function(error) {
                    console.log('Error');
                });


            }
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        }

        
    },
    updated: function () {
        
    },
    mounted: function() {
        
        this.getPagecontent();
       
    },watch:{
        commonPageDetails: function () {
            
        }
    }
});

