var generalInformation = {
    systemSettings: {
        calendarDisplay: 1,
        calendarDisplayInMobile: 1
    },
}
var images = [];
var currimg = 0;
const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var flightserchfromComponent = Vue.component('flightserch', {
    data() {
        return {
            access_token: '',
            KeywordSearch: '',
            resultItemsarr: [],
            autoCompleteProgress: false,
            highlightIndex: 0
        }
    },
    props: {
        itemText: String,
        itemId: String,
        placeHolderText: String,
        returnValue: Boolean,
        id: { type: String, default: '', required: false },
        leg: Number,
        //KeywordSearch:String
    },

    template: `<div class="autocomplete">
        <input type="text" :disabled="id=='Departurefrom1'" :placeholder="placeHolderText" :id="id" :data-aircode="KeywordSearch" autocomplete="off" 
        v-model="KeywordSearch" class="form-control" 
        :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !autoCompleteProgress  }" 
        @input="onSelectedAutoCompleteEvent($event);highlightIndex=0"
            @keydown.down="down"
            @keydown.up="up"           
            @keydown.esc="autoCompleteProgress=false"
            @keydown.enter="onSelected(resultItemsarr[highlightIndex])"
            @keydown.tab="tabclick(resultItemsarr[highlightIndex])"/>
        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>        
        <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length>0">
            <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item)">
                {{ item.label }}
            </li>
        </ul>
    </div>`,
    mounted() {
        if (this.id == "Departurefrom1") {
            this.onSelectedAutoCompleteEvent("", this.onSelected);
        }
    },

    methods: {
        up: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex < this.resultItemsarr.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
            if (this.$refs.options[this.highlightIndex]) {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            }

        },
        onSelectedAutoCompleteEvent: _.debounce(async function (event, callback) {
            var self = this;
            var keywordEntered = "KRT";
            if (event) {
                keywordEntered = event.target.value;
            }
            if (keywordEntered.length > 2) {
                this.autoCompleteProgress = true;
                self.resultItemsarr = await getFlightResultUsingElasticSearch(keywordEntered);
               
                if (callback) {
                    callback(self.resultItemsarr[0]);
                }
            

            } else {
                this.autoCompleteProgress = false;
                this.resultItemsarr = [];

            }
        }, 100),
        onSelected: function (item) {
            this.KeywordSearch = item.label;
            this.resultItemsarr = [];
            var targetWhenClicked = $(event.target).parent().parent().find("input").attr("id");
            console.log(maininstance.triptype);
            console.log(targetWhenClicked);
            console.log(event.target.id);
            if (maininstance.triptype != 'M') {
                if (event.target.id == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
                    $('#Cityto1').focus();
                } else if (event.target.id == "Cityto1" || targetWhenClicked == "Cityto1") {
                    $('#deptDate01').focus();
                }
            } else {
                var eventTarget = event.target.id;
                $(document).ready(function () {
                    if (eventTarget == "Cityfrom1" || targetWhenClicked == "Cityfrom1") {
                        $('#Cityto1').focus();
                    } else if (eventTarget == "Cityto1" || targetWhenClicked == "Cityto1") {
                        $('#deptDate01').focus();
                    } else if (eventTarget == "DeparturefromLeg1" || targetWhenClicked == "DeparturefromLeg1") {
                        $('#ArrivalfromLeg1').focus();
                    } else if (eventTarget == "ArrivalfromLeg1" || targetWhenClicked == "ArrivalfromLeg1") {
                        $('#txtLeg1Date').focus();
                    } else if (eventTarget == "DeparturefromLeg2" || targetWhenClicked == "DeparturefromLeg2") {
                        $('#ArrivalfromLeg2').focus();
                    } else if (eventTarget == "ArrivalfromLeg2" || targetWhenClicked == "ArrivalfromLeg2") {
                        $('#txtLeg2Date').focus();
                    } else if (eventTarget == "DeparturefromLeg3" || targetWhenClicked == "DeparturefromLeg3") {
                        $('#ArrivalfromLeg3').focus();
                    } else if (eventTarget == "ArrivalfromLeg3" || targetWhenClicked == "ArrivalfromLeg3") {
                        $('#txtLeg3Date').focus();
                    } else if (eventTarget == "DeparturefromLeg4" || targetWhenClicked == "DeparturefromLeg4") {
                        $('#ArrivalfromLeg4').focus();
                    } else if (eventTarget == "ArrivalfromLeg4" || targetWhenClicked == "ArrivalfromLeg4") {
                        $('#txtLeg4Date').focus();
                    } else if (eventTarget == "DeparturefromLeg5" || targetWhenClicked == "DeparturefromLeg5") {
                        $('#ArrivalfromLeg5').focus();
                    } else if (eventTarget == "ArrivalfromLeg5" || targetWhenClicked == "ArrivalfromLeg5") {
                        $('#txtLeg5Date').focus();
                    } else if (eventTarget == "DeparturefromLeg6" || targetWhenClicked == "DeparturefromLeg6") {
                        $('#ArrivalfromLeg6').focus();
                    } else if (eventTarget == "ArrivalfromLeg6" || targetWhenClicked == "ArrivalfromLeg6") {
                        $('#txtLeg6Date').focus();
                    }
                });

            }

            this.$emit('air-search-completed', item.code, item.label, this.leg);


        },
        tabclick: function (item) {
            if (!item) {

            } else {
                this.onSelected(item);
            }
        }

    },
    watch: {
        returnValue: function () {
            this.KeywordSearch = this.itemText;
        }

    }

});

Vue.component('loading', VueLoading)

var maininstance = new Vue({
    i18n,
    el: '#indexmain',
    name: 'indexmain',
    data: {
        titleDetails: { Title: '' },
        titleComponent: { Banner_Title: '' },
        banner: null,
        MainArea: {
            Flights_Label: '',
            Hotels_Label: '',
            Holidays_Label: '',
            Holidays_Label: '',
            Oneway_Label: '',
            Round_Trip_Label: '',
            Multi_City_Label: '',
            Package_Title: '',
            Roomslabel: ''
        },
        PromotionArea: {
            Image: '',
            Save_More_On_Label: '',
            Title: '',
            Description: '',
        },
        getpackage: false,
        destinations: null,
        air: null,
        packageArea: {
            Destination_Title: '',
            Airline_Title: ''
        },
        flightDeals: null,
        holidayPackages: null,
        triptype: "R",
        CityFrom: '',
        CityTo: '',
        validationMessage: '',
        adtcount: 1,
        chdcount: 0,
        infcount: 0,
        preferAirline: '',
        returnValue: true,
        legcount: 1,
        legs: [],
        cityList: [],
        adt: "adult",
        placeholderfrom: "Enter Airport or City",
        placeholderTo: "Enter Airport or City",
        cabinclass: [{ 'value': 'Y', 'text': 'Economy' },
        { 'value': 'C', 'text': 'Business' },
        { 'value': 'F', 'text': 'First' }
        ],
        selected_cabin: 'Y',
        selected_adults: 1,
        selected_children: 0,
        selected_infant: 0,
        totalAllowdPax: 9,
        //selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'NGN',
        //CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        arabic_dropdown: '',
        childLabel: "",
        childrenLabel: "",
        adultLabel: "",
        adultsLabel: "Adults",
        ageLabel: "year",
        agesLabel: "years",
        infantLabel: "Infant",
        searchBtnLabel: "Search",
        addUptoLabel: "add upto",
        tripsLabel: "trips",
        tripLabel: "trip",
        Totaltravaller: '1 Travellers',
        travellerdisply: false,
        travellerdisplymul: false,
        child: 0,
        flightSearchCityName: { cityFrom1: '', cityTo1: '', cityFrom2: '', cityTo2: '', cityFrom3: '', cityTo3: '', cityFrom4: '', cityTo4: '', cityFrom5: '', cityTo5: '', cityFrom6: '', cityTo6: '' },
        advncedsearch: false,

        direct_flight: false,
        airlineList: AirlinesDatas,
        Airlineresults: [],
        selectedAirline: [],
        adultrange: '',
        childrange: '',
        infantrange: '',
        donelabel: 'Done',
        classlabel: 'class',
        hotelInit: Math.random(), //hotel init 
        iName: null,
        iDob: null,
        iPassportNo: null,
        iGender: null,
        iPhoneno: null,
        iEmail: null,
        iOccupation: null,
        iDestination: null,
        iPeriod: null,
        iDepartureDate: null,
        iReturnDate: null,
        iReturnDate: null,
        iAddress: null,
        iAddressOffice: null,
        iPurpose: null,
        insterms: true,
        cartimedisplay: false,
        homepageDetails: { About_US_Title: "About Us" },
        commonPageDetails: {},
        flightInChild: [{ Name: "0", Value: "0" }],
        flight_infant: [{ Name: "0", Value: "0" }],
        tripDates: { from1: "", from2: "", from3: "", from4: "", from5: "", from6: "", from7: "", to1: "", to2: "", to3: "", to4: "" },
        actvetab: sessionStorage.active_el ? (sessionStorage.active_el == 0 || sessionStorage.active_el == 4) ? 1 : sessionStorage.active_el : 1,
        isLoading: false,
        fullPage: true,
    },
    methods: {

        pageContent: function () {
            var self = this;
            // self.isLoading = true;
            var date = moment(new Date()).format("MM/DD/YYYY");
            var dateTo = moment(new Date().setDate(new Date().getDate() + 1)).format("MM/DD/YYYY");
            for (var index = 1; index <= 7; index++) {
                self.tripDates["from" + index] = date;
                self.tripDates["to" + index] = dateTo;
            }
            self.flightAdultChnage();



            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                // self.dir = langauage == "ar" ? "rtl" : "ltr";
                var homePageURL = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/General Details/General Details/General Details.ftl';
                var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Home/Home/Home.ftl';
                var holidaypackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Package/Package/Package.ftl';
                // var flightdealurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Flight deals/Flight deals/Flight deals.ftl';
                // self.eVisaUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/EVISA Page/EVISA Page/EVISA Page.ftl';
                // self.popupurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/News and Notice/News and Notice/News and Notice.ftl';
                axios.get(homePageURL, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    if (response.data.area_List.length) {

                        if (response.data.area_List[0].Home_Page_Details != undefined) {
                            var titleDataTemp = self.getAllMapData(response.data.area_List[0].Home_Page_Details.component);
                            self.homepageDetails = titleDataTemp;
                        }
                        if (response.data.area_List[1].Common_Details != undefined) {
                            var titleDataTemp = self.getAllMapData(response.data.area_List[1].Common_Details.component);
                            self.commonPageDetails = titleDataTemp;
                            self.slider();
                        }
                    }


                    // self.isLoading = false;
                    self.stopLoader();

                }).catch(function (error) {
                    console.log('Error');
                    // self.isLoading = false;
                    self.stopLoader();
                });


                /*axios.get(pageurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                    self.content = response.data;
                    if (response.data.area_List.length) {
                        var titleComponent = self.pluck('Title', self.content.area_List);
                        self.titleComponent.Banner_Title = self.pluckcom('Banner_Title', titleComponent[0].component);

                        var bannerCompnt = self.pluckcom('Banner_Image', titleComponent[0].component);
                        self.banner = bannerCompnt;

                        var Main_Area = self.pluck('Main_Area', self.content.area_List);
                        self.MainArea.Flights_Label = self.pluckcom('Flights_Label', Main_Area[0].component);
                        self.MainArea.Hotels_Label = self.pluckcom('Hotels_Label', Main_Area[0].component);
                        self.MainArea.Holidays_Label = self.pluckcom('Holidays_Label', Main_Area[0].component);
                        self.MainArea.Oneway_Label = self.pluckcom('Oneway_Label', Main_Area[0].component);
                        self.MainArea.Round_Trip_Label = self.pluckcom('Round_Trip_Label', Main_Area[0].component);
                        self.MainArea.Multi_City_Label = self.pluckcom('Multi_City_Label', Main_Area[0].component);
                        self.MainArea.Package_Title = self.pluckcom('Package_Title', Main_Area[0].component);
                        //self.MainArea.Roomslabel = self.pluckcom('RoomsLabel', Main_Area[0].component);

                        var PromotionArea = self.pluck('Promotion_Area', self.content.area_List);
                        self.PromotionArea.Image = self.pluckcom('Image', PromotionArea[0].component);
                        self.PromotionArea.Save_More_On_Label = self.pluckcom('Save_More_On_Label', PromotionArea[0].component);
                        self.PromotionArea.Title = self.pluckcom('Title', PromotionArea[0].component);
                        self.PromotionArea.Description = self.pluckcom('Description', PromotionArea[0].component);
            



                        var packageArea = self.pluck('Top_Destination_and_Airlines', self.content.area_List);
                        self.packageArea.Destination_Title = self.pluckcom('Destination_Title', packageArea[0].component);
                        self.packageArea.Airline_Title = self.pluckcom('Airline_Title', packageArea[0].component);

                        var desti = self.pluck('Top_Destination_and_Airlines', self.content.area_List);
                        self.destinations =  self.pluckcom('Destination', desti[0].component);

                        var desti = self.pluck('Top_Destination_and_Airlines', self.content.area_List);
                        self.air =  self.pluckcom('Airlines', desti[0].component);
                      



                   

                       // self.bindBanner();
                        self.holidayPackageCard(holidaypackageurl, langauage);
                        self.flightDealsCard(flightdealurl, langauage);

                    }




                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });*/
            });




        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            contentArry.map(function (item) {
                let allKeys = Object.keys(item)
                for (let j = 0; j < allKeys.length; j++) {
                    let key = allKeys[j];
                    let value = item[key];

                    if (key != 'name' && key != 'type') {
                        if (value == undefined || value == null) {
                            value = "";
                        }
                        tempDataObject[key] = value;
                    }
                }
            });
            return tempDataObject;
        },
        onCheck: function (e) {
            var currid = $(e.target).attr('id');
            if (currid == 'inlineRadio2') {
                $('#to1').parent().find("button").css("pointer-events", "none");

            } else {
                if ($('#to1').parent().find("button") != undefined) {
                    $('#to1').parent().find("button").css("pointer-events", "unset");
                }

            }

            var selectedDate = $("#deptDate01").val();
            $("#retDate").datepicker("option", "minDate", selectedDate);
            $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
            $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
        },
        holidayPackageCard(holidaypackageurl, langauage) {
            var self = this;
            axios.get(holidaypackageurl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {


                let holidayaPackageListTemp = [];
                if (response != undefined && response.data != undefined && response.data.Values != undefined) {
                    holidayaPackageListTemp = response.data.Values.filter(function (el) {
                        return el.Status == true &&
                            el.Show_In_Home_Page == true
                    });
                }
                self.holidayPackages = holidayaPackageListTemp;
                if (self.holidayPackages != null) {
                    self.getpackage = true;
                    setTimeout(function () { setCarousel() }, 10);
                }
            }).catch(function (error) {
                console.log('Error');
                self.flightDeals = [];
            });

        },
        flightDealsCard(flightdealurl, langauage) {
            var self = this;
            axios.get(flightdealurl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
                // self.contentPackage = response.data.Values;
                var DealListTemp = [];
                if (response != undefined && response.data != undefined && response.data.Values != undefined) {
                    DealListTemp = response.data.Values.filter(function (el) {
                        return el.Status == true &&
                            el.Show_In_Home_Page == true
                    });
                    if (DealListTemp.length > 0) {
                        self.flightDeals = DealListTemp;
                        //  setTimeout(function () { dealsCarousel() }, 10);
                    }

                }


            }).catch(function (error) {
                console.log('Error');
                self.flightDeals = [];
            });




        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = null;
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        Departurefrom(AirportCode, AirportName, leg) {

            if (Number(leg) >= 0) {
                var from = 'Departurefrom' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].to) {
                        this.returnValue = false;
                        this.flightSearchCityName[from] = "";
                        alertify.alert('Alert', 'Departure and arrival airports should not be same !');
                    } else {
                        this.cityList[index].from = AirportCode
                        this.flightSearchCityName[from] = AirportName;
                        this.returnValue = true;
                    }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: AirportCode,
                        to: ''

                    });
                    this.flightSearchCityName[from] = AirportName;
                    this.returnValue = true;
                }

            }
        },
        Arrivalfrom(AirportCode, AirportName, leg) {
            if (Number(leg) >= 0) {
                var to = 'DepartureTo' + leg;
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    if (AirportCode == this.cityList[index].from) {
                        this.returnValue = false;
                        this.flightSearchCityName[to] = "";
                        alertify.alert('Alert', 'Departure and arrival airports should not be same !');

                    } else {
                        this.cityList[index].to = AirportCode;
                        this.flightSearchCityName[to] = AirportName;
                        this.returnValue = true;
                    }
                } else {
                    this.cityList.push({
                        id: leg,
                        from: '',
                        to: AirportCode

                    });
                    this.flightSearchCityName[to] = AirportName;
                    this.returnValue = true;
                }
            }
        },
        slider: function () {
            if (this.commonPageDetails.Partner_Details != undefined && this.commonPageDetails.Partner_Details.length > 0) {
                Vue.nextTick(function () {
                    $('.autoplay').slick({
                        slidesToShow: 6,
                        slidesToScroll: 1,
                        autoplay: true,
                        autoplaySpeed: 2000,
                        responsive: [
                            {
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 6,
                                    slidesToScroll: 3,
                                    infinite: true,
                                    dots: false
                                }
                            },
                            {
                                breakpoint: 990,
                                settings: {
                                    slidesToShow: 5,
                                    slidesToScroll: 2
                                }
                            },
                            {
                                breakpoint: 600,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1
                                }
                            }
                        ]
                    });
                }, this);
            }


        },
        getMonthofDate: function (date) {
            var travelDateDept = "";
            if (date != undefined && date != '') {
                travelDateDept = moment(date).format('MMMM');
            }
            return travelDateDept;
        },
        getDateofDate: function (date) {
            var travelDateDept = "";
            if (date != undefined && date != '') {
                travelDateDept = moment(date).format('DD');
            }
            return travelDateDept;
        },
        getDayofDate: function (date) {
            var travelDateDept = "";
            if (date != undefined && date != '') {
                travelDateDept = moment(date).format('dddd');
            }
            return travelDateDept;
        },
        MultiSearchFlight: function () {
            var sectors = '';
            var legDetails = [];

            for (var legValue = 0; legValue <= this.legcount; legValue++) {

                if (this.cityList.length != 0 && this.cityList[legValue] != undefined) {
                    var departureDate = $('#from' + (Number(legValue) + 1)).val() == "" ? "" : $('#from' + (Number(legValue) + 1)).datepicker('getDate');
                    var cityObject = this.cityList[legValue];
                    var departureFrom = cityObject.from;
                    var arrivalTo = cityObject.to;
                    console.log(departureFrom + "-" + arrivalTo);
                    console.log(departureDate);
                    legDetails.push(departureFrom + '|' + arrivalTo)
                    if (departureFrom == undefined || departureFrom.trim() == '') {
                        $("#Departurefrom" + (Number(legValue) + 1)).focus();
                        alertify.alert('Alert', 'Please fill origin !');
                        return;
                    } else if (arrivalTo == undefined || arrivalTo.trim() == '') {

                        $("#DepartureTo" + (Number(legValue) + 1)).focus();
                        alertify.alert('Alert', 'Please fill destination ! ');
                        return;
                    } else if (departureDate == undefined || departureDate.toString().trim() == '') {
                        $("#from" + (Number(legValue) + 1)).focus();
                        alertify.alert('Alert', 'Please choose departure date !').set('closable', false);
                        return;
                    }
                    var travelDateDept = moment(departureDate).format('DD|MM|YYYY');
                    if (sectors == '') {
                        sectors = "/" + departureFrom + '-' + arrivalTo + '-' + travelDateDept;
                    } else {
                        sectors = sectors + "/" + departureFrom + '-' + arrivalTo + '-' + travelDateDept;
                    }
                }
            }
            console.log("sectors : " + sectors);
            if (sectors == '') {
                $("#Departurefrom1").focus();
                alertify.alert('Alert', 'Please fill origin !');
                return;
            }
            this.isLoading = true;
            var directFlight = this.direct_flight ? 'DF' : 'AF';

            var adult = this.selected_adults;
            var child = this.selected_children;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            getSuppliers(legDetails,
            function(supp) {
                var searchUrl = '/Flights/flight-listing.html?flight=' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                // searchUrl = searchUrl.toLocaleLowerCase();
                window.location.href = searchUrl;
            })

        },
        SearchFlight: function () {

            if (this.triptype == 'R' || this.triptype == 'O') {
                var Departuredate = $('#from1').val() == "" ? "" : $('#from1').datepicker('getDate');
                var arrivaldate = $('#to1').val() == "" ? "" : $('#to1').datepicker('getDate');
                var sectors = "";
                for (var legValue = 0; legValue <= 0; legValue++) {

                    if (this.cityList.length != 0 && this.cityList[legValue] != undefined) {
                        var departureDate = $('#from' + (Number(legValue) + 1)).val() == "" ? "" : $('#from' + (Number(legValue) + 1)).datepicker('getDate');
                        var arrivaldate = $('#to' + (Number(legValue) + 1)).val() == "" ? "" : $('#to' + (Number(legValue) + 1)).datepicker('getDate');
                        var cityObject = this.cityList[legValue];
                        var departureFrom = cityObject.from;
                        var arrivalTo = cityObject.to;

                        if (departureFrom == undefined || departureFrom == '') {
                            $("#Departurefrom" + (Number(legValue) + 1)).focus();
                            alertify.alert('Alert', 'Please fill origin !');
                            return;
                        } else if (arrivalTo == undefined || arrivalTo == '') {
                            $("#DepartureTo" + (Number(legValue) + 1)).focus();
                            alertify.alert('Alert', 'Please fill destination ! ');
                            return;
                        } else if (departureDate == undefined || departureDate == '') {
                            $("#from" + (Number(legValue) + 1)).focus();
                            alertify.alert('Alert', 'Please choose departure date !').set('closable', false);
                            return;
                        } else if (this.triptype == 'R' && (arrivaldate == undefined || arrivaldate == '')) {
                            $("#to" + (Number(legValue) + 1)).focus();
                            alertify.alert('Alert', 'Please choose return date !').set('closable', false);
                            return;
                        }
                        var travelDateDept = moment(departureDate).format('DD|MM|YYYY');

                        if (this.triptype == 'R') {
                            var ArrivalDate = moment(arrivaldate).format('DD|MM|YYYY');
                            sectors = departureFrom + '-' + arrivalTo + '-' + travelDateDept + '/' + arrivalTo + '-' + departureFrom + '-' + ArrivalDate;;
                        } else {
                            sectors = departureFrom + '-' + arrivalTo + '-' + travelDateDept;
                        }
                    }
                }
                if (sectors == '') {
                    $("#Departurefrom1").focus();
                    alertify.alert('Alert', 'Please fill origin !');
                    return;
                }
                this.isLoading = true;
                var directFlight = this.direct_flight ? 'DF' : 'AF';

                var adult = this.selected_adults;
                var child = this.selected_children;
                var infant = this.selected_infant;
                var cabin = this.selected_cabin;
                var tripType = this.triptype;
                var preferAirline = this.preferAirline;
                getSuppliers([departureFrom + '|' + arrivalTo],
                function(supp) {
                    var searchUrl = '/Flights/flight-listing.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-'+supp+'-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
                    // searchUrl = searchUrl.toLocaleLowerCase();
                    sessionStorage.active_el = 1;
                    window.location.href = searchUrl;
                })
            } else {
                console.log(this.triptype);
                this.MultiSearchFlight();
            }

        },
        AddNewLeg() {
            if (this.legcount <= 5) {
                ++this.legcount;
                var legno = 1;
                if (this.cityList.length > 0) {
                    legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                    legno = legno + 1;
                }

                this.cityList.push({
                    id: legno,
                    from: '',
                    to: ''
                });

            }
            console.log(this.cityList);
        },
        DeleteLeg(leg) {
            var legs = this.legcount;
            if (legs > 1) {
                --this.legcount;
                var legno = Math.max.apply(Math, this.cityList.map(function (o) { return o.id; }));
                var index = this.cityList.findIndex(function (element) {
                    return element.id === leg;
                })
                if (index !== -1) {
                    this.cityList.splice(index, 1);

                }


            }
        },
        setCalender() {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = generalInformation.systemSettings.calendarDisplay;

            var self = this;






            $("#from1, #to1,#from2,#from3,#from4,#from5,#from6,#from7,#to, #to1, #to2, #to3, #to4, #to5, #to6").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                dateFormat: systemDateFormat,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",

                showOptions: {
                    direction: "up"
                },
                beforeShow: function (event, ui) {
                    var targetID = this.id;
                    $("#" + targetID).datepicker("setDate", self.tripDates[targetID]);
                    //  $("#"+targetID).datepicker("option", "setDate", self.tripDates[targetID] );
                    if (targetID.includes("from")) {
                        var targetNumber = Number(targetID.replace("from", "").trim());
                        if (targetNumber == 1) {

                        } else {
                            var prevID = Number(targetNumber) - 1;
                            var selectedDate = $("#from" + prevID).datepicker('getDate');
                            $("#" + targetID).datepicker("option", "minDate", selectedDate);
                        }
                    } else if (targetID.includes("to")) {
                        var targetNumber = Number(targetID.replace("to", "").trim());
                        var selectedDate = $("#from1").datepicker('getDate');
                        $("#" + targetID).datepicker("option", "minDate", selectedDate);
                    }
                },
                onSelect: function (selectedDate, date) {
                    var targetID = this.id;
                    if (targetID.includes("from")) {
                        var targetNumber = Number(targetID.replace("from", "").trim()) + 1;
                        if (self.triptype == 'M') {
                            var dateObject = new Date(selectedDate);
                            for (var k = targetNumber; k <= 7; k++) {
                                dateObject.setDate(dateObject.getDate() + 1);
                                $("#from" + k).datepicker("setDate", dateObject);
                                self.tripDates["from" + k] = moment(dateObject).format("MM/DD/YYYY");;
                            }
                        } else if (self.triptype == 'R') {
                            var dateObject = new Date(selectedDate);
                            dateObject.setDate(dateObject.getDate() + 1);
                            $("#to1").datepicker("setDate", dateObject);
                            self.tripDates["to1"] = moment(dateObject).format("MM/DD/YYYY");;
                        }
                        self.tripDates[targetID] = selectedDate;
                    } else if (targetID.includes("to")) {
                        self.tripDates[targetID] = moment($("#" + targetID).datepicker('getDate')).format("MM/DD/YYYY");
                        //  var targetNumber=Number(targetID.replace("to","").trim())+1;
                        //  $("#from"+targetNumber).datepicker("option", "minDate", selectedDate);
                    }
                }
            });
        },
        flightAdultChnage: function () {
            var adultObject = Number(this.selected_adults);
            var childVal = Number(this.totalAllowdPax) - Number(adultObject);
            var infantObject = [];
            var ChildTempObject = [];
            for (var i = 0; i <= Number(childVal); i++) {
                infantObject.push({ Name: i, Value: i });
            }
            for (var i = 0; i <= Number(childVal); i++) {
                ChildTempObject.push({ Name: i, Value: i });
            }



            this.selected_children = 0;
            this.selected_infant = 0;
            this.flightInChild = ChildTempObject;
            this.flight_infant = infantObject;

            var totalpax = Number(this.selected_adults) + Number(this.selected_children) + Number(this.selected_infant);
            if (parseInt(totalpax) > 1) {
                totalpax = totalpax + ' Passengers';
            } else {
                totalpax = totalpax + ' Passenger'
            }
            this.Totaltravaller = totalpax;


        },
        setpaxcount(item) {

            if (item == 'Adult') {
                this.flightAdultChnage();
            } else {



                if (item == 'Children') {
                    var tempPax = Number(this.selected_adults) + Number(this.selected_children);
                    var infantObject = [];
                    var total = Number(this.totalAllowdPax) - Number(tempPax);
                    for (var i = 0; i <= total; i++) {
                        infantObject.push({ Name: i, Value: i });
                    }
                    this.selected_infant = 0;
                    this.flight_infant = infantObject;
                } else if (item == 'Infant') {
                    var tempPax = Number(this.selected_adults) + Number(this.selected_infant);
                    var infantObject = [];
                    var total = Number(this.totalAllowdPax) - Number(tempPax);
                    console.log(total);
                    for (var i = 0; i <= total; i++) {
                        infantObject.push({ Name: i, Value: i });
                    }
                    this.selected_children = (Number(total) >= Number(this.selected_children)) ? this.selected_children : 0;
                    this.flightInChild = infantObject;

                }
                var totalpax = Number(this.selected_adults) + Number(this.selected_children) + Number(this.selected_infant);

                if (parseInt(totalpax) > 1) {
                    totalpax = totalpax + ' Passengers';
                } else {
                    totalpax = totalpax + ' Passenger'
                }
                this.Totaltravaller = totalpax;
            }
        },
        clickoutside: function () {
            this.triptype = 'O';
        },
        getmoreinfo(url) {
            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/Elitetravel/Holidaypackagedetails.html?page=" + url;
                    console.log(url);
                    sessionStorage.active_el = 4;
                    window.location.href = url;
                }
                else {
                    url = "#";
                }
            }
            else {
                url = "#";
            }
            return url;

        },
        dateConvert: function (utc) {
            // this.value=this.date
            return (moment(utc).format("DD-MMM-YYYY"));
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        closeInsurance: function () {
            if ($("#flightTab") != undefined) {
                $("#flightTab").click();
            }

        },
        swapLocations: function (id) {
            if ((this.CityFrom) && (this.CityTo)) {
                var from = this.CityFrom;
                var to = this.CityTo;
                this.CityFrom = to;
                this.CityTo = from;
                swpaloc(id)
            }
        },
        insureClaender: function () {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = generalInformation.systemSettings.calendarDisplay;
            $("#insdob").datepicker({
                maxDate: "0d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                changeYear: true,
                yearRange: '-100:+0'

            });
            $("#insdeparture").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    $("#insreturn").datepicker("option", "minDate", selectedDate);

                }
            });

            $("#insreturn").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: numberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#insdeparture").val();
                    $("#insreturn").datepicker("option", "minDate", selectedDate);
                },

            });
        },
        getAmount: function (amount) {
            amount = parseFloat(amount.replace(/[^\d\.]*/g, ''));
            return amount;
        },
        stopLoader: function () {
            var div = document.getElementById("preloader");
            // if (div.style.display !== "none") {  
            div.style.display = "none";
            // }  else {  
            //     div.style.display = "block";  
            // }
        }
    },
    updated: function () {
        this.setCalender();
        ///  this.slider();
    },
    created() {
        $('#preloader').delay(100).fadeOut(100);
    },
    mounted: function () {
        this.pageContent();
        this.flightDealsCard();
        this.holidayPackageCard();
        this.setCalender();
        this.insureClaender();

    },
    watch: {
        triptype: function () {
            if (this.triptype == "M") {
                this.travellerdisply = false;
            }
        }
    }
});

function setCarousel() {


    $("#holidaypackage").owlCarousel({
        items: 3,
        itemsCustom: false,
        itemsDesktop: [2000, 3],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: [600, 2],
        itemsMobile: [479, 1],
        singleItem: false,
        itemsScaleUp: false,
        slideSpeed: 1000,
        //Autoplay
        autoPlay: true,
        stopOnHover: true,

        // Navigation
        navigation: false,
        navigationText: ['<i class="fa  fa-angle-left"></i>', '<i class="fa  fa-angle-right"></i>'],
        rewindNav: true,
        scrollPerPage: false,

        //Pagination
        pagination: true,
        paginationNumbers: false,

        // Responsive 
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
    });
}


function getCabinName(cabinCode) {
    var cabinClass = 'Economy';
    if (cabinCode == "F") {
        cabinClass = "First Class";
    } else if (cabinCode == "C") {
        cabinClass = "Business";
    } else {
        try { cabinClass = getCabinClassObject(cabinCode).BasicClass; } catch (err) { }
    }
    return cabinClass;
}

function isNullorUndefined(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) { status = true; }
    return status;
}



$(function () {

    //  loadCountryPicker();
})
function swpaloc(id) {
    var from = $("#Cityfrom" + id).val();
    var to = $("#Cityto" + id).val();
    $("#Cityfrom" + id).val(to);
    $("#Cityto" + id).val(from);
}

function onCabinChange(e) {
    maininstance.selected_cabin = $("#cabinValue option:selected").val();
}