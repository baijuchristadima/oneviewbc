var myBookingObject = new Vue({
  name: "MyBookings",
  el: "#MyBookings",
  data: {
    isLoading: false,
    requestType: "0",
    amendForm: {
      name: "",
      email: "",
      phone: "",
      refno: "",
    },
    key: 0,
    language: localStorage.Language ? localStorage.Language : "en",
  },
  methods: {
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    stopLoader: function () {
      $("#preloader").delay(50).fadeOut(250);
    },
    amendBooking: function () {
      let self = this;
      let from = $("#from1").val();
      let to = $("#from2").val();
      let agencyCode = JSON.parse(localStorage.User).loginNode.code;

      let newFrom = from == "" ? "" : from;
      let newTo = to == "" ? "" : to;

      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = self.amendForm.email.match(emailPat);

      if (self.amendForm.name == undefined || self.amendForm.name == "") {
        alertify.alert("Error", "Please enter your name.");
        return;
      } else if (self.amendForm.email == undefined || self.amendForm.email == "") {
        alertify.alert("Error", "Please enter your email.");
        return;
      } else if (matchArray == null) {
        alertify.alert("Error", "Incorrect email format.").set("closable", false);
        return false;
      } else if (self.amendForm.phone == undefined || self.amendForm.phone == "") {
        alertify.alert("Error", "Please enter your contact number.");
        return;
      } else if (self.amendForm.refno == undefined || self.amendForm.refno == "") {
        alertify.alert("Error", "Please enter booking reference number.");
        return;
      } else if (newFrom == undefined || newFrom == "") {
        alertify.alert("Error", "Please enter new check in date.");
        return;
      } else if (newTo == undefined || newTo == "") {
        alertify.alert("Error", "Please enter new checkout date.");
        return;
      }
      self.isLoading = true;

      var sendingRequest = {
        type: "Amend Booking",
        keyword1: self.amendForm.name,
        keyword2: self.amendForm.email,
        keyword4: self.requestType == "0" ? "Change date" : "Cancel",
        keyword5: self.amendForm.phone,
        keyword6: self.amendForm.refno,
        date1: moment(newFrom, 'DD/MM/YYYY').format('YYYY-MM-DDT00:00:00'),
        date2: moment(newTo, 'DD/MM/YYYY').format('YYYY-MM-DDT00:00:00'),
        nodeCode: agencyCode,
      };
      self
        .cmsRequestData("POST", "cms/data", sendingRequest, null)
        .then(function (response) {
          if (response) {
            var user = JSON.parse(localStorage.User);
            var fromEmail = user.loginNode.parentEmailId ? user.loginNode.parentEmailId : (user.loginNode.email ? user.loginNode.email : "uaecrt@gmail.com") || "uaecrt@gmail.com";
            var toEmail = user.loginNode.parentEmailId;
            var ccEmails = _.filter(user.loginNode.emailList, function (o) {
              return o.emailTypeId == 3 && o.emailType.toLowerCase() == "support";
            });
            ccEmails = _.map(ccEmails, function (o) {
              return o.emailId;
            });
            self
              .getTemplate("WtadAdminEmail")
              .then(function (templateResponse) {
                var data = templateResponse.data.data;
                var emailTemplate = "";
                if (data.length > 0) {
                  for (var x = 0; x < data.length; x++) {
                    if (data[x].enabled == true && data[x].type == "WtadAdminEmail") {
                      emailTemplate = data[x].content;
                      break;
                    }
                  }
                }
       
                var emailDataAdmin = {
                  bookingReference: self.amendForm.refno,
                  fullName: self.amendForm.name,
                  emailAddress: self.amendForm.email,
                  contact: self.amendForm.phone,
                  hotelDetails:
                    "<ul><li>New Checkin Date:&nbsp;" +
                    moment(newFrom, 'DD/MM/YYYY').format('YYYY-MM-DD') +
                    "</li>" +
                    "<li>New Checkout Date:&nbsp;" +
                    moment(newTo, 'DD/MM/YYYY').format('YYYY-MM-DD') +
                    "</li></ul>",
                };
                var htmlGenerate = ServiceUrls.emailServices.htmlGenerate;
                var emailData = {
                  template: emailTemplate,
                  content: emailDataAdmin,
                };
                debugger
                axios.post(htmlGenerate, emailData).then(function (htmlResponse) {
                  var emailPostData = {
                    type: "AttachmentRequest",
                    toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                    fromEmail: fromEmail,
                    ccEmails: ccEmails,
                    bccEmails: null,
                    subject: "Amend booking - " + self.amendForm.refno,
                    attachmentPath: "",
                    html: htmlResponse.data.data,
                  };
                  var mailUrl = ServiceUrls.emailServices.emailApiWithAttachment;
                  sendMailServiceWithCallBack(mailUrl, emailPostData, function () {
                    alertify.alert("Success", "Amend booking submitted.");
                    self.amendForm.hselform = "";
                    self.amendForm.name = "";
                    self.amendForm.email = "";
                    self.amendForm.phone = "";
                    self.amendForm.refno = "";
                    $("#from1").val("");
                    $("#from2").val("");
                    self.isLoading = false;
                  });
                });
              })
              .catch(function (error) {
                console.log(error);
              });
        } else {
            alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M045")).set("closable", false);
          }
        })
        .catch(function (error) {
          self.isLoading = false;
          console.error(error);
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M045")).set("closable", false);
        });
    },
    cmsRequestData: async function (callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      return fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          "Content-Type": "application/json",
        },
        body: data, // body data type must match "Content-Type" header
      }).then(function (response) {
        return response.json();
      });
    },
    getTemplate(template) {
      // var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
      var url = ServiceUrls.emailServices.getTemplate + "AGY75/" + template;
      // var url = ServiceUrls.emailServices.getTemplate + LoggedUser.loginNode.code + "/" + template;
      return axios.get(url);
  },
  },
  mounted: function () {
    var vm = this;
    this.$nextTick(function () {
      var dateFormat = "dd/mm/yy";
      var dateFormat = "dd/mm/yy"
            $("#from1").datepicker({
                // minDate: "0d",
                // maxDate: "360d",
                minDate: new Date('05/23/2022'),
                maxDate: new Date('05/30/2022'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
            });

            $("#from2").datepicker({
                minDate: new Date('05/23/2022'),
                maxDate: new Date('05/30/2022'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
                beforeShow: function (event, ui) {
                    try {
                        var selectedDate = $('#from1').datepicker('getDate', '+1d');
                        selectedDate.setDate(selectedDate.getDate() + 1);
                        $("#from2").datepicker("option", "minDate", selectedDate);
                    } catch (error) {
                            
                    }
                }
            });
      vm.stopLoader();
      sessionStorage.active_e = 20;
    });
  },
});
