var currentUser = getUser();
const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var packageView = new Vue({
    i18n,
    el: '#Hoteldetails',
    name: 'Hoteldetails',
    data: {
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'AED',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        isLoading: false,
        maincontent: {
            Hotel_Name: '',
        },
        details: {
            Informations: '',
            Images: [{
                Image: ''
            }],
            Amenities: '',
        },
        Bookingdetails: {
            Instructions: '',
        },
        Roomdetails: {
            Instructions: '',
        },
        pageURLLink: '',

        email: '',
        phone: '',
        indexRoom: null,
        city: '',
        checkIn: '',
        checkOut: '',
        noOfNights: 0,
        norooms: 0,
        withPark: false,
        message: "",
        rooms: [],
        SelectedRooms: [{
            Price: ''
        }],
        mainData:[],
        headerArea:[],
        paxDetails: [],
        typeList: [{
                typeId: 1,
                name: 'SingleRoom',
                adult: 1,
                child: 0,
                pax: [{
                    paxId: 1,
                    title: 'Mr',
                    name: '',
                    surName: '',
                    type: 'Adult',
                    // group: "Elite",
                    requested: true
                }]
            },
            {
                typeId: 2,
                name: 'DoubleRoom',
                adult: 2,
                child: 0,
                pax: [{
                        paxId: 1,
                        title: 'Mr',
                        name: '',
                        surName: '',
                        type: 'Adult',
                        // group: "Elite",
                        requested: true
                    },
                    {
                        paxId: 2,
                        title: 'Mr',
                        name: '',
                        surName: '',
                        type: 'Adult',
                        // group: "Elite",
                        requested: true
                    }
                ]
            },
            {
                typeId: 3,
                name: 'DoublePlusChild',
                adult: 2,
                child: 1,
                pax: [{
                        paxId: 1,
                        title: 'Mr',
                        name: '',
                        surName: '',
                        type: 'Adult',
                        // group: "Elite",
                        requested: true
                    },
                    {
                        paxId: 2,
                        title: 'Mr',
                        name: '',
                        surName: '',
                        type: 'Adult',
                        // group: "Elite",
                        requested: true
                    },
                    {
                        paxId: 3,
                        title: 'Mstr',
                        name: '',
                        surName: '',
                        type: 'Child',
                        // group: "Elite",
                        requested: true
                    }
                ]
            },
            {
                typeId: 4,
                name: 'DoublePlusAdult',
                adult: 3,
                child: 0,
                pax: [{
                        paxId: 1,
                        title: 'Mr',
                        name: '',
                        surName: '',
                        type: 'Adult',
                        // group: "Elite",
                        requested: true
                    },
                    {
                        paxId: 2,
                        title: 'Mr',
                        name: '',
                        surName: '',
                        type: 'Adult',
                        // group: "Elite",
                        requested: true
                    },
                    {
                        paxId: 3,
                        title: 'Mr',
                        name: '',
                        surName: '',
                        type: 'Adult',
                        // group: "Elite",
                        requested: true
                    }
                ]
            }
        ],
        bookType: 1
    },
    mounted() {
        // localStorage.removeItem("backUrl");
        sessionStorage.active_e = 2;
        var self = this;
        // this.setCalender();
        this.getPackagedetails();
        self.getPagedata()
        // var urlParameters = this.getSearchData();
        // var obj = JSON.stringify(this.typeList.find((x) => x.typeId == Number(urlParameters.occupancy)))
        // this.roomTypePax = JSON.parse(obj).pax;
        // this.maxAdults = JSON.parse(obj).adult;
        // this.maxChild = JSON.parse(obj).child;

        this.$nextTick(function () {
            setTimeout(() => {
                $("#phone").intlTelInput({
                    initialCountry: currentUser.loginNode.country.code || "AE",
                    geoIpLookup: function (callback) {
                        $.get('https://ipinfo.io', function () {}, "jsonp").always(function (resp) {
                            var countryCode = (resp && resp.country) ? resp.country : "";
                            (countryCode);
                        });
                    },
                    separateDialCode: true,
                    autoPlaceholder: "off",
                    preferredCountries: ['ae', 'sa', 'om', 'qa'],
                    utilsScript: "../assets/js/intlTelInput.js?v=201903150000" // just for formatting/placeholders etc
                });
            }, 500);
        });

        // var vm = this;
        // this.$nextTick(function () {
        //     $('#roomno').on("change", function (e) {
        //         vm.dropdownChange(e, 'roomno')
        //         setTimeout(() => {
        //             $(".myselect").select2({
        //                 minimumResultsForSearch: Infinity
        //             });
        //         }, );
        //         var roomNo = Number(e.target.value);
        //         setTimeout(() => {
        //             for (let index = 0; index < roomNo; index++) {
        //                 $('#Adult-' + index).val('1');
        //                 $('#Adult-' + index).trigger('change');
        //             }
        //         }, );

        //     });
        //     $('#from2').on("change", function (e) {
        //         self.noOfnightsFind();
        //     });
        //     $(document.body).on("change", ".myselect", function (e) {
        //         var tags = e.target.id.split('-');
        //         console.log(tags);
        //         if (tags[0] == "Adult") {
        //             self.setAdults(tags[1], Number(e.target.value));
        //         } else if (tags[0] == "Child") {
        //             self.setChild(tags[1], Number(e.target.value));
        //         }
        //     });
        // })

    },
    computed: {
        total: function () {
            var self = this;
            var a = _.reduce(self.SelectedRooms, function (sum, n) {
                return sum + (Number(n.Price) * Number(self.noOfNights));
            }, 0);

            if ((moment('2021-12-02').isBetween(moment(self.checkIn, "DD/MM/YYYY"), moment(self.checkOut, "DD/MM/YYYY")) 
            || moment('2021-12-02').isSame(moment(self.checkIn, "DD/MM/YYYY")) 
            || moment('2021-12-02').isSame(moment(self.checkOut, "DD/MM/YYYY"))) && self.maincontent.Hotel_ID != "HOTEL2") {
                a = parseFloat(a) + 27.17;
            }

            return a;
        },
        isSupplement: function() {
            var a = false;
            var self = this;
            if ((moment('2021-12-02').isBetween(moment(self.checkIn, "DD/MM/YYYY"), moment(self.checkOut, "DD/MM/YYYY")) 
            || moment('2021-12-02').isSame(moment(self.checkIn, "DD/MM/YYYY")) 
            || moment('2021-12-02').isSame(moment(self.checkOut, "DD/MM/YYYY"))) && self.maincontent.Hotel_ID != "HOTEL2") {
                a = true;
            }
            return a;
        }
    },
    methods: {
        // noOfnightsFind: function () {
        //     let date1 = document.getElementById("from1").value;
        //     let date2 = document.getElementById("from2").value;
        //     if (date1 && date2) {
        //         var start = moment(date1, "DD/MM/YYYY");
        //         var end = moment(date2, "DD/MM/YYYY");
        //         var nights = end.diff(start, 'days')
        //         this.noOfNights = nights;
        //     }
        // },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        dateFormat: function (d) {
            return moment(d).format('DD-MMM-YYYY');
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPackagedetails: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                try {
                    var hotelurl = getQueryStringValue('page');
                    var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                    if (hotelurl != "") {
                        hotelurl = hotelurl.split('-').join(' ');
                        hotelurl = hotelurl.split('_')[0];
                        var tohotelurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + hotelurl + '.ftl';
                        self.pageURLLink = hotelurl;
                        axios.get(tohotelurl, {
                            headers: {
                                'content-type': 'text/html',
                                'Accept': 'text/html',
                                'Accept-Language': langauage
                            }
                        }).then(function (response) {

                            // var fromDate = searchData.cin.trim();
                            // var toDate = searchData.cout.trim();
                            // var roomType = searchData.roomType;
                            // var Occupancy_ID = searchData.occupancy;
                            self.city = getQueryStringValue('city');
                            self.checkIn = getQueryStringValue('cin');
                            self.checkOut = getQueryStringValue('cout');
                            self.norooms = getQueryStringValue('rooms');
                            self.noOfNights = getQueryStringValue('nights');
                            // self.withPark = new Boolean(getQueryStringValue('parking'))
                            self.withPark = getQueryStringValue('parking');
                            self.rooms = JSON.parse(getQueryStringValue('roomTypes'));
                            // self.indexRoom = JSON.parse(getQueryStringValue('ind'));

                            // var searchCriteria = Object.assign({}, searchData);
                            // localStorage.setItem("backUrl", self.yourCallBackFunction(searchCriteria));
                            // $("#from1").val(fromDate);
                            // $("#from2").val(toDate);
                            // var start = moment(fromDate, "DD/MM/YYYY");
                            // var end = moment(toDate, "DD/MM/YYYY");
                            // var nights = end.diff(start, 'days')
                            // self.noOfNights = nights;

                            var maincontent = self.pluck('Hotel_Details', response.data.area_List);
                            if (maincontent.length > 0) {
                                self.maincontent = self.getAllMapData(maincontent[0].component);
                                // self.maincontent.Hotel_Name = self.pluckcom('Hotel_Name', maincontent[0].component);
                                // self.maincontent.Rate = self.pluckcom('Rate', maincontent[0].component);
                                // self.checkPrice();
                            }
                            var details = self.pluck('Hotel_Details_Section', response.data.area_List);
                            if (details.length > 0) {
                                self.details = self.getAllMapData(details[0].component);
                                // self.details.Informations = self.pluckcom('Informations', details[0].component);
                                // self.details.Hotel_Image_Banner = self.pluckcom('Hotel_Image_Banner', details[0].component);
                                // self.details.Amenities = self.pluckcom('Amenities', details[0].component);
                            }
                            var Bookingdetails = self.pluck('Booking_Details_Section', response.data.area_List);
                            if (Bookingdetails.length > 0) {
                                self.Bookingdetails = self.getAllMapData(Bookingdetails[0].component);
                                // self.Bookingdetails.inDate = self.pluckcom('Main_Check_In_Date', Bookingdetails[0].component);
                                // self.Bookingdetails.outDate = self.pluckcom('Main_Check_Out_Date', Bookingdetails[0].component);
                                // self.Bookingdetails.nights = self.pluckcom('Number_of_Nights', Bookingdetails[0].component);
                                // self.Bookingdetails.instructions = self.pluckcom('Instructions', Bookingdetails[0].component);

                            }
                            var Roomdetails = self.pluck('Room_Details_Section', response.data.area_List);
                            if (Roomdetails.length > 0) {
                                self.Roomdetails.Instructions = self.pluckcom('Instructions', Roomdetails[0].component);
                                self.instructions = self.getAllMapData(Roomdetails[0].component)
                                var RoomList = self.pluckcom('Room_Details', Roomdetails[0].component);

                                // self.Roomdetails.Room_Details =
                                //     RoomList.filter(element => {
                                //         return element.Occupancy_ID == Occupancy_ID
                                //     });


                                // self.withPark 
                                // self.rooms
                                // self.typeList
                                // self.paxDetails
                                var hotelRooms = [];
                                try {
                                    self.rooms.forEach(room => {
                                        var roomSelected = RoomList.filter(element => ((room == element.Occupancy_ID)));
                                        // if (self.withPark == 'true') {
                                        //     roomSelected = roomSelected.find(element => (element.Including_Park_Access));
                                        // } else if (self.withPark == 'false') {
                                        //     roomSelected = roomSelected.find(element => (!element.Including_Park_Access));
                                        // }
                                        // hotelRooms.push(roomSelected[self.indexRoom]);
                                        hotelRooms.push(roomSelected[0]);
                                    });

                                    self.SelectedRooms = hotelRooms;
                                } catch (error) {
                                    console.log(error);
                                }
                                console.log(hotelRooms);

                                var paxDetails = [];
                                self.rooms.forEach(room => {
                                    try {
                                        var roomSelected = self.typeList.find(element => (Number(room) == Number(element.typeId)));
                                        roomSelected = JSON.stringify(roomSelected.pax);
                                        paxDetails.push(JSON.parse(roomSelected));
                                    } catch (error) {
                                        console.log(error);
                                    }
                                });
                                self.paxDetails = paxDetails;

                                setTimeout(() => {
                                    $('.fotorama').fotorama();
                                }, 100);
                            }

                        })
                    }
                } catch (error) {
                    console.log(error);
                }
            });
        },
        getPagedata(){
            var self=this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Hotel Details Page/Hotel Details Page/Hotel Details Page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                }).then(function (response) {
                                    var headerSection = pluck('Banner_Section', response.data.area_List);
                    self.headerArea = getAllMapData(headerSection[0].component);
                      var Main = pluck('Main_Section', response.data.area_List);
                    self.mainData = getAllMapData(Main[0].component);
                    self.getPackagedetails();
                        }).catch(function (error) {
                    console.log('Error', error);
                });
      
            });
        },
        // yourCallBackFunction: function (data) {
        //     if (data != null) {
        //         if (data != "") {
        //             var customSearch =
        //                 "&city=" + data.city +
        //                 "&cin=" + data.cin +
        //                 "&cout=" + data.cout +
        //                 "&occupancy=" + data.occupancy + "&";


        //             url = "/TDRA/hotel-results.html?page=" + customSearch;
        //         } else {
        //             url = "#";
        //         }
        //     } else {
        //         url = "#";
        //     }
        //     return url;
        // },
        // setAdults: function (room, count) {
        //     var self = this;
        //     var adultList = self.paxDetails[room].filter(element => (element.type == "Adult"));
        //     var flag = 0;
        //     self.paxDetails[room].forEach((obj) => {
        //         if (flag < count && obj.paxId == adultList[flag].paxId) {
        //             obj.requested = true;
        //             flag++
        //         } else if (obj.type == "Adult") {
        //             obj.requested = false;
        //         }
        //     })
        // },
        // setChild: function (room, count) {
        //     var self = this;
        //     var childList = self.paxDetails[room].filter(element => (element.type == "Child"));
        //     var flag = 0;
        //     self.paxDetails[room].forEach((obj) => {
        //         if (flag < count && obj.paxId == childList[flag].paxId) {
        //             obj.requested = true;
        //             flag++
        //         } else if (obj.type == "Child") {
        //             obj.requested = false;
        //         }
        //     })
        // },
        // addRomms: function (old, rooms) {
        //     var self = this;
        //     if (old > rooms) {
        //         self.paxDetails = self.paxDetails.slice(0, rooms);
        //     } else {
        //         var diff = rooms - old;
        //         var obj = JSON.stringify(self.roomTypePax);
        //         for (let index = 0; index < diff; index++) {
        //             // self.paxDetails.push({
        //             //     ...self.roomTypePax
        //             // });
        //             self.paxDetails.push(JSON.parse(obj));
        //         }
        //     }
        // },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                if (type == 'roomno') {
                    var val = Number(event.target.value);
                    this.addRomms(this.norooms, val);
                    this.norooms = val;
                }
            }
        },
        checkReservation: function () {
            // var self = this;
            // let dateObj1 = document.getElementById("from1");
            // let dateValue1 = dateObj1.value;
            // let dateObj2 = document.getElementById("from2");
            // let dateValue2 = dateObj2.value;
            // let room = document.getElementById("roomno");
            // let roomValue = room.value;
            // // let yourPrice = roomValue*
            // if (dateValue1 == undefined || dateValue1 == '') {
            //     alertify.alert('Alert', 'Select Check In date').set('closable', false);
            //     return false;
            // }
            // if (dateValue2 == undefined || dateValue2 == '') {
            //     alertify.alert('Alert', 'Select Check Out Date').set('closable', false);
            //     return false;
            // }
            // if (roomValue == undefined || roomValue == '') {
            //     alertify.alert('Alert', 'Please Select Rooms.').set('closable', false);
            //     return false;
            // }
            // if (dateValue1 == dateValue2) {
            //     alertify.alert('Alert', 'Check-In date and Check-Out date are same.').set('closable', false);
            //     return false;
            // } else {
            this.$nextTick(function () {
                $('html, body').animate({
                    scrollTop: $("#htlbooking").offset().top
                }, 1000);
            })
            // }
        },
        bookHotel: function () {
            var self = this;
            // let dateObj1 = document.getElementById("from1");
            let dateValue1 = self.checkIn;
            // let dateObj2 = document.getElementById("from2");
            let dateValue2 = self.checkOut;
            // let room = document.getElementById("roomno");
            let roomValue = self.norooms;

            var flagPaxName = false
            self.paxDetails.forEach(room => {
                room.forEach(pax => {
                    if (pax.requested && (pax.name == '' || pax.surName == '')) {
                        flagPaxName = true;
                        return false;
                    }
                });
                if (flagPaxName) {
                    return false;
                }
            });
            if (flagPaxName) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M008')).set('closable', false);
                return false;
            }
            if (!self.email) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M009')).set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = self.email.match(emailPat);
            if (matchArray == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M019')).set('closable', false);
                return false;
            }
            if (!self.phone || self.phone == '') {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M010')).set('closable', false);
                return false;
            }
            if (self.phone.length < 8) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M046')).set('closable', false);
                return false;
            } else {
                self.isLoading = true;
                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                var guestNames = '';
                var guestCount = 0;
                self.paxDetails.forEach((room, index) => {
                    var roomSelect = self.SelectedRooms[index].Room_Type
                    guestNames += ('\n \n' + 'Room - ' + (index + 1) + ' - ' + roomSelect);
                    room.forEach(pax => {
                        if (pax.name) {
                            var surname = pax.surName ? pax.surName : '';
                            guestNames += (' \n' + pax.title  + ' ' + pax.name + ' ' + surname + ' - ' + pax.type);
                        }
                        if (pax.requested) {
                            guestCount++;
                        }
                    });
                });
                console.log(guestCount);
                var leadPax = self.paxDetails[0][0].title + ' ' + self.paxDetails[0][0].name + ' ' + self.paxDetails[0][0].surName;
                // var referenceNumber = agencyCode + '-HTL' + moment(new Date()).format("YYMMDDhhmmssSSS");
                var referenceNumber = agencyCode + '-';
                // dateValue1 = moment(dateValue1, 'DD/MM/YYYY').format('YYYY-MM-DDThh:mm:ss');
                dateValue1 = moment(dateValue1, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
                dateValue2 = moment(dateValue2, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
                var roomTypeString = "";
                if (self.SelectedRooms[0].Bed_and_Breakfast) {
                    roomTypeString = "Bed and Breakfast";
                } else if (self.SelectedRooms[0].Half_Board) {
                    roomTypeString = "Half Board";
                } else {
                    roomTypeString = "Full Board";
                }
                // var roomType = self.withPark == 'true' ? 'Stay Including Park Access' : 'Only Stay' || 'Only Stay';
                var roomType = roomTypeString;
                var phoneNumber = $('.selected-dial-code').text() + self.phone;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                var sendingRequest = {
                    type: "Hotel Booking",
                    keyword1: leadPax.trim(),
                    keyword2: self.email,
                    keyword3: roomValue,
                    keyword4: self.maincontent.Hotel_Name,
                    keyword5: guestCount, //totalPax
                    keyword6: phoneNumber,
                    // keyword10: referenceNumber,
                    date1: dateValue1,
                    date2: dateValue2,
                    amount1: self.total,
                    text2: guestNames.trim(),
                    text3: roomType.trim(),
                    text4: self.message,
                    text10: JSON.stringify(self.paxDetails),
                    date3:requestedDate,
                    // keyword18: self.maincontent.Hotel_ID,
                    nodeCode: agencyCode
                };
                self.cmsRequestData("POST", "cms/data", sendingRequest, null).then(function (response) {
                    let insertID = Number(response);
                    var finalConfirmation = {
                        hotelName: self.maincontent.Hotel_Name,
                        packegeUrl: self.pageURLLink,
                        personName: leadPax,
                        emailAddress: self.email,
                        contact: phoneNumber,
                        checkIn: dateValue1,
                        checkOut: dateValue2,
                        adults: self.countGuest,
                        referenceNumber: referenceNumber + insertID,
                        cmsId: insertID,
                        amount: self.total,
                        HotelId: self.maincontent.Hotel_ID,
                    }
                    var updateDate = {
                        keyword10: referenceNumber + insertID
                    }
                    self.cmsFormUpdateData(updateDate, insertID).then(function (update) {
                        if (update.status == 200) {
                            window.sessionStorage.setItem("HotelDetails", JSON.stringify(finalConfirmation));
                            window.sessionStorage.setItem("SelectedRooms", JSON.stringify(self.SelectedRooms));
                            window.sessionStorage.setItem("emailStatusHtl", false);
                            if (referenceNumber && insertID) {
                                if (self.bookType == 1) {
                                    self.paymentFunction((referenceNumber + insertID));
                                } else {
                                    window.sessionStorage.setItem("pType", true);
                                    window.location.href = window.location.origin + "/World-Sailing/hotelconfirmation.html"
                                }
                            }
                            console.log('Success:' + update.status);
                        } else {
                            alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M045')).set('closable', false);
                        }
                    }).catch(function (updateError) {
                        self.isLoading = false;
                        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M045')).set('closable', false);
                        console.error(updateError);
                    });
                }).catch(function (error) {
                    self.isLoading = false;
                    console.error(error);
                    alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M045')).set('closable', false);
                });
            }
        },
        paymentFunction: function (RefNumber) {
            try {
                var paymentDetails = {
                    "totalAmount": -188.88,
                    "customerEmail": "test@test.com",
                    "bookingReference": "",
                    "cartID": ""
                };
                var user = JSON.parse(localStorage.User);
                var returnURL = window.location.origin + "/World-Sailing/hotelconfirmation.html"
                paymentDetails.bookingReference = RefNumber;
                // paymentDetails.totalAmount = this.$n((this.total / this.CurrencyMultiplier), 'currency', this.selectedCurrency).replace(/[^\d.-]/g, "");
                paymentDetails.totalAmount = this.total;
                paymentDetails.cartID = RefNumber;
                paymentDetails.currency = user.loginNode.currency || 'AED';
                paymentDetails.currentPayGateways = user.loginNode.paymentGateways;
                paymentDetails.customerEmail = this.email;
                var awsApi = true
                var paymentForm = paymentManager(paymentDetails, returnURL, window, awsApi);
                return paymentForm;

            } catch (error) {

            }
        },
        // getSearchData: function () {
        //     var urldata = this.getUrlVars();
        //     return urldata;
        // },
        cmsRequestData: async function (callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            return fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json'
                },
                body: data, // body data type must match "Content-Type" header
            }).then(function (response) {
                return response.json();
            });
        },
        cmsFormUpdateData: async function (data, id) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/cms/data/" + id;
            var callMethod = "PUT";
            if (data != null) {
                data = JSON.stringify(data);
            }
            try {
                let formPostRes = await axios({
                    url: url,
                    method: callMethod,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    data: data
                })
                return formPostRes;
            } catch (error) {
                console.log(error);
                return error.response.data.code;
            }
        },
        // dateChecker: function () {
        //     let dateObj1 = document.getElementById("from1");
        //     let dateValue1 = dateObj1.value;
        //     let dateObj2 = document.getElementById("from2");
        //     let dateValue2 = dateObj2.value;
        //     if (dateValue2 !== undefined || dateValue2 !== '') {
        //         // document.getElementById("to1").value = '';
        //         $('#from2').val('').trigger('change');
        //         $("#from2").val("");
        //         return false;
        //     }
        // },
        // setCalender() {
        //     var dateFormat = "dd/mm/yy"
        //     $("#from1").datepicker({
        //         // minDate: "0d",
        //         // maxDate: "360d",
        //         minDate: new Date('11/20/2021'),
        //         maxDate: new Date('12/08/2021'),
        //         numberOfMonths: 1,
        //         changeMonth: true,
        //         showOn: "both",
        //         buttonText: "<i class='fa fa-calendar'></i>",
        //         duration: "fast",
        //         showAnim: "slide",
        //         showOptions: {
        //             direction: "up"
        //         },
        //         showButtonPanel: false,
        //         dateFormat: dateFormat,
        //     });

        //     $("#from2").datepicker({
        //         minDate: new Date('11/20/2021'),
        //         maxDate: new Date('12/08/2021'),
        //         numberOfMonths: 1,
        //         changeMonth: true,
        //         showOn: "both",
        //         buttonText: "<i class='fa fa-calendar'></i>",
        //         duration: "fast",
        //         showAnim: "slide",
        //         showOptions: {
        //             direction: "up"
        //         },
        //         showButtonPanel: false,
        //         dateFormat: dateFormat,
        //         beforeShow: function (event, ui) {
        //             var selectedDate = $("#from1").val();
        //             $("#from2").datepicker("option", "minDate", selectedDate);
        //         }
        //     });
        // }
    }
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
jQuery(document).ready(function ($) {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
});

function getUser() {
    if (localStorage.IsLogin) {
        var user = JSON.parse(localStorage.User);
        return user;
    }
    return null;
}