const i18n = new VueI18n({
  numberFormats,
  locale: "en", // set locale
  // set locale messages
});
var visa = new Vue({
  i18n,
  el: "#planmytrip",
  name: "planmytrip",
  data: {
    selectedCurrency: localStorage.selectedCurrency ? localStorage.selectedCurrency : "USD",
    CurrencyMultiplier: localStorage.CurrencyMultiplier ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    countryList: countryList,
    hasFlight: false,
    hasAccomodation: false,
    hasHotel: false,
    hasTent: false,
    hasCarRental: false,
    hasTransfers: false,
    hasUAETour: false,
    hasVisa: false,
    arrAirport: "",
    depAirport: "",
    title: "Mr",
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    countryOfResidence: "",
    message: "",
    flightDepDate: "",
    flightArrDate: "",
    tentDateIn: "",
    tentDateOut: "",
    hotelDateIn: "",
    hotelDateOut: "",
    carRentalDateIn: "",
    carRentalDateOut: "",
    carRentalPickUp: "",
    carRentalReturn: "",
    transferDateIn: "",
    transferDateOut: "",
    transferPickUp: "",
    transferDropOff: "",
    UAETourDesc: "",
    visaNationNality: "",
    child: "",
    adult: "",
    infant: "",
    hasTravelInsurance: true,
    cabinValue: "0",
    isBooking: false,
    LabelSection: {},
  },
  methods: {
    getPagecontent: function () {
      var self = this;

      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = localStorage.Languagecode ? localStorage.Languagecode : "en";
        // self.dir = langauage == "ar" ? "rtl" : "ltr";
        var pageurl = huburl + portno + "/persons/source?path=/B2B/AdminPanel/CMS/" + Agencycode + "/Template/Plan My Trip/Plan My Trip/Plan My Trip.ftl";

        axios
          .get(pageurl, {
            headers: {
              "content-type": "text/html",
              Accept: "text/html",
              "Accept-Language": langauage,
            },
          })
          .then(function (response) {
            if (response.data.area_List.length > 0) {
              var label = pluck("Page_Labels", response.data.area_List);
              self.LabelSection = getAllMapData(label[0].component);
              self.isLoading = false;
            }
            self.stopLoader();

          })
          .catch(function (error) {
            console.log("Error");
            self.stopLoader();

            self.aboutUsContent = [];
          });
      });
    },
    sendPlanMyTrip: function () {
      var self = this;
      if (this.hasFlight) {
        if (!this.flightDepDate) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M003")).set("closable", false);
          return false;
        }
        if (!this.depAirport) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M002")).set("closable", false);
          return false;
        }
        if (!this.flightArrDate) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M004")).set("closable", false);
          return false;
        }
        if (!this.arrAirport) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M001")).set("closable", false);
          return false;
        }
      }
      if (this.hasAccomodation && this.hasHotel) {
        if (!this.hotelDateIn) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M006")).set("closable", false);
          return false;
        }
        if (!this.hotelDateOut) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M007")).set("closable", false);
          return false;
        }
      }
      if (this.hasAccomodation && (!this.hasHotel&&!this.hasTent)) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M059")).set("closable", false);
          return false;
      }
      if (this.hasCarRental) {
        if (!this.carRentalDateIn) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M049")).set("closable", false);
          return false;
        }
        if (!this.carRentalPickUp) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M013")).set("closable", false);
          return false;
        }
        if (!this.carRentalDateOut) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M049")).set("closable", false);
          return false;
        }
        if (!this.carRentalReturn) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M014")).set("closable", false);
          return false;
        }
      }
      if (this.hasTransfers) {
        if (!this.transferDateIn) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M012")).set("closable", false);
          return false;
        }
        if (!this.transferPickUp) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M013")).set("closable", false);
          return false;
        }
        // if (!this.transferDateOut) {
        //   alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M012")).set("closable", false);
        //   return false;
        // }
        if (!this.transferDropOff) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M014")).set("closable", false);
          return false;
        }
      }
      if (this.hasUAETour) {
        if (!this.UAETourDesc) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M056")).set("closable", false);
          return false;
        }
      }
      if (this.hasVisa) {
        if (!this.visaNationNality) {
          alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M057")).set("closable", false);
          return false;
        }
      }
      if (!this.hasFlight && !this.hasAccomodation && !this.hasCarRental && !this.hasTransfers && 
        !this.hasUAETour && !this.hasVisa) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M058")).set("closable", false);
        return false;
      }
      if (!this.adult) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M031")).set("closable", false);
        return false;
      }
      if (!this.firstName || !this.lastName) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M032")).set("closable", false);
        return false;
      }
      if (this.countryOfResidence == "") {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M055")).set("closable", false);
        return false;
      }
      if (this.email == null) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M009")).set("closable", false);
        return false;
      }
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.email.match(emailPat);
      if (matchArray == null) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M019")).set("closable", false);
        return false;
      }
      if (this.phone == null) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M010")).set("closable", false);
        return false;
      }
      var phoneno = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
      var matchphone = this.phone.match(phoneno);
      if (matchphone == null) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M046")).set("closable", false);
        return false;
      }
      if (this.phone.length < 8) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M046")).set("closable", false);
        return false;
        // if (!this.message) {
        //   alertify.alert('Alert', 'Message required.').set('closable', false);
        //   return false;
        // }
      } else {
        this.isBooking = true;
        var user = JSON.parse(localStorage.User);
        var fromEmail = user.loginNode.parentEmailId ? user.loginNode.parentEmailId : (user.loginNode.email ? user.loginNode.email : "uaecrt@gmail.com") || "uaecrt@gmail.com";
        var toEmail = user.loginNode.parentEmailId;
        // var toEmail = JSON.parse(localStorage.User).emailId;
        var ccEmails = _.filter(user.loginNode.emailList, function (o) {
          return o.emailTypeId == 3 && o.emailType.toLowerCase() == "support";
        });
        ccEmails = _.map(ccEmails, function (o) {
          return o.emailId;
        });
        var agency = localStorage.getItem("AgencyCode");
        // var referenceNumber = agency + '-VIS' + moment(new Date()).format("YYMMDDhhmmssSSS");
        var referenceNumber = agency + "-";

        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        let requestedDate = moment(String(new Date())).format("YYYY-MM-DDThh:mm:ss");
        var sendingRequest = {
          type: "Plan My Trip",
          text1: this.hasFlight ? moment(this.flightDepDate, "DD/MM/YYYY").format("YYYY-MM-DD") : "N/A",
          text2: this.hasFlight ? moment(this.flightArrDate, "DD/MM/YYYY").format("YYYY-MM-DD") : "N/A",
          text3: this.hasFlight ? this.depAirport : "N/A",
          text4: this.hasFlight ? this.arrAirport : "N/A",
          text5: this.hasHotel ? moment(this.hotelDateIn, "DD/MM/YYYY").format("YYYY-MM-DD") : "N/A",
          text6: this.hasHotel ? moment(this.hotelDateOut, "DD/MM/YYYY").format("YYYY-MM-DD") : "N/A",
          text7: this.hasTent ? moment(this.tentDateIn, "DD/MM/YYYY").format("YYYY-MM-DD") : "N/A",
          text8: this.hasTent ? moment(this.tentDateOut, "DD/MM/YYYY").format("YYYY-MM-DD") : "N/A",
          text9: this.hasCarRental ? moment(this.carRentalDateIn, "DD/MM/YYYY").format("YYYY-MM-DD") : "N/A",
          text10: this.hasCarRental ? moment(this.carRentalDateOut, "DD/MM/YYYY").format("YYYY-MM-DD") : "N/A",
          text11: this.hasCarRental ? this.carRentalPickUp : "N/A",
          text12: this.hasCarRental ? this.carRentalReturn : "N/A",
          text13: this.hasTransfers ? moment(this.transferDateIn, "DD/MM/YYYY").format("YYYY-MM-DD") : "N/A",
          // text14: this.hasTransfers ? moment(this.transferDateOut, "DD/MM/YYYY").format("YYYY-MM-DD") : "N/A",
          text14: "N/A",
          text15: this.hasTransfers ? this.transferPickUp : "N/A",
          text16: this.hasTransfers ? this.transferDropOff : "N/A",
          text17: this.hasUAETour ? this.UAETourDesc : "N/A",
          text18: this.hasVisa ? this.visaNationNality : "N/A",
          text19: this.adult.toString() || "N/A",
          text20: this.child.toString() || "N/A",
          text21: this.infant.toString() || "N/A",
          text22: this.title || "N/A",
          text23: this.firstName,
          text24: this.lastName,
          text25: this.email,
          text26: $(".selected-dial-code").text() + this.phone,
          text27: this.countryOfResidence,
          text28: this.message,
          text29: requestedDate,
          text30: this.hasTravelInsurance ? "YES" : "NO",
          text31: this.hasFlight ? ["Economy", "Business", "First Class"].filter(function(e,i){return self.cabinValue == i;})[0] : "N/A",
          nodeCode: agencyCode,
        };
        this.cmsRequestData("POST", "cms/data", sendingRequest, null)
          .then(function (response) {
            let insertID = Number(response);
            referenceNumber = referenceNumber + insertID;
            var emailApi = ServiceUrls.emailServices.emailApi;
            var updateDate = {
              keyword15: referenceNumber,
            };
            self
              .cmsFormUpdateData(updateDate, insertID)
              .then(function (update) {
                if (update.status == 200) {
                  console.log("Success:" + update.status);
                  var traveller = "";
                  if (self.adult.toString()) {
                    traveller = self.adult.toString() + " Adult";
                  }
                  if (self.child.toString()) {
                    traveller = traveller + ", " + self.child.toString() + " Child";
                  } 
                  if(self.infant.toString()) {
                    traveller = traveller + ", " + self.infant.toString() + " Infant";
                  }
                  var emailDataAdmin = {
                    bookingReference: referenceNumber,
                    fullName: self.title + " " + self.firstName + " " + self.lastName,
                    emailAddress: self.email,
                    contact: $(".selected-dial-code").text() + self.phone,
                    countryOfResidence: self.countryOfResidence,
                    travellers: traveller,
                    flightDetails: self.hasFlight ? "Departure: " + moment(self.flightDepDate, "DD/MM/YYYY").format("YYYY-MM-DD") + " " + 
                    self.depAirport +  ", Arrival: " + moment(self.flightArrDate, "DD/MM/YYYY").format("YYYY-MM-DD") + 
                      " " + self.arrAirport + ", " + ["Economy", "Business", "First Class"].filter(function(e,i){return self.cabinValue == i;})[0]: "N/A",
                    hotelDetails: self.hasHotel ? "Check In: " + moment(self.hotelDateIn, "DD/MM/YYYY").format("YYYY-MM-DD") + 
                      ", Check Out: " + moment(self.hotelDateOut, "DD/MM/YYYY").format("YYYY-MM-DD"): "N/A",
                    campDetails: self.hasTent ? "Check In: " + moment(self.tentDateIn, "DD/MM/YYYY").format("YYYY-MM-DD") + 
                      ", Check Out: " + moment(self.tentDateOut, "DD/MM/YYYY").format("YYYY-MM-DD"): "N/A",
                    carRentalDetails: self.hasCarRental ? "Pick Up: " + moment(self.carRentalDateIn, "DD/MM/YYYY").format("YYYY-MM-DD") + " " + 
                      self.carRentalPickUp +  ", Return: " + moment(self.carRentalDateOut, "DD/MM/YYYY").format("YYYY-MM-DD") + 
                      " " + self.carRentalReturn: "N/A",
                    transferDetails: self.hasTransfers ? "Pick Up: " + moment(self.transferDateIn, "DD/MM/YYYY").format("YYYY-MM-DD") + " " + 
                      self.transferPickUp +  ", Drop off: " + self.transferDropOff: "N/A",
                    tourDetails: self.hasUAETour ? self.UAETourDesc: "N/A",
                    visaDetails: self.hasVisa ? "Nationality: " + self.visaNationNality: "N/A",
                    message: self.message || "N/A",
                  }

                  self.getTemplate("SpartanAdminEmail").then(function (templateResponse) {
                    var data = templateResponse.data.data;
                    var emailTemplate = "";
                    if (data.length > 0) {
                      for (var x = 0; x < data.length; x++) {
                        if (data[x].enabled == true && data[x].type == "SpartanAdminEmail") {
                          emailTemplate = data[x].content;
                          break;
                        }
                      }
                    }
                    var htmlGenerate = ServiceUrls.emailServices.htmlGenerate;
                    var emailData = {
                      template: emailTemplate,
                      content: emailDataAdmin,
                    };
                    axios
                      .post(htmlGenerate, emailData)
                      .then(function (htmlResponse) {
                        var emailPostData = {
                          type: "AttachmentRequest",
                          toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                          fromEmail: fromEmail,
                          ccEmails: ccEmails,
                          bccEmails: null,
                          subject: "Plan my trip booking - " + referenceNumber,
                          attachmentPath: "",
                          html: htmlResponse.data.data,
                        };
                        var mailUrl = ServiceUrls.emailServices.emailApiWithAttachment;
                        sendMailServiceWithCallBack(mailUrl, emailPostData, function () {
                          try {
                            // let insertID = Number(responseObject);
                            var emailApi = ServiceUrls.emailServices.emailApi;
                            // sendMailService(emailApi, postData);
                            // sendMailService(emailApi, custmail);
        
                            var phoneNumber = JSON.parse(localStorage.User).loginNode.phoneList.filter(function (phones) {
                              return phones.type.toLowerCase().includes("telephone") || phones.type.toLowerCase().includes("mobile");
                            });
        
                            var emailContent = {
                              logoUrl: JSON.parse(localStorage.User).loginNode.logo,
                              bookingLabel: "Booking ID",
                              bookingReference: referenceNumber,
                              agencyEmail: fromEmail,
                              title: "Inquiry Received for Plan My Trip",
                              personName: self.firstName + " " + self.lastName,
                              servicesData: [
                                {
                                  name: "Guest Name",
                                  value: self.firstName + " " + self.lastName,
                                },
                                {
                                  name: "Email Address",
                                  value: self.email,
                                },
                                {
                                  name: "Contact",
                                  value: $(".selected-dial-code").text() + self.phone,
                                },
                                {
                                  name: "Request Date",
                                  value: moment(new Date()).format("YYYY-MM-DD"),
                                },
                              ],
                              emailMessage: "<p>Your inquiry is successfully received and is under booking ID&nbsp;" + referenceNumber + ".</p><p>One of our agent will get back to you shortly.</p>",
                              agencyPhone: phoneNumber[0].nodeContactNumber.number ? "+" + JSON.parse(localStorage.User).loginNode.country.telephonecode + "" + phoneNumber[0].nodeContactNumber.number : "NA",
                            };
        
                            self.getTemplate("SpartanEmailTemplate").then(function (templateResponse) {
                              var data = templateResponse.data.data;
                              var emailTemplate = "";
                              if (data.length > 0) {
                                for (var x = 0; x < data.length; x++) {
                                  if (data[x].enabled == true && data[x].type == "SpartanEmailTemplate") {
                                    emailTemplate = data[x].content;
                                    break;
                                  }
                                }
                              }
                              var htmlGenerate = ServiceUrls.emailServices.htmlGenerate;
                              var emailData = {
                                template: emailTemplate,
                                content: emailContent,
                              };
                              axios
                                .post(htmlGenerate, emailData)
                                .then(function (htmlResponse) {
                                  var emailPostData = {
                                    type: "AttachmentRequest",
                                    toEmails: Array.isArray(self.email) ? self.email : [self.email],
                                    fromEmail: fromEmail,
                                    ccEmails: null,
                                    bccEmails: null,
                                    subject: "Booking Confirmation - " + referenceNumber,
                                    attachmentPath: "",
                                    html: htmlResponse.data.data,
                                  };
                                  var mailUrl = ServiceUrls.emailServices.emailApiWithAttachment;
                                  sendMailServiceWithCallBack(mailUrl, emailPostData, function () {
                                    self.isBooking = false;
                                    self.email = "";
                                    self.name = "";
                                    self.phone = "";
                                    self.message = "";
                                    alertify.alert(getValidationMsgByCode("M023"), getValidationMsgByCode("M022"), function () {
                                      window.location.reload();
                                    });
                                  });
                                })
                                .catch(function (error) {
                                  console.log(error);
                                });
                            });
                          } catch (e) {
                            console.log(e);
                          }
                        });
                      })
                      .catch(function (error) {
                        console.log(error);
                      });
                  });
                }
              })
              .catch(function (updateError) {
                self.isLoading = false;
                alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M045")).set("closable", false);
                console.error(updateError);
              });
          })
          .catch(function (error) {
            self.isLoading = false;
            console.error(error);
            alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M045")).set("closable", false);
          });
      }
    },
    stopLoader: function () {
        $('#preloader').delay(50).fadeOut(250);
    },
    getTemplate(template) {
      // var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
      var url = ServiceUrls.emailServices.getTemplate + "AGY75/" + template;
      // var url = ServiceUrls.emailServices.getTemplate + LoggedUser.loginNode.code + "/" + template;
      return axios.get(url);
    },
    cmsFormUpdateData: async function (data, id) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/cms/data/" + id;
      var callMethod = "PUT";
      if (data != null) {
        data = JSON.stringify(data);
      }
      try {
        let formPostRes = await axios({
          url: url,
          method: callMethod,
          headers: {
            "Content-Type": "application/json",
          },
          data: data,
        });
        return formPostRes;
      } catch (error) {
        console.log(error);
        return error.response.data.code;
      }
    },
    cmsRequestData: function (callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      return fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          "Content-Type": "application/json",
        },
        body: data, // body data type must match "Content-Type" header
      }).then(function (response) {
        return response.json();
      });
    },
    resetValues() {
      this.hasHotel = false;
      this.hasTent = false;
      this.hotelDateIn = "";
      this.hotelDateOut = "";
      this.tentDateIn = "";
      this.tentDateOut = "";
    },
    dropdownChange: function (event, type) {
      if (event != undefined && event.target != undefined && event.target.value != undefined) {
        if (type == "flightfrom") {
          this.flightDepDate = event.target.value;
        } else if (type == "flightto") {
          this.flightArrDate = event.target.value;
        }  else if (type == "hotelin") {
          this.hotelDateIn = event.target.value;
        } else if (type == "hotelout") {
          this.hotelDateOut = event.target.value;
        } else if (type == "tentin") {
          this.tentDateIn = event.target.value;
        } else if (type == "tentout") {
          this.tentDateOut = event.target.value;
        } else if (type == "transferfrom") {
          this.transferDateIn = event.target.value;
        } else if (type == "transferto") {
          this.transferDateOut = event.target.value;
        } else if (type == "rentalfrom") {
          this.carRentalDateIn = event.target.value;
        } else if (type == "rentalto") {
          this.carRentalDateOut = event.target.value;
        }
      }
    },
    setCalender() {
      var dateFormat = "dd/mm/yy";
      $("#flightfrom, #rentalfrom, #transferfrom").datepicker({
        minDate: new Date("11/20/2021"),
        maxDate: new Date("12/08/2021"),
        numberOfMonths: 1,
        changeMonth: true,
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>",
        duration: "fast",
        showAnim: "slide",
        showOptions: {
          direction: "up",
        },
        showButtonPanel: false,
        dateFormat: dateFormat,
      });
      $("#flightto, #rentalto, #transferto").datepicker({
        minDate: new Date("11/20/2021"),
        maxDate: new Date("12/08/2021"),
        numberOfMonths: 1,
        changeMonth: true,
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>",
        duration: "fast",
        showAnim: "slide",
        showOptions: {
          direction: "up",
        },
        showButtonPanel: false,
        dateFormat: dateFormat,
        beforeShow: function (event, ui) {
          var selectedDate = $("#flightfrom").datepicker("getDate", "+1d");
          if (selectedDate) {
            selectedDate.setDate(selectedDate.getDate() + 1);
            $("#flightto").datepicker("option", "minDate", selectedDate);
          }
          var selectedDate = $("#rentalfrom").datepicker("getDate", "+1d");
          if (selectedDate) {
            selectedDate.setDate(selectedDate.getDate() + 1);
            $("#rentalto").datepicker("option", "minDate", selectedDate);
          }
          var selectedDate = $("#transferfrom").datepicker("getDate", "+1d");
          if (selectedDate) {
            selectedDate.setDate(selectedDate.getDate() + 1);
            $("#transferto").datepicker("option", "minDate", selectedDate);
          }
        },
      });
      $("#tentin").datepicker({
        minDate: new Date("12/02/2021"),
        maxDate: new Date("12/05/2021"),
        numberOfMonths: 1,
        changeMonth: true,
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>",
        duration: "fast",
        showAnim: "slide",
        showOptions: {
          direction: "up",
        },
        showButtonPanel: false,
        dateFormat: dateFormat,
        beforeShow: function (i) {
          // if ($(i).attr("readonly")) {
          //   return false;
          // }
        },
      });
      $("#tentout").datepicker({
        minDate: new Date("12/02/2021"),
        maxDate: new Date("12/05/2021"),
        numberOfMonths: 1,
        changeMonth: true,
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>",
        duration: "fast",
        showAnim: "slide",
        showOptions: {
          direction: "up",
        },
        showButtonPanel: false,
        dateFormat: dateFormat,
        beforeShow: function (event, ui) {
          var selectedDate = $('#tentin').datepicker('getDate', '+1d');
          if (selectedDate) {
            selectedDate.setDate(selectedDate.getDate() + 1);
            $("#tentout").datepicker("option", "minDate", selectedDate);
          }
          // if ($(event).attr("readonly")) {
          //   return false;
          // }
        },
      });
      // $("#tentin").datepicker("setDate", new Date("12/02/2021"));
      // $("#tentout").datepicker("setDate", new Date("12/05/2021"));
      $("#hotelin").datepicker({
        minDate: new Date("11/20/2021"),
        maxDate: new Date("12/08/2021"),
        numberOfMonths: 1,
        changeMonth: true,
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>",
        duration: "fast",
        showAnim: "slide",
        showOptions: {
          direction: "up",
        },
        showButtonPanel: false,
        dateFormat: dateFormat,
      });
      $("#hotelout").datepicker({
        minDate: new Date("11/20/2021"),
        maxDate: new Date("12/08/2021"),
        numberOfMonths: 1,
        changeMonth: true,
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>",
        duration: "fast",
        showAnim: "slide",
        showOptions: {
          direction: "up",
        },
        showButtonPanel: false,
        dateFormat: dateFormat,
        beforeShow: function (event, ui) {
          var selectedDate = $("#hotelin").datepicker("getDate", "+1d");
          if (selectedDate) {
            selectedDate.setDate(selectedDate.getDate() + 2);
            $("#hotelout").datepicker("option", "minDate", selectedDate);
          }
        },
      });
      $("#check_9").click(function() { return false; });
    },
  },

  mounted: function () {
    this.getPagecontent();
    this.setCalender();
    sessionStorage.active_e = 7;
    var vm = this;
    this.$nextTick(function () {
      $("#flightfrom").on("change", function (e) {
        vm.dropdownChange(e, "flightfrom");
      });
      $("#flightto").on("change", function (e) {
        vm.dropdownChange(e, "flightto");
      });
      $("#hotelin").on("change", function (e) {
        vm.dropdownChange(e, "hotelin");
      });
      $("#hotelout").on("change", function (e) {
        vm.dropdownChange(e, "hotelout");
      });
      $("#tentin").on("change", function (e) {
        vm.dropdownChange(e, "tentin");
      });
      $("#tentout").on("change", function (e) {
        vm.dropdownChange(e, "tentout");
      });
      $("#transferfrom").on("change", function (e) {
        vm.dropdownChange(e, "transferfrom");
      });
      $("#transferto").on("change", function (e) {
        vm.dropdownChange(e, "transferto");
      });
      $("#rentalfrom").on("change", function (e) {
        vm.dropdownChange(e, "rentalfrom");
      });
      $("#rentalto").on("change", function (e) {
        vm.dropdownChange(e, "rentalto");
      });
    });
  },
});

$("#phone").intlTelInput({
  initialCountry: JSON.parse(localStorage.User).loginNode.country.code || "AE",
  geoIpLookup: function (callback) {
      $.get('https://ipinfo.io', function () { }, "jsonp").always(function (resp) {
          var countryCode = (resp && resp.country) ? resp.country : "";
          (countryCode);
      });
  },
  separateDialCode: true,
  autoPlaceholder: "off",
  preferredCountries: ['ae', 'sa', 'om', 'qa'],
  utilsScript: "../assets/js/intlTelInput.js?v=201903150000" // just for formatting/placeholders etc
});