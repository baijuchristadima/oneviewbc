var hotelList = new Vue({
    el: '#hotels',
    name: 'hotels',
    data: {
        cityName: 'ABU DHABI',
        cityNameUi: 'أبو ظبي',
        HotelName:"Rosewood Abu Dhabi",
        HotelNameAr:"روزوود أبوظبي",
        // occupancyType: '',
        locationHotels: [],
        OccupancyHotels: [],
        roomCount: 0,
        numberOfNights: 0,
        rooms: [],
        Banner: {
            Banner_Image: ''
        },
        fromDate: '',
        toDate: '',
        labels: {
            Adult_Label: "Adult",
            Child_Label: "Child",
            Infant_Label: "Infant",
            Traveler_Placeholder: "Guest",
            City_Placeholder: "City",
            Room_Label: "Room",
            Child_Age_Label: "(2-12 yrs)",
            Adult_Age_Label: "(12+ yrs)",
        },
        showOccupancy: false,
        occupancySummary: "1 Adult",
        MainSection:{},
        language: (localStorage.Languagecode) ? localStorage.Languagecode : "en"
    },
    created: function () {
        // this.$set(this.values, id, '')
    },
    methods: {
        stopLoader: function () {
            $('#preloader').delay(50).fadeOut(250);
        },
        addRoom: function () {
            var self = this;
            if (self.roomCount < 5) {
                self.roomCount++;
                let type = _.first(self.OccupancyHotels).Occupancy_ID;
                self.rooms.push(Number(type));
            }
        },
        removeRoom: function () {
            var self = this;
            if (self.roomCount > 1) {
                self.rooms = self.rooms.slice(0, self.rooms.length - 1);
                self.roomCount--;
            }
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                self.language = langauage;
                var homecms = commonPath + Agencycode + '/Template/Home Page/Home Page/Home Page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (res) {
                    console.log(res);

                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var main = pluck('Index_Section', self.content);
                        self.MainSection = getAllMapData(main[0].component);
                        self.Banner.Banner_Image = self.MainSection.Banner_Image_1920_x_700px;
                        // var Banner = self.pluck('Index_Section', self.content);
                        // self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);

                        setTimeout(() => {
                            initSelect2();
                        }, 100);
                        self.getOccupancy(true);
                        self.stopLoader();
                    }

                }).catch(function (error) {
                    self.stopLoader();
                    console.log('Error', error);
                    // self.content = [];
                });
            });
        },
        getOccupancy: function (isForLanguage) {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = commonPath + Agencycode + '/Template/Hotel Configuration/Hotel Configuration/Hotel Configuration.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (res) {
                    console.log(res);
                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var val = self.pluck('Configurations', self.content);
                        self.OccupancyHotels = self.pluckcom('Room_Occupancy_List', val[0].component);
                        self.addRoom();
                        if (isForLanguage) {
                            self.removeRoom();
                        }

                           
                        
                    }
                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });
            });
        },
        getPackage: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var tohotelurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/Hotel/Hotel/Hotel.ftl';
                axios.get(tohotelurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    var packageData = response.data;
                    let PackageListTemp = [];
                    if (packageData != undefined && packageData.Values != undefined) {
                        PackageListTemp = packageData.Values.filter(function (el) {
                            return el.Status == true
                        });
                        PackageListTemp.forEach(function (item, index) {
                            self.locationHotels.push(item.Location.toUpperCase().trim());
                        });
                        self.$nextTick(function () {
                            self.locationHotels = _.uniq(_.sortBy(self.locationHotels));
                        });
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });
        },
        dateChecker: function () {
            let dateObj1 = document.getElementById("from1");
            let dateValue1 = dateObj1.value;
            let dateObj2 = document.getElementById("to1");
            let dateValue2 = dateObj2.value;
            if (dateValue2 !== undefined || dateValue2 !== '') {
                // document.getElementById("to1").value = '';
                $('#to1').val('').trigger('change');
                $("#to1").val("");
                return false;
            }
        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {

                if (type == 'from1') {
                    this.fromDate = event.target.value;
                } else if (type == 'to1') {
                    this.toDate = event.target.value;
                    // this.noOfnightsFind(this.fromDate, this.toDate);
                } else if (type == 'city') {
                    this.cityName = event.target.value;
                } else if (type == 'occupancy') {
                    this.occupancyType = event.target.value;
                }
            }
        },
        searchHotels: function () {
            if (this.cityName == null || this.cityName.trim() == "") {
                alertify.alert('Alert', 'Please select a city.');
            } else if (this.fromDate == null || this.fromDate.trim() == "") {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M006')).set('closable', false);
            } else if (this.toDate == null || this.toDate.trim() == "") {
                // alertify.alert('Alert', 'Please select Check Out Date.');
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M007')).set('closable', false);
            } else {
                var searchCriteria =
                    "&city=" + this.cityName +
                    "&cin=" + this.fromDate +
                    "&cout=" + this.toDate +
                    "&rooms=" + this.roomCount +
                    "&roomtype=" + JSON.stringify(this.rooms);
                // searchCriteria = searchCriteria.split(' ').join('-');
                var uri = "/World-Sailing/hotel-results.html?" + searchCriteria;
                window.location.href = uri;
            }

        },
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#from1").datepicker({
                // minDate: "0d",
                // maxDate: "360d",
                minDate: new Date('05/23/2022'),
                maxDate: new Date('05/30/2022'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
            });

            $("#to1").datepicker({
                minDate: new Date('05/23/2022'),
                maxDate: new Date('05/30/2022'),
                numberOfMonths: 1,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
                beforeShow: function (event, ui) {
                    try {
                        var selectedDate = $('#from1').datepicker('getDate', '+1d');
                        selectedDate.setDate(selectedDate.getDate() + 1);
                        $("#to1").datepicker("option", "minDate", selectedDate);
                    } catch (error) {
                            
                    }
                }
            });
        },
        noOfnightsFind: function () {
            try {
                var date1 = this.fromDate;
                var date2 = this.toDate;
                // if (date1 && date2) {
                var start = moment(date1, "DD/MM/YYYY");
                var end = moment(date2, "DD/MM/YYYY");
                var nights = end.diff(start, 'days')
                return nights > 1 ? nights + ' '+this.MainSection.Nights_Label : nights == 1 ? '1 '+ this.MainSection.Night_Label : this.MainSection.Number_Of_Nights_Label || this.MainSection.Number_Of_Nights_Label;
                // }
            } catch (error) {
                return this.MainSection.Number_Of_Nights_Label
            }
        },
    },
    mounted: function () {
        // localStorage.removeItem("backUrl");
        this.setCalender();
        this.getPagecontent();
        this.getOccupancy(false);
        this.getPackage();
        sessionStorage.active_e = 2;
        var vm = this;
        this.$nextTick(function () {
            $('#from1').on("change", function (e) {
                vm.dropdownChange(e, 'from1')
            });
            $('#to1').on("change", function (e) {
                vm.dropdownChange(e, 'to1')
            });
            $('#city').on("change", function (e) {
                vm.dropdownChange(e, 'city')
            });
            $('#occupancy').on("change", function (e) {
                vm.dropdownChange(e, 'occupancy')
            });
        });
    },
    watch: {
        rooms: function () {
            var self = this;
            var occupancySummary = 0;
            var withChild = false;
            for (var index = 0; index < self.rooms.length; index++) {
                if (self.rooms[index] == 3) {
                    withChild = true;
                    occupancySummary += 2;
                } else if (self.rooms[index] == 4){
                    occupancySummary += 3;
                }
                else {
                    occupancySummary += self.rooms[index];
                }
                
            }
            self.occupancySummary = occupancySummary + (occupancySummary > 1 ? " " +this.MainSection.Adults_Label : " " + this.MainSection.Adult_Label) + (withChild ? ", 1 " +this.MainSection.Child_Label: "");
        },
    }
});
jQuery(document).ready(function ($) {
    initSelect2();
});

function initSelect2() {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
}