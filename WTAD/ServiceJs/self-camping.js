var tentList = new Vue({
    el: '#tent',
    name: 'tent',
    data: {
        cityName: 'ABU DHABI',
        // occupancyType: '',
        locationTent: [],
        OccupancyHotels: [],
        roomCount: 0,
        numberOfNights: 0,
        rooms: [],
        Banner: {
            Banner_Image: ''
        },
        fromDate: '',
        toDate: '',
        labels: {
            Adult_Label: "Adult",
            Child_Label: "Child",
            Infant_Label: "Infant",
            Traveler_Placeholder: "Guest",
            City_Placeholder: "City",
            Room_Label: "Room",
            Child_Age_Label: "(2-12 yrs)",
            Adult_Age_Label: "(12+ yrs)",
        },
        showOccupancy: false,
        occupancySummary: "Single",
        MainSection:{},
        occupancy:'',
        set: null
    },
    created: function () {
        // this.$set(this.values, id, '')
    },
    methods: {
        addRoom: function () {
            var self = this;
            if (self.roomCount < 5) {
                self.roomCount++;
                let type = _.first(self.OccupancyHotels).Occupancy_ID;
                self.rooms.push(Number(type));
            }
        },
        removeRoom: function () {
            var self = this;
            if (self.roomCount > 1) {
                self.rooms = self.rooms.slice(0, self.rooms.length - 1);
                self.roomCount--;
            }
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getBanner: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = commonPath + Agencycode + '/Template/Home Page/Home Page/Home Page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (res) {
                    console.log(res);

                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var main = pluck('Index_Section', self.content);
                        self.MainSection = getAllMapData(main[0].component);
                        self.Banner.Banner_Image = self.MainSection.Banner_Image_1920_x_700px;
                        self.getRoomData();
                        self.getPackage();
                        self.getOccupancy();
                        // var Banner = self.pluck('Index_Section', self.content);
                        // self.Banner.Banner_Image = self.pluckcom('Banner_Image', Banner[0].component);
                    }

                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });
            });
        },
        getOccupancy: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = commonPath + Agencycode + '/Template/Tent Configuration/Tent Configuration/Tent Configuration.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (res) {
                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var val = self.pluck('Configurations', self.content);
                        var tempOccupancyHotels = self.pluckcom('Room_Occupancy_List', val[0].component);
                        self.OccupancyHotels = tempOccupancyHotels.splice(0,2)
                        // self.addRoom();
                           
                        
                    }
                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });
            });
        },
        getPackage: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var tohotelurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/List Of Self Camping/List Of Self Camping/List Of Self Camping.ftl';
                axios.get(tohotelurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    var packageData = response.data;
                    let PackageListTemp = [];
                    if (packageData != undefined && packageData.Values != undefined) {
                        PackageListTemp = packageData.Values.filter(function (el) {
                            return el.Status == true
                        });
                        PackageListTemp.forEach(function (item, index) {
                            self.locationTent.length=0;
                            self.locationTent.push(item.Location.toUpperCase().trim());
                        });
                        self.$nextTick(function () {
                            self.locationTent = _.uniq(_.sortBy(self.locationTent));
                        });
                        var url_string = window.location.href
                        var url = new URL(url_string);
                        var c = url.searchParams.get("set");
                        self.set = c;
                    }
                }).catch(function (error) {
                    console.log('Error');
                    self.content = [];
                });
            });
        },
        dateChecker: function () {
            let dateObj1 = document.getElementById("from1");
            let dateValue1 = dateObj1.value;
            let dateObj2 = document.getElementById("to1");
            let dateValue2 = dateObj2.value;
            if (dateValue2 !== undefined || dateValue2 !== '') {
                // document.getElementById("to1").value = '';
                $('#to1').val('').trigger('change');
                $("#to1").val("");
                return false;
            }
        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {

                if (type == 'from1') {
                    this.fromDate = event.target.value;
                } else if (type == 'to1') {
                    this.toDate = event.target.value;
                    // this.noOfnightsFind(this.fromDate, this.toDate);
                } else if (type == 'city') {
                    this.cityName = event.target.value;
                } else if (type == 'occupancy') {
                    this.occupancyType = event.target.value;
                }
            }
        },
        searchTent: function () {
            if (this.cityName == null || this.cityName.trim() == "") {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M011')).set('closable', false);
            } else if (this.fromDate == null || this.fromDate.trim() == "") {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M006')).set('closable', false);
            } else if (this.toDate == null || this.toDate.trim() == "") {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M007')).set('closable', false);
            } else {
                var url_string = window.location.href
                var url = new URL(url_string);
                var c = url.searchParams.get("set");
                
                var searchCriteria =
                    "&city=" + this.cityName +
                    "&cin=" + this.fromDate +
                    "&cout=" + this.toDate +
                    "&rooms=" + this.roomCount +
                    "&set=" + c +
                    "&roomtype=" + JSON.stringify(this.rooms);
                // searchCriteria = searchCriteria.split(' ').join('-');
                var uri = "/WTAD/self-camping-result.html?" + searchCriteria;
                window.location.href = uri;
            }

        },
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#from1").datepicker({
                // minDate: "0d",
                // maxDate: "360d",
                minDate: new Date('12/02/2021'),
                maxDate: new Date('12/05/2021'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
            });

            $("#to1").datepicker({
                minDate: new Date('12/02/2021'),
                maxDate: new Date('12/05/2021'),
                numberOfMonths: 1,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
                beforeShow: function (event, ui) {
                    var self=this;
                    var url_string = window.location.href
                    var url = new URL(url_string);
                    var c = url.searchParams.get("set");
                    var selectedDate = $('#from1').datepicker('getDate', '+1d');
                    // selectedDate.setDate(selectedDate.getDate() + ((c == "SC3" || c == "SC2") ? 1 : 2));
                    selectedDate.setDate(selectedDate.getDate() + 1);
                    $("#to1").datepicker("option", "minDate", selectedDate);
                    // if (c != 'SC3' && c != 'SC2') {
                    //     var maxDateTemp = $('#from1').datepicker('getDate', '+1d');
                    //     maxDateTemp.setDate(maxDateTemp.getDate() + 7);
                    //     if(maxDateTemp > new Date('12/05/2021')){
                    //         $("#to1").datepicker("option", "maxDate", new Date('12/05/2021'));
                    //     } else {
                    //         $("#to1").datepicker("option", "maxDate", maxDateTemp);
                    //     }
                    // }
                }
            });
        },
        noOfnightsFind: function () {
            try {
                var date1 = this.fromDate;
                var date2 = this.toDate;
                // if (date1 && date2) {
                var start = moment(date1, "DD/MM/YYYY");
                var end = moment(date2, "DD/MM/YYYY");
                var nights = end.diff(start, 'days')
                return nights > 1 ? nights + ' '+this.MainSection.Nights_Label : nights == 1 ? '1 '+ this.MainSection.Night_Label : this.MainSection.Number_Of_Nights_Label || this.MainSection.Number_Of_Nights_Label;
                // }
            } catch (error) {
                return this.MainSection.Number_Of_Nights_Label
            }
        },
        getRoomData:function(){
            var self = this;
            var occupancySummary = 0;
            for (var index = 0; index < self.rooms.length; index++) {
                if (self.rooms[index] == 3) {
                    occupancySummary += 3;
                }
                else {
                    occupancySummary += self.rooms[index];
                }
                
            }
            self.occupancySummary = occupancySummary + (occupancySummary > 1 ? " " + this.MainSection.Persons_Label : " " + this.MainSection.Person_Label);
        },
    },
    mounted: function () {
        // localStorage.removeItem("backUrl");
        var vm = this;
        this.setCalender();
        this.getBanner();
        this.getOccupancy();
        setTimeout(() => vm.addRoom(), 1000);
        this.getPackage();
        sessionStorage.active_e = 2;
        
        this.$nextTick(function () {
            $('#from1').on("change", function (e) {
                vm.dropdownChange(e, 'from1')
            });
            $('#to1').on("change", function (e) {
                vm.dropdownChange(e, 'to1')
            });
            $('#city').on("change", function (e) {
                vm.dropdownChange(e, 'city')
            });
            $('#occupancy').on("change", function (e) {
                vm.dropdownChange(e, 'occupancy')
            });
        });
    },
    watch: {
        rooms: function () {
            this.getRoomData();
            
        }
    }
});
jQuery(document).ready(function ($) {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
});