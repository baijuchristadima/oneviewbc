const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var trasferList = new Vue({
    i18n,
    el: '#transferconfirmation',
    name: 'transferconfirmation',
    data: {
        divHtlBkngSucces: false,
        divHtlBkngCancel:false,
        divHtlBkngFailed:false,
        isLoading: true,
        user: {
            "loginNode": {
                "address": "",
                "country": {
                    "code": "",
                    "telephonecode": ""
                },
                "phoneList": [{
                        "type": "",
                        "nodeContactNumber": {
                            "number": ""
                        },
                        "number": ""
                    },
                    {
                        "type": "",
                        "nodeContactNumber": {
                            "number": ""
                        },
                        "number": ""
                    },
                    {
                        "type": "",
                        "nodeContactNumber": {
                            "number": ""
                        },
                        "number": ""
                    }
                ],
            }
        },
        logoUrl: '',
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'AED',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        maincontent: {},
        details: {},
        Bookingdetails: {},
        bookDetailsfromCMS: {},
        phoneNumberAgency: '',
        telephonecode: '',
        paxDetails: {},
        labels:{}
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        getLabel: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var commonPath = '/persons/source?path=/B2B/AdminPanel/CMS/';
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = commonPath + Agencycode + '/Template/Transfer Confirmation/Transfer Confirmation/Transfer Confirmation.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (res) {
                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var main = pluck('Main_Section', self.content);
                        self.labels = getAllMapData(main[0].component);
                    }

                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });
            });
        },
        dropdownChange: function (event, type) {
            if (event != undefined && event.target != undefined && event.target.value != undefined) {

                if (type == 'from1') {
                    this.fromDate = event.target.value;
                } else if (type == 'to1') {
                    this.toDate = event.target.value;
                } else if (type == 'city') {
                    this.cityName = event.target.value;
                } else if (type == 'occupancy') {
                    this.occupancyType = event.target.value;
                }
            }
        },
        getCmsFormDataById: async function (id) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/cms/data/" + id;
            // var HubServices = this.HubServicesUrls;
            // var url = HubServices.CMS_SERVER + HubServices.CMS_Form + "/" + id;
            var callMethod = "GET";
            try {
                let formPostRes = await axios({
                    url: url,
                    method: callMethod,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                })
                return formPostRes;
            } catch (error) {
                console.log(error);
                return error.response.data.code;
            }
        },
        getPackagedetails: function (transferlurl) {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                if (transferlurl != "") {
                    var tohotelurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + transferlurl + '.ftl';
                    axios.get(tohotelurl, {
                        headers: {
                            'content-type': 'text/html',
                            'Accept': 'text/html',
                            'Accept-Language': langauage
                        }
                    }).then(function (response) {

                    })
                }
            });
        },
        getTemplate(template) {
            // var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
            var url = ServiceUrls.emailServices.getTemplate + "AGY75/" + template;
            // var url = ServiceUrls.emailServices.getTemplate + LoggedUser.loginNode.code + "/" + template;
            return axios.get(url);
        },
        sentMail: function () {
            var self = this;
            var user = JSON.parse(localStorage.User);
            var fromEmail = user.loginNode.parentEmailId ? user.loginNode.parentEmailId : (user.loginNode.email ? user.loginNode.email : "uaecrt@gmail.com") || "uaecrt@gmail.com";
            var toEmail = user.loginNode.parentEmailId;
            var ccEmails = _.filter(user.loginNode.emailList, function (o) {
              return o.emailTypeId == 3 && o.emailType.toLowerCase() == "support";
            });
            ccEmails = _.map(ccEmails, function (o) {
              return o.emailId;
            });
            self
              .getTemplate("WtadAdminEmail")
              .then(function (templateResponse) {
                var data = templateResponse.data.data;
                var emailTemplate = "";
                if (data.length > 0) {
                  for (var x = 0; x < data.length; x++) {
                    if (data[x].enabled == true && data[x].type == "WtadAdminEmail") {
                      emailTemplate = data[x].content;
                      break;
                    }
                  }
                }
                debugger;
                var emailDataAdmin = {
                  bookingReference: self.transferDetails.referenceNumber,
                  fullName: self.transferDetails.text23,
                  emailAddress: self.transferDetails.text17,
                  contact: self.transferDetails.text18,
                  hotelDetails:
                    "<ul>" + 
                    "<li>Pick up:&nbsp;" +
                    self.bookDetailsfromCMS.text10 +
                    "</li>" +
                    "<li>Drop off:&nbsp;" +
                    self.bookDetailsfromCMS.text11 +
                    "</li>" +
                    "<li>Pick up date:&nbsp;" +
                    moment(self.bookDetailsfromCMS.text12, "DD/MM/YYYY").format("YYYY-MM-DD") +
                    "</li>" +
                    "<li>Pick up time:&nbsp;" +
                    self.bookDetailsfromCMS.text14 +
                    "</li>" +
                    "<li>Return:&nbsp;" +
                    (self.bookDetailsfromCMS.text13 == "NA" ? "NA" : moment(self.bookDetailsfromCMS.text13, "DD/MM/YYYY").format("YYYY-MM-DD")) +
                    "</li>" +
                    "<li>Vehicle Type:&nbsp; " +
                    self.bookDetailsfromCMS.text15 +
                    "</li>" +
                    "<li>Hotel name:&nbsp;" +
                    self.bookDetailsfromCMS.text22 +
                    "</li>" +
                    "<li>Airport:&nbsp;" +
                    self.bookDetailsfromCMS.text21 +
                    "</li>" +
                    "<li>Flight Number:&nbsp;" +
                    self.bookDetailsfromCMS.text19 +
                    "</li>" +
                    "<li>Flight Date & Time:&nbsp;" +
                    self.bookDetailsfromCMS.text20 +
                    "</li>" +
                   "</ul>",
                };
                var htmlGenerate = ServiceUrls.emailServices.htmlGenerate;
                var emailData = {
                  template: emailTemplate,
                  content: emailDataAdmin,
                };
                axios.post(htmlGenerate, emailData).then(function (htmlResponse) {
                  var emailPostData = {
                    type: "AttachmentRequest",
                    toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                    fromEmail: fromEmail,
                    ccEmails: ccEmails,
                    bccEmails: null,
                    subject: "Transfer booking - " + self.transferDetails.referenceNumber,
                    attachmentPath: "",
                    html: htmlResponse.data.data,
                  };
                  var mailUrl = ServiceUrls.emailServices.emailApiWithAttachment;
                  sendMailServiceWithCallBack(mailUrl, emailPostData, function () {
                    try {
                      var emailApi = ServiceUrls.emailServices.emailApi;
                      // sendMailService(emailApi, postData);
                      // sendMailService(emailApi, custmail);

                      var phoneNumber = JSON.parse(localStorage.User).loginNode.phoneList.filter(function (phones) {
                        return phones.type.toLowerCase().includes("telephone") || phones.type.toLowerCase().includes("mobile");
                      });

                      var message = "";

                      if (self.divHtlBkngSucces) {
                        message =
                          "<p>Your transfer booking confirmed with Booking ID&nbsp;" +
                          self.transferDetails.referenceNumber ;
                      } else {
                        message = "<p>Your booking has failed.</p>";
                      }

                      var nights = moment(new Date(self.bookDetailsfromCMS.date2)).diff(moment(new Date(self.bookDetailsfromCMS.date1)), "days");

                      var emailContent = {
                        message: message,
                        title: "Inquiry Received for Transfer",
                        agencyPhone: phoneNumber[0].nodeContactNumber.number ? "+" + JSON.parse(localStorage.User).loginNode.country.telephonecode + "" + phoneNumber[0].nodeContactNumber.number : "NA",
                        agencyEmail: fromEmail || "",
                        logoUrl: JSON.parse(localStorage.User).loginNode.logo,
                        fullName: self.bookDetailsfromCMS.text23,
                        bookingRefId: self.transferDetails.referenceNumber,
                        hotelRef: "Pending",
                        vehicleType: self.bookDetailsfromCMS.text15,
                        fromTo: self.bookDetailsfromCMS.text10 + " to " + self.bookDetailsfromCMS.text11,
                        pickUp: moment(self.bookDetailsfromCMS.text12, "DD/MM/YYYY").format("YYYY-MM-DD") + " @ " + self.bookDetailsfromCMS.text14,
                        return: self.bookDetailsfromCMS.text13 == "NA" ? "NA" : moment(self.bookDetailsfromCMS.text13, "DD/MM/YYYY").format("YYYY-MM-DD"),
                        guestDetails: [{
                            pax: "<span>" + self.transferDetails.text23 + "</span>",
                      }]
                    }

                      self.getTemplate("wtadTransferBooking").then(function (templateResponse) {
                        var data = templateResponse.data.data;
                        var emailTemplate = "";
                        if (data.length > 0) {
                          for (var x = 0; x < data.length; x++) {
                            if (data[x].enabled == true && data[x].type == "wtadTransferBooking") {
                              emailTemplate = data[x].content;
                              break;
                            }
                          }
                        }
                        var htmlGenerate = ServiceUrls.emailServices.htmlGenerate;
                        var emailData = {
                          template: emailTemplate,
                          content: emailContent,
                        };
                        axios
                          .post(htmlGenerate, emailData)
                          .then(function (htmlResponse) {
                            var emailPostData = {
                              type: "AttachmentRequest",
                              toEmails: Array.isArray(self.transferDetails.text17) ? self.transferDetails.text17 : [self.transferDetails.text17],
                              fromEmail: fromEmail,
                              ccEmails: null,
                              bccEmails: null,
                              subject: "Booking Confirmation - " + self.transferDetails.referenceNumber,
                              attachmentPath: "",
                              html: htmlResponse.data.data,
                            };
                            var mailUrl = ServiceUrls.emailServices.emailApiWithAttachment;
                            sendMailServiceWithCallBack(mailUrl, emailPostData, function () {
                              // sendMailServiceWithCallBack(emailApi, postData, function () {
                              //     // self.isBooking = false;
                              // });
                            });
                          })
                          .catch(function (error) {
                            console.log(error);
                          });
                      });
                    } catch (e) {
                      console.log(e);
                    }
                  });
                });
              })
              .catch(function (error) {
                console.log(error);
              });
        },
        dayFromDate: function (date) {
            return moment(date).format('dddd');
        },
        formatDate: function (date) {
            return moment(date).format('YYYY-MM-DD');
        },
        leadPax: function (room) {
            var PaxList = room.filter(pax => (pax.requested && pax.type == "Adult"));
            if (PaxList) {
                return (PaxList[0].name + ' ' + PaxList[0].surName).trim();
            }
            return '';
        }
    },
    mounted: function () {
            debugger
            sessionStorage.active_e = 2;
        var self = this;
        this.user = JSON.parse(localStorage.User);
        this.isLoading = true;
        this.getLabel();
        var emailStatus = window.sessionStorage.getItem("emailStatusTransfer");
        this.transferDetails = JSON.parse(window.sessionStorage.getItem("TransferDetails"));
        this.logoUrl = JSON.parse(localStorage.User).loginNode.logo;
        this.phoneNumberAgency = JSON.parse(localStorage.User).loginNode.phoneList[1].number;
        this.telephonecode = '+' + JSON.parse(localStorage.User).loginNode.country.telephonecode;
        this.getPackagedetails(this.transferDetails.packegeUrl);
        this.getCmsFormDataById(this.transferDetails.cmsId).then(function (response) {
            console.log(response);
            self.bookDetailsfromCMS = response.data;
            if (response.data.keyword8.toLowerCase() == "success") {
                self.divHtlBkngSucces = true;
                self.isLoading = false;
                if (emailStatus === 'false') {
                    window.sessionStorage.setItem("emailStatusTransfer", true);
                    self.sentMail();
                }
                return
            }
            if (response.data.keyword8 == "Cancelled") {
                self.divHtlBkngCancel = true;
                self.isLoading = false;
                return false
            }
            self.divHtlBkngFailed = true;
            self.isLoading = false;
        }).catch(function (error) {
            self.isLoading = false;
            console.error(error);
        });
    },
});