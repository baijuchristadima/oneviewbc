const i18n = new VueI18n({
  numberFormats,
  locale: 'en', // set locale
  // set locale messages
})
var visa = new Vue({
  i18n,
  el: '#visapage',
  name: 'visapage',
  data: {
    selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
    CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    visaDetails: {
      Banner_Image: '',
      Title: '',
      Price: '',
      Document_Requirement_Title: '',
      Document_Requirements: '',
      Description_Title: '',
      Description: ''
    },
    name: '',
    phone: '',
    mail: '',
    visaType: '',
    file: null,
    file2: null,
    fileLink: '',
    fileLink2: '',
    fileLink3: '',
    customFile: '',
    passport: '',
    uploadflag: false,
    uploadimg: false,
    terms: '',
    visaSection: {},
    LabelSection: {},
    isBooking: false,
  },
  methods: {
    getPagecontent: function () {
      var self = this;

      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        // self.dir = langauage == "ar" ? "rtl" : "ltr";
        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Visa Page/Visa Page/Visa Page.ftl';

        axios.get(pageurl, {
          headers: {
            'content-type': 'text/html',
            'Accept': 'text/html',
            'Accept-Language': langauage
          }
        }).then(function (response) {
          if (response.data.area_List.length > 0) {
            var visa = pluck('Visa_Section', response.data.area_List);
            self.visaSection = getAllMapData(visa[0].component);

            var label = pluck('Page_Labels', response.data.area_List);
            self.LabelSection = getAllMapData(label[0].component);
            // $('.myselect').prepend('<option value="" selected disabled hidden>'+ self.choose +'</option>');
            self.isLoading = false;
          }
          // var visaDetails = self.pluck('Visa_Section', self.content.area_List);
          // if (visaDetails.length > 0) {
          //   self.visaDetails.Banner_Image = self.pluckcom('Banner_Image', visaDetails[0].component);
          //   self.visaDetails.Title = self.pluckcom('Title', visaDetails[0].component);
          //   self.visaDetails.Price = self.pluckcom('Price', visaDetails[0].component);
          //   self.visaDetails.Document_Requirement_Title = self.pluckcom('Document_Requirement_Title', visaDetails[0].component);
          //   self.visaDetails.Document_Requirements = self.pluckcom('Document_Requirements', visaDetails[0].component);
          //   self.visaDetails.Description_Title = self.pluckcom('Description_Title', visaDetails[0].component);
          //   self.visaDetails.Description = self.pluckcom('Description', visaDetails[0].component);
          // }

          // self.getdata = true;

        }).catch(function (error) {
          console.log('Error');
          self.aboutUsContent = [];
        });
      });
    },
    getTemplate(template) {
      // var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
      var url = ServiceUrls.emailServices.getTemplate + "AGY75/" + template;
      // var url = ServiceUrls.emailServices.getTemplate + LoggedUser.loginNode.code + "/" + template;
      return axios.get(url);
    },
    sendvisa: function () {
      debugger
      var self = this;
      let fileObj1 = document.getElementById("photo");
      let fileImg = fileObj1.value;
      let fileObj2 = document.getElementById("passport");
      let filePDF = fileObj2.value;
      let fileObj3 = document.getElementById("national-id");
      let fileID = fileObj3.value;
      if (!this.visaType) {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M025')).set('closable', false);
        return false;
      }
      if (!this.name) {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M032')).set('closable', false);
        return false;
      }
      if (this.mail == null) {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M009')).set('closable', false);
        return false;
      }
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.mail.match(emailPat);
      if (matchArray == null) {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M019')).set('closable', false);
        return false;
      }
      if (this.phone == null) {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M010')).set('closable', false);
        return false;
      }
      var phoneno = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
      // var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
      var matchphone = this.phone.match(phoneno);
      if (matchphone == null) {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M046')).set('closable', false);
        return false;
      }
      if (this.phone.length < 8) {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M046')).set('closable', false);
        return false;
      }
      // if (!this.message) {
      //   alertify.alert('Alert', 'Message required.').set('closable', false);
      //   return false;
      // }
      if (filePDF == undefined || filePDF == '') {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M026')).set('closable', false);
        return false;
      }
      if (fileImg == undefined || fileImg == '') {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M027')).set('closable', false);
        return false;
      }

      if (fileID == undefined || fileID == '') {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M036')).set('closable', false);
        return false;
      }
      if (!this.fileLink) {
        alertify.alert('Alert', 'Please Wait Image is uploading. Or Choose Another File').set('closable', false);
        return false;
      }
      if (!this.fileLink2) {
        alertify.alert('Alert', 'Please Wait Passport is uploading.Or Choose Another File').set('closable', false);
        return false;
      }
      if (!this.fileLink3) {
        alertify.alert('Alert', 'Please Wait National ID is uploading.Or Choose Another File').set('closable', false);
        return false;
      }
      if (!this.terms) {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M029')).set('closable', false);
        return false;
      } else {
        this.isBooking = true;
        var user = JSON.parse(localStorage.User);
        var fromEmail = user.loginNode.parentEmailId ? user.loginNode.parentEmailId : (user.loginNode.email ? user.loginNode.email : "uaecrt@gmail.com") || "uaecrt@gmail.com";
        var toEmail = user.loginNode.parentEmailId;
        // var toEmail = JSON.parse(localStorage.User).emailId;
        var ccEmails = _.filter(user.loginNode.emailList, function (o) {
          return o.emailTypeId == 3 && o.emailType.toLowerCase() == "support";
        });
        ccEmails = _.map(ccEmails, function (o) {
          return o.emailId;
        });
        var agency = localStorage.getItem("AgencyCode");
        // var referenceNumber = agency + '-VIS' + moment(new Date()).format("YYMMDDhhmmssSSS");
        var referenceNumber = agency + '-';

        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        let requestedDate = moment(String(new Date())).format("YYYY-MM-DDThh:mm:ss");
        var sendingRequest = {
          type: "Visa Request",
          keyword1: this.name,
          keyword2: this.mail,
          keyword3: this.phone,
          keyword4: this.visaType,
          // keyword10: referenceNumber,
          text11: this.fileLink,
          text12: this.fileLink2,
          date1: requestedDate,
          text13: this.fileLink3,
          nodeCode: agencyCode,
        };
        this.cmsRequestData("POST", "cms/data", sendingRequest, null)
          .then(function (response) {
            let insertID = Number(response);
            referenceNumber = referenceNumber + insertID;
            var emailApi = ServiceUrls.emailServices.emailApi;
            var updateDate = {
              keyword12: referenceNumber,
            };
            self
              .cmsFormUpdateData(updateDate, insertID)
              .then(function (update) {
                if (update.status == 200) {
                  console.log("Success:" + update.status);
                  var postData = {
                    type: "VisaAdmin",
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                    ccEmails: ccEmails,
                    bccEmails: [],
                    primaryColor: "#" + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: "#" + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary,
                    logo: JSON.parse(localStorage.User).loginNode.logo,
                    // logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: self.name,
                    emailAddress: self.mail,
                    contact: self.phone,
                    departureDate: moment(String(new Date())).format("YYYY-MM-DD"),
                    message: "Visa Request Reference Number: " + referenceNumber,
                  };
                  var custmail = {
                    type: "ThankYouRequest",
                    message: "Your Visa Request Reference Number: " + referenceNumber,
                    fromEmail: fromEmail,
                    toEmails: Array.isArray(self.mail) ? self.mail : [self.mail],
                    logo: JSON.parse(localStorage.User).loginNode.logo,
                    // logo: ServiceUrls.hubConnection.logoBaseUrl + JSON.parse(localStorage.User).loginNode.logo + ".xhtml?ln=logo",
                    agencyName: JSON.parse(localStorage.User).loginNode.name || "",
                    agencyAddress: JSON.parse(localStorage.User).loginNode.address || "",
                    personName: self.name,
                    primaryColor: "#" + JSON.parse(localStorage.User).loginNode.lookNfeel.primary,
                    secondaryColor: "#" + JSON.parse(localStorage.User).loginNode.lookNfeel.secondary,
                  };

                  try {
                    // let insertID = Number(responseObject);
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    // sendMailService(emailApi, postData);
                    // sendMailService(emailApi, custmail);

                    var phoneNumber = JSON.parse(localStorage.User).loginNode.phoneList.filter(function (phones) {
                      return phones.type.toLowerCase().includes("telephone") || phones.type.toLowerCase().includes("mobile");
                    });

                    var emailContent = {
                      logoUrl: JSON.parse(localStorage.User).loginNode.logo,
                      bookingLabel: "Booking ID",
                      bookingReference: referenceNumber,
                      agencyEmail: fromEmail,
                      title: "Inquiry Received for Visa",
                      personName: self.name,
                      servicesData: [{
                          name: "Guest Name",
                          value: self.name,
                        },
                        {
                          name: "Email Address",
                          value: self.mail,
                        },
                        {
                          name: "Contact",
                          value: self.phone,
                        },
                        {
                          name: "Start Date",
                          value: moment(String(new Date())).format("YYYY-MM-DD"),
                        },
                      ],
                      emailMessage: "<p>Your inquiry is successfully received and is under booking ID&nbsp;" + referenceNumber + ".</p><p>One of our agent will get back to you shortly.</p>",
                      agencyPhone: phoneNumber[0].nodeContactNumber.number ? "+" + JSON.parse(localStorage.User).loginNode.country.telephonecode + "" + phoneNumber[0].nodeContactNumber.number : "NA",
                    };

                    self.getTemplate("SpartanEmailTemplate").then(function (templateResponse) {
                      var data = templateResponse.data.data;
                      var emailTemplate = "";
                      if (data.length > 0) {
                        for (var x = 0; x < data.length; x++) {
                          if (data[x].enabled == true && data[x].type == "SpartanEmailTemplate") {
                            emailTemplate = data[x].content;
                            break;
                          }
                        }
                      }
                      var htmlGenerate = ServiceUrls.emailServices.htmlGenerate;
                      var emailData = {
                        template: emailTemplate,
                        content: emailContent,
                      };
                      axios
                        .post(htmlGenerate, emailData)
                        .then(function (htmlResponse) {
                          var emailPostData = {
                            type: "AttachmentRequest",
                            toEmails: Array.isArray(self.mail) ? self.mail : [self.mail],
                            fromEmail: fromEmail,
                            ccEmails: null,
                            bccEmails: null,
                            subject: "Booking Confirmation - " + referenceNumber,
                            attachmentPath: "",
                            html: htmlResponse.data.data,
                          };
                          var mailUrl = ServiceUrls.emailServices.emailApiWithAttachment;
                          sendMailServiceWithCallBack(mailUrl, emailPostData, function () {
                            sendMailServiceWithCallBack(emailApi, postData, function () {
                              self.isBooking = false;
                              self.mail = "";
                              self.name = "";
                              self.phone = "";
                              self.message = "";
                              $("#customFile").val("").trigger("change");
                              $("#customFile2").val("").trigger("change");
                              self.booking = false;

                              $(".custom-file-input").siblings(".custom-file-label").removeClass("selected").html("Choose Your Photo");
                              $(".custom-file-input2").siblings(".custom-file-label2").removeClass("selected").html("Choose Your Passport");

                              // document.getElementById("customFile").value = null;
                              // document.getElementById("customFile2").value = null;
                              alertify.alert(getValidationMsgByCode('M023'), getValidationMsgByCode('M022'), function () {
                                window.location.reload();
                              });
                            });
                          });
                        })
                        .catch(function (error) {
                          console.log(error);
                        });
                    });
                  } catch (e) {
                    console.log(e);
                  }
                }
              })
              .catch(function (updateError) {
                self.isLoading = false;
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M045')).set('closable', false);
                console.error(updateError);
              });
          })
          .catch(function (error) {
            self.isLoading = false;
            console.error(error);
            alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M045')).set('closable', false);
          });
      }
    },
    cmsFormUpdateData: async function (data, id) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/cms/data/" + id;
      var callMethod = "PUT";
      if (data != null) {
        data = JSON.stringify(data);
      }
      try {
        let formPostRes = await axios({
          url: url,
          method: callMethod,
          headers: {
            'Content-Type': 'application/json',
          },
          data: data
        })
        return formPostRes;
      } catch (error) {
        console.log(error);
        return error.response.data.code;
      }
    },
    cmsRequestData: function (callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      return fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
        },
        body: data, // body data type must match "Content-Type" header
      }).then(function (response) {
        return response.json();
      });
    },
    handleFilePhoto() {
      var self = this;
      file_temp = self.$refs.file.files[0];
      var ext = file_temp.name.split('.')[1];
      if (ext == "jpg" || ext == "png" || ext == "PNG" || ext == "JPEG" || ext == "jpeg") {
        self.file = self.$refs.file.files[0];
        if (self.file !== undefined) {

          // var fileName = $(this).val().split("\\").pop();
          $(".custom-file-input").siblings(".custom-file-label").addClass("selected").html(self.file.name);


          self.submitPhoto()
        }
      } else {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M028')).set('closable', false);
        // $("#resume").val(null);
        self.file = null;
      }
      // console.log('>>>> 1st element in files array >>>> ', this.file);
    },
    submitPhoto() {
      var self = this;
      self.uploadimg = true;
      if (self.file != null && self.file != undefined && !jQuery.isEmptyObject(self.file)) {
        $.getJSON('credentials/AgencyCredentials.json', function (json) {
          let Password = json.file_upload_Spartan[0].Password;
          // var Password = "6Yhn7Ujm^";
          var LoggedUser = window.localStorage.getItem("AgencyCode");
          // var LoggedUser = "AGY20483";
          // var UserName=LoggedUser.loginNode.code;	
          var encodedString = btoa(LoggedUser + ":" + Password);
          var formData = new FormData();
          formData.append('file', self.file);
          // console.log('>> formData >> ', formData);
          var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "visa"
          // You should have a server side REST API 
          axios.post(url,
              formData, {
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                  'Authorization': 'Basic ' + encodedString
                }
              }
            ).then(function (repo) {
              console.log('SUCCESS!!', repo.data.message);
              self.fileLink = repo.data.message;
              if (self.fileLink) {
                self.uploadimg = false;
              }
              // alertify.alert("Success", "Image Successfully uploaded.");
              self.file = null;
            })
            .catch(function () {
              self.uploadimg = false;
              console.log('FAILURE!!');
              self.fileLink = null;
            });
        });
      } else {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M051')).set('closable', false);
        // alertify.alert("warning", "please select the file")
      }
    },
    handleFilePassport() {
      var self = this;
      file_temp = self.$refs.file2.files[0];
      var ext = file_temp.name.split('.')[1];
      if (ext == "pdf" || ext == "docx" || ext == "doc" || ext == "jpg" || ext == "png" || ext == "PNG" || ext == "JPEG" || ext == "jpeg") {
        self.file2 = self.$refs.file2.files[0];
        if (self.file2) {
          // var fileName = $(this).val().split("\\").pop();
          $(".custom-file-input2").siblings(".custom-file-label2").addClass("selected").html(self.file2.name);
          self.submitPassport()
        }
      } else {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M028')).set('closable', false);
        // $("#resume").val(null);
        self.file2 = null;
      }
      // console.log('>>>> 1st element in files array >>>> ', this.file);
    },
    submitPassport() {
      var self = this;
      self.uploadflag = true;
      if (self.file2 != null && self.file2 != undefined && !jQuery.isEmptyObject(self.file2)) {
        $.getJSON('credentials/AgencyCredentials.json', function (json) {
          let Password = json.file_upload_Spartan[0].Password;
          // var Password = "6Yhn7Ujm^";
          var LoggedUser = window.localStorage.getItem("AgencyCode");
          // var LoggedUser = "AGY20483";
          // var UserName=LoggedUser.loginNode.code;	
          var encodedString = btoa(LoggedUser + ":" + Password);
          var formData = new FormData();
          formData.append('file', self.file2);
          // console.log('>> formData >> ', formData);
          var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "passport"
          // You should have a server side REST API 
          axios.post(url,
              formData, {
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                  'Authorization': 'Basic ' + encodedString
                }
              }
            ).then(function (repo) {
              console.log('SUCCESS!!', repo.data.message);
              self.fileLink2 = repo.data.message;
              if (self.fileLink2) {
                self.uploadflag = false;
              }
              // alertify.alert("Success", "Passport Successfully uploaded.Now Submit");
              self.file2 = null;
            })
            .catch(function () {
              self.uploadflag = false;
              console.log('FAILURE!!');
              self.fileLink2 = null;
            });
        });
      } else {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M051')).set('closable', false);
      }
    },
    handleFileId() {
      var self = this;
      file_temp = self.$refs.file3.files[0];
      var ext = file_temp.name.split('.')[1];
      if (ext == "jpg" || ext == "png" || ext == "PNG" || ext == "JPEG" || ext == "jpeg" || ext == "pdf" || ext == "docx" || ext == "doc") {
        self.file3 = self.$refs.file3.files[0];
        if (self.file !== undefined) {

          // var fileName = $(this).val().split("\\").pop();
          $(".custom-file-input").siblings(".custom-file-label").addClass("selected").html(self.file3.name);


          self.submitId()
        }
      } else {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M028')).set('closable', false);
        // $("#resume").val(null);
        self.file = null;
      }
      // console.log('>>>> 1st element in files array >>>> ', this.file);
    },
    submitId() {
      var self = this;
      self.uploadimg = true;
      if (self.file3 != null && self.file3 != undefined && !jQuery.isEmptyObject(self.file3)) {
        $.getJSON('credentials/AgencyCredentials.json', function (json) {
          let Password = json.file_upload_Spartan[0].Password;
          // var Password = "6Yhn7Ujm^";
          var LoggedUser = window.localStorage.getItem("AgencyCode");
          // var LoggedUser = "AGY20483";
          // var UserName=LoggedUser.loginNode.code;	
          var encodedString = btoa(LoggedUser + ":" + Password);
          var formData = new FormData();
          formData.append('file', self.file3);
          // console.log('>> formData >> ', formData);
          var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "Idcard"
          // You should have a server side REST API 
          axios.post(url,
              formData, {
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                  'Authorization': 'Basic ' + encodedString
                }
              }
            ).then(function (repo) {
              console.log('SUCCESS!!', repo.data.message);
              self.fileLink3 = repo.data.message;
              if (self.fileLink3) {
                self.uploadimg = false;
              }
              // alertify.alert("Success", "Image Successfully uploaded.");
              self.file3 = null;
            })
            .catch(function () {
              self.uploadimg = false;
              console.log('FAILURE!!');
              self.fileLink3 = null;
            });
        });
      } else {
        alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M051')).set('closable', false);
      }
    },
    dropdownChange: function (event, type) {
      if (event != undefined && event.target != undefined && event.target.value != undefined) {

        if (type == 'VisaType') {
          this.visaType = event.target.value;

        }

      }
    }
  },

  mounted: function () {
    this.getPagecontent();
    sessionStorage.active_e = 5;
    var vm = this;
    this.$nextTick(function () {
      $('#Visaselect').on("change", function (e) {
        vm.dropdownChange(e, 'VisaType')
      });
    })
  },
})