const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var maininstance = new Vue({
    i18n,
    el: '#transfer-results',
    name: 'transfer-results',
    data: {
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'AED',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        Packages: [],
        cityList: [],
        PackagesFull: [],
        resultFrom: [],
        resultTo: [],

        cityName: '',
        transferDate: '',
        returnDate:'',
        fromL: '',
        toL: '',
        pickUpH: '',
        pickUpM: '',
        guest: '',
        numberPassenger: 6,
        pageLoad: true,
        bannerSection:{},
        labels:{},
        commonLabels:{},
        isReturn:false,
        fromList:[],
        toList:[],
        PackagesList:[]
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var language = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var transferUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/List Of Transfers/List Of Transfers/List Of Transfers.ftl';
                var cmsUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Transfer Result Page/Transfer Result Page/Transfer Result Page.ftl';
                var labelUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Home Page/Home Page/Home Page.ftl';
                var searchData = self.getSearchData();

                if (searchData != "" || searchData != null) {
                    // searchData.city = searchData.city.split('-').join(' ');
                    searchData.from = searchData.from.split('-').join(' ');
                    searchData.to = searchData.to.split('-').join(' ');
                    searchData.guest = searchData.guest.trim();

                    self.cityName = searchData.city;

                    // self.fromL = searchData.from;
                    // self.toL = searchData.to;

                    self.pickUpH = searchData.hour;
                    self.pickUpM = searchData.min;
                    self.transferDate = searchData.date;
                    self.returnDate = searchData.rdate;
                    self.guest = searchData.guest;

                    axios.get(transferUrl, {
                        headers: {
                            'content-type': 'text/html',
                            'Accept': 'text/html',
                            'Accept-Language': language
                        }
                    }).then(function (res) {
                        var PackagesTemp = [];
                        var packageData = res.data;
                        self.PackagesFull = packageData.Values;
                        self.PackagesList =res.data.Values;
                        self.fromTo();
                        self.fromL = searchData.from;
                        self.toL = searchData.to;
                        PackagesTemp = packageData.Values;

                        if(self.returnDate){
                            self.isReturn = true;
                        }
                        $("#from2").val(self.transferDate);
                        $("#to2").val(self.returnDate);

                        PackagesTemp = PackagesTemp.filter(function (x) {
                            return x.Destination_From.toLowerCase().trim() == self.fromL.toLowerCase().trim() &&
                                x.Destination_To.toLowerCase().trim() == self.toL.toLowerCase().trim() &&
                                x._Max_Number_of_Passengers.trim() >= Number(self.guest.trim());
                        });
                        // self.Packages = PackagesTemp;
                        self.Packages = _.sortBy(PackagesTemp, [function (o) {
                            return Number(o.Price);
                        }]);
                        if (!self.Packages.length) {
                            self.pageLoad = false;
                        } else {
                            self.pageLoad = true;
                        }
                        self.getlocationList();
                    }).catch(function (error) {
                        console.log('Error', error);
                        self.content = [];
                        self.pageLoad = false;
                    });
                }

                axios.get(cmsUrl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': language
                    }
                }).then(function (res) {
                    console.log(res);
                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var banner = pluck('Banner_Section', self.content);
                        self.bannerSection = getAllMapData(banner[0].component);
                        var main = pluck('Main_Section', self.content);
                        self.labels = getAllMapData(main[0].component);
                    }

                }).catch(function (error) {
                    console.log('Error', error);
                });

                axios.get(labelUrl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': language
                    }
                }).then(function (res) {
                    console.log(res);
                    if (res.data.area_List.length) {
                        self.content = res.data.area_List;
                        var main = pluck('Index_Section', self.content);
                        self.commonLabels = getAllMapData(main[0].component);
                        if (self.mainSection) {
                            $('#city').prepend('<option value="" selected disabled hidden>'+self.mainSection.City_Label+'</option>');
                            $('#to').prepend('<option value="" selected disabled hidden>'+self.mainSection.Drop_Off_Location_Label+'</option>');
                            $('#from').prepend('<option value="" selected disabled hidden>'+self.mainSection.Pick_Up_Location_Label+'</option>');
                            $('#pickupmin').prepend('<option value="" selected disabled hidden>'+self.mainSection.Pickup_Time_M_Label+'</option>');
                            $('#pickuphour').prepend('<option value="" selected disabled hidden>'+self.mainSection.Pickup_Time_H_Label+'</option>');
                        }
                    }
                }).catch(function (error) {
                    console.log('Error', error);
                    // self.content = [];
                });
            });
        },
        getlocationList:function(){
            var self = this;
            var tempFromList = [];
            var tempToList = [];
            self.PackagesList.forEach(function (item, index) {
                if(item.Destination_From){
                    tempFromList.push(item.Destination_From.toUpperCase().trim());
                }
                self.fromList = _.uniq(_.sortBy(tempFromList));
                if(item.Destination_To){
                    tempToList.push(item.Destination_To.toUpperCase().trim());
                }
                self.toList = _.uniq(_.sortBy(tempToList));
            });

        },
            getmoreinfo(url) {
            var customSearch =
                "&date=" + this.transferDate +
                "&rdate=" + this.returnDate +
                "&hour=" + ('0' + (this.pickUpH - 1)).slice(-2) +
                "&min=" + ('0' + (this.pickUpM - 1)).slice(-2) +
                "&guest=" + this.guest +
                // "&city=" + this.cityName.split(' ').join('-') +
                "&from=" + this.fromL.split(' ').join('-') +
                "&to=" + this.toL.split(' ').join('-') + "&";

            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/WTAD/transfer-booking.html?page=" + url + customSearch;
                    window.location.href = url;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;
        },
        getSearchData: function () {
            var urldata = this.getUrlVars();
            return urldata;
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            let decodeUrl= decodeURIComponent(window.location.href);
            var hashes = decodeUrl.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        dropdownChange: function (event, type) {
            var self = this;
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                if (type == 'cityD') {
                    self.cityName = event.target.value;
                    // $('#tFrom').val('');
                    // $('#tTo').val('');
                    self.fromTo();
                } else if (type == 'fromD') {
                    self.fromL = event.target.value;
                } else if (type == 'toD') {
                    self.toL = event.target.value;
                } else if (type == 'guestIdD') {
                    self.guest = event.target.value;
                } else if (type == 'from2D') {
                    self.transferDate = event.target.value;
                } else if (type == 'to2D') {
                    self.returnDate = event.target.value;
                }
                else if (type == 'pickuphourD') {
                    self.pickUpH = event.target.value;
                } else if (type == 'pickupminD') {
                    self.pickUpM = event.target.value;
                }
            }
        },
        modifySearch: function () {
            var self = this;
            self.setCalender();

            // if (self.cityName == undefined || self.cityName == '' || self.cityName == null) {
            //     alertify.alert(getValidationMsgByCode('M011'), getValidationMsgByCode('M035')).set('closable', false);
            //     return false;
            // } 
            if (self.transferDate == undefined || self.transferDate == '' || self.transferDate == null) {
                alertify.alert(getValidationMsgByCode('M012'), getValidationMsgByCode('M035')).set('closable', false);
                return false;
            } 
            else if(self.isReturn == true && (self.returnDate == undefined || self.returnDate == '' || self.returnDate == null)){
                    alertify.alert(getValidationMsgByCode('M012'), getValidationMsgByCode('M004')).set('closable', false);
                    return false;
            }
            else if (self.fromL == undefined || self.fromL == '' || self.fromL == null) {
                alertify.alert(getValidationMsgByCode('M013'), getValidationMsgByCode('M035')).set('closable', false);
                return false;
            } else if (self.toL == undefined || self.toL == '' || self.toL == null) {
                alertify.alert(getValidationMsgByCode('M014'), getValidationMsgByCode('M035')).set('closable', false);
                return false;
            } else if (self.guest == undefined || self.guest == '' || self.guest == null) {
                alertify.alert(getValidationMsgByCode('M047'), getValidationMsgByCode('M035')).set('closable', false);
                return false;
            } else if (self.pickUpH == undefined || self.pickUpH == '' || self.pickUpH == null) {
                alertify.alert(getValidationMsgByCode('M015'), getValidationMsgByCode('M035')).set('closable', false);
                return false;
            } else if (self.pickUpM == undefined || self.pickUpM == '' || self.pickUpM == null) {
                alertify.alert(getValidationMsgByCode('M016'), getValidationMsgByCode('M035')).set('closable', false);
                return false;
            } else if (self.fromL == self.toL) {
                alertify.alert(getValidationMsgByCode('M048'), getValidationMsgByCode('M035')).set('closable', false);
                return false;
            } else {
                var PackagesTemp = [];
                PackagesTemp = self.PackagesFull;
                PackagesTemp = PackagesTemp.filter(function (x) {
                    return x.Destination_From.toLowerCase().trim() == self.fromL.toLowerCase().trim() &&
                        x.Destination_To.toLowerCase().trim() == self.toL.toLowerCase().trim() &&
                        x._Max_Number_of_Passengers.trim() >= Number(self.guest.trim());
                });
                self.$nextTick(function () {
                    // self.Packages = PackagesTemp;
                    self.Packages = _.sortBy(PackagesTemp, [function (o) {
                        return Number(o.Price);
                    }]);
                    if (!self.Packages.length) {
                        self.pageLoad = false;
                    } else {
                        self.pageLoad = true;
                    }
                });
            }
            var searchCriteria =
            "&date=" + this.transferDate +
            "&rdate=" + (this.isReturn == true ? this.returnDate :'') +
            "&hour=" + this.pickUpH +
            "&min=" + this.pickUpM +
            "&from=" + this.fromL +
            "&to=" + this.toL +
            "&guest=" + this.guest + "&";
            searchCriteria = searchCriteria.split(' ').join('-');
            var uri = "/WTAD/transfer-results.html?" + searchCriteria;
            window.location.href = uri;
        },
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#from2").datepicker({
                minDate: new Date('11/22/2022'),
                maxDate: new Date('11/28/2022'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
            });
            $("#to2").datepicker({
                minDate: new Date('11/22/2022'),
                maxDate: new Date('11/28/2022'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $('#from2').datepicker('getDate', '+1d');
                    selectedDate.setDate(selectedDate.getDate() + 1);
                    $("#to2").datepicker("option", "minDate", selectedDate);
                }
            });
        },
        fromTo: function () {
            var self = this;
            // $('#from').val('');
            // $('#to').val('');

            $('#tFrom').val('').trigger('change');
            $('#tTo').val('').trigger('change');
            self.resultFrom = [];
            self.resultTo = [];
            self.cityList = [];
            self.fromL = '';
            self.toL = '';

            var fromTemp = [];
            var toTemp = [];
            var maxPax = [];
            var cityFull = [];
            var filteredListFromToTemp = [];
            self.PackagesFull.forEach(function (item, index) {
                // cityFull.push(item.City.toUpperCase().trim());
                // maxPax.push(item._Max_Number_of_Passengers.trim());
                if (!isNaN(item._Max_Number_of_Passengers.trim())) {
                    maxPax.push(Number((item._Max_Number_of_Passengers.trim())));
                }

            });
            // filteredListFromToTemp = self.PackagesFull.filter(Transfer => (Transfer.City.toLowerCase()).trim() == (self.cityName.toLowerCase()).trim());
            filteredListFromToTemp.forEach(function (item, index) {
                fromTemp.push(item.Destination_From.toUpperCase().trim());
                toTemp.push(item.Destination_To.toUpperCase().trim());
            });
            self.numberPassenger = _.max(maxPax);
            // self.resultFrom = fromTemp;
            // self.resultTo = toTemp;


            self.$nextTick(function () {
                // self.cityList = _.uniq(_.sortBy(cityFull));
                self.resultFrom = _.uniq(_.sortBy(fromTemp));
                self.resultTo = _.uniq(_.sortBy(toTemp));
            });
        },
    },
    mounted: function () {
        var vm = this;
        var historyPage = localStorage.getItem("backUrl") ? localStorage.getItem("backUrl") : null;
        if (historyPage) {
            localStorage.removeItem("backUrl");
            window.location.href = historyPage;
        }

        vm.getPagecontent();
        vm.setCalender();
        sessionStorage.active_e = 3;
        // vm.$nextTick(function () {
        $('#city').on("change", function (e) {
            vm.dropdownChange(e, 'cityD');
        });
        $('#tFrom').on("change", function (e) {
            vm.dropdownChange(e, 'fromD')
        });
        $('#tTo').on("change", function (e) {
            vm.dropdownChange(e, 'toD')
        });
        $('#pickuphour').on("change", function (e) {
            vm.dropdownChange(e, 'pickuphourD')
        });
        $('#pickupmin').on("change", function (e) {
            vm.dropdownChange(e, 'pickupminD')
        });
        $('#guestId').on("change", function (e) {
            vm.dropdownChange(e, 'guestIdD')
        });
        $('#from2').on("change", function (e) {
            vm.dropdownChange(e, 'from2D')
            // });
        })
        $('#to2').on("change", function (e) {
            vm.dropdownChange(e, 'to2D')
        })
    },
    computed: {
        pickUp: function (){
            // if (this.to != "" && (this.to||"").toLowerCase().includes("airport")) {
            //     return this.fromList.filter(function(e) {return !e.toLowerCase().includes("airport"); })
            // }
            return this.fromList;
        },
        dropOff: function () {
            if (this.fromL != "" && (this.fromL||"").toLowerCase().includes("airport")) {
                return this.toList.filter(function(e) {return !e.toLowerCase().includes("airport"); })
            } else {
                return this.toList.filter(function(e) {return e.toLowerCase().includes("airport"); })
            }
        }
    },
    watch: {
        fromL: function(){
            setTimeout(() => {
                initSelect2();
            }, 50);
        },
        isReturn(newV, oldV){
            if(newV == false){
                this.returnDate=""
                $('#to2').datepicker('setDate', null);
            }
        }
    }
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
jQuery(document).ready(function ($) {
    initSelect2();
});
function initSelect2() {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
}