const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var maininstance = new Vue({
    i18n,
    el: '#transfer-booking',
    name: 'transfer-booking',
    data: {
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        content: {
            area_List: []
        },
        Transfer: {
            Description_Title: null,
            Destination_From: null
        },
        Traveler_Details: {
            Terms_and_Conditions: null
        },
        pageURLLink: null,
        travellerInfo: {
            firstName: null,
            lastName: null,
            email: null,
            contactNumber: null,
            flightNumber: null,
            flightDateTime: null,
            airport: null,
            hotelName: "NA"
        },
        siteData: null,
        searchCriteria: {},
        checked: false,
        isBooking: false,
        labels:{},
        banner:{}
        // historyData: '',
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                packageurl = getQueryStringValue('page');
                if (packageurl != "") {
                    packageurl = packageurl.split('-').join(' ');
                    packageurl = packageurl.split('_')[0];
                    self.pageURLLink = packageurl;
                    var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/' + packageurl + '.ftl';
                    var siteData = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Transfer Booking Page/Transfer Booking Page/Transfer Booking Page.ftl';
                    var searchData = self.getSearchData();
                    // self.historyData = searchData;
                    if (searchData != "" || searchData != null) {
                        searchData.hour = searchData.hour.trim();
                        searchData.date = searchData.date.trim();
                        searchData.rdate = searchData.rdate.trim();
                        searchData.min = searchData.min.trim();
                        searchData.guest = searchData.guest.trim();
                        self.searchCriteria = Object.assign({}, searchData);
                        localStorage.setItem("backUrl", self.yourCallBackFunction(self.searchCriteria));
                        axios.get(topackageurl, {
                            headers: {
                                'content-type': 'text/html',
                                'Accept': 'text/html',
                                'Accept-Language': langauage
                            }
                        }).then(function (response) {
                            self.content = response.data;
                            var pagecontent = self.pluck('Transfers_Details_Section', self.content.area_List);
                            if (pagecontent.length > 0) {
                                self.Transfer.Destination_From = self.pluckcom('Destination_From', pagecontent[0].component);
                                self.Transfer.Destination_To = self.pluckcom('Destination_To', pagecontent[0].component);
                                self.Transfer.Vehicle_Name = self.pluckcom('Vehicle_Name', pagecontent[0].component);
                                self.Transfer.Description = self.pluckcom('Description', pagecontent[0].component);
                                self.Transfer.Cancellation_Policy = self.pluckcom('Cancellation_Policy', pagecontent[0].component);
                                self.Transfer.Availability_Status = self.pluckcom('Availability_Status', pagecontent[0].component);
                                self.Transfer.Price = self.pluckcom('Price', pagecontent[0].component);
                                self.Transfer.Extra_Hour_Rate = self.pluckcom('Extra_Hour_Rate', pagecontent[0].component);
                                self.Transfer.Full_Day_Rent = self.pluckcom('Full_Day_Rent', pagecontent[0].component);
                                self.Transfer.City = self.pluckcom('City', pagecontent[0].component);
                                self.Transfer.Max_Number_of_Passengers = self.pluckcom('_Max_Number_of_Passengers', pagecontent[0].component);
                                self.Transfer.Rate_Basis = self.pluckcom('Rate_Basis', pagecontent[0].component);
                                self.Transfer.Vehicle_Image = self.pluckcom('Vehicle_Image', pagecontent[0].component);

                            }
                        }).catch(function (error) {
                            console.log("2", error)
                            window.location.href = "/WTAD/transfer-booking.html";
                            self.content = [];
                        });
                    }
                    axios.get(siteData, {
                        headers: {
                            'content-type': 'text/html',
                            'Accept': 'text/html',
                            'Accept-Language': langauage
                        }
                    }).then(function (res) {
                        self.siteData = res.data;
                        if (siteData.length > 0) {
                        var main = pluck('Main_Section', self.siteData.area_List);
                        self.labels = getAllMapData(main[0].component);
                        var bannerDetails=pluck('Banner_Section',self.siteData.area_List);
                        self.banner=getAllMapData(bannerDetails[0].component);
                        }
                        // var siteData = self.pluck('Transfer_Details_Section', self.siteData.area_List);
                        // if (siteData.length > 0) {
                        //     self.Transfer.Description_Title = self.pluckcom('Description_Title', siteData[0].component);
                        //     self.Transfer.Cancellation_Policy_Title = self.pluckcom('Cancellation_Policy_Title', siteData[0].component);
                        // }
                        // var Traveler = self.pluck('Traveler_Details_Section', self.siteData.area_List);
                        // if (Traveler.length > 0) {
                        //     self.Traveler_Details.Terms_and_Conditions = self.pluckcom('Terms_and_Conditions', Traveler[0].component);
                        //     self.Traveler_Details.Description = self.pluckcom('Description', Traveler[0].component);
                        // }

                    }).catch(function (error) {
                        console.log("1", error)
                        window.location.href = "/WTAD/transfer-booking.html";
                        self.siteData = [];
                    });


                }
            });
        },
        getTemplate(template) {
            // var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
            var url = ServiceUrls.emailServices.getTemplate + "AGY75/" + template;
            // var url = ServiceUrls.emailServices.getTemplate + LoggedUser.loginNode.code + "/" + template;
            return axios.get(url);
        },
        bookingDeatils: function () {
            var self = this;
            if (!this.travellerInfo.firstName) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M017')).set('closable', false);
                return false;
            }
            if (!this.travellerInfo.lastName) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M018')).set('closable', false);
            }
            if (this.travellerInfo.email == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M009')).set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.travellerInfo.email.match(emailPat);
            if (matchArray == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M019')).set('closable', false);
                return false;
            }
            if (this.travellerInfo.contactNumber == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M020')).set('closable', false);
                return false;
            }
            if (!this.travellerInfo.contactNumber) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M020')).set('closable', false);
                return false;
            }
            if (this.travellerInfo.contactNumber.length < 8) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M046')).set('closable', false);
                return false;
            }
            if (this.checked == false) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M021')).set('closable', false);
                return false;
            } else {
                this.isBooking = true;
                var user = JSON.parse(localStorage.User);
                var fromEmail = user.loginNode.parentEmailId ? user.loginNode.parentEmailId : (user.loginNode.email ? user.loginNode.email : 'uaecrt@gmail.com') || 'uaecrt@gmail.com';
                var toEmail = user.loginNode.parentEmailId;
                var ccEmails = _.filter(user.loginNode.emailList, function (o) {
                    return o.emailTypeId == 3 && o.emailType.toLowerCase() == 'support';
                });
                ccEmails = _.map(ccEmails, function (o) {
                    return o.emailId
                });
                var agency = localStorage.getItem("AgencyCode");
                // var referenceNumber = agency + '-TFR' + moment(new Date()).format("YYMMDDhhmmssSSS");
                var referenceNumber = agency + '-';


                let agencyCode = JSON.parse(localStorage.User).loginNode.code;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                var bookDate = moment(self.searchCriteria.date, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
                if(self.searchCriteria.rdate){
                var returnDate = moment(self.searchCriteria.rdate, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
                 }
                var sendingRequest = {
                    type: "Transfer Booking",
                    text23: self.travellerInfo.firstName + " " + self.travellerInfo.lastName,
                    text10: self.Transfer.Destination_From,
                    text11: self.Transfer.Destination_To,
                    text12: self.searchCriteria.date,
                    text13: self.searchCriteria.rdate ? self.searchCriteria.rdate : "NA",
                    text14: self.searchCriteria.hour + ":" + self.searchCriteria.min,
                    text15: self.Transfer.Vehicle_Name,
                    text17: self.travellerInfo.email,
                    text18: self.travellerInfo.contactNumber,
                    text19: self.travellerInfo.flightNumber,
                    text20: self.travellerInfo.flightDateTime,
                    text21: self.travellerInfo.airport,
                    text22: self.travellerInfo.hotelName,
                    date1: bookDate,
                    text16: self.searchCriteria.guest,
                    amount1: (self.searchCriteria.rdate = undefined )? self.Transfer.Price :(self.Transfer.Price * 2),
                    nodeCode: agencyCode
                };
                
                self.cmsRequestData("POST", "cms/data", sendingRequest, null).then(function (response) {
                    let insertID = Number(response);
                    referenceNumber = referenceNumber + insertID;
                    var emailApi = ServiceUrls.emailServices.emailApi;
                    var updateDate = {
                        keyword10: referenceNumber
                    }
                    sendingRequest.cmsId = insertID;
                    sendingRequest.packegeUrl = self.pageURLLink;
                    sendingRequest.referenceNumber = referenceNumber;
                    window.sessionStorage.setItem("TransferDetails", JSON.stringify(sendingRequest));
                    self.cmsFormUpdateData(updateDate, insertID).then(function (update) {
                        if (update.status == 200) {
                            console.log('Success:' + update.status);
                            window.sessionStorage.setItem("emailStatusTransfer", false);
                            if (referenceNumber && insertID) {
                                self.paymentFunction((referenceNumber));
                                // window.location.href = window.location.origin + "/WTAD/hotelconfirmation.html"
                            }
                            console.log('Success:' + update.status);
                        } else {
                            alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M21')).set('closable', false);
                        }
                    }).catch(function (updateError) {
                        self.isLoading = false;
                        alertify.alert(getValidationMsgByCode('M023'), getValidationMsgByCode('M045')).set('closable', false);
                        console.error(updateError);
                    });
                }).catch(function (error) {
                    self.isLoading = false;
                    console.error(error);
                    alertify.alert(getValidationMsgByCode('M023'), getValidationMsgByCode('M045')).set('closable', false);
                });
            }
        },
        cmsRequestData: function (callMethod, urlParam, data, headerVal) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            return fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json'
                },
                body: data, // body data type must match "Content-Type" header
            }).then(function (response) {
                return response.json();
            });
        },
        cmsFormUpdateData: async function (data, id) {
            var huburl = ServiceUrls.hubConnection.cmsUrl;
            var portno = ServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/cms/data/" + id;
            var callMethod = "PUT";
            if (data != null) {
                data = JSON.stringify(data);
            }
            try {
                let formPostRes = await axios({
                    url: url,
                    method: callMethod,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    data: data
                })
                return formPostRes;
            } catch (error) {
                console.log(error);
                return error.response.data.code;
            }
        },
        getSearchData: function () {
            var urldata = this.getUrlVars();
            // urldata = decodeURIComponent(urldata);
            return urldata;
        },
        paymentFunction: function (RefNumber) {
            try {
                var paymentDetails = {
                    "totalAmount": -188.88,
                    "customerEmail": "test@test.com",
                    "bookingReference": "",
                    "cartID": ""
                };
                var user = JSON.parse(localStorage.User);
                var returnURL = window.location.origin + "/WTAD/transferconfirmation.html"
                paymentDetails.bookingReference = RefNumber;
                // paymentDetails.totalAmount = this.$n((this.total / this.CurrencyMultiplier), 'currency', this.selectedCurrency).replace(/[^\d.-]/g, "");
                paymentDetails.totalAmount = (this.searchCriteria.rdate = undefined )? this.Transfer.Price :(this.Transfer.Price * 2);
                paymentDetails.cartID = RefNumber;
                paymentDetails.currency = user.loginNode.currency || 'AED';
                paymentDetails.currentPayGateways = user.loginNode.paymentGateways;
                paymentDetails.customerEmail = this.travellerInfo.email;
                var awsApi = true
                var paymentForm = paymentManager(paymentDetails, returnURL, window, awsApi);
                return paymentForm;

            } catch (error) {
                console.log(error)
            }
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            let decodeUrl= decodeURIComponent(window.location.href);
            var hashes = decodeUrl.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        yourCallBackFunction: function (data) {
            if (data != null) {
                if (data != "") {
                    var customSearch =
                        "&date=" + data.date +
                        "&hour=" + (parseInt(data.hour) + 1) +
                        "&min=" + (parseInt(data.min) + 1) +
                        "&guest=" + data.guest +
                        "&city=" + data.city +
                        "&from=" + data.from +
                        "&to=" + data.to + "&";

                    url = "/WTAD/transfer-results.html?page=" + customSearch;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;
        },
        },    mounted: function () {
        localStorage.removeItem("backUrl");
        this.getPagecontent();
        sessionStorage.active_e = 3;
    },
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}