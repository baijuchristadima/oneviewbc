const i18n = new VueI18n({
  numberFormats,
  locale: "en", // set locale
  // set locale messages
});
var visa = new Vue({
  i18n,
  el: "#planmytrip",
  name: "planmytrip",
  data: {
    selectedCurrency: localStorage.selectedCurrency ? localStorage.selectedCurrency : "USD",
    CurrencyMultiplier: localStorage.CurrencyMultiplier ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
    guestName: "",
    flightDateFrom: "",
    flightDateTo: "",
    arrivalPlace: "",
    numberOfPax: "",
    numberOfBikes: "",
    departurePlace: "",
    flightNumber: "",
    flightTime: "",
    flightNumberDep: "",
    flightTimeDep: "",
    isBooking: false,
    LabelSection: {},
    email: "",
    phone: "",
  },
  methods: {
    getPagecontent: function () {
      var self = this;

      getAgencycode(function (response) {
        var Agencycode = response;
        var huburl = ServiceUrls.hubConnection.cmsUrl;
        var portno = ServiceUrls.hubConnection.ipAddress;
        var langauage = localStorage.Languagecode ? localStorage.Languagecode : "en";
        // self.dir = langauage == "ar" ? "rtl" : "ltr";
        var pageurl = huburl + portno + "/persons/source?path=/B2B/AdminPanel/CMS/" + Agencycode + "/Template/Plan My Trip/Plan My Trip/Plan My Trip.ftl";

        // axios
        //   .get(pageurl, {
        //     headers: {
        //       "content-type": "text/html",
        //       Accept: "text/html",
        //       "Accept-Language": langauage,
        //     },
        //   })
        //   .then(function (response) {
        //     if (response.data.area_List.length > 0) {
        //       var label = pluck("Page_Labels", response.data.area_List);
        //       self.LabelSection = getAllMapData(label[0].component);
        //       self.isLoading = false;
        //     }
            self.stopLoader();

          // })
          // .catch(function (error) {
          //   console.log("Error");
          //   self.stopLoader();

          //   self.aboutUsContent = [];
          // });
      });
    },
    sendPlanMyTrip: function () {
      var self = this;

      if (!this.guestName) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M032")).set("closable", false);
        return false;
      } else if (!this.email) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M009")).set("closable", false);
        return false;
      } else if (!this.phone) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M010")).set("closable", false);
        return false;
      } else if (!this.numberOfPax) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M062")).set("closable", false);
        return false;
      } else if (!this.numberOfBikes) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M063")).set("closable", false);
        return false;
      } else if (!this.flightDateFrom) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M060")).set("closable", false);
        return false;
      } else if (!this.arrivalPlace) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M061")).set("closable", false);
        return false;
      } else if (!this.flightTime) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M065")).set("closable", false);
        return false;
      } else if (!this.flightNumber) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M064")).set("closable", false);
        return false;
      } else if (!this.flightDateTo) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M067")).set("closable", false);
        return false;
      } else if (!this.departurePlace) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M066")).set("closable", false);
        return false;
      } else if (!this.flightTimeDep) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M068")).set("closable", false);
        return false;
      } else if (!this.flightNumberDep) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M069")).set("closable", false);
        return false;
      } 
      
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.email.match(emailPat);
      if (matchArray == null) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M019")).set("closable", false);
        return false;
      }
      if (this.phone == null) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M010")).set("closable", false);
        return false;
      }
      var phoneno = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
      var matchphone = this.phone.match(phoneno);
      if (matchphone == null) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M046")).set("closable", false);
        return false;
      }
      if (this.phone.length < 8) {
        alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M046")).set("closable", false);
        return false;
        // if (!this.message) {
        //   alertify.alert('Alert', 'Message required.').set('closable', false);
        //   return false;
        // }
      }
      else {
        this.isBooking = true;
        var user = JSON.parse(localStorage.User);
        var fromEmail = user.loginNode.parentEmailId ? user.loginNode.parentEmailId : (user.loginNode.email ? user.loginNode.email : "uaecrt@gmail.com") || "uaecrt@gmail.com";
        var toEmail = user.loginNode.parentEmailId;
        // var toEmail = JSON.parse(localStorage.User).emailId;
        var ccEmails = _.filter(user.loginNode.emailList, function (o) {
          return o.emailTypeId == 3 && o.emailType.toLowerCase() == "support";
        });
        ccEmails = _.map(ccEmails, function (o) {
          return o.emailId;
        });
        var agency = localStorage.getItem("AgencyCode");
        // var referenceNumber = agency + '-VIS' + moment(new Date()).format("YYMMDDhhmmssSSS");
        var referenceNumber = agency + "-";

        let agencyCode = JSON.parse(localStorage.User).loginNode.code;
        let requestedDate = moment(String(new Date())).format("YYYY-MM-DDThh:mm:ss");
        var sendingRequest = {
          type: "Transfer Details",
          text1: this.guestName,
          text2: moment(this.flightDateFrom, "DD/MM/YYYY").format("YYYY-MM-DD"),
          text3: this.arrivalPlace,
          text4: this.numberOfPax,
          text5: this.numberOfBikes,
          text6: this.flightNumber,
          text7: this.flightTime,
          text8: moment(this.flightDateTo, "DD/MM/YYYY").format("YYYY-MM-DD"),
          text9: this.departurePlace,
          text10: this.flightTimeDep,
          text11: this.flightNumberDep,
          text12: this.email,
          text13: $(".selected-dial-code").text() + this.phone,
          nodeCode: agencyCode,
        };
        this.cmsRequestData("POST", "cms/data", sendingRequest, null)
          .then(function (response) {
            let insertID = Number(response);
            referenceNumber = referenceNumber + insertID;
            var emailApi = ServiceUrls.emailServices.emailApi;
            var updateDate = {
              keyword15: referenceNumber,
            };
            self
              .cmsFormUpdateData(updateDate, insertID)
              .then(function (update) {
                if (update.status == 200) {
                  console.log("Success:" + update.status);
                  var emailDataAdmin = {
                    bookingReference: referenceNumber,
                    guestName: self.guestName,
                    email: self.email,
                    phone: $(".selected-dial-code").text() + self.phone,
                    flightDate: self.flightDateFrom,
                    arrivalPlace: self.arrivalPlace,
                    numberOfPax: self.numberOfPax,
                    numberOfBikes: self.numberOfBikes,
                    flightNumber: self.flightNumber,
                    flightTime: self.flightTime,
                    flightDateTo: self.flightDateTo,
                    departurePlace: self.departurePlace,
                    flightTimeDep: self.flightTimeDep,
                    flightNumberDep: self.flightNumberDep,
                  }

                  self.getTemplate("WtadAdminEmailNewPage").then(function (templateResponse) {
                    var data = templateResponse.data.data;
                    var emailTemplate = "";
                    if (data.length > 0) {
                      for (var x = 0; x < data.length; x++) {
                        if (data[x].enabled == true && data[x].type == "WtadAdminEmailNewPage") {
                          emailTemplate = data[x].content;
                          break;
                        }
                      }
                    }
                    var htmlGenerate = ServiceUrls.emailServices.htmlGenerate;
                    var emailData = {
                      template: emailTemplate,
                      content: emailDataAdmin,
                    };
                    axios
                      .post(htmlGenerate, emailData)
                      .then(function (htmlResponse) {
                        var emailPostData = {
                          type: "AttachmentRequest",
                          toEmails: Array.isArray(toEmail) ? toEmail : [toEmail],
                          fromEmail: fromEmail,
                          ccEmails: ccEmails,
                          bccEmails: null,
                          subject: "Transfer Details booking - " + referenceNumber,
                          attachmentPath: "",
                          html: htmlResponse.data.data,
                        };
                        var mailUrl = ServiceUrls.emailServices.emailApiWithAttachment;
                        sendMailServiceWithCallBack(mailUrl, emailPostData, function () {
                          self.guestName = "";
                          self.email = "";
                          self.phone = "";
                          self.flightDateFrom = "";
                          self.flightDateTo = "";
                          self.arrivalPlace = "";
                          self.departurePlace = "";
                          self.numberOfPax = "";
                          self.numberOfBikes = "";
                          self.flightNumber = "";
                          self.flightTime = "";
                          self.flightNumberDep = "";
                          self.flightTimeDep = "";
                          self.isBooking = false;
                          alertify.alert(getValidationMsgByCode("M023"), getValidationMsgByCode("M022"), function () {
                            window.location.reload();
                          });
                        //   try {
                        //     // let insertID = Number(responseObject);
                        //     var emailApi = ServiceUrls.emailServices.emailApi;
                        //     // sendMailService(emailApi, postData);
                        //     // sendMailService(emailApi, custmail);
        
                        //     var phoneNumber = JSON.parse(localStorage.User).loginNode.phoneList.filter(function (phones) {
                        //       return phones.type.toLowerCase().includes("telephone") || phones.type.toLowerCase().includes("mobile");
                        //     });
        
                        //     var emailContent = {
                        //       logoUrl: JSON.parse(localStorage.User).loginNode.logo,
                        //       bookingLabel: "Booking ID",
                        //       bookingReference: referenceNumber,
                        //       agencyEmail: fromEmail,
                        //       title: "Inquiry Received for New Page",
                        //       personName: self.guestName,
                        //       servicesData: [
                        //         {
                        //           name: "Lead Guest Name",
                        //           value: self.guestName,
                        //         },
                        //         {
                        //           name: "Request Date",
                        //           value: moment(new Date()).format("YYYY-MM-DD"),
                        //         },
                        //       ],
                        //       emailMessage: "<p>Your inquiry is successfully received and is under booking ID&nbsp;" + referenceNumber + ".</p><p>One of our agent will get back to you shortly.</p>",
                        //       agencyPhone: phoneNumber[0].nodeContactNumber.number ? "+" + JSON.parse(localStorage.User).loginNode.country.telephonecode + "" + phoneNumber[0].nodeContactNumber.number : "NA",
                        //     };
        
                        //     self.getTemplate("SpartanEmailTemplate").then(function (templateResponse) {
                        //       var data = templateResponse.data.data;
                        //       var emailTemplate = "";
                        //       if (data.length > 0) {
                        //         for (var x = 0; x < data.length; x++) {
                        //           if (data[x].enabled == true && data[x].type == "SpartanEmailTemplate") {
                        //             emailTemplate = data[x].content;
                        //             break;
                        //           }
                        //         }
                        //       }
                        //       var htmlGenerate = ServiceUrls.emailServices.htmlGenerate;
                        //       var emailData = {
                        //         template: emailTemplate,
                        //         content: emailContent,
                        //       };
                        //       axios
                        //         .post(htmlGenerate, emailData)
                        //         .then(function (htmlResponse) {
                        //           var emailPostData = {
                        //             type: "AttachmentRequest",
                        //             toEmails: Array.isArray(self.email) ? self.email : [self.email],
                        //             fromEmail: fromEmail,
                        //             ccEmails: null,
                        //             bccEmails: null,
                        //             subject: "Booking Confirmation - " + referenceNumber,
                        //             attachmentPath: "",
                        //             html: htmlResponse.data.data,
                        //           };
                        //           var mailUrl = ServiceUrls.emailServices.emailApiWithAttachment;
                        //           sendMailServiceWithCallBack(mailUrl, emailPostData, function () {
                        //             self.guestName = "";
                        //             self.flightDate = "";
                        //             self.arrivalPlace = "";
                        //             self.numberOfPax = "";
                        //             self.numberOfBikes = "";
                        //             self.flightNumber = "";
                        //             self.flightTime = "";
                        //             self.isBooking = false;
                        //             alertify.alert(getValidationMsgByCode("M023"), getValidationMsgByCode("M022"), function () {
                        //               window.location.reload();
                        //             });
                        //           });
                        //         })
                        //         .catch(function (error) {
                        //           console.log(error);
                        //         });
                        //     });
                        //   } catch (e) {
                        //     console.log(e);
                        //   }
                        });
                      })
                      .catch(function (error) {
                        console.log(error);
                      });
                  });
                }
              })
              .catch(function (updateError) {
                self.isLoading = false;
                alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M045")).set("closable", false);
                console.error(updateError);
              });
          })
          .catch(function (error) {
            self.isLoading = false;
            console.error(error);
            alertify.alert(getValidationMsgByCode("M035"), getValidationMsgByCode("M045")).set("closable", false);
          });
      }
    },
    stopLoader: function () {
        $('#preloader').delay(50).fadeOut(250);
    },
    getTemplate(template) {
      // var LoggedUser = JSON.parse(window.localStorage.getItem("User"));
      var url = ServiceUrls.emailServices.getTemplate + "AGY75/" + template;
      // var url = ServiceUrls.emailServices.getTemplate + LoggedUser.loginNode.code + "/" + template;
      return axios.get(url);
    },
    cmsFormUpdateData: async function (data, id) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/cms/data/" + id;
      var callMethod = "PUT";
      if (data != null) {
        data = JSON.stringify(data);
      }
      try {
        let formPostRes = await axios({
          url: url,
          method: callMethod,
          headers: {
            "Content-Type": "application/json",
          },
          data: data,
        });
        return formPostRes;
      } catch (error) {
        console.log(error);
        return error.response.data.code;
      }
    },
    cmsRequestData: function (callMethod, urlParam, data, headerVal) {
      var huburl = ServiceUrls.hubConnection.cmsUrl;
      var portno = ServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      return fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          "Content-Type": "application/json",
        },
        body: data, // body data type must match "Content-Type" header
      }).then(function (response) {
        return response.json();
      });
    },
    resetValues() {
    },
    dropdownChange: function (event, type) {
      if (event != undefined && event.target != undefined && event.target.value != undefined) {
        if (type == "flightfrom") {
          this.flightDateFrom = event.target.value;
        }
        if (type == "flightto") {
          this.flightDateTo = event.target.value;
        }
      }
    },
    setCalender() {
      var dateFormat = "dd/mm/yy";
      $("#flightfrom, #flightto").datepicker({
        minDate: new Date("10/29/2021"),
        maxDate: new Date("11/11/2021"),
        numberOfMonths: 1,
        changeMonth: true,
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>",
        duration: "fast",
        showAnim: "slide",
        showOptions: {
          direction: "up",
        },
        showButtonPanel: false,
        dateFormat: dateFormat,
      });
      $("#check_9").click(function() { return false; });
    },
  },

  mounted: function () {
    this.getPagecontent();
    this.setCalender();
    sessionStorage.active_e = 15;
    var vm = this;
    this.$nextTick(function () {
      $("#flightfrom").on("change", function (e) {
        vm.dropdownChange(e, "flightfrom");
      });
      $("#flightto").on("change", function (e) {
        vm.dropdownChange(e, "flightto");
      });
    });
  },
});

$("#phone").intlTelInput({
  initialCountry: JSON.parse(localStorage.User).loginNode.country.code || "AE",
  geoIpLookup: function (callback) {
      $.get('https://ipinfo.io', function () { }, "jsonp").always(function (resp) {
          var countryCode = (resp && resp.country) ? resp.country : "";
          (countryCode);
      });
  },
  separateDialCode: true,
  autoPlaceholder: "off",
  preferredCountries: ['ae', 'sa', 'om', 'qa'],
  utilsScript: "../assets/js/intlTelInput.js?v=201903150000" // just for formatting/placeholders etc
});