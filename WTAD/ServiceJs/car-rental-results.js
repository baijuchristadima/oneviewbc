const i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    // set locale messages
})
var maininstance = new Vue({
    i18n,
    el: '#car-rental-results',
    name: 'car-rental-results',
    data: {
        selectedCurrency: (localStorage.selectedCurrency) ? localStorage.selectedCurrency : 'USD',
        CurrencyMultiplier: (localStorage.CurrencyMultiplier) ? parseFloat(localStorage.CurrencyMultiplier) : parseFloat(1),
        Packages: [],
        cityList: [],
        PackagesFull: [],
        resultFrom: [],
        resultTo: [],
        content: [],
        cityName: '',
        carRentDate: '',
        fromL: '',
        toL: '',
        pickUpH: '',
        pickUpM: '',
        noOfDays: '',
        headerArea: {},
        MainData: [],
        noOfDaysMultiplier: 1,
        // numberPassenger: 6,
        pageLoad: true,
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagedata() {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var homecms = '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Template/Car Rental Result Page/Car Rental Result Page/Car Rental Result Page.ftl';
                var cmsurl = huburl + portno + homecms;
                axios.get(cmsurl, {
                    headers: {
                        'content-type': 'text/html',
                        'Accept': 'text/html',
                        'Accept-Language': langauage
                    }
                }).then(function (response) {
                    self.content = response.data;
                    var headerSection = pluck('Banner_Section', response.data.area_List);
                    self.headerArea = getAllMapData(headerSection[0].component);
                    var Main = pluck('Main_Section', response.data.area_List);
                    self.MainData = getAllMapData(Main[0].component);
                }).catch(function (error) {
                    console.log('Error', error);
                    this.content = [];
                });

            });
        },
        getPagecontent: function () {
            var self = this;
            getAgencycode(function (response) {
                var Agencycode = response;
                var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                var huburl = ServiceUrls.hubConnection.cmsUrl;
                var portno = ServiceUrls.hubConnection.ipAddress;
                var carRentUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + Agencycode + '/Master Table/List Of Car Rental/List Of Car Rental/List Of Car Rental.ftl';
                var searchData = self.getSearchData();

                if (searchData != "" || searchData != null) {
                    searchData.city = searchData.city.split('-').join(' ');
                    searchData.from = searchData.from.split('-').join(' ');
                    searchData.to = searchData.to.split('-').join(' ');
                    searchData.noOfDays = searchData.noOfDays.trim();
                    self.cityName = searchData.city;

                    // self.fromL = searchData.from;
                    // self.toL = searchData.to;

                    self.pickUpH = searchData.hour;
                    self.pickUpM = searchData.min;
                    self.carRentDate = searchData.date;
                    self.noOfDays = searchData.noOfDays;
                    self.noOfDaysMultiplier = searchData.noOfDays;

                    axios.get(carRentUrl, {
                        headers: {
                            'content-type': 'text/html',
                            'Accept': 'text/html',
                            'Accept-Language': langauage
                        }
                    }).then(function (res) {
                        var PackagesTemp = [];
                        var packageData = res.data;
                        self.PackagesFull = packageData.Values;
                        self.fromTo();
                        self.fromL = searchData.from;
                        self.toL = searchData.to;
                        PackagesTemp = packageData.Values;


                        $("#from2").val(self.carRentDate);

                        PackagesTemp = PackagesTemp.filter(function (x) {
                            return x.City.toLowerCase().trim() == self.cityName.toLowerCase().trim() &&
                                x.Destination_From.toLowerCase().trim() == self.fromL.toLowerCase().trim() &&
                                x.Destination_To.toLowerCase().trim() == self.toL.toLowerCase().trim() &&
                                x.Number_Of_Days.trim() >= Number(self.noOfDays.trim());
                        });
                        // self.Packages = PackagesTemp;

                        // self.Packages = PackagesTemp;
                        self.Packages = _.sortBy(PackagesTemp, [function (o) {
                            return Number(o.Price);
                        }]);
                        if (!self.Packages.length) {
                            self.pageLoad = false;
                        } else {
                            self.pageLoad = true;
                        }

                    }).catch(function (error) {
                        console.log('Error', error);
                        self.content = [];
                        self.pageLoad = false;
                    });
                }


            });
        },
        getmoreinfo(url) {
            var customSearch =
                "&date=" + this.carRentDate +
                "&hour=" + ('0' + (this.pickUpH - 1)).slice(-2) +
                "&min=" + ('0' + (this.pickUpM - 1)).slice(-2) +
                "&noOfDays=" + this.noOfDays +
                "&city=" + this.cityName.split(' ').join('-') +
                "&from=" + this.fromL.split(' ').join('-') +
                "&to=" + this.toL.split(' ').join('-') + "&";

            if (url != null) {
                if (url != "") {
                    url = url.split("/Template/")[1];
                    url = url.split(' ').join('-');
                    url = url.split('.').slice(0, -1).join('.')
                    url = "/WTAD/car-rental-booking.html?page=" + url + customSearch;
                    window.location.href = url;
                } else {
                    url = "#";
                }
            } else {
                url = "#";
            }
            return url;
        },
        getSearchData: function () {
            var urldata = this.getUrlVars();
            return urldata;
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            let decodeUrl = decodeURIComponent(window.location.href);
            var hashes = decodeUrl.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        dropdownChange: function (event, type) {
            var self = this;
            if (event != undefined && event.target != undefined && event.target.value != undefined) {
                if (type == 'cityD') {
                    self.cityName = event.target.value;
                    // $('#tFrom').val('');
                    // $('#tTo').val('');
                    self.fromTo();
                } else if (type == 'fromD') {
                    self.fromL = event.target.value;
                } else if (type == 'toD') {
                    self.toL = event.target.value;
                } else if (type == 'noOfDaysId') {
                    self.noOfDays = event.target.value;
                } else if (type == 'from2D') {
                    self.carRentDate = event.target.value;
                } else if (type == 'pickuphourD') {
                    self.pickUpH = event.target.value;
                } else if (type == 'pickupminD') {
                    self.pickUpM = event.target.value;
                }
            }
        },
        modifySearch: function () {
            var self = this;
            self.setCalender();

            if (self.cityName == undefined || self.cityName == '' || self.cityName == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M011')).set('closable', false);
                return false;
            } else if (self.carRentDate == undefined || self.carRentDate == '' || self.carRentDate == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M049')).set('closable', false);
                return false;
            } else if (self.fromL == undefined || self.fromL == '' || self.fromL == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M013')).set('closable', false);
                return false;
            } else if (self.toL == undefined || self.toL == '' || self.toL == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M014')).set('closable', false);
                return false;
            } else if (self.noOfDays == undefined || self.noOfDays == '' || self.noOfDays == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M050')).set('closable', false);
                return false;
            } else if (self.pickUpH == undefined || self.pickUpH == '' || self.pickUpH == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M015')).set('closable', false);
                return false;
            } else if (self.pickUpM == undefined || self.pickUpM == '' || self.pickUpM == null) {
                alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M016')).set('closable', false);
                return false;
            }
            // else if (self.fromL == self.toL) {
            //     alertify.alert(getValidationMsgByCode('M035'), getValidationMsgByCode('M048')).set('closable', false);
            //     return false;
            // } 
            else {
                var PackagesTemp = [];
                PackagesTemp = self.PackagesFull;
                PackagesTemp = PackagesTemp.filter(function (x) {
                    return x.City.toLowerCase().trim() == self.cityName.toLowerCase().trim() &&
                        x.Destination_From.toLowerCase().trim() == self.fromL.toLowerCase().trim() &&
                        x.Destination_To.toLowerCase().trim() == self.toL.toLowerCase().trim() &&
                        x.Number_Of_Days.trim() >= Number(self.noOfDays.trim());
                });
                self.$nextTick(function () {
                    // self.Packages = PackagesTemp;
                    self.Packages = _.sortBy(PackagesTemp, [function (o) {
                        return Number(o.Price);
                    }]);
                    self.noOfDaysMultiplier=self.noOfDays;
                    if (!self.Packages.length) {
                        self.pageLoad = false;
                    } else {
                        self.pageLoad = true;
                    }
                });
            }

        },
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#from2").datepicker({
                minDate: new Date('11/20/2021'),
                maxDate: new Date('12/08/2021'),
                numberOfMonths: 1,
                changeMonth: true,
                showOn: "both",
                buttonText: "<i class='fa fa-calendar'></i>",
                duration: "fast",
                showAnim: "slide",
                showOptions: {
                    direction: "up"
                },
                showButtonPanel: false,
                dateFormat: dateFormat,
            });
        },
        fromTo: function () {
            var self = this;
            // $('#from').val('');
            // $('#to').val('');

            $('#tFrom').val('').trigger('change');
            $('#tTo').val('').trigger('change');
            self.resultFrom = [];
            self.resultTo = [];
            self.cityList = [];
            self.fromL = '';
            self.toL = '';

            var fromTemp = [];
            var toTemp = [];
            var maxPax = [];
            var cityFull = [];
            var filteredListFromToTemp = [];
            self.PackagesFull.forEach(function (item, index) {
                cityFull.push(item.City.toUpperCase().trim());
                // maxPax.push(item.Number_Of_Days.trim());
                if (!isNaN(item.Number_Of_Days.trim())) {
                    maxPax.push(Number((item.Number_Of_Days.trim())));
                }

            });
            filteredListFromToTemp = self.PackagesFull.filter(carRental => (carRental.City.toLowerCase()).trim() == (self.cityName.toLowerCase()).trim());
            filteredListFromToTemp.forEach(function (item, index) {
                fromTemp.push(item.Destination_From.toUpperCase().trim());
                toTemp.push(item.Destination_To.toUpperCase().trim());
            });
            self.numberPassenger = _.max(maxPax);
            // self.resultFrom = fromTemp;
            // self.resultTo = toTemp;


            self.$nextTick(function () {
                self.cityList = _.uniq(_.sortBy(cityFull));
                self.resultFrom = _.uniq(_.sortBy(fromTemp));
                self.resultTo = _.uniq(_.sortBy(toTemp));
            });
        },
    },
    mounted: function () {
        var vm = this;
        var historyPage = localStorage.getItem("backUrl") ? localStorage.getItem("backUrl") : null;
        if (historyPage) {
            localStorage.removeItem("backUrl");
            window.location.href = historyPage;
        }
        vm.getPagedata();
        vm.getPagecontent();
        vm.setCalender();
        sessionStorage.active_e = 6;
        // vm.$nextTick(function () {
        $('#city').on("change", function (e) {
            vm.dropdownChange(e, 'cityD');
        });
        $('#tFrom').on("change", function (e) {
            vm.dropdownChange(e, 'fromD')
        });
        $('#tTo').on("change", function (e) {
            vm.dropdownChange(e, 'toD')
        });
        $('#pickuphour').on("change", function (e) {
            vm.dropdownChange(e, 'pickuphourD')
        });
        $('#pickupmin').on("change", function (e) {
            vm.dropdownChange(e, 'pickupminD')
        });
        $('#noOfDaysId').on("change", function (e) {
            vm.dropdownChange(e, 'noOfDaysId')
        });
        $('#from2').on("change", function (e) {
            vm.dropdownChange(e, 'from2D')
            // });
        })
    }
});

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
jQuery(document).ready(function ($) {
    $('.myselect').select2({
        minimumResultsForSearch: Infinity,
        'width': '100%'
    });
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-chevron-down"></i>');
});