

/*----------------------------------------------------*/
// Select2
/*----------------------------------------------------*/

jQuery(document).ready(function ($) {
	$('.myselect').select2({
		minimumResultsForSearch: Infinity,
		'width': '100%'
	});
	$('b[role="presentation"]').hide();
	$('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>');
});


/***************************************************************
		BEGIN: VARIOUS DATEPICKER & SPINNER INITIALIZATION
***************************************************************/
$(document).ready(function () {
	$( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#from, #from1, #from2" )
        .datepicker({
          minDate: 'dateToday',
            maxDate: '+6D',
          // minDate: new Date('11-15-20'),
          //  maxDate: new Date('11-20-20'),
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
          
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to, #to1, #to2" ).datepicker({
        minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
});


